package com.thailife.api.policyInfoService.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import org.junit.jupiter.api.Assertions;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class ServiceCommon<Resp> {
  private final ObjectMapper mapper = JsonUtils.mapper;

  public void serviceCommon( ResponseMessages<Resp> response,  ResponseMessages<Resp> expected) {
    expectedResult(response,expected);
  }

  public void expectedResult(ResponseMessages<Resp> response, ResponseMessages<Resp> expected) {
    expectedStatus(expected, response);
  }

  public ResponseMessages<Resp> getRespJson(String key, String fileName)
      throws JsonProcessingException {
    return mapper.readValue(
        getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<Resp>>() {});
  }

  private void expectedStatus(
      ResponseMessages<Resp> responseJson, ResponseMessages<Resp> response) {
    Assert.notNull(response, "response object.[" + response + "]");
    Assert.notNull(
        response.getResponseStatus(),
        "responseStatus object.[" + response.getResponseStatus() + "]");
    ResponseStatus statusJson = responseJson.getResponseStatus();
    switch (statusJson.getErrorCode()) {
      case "200":
        TestUtils.exceptSuccess(response.getResponseStatus());
        break;
      case "204":
        TestUtils.exceptDataNotFound(response.getResponseStatus());
        break;
      case "400":
        TestUtils.exceptIncorrectData(response.getResponseStatus());
        break;
      case "500":
        TestUtils.exceptInternalServerError(responseJson.getResponseStatus());
        break;
    }
  }

  public String getJsonString(String key, String contactFile) {
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    return json;
  }

  public void expectedEquals(Object resp, Object act, String msg) {
    Assertions.assertEquals(resp, act, String.format(msg +" | response: %s, actual: %s", resp, act));
  }

  public void expectedSizeArrays(int resp, int act) {
    Assertions.assertEquals(resp, act, String.format("size response: %d, actual: %d ", resp, act));
  }
}
