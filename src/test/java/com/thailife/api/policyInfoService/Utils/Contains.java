package com.thailife.api.policyInfoService.Utils;

public class Contains {
  //success
  public static final String S = "S";
  //internal error
  public static final String E = "E";
  //incorrect data
  public static final String I = "I";
  //not found
  public static final String N = "N";
}
