package com.thailife.api.policyInfoService.Utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonUtils {
    public static final String keyNotFound = "Key Not Found";
    public static final ObjectMapper mapper =
            new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    public static String getJsonByKey(String key, String fileName) {
        try {
            Path resourceDirectory = Paths.get("src", "test", "resources/json/" + fileName);
            String text = new String(Files.readAllBytes(resourceDirectory));
            JSONObject json = new JSONObject(text);
            if (json.has(key)) {
                return json.get(key).toString();
            } else {
                return keyNotFound;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return keyNotFound;
        }
    }
}
