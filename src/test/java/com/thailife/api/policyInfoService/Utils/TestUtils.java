package com.thailife.api.policyInfoService.Utils;

import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import org.junit.jupiter.api.Assertions;
import org.springframework.util.Assert;

public class TestUtils {
  public static void exceptSuccess(ResponseStatus responseStatus) {
    Assertions.assertEquals(
        "200",
        responseStatus.getErrorCode(),
        "[ErrorCode] : 200 == " + responseStatus.getErrorCode());
    Assertions.assertEquals(
        Contains.S,
        responseStatus.getStatusCode(),
        "[StatusCode] : " + Contains.S + " == " + responseStatus.getStatusCode());
    Assertions.assertEquals(
        "ดำเนินการเรียบร้อย",
        responseStatus.getErrorMessage(),
        "[ErrorMessage] : ดำเนินการเรียบร้อย == " + responseStatus.getErrorMessage());
  }

  public static void exceptIncorrectData(ResponseStatus responseStatus) {
    Assertions.assertEquals(
        "400",
        responseStatus.getErrorCode(),
        "[ErrorCode] : 400 == " + responseStatus.getErrorCode());
    Assertions.assertEquals(
        Contains.E,
        responseStatus.getStatusCode(),
        "[StatusCode] : " + Contains.E + " == " + responseStatus.getStatusCode());
    Assertions.assertEquals(
        "การค้นหาไม่ถูกต้อง",
        responseStatus.getErrorMessage(),
        "[ErrorMessage] : การค้นหาไม่ถูกต้อง == " + responseStatus.getErrorMessage());
  }

  public static void exceptDataNotFound(ResponseStatus responseStatus) {
    Assertions.assertEquals(
        "204",
        responseStatus.getErrorCode(),
        "[ErrorCode] : 204 == " + responseStatus.getErrorCode());
    Assertions.assertEquals(
        Contains.E,
        responseStatus.getStatusCode(),
        "[StatusCode] : " + Contains.E + " == " + responseStatus.getStatusCode());
    Assertions.assertEquals(
        "ไม่พบข้อมูลที่ต้องการในระบบ",
        responseStatus.getErrorMessage(),
        "[ErrorMessage] : ไม่พบข้อมูลที่ต้องการในระบบ == " + responseStatus.getErrorMessage());
  }

  public static void exceptInternalServerError(ResponseStatus responseStatus) {
    Assertions.assertEquals(
            "500",
            responseStatus.getErrorCode(),
            "[ErrorCode] : 500 == " + responseStatus.getErrorCode());
    Assertions.assertEquals(
            Contains.E,
            responseStatus.getStatusCode(),
            "[StatusCode] : " + Contains.E + " == " + responseStatus.getStatusCode());
    Assertions.assertEquals(
            "ระบบขัดข้องกรุณาลองใหม่อีกครั้ง",
            responseStatus.getErrorMessage(),
            "[ErrorMessage] : ระบบขัดข้องกรุณาลองใหม่อีกครั้ง == " + responseStatus.getErrorMessage());
  }
}
