package com.thailife.api.policyInfoService.controller.pol18;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.model.PolicyOnlyM;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
public class SearchPolicyOnlyByPartyIdTest {
  private final ObjectMapper mapper = JsonUtils.mapper;

  @Autowired SearchPolicyOnlyByPartyIdController searchPolicyOnlyByPartyId;

  @Test
  public void success01() throws Exception {
    String key = "success01";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    SearchPolicyByPartyIdResponse expecResult =  getExpectResult(key);
    expect(resp, expecResult, Contains.S);
  }
  @Test
  public void success02() throws Exception {
    String key = "success02";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    SearchPolicyByPartyIdResponse expecResult =  getExpectResult(key);
    expect(resp, expecResult, Contains.S);
  }
  @Test
  public void success03() throws Exception {
    String key = "success03";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    SearchPolicyByPartyIdResponse expecResult =  getExpectResult(key);
    expect(resp, expecResult, Contains.S);
  }
  @Test
  public void validateMsgIsEmpty() throws Exception {
    String key = "validateMsgIsEmpty";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.E);
  }
  @Test
  public void validateDateSentIsEmpty() throws Exception {
    String key = "validateDateSentIsEmpty";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.E);
  }

  @Test
  public void validateCustomerIdIsEmpty() throws Exception {
    String key = "validateCustomerIdIsEmpty";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.E);
  }

  @Test
  public void validateNoHeader() throws Exception {
    String key = "validateNoHeader";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.E);
  }

  @Test
  public void msgIdOverLength() throws Exception {
    String key = "msgIdOverLength";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.E);
  }
  @Test
  public void msgIdOverLength2() throws Exception {
    String key = "msgIdOverLength2";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.E);
  }

  @Test
  public void customerIdNotFound() throws Exception {
    String key = "customerIdNotFound";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.N);
  }
  @Test
  public void customerIdWrongFormat() throws Exception {
    String key = "customerIdWrongFormat";
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = getResponse(key);
    expect(resp, null, Contains.E);
  }



  private ResponseMessages<SearchPolicyByPartyIdResponse> getResponse(String key) throws Exception {
    String contactFile = "request/pol18.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    RequestMessages<SearchPolicyByCustomerIdRequest> req =
        mapper.readValue(
            json, new TypeReference<RequestMessages<SearchPolicyByCustomerIdRequest>>() {});
    return searchPolicyOnlyByPartyId.service(req).getBody();
  }

  private void expect(
      ResponseMessages<SearchPolicyByPartyIdResponse> resp, SearchPolicyByPartyIdResponse expect, String errorCode)
      throws JsonProcessingException {
    switch (errorCode) {
      case "E": // validateError
        Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
        Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
        break;
      case "S": // success
        Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
        Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
        List<PolicyOnlyM> respPol = resp.getResponseRecord().getList_of_policy();
        List<PolicyOnlyM> assertPol = expect.getList_of_policy();
        Collections.sort(assertPol, new Comparator<PolicyOnlyM>() {
          public int compare(PolicyOnlyM o1, PolicyOnlyM o2) {
            return o2.getPolicyno().compareTo(o1.getPolicyno());
          }
        });
        Collections.sort(respPol, new Comparator<PolicyOnlyM>() {
          public int compare(PolicyOnlyM o1, PolicyOnlyM o2) {
            return o2.getPolicyno().compareTo(o1.getPolicyno());
          }
        });
        Assertions.assertEquals(resp.getResponseRecord(), expect);
        break;
      case "N" :
        Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
        Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
        break;
    }
  }

  private SearchPolicyByPartyIdResponse getExpectResult(String key) throws JsonProcessingException {
    String contactFile = "expectResult/pol18.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    return mapper.readValue(json, new TypeReference<SearchPolicyByPartyIdResponse>() {});
  }

}
