package com.thailife.api.policyInfoService.controller.pol12;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.model.CurrentAVM;
import com.thailife.api.policyInfoService.model.ListOfProtection;
import com.thailife.api.policyInfoService.model.request.CurrentAVInfoRequest;
import com.thailife.api.policyInfoService.model.request.GetCoverageOverviewRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.CurrentAVInfoResponse;
import com.thailife.api.policyInfoService.model.response.GetCoverageOverviewResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
public class GetCoverageOverviewTest {
    private final ObjectMapper mapper = JsonUtils.mapper;

    @Autowired GetCoverageOverviewController getCoverageOverviewController;

    @Test
    public void success01() throws Exception {
        String key = "success01";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success02() throws Exception {
        String key = "success02";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success03() throws Exception {
        String key = "success03";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success04() throws Exception {
        String key = "success04";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success05() throws Exception {
        String key = "success05";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success06() throws Exception {
        String key = "success06";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success07() throws Exception {
        String key = "success07";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success08() throws Exception {
        String key = "success08";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success09() throws Exception {
        String key = "success09";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success10() throws Exception {
        String key = "success10";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success11() throws Exception {
        String key = "success11";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success12() throws Exception {
        String key = "success12";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success13() throws Exception {
        String key = "success13";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success14() throws Exception {
        String key = "success14";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success15() throws Exception {
        String key = "success15";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success16() throws Exception {
        String key = "success16";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success17() throws Exception {
        String key = "success17";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success18() throws Exception {
        String key = "success18";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success19() throws Exception {
        String key = "success19";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success20() throws Exception {
        String key = "success20";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success21() throws Exception {
        String key = "success21";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success22() throws Exception {
        String key = "success22";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success23() throws Exception {
        String key = "success23";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success24() throws Exception {
        String key = "success24";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success25() throws Exception {
        String key = "success25";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        ListOfProtection expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void validateMsgIsEmpty() throws Exception {
        String key = "validateMsgIsEmpty";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateSendTimeIsEmpty() throws Exception {
        String key = "validateSendTimeIsEmpty";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateCustIDIsEmpty() throws Exception {
        String key = "validateCustIDIsEmpty";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateMsgIDOverLength() throws Exception {
        String key = "validateMsgIDOverLength";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateCustIdOverLength() throws Exception {
        String key = "validateCustIdOverLength";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void notFoundCustId() throws Exception {
        String key = "notFoundCustId";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void validateCustIdWrongFormat01() throws Exception {
        String key = "validateCustIdWrongFormat01";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateCustIdWrongFormat02() throws Exception {
        String key = "validateCustIdWrongFormat02";
        ResponseMessages<ListOfProtection> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }



    private ResponseMessages<ListOfProtection> getResponse(String key) throws Exception {
        String contactFile = "request/pol12.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<GetCoverageOverviewRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<GetCoverageOverviewRequest>>() {});
        return getCoverageOverviewController.service(req).getBody();
    }

    private void expect(
            ResponseMessages<ListOfProtection> resp, ListOfProtection expect, String errorCode)
            throws JsonProcessingException {
        switch (errorCode) {
            case "E": // validateError
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
                break;
            case "S": // success
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                List<GetCoverageOverviewResponse> respWs = resp.getResponseRecord().getList_of_protection();
                List<GetCoverageOverviewResponse> assertResp = expect.getList_of_protection();
                Collections.sort(respWs, new Comparator<GetCoverageOverviewResponse>() {
                    public int compare(GetCoverageOverviewResponse o1, GetCoverageOverviewResponse o2) {
                        return o2.getCoverageCode().compareTo(o1.getCoverageCode());
                    }
                });
                Collections.sort(assertResp, new Comparator<GetCoverageOverviewResponse>() {
                    public int compare(GetCoverageOverviewResponse o1, GetCoverageOverviewResponse o2) {
                        return o2.getCoverageCode().compareTo(o1.getCoverageCode());
                    }
                });
                Assertions.assertEquals(resp.getResponseRecord(), expect);
                break;
            case "N" :
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                break;
        }
    }

    private ListOfProtection getExpectResult(String key) throws JsonProcessingException {
        String contactFile = "expectResult/pol12.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        return mapper.readValue(json, new TypeReference<ListOfProtection>() {});
    }
}
