package com.thailife.api.policyInfoService.controller.pol15;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.controller.pol14.SearchConsentController;
import com.thailife.api.policyInfoService.model.request.InsertConsentRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchConsentRequest;
import com.thailife.api.policyInfoService.model.response.InsertConsentResponse;
import com.thailife.api.policyInfoService.model.response.PolicyM;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SearchConsentResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class InsertConsentTest {
    private final ObjectMapper mapper = JsonUtils.mapper;
    @Autowired InsertConsentController insertConsentController;
    @Autowired SearchConsentController searchConsentController;

    @Test
    public void statusYPolicyOL_ORD() throws Exception {
        String key = "statusYPolicyOL_ORD";
        getResponse(key, Contains.S);

    }
    @Test
    public void statusYPolicyOL_IND() throws Exception {
        String key = "statusYPolicyOL_ORD";
        getResponse(key, Contains.S);

    }
    @Test
    public void statusYPolicyOL_WHL() throws Exception {
        String key = "statusYPolicyOL_WHL";
        getResponse(key, Contains.S);

    }
    @Test
    public void statusYPolicyOL_Takaful() throws Exception {
        String key = "statusYPolicyOL_Takaful";
        getResponse(key, Contains.S);
    }
    @Test
    public void statusYPolicyOL_PA() throws Exception {
        String key = "statusYPolicyOL_PA";
        getResponse(key, Contains.S);
    }
    @Test
    public void statusYPolicyUL() throws Exception {
        String key = "statusYPolicyOL_PA";
        getResponse(key, Contains.S);
    }
    @Test
    public void statusYPolicyUlip() throws Exception {
        String key = "statusYPolicyUlip";
        getResponse(key, Contains.S);
    }
//    @Test
//    public void statusYPolicyCL() throws Exception {
//        String key = "statusYPolicyCL";
//        getResponse(key, Contains.S);
//    }
    @Test
    public void statusNPolicyOL_ORD() throws Exception {
        String key = "statusNPolicyOL_ORD";
        getResponse(key, Contains.N);
    }
    @Test
    public void statusNPolicyOL_IND() throws Exception {
        String key = "statusNPolicyOL_IND";
        getResponse(key, Contains.S);
    }
    @Test
    public void statusNPolicyOL_WHD() throws Exception {
        String key = "statusNPolicyOL_WHD";
        getResponse(key, Contains.S);
    }
    @Test
    public void statusNPolicyOL_Takaful() throws Exception {
        String key = "statusNPolicyOL_Takaful";
        getResponse(key, Contains.S);
    }
    @Test
    public void statusNPolicyOL_PA() throws Exception {
        String key = "statusNPolicyOL_PA";
        getResponse(key, Contains.S);
    }
    @Test
    public void statusNPolicyUL() throws Exception {
        String key = "statusNPolicyUL";
        getResponse(key, Contains.S);
    }

    private void getResponse(String key, String checkType) throws Exception {
        String contactFile = "request/pol15.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<InsertConsentRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<InsertConsentRequest>>() {});

        ResponseMessages<InsertConsentResponse> result = insertConsentController.service(req).getBody();
        SearchConsentResponse expecResult = null;
        if(checkType.equals(Contains.S)){
            expecResult = checkResultResponse(key);
        }
        expect(result, expecResult, checkType, req.getRequestRecord());

    }

    private SearchConsentResponse checkResultResponse(String key) throws Exception {
        String contactFile = "request/pol15_callpol14CheckInsert.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<SearchConsentRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<SearchConsentRequest>>() {});
        return searchConsentController.service(req).getBody().getResponseRecord();
    }

    private void expect(
            ResponseMessages<InsertConsentResponse> resp, SearchConsentResponse expect, String errorCode, InsertConsentRequest req)
            throws JsonProcessingException {
        switch (errorCode) {
            case "E": // validateError
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
                Assertions.assertEquals(resp.getResponseStatus().getErrorMessage(), "การค้นหาไม่ถูกต้อง");
                break;
            case "S": // success
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
//                Assertions.assertEquals(expect.getCustomer().getFirstName(), req.getFirstName());
//                Assertions.assertEquals(expect.getCustomer().getLastName(), req.getLastName());
//                Assertions.assertEquals(expect.getCustomer().getPreName(), req.getPreName());
                for(PolicyM policyReq: req.getPolicy()){
                    for(PolicyM policyResp : expect.getPolicy()){
                        if(policyResp.getPolicyNo().equals(policyReq)){
                            Assertions.assertEquals(policyResp.getPolicyType(), policyReq.getPolicyType());
                            Assertions.assertEquals(policyResp.getFlag(), policyReq.getFlag());
                            Assertions.assertEquals(policyResp.getPlanName(), policyReq.getPlanName());
                        }
                    }
                }
                break;
            case "N" :
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                break;
        }
    }
}
