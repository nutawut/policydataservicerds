package com.thailife.api.policyInfoService.controller.pol07;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.request.GetDividendInfoRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetDividendInfoListResponse;
import com.thailife.api.policyInfoService.model.response.GetDividendInfoResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.service.impl.GetDividendInfoServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.RoundingMode;

@SpringBootTest
public class GetDividendInfo {
  @Autowired private GetDividendInfoServiceImpl getDividendInfoService;
  private final ServiceCommon<GetDividendInfoListResponse> serviceCommon = new ServiceCommon<>();
  private final String fileName = "pol_07_01.json";
  private String key = "POL-07-";

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินปันผล_รับแล้ว() throws Exception {
    key = key + "001";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินปันผล_ยังไม่ได้รับ() throws Exception {
    key = key + "002";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินปันผล_ยังไม่ได้รับตลอดสัญญา()
      throws Exception {
    key = key + "003";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินคืนตามเงื่อนไข_รับแล้ว() throws Exception {
    key = key + "004";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินคืนตามเงื่อนไข_ยังไม่ได้รับ()
      throws Exception {
    key = key + "005";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินคืนตามเงื่อนไข_ยังไม่ได้รับตลอดสัญญา()
      throws Exception {
    key = key + "006";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void ปันผล_OL() throws Exception {
    key = key + "007";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void ปันผล_IND() throws Exception {
    key = key + "008";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void ปันผล_WHL() throws Exception {
    key = key + "009";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void เงินคืน_OL() throws Exception {
    key = key + "010";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void เงินคืน_IND() throws Exception {
    key = key + "011";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  @Test
  public void เงินคืนทันที_OL() throws Exception {
    key = key + "012";
    RequestMessages<GetDividendInfoRequest> request = getRequest();
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    expected(response);
  }

  public void expected(ResponseMessages<GetDividendInfoListResponse> response) {
    try {
      ResponseMessages<GetDividendInfoListResponse> responseJson = getRespJson();
      serviceCommon.serviceCommon(response, responseJson);
      Assertions.assertNotNull(
          response.getResponseRecord(), "response: " + response.getResponseRecord());
      serviceCommon.expectedSizeArrays(
          response.getResponseRecord().getList_of_benefit().size(),
          responseJson.getResponseRecord().getList_of_benefit().size());
      boolean isFound = false;
      for (GetDividendInfoResponse tmpAct : responseJson.getResponseRecord().getList_of_benefit()) {
        for (GetDividendInfoResponse tmp : response.getResponseRecord().getList_of_benefit()) {
          if (tmp.getBenefit_year().equals(tmpAct.getBenefit_year())) {
            serviceCommon.expectedEquals(
                tmp.getBenefit_year(), tmpAct.getBenefit_year(), "getBenefit_year");
            serviceCommon.expectedEquals(
                tmp.getBenefit_amount_receive(),
                tmpAct.getBenefit_amount_receive().setScale(2, RoundingMode.HALF_EVEN),
                "getBenefit_amount_receive");
            serviceCommon.expectedEquals(
                tmp.getBenefit_channel_receive(),
                tmpAct.getBenefit_channel_receive(),
                "getBenefit_channel_receive");
            serviceCommon.expectedEquals(
                tmp.getBenefit_interest(),
                tmpAct.getBenefit_interest().setScale(2, RoundingMode.HALF_EVEN),
                "getBenefit_interest");
            serviceCommon.expectedEquals(
                tmp.getBenefit_effective_date(),
                tmpAct.getBenefit_effective_date(),
                "getBenefit_effective_date");
            serviceCommon.expectedEquals(
                tmp.getBenefit_premium(),
                tmpAct.getBenefit_premium().setScale(2, RoundingMode.HALF_EVEN),
                "getBenefit_premium");
            serviceCommon.expectedEquals(
                tmp.getBenefit_receivedstatus(),
                tmpAct.getBenefit_receivedstatus(),
                "getBenefit_receivedstatus");
            serviceCommon.expectedEquals(
                tmp.getBenefit_status(), tmpAct.getBenefit_status(), "getBenefit_status");
            serviceCommon.expectedEquals(
                tmp.getBenefit_statusdesc(),
                tmpAct.getBenefit_statusdesc(),
                "getBenefit_statusdesc");
            serviceCommon.expectedEquals(
                tmp.getBenefit_type(), tmpAct.getBenefit_type(), "getBenefit_type");
            isFound = true;
            continue;
          }
          Assertions.assertTrue(isFound, "isFound: " + isFound);
        }
        isFound = false;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public ResponseMessages<GetDividendInfoListResponse> getRespJson()
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<GetDividendInfoListResponse>>() {});
  }

  private RequestMessages<GetDividendInfoRequest> getRequest() throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "request/" + fileName),
        new TypeReference<RequestMessages<GetDividendInfoRequest>>() {});
  }
}
