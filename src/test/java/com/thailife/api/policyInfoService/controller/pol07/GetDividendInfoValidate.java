package com.thailife.api.policyInfoService.controller.pol07;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.request.GetDividendInfoRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetDividendInfoListResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.service.impl.GetDividendInfoServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetDividendInfoValidate {
  @Autowired private GetDividendInfoServiceImpl getDividendInfoService;
  private final ServiceCommon<GetDividendInfoListResponse> serviceCommon = new ServiceCommon<>();
  private final String fileName = "pol_07_02.json";
  private String key = "POL-07-";
  ResponseMessages<GetDividendInfoListResponse> response;

  @Test
  public void กรณี_messageId_เป็นค่าว่าง() throws Exception {
    key = key + "014";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  @Test
  public void กรณี_sentDateTime_เป็นค่าว่าง() throws Exception {
    key = key + "015";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  @Test
  public void กรณี_policyno_เป็นค่าว่าง() throws Exception {
    key = key + "016";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  @Test
  public void กรณี_type_เป็นค่าว่าง() throws Exception {
    key = key + "017";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  @Test
  public void กรณี_policyno_ไม่มีอยู่ในฐานข้อมูล() throws Exception {
    key = key + "018";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  @Test
  public void กรณี_messageId_มากกว่า_Length_50() throws Exception {
    key = key + "019";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  @Test
  public void กรณี_policyno_มากกว่า_Length_8() throws Exception {
    key = key + "020";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  @Test
  public void กรณี_type_มากกว่า_Length_30() throws Exception {
    key = key + "022";
    response = getDividendInfoService.service(getRequest(fileName, key));
    ResponseMessages<GetDividendInfoListResponse> expectedM = getRespJson();
    serviceCommon.serviceCommon(response, expectedM);
  }

  public RequestMessages<GetDividendInfoRequest> getRequest(String fileName, String key)
      throws JsonProcessingException {
    String pathReq = "request/";
    RequestMessages<GetDividendInfoRequest> request =
        JsonUtils.mapper.readValue(
            serviceCommon.getJsonString(key, pathReq + fileName),
            new TypeReference<RequestMessages<GetDividendInfoRequest>>() {});
    return request;
  }

  public ResponseMessages<GetDividendInfoListResponse> getRespJson()
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<GetDividendInfoListResponse>>() {});
  }
}
