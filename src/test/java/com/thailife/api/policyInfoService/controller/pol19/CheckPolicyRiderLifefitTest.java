package com.thailife.api.policyInfoService.controller.pol19;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.CheckPolicyRiderLifefitResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CheckPolicyRiderLifefitTest {

    @Autowired CheckPolicyRiderLifefitController checkPolicyRiderLifefitController;

    private final ObjectMapper mapper = JsonUtils.mapper;
    private final String errorValidate = "V";
    private final String errorNotFound = "E";

    @Test
    public void validateNotRequestRecord() throws Exception {
        String key = "validateNotRequestRecord";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateNotHeader() throws Exception {
        String key = "validateNotHeader";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateCustIdIsWrongFormat() throws Exception {
        String key = "validateCustIdIsWrongFormat";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateCusIdIsSpace() throws Exception {
        String key = "validateCusIdIsSpace";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void customerIdNotFoundDB() throws Exception {
        String key = "customerIdNotFoundDB";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorNotFound);
    }
    @Test
    public void customerIdLengthOverSize() throws Exception {
        String key = "customerIdLengthOverSize";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void msgIdLengthOverSize() throws Exception {
        String key = "msgIdLengthOverSize";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void msgIdIsEmpty() throws Exception {
        String key = "msgIdIsEmpty";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void sendDateIsEmpty() throws Exception {
        String key = "sendDateIsEmpty";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorValidate);
    }
    @Test
    public void notfound() throws Exception {
        String key = "notfound";
        ResponseMessages<CheckPolicyRiderLifefitResponse> resp = checkPolicyRiderLifefit(key);
        expect(resp, errorNotFound);
    }

    private ResponseMessages<CheckPolicyRiderLifefitResponse> checkPolicyRiderLifefit(String key)
            throws Exception {
        String contactFile = "request/pol19.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<SearchPolicyByCustomerIdRequest> req =
                mapper.readValue(json, new TypeReference<RequestMessages<SearchPolicyByCustomerIdRequest>>() {});
        return checkPolicyRiderLifefitController.service(req).getBody();
    }

    private void expect(ResponseMessages<?> resp, String expect)
            throws JsonProcessingException {
        ResponseStatus responseStatus = resp.getResponseStatus();
        if(expect.equals("S")){
            Assertions.assertEquals(responseStatus.getStatusCode(), "S");
            Assertions.assertEquals(responseStatus.getErrorCode(), "200");
            Assertions.assertEquals(responseStatus.getErrorMessage(), "ดำเนินการเรียบร้อย");
        }else if(expect.equals("E")){
//            Assertions.assertEquals(responseStatus.getStatusCode(), "E");
            Assertions.assertEquals(responseStatus.getErrorCode(), "204");
            Assertions.assertEquals(responseStatus.getErrorMessage(), "ไม่พบข้อมูลที่ต้องการในระบบ");
        }else if(expect.equals("V")){
            Assertions.assertEquals(responseStatus.getStatusCode(), "E");
            Assertions.assertEquals(responseStatus.getErrorCode(), "400");
            Assertions.assertEquals(responseStatus.getErrorMessage(), "การค้นหาไม่ถูกต้อง");
        }
    }
}
