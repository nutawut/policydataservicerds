package com.thailife.api.policyInfoService.controller.pol10;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.TestUtils;
import com.thailife.api.policyInfoService.model.request.BeneficiaryInfoRequest;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.response.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class BeneficiaryInfoControllerTest {
  @Autowired private BeneficiaryInfoController beneficiaryInfoController;
  private final ObjectMapper mapper = JsonUtils.mapper;


  @Test
  public void typeOL_O() throws Exception {
    String key = "typeOL_O";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeOL_I() throws Exception {
    String key = "typeOL_I";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeOL_W() throws Exception {
    String key = "typeOL_W";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeOL_Takaful() throws Exception {
    String key = "typeOL_Takaful";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeOL_PA() throws Exception {
    String key = "typeOL_PA";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeUL() throws Exception {
    String key = "typeUL";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeULIP() throws Exception {
    String key = "typeULIP";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeCL() throws Exception {
    String key = "typeCL";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void typeCL_Takaful() throws Exception {
    String key = "typeCL_Takaful";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.S, key);
  }


  @Test
  public void policyNoIsEmpty() throws Exception {
    String key = "policyNoIsEmpty";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.I);
  }

  @Test
  public void clCertNoIsEmpty() throws Exception {
    String key = "clCertNoIsEmpty";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.N);
  }

  @Test
  public void typeIsEmpty() throws Exception {
    String key = "typeIsEmpty";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.I);
  }

  @Test
  public void policyNoOverLength() throws Exception {
    String key = "policyNoOverLength";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.I);
  }

  @Test
  public void certNoOverLength() throws Exception {
    String key = "certNoOverLength";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.I);
  }

  @Test
  public void typeOverLength() throws Exception {
    String key = "typeOverLength";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.I);
  }

  @Test
  public void policyNoNotFound() throws Exception {
    String key = "policyNoNotFound";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.N);
  }

  @Test
  public void policyNoNotRelateType() throws Exception {
    String key = "policyNoNotRelateType";
    ResponseMessagesList<BeneficiaryInfoResponse> resp = getBeneficiaryInfoCommon(key);
    except(resp, Contains.N);
  }

  private ResponseMessagesList<BeneficiaryInfoResponse> getBeneficiaryInfoCommon(String key)
      throws Exception {
    String contactFile = "request/pol10.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    DataServiceRequestT<BeneficiaryInfoRequest> request =
        mapper.readValue(json, new TypeReference<DataServiceRequestT<BeneficiaryInfoRequest>>() {});
    return beneficiaryInfoController.GetBeneficiaryInfo(request).getBody();
  }

  private List<BeneficiaryInfoResponse> getExpectResult(String key) throws JsonProcessingException {
    String contactFile = "expectResult/pol10.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    return mapper.readValue(json, new TypeReference<List<BeneficiaryInfoResponse>>() {});
  }
  private void except(ResponseMessagesList<BeneficiaryInfoResponse>  resp, String expect)
          throws JsonProcessingException {
    except(resp, expect, "");
  }

  private void except(ResponseMessagesList<BeneficiaryInfoResponse> resp, String expect, String key)
      throws JsonProcessingException {
    ResponseStatus responseStatus = resp.getResponseStatus();
    if (Contains.S.equals(expect)) {
      TestUtils.exceptSuccess(responseStatus);
      Assertions.assertNotNull(resp.getResponseRecord());
      verifyResult(resp.getResponseRecord(), getExpectResult(key));
    } else if (Contains.I.equals(expect)) {
      TestUtils.exceptIncorrectData(responseStatus);
    } else {
      TestUtils.exceptDataNotFound(responseStatus);
    }
  }

  private void verifyResult(
      List<BeneficiaryInfoResponse> resultList, List<BeneficiaryInfoResponse> expectList) {
    Assertions.assertNotNull(resultList, "list_of_policy Shouldn't NULL");
    Assertions.assertFalse(resultList.isEmpty(), "list_of_policy Shouldn't Empty");
    Assertions.assertEquals(expectList.size(), resultList.size(), "result list size = expect list");
    for (int i = 0; i < resultList.size(); i++) {
      BeneficiaryInfoResponse result = resultList.get(i);
      BeneficiaryInfoResponse expect = expectList.get(i);
      Assertions.assertEquals(expect.getSequence(), result.getSequence(), "field [Sequence]");
      Assertions.assertEquals(expect.getPrename(), result.getPrename(), "field [Prename]");
      Assertions.assertEquals(expect.getFirstname(), result.getFirstname(), "field [Firstname]");
      Assertions.assertEquals(expect.getLastname(), result.getLastname(), "field [Lastname]");
      Assertions.assertEquals(
          expect.getRelationship(), result.getRelationship(), "field [Relationship]");
      Assertions.assertEquals(
          expect.getPercentshare(), result.getPercentshare(), "field [Percentshare]");
      Assertions.assertEquals(
          expect.getMainbeneficiary(), result.getMainbeneficiary(), "field [Mainbenficiary]");
    }
  }
}
