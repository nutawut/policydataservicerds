package com.thailife.api.policyInfoService.controller.pol02;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.TestUtils;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.PolicyDetailOLResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PolicyDetailOLControllerTest {
  @Autowired private PolicyDetailOLController policyDetailOLController;
  private final ObjectMapper mapper = JsonUtils.mapper;

  @Test
  public void nowHighDate2ORDStatusA() throws JsonProcessingException {
    String key = "nowHighDate2ORDStatusA";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2ORDStatusE() throws JsonProcessingException {
    String key = "nowHighDate2ORDStatusE";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2ORDStatusR() throws JsonProcessingException {
    String key = "nowHighDate2ORDStatusR";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2ORDStatusU() throws JsonProcessingException {
    String key = "nowHighDate2ORDStatusU";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2INDStatusR() throws JsonProcessingException {
    String key = "nowHighDate2INDStatusR";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2WHLStatusR() throws JsonProcessingException {
    String key = "nowHighDate2WHLStatusR";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusA() throws JsonProcessingException {
    String key = "nowLessDate2ORDStatusA";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusE() throws JsonProcessingException {
    String key = "nowLessDate2ORDStatusE";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusR() throws JsonProcessingException {
    String key = "nowLessDate2ORDStatusR";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusU() throws JsonProcessingException {
    String key = "nowLessDate2ORDStatusU";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2INDStatusR() throws JsonProcessingException {
    String key = "nowLessDate2INDStatusR";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2INDStatusU() throws JsonProcessingException {
    String key = "nowLessDate2INDStatusU";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2TakafuStatusU() throws JsonProcessingException {
    String key = "nowLessDate2TakafuStatusU";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void policyNoIsEmpty() throws JsonProcessingException {
    String key = "policyNoIsEmpty";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void policyNoIsNull() throws JsonProcessingException {
    String key = "policyNoIsNull";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void policyNoOverLength() throws JsonProcessingException {
    String key = "policyNoOverLength";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void typeIsEmpty() throws JsonProcessingException {
    String key = "typeIsEmpty";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void typeIsNull() throws JsonProcessingException {
    String key = "typeIsNull";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void typeOverLength() throws JsonProcessingException {
    String key = "typeOverLength";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void policyNotRelatedType() throws JsonProcessingException {
    String key = "policyNotRelatedType";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.E);
  }

  @Test
  public void policyNotFound() throws JsonProcessingException {
    String key = "policyNotFound";
    ResponseMessages<PolicyDetailOLResponse> resp = getPolicyDetailOL(key);
    expect(resp, Contains.E);
  }

  private ResponseMessages<PolicyDetailOLResponse> getPolicyDetailOL(String key)
      throws JsonProcessingException {
    String contactFile = "request/pol02_01.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    RequestMessages<PolicyDetailRequest> req =
        mapper.readValue(json, new TypeReference<RequestMessages<PolicyDetailRequest>>() {});
    return policyDetailOLController.service(req).getBody();
  }

  private PolicyDetailOLResponse getExpectResult(String key) throws JsonProcessingException {
    String contactFile = "expectResult/pol02_01.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    return mapper.readValue(json, new TypeReference<PolicyDetailOLResponse>() {});
  }

  private void expect(ResponseMessages<PolicyDetailOLResponse> resp, String expect)
      throws JsonProcessingException {
    expect(resp, expect, "");
  }

  private void expect(ResponseMessages<PolicyDetailOLResponse> resp, String expect, String key)
      throws JsonProcessingException {
    ResponseStatus responseStatus = resp.getResponseStatus();
    if (Contains.S.equals(expect)) {
      TestUtils.exceptSuccess(responseStatus);
      Assertions.assertNotNull(resp.getResponseRecord());
      verifyResult(resp.getResponseRecord(), getExpectResult(key));
    } else if (Contains.I.equals(expect)) {
      TestUtils.exceptIncorrectData(responseStatus);
    } else {
      TestUtils.exceptInternalServerError(responseStatus);
    }
  }

  private void verifyResult(PolicyDetailOLResponse result, PolicyDetailOLResponse expect) {
    Assertions.assertNotNull(result, "policyDetailOLResp Shouldn't NULL");
    Assertions.assertEquals(
        expect.getPrename(),
        result.getPrename(),
        String.format("PreName =: %s == %s", expect.getPrename(), result.getPrename()));
    Assertions.assertEquals(
        expect.getFirstname(),
        result.getFirstname(),
        String.format("FirstName =: %s == %s", expect.getFirstname(), result.getFirstname()));
    Assertions.assertEquals(
        expect.getLastname(),
        result.getLastname(),
        String.format("LastName =: %s == %s", expect.getLastname(), result.getLastname()));
    Assertions.assertEquals(
        expect.getSex(),
        result.getSex(),
        String.format("Sex =: %s == %s", expect.getSex(), result.getSex()));
    Assertions.assertEquals(
        expect.getType(),
        result.getType(),
        String.format("Type =: %s == %s", expect.getType(), result.getType()));

    Assertions.assertEquals(
        expect.getLifepremium(),
        result.getLifepremium(),
        String.format("LifePremium =: %s == %s", expect.getLifepremium(), result.getLifepremium()));
    Assertions.assertEquals(
        expect.getTopuppremium(),
        result.getTopuppremium(),
        String.format(
            "TopUpPremium =: %s == %s", expect.getTopuppremium(), result.getTopuppremium()));
    Assertions.assertEquals(
        expect.getInsureage(),
        result.getInsureage(),
        String.format("Insureage =: %s == %s", expect.getInsureage(), result.getInsureage()));
    Assertions.assertEquals(
        expect.getEffectivedate(),
        result.getEffectivedate(),
        String.format(
            "EffectiveDate =: %s == %s", expect.getEffectivedate(), result.getEffectivedate()));
    Assertions.assertEquals(
        expect.getMaturedate(),
        result.getMaturedate(),
        String.format("MatureDate =: %s == %s", expect.getMaturedate(), result.getMaturedate()));
    Assertions.assertEquals(
        expect.getPlancode(),
        result.getPlancode(),
        String.format("PlanCode =: %s == %s", expect.getPlancode(), result.getPlancode()));
    Assertions.assertEquals(
        expect.getPlanname(),
        result.getPlanname(),
        String.format("PlanName =: %s == %s", expect.getPlanname(), result.getPlanname()));
    Assertions.assertEquals(
        expect.getSum(),
        result.getSum(),
        String.format("Sum =: %s == %s", expect.getSum(), result.getSum()));
    Assertions.assertEquals(
        expect.getMode(),
        result.getMode(),
        String.format("Mode =: %s == %s", expect.getMode(), result.getMode()));
    Assertions.assertEquals(
        expect.getPolicystatus1(),
        result.getPolicystatus1(),
        String.format(
            "PolicyStatus1 =: %s == %s", expect.getPolicystatus1(), result.getPolicystatus1()));
    Assertions.assertEquals(
        expect.getPolicystatus1desc(),
        result.getPolicystatus1desc(),
        String.format(
            "PolicyStatus1Desc =: %s == %s",
            expect.getPolicystatus1desc(), result.getPolicystatus1desc()));
    Assertions.assertEquals(
        expect.getPolicystatus2(),
        result.getPolicystatus2(),
        String.format(
            "PolicyStatus2 =: %s == %s", expect.getPolicystatus2(), result.getPolicystatus2()));
    Assertions.assertEquals(
        expect.getPolicystatus2desc(),
        result.getPolicystatus2desc(),
        String.format(
            "PolicyStatus2Desc =: %s == %s",
            expect.getPolicystatus2desc(), result.getPolicystatus2desc()));
    Assertions.assertEquals(
        expect.getRemark(),
        result.getRemark(),
        String.format("Remark =: %s == %s", expect.getRemark(), result.getRemark()));

    Assertions.assertEquals(
        expect.getStatusdisplay(),
        result.getStatusdisplay(),
        String.format(
            "StatusDisplay =: %s == %s", expect.getStatusdisplay(), result.getStatusdisplay()));
    Assertions.assertEquals(
        expect.getEndownmentyear(),
        result.getEndownmentyear(),
        String.format(
            "EndOwnmentYear =: %s == %s", expect.getEndownmentyear(), result.getEndownmentyear()));
  }
}
