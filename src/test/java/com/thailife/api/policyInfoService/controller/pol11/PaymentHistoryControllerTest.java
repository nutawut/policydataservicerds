package com.thailife.api.policyInfoService.controller.pol11;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.TestUtils;
import com.thailife.api.policyInfoService.model.PaymentM;
import com.thailife.api.policyInfoService.model.request.PaymentHistoryRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class PaymentHistoryControllerTest {
  @Autowired private PaymentHistoryController paymentHistoryController;
  private final ObjectMapper mapper = JsonUtils.mapper;
  @Test
  public void ulAs2Year() throws Exception {
    String key = "ulAs2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void clAs2Year() throws Exception {
    String key = "clAs2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void olTakafulAs2Year() throws Exception {
    String key = "olTakafulAs2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void olOLess2Year() throws Exception {
    String key = "olOLess2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void ulLess2Year() throws Exception {
    String key = "ulLess2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void ulipLess2Year() throws Exception {
    String key = "ulipLess2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void clLess2Year() throws Exception {
    String key = "clLess2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void clTakafulLess2Year() throws Exception {
    String key = "clTakafulLess2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void olHigh2Year() throws Exception {
    String key = "olHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void ulHigh2Year() throws Exception {
    String key = "ulHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void ulipHigh2Year() throws Exception {
    String key = "ulipHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void clHigh2Year() throws Exception {
    String key = "clHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void olTakafulHigh2Year() throws Exception {
    String key = "olTakafulHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void setEffDateOLHigh2Year() throws Exception {
    String key = "setEffDateOLHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void setEffDateULHigh2Year() throws Exception {
    String key = "setEffDateULHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void setEffDateULIPHigh2Year() throws Exception {
    String key = "setEffDateULIPHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void setEffDateCLHigh2Year() throws Exception {
    String key = "setEffDateCLHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void setEffDateCLTakafulHigh2Year() throws Exception {
    String key = "setEffDateCLTakafulHigh2Year";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void policyNoNotRelateCertNo() throws Exception {
    String key = "policyNoNotRelateCertNo";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.N);
  }

  @Test
  public void policyNoIsEmpty() throws Exception {
    String key = "policyNoIsEmpty";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.I);
  }

  @Test
  public void certNoIsEmpty() throws Exception {
    String key = "certNoIsEmpty";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.N);
  }

  @Test
  public void typeIsEmpty() throws Exception {
    String key = "typeIsEmpty";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.I);
  }

  @Test
  public void effectDateAlien() throws Exception {
    String key = "effectDateAlien";
    ResponseMessages<PaymentHistoryResponse> resp = callPaymentHistory(key);
    except(resp, Contains.I);
  }

  private ResponseMessages<PaymentHistoryResponse> callPaymentHistory(String key) throws Exception {
    String contactFile = "request/pol11.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    RequestMessages<PaymentHistoryRequest> request =
        mapper.readValue(json, new TypeReference<RequestMessages<PaymentHistoryRequest>>() {});
    return paymentHistoryController.service(request).getBody();
  }

  private PaymentHistoryResponse getExpectResult(String key) throws JsonProcessingException {
    String contactFile = "expectResult/pol11.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    return mapper.readValue(json, new TypeReference<PaymentHistoryResponse>() {});
  }


  private void except(ResponseMessages<PaymentHistoryResponse>  resp, String expect)
          throws JsonProcessingException {
    except(resp, expect, "");
  }

  private void except(ResponseMessages<PaymentHistoryResponse> resp, String expect, String key)
          throws JsonProcessingException {
    ResponseStatus responseStatus = resp.getResponseStatus();
    if (Contains.S.equals(expect)) {
      TestUtils.exceptSuccess(responseStatus);
      Assertions.assertNotNull(resp.getResponseRecord());
      verifyResult(resp.getResponseRecord(), getExpectResult(key));
    } else if (Contains.I.equals(expect)) {
      TestUtils.exceptIncorrectData(responseStatus);
    } else {
      TestUtils.exceptDataNotFound(responseStatus);
    }
  }

  private void verifyResult(PaymentHistoryResponse resultList, PaymentHistoryResponse expectList) {
    Assertions.assertNotNull(resultList, "list_of_policy Shouldn't NULL");
    Assertions.assertFalse(
        resultList.getList_payment_history().isEmpty(), "list_of_policy Shouldn't Empty");
    Assertions.assertEquals(
        expectList.getList_payment_history().size(),
        resultList.getList_payment_history().size(),
        "result list size = expect list");
    Assertions.assertEquals(expectList.getType(), resultList.getType(), "field [Type]");
    for (int i = 0; i < resultList.getList_payment_history().size(); i++) {
      PaymentM result = resultList.getList_payment_history().get(i);
      PaymentM expect = expectList.getList_payment_history().get(i);

      Assertions.assertEquals(expect.getPay_period(),result.getPay_period(),"field [PayPeriod]");
      Assertions.assertEquals(expect.getEffective_date(),result.getEffective_date(),"field [effective_date]");
      Assertions.assertEquals(expect.getPay_date(),result.getPay_date(),"field [pay_date]");
      Assertions.assertEquals(expect.getReceipt_no(),result.getReceipt_no(),"field [receipt_no]");
      Assertions.assertEquals(expect.getPremium(),result.getPremium(),"field [premium]");
      Assertions.assertEquals(expect.getExtrapremium(),result.getExtrapremium(),"field [extrapremium]");
      Assertions.assertEquals(expect.getDiscountpremium(),result.getDiscountpremium(),"field [discountpremium]");
      Assertions.assertEquals(expect.getTotalpremium(),result.getTotalpremium(),"field [totalpremium]");
      Assertions.assertEquals(expect.getPayment_channel(),result.getPayment_channel(),"field [payment_channel]");
      Assertions.assertEquals(expect.getDuedate(),result.getDuedate(),"field [duedate]");
      Assertions.assertEquals(expect.getReceipt_sys_date(),result.getReceipt_sys_date(),"field [ReceiptSysDate]");
    }
  }
}
