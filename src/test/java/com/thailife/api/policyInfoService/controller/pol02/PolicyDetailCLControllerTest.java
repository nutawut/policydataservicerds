package com.thailife.api.policyInfoService.controller.pol02;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.TestUtils;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.PolicyDetailCLResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PolicyDetailCLControllerTest {
  @Autowired private PolicyDetailCLController policyDetailCLController;

  private final ObjectMapper mapper = JsonUtils.mapper;

  @Test
  public void typeTakaful() throws JsonProcessingException {
    String key = "typeTakaful";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void typeCL() throws JsonProcessingException {
    String key = "typeCL";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void monthlyPremiumPayment() throws JsonProcessingException {
    String key = "monthlyPremiumPayment";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void yearPremiumPayment() throws JsonProcessingException {
    String key = "yearPremiumPayment";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void oneTimePremiumPayment90() throws JsonProcessingException {
    String key = "oneTimePremiumPayment90";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void oneTimePremiumPaymentThroughOut() throws JsonProcessingException {
    String key = "oneTimePremiumPaymentThroughOut";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.S, key);
  }

  @Test
  public void policyIsEmpty() throws JsonProcessingException {
    String key = "policyIsEmpty";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void certNoIsEmpty() throws JsonProcessingException {
    String key = "certNoIsEmpty";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void typeIsEmpty() throws JsonProcessingException {
    String key = "typeIsEmpty";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void typeNoFound() throws JsonProcessingException {
    String key = "typeNoFound";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void policyNoMisMatchType() throws JsonProcessingException {
    String key = "policyNoMisMatchType";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.I);
  }

  @Test
  public void policyNoNotFound() throws JsonProcessingException {
    String key = "policyNoNotFound";
    ResponseMessages<PolicyDetailCLResponse> resp = getPolicyDetailCL(key);
    expect(resp, Contains.N);
  }

  private ResponseMessages<PolicyDetailCLResponse> getPolicyDetailCL(String key) throws JsonProcessingException {
    String contactFile = "request/pol02_02.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    RequestMessages<PolicyDetailRequest> req =
        mapper.readValue(json, new TypeReference<RequestMessages<PolicyDetailRequest>>() {});
    return policyDetailCLController.service(req).getBody();
  }

  private PolicyDetailCLResponse getExpectResult(String key) throws JsonProcessingException {
    String contactFile = "expectResult/pol02_02.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    return mapper.readValue(json, new TypeReference<PolicyDetailCLResponse>() {});
  }

  private void expect(ResponseMessages<PolicyDetailCLResponse> resp, String expect)
      throws JsonProcessingException {
    expect(resp, expect, "");
  }

  private void expect(ResponseMessages<PolicyDetailCLResponse> resp, String expect, String key)
      throws JsonProcessingException {
    ResponseStatus responseStatus = resp.getResponseStatus();
    if (Contains.S.equals(expect)) {
      TestUtils.exceptSuccess(responseStatus);
      Assertions.assertNotNull(resp.getResponseRecord());
      verifyResult(resp.getResponseRecord(), getExpectResult(key));
    } else if (Contains.I.equals(expect)) {
      TestUtils.exceptIncorrectData(responseStatus);
    } else if (Contains.N.equals(expect)) {
      TestUtils.exceptDataNotFound(responseStatus);
    } else {
      TestUtils.exceptInternalServerError(responseStatus);
    }
  }

  private void verifyResult(PolicyDetailCLResponse result, PolicyDetailCLResponse expect) {
    Assertions.assertNotNull(result, "policyDetailOLResp Shouldn't NULL");
    Assertions.assertEquals(
        expect.getPrename(),
        result.getPrename(),
        String.format("PreName =: %s == %s", expect.getPrename(), result.getPrename()));
    Assertions.assertEquals(
        expect.getFirstname(),
        result.getFirstname(),
        String.format("FirstName =: %s == %s", expect.getFirstname(), result.getFirstname()));
    Assertions.assertEquals(
        expect.getLastname(),
        result.getLastname(),
        String.format("LastName =: %s == %s", expect.getLastname(), result.getLastname()));
    Assertions.assertEquals(
        expect.getSex(),
        result.getSex(),
        String.format("Sex =: %s == %s", expect.getSex(), result.getSex()));
    Assertions.assertEquals(
        expect.getType(),
        result.getType(),
        String.format("Type =: %s == %s", expect.getType(), result.getType()));
    Assertions.assertEquals(
        expect.getInsureage(),
        result.getInsureage(),
        String.format("Insureage =: %s == %s", expect.getInsureage(), result.getInsureage()));
    Assertions.assertEquals(
        expect.getEffectivedate(),
        result.getEffectivedate(),
        String.format(
            "EffectiveDate =: %s == %s", expect.getEffectivedate(), result.getEffectivedate()));
    Assertions.assertEquals(
        expect.getMaturedate(),
        result.getMaturedate(),
        String.format("MatureDate =: %s == %s", expect.getMaturedate(), result.getMaturedate()));
    Assertions.assertEquals(
        expect.getPlancode(),
        result.getPlancode(),
        String.format("PlanCode =: %s == %s", expect.getPlancode(), result.getPlancode()));
    Assertions.assertEquals(
        expect.getPlanname(),
        result.getPlanname(),
        String.format("PlanName =: %s == %s", expect.getPlanname(), result.getPlanname()));
    Assertions.assertEquals(
        expect.getSum(),
        result.getSum(),
        String.format("Sum =: %s == %s", expect.getSum(), result.getSum()));
    Assertions.assertEquals(
        expect.getMode(),
        result.getMode(),
        String.format("Mode =: %s == %s", expect.getMode(), result.getMode()));
    Assertions.assertEquals(
        expect.getRemark(),
        result.getRemark(),
        String.format("Remark =: %s == %s", expect.getRemark(), result.getRemark()));
    Assertions.assertEquals(
        expect.getStatusdisplay(),
        result.getStatusdisplay(),
        String.format(
            "StatusDisplay =: %s == %s", expect.getStatusdisplay(), result.getStatusdisplay()));
    Assertions.assertEquals(
        expect.getEndownmentyear(),
        result.getEndownmentyear(),
        String.format(
            "EndOwnmentYear =: %s == %s", expect.getEndownmentyear(), result.getEndownmentyear()));
    Assertions.assertEquals(
        expect.getStatcer(),
        result.getStatcer(),
        String.format("Statcer =: %s == %s", expect.getStatcer(), result.getStatcer()));
    Assertions.assertEquals(
        expect.getStatcer2(),
        result.getStatcer2(),
        String.format("Statcer2 =: %s == %s", expect.getStatcer2(), result.getStatcer2()));
    Assertions.assertEquals(
        expect.getStatcerdesc(),
        result.getStatcerdesc(),
        String.format("StatcerDesc =: %s == %s", expect.getStatcerdesc(), result.getStatcerdesc()));
    Assertions.assertEquals(
        expect.getStatcer2desc(),
        result.getStatcer2desc(),
        String.format(
            "Statcer2Desc =: %s == %s", expect.getStatcer2desc(), result.getStatcer2desc()));
  }
}
