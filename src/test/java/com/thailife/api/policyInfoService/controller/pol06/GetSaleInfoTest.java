package com.thailife.api.policyInfoService.controller.pol06;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SaleInfoRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SaleInfoResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetSaleInfoTest {
    private final ServiceCommon<SaleInfoResponse> serviceCommon = new ServiceCommon<>();

    @Autowired SaleInfoController saleInfoController;
    private final ObjectMapper mapper = JsonUtils.mapper;

    @Test
    public void successOL_O() throws Exception {
        String key = "successOL_O";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successOL_I() throws Exception {
        String key = "successOL_I";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successOL_W() throws Exception {
        String key = "successOL_W";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successOL_Takaful() throws Exception {
        String key = "successOL_Takaful";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successOL_PA() throws Exception {
        String key = "successOL_PA";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successUL() throws Exception {
        String key = "successUL";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successUlip() throws Exception {
        String key = "successUlip";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
//    @Test
//    public void successCL() throws Exception {
//        String key = "successCL";
//        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
//        SaleInfoResponse expecResult =  getExpectResult(key);
//        expect(resp, expecResult, Contains.S);
//    }

    @Test
    public void validateMsgEmpty() throws Exception {
        String key = "validateMsgEmpty";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateSendTimeEmpty() throws Exception {
        String key = "validateSendTimeEmpty";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validatePolicyNoEmpty() throws Exception {
        String key = "validatePolicyNoEmpty";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateTypeEmpty() throws Exception {
        String key = "validateTypeEmpty";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void ValidateMsgIdOverLength() throws Exception {
        String key = "ValidateMsgIdOverLength";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validatePolicyOverLength() throws Exception {
        String key = "validatePolicyOverLength";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateTypeOverlemgth() throws Exception {
        String key = "validateTypeOverlemgth";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void policyNotFoundDB() throws Exception {
        String key = "policyNotFoundDB";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
    }
//    @Test
//    public void policyAndypeNotMath() throws Exception {
//        String key = "policyAndypeNotMath";
//        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
//        SaleInfoResponse expecResult =  getExpectResult(key);
//        expect(resp, expecResult, Contains.N);
//    }
    @Test
    public void validatePOlicySpace() throws Exception {
        String key = "validatePOlicySpace";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validatePolicyWrongFormat() throws Exception {
        String key = "validatePolicyWrongFormat";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void typeISspace() throws Exception {
        String key = "typeISspace";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        SaleInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void validateHeaderIsNull() throws Exception {
        String key = "validateHeaderIsNull";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void ValidateRecordIsNull() throws Exception {
        String key = "ValidateRecordIsNull";
        ResponseMessages<SaleInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }



    private ResponseMessages<SaleInfoResponse> getResponse(String key) throws Exception {
        String contactFile = "request/pol06.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<SaleInfoRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<SaleInfoRequest>>() {});
        return saleInfoController.service(req).getBody();
    }

    private void expect(
            ResponseMessages<SaleInfoResponse> resp, SaleInfoResponse expect, String errorCode)
            throws JsonProcessingException {
        switch (errorCode) {
            case "E": // validateError
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
                break;
            case "S": // success
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                Assertions.assertEquals(resp.getResponseRecord(), expect);
                break;
            case "N" :
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                break;
        }
    }

    private SaleInfoResponse getExpectResult(String key) throws JsonProcessingException {
        String contactFile = "expectResult/pol06.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        return mapper.readValue(json, new TypeReference<SaleInfoResponse>() {});
    }
}
