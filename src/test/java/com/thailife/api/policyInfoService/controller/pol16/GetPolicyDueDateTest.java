package com.thailife.api.policyInfoService.controller.pol16;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.model.PolicyOnlyM;
import com.thailife.api.policyInfoService.model.request.GetPolicyDueDateRequest;
import com.thailife.api.policyInfoService.model.request.PolicyTypeRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetPolicyDueDateResponse;
import com.thailife.api.policyInfoService.model.response.PolicyResponse;
import com.thailife.api.policyInfoService.model.response.PolicyTypeResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
public class GetPolicyDueDateTest {
    private final ObjectMapper mapper = JsonUtils.mapper;
    @Autowired GetPolicyDueDateController getPolicyDueDateController;

    @Test
    public void success01() throws Exception {
        String key = "success01";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successUl() throws Exception {
        String key = "successUl";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void dataNotFound() throws Exception {
        String key = "dataNotFound";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
//        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void dataNotFound2() throws Exception {
        String key = "dataNotFound2";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
//        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void successListEmpty() throws Exception {
        String key = "successListEmpty";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
//        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, null, Contains.N);
    }

    @Test
    public void listEmpty2() throws Exception {
        String key = "listEmpty2";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
//        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void successOlStatusB() throws Exception {
        String key = "successOlStatusB";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successcUnitlink() throws Exception {
        String key = "successcUnitlink";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success037() throws Exception {
        String key = "success037";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success02_001() throws Exception {
        String key = "success02_001";
        ResponseMessages<GetPolicyDueDateResponse> resp = getResponse(key);
        GetPolicyDueDateResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }




    private ResponseMessages<GetPolicyDueDateResponse> getResponse(String key) throws Exception {
        String contactFile = "request/pol16.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<GetPolicyDueDateRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<GetPolicyDueDateRequest>>() {});
        return getPolicyDueDateController.service(req).getBody();
    }

    private void expect(
            ResponseMessages<GetPolicyDueDateResponse> resp, GetPolicyDueDateResponse expect, String errorCode)
            throws JsonProcessingException {
        switch (errorCode) {
            case "E": // validateError
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
                break;
            case "S": // success
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                Assertions.assertEquals(resp.getResponseRecord().getList_of_policy().size(),expect.getList_of_policy().size());
                List<PolicyResponse> respPol = resp.getResponseRecord().getList_of_policy();
                List<PolicyResponse> assertPol = expect.getList_of_policy();
                Collections.sort(assertPol, new Comparator<PolicyResponse>() {
                    public int compare(PolicyResponse o1, PolicyResponse o2) {
                        return o2.getPolicyno().compareTo(o1.getPolicyno());
                    }
                });
                Collections.sort(respPol, new Comparator<PolicyResponse>() {
                    public int compare(PolicyResponse o1, PolicyResponse o2) {
                        return o2.getPolicyno().compareTo(o1.getPolicyno());
                    }
                });
                Assertions.assertEquals(resp.getResponseRecord(), expect);
                break;
            case "N" :
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                break;
        }
    }

    private GetPolicyDueDateResponse getExpectResult(String key) throws JsonProcessingException {
        String contactFile = "expectResult/pol16.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        return mapper.readValue(json, new TypeReference<GetPolicyDueDateResponse>() {});
    }
}
