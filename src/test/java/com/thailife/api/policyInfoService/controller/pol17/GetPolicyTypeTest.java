package com.thailife.api.policyInfoService.controller.pol17;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.model.request.PolicyTypeRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.PolicyTypeResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetPolicyTypeTest {
    private final ObjectMapper mapper = JsonUtils.mapper;

    @Autowired GetPolicyTypeController getPolicyTypeController;

    @Test
    public void success01() throws Exception {
        String key = "success01";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success02() throws Exception {
        String key = "success02";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success03() throws Exception {
        String key = "success03";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success04() throws Exception {
        String key = "success04";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success05() throws Exception {
        String key = "success05";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success06() throws Exception {
        String key = "success06";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success07() throws Exception {
        String key = "success07";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.N);
    }
    @Test
    public void success08() throws Exception {
        String key = "success08";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.N);
    }
    @Test
    public void success09() throws Exception {
        String key = "success09";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success10() throws Exception {
        String key = "success10";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.N);
    }
    @Test
    public void success11() throws Exception {
        String key = "success11";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.N);
    }
    @Test
    public void success12() throws Exception {
        String key = "success12";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.N);
    }
    @Test
    public void success13() throws Exception {
        String key = "success13";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success14() throws Exception {
        String key = "success14";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success15() throws Exception {
        String key = "success15";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void success16() throws Exception {
        String key = "success16";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        PolicyTypeResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void validateSentTimeEmpty() throws Exception {
        String key = "validateSentTimeEmpty";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validatePolicyEmpty() throws Exception {
        String key = "validatePolicyEmpty";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void cerNoIsEmpty() throws Exception {
        String key = "cerNoIsEmpty";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void policyNotFound() throws Exception {
        String key = "policyNotFound";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void policyIsSpace() throws Exception {
        String key = "policyIsSpace";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void MsgIdIsOverSize() throws Exception {
        String key = "MsgIdIsOverSize";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void policyNoOverSize() throws Exception {
        String key = "policyNoOverSize";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void certNoOversize() throws Exception {
        String key = "certNoOversize";
        ResponseMessages<PolicyTypeResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }

    private ResponseMessages<PolicyTypeResponse> getResponse(String key) throws Exception {
        String contactFile = "request/pol17.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<PolicyTypeRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<PolicyTypeRequest>>() {});
        return getPolicyTypeController.service(req).getBody();
    }

    private void expect(
            ResponseMessages<PolicyTypeResponse> resp, PolicyTypeResponse expect, String errorCode)
            throws JsonProcessingException {
        switch (errorCode) {
            case "E": // validateError
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
                break;
            case "S": // success
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                Assertions.assertEquals(resp.getResponseRecord().getType(),expect.getType());
                Assertions.assertEquals(resp.getResponseRecord().getScreentype(),expect.getScreentype());
                break;
            case "N" :
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                break;
        }
    }

    private PolicyTypeResponse getExpectResult(String key) throws JsonProcessingException {
        String contactFile = "expectResult/pol17.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        return mapper.readValue(json, new TypeReference<PolicyTypeResponse>() {});
    }

}
