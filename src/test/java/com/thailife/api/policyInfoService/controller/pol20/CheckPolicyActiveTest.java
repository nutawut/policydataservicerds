package com.thailife.api.policyInfoService.controller.pol20;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.TestUtils;
import com.thailife.api.policyInfoService.model.request.CheckPolicyActiveRequest;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.response.PolicyDetailOLResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CheckPolicyActiveTest {

    @Autowired CheckPolicyActiveController checkPolicyActiveController;

    private String susscess = "S";
    private String notFound = "E";
    private String errorValidate = "V";
    private final ObjectMapper mapper = JsonUtils.mapper;

    @Test
    public void successOloStatusA() throws Exception {
        String key = "successOlOStatusA";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }

    @Test
    public void successOlOStatusB() throws Exception {
        String key = "successOlOStatusB";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }

    @Test
    public void successOloStatusE() throws Exception {
        String key = "successOlOStatusE";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlOStatusF() throws Exception {
        String key = "successOlOStatusF";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlOStatusJ() throws Exception {
        String key = "successOlOStatusJ";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlOStatusN() throws Exception {
        String key = "successOlOStatusN";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlOStatusR() throws Exception {
        String key = "successOlOStatusR";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlOStatusU() throws Exception {
        String key = "successOlOStatusI";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlOStatusW() throws Exception {
        String key = "successOlOStatusW";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlIStatusW() throws Exception {
        String key = "successOlIStatusW";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOlWStatusR() throws Exception {
        String key = "successOlWStatusR";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOLTakafulStatusI() throws Exception {
        String key = "successOLTakafulStatusI";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successOLPAStatusI() throws Exception {
        String key = "successOLPAStatusI";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successULStatusH() throws Exception {
        String key = "successULStatusH";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successULStatusI() throws Exception {
        String key = "successULStatusI";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successUKStatusH() throws Exception {
        String key = "successUKStatusH";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }

    @Test
    public void successUKStatusI() throws Exception {
        String key = "successUKStatusI";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successCLStatusL() throws Exception {
        String key = "successCLStatusL";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }

    @Test
    public void successCLStatusN() throws Exception {
        String key = "successCLStatusL";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successCLStatusT() throws Exception {
        String key = "successCLStatusL";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successCLTakafulStatusI() throws Exception {
        String key = "successCLTakafulStatusI";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void successMultiPOlStatus1() throws Exception {
        String key = "successMultiPOlStatus1";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, susscess);
    }
    @Test
    public void failOLOStatusC() throws Exception {
        String key = "failOLOStatusC";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusD() throws Exception {
        String key = "failOLOStatusD";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusG() throws Exception {
        String key = "failOLOStatusG";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusK() throws Exception {
        String key = "failOLOStatusK";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusL() throws Exception {
        String key = "failOLOStatusL";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusM() throws Exception {
        String key = "failOLOStatusM";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusO() throws Exception {
        String key = "failOLOStatusO";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusS() throws Exception {
        String key = "failOLOStatusS";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusT() throws Exception {
        String key = "failOLOStatusT";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusX() throws Exception {
        String key = "failOLOStatusX";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusY() throws Exception {
        String key = "failOLOStatusY";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatusZ() throws Exception {
        String key = "failOLOStatusZ";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatus01() throws Exception {
        //status #
        String key = "failOLOStatus01";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failOLOStatus02() throws Exception {
        //status *
        String key = "failOLOStatus02";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }

    @Test
    public void failULStatusD() throws Exception {
        String key = "failULStatusD";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failULStatusL() throws Exception {
        String key = "failULStatusL";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failULStatusS() throws Exception {
        String key = "failULStatusS";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failULStatusX() throws Exception {
        String key = "failULStatusX";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failULStatus01() throws Exception {
        //status #
        String key = "failULStatus01";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failUKStatusD() throws Exception {
        String key = "failUKStatusD";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failUKStatusL() throws Exception {
        String key = "failUKStatusL";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failUKStatusS() throws Exception {
        String key = "failUKStatusS";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failUKStatusX() throws Exception {
        String key = "failUKStatusX";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failCLStatusC() throws Exception {
        String key = "failCLStatusC";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failCLStatusD() throws Exception {
        String key = "failCLStatusD";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failCLStatusL() throws Exception {
        String key = "failCLStatusL";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failCLStatusS() throws Exception {
        String key = "failCLStatusS";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void failCLStatusV() throws Exception {
        String key = "failCLStatusV";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }

    @Test
    public void failCLStatusW() throws Exception {
        String key = "failCLStatusW";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, notFound);
    }
    @Test
    public void validateMsgIsNull() throws Exception {
        String key = "validateMsgIsNull";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateSendDateIsEmpty() throws Exception {
        String key = "validateSendDateIsEmpty";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateCitizenIsEmpty() throws Exception {
        String key = "validateCitizenIsEmpty";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateCitizenNotFoundDB() throws Exception {
        String key = "validateCitizenNotFoundDB";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateCitizenChar01() throws Exception {
        // 88602900111!@
        String key = "validateCitizenChar01";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateCitizenIsCharTH() throws Exception {
        String key = "validateCitizenIsCharTH";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateCitizenIsSpace() throws Exception {
        String key = "validateCitizenIsSpace";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateMsgIDLength50() throws Exception {
        String key = "validateMsgIDLengthMore50";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateHeadIsNull() throws Exception {
        String key = "validateHeadIsNull";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }
    @Test
    public void validateRecordIsnull() throws Exception {
        String key = "validateRecordIsnull";
        ResponseMessages<?> resp = checkOlicyActive(key);
        expect(resp, errorValidate);
    }

    private ResponseMessages<?> checkOlicyActive(String key)
            throws Exception {
        String contactFile = "request/pol20.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        DataServiceRequestT<CheckPolicyActiveRequest> req =
                mapper.readValue(json, new TypeReference<DataServiceRequestT<CheckPolicyActiveRequest>>() {});
        return (ResponseMessages<?>) checkPolicyActiveController.controller(req).getBody();
    }

    private void expect(ResponseMessages<?> resp, String expect)
            throws JsonProcessingException {
        ResponseStatus responseStatus = resp.getResponseStatus();
        if(expect.equals("S")){
            Assertions.assertEquals(responseStatus.getStatusCode(), "S");
            Assertions.assertEquals(responseStatus.getErrorCode(), "200");
            Assertions.assertEquals(responseStatus.getErrorMessage(), "ดำเนินการเรียบร้อย");
        }else if(expect.equals("E")){
            Assertions.assertEquals(responseStatus.getStatusCode(), "E");
            Assertions.assertEquals(responseStatus.getErrorCode(), "204");
            Assertions.assertEquals(responseStatus.getErrorMessage(), "ไม่พบข้อมูลที่ต้องการในระบบ");
        }else if(expect.equals("V")){
            Assertions.assertEquals(responseStatus.getStatusCode(), "E");
            Assertions.assertEquals(responseStatus.getErrorCode(), "400");
            Assertions.assertEquals(responseStatus.getErrorMessage(), "การค้นหาไม่ถูกต้อง");
        }
;
    }




}
