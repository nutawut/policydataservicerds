package com.thailife.api.policyInfoService.controller.pol05;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
import com.thailife.api.policyInfoService.service.GetRiderDetialService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetRiderDetailValidate {
  @Autowired GetRiderDetialService riderDetialService;
  ServiceCommon<RiderDetialResponse> serviceCommon = new ServiceCommon<>();
  ResponseMessages<RiderDetialResponse> response;
  private final String fileName = "pol_05_02.json";
  private String key = "POL-05-";

  @Test
  public void กรณีไม่ส่งค่า_messageId() throws Exception {
    key = key + "016";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีไม่ส่งค่า_sentDateTime() throws Exception {
    key = key + "017";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีไม่ส่งค่า_Policyno() throws Exception {
    key = key + "018";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีไม่ส่งค่า_type() throws Exception {
    key = key + "019";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีไม่ส่งค่า_certno_CL() throws Exception {
    key = key + "020";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี่ส่งค่า_Policyno_ที่ไม่มีในระบบ() throws Exception {
    key = key + "021";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีส่งค่า_certno_ที่ไม่มีในระบบ_CL() throws Exception {
    key = key + "022";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีส่งค่า_Type_ที่ไม่มีในระบบ() throws Exception {
    key = key + "023";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_Policyno_ไม่_match_กับ_Type() throws Exception {
    key = key + "024";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_Policyno_ไม่_macth_กับ_certno_CL() throws Exception {
    key = key + "025";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  protected void expected(String key, ResponseMessages<RiderDetialResponse> response)
      throws Exception {
    ResponseMessages<RiderDetialResponse> responseJson = getRespJson(key, fileName);
    serviceCommon.serviceCommon(response, responseJson);
    Assertions.assertNull(
        response.getResponseRecord(), "responseRecord: " + response.getResponseRecord());
  }

  public ResponseMessages<RiderDetialResponse> getRespJson(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<RiderDetialResponse>>() {});
  }

  private RequestMessages<RiderDetailRequest> getRequest(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "request/" + fileName),
        new TypeReference<RequestMessages<RiderDetailRequest>>() {});
  }
}
