package com.thailife.api.policyInfoService.controller.pol01;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.TestUtils;

import com.thailife.api.policyInfoService.model.PolicyM;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByCustomerIdResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SearchPolicyByCustomerIdControllerTest {
  @Autowired
  private SearchPolicyByCustomerIdController searchPolicyByCustomerIdController;
  private final ObjectMapper mapper = JsonUtils.mapper;

  @Test
  public void nowHighDate2ORDStatusA() throws Exception {
    String key = "nowHighDate2ORDStatusA";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2ORDStatusE() throws Exception {
    String key = "nowHighDate2ORDStatusE";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2ORDStatusR() throws Exception {
    String key = "nowHighDate2ORDStatusR";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2ORDStatusU() throws Exception {
    String key = "nowHighDate2ORDStatusU";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2INDStatusR() throws Exception {
    String key = "nowHighDate2INDStatusR";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowHighDate2WHLStatusR() throws Exception {
    String key = "nowHighDate2WHLStatusR";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusA() throws Exception {
    String key = "nowLessDate2ORDStatusA";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusE() throws Exception {
    String key = "nowLessDate2ORDStatusE";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusR() throws Exception {
    String key = "nowLessDate2ORDStatusR";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2ORDStatusU() throws Exception {
    String key = "nowLessDate2ORDStatusU";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2INDStatusR() throws Exception {
    String key = "nowLessDate2INDStatusR";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2WHLStatusR() throws Exception {
    String key = "nowLessDate2WHLStatusR";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void nowLessDate2TakafulStatusR() throws Exception {
    String key = "nowLessDate2WHLStatusR";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.S, key);
  }

  @Test
  public void cusIdIsEmpty() throws Exception {
    String key = "cusIdIsEmpty";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.I);
  }

  @Test
  public void cusIdIsNotFound() throws Exception {
    String key = "cusIdIsNotFound";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.E);
  }

  @Test
  public void cusIdOverLength() throws Exception {
    String key = "cusIdOverLength";
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = searchPolicyByCusIdCommon(key);
    except(resp, Contains.I);
  }

  private ResponseMessages<SearchPolicyByCustomerIdResponse> searchPolicyByCusIdCommon(
      String key) throws Exception {
    String contactFile = "request/pol01.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    RequestMessages<SearchPolicyByCustomerIdRequest> request =
        mapper.readValue(
            json, new TypeReference<RequestMessages<SearchPolicyByCustomerIdRequest>>() {});
    return searchPolicyByCustomerIdController.service(request).getBody();
  }

  private void except(ResponseMessages<SearchPolicyByCustomerIdResponse> resp, String expect)
      throws JsonProcessingException {
    except(resp, expect, "");
  }

  private void except(
      ResponseMessages<SearchPolicyByCustomerIdResponse> resp, String expect, String key)
      throws JsonProcessingException {
    ResponseStatus responseStatus = resp.getResponseStatus();
    if (Contains.S.equals(expect)) {
      TestUtils.exceptSuccess(responseStatus);
      Assertions.assertNotNull(resp.getResponseRecord());
      verifyResult(resp.getResponseRecord().getList_of_policy(), getExpectResult(key));
    } else if (Contains.I.equals(expect)) {
      TestUtils.exceptIncorrectData(responseStatus);
    } else {
      TestUtils.exceptDataNotFound(responseStatus);
    }
  }


  private PolicyM getExpectResult(String key) throws JsonProcessingException {
    String contactFile = "expectResult/pol01.json";
    String json = JsonUtils.getJsonByKey(key, contactFile);
    Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
    return mapper.readValue(json, new TypeReference<PolicyM>() {});
  }

  private void verifyResult(List<PolicyM> resultList, PolicyM expect) {
    Assertions.assertNotNull(resultList, "list_of_policy Shouldn't NULL");
    Assertions.assertFalse(resultList.isEmpty(), "list_of_policy Shouldn't Empty");
    boolean isMatchPolicy = false;
    for (PolicyM result : resultList) {
      if (expect.getPolicyNo().equals(result.getPolicyNo())
          && expect.getCertNo().equals(result.getCertNo())) {
        Assertions.assertEquals(
            expect.getPlanCode(),
            result.getPlanCode(),
            "[PlanCode] : " + expect.getPlanCode() + " == " + result.getPlanCode());
        Assertions.assertEquals(
            expect.getPlanName(),
            result.getPlanName(),
            "[PlanName] : " + expect.getPlanName() + " == " + result.getPlanName());
        Assertions.assertEquals(
            expect.getType(),
            result.getType(),
            "[Type] : " + expect.getType() + " == " + result.getType());
        Assertions.assertEquals(
            expect.getScreenType(),
            result.getScreenType(),
            "[ScreenType] : " + expect.getScreenType() + " == " + result.getScreenType());
        Assertions.assertEquals(
            expect.getEffectiveDate(),
            result.getEffectiveDate(),
            "[EffectiveDate] : " + expect.getEffectiveDate() + " == " + result.getEffectiveDate());
        Assertions.assertEquals(
            expect.getMatureDate(),
            result.getMatureDate(),
            "[MatureDate] : " + expect.getMatureDate() + " == " + result.getMatureDate());
        Assertions.assertEquals(
            expect.getPolicyStatusDate1(),
            result.getPolicyStatusDate1(),
            "[PolicyStatusDate1] : "
                + expect.getPolicyStatusDate1()
                + " == "
                + result.getPolicyStatusDate1());
        Assertions.assertEquals(
            expect.getPolicyStatus1(),
            result.getPolicyStatus1(),
            "[PolicyStatus1] : " + expect.getPolicyStatus1() + " == " + result.getPolicyStatus1());
        Assertions.assertEquals(
            expect.getPolicyStatus1desc(),
            result.getPolicyStatus1desc(),
            "[PolicyStatus1Desc] : "
                + expect.getPolicyStatus1desc()
                + " == "
                + result.getPolicyStatus1desc());
        Assertions.assertEquals(
            expect.getStatusDisplay(),
            result.getStatusDisplay(),
            "[StatusDisplay] : " + expect.getStatusDisplay() + " == " + result.getStatusDisplay());
        Assertions.assertEquals(
            expect.getSum(),
            result.getSum(),
            "[Sum] : " + expect.getSum() + " == " + result.getSum());
        Assertions.assertEquals(
            expect.getPreName(),
            result.getPreName(),
            "[PreName] : " + expect.getPreName() + " == " + result.getPreName());
        Assertions.assertEquals(
            expect.getFirstname(),
            result.getFirstname(),
            "[FirstName] : " + expect.getFirstname() + " == " + result.getFirstname());
        Assertions.assertEquals(
            expect.getLastname(),
            result.getLastname(),
            "[LastName] : " + expect.getLastname() + " == " + result.getLastname());
        Assertions.assertEquals(
            expect.getNextDueDate(),
            result.getNextDueDate(),
            "[NextDueDate] : " + expect.getNextDueDate() + " == " + result.getNextDueDate());
        Assertions.assertEquals(
            expect.getRiderSum(),
            result.getRiderSum(),
            "[RiderSum] : " + expect.getRiderSum() + " == " + result.getRiderSum());
        Assertions.assertEquals(
            expect.getOldCertNo(),
            result.getOldCertNo(),
            "[OldCertNo] : " + expect.getOldCertNo() + " == " + result.getOldCertNo());
        Assertions.assertEquals(
            expect.getOldPolicyNo(),
            result.getOldPolicyNo(),
            "[OldPolicyNo] : " + expect.getOldPolicyNo() + " == " + result.getOldPolicyNo());
        Assertions.assertEquals(
            expect.getPayPeriod(),
            result.getPayPeriod(),
            "[PayPeriod] : " + expect.getPayPeriod() + " == " + result.getPayPeriod());
        Assertions.assertEquals(
            expect.getGroupPolicyStatus(),
            result.getGroupPolicyStatus(),
            "[GroupPolicyStatus] : "
                + expect.getGroupPolicyStatus()
                + " == "
                + result.getGroupPolicyStatus());
        isMatchPolicy = true;
      }
    }
    Assertions.assertTrue(isMatchPolicy, "Policy is matcher");
  }
}
