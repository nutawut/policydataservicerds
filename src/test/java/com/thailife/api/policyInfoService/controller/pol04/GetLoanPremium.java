package com.thailife.api.policyInfoService.controller.pol04;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.request.GetLoanPremumRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetLoanPremumResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.service.GetLoanPremiumService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetLoanPremium {
  @Autowired GetLoanPremiumService getLoanPremiumService;
  ServiceCommon<GetLoanPremumResponse> serviceCommon = new ServiceCommon<>();
  private final String fileName = "pol_04_01.json";
  private String key = "POL-04-";

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินกู้_ORD() throws Exception {
    key = key + "001";
    RequestMessages<GetLoanPremumRequest> request = getRequest();
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินกู้_IND() throws Exception {
    key = key + "002";
    RequestMessages<GetLoanPremumRequest> request = getRequest();
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินกู้_WHL() throws Exception {
    key = key + "003";
    RequestMessages<GetLoanPremumRequest> request = getRequest();
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_ค้นหาข้อมูลได้สำเร็จ_กรมธรรม์ที่มีเงินกู้() throws Exception {
    key = key + "004";
    RequestMessages<GetLoanPremumRequest> request = getRequest();
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  protected void expected(String key, ResponseMessages<GetLoanPremumResponse> response)
      throws Exception {
    ResponseMessages<GetLoanPremumResponse> responseJson = getRespJson(key, "pol_04_01.json");
    serviceCommon.serviceCommon(response, responseJson);
    Assertions.assertNotNull(
        response.getResponseRecord(), "response: " + response.getResponseRecord());
    expectedField(responseJson, response);
  }

  protected void expectedField(
      ResponseMessages<GetLoanPremumResponse> expected,
      ResponseMessages<GetLoanPremumResponse> actual) {
    GetLoanPremumResponse exp, act;
    exp = expected.getResponseRecord();
    act = actual.getResponseRecord();
    Assertions.assertEquals(exp.getCalculatedate(), act.getCalculatedate(), "CalculateDate");
    Assertions.assertEquals(exp.getComloanamount(), act.getComloanamount(), "ComloanAmount");
    Assertions.assertEquals(exp.getDuty(), act.getDuty(), "getDuty");
    Assertions.assertEquals(exp.getInterest(), act.getInterest(), "getInterest");
    Assertions.assertEquals(exp.getInterestrate(), act.getInterestrate(), "getInterestrate");
    Assertions.assertEquals(
        exp.getGrossloanamount(), act.getGrossloanamount(), "getGrossloanamount");
    Assertions.assertEquals(exp.getIntfromdate(), act.getIntfromdate(), "getIntfromdate");
    Assertions.assertEquals(exp.getIntstatus(), act.getIntstatus(), "getIntstatus");
    Assertions.assertEquals(exp.getLoanamount(), act.getLoanamount(), "getLoanamount");
    Assertions.assertEquals(exp.getInttodate(), act.getInttodate(), "getInttodate");
    Assertions.assertEquals(exp.getLoandate(), act.getLoandate(), "getLoandate");
    Assertions.assertEquals(exp.getLoanyear(), act.getLoanyear(), "getLoanyear");
    Assertions.assertEquals(exp.getPayloanamount(), act.getPayloanamount(), "getPayloanamount");
    Assertions.assertEquals(exp.getTotalloan(), act.getTotalloan(), "getTotalloan");
    Assertions.assertEquals(exp.getTotalloanall(), act.getTotalloanall(), "getTotalloanall");
  }

  public ResponseMessages<GetLoanPremumResponse> getRespJson(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<GetLoanPremumResponse>>() {});
  }

  private RequestMessages<GetLoanPremumRequest> getRequest() throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "request/" + fileName),
        new TypeReference<RequestMessages<GetLoanPremumRequest>>() {});
  }
}
