package com.thailife.api.policyInfoService.controller.pol14;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchConsentRequest;
import com.thailife.api.policyInfoService.model.response.GetPolicyDueDateResponse;
import com.thailife.api.policyInfoService.model.response.PolicyM;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SearchConsentResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
public class SearchConsenTest {

    private final ObjectMapper mapper = JsonUtils.mapper;

    @Autowired SearchConsentController searchConsentController;

    @Test
    public void successFlagY() throws Exception {
        String key = "successFlagY";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagN() throws Exception {
        String key = "successFlagN";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagYPolicyOl_O() throws Exception {
        String key = "successFlagYPolicyOl_O";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagYPolicyOl_W() throws Exception {
        String key = "successFlagYPolicyOl_W";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagYPolicyOl_PA() throws Exception {
        String key = "successFlagYPolicyOl_PA";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagYPolicyUl() throws Exception {
        String key = "successFlagYPolicyUl";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagYPolicyCl() throws Exception {
        String key = "successFlagYPolicyCl";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagYPolicyCl_takaful() throws Exception {
        String key = "successFlagYPolicyCl_takaful";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyOL_O() throws Exception {
        String key = "successFlagNPolicyOL_O";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyOL_W() throws Exception {
        String key = "successFlagNPolicyOL_W";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyOL_takaful() throws Exception {
        String key = "successFlagNPolicyOL_takaful";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyOL_PA() throws Exception {
        String key = "successFlagNPolicyOL_PA";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyUL() throws Exception {
        String key = "successFlagNPolicyUL";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyCL() throws Exception {
        String key = "successFlagNPolicyCL";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyCL_takaful() throws Exception {
        String key = "successFlagNPolicyCL_takaful";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successFlagNPolicyUlip() throws Exception {
        String key = "successFlagNPolicyUlip";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        SearchConsentResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void validateCusIdIsEmpty() throws Exception {
        String key = "validateCusIdIsEmpty";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void custIDNotFound() throws Exception {
        String key = "custIDNotFound";
        ResponseMessages<SearchConsentResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
    }

    private ResponseMessages<SearchConsentResponse> getResponse(String key) throws Exception {
        String contactFile = "request/pol14.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<SearchConsentRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<SearchConsentRequest>>() {});
        return searchConsentController.service(req).getBody();
    }

    private void expect(
            ResponseMessages<SearchConsentResponse> resp, SearchConsentResponse expect, String errorCode)
            throws JsonProcessingException {
        switch (errorCode) {
            case "E": // validateError
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
                Assertions.assertEquals(resp.getResponseStatus().getErrorMessage(), "การค้นหาไม่ถูกต้อง");
                break;
            case "S": // success
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                Assertions.assertEquals(resp.getResponseRecord().getPolicy().size(), expect.getPolicy().size());
                Assertions.assertEquals(resp.getResponseRecord().getCustomer(), expect.getCustomer());
                List<PolicyM> respPol = resp.getResponseRecord().getPolicy();
                List<PolicyM> assertPol = expect.getPolicy();
                Collections.sort(assertPol, new Comparator<PolicyM>() {
                    public int compare(PolicyM o1, PolicyM o2) {
                        return o2.getPolicyNo().compareTo(o1.getPolicyNo());
                    }
                });
                Collections.sort(respPol, new Comparator<PolicyM>() {
                    public int compare(PolicyM o1, PolicyM o2) {
                        return o2.getPolicyNo().compareTo(o1.getPolicyNo());
                    }
                });
//                Assertions.assertEquals(resp.getResponseRecord(), expect);
                break;
            case "N" :
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                break;
        }
    }

    private SearchConsentResponse getExpectResult(String key) throws JsonProcessingException {
        String contactFile = "expectResult/pol14.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        return mapper.readValue(json, new TypeReference<SearchConsentResponse>() {});
    }
}
