package com.thailife.api.policyInfoService.controller.pol05;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.RiderM;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
import com.thailife.api.policyInfoService.service.GetRiderDetialService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetRiderDetailCheckValue {
  @Autowired GetRiderDetialService riderDetialService;
  ServiceCommon<RiderDetialResponse> serviceCommon = new ServiceCommon<>();
  ResponseMessages<RiderDetialResponse> response;
  private final String fileName = "pol_05_03.json";
  private String key = "POL-05-";

  @Test
  public void กรณี_Policy_type_OL_O() throws Exception {
    key = key + "026";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_Policy_type_OL_I() throws Exception {
    key = key + "027";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_Policy_type_CL() throws Exception {
    key = key + "028";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void
      สถานะกรมธรรม์หลัก_เท่ากับ_F_ให้ดูว่า_Nextduedate_Rider_บวก31_น้อยกว่า_currentDate_ส่งสถานะ_rider_เป็น_A()
          throws Exception {
    key = key + "029";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  protected void expected(String key, ResponseMessages<RiderDetialResponse> response)
      throws Exception {
    ResponseMessages<RiderDetialResponse> responseJson = getRespJson(key, fileName);
    serviceCommon.serviceCommon(response, responseJson);
    Assertions.assertNotNull(
        response.getResponseRecord(), "response: " + response.getResponseRecord());
    Assertions.assertNotNull(
        response.getResponseRecord().getList_of_rider(),
        "List_of_rider: " + response.getResponseRecord().getList_of_rider());
    expectedField(key, responseJson, response);
  }

  protected void expectedField(
      String key,
      ResponseMessages<RiderDetialResponse> expected,
      ResponseMessages<RiderDetialResponse> actual) {
    RiderDetialResponse exp, act;
    exp = expected.getResponseRecord();
    act = actual.getResponseRecord();
    int expSize, actSize;
    expSize = exp.getList_of_rider().size();
    actSize = act.getList_of_rider().size();
    Assertions.assertEquals(
        expSize, actSize, String.format("size ==> exp: %d, act: %d ", expSize, actSize));
    boolean notFound = true;
    for (RiderM tmpAct : act.getList_of_rider()) {
      for (RiderM tmpExp : exp.getList_of_rider()) {
        if (tmpAct.getRidercode().equals(tmpExp.getRidercode())) {
          notFound = false;
          Assertions.assertEquals(
              tmpAct.getRidercode(), tmpExp.getRidercode(), "getRidercode [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRidername(), tmpExp.getRidername(), "getRidername [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRidersortname(),
              tmpExp.getRidersortname(),
              "getRidersortname [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRidersum(), tmpExp.getRidersum(), "getRidersum [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderpremium(), tmpExp.getRiderpremium(), "getRiderpremium [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderstatus(), tmpExp.getRiderstatus(), "getRiderstatus [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderstatusdesc(),
              tmpExp.getRiderstatusdesc(),
              "getRiderstatusdesc [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderstatusdate(),
              tmpExp.getRiderstatusdate(),
              "getRiderstatusdate [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getStatusdisplay(),
              tmpExp.getStatusdisplay(),
              "getStatusdisplay [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getEffectivedate(),
              tmpExp.getEffectivedate(),
              "getEffectivedate [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getExtrariderpremium(),
              tmpExp.getExtrariderpremium(),
              "getExtrariderpremium [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getLabel_status(), tmpExp.getLabel_status(), "getLabel_status [" + key + "]");
        }
      }
      Assertions.assertFalse(notFound, " not found rider. ");
      notFound = true;
    }
  }

  public ResponseMessages<RiderDetialResponse> getRespJson(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<RiderDetialResponse>>() {});
  }

  private RequestMessages<RiderDetailRequest> getRequest(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "request/" + fileName),
        new TypeReference<RequestMessages<RiderDetailRequest>>() {});
  }
}
