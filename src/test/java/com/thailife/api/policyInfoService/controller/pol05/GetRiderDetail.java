package com.thailife.api.policyInfoService.controller.pol05;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.RiderM;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
import com.thailife.api.policyInfoService.service.GetRiderDetialService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetRiderDetail {
  @Autowired GetRiderDetialService riderDetialService;
  ServiceCommon<RiderDetialResponse> serviceCommon = new ServiceCommon<>();
  ResponseMessages<RiderDetialResponse> response;
  private final String fileName = "pol_05_01.json";
  private String key = "POL_05_";

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_OL_I_Rider_อุบัติเหตุ()
      throws Exception {
    key = key + "002";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_OL_O_Rider_อุบัติเหตุ()
      throws Exception {
    key = key + "003";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_OL_O_Rider_สุขภาพุ()
      throws Exception {
    key = key + "004";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_OL_UL_Rider_สุขภาพุ()
      throws Exception {
    key = key + "005";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_OL_UL_Rider_อุบัติเหตุ()
      throws Exception {
    key = key + "006";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void
      ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_OL_Takaful_Rider_สุขภาพ_กรณีทุพพลภาพถาวรสิ้นเชิง_ตทพุ()
          throws Exception {
    key = key + "007";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void
      ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_OL_Takaful_Rider_อุบัติเหต_ตะกาฟุลอุบัติเหตุ_การเสียชีวิต_สูญเสียอวัยวะ_และทุพพลภาพ_ตอ2ุ()
          throws Exception {
    key = key + "008";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_CL_Rider_อุบัติเหตุ()
      throws Exception {
    key = key + "010";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void
      ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_CL_Takaful_Rider_อุบัติเหตุ()
          throws Exception {
    key = key + "011";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_ULIP_Rider_สุขภาพุ()
      throws Exception {
    key = key + "012";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_Success_Sale_Chanel_Agent_Type_ULIP_Rider_อุบัติเหตุ()
      throws Exception {
    key = key + "013";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_โดยเป็น_Policy_ที่ไม่มี_Riderุ() throws Exception {
    key = key + "014";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  @Test
  public void ตรวจสอบกรณี_Get_Rider_Detial_โดยเป็น_Policy_ที่มี_เบี้ยเพิ่มุ() throws Exception {
    key = key + "015";
    RequestMessages<RiderDetailRequest> request = getRequest(key, fileName);
    response = riderDetialService.service(request);
    expected(key, response);
  }

  protected void expected(String key, ResponseMessages<RiderDetialResponse> response)
      throws Exception {
    ResponseMessages<RiderDetialResponse> responseJson = getRespJson(key, fileName);
    serviceCommon.serviceCommon(response, responseJson);
    Assertions.assertNotNull(
        response.getResponseRecord(), "response: " + response.getResponseRecord());
    Assertions.assertNotNull(
        response.getResponseRecord().getList_of_rider(),
        "List_of_rider: " + response.getResponseRecord().getList_of_rider());
    expectedField(key, responseJson, response);
  }

  protected void expectedField(
      String key,
      ResponseMessages<RiderDetialResponse> expected,
      ResponseMessages<RiderDetialResponse> actual) {
    RiderDetialResponse exp, act;
    exp = expected.getResponseRecord();
    act = actual.getResponseRecord();
    int expSize, actSize;
    expSize = exp.getList_of_rider().size();
    actSize = act.getList_of_rider().size();
    Assertions.assertEquals(
        expSize, actSize, String.format("size ==> exp: %d, act: %d ", expSize, actSize));
    boolean notFound = true;
    for (RiderM tmpAct : act.getList_of_rider()) {
      for (RiderM tmpExp : exp.getList_of_rider()) {
        if (tmpAct.getRidercode().equals(tmpExp.getRidercode())) {
          notFound = false;
          Assertions.assertEquals(
              tmpAct.getRidercode(), tmpExp.getRidercode(), "getRidercode [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRidername(), tmpExp.getRidername(), "getRidername [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRidersortname(),
              tmpExp.getRidersortname(),
              "getRidersortname [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRidersum(), tmpExp.getRidersum(), "getRidersum [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderpremium(), tmpExp.getRiderpremium(), "getRiderpremium [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderstatus(), tmpExp.getRiderstatus(), "getRiderstatus [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderstatusdesc(),
              tmpExp.getRiderstatusdesc(),
              "getRiderstatusdesc [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getRiderstatusdate(),
              tmpExp.getRiderstatusdate(),
              "getRiderstatusdate [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getStatusdisplay(),
              tmpExp.getStatusdisplay(),
              "getStatusdisplay [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getEffectivedate(),
              tmpExp.getEffectivedate(),
              "getEffectivedate [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getExtrariderpremium(),
              tmpExp.getExtrariderpremium(),
              "getExtrariderpremium [" + key + "]");
          Assertions.assertEquals(
              tmpAct.getLabel_status(), tmpExp.getLabel_status(), "getLabel_status [" + key + "]");
        }
      }
      Assertions.assertFalse(notFound, " not found rider. ");
      notFound = true;
    }
  }

  public ResponseMessages<RiderDetialResponse> getRespJson(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<RiderDetialResponse>>() {});
  }

  private RequestMessages<RiderDetailRequest> getRequest(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "request/" + fileName),
        new TypeReference<RequestMessages<RiderDetailRequest>>() {});
  }
}
