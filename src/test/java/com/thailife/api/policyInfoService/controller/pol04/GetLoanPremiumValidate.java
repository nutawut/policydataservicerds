package com.thailife.api.policyInfoService.controller.pol04;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.Utils.ServiceCommon;
import com.thailife.api.policyInfoService.model.request.GetLoanPremumRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetLoanPremumResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.service.GetLoanPremiumService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GetLoanPremiumValidate {
  @Autowired GetLoanPremiumService getLoanPremiumService;
  ServiceCommon<GetLoanPremumResponse> serviceCommon = new ServiceCommon<>();
  private final String fileName = "pol_04_02.json";
  private String key = "POL-04-";

  @Test
  public void กรณี_messageId_เป็นค่าว่าง() throws Exception {
    key = key + "022";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_sentDateTime_เป็นค่าว่าง() throws Exception {
    key = key + "023";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_policyno_เป็นค่าว่าง() throws Exception {
    key = key + "024";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_type_เป็นค่าว่าง() throws Exception {
    key = key + "026";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_policyno_และ_type_ไม่เข้าคู่กัน() throws Exception {
    key = key + "028";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_policyno_ไม่มีอยู่ในฐานข้อมูล00() throws Exception {
    key = key + "030";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_type_ไม่มีอยู่ในฐานข้อมูล() throws Exception {
    key = key + "032";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีไม่มีเงินกู้เดิม() throws Exception {
    key = key + "033";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_Request_policy_ที่ไม่สามารถกู้เงินได้() throws Exception {
    key = key + "034";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีไม่ส่ง_headerData() throws Exception {
    key = key + "036";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณีไม่ส่ง_requestRecord() throws Exception {
    key = key + "037";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_messageId_มากกว่า_Length_50() throws Exception {
    key = key + "038";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_policyno_มากกว่า_length_8() throws Exception {
    key = key + "039";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_certno_มากกว่า_length_8() throws Exception {
    key = key + "040";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_type_มากกว่า_length_30() throws Exception {
    key = key + "041";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  @Test
  public void กรณี_policyno_ไม่มีอยู่ในฐานข้อมูล01() throws Exception {
    key = key + "042";
    RequestMessages<GetLoanPremumRequest> request = getRequest(key);
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    expected(key, response);
  }

  public ResponseMessages<GetLoanPremumResponse> getRespJson(String key, String fileName)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "expectResult/" + fileName),
        new TypeReference<ResponseMessages<GetLoanPremumResponse>>() {});
  }

  private RequestMessages<GetLoanPremumRequest> getRequest(String key)
      throws JsonProcessingException {
    return JsonUtils.mapper.readValue(
        serviceCommon.getJsonString(key, "request/" + fileName),
        new TypeReference<RequestMessages<GetLoanPremumRequest>>() {});
  }

  protected void expected(String key, ResponseMessages<GetLoanPremumResponse> response)
      throws Exception {
    ResponseMessages<GetLoanPremumResponse> responseJson = getRespJson(key, fileName);
    serviceCommon.serviceCommon(response, responseJson);
    Assertions.assertNotNull(
        response.getResponseRecord(),
        String.format("key: %s, response: %s", key, response.getResponseRecord()));
    expectedField(key, responseJson, response);
  }

  protected void expectedField(
      String key,
      ResponseMessages<GetLoanPremumResponse> expected,
      ResponseMessages<GetLoanPremumResponse> actual) {
    GetLoanPremumResponse exp, act;
    exp = expected.getResponseRecord();
    act = actual.getResponseRecord();
    Assertions.assertEquals(
        exp.getCalculatedate(), act.getCalculatedate(), "CalculateDate[" + key + "]");
    Assertions.assertEquals(
        exp.getComloanamount(), act.getComloanamount(), "ComloanAmount[" + key + "]");
    Assertions.assertEquals(exp.getDuty(), act.getDuty(), "getDuty[" + key + "]");
    Assertions.assertEquals(exp.getInterest(), act.getInterest(), "getInterest[" + key + "]");
    Assertions.assertEquals(
        exp.getInterestrate(), act.getInterestrate(), "getInterestrate[" + key + "]");
    Assertions.assertEquals(
        exp.getGrossloanamount(), act.getGrossloanamount(), "getGrossloanamount[" + key + "]");
    Assertions.assertEquals(
        exp.getIntfromdate(), act.getIntfromdate(), "getIntfromdate[" + key + "]");
    Assertions.assertEquals(exp.getIntstatus(), act.getIntstatus(), "getIntstatus[" + key + "]");
    Assertions.assertEquals(exp.getLoanamount(), act.getLoanamount(), "getLoanamount[" + key + "]");
    Assertions.assertEquals(exp.getInttodate(), act.getInttodate(), "getInttodate[" + key + "]");
    Assertions.assertEquals(exp.getLoandate(), act.getLoandate(), "getLoandate[" + key + "]");
    Assertions.assertEquals(exp.getLoanyear(), act.getLoanyear(), "getLoanyear[" + key + "]");
    Assertions.assertEquals(
        exp.getPayloanamount(), act.getPayloanamount(), "getPayloanamount[" + key + "]");
    Assertions.assertEquals(exp.getTotalloan(), act.getTotalloan(), "getTotalloan[" + key + "]");
    Assertions.assertEquals(
        exp.getTotalloanall(), act.getTotalloanall(), "getTotalloanall[" + key + "]");
  }
}
