package com.thailife.api.policyInfoService.controller.pol13;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.Utils.Contains;
import com.thailife.api.policyInfoService.Utils.JsonUtils;
import com.thailife.api.policyInfoService.model.CurrentAVM;
import com.thailife.api.policyInfoService.model.request.CurrentAVInfoRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.CurrentAVInfoResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
public class CurrentAVInfoTest {
    @Autowired CurrentAVInfoController currentAVInfoController;

    private final ObjectMapper mapper = JsonUtils.mapper;

    @Test
    public void successUlipList1() throws Exception {
        String key = "successUlipList1";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        CurrentAVInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successUlipList2() throws Exception {
        String key = "successUlipList2";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        CurrentAVInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successUlipList3() throws Exception {
        String key = "successUlipList3";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        CurrentAVInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void successUlipList4() throws Exception {
        String key = "successUlipList4";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        CurrentAVInfoResponse expecResult =  getExpectResult(key);
        expect(resp, expecResult, Contains.S);
    }
    @Test
    public void validateMsgIsEmpty() throws Exception {
        String key = "validateMsgIsEmpty";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void sendTimeIsEmpty() throws Exception {
        String key = "sendTimeIsEmpty";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validatePolicyNoIsEmpty() throws Exception {
        String key = "validatePolicyNoIsEmpty";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateTypeIsEmpty() throws Exception {
        String key = "validateTypeIsEmpty";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateMsgIdIsOverLength() throws Exception {
        String key = "validateMsgIdIsOverLength";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validatePolicyNoIsOverLength() throws Exception {
        String key = "validatePolicyNoIsOverLength";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void validateTypeIsOverLength() throws Exception {
        String key = "validateTypeIsOverLength";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void policyNotFoundDB() throws Exception {
        String key = "policyNotFoundDB";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
    }
    @Test
    public void policyAndTypeNotMath() throws Exception {
        String key = "policyAndTypeNotMath";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
        //ของเก่าขึ้นไม่พบข้อมูล
    }
    @Test
    public void policyNoIsSpace() throws Exception {
        String key = "policyNoIsSpace";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void policyNoWrongFormat() throws Exception {
        String key = "policyNoWrongFormat";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.N);
        //ของเก่า 204
    }
//    @Test
//    public void typeIsSpace() throws Exception {
//          policy ของเก่าส่งได้
//        String key = "typeIsSpace";
//        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
//        expect(resp, null, Contains.E);
//    }
    @Test
    public void headerIsNull() throws Exception {
        String key = "headerIsNull";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }
    @Test
    public void requestRecordIsNull() throws Exception {
        String key = "requestRecordIsNull";
        ResponseMessages<CurrentAVInfoResponse> resp = getResponse(key);
        expect(resp, null, Contains.E);
    }

    private ResponseMessages<CurrentAVInfoResponse> getResponse(String key) throws Exception {
        String contactFile = "request/pol13.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        RequestMessages<CurrentAVInfoRequest> req =
                mapper.readValue(
                        json, new TypeReference<RequestMessages<CurrentAVInfoRequest>>() {});
        return currentAVInfoController.service(req).getBody();
    }

    private void expect(
            ResponseMessages<CurrentAVInfoResponse> resp, CurrentAVInfoResponse expect, String errorCode)
            throws JsonProcessingException {
        switch (errorCode) {
            case "E": // validateError
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "400");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "E");
                break;
            case "S": // success
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "200");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                List<CurrentAVM> respWs = resp.getResponseRecord().getCurrent_av_list();
                List<CurrentAVM> assertResp = expect.getCurrent_av_list();
                Collections.sort(respWs, new Comparator<CurrentAVM>() {
                    public int compare(CurrentAVM o1, CurrentAVM o2) {
                        return o2.getCode().compareTo(o1.getCode());
                    }
                });
                Collections.sort(assertResp, new Comparator<CurrentAVM>() {
                    public int compare(CurrentAVM o1, CurrentAVM o2) {
                        return o2.getCode().compareTo(o1.getCode());
                    }
                });
                Assertions.assertEquals(resp.getResponseRecord(), expect);
                break;
            case "N" :
                Assertions.assertEquals(resp.getResponseStatus().getErrorCode(), "204");
                Assertions.assertEquals(resp.getResponseStatus().getStatusCode(), "S");
                break;
        }
    }

    private CurrentAVInfoResponse getExpectResult(String key) throws JsonProcessingException {
        String contactFile = "expectResult/pol13.json";
        String json = JsonUtils.getJsonByKey(key, contactFile);
        Assertions.assertNotEquals(JsonUtils.keyNotFound, json, "Error key not found");
        return mapper.readValue(json, new TypeReference<CurrentAVInfoResponse>() {});
    }
}
