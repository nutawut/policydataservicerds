package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.thailife.api.policyInfoService.entity.DetcertEntity;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.DetcertDao;
import com.thailife.api.policyInfoService.mapper.DetcertMapper;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;

@Service
public class DetcertDaoImpl {
  private final Log log = new Log();
  private final String className = getClass().getName();

  public DetcertDao findByPolicynoAndCertno(String policyno, String certno) throws Exception {
    log.printDebug(className, "findByPolicynoAndCertno : " + policyno + "certno " + certno);
    DetcertDao result = new DetcertDao();
    try (Connection con =
        DBConnection.con(DBproperties.getDBPolicy(),"")) {
      String sql = " select * from mortgage.detcert  where policyno = ? and certno = ?";
      PreparedStatement pst = con.prepareStatement(sql);
      pst.setString(1, policyno);
      pst.setString(2, certno);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result = DetcertMapper.detcertMapper(rs);
      }
    } catch (Exception e) {
      log.printError(className, "findByPolicynoAndCertno error SQLException : " + e.getMessage());
      throw e;
    }

    return result;
  }
  public String getCodeByPolicynoAndCertno(String policyno, String certno, Connection con) {
    try{
      String query = "select code from mortgage.detcert where policyno = ? and certno = ? ";
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, policyno);
        pst.setString(2, certno);
        log.printInfo(className, "sql:" + pst);
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return rs.getString(DetcertEntity.code);
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "Error GetCodeByPolicynoAndCertno : " + e.getMessage());
      e.printStackTrace();
    }
    return null;
  }
}
