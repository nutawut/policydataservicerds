package com.thailife.api.policyInfoService.dao;

import java.sql.Timestamp;
import lombok.Data;

@Data
public class ContactPhoneDao   {

    private int phoneId;
    private String countryCode;
    private String phoneNumber;
    private String extension;
    private String createUserCode;
    private Timestamp createTime;
    private String updateUserCode;
    private Timestamp lastUpdate;
    private int systemId;
    private String systemKey;
}