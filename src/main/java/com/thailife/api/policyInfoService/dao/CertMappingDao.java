package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class CertMappingDao {
  private String policyno;
  private String certno;
  private String rpolicyno;
  private String rcertno;
  private Timestamp recordtimestamp;
  private Timestamp journaltimestamp;
}
