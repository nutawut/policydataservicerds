package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class UlRiderDao {
  private String policyno;
  private String ridertype;
  private String effectivedate;
  private Timestamp journaltimestamp;
  private String marker;
  private Timestamp recordtimestamp;
  private BigDecimal riderpremium;
  private String riderstatus;
  private String riderstatusdate;
  private BigDecimal ridersum;
}
