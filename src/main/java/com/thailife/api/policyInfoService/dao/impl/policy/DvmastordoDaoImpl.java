package com.thailife.api.policyInfoService.dao.impl.policy;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.DvmastordoDao;
import com.thailife.api.policyInfoService.mapper.DvmastordoMapper;
import com.thailife.api.policyInfoService.util.Log;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class DvmastordoDaoImpl {
  private final Log log = new Log();
  private final String className = getClass().getName();
  private final String scheme = "mstpolicy.";
  private final String table = "dvmastordo";

  public List<DvmastordoDao> findByPolicyNo(String policyNo) {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM ").append(scheme).append(table);
    sql.append(" WHERE policyno = ? ");
    log.printDebug(className, "sql: " + sql);
    try (Connection con = DBConnection.getConnectionPolicy()) {
      try(PreparedStatement pst = con.prepareStatement(sql.toString())){
        pst.setInt(1, Integer.parseInt(policyNo));
        try(ResultSet rs = pst.executeQuery()){
          List<DvmastordoDao> res = new ArrayList<>();
          while (rs.next()){
            res.add(new DvmastordoMapper().mapper(rs));
          }
          return res;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static void main(String[] args) {
    DvmastordoDaoImpl dao = new DvmastordoDaoImpl();
    System.out.println(dao.findByPolicyNo("04007890"));
    ;
  }
}
