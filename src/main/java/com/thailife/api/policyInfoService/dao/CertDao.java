package com.thailife.api.policyInfoService.dao;

import java.math.BigDecimal;
import java.security.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CertDao {
  private BigDecimal tpdpremium;
  private BigDecimal tpd;
  private String statdate2;
  private String statdate1;
  private String statcer2;
  private String statcer;
  private BigDecimal smi;
  private String rpno;
  private String reserve;
  private String reqdate;
  private String referenceno;
  private Date recordtimestamp;
  private String receiptflag;
  private String premiumdate;
  private String policyno;
  private String period;
  private String payperiod;
  private String paydate2;
  private String paydate1;

  @JsonProperty(value = "package")
  private String packAge;

  private String oldstatcertdate2;
  private String oldstatcertdate1;
  private String oldstatcert2;
  private String oldstatcert1;
  private String nameid;
  private String mode;
  private String maturedate;
  private String loan_term;
  private BigDecimal lifesum;
  private BigDecimal lifepremium;
  private Date journaltimestamp;
  private String informdate2;
  private String informdate1;
  private String familytype;
  private BigDecimal extratpdpremium;
  private BigDecimal extrapremium;
  private String endownmentyear;
  private BigDecimal em;
  private String effectivedate;
  private String duedate;
  private String deaddate;
  private String deadcause;
  private String certyyyymm;
  private String certno;
  private String appno;
  private BigDecimal amount2;
  private BigDecimal amount1;
  private int age;
  private BigDecimal addpremium;
  private BigDecimal accidentsum;
}
