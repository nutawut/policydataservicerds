package com.thailife.api.policyInfoService.dao;

import java.sql.Timestamp;
import lombok.Data;
@Data
public class NameDao {
  private String nameid;
  private String firstname;
  private Timestamp journaltimestamp;
  private String lastname;
  private String personid;
  private String prename;
  private Timestamp recordtimestamp;
  private String startdate;
}
