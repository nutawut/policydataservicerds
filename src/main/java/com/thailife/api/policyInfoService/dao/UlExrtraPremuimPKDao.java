package com.thailife.api.policyInfoService.dao;

import lombok.Data;

@Data
public class UlExrtraPremuimPKDao {
  private String policyno;
  private String extratype;
  private long startyear;

  public UlExrtraPremuimPKDao() {
  }
  public String getPolicyno() {
    return this.policyno;
  }
  public void setPolicyno(String policyno) {
    this.policyno = policyno;
  }
  public String getExtratype() {
    return this.extratype;
  }
  public void setExtratype(String extratype) {
    this.extratype = extratype;
  }
  public long getStartyear() {
    return this.startyear;
  }
  public void setStartyear(long startyear) {
    this.startyear = startyear;
  }

  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof UlExrtraPremuimPKDao)) {
      return false;
    }
    UlExrtraPremuimPKDao castOther = (UlExrtraPremuimPKDao)other;
    return
            this.policyno.equals(castOther.policyno)
                    && this.extratype.equals(castOther.extratype)
                    && (this.startyear == castOther.startyear);
  }

  public int hashCode() {
    final int prime = 31;
    int hash = 17;
    hash = hash * prime + this.policyno.hashCode();
    hash = hash * prime + this.extratype.hashCode();
    hash = hash * prime + ((int) (this.startyear ^ (this.startyear >>> 32)));

    return hash;
  }
}
