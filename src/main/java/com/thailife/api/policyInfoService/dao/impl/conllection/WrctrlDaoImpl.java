package com.thailife.api.policyInfoService.dao.impl.conllection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.model.WrctrlM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class WrctrlDaoImpl {
  private final static Log log = new Log();
  private final static String className = "AnnualindecomeDaoImpl";
  
  public static List<WrctrlM> selectByPolicyNo(
      String policyno, String year, Integer limit, String paydate) {
    log.printDebug(className, "selectByPolicyNo : " + policyno);
    List<WrctrlM> list = new ArrayList<>();
    Connection con = null;
    try {
      con = DBConnection.getConnectionCollection();
      String table = "receipt.wrctrl" + year;
      String query =
          "select * from "
              + table
              + " where policyno = ? and currentstatus in ('P', 'B', 'E')"
              + " and to_date(paydate, 'YYYYMMDD') >= to_date(?, 'YYYYMMDD')"
              + " order by "
              + table
              + ".paydate desc limit ?";
      try (PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(query))) {
        pst.setString(1, policyno);
        pst.setString(2, paydate);
        pst.setInt(3, limit);
        try (ResultSet rs = pst.executeQuery(); ) {
          while (rs.next()) {
            WrctrlM wrctrlM =
                new ObjectMapper().readValue(rs.getString("json"), new TypeReference<WrctrlM>() {});
            list.add(wrctrlM);
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "selectByPolicyNo error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return list;
  }

  public boolean checkTable(String year) {
    log.printDebug(className, "checkTable : year" + year);
    Connection con = null;
    try {
      con = DBConnection.getConnectionCollection();
      String table = "wrctrl" + year;
      String query =
          "SELECT EXISTS(SELECT 1 FROM pg_tables WHERE schemaname = 'receipt' AND tablename = '"
              + table
              + "')";
      PreparedStatement pst = con.prepareStatement(query);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        return rs.getBoolean("exists");
      }
    } catch (Throwable e) {
      log.printError(className, "checkTable error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return false;
  }
}
