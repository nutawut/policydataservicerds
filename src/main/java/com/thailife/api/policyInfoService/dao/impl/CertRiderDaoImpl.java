package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.CertRiderDao;
import com.thailife.api.policyInfoService.model.CertRiderM;
import com.thailife.api.policyInfoService.util.PolicyException;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.mapper.RiderDetailMapper;
import com.thailife.api.policyInfoService.model.RiderDetailM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;

@Service
public class CertRiderDaoImpl {
  private static final Log log = new Log();
  private static final String className = "CertRiderDaoImpl";

  public ArrayList<RiderDetailM> searchRiderDetailByMortgage(String policyno, String certno) {
    log.printDebug(
        className, "searchRiderDetailByMortgage policyno :" + policyno + "certno : " + certno);
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "mortgage");
      String query =
          "select ridertype, sum as ridersum, (premium + extrapremium) as riderpremium,"
              + " riderstatus, riderstatusdate, effectivedate, extrapremium"
              + " from mortgage.certrider where policyno = ? and certno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, certno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (Throwable e) {
      log.printError(
          className, "searchRiderDetailByMortgage error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public ArrayList<RiderDetailM> searchRiderDetailByMortgage(String policyno) {
    log.printDebug(className, "searchRiderDetailByMortgage policyno :" + policyno);
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select ridertype, sum as ridersum, (premium + extrapremium) as riderpremium,"
              + " riderstatus, riderstatusdate, effectivedate, extrapremium"
              + " from mortgage.certrider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (Throwable e) {
      log.printError(
          className, "searchRiderDetailByMortgage error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public ArrayList<RiderDetailM> searchRiderDetailByWhlmast(String policyno) {
    log.printDebug(className, "searchRiderDetailByWhlmast policyno :" + policyno);
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select ridertype, ridersum, riderpremium, riderstatus,"
              + " riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from mstpolicy.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from mstpolicy.rider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (Throwable e) {
      log.printError(
          className, "searchRiderDetailByWhlmast error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public ArrayList<RiderDetailM> searchRiderDetailByIndmast(String policyno) {
    log.printDebug(className, "searchRiderDetailByIndmast policyno :" + policyno);
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select ridertype, ridersum, riderpremium, riderstatus,"
              + " riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from mstpolicy.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from mstpolicy.indrider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (Throwable e) {
      log.printError(
          className, "searchRiderDetailByIndmast error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public ArrayList<RiderDetailM> searchRiderDetailByUnitlink(String policyno) {
    log.printDebug(className, "searchRiderDetailByUnitlink policyno :" + policyno);
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select ridertype, ridersum, riderpremium, riderstatus,"
              + " riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from unitlink.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from unitlink.rider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (Throwable e) {
      log.printError(
          className, "searchRiderDetailByUnitlink error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public ArrayList<RiderDetailM> searchRiderDetailByUniversallife(String policyno) {
    log.printDebug(className, "searchRiderDetailByUniversallife policyno :" + policyno);

    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select ridertype, ridersum, riderpremium, riderstatus, riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from unitlink.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from universal.ulrider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (Throwable e) {
      log.printError(
          className, "searchRiderDetailByUniversallife error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public List<CertRiderDao> findByIdPolicynoAndIdCertno(String policyNo, String certNo)
      throws PolicyException {
    {
      log.printDebug(
          className, "findByIdPolicynoAndIdCertno policyno :" + policyNo + "certNo" + certNo);
      List<CertRiderDao> certRiderDaoList = new ArrayList<>();
      StringBuilder sql = new StringBuilder();
      sql.append("select row_to_json(tmp) as json ");
      sql.append("from (select * ");
      sql.append("      from mortgage.certrider ");
      sql.append("      where policyno = ? ");
      sql.append("        and certno = ?) tmp ");
      try (Connection con = DBConnection.getConnectionPolicy()) {
        try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
          pst.setString(1, policyNo);
          pst.setString(2, certNo);
          try (ResultSet rs = pst.executeQuery()) {
            CertRiderDao certRiderDao = new CertRiderDao();
            ObjectMapper mapper =
                new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            while (rs.next()) {
              certRiderDao =
                  mapper.readValue(rs.getString("json"), new TypeReference<CertRiderDao>() {});
              certRiderDaoList.add(certRiderDao);
            }
          }
        }
        return certRiderDaoList;
      } catch (Exception e) {
        log.printError(
            className, "findByIdPolicynoAndIdCertno error SQLException : " + e.getMessage());
        e.printStackTrace();
        throw new PolicyException(e.getMessage());
      }
    }
  }

  public List<CertRiderDao> findByIdPolicyno(String policyNo) throws PolicyException {
    {
      log.printDebug(className, "findByIdPolicynoAndIdCertno policyno :" + policyNo);
      List<CertRiderDao> certRiderDaoList = new ArrayList<>();
      StringBuilder sql = new StringBuilder();
      sql.append(" select row_to_json(tmp) as json");
      sql.append(" from (select *");
      sql.append(" from mortgage.certrider ");
      sql.append(" where policyno = ? ) tmp");
      try (Connection con = DBConnection.getConnectionPolicy()) {
        try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
          pst.setString(1, policyNo);
          try (ResultSet rs = pst.executeQuery()) {
            CertRiderDao certRiderDao;
            ObjectMapper mapper =
                new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            while (rs.next()) {
              certRiderDao =
                  mapper.readValue(rs.getString("json"), new TypeReference<CertRiderDao>() {});
              certRiderDaoList.add(certRiderDao);
            }
          }
        }
        return certRiderDaoList;
      } catch (Exception e) {
        log.printError(
            className, "findByIdPolicynoAndIdCertno error SQLException : " + e.getMessage());
        e.printStackTrace();
        throw new PolicyException(e.getMessage());
      }
    }
  }

  public static ArrayList<RiderDetailM> searchRiderDetailByPolicynoAndCertno(
      String policyno, String certno) {
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.getConnectionPolicy();
      String query =
          "select ridertype, sum as ridersum, (premium + extrapremium) as riderpremium,"
              + " riderstatus, riderstatusdate, effectivedate, extrapremium"
              + " from mortgage.certrider where policyno = ? and certno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, certno);
      log.printDebug(className, "sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return list;
  }

  public static ArrayList<RiderDetailM> searchRiderDetailByPolicyno(String policyno)
      throws Exception {
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.getConnectionPolicy();
      String query =
          "ridertype, sum as ridersum, (premium + extrapremium) as riderpremium,"
              + " riderstatus, riderstatusdate, effectivedate, extrapremium"
              + " from mortgage.certrider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      log.printError(className, e.getMessage());
    }
    return list;
  }

  public static CertRiderM searchByPolicynoAndCertnoAndRiderType106(String policyno, String certno) throws SQLException {
    Connection con = null;
    try {
      con = DBConnection.getConnectionPolicy();
      String query = "select policyno, certno, ridertype, sum, premium, extrapremium,"
              + " riderstatus, riderstatusdate, effectivedate, reserve, recordtimestamp,"
              + " journaltimestamp from mortgage.certrider where policyno = ? and certno = ? " +
              " and ridertype = '106' and riderstatus <> 'N'";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, certno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        CertRiderM certRiderM = new CertRiderM();
        certRiderM.setPolicyno(rs.getString("policyno"));
        certRiderM.setCertno(rs.getString("certno"));
        certRiderM.setRidertype(rs.getString("ridertype"));
        certRiderM.setSum(rs.getString("sum"));
        certRiderM.setPremium(rs.getString("premium"));
        certRiderM.setRiderstatus(rs.getString("riderstatus"));
        certRiderM.setRiderstatusdate(rs.getString("riderstatusdate"));
        certRiderM.setEffectivedate(rs.getString("effectivedate"));
        certRiderM.setReserve(rs.getString("reserve"));
        certRiderM.setRecordtimestamp(rs.getString("recordtimestamp"));
        certRiderM.setJournaltimestamp(rs.getString("journaltimestamp"));
        return certRiderM;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }finally{
      con.close();
    }
    return null;
  }
}
