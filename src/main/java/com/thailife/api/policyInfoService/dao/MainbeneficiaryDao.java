package com.thailife.api.policyInfoService.dao;

import java.sql.Timestamp;
import lombok.Data;
@Data
public class MainbeneficiaryDao {
  private String policyno;
  private String certno;
  private String name;
  private String status;
  private Timestamp recordtimestamp;
  private Timestamp journaltimestamp;
}
