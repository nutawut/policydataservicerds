package com.thailife.api.policyInfoService.dao.impl.customer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.PolicyParticipantDao;
import com.thailife.api.policyInfoService.model.PolicyInfoM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class PolicyParticipantDaoImpl {
  private final Log log = new Log();
  private final String className = "PolicyParticipantDaoImpl";
  public List<PolicyInfoM> searchPolicyByPartyId(long partyId) {
    log.printDebug(className, "searchPolicyByPartyId : " + partyId);
    Connection con = null;
    ArrayList<PolicyInfoM> list = new ArrayList<>();
    try {
      con = DBConnection.getConnectionCustomer();
      String query =
          "select pp.policy_no, p.pname_th, p.fname_th, p.lname_th, p.birth_date from \"policy\".policy_participant pp"
              + " left join party.person p on p.party_id = pp.party_id where pp.party_id = ?";
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setLong(1, partyId);
        //log.debug("sql searchPolicyByPartyId:" + pst);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            PolicyInfoM bean = new PolicyInfoM();
            bean.setPolicy_no(rs.getString("policy_no"));
            bean.setPname_th(rs.getString("pname_th"));
            bean.setFname_th(rs.getString("fname_th"));
            bean.setLname_th(rs.getString("lname_th"));
            list.add(bean);
          }
        }
      }

    } catch (Throwable e) {
      log.printError(className, "searchPolicyByPartyId error Throwable : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public ArrayList<PolicyParticipantDao> findByPartyidOrderByLastUpdateDesc(long partyId) {
    log.printDebug(className, "findByPartyidOrderByLastUpdateDesc : " + partyId);
    Connection con = null;
    ArrayList<PolicyParticipantDao> list = null;
    try {
      list = new ArrayList<>();
      con = DBConnection.getConnectionCustomer();
      String table = "\"policy\".policy_participant";
      String query =
          "select * from "
              + table
              + " where party_id = ? and participant_role = 1 order by last_update desc ";
      try (PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(query))) {
        pst.setLong(1, partyId);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            PolicyParticipantDao participantDao =
                new ObjectMapper()
                    .readValue(rs.getString("json"), new TypeReference<PolicyParticipantDao>() {});
            list.add(participantDao);
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "findByPartyidOrderByLastUpdateDesc error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return list;
  }
  
  public String checkExsitPolicyByPolicyNo(String policyNo) {
    log.printDebug(className, "checkExsitPolicyByPolicyNo : " + policyNo);
    Connection con = null;
    try {
      con = DBConnection.getConnectionPolicy();
      String query = "select 'O' as policytype from mstpolicy.ordmast o where policyno = ? "
          + " union select 'I' as policytype from mstpolicy.indmast i where policyno = ? "
          + " union select 'W' as policytype from mstpolicy.whlmast w where policyno = ? "
          + " union select 'L' as policytype from unitlink.unitlink u where policyno = ? "
          + " union select 'U' as policytype from universal.universallife u2 where policyno = ? ";

      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, policyNo);
        pst.setString(2, policyNo);
        pst.setString(3, policyNo);
        pst.setString(4, policyNo);
        pst.setString(5, policyNo);
        System.out.printf("sql searchPolicyByPartyId:" + pst);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            log.printDebug(className, "policyNo : "+policyNo+ " policytype : " + rs.getString("policytype"));
            return rs.getString("policytype");
          }
        }
      }

    } catch (Throwable e) {
      log.printError(className, "checkExsitPolicyByPolicyNo error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return "";
  }

  public String checkExsitPolicyByPolicyNoAndCertNo(String policyNo, String certNo) {
    log.printDebug(className, "checkExsitPolicyByPolicyNoAndCertNo : " + policyNo);
    Connection con = null;
    try {
      con = DBConnection.getConnectionPolicy();
      String query = "select 'M' as policytype from mortgage.cert  c where certno = ? and policyno = ? ";
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, certNo);
        pst.setString(2, policyNo);
        System.out.printf("sql searchPolicyByPartyId:" + pst);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            return rs.getString("policytype");
          }
        }
      }

    } catch (Throwable e) {
      log.printError(className, "checkExsitPolicyByPolicyNoAndCertNo error Throwable : " + e.getMessage());
      e.printStackTrace();
    }
    return "";
  }
}
