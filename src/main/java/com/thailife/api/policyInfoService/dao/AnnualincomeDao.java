package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class AnnualincomeDao implements Serializable {
  private static final long serialVersionUID = 1L;
  private String policyno;
  private BigDecimal amount;
  private String annualtype;
  private String annuitydate;
  private BigDecimal cv;
  private Timestamp journaltimestamp;
  private BigDecimal netcv;
  private BigDecimal netloan;
  private BigDecimal netpension;
  private BigDecimal netprem;
  private BigDecimal pension;
  private Timestamp recordtimestamp;

}
