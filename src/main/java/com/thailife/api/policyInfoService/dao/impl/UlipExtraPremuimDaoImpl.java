package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.ExtraPremiumUKDao;
import com.thailife.api.policyInfoService.dao.UlExtraPremiumPKUKDao;
import com.thailife.api.policyInfoService.dao.UlExtrapremuimDao;
import com.thailife.api.policyInfoService.mapper.ExtraPremiumMapper;
import com.thailife.api.policyInfoService.mapper.UlipExtraPremiumMapper;
import com.thailife.api.policyInfoService.mapper.UlipExtraPremiumPKMapper;
import com.thailife.api.policyInfoService.util.Log;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class UlipExtraPremuimDaoImpl {
  private final String scheme = "unitlink.";
  private final String table = "extrapremium";
  private final Log log = new Log();
  private final String className = "UlipExtraPremuimDaoImpl";

  public List<ExtraPremiumUKDao> findByIdPolicyNoAndIdExtraType(String policyno, String ridertype) {
    try (Connection con = DBConnection.getConnectionPolicy()) {
      StringBuilder sql = new StringBuilder();
      sql.append(" SELECT * FROM ").append(scheme).append(table);
      sql.append(" WHERE policyno = ? and ridertype = ?");
      log.printDebug(className, "sql: " + sql);
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyno);
        pst.setString(2, ridertype);
        try (ResultSet rs = pst.executeQuery()) {
          List<ExtraPremiumUKDao> tmp = new ArrayList<>();
          while (rs.next()) {
            tmp.add(ExtraPremiumMapper.mapperUk(rs));
          }
          return tmp;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, "findByIdPolicyNoAndIdExtraType: " + e.getMessage());
      return null;
    }
  }
}
