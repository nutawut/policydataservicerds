package com.thailife.api.policyInfoService.dao.impl.conllection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.model.OrctrlM;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrctrlDaoImpl {
  private final Log log = new Log();
  private final String className = "OrctrlDaoImpl";
  public List<OrctrlM> selectByPolicyNo(
      String policyno, String year, Integer limit, String paydate, Connection con)
      throws SQLException, JsonProcessingException {
    log.printDebug(className, "selectByPolicyNo : " + policyno);
    List<OrctrlM> list = new ArrayList<>();
    try {
      String table = "receipt.orctrl" + year;
      StringBuilder query = new StringBuilder();
      query.append(" select row_to_json(tmp) as json ");
      query.append(" from ( ");
      query.append(" select * from ").append(table);
      query.append(" where policyno = ? and currentstatus in ('P', 'B', 'E') ");
      query.append(" and to_date(paydate, 'YYYYMMDD') >= to_date(?, 'YYYYMMDD') ");
      query.append(" order by ").append(table).append(".paydate desc limit ? ");
      query.append(" ) tmp ");
      try (PreparedStatement pst = con.prepareStatement(query.toString()); ) {
        pst.setString(1, policyno);
        pst.setString(2, paydate);
        pst.setInt(3, limit);
        try (ResultSet rs = pst.executeQuery(); ) {
          ObjectMapper mapper =
              new ObjectMapper()
                  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          while (rs.next()) {
            OrctrlM orctrlM =
                mapper.readValue(rs.getString("json"), new TypeReference<OrctrlM>() {});
            list.add(orctrlM);
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, "selectByPolicyNo error Exception : " + e.getMessage());
      throw e;
    }
    return list;
  }

  public List<String> getEffectiveDate(
      String policyno, String year, Integer limit, String paydate, Connection con) {
    log.printDebug(className, "getEffectiveDate : policyno " + policyno);
    List<String> list = new ArrayList<>();
    try {
      String table = "receipt.orctrl" + year;
      String query =
          "select effectivedate from "
              + table
              + " where policyno = ? and currentstatus in ('P', 'B', 'E')"
              + " and to_date(paydate, 'YYYYMMDD') >= to_date(?, 'YYYYMMDD')"
              + " order by "
              + table
              + ".paydate desc limit ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, paydate);
      pst.setInt(3, limit);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(rs.getString("effectivedate"));
      }
    } catch (Throwable e) {
      log.printError(className, "getEffectiveDate error Throwable : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public boolean checkTable(String year, Connection con) {
    log.printDebug(className, "checkTable : year " + year);
    try {
      String table = "orctrl" + year;
      String query =
          "SELECT 1 FROM pg_tables WHERE schemaname = 'receipt' AND tablename = '" + table + "'";
      PreparedStatement pst = con.prepareStatement(query);
      ResultSet rs = pst.executeQuery();
      return rs.next();
    } catch (Throwable e) {
      log.printError(className, "checkTable error Throwable : " + e.getMessage());
      e.printStackTrace();
    }
    return false;
  }
}
