package com.thailife.api.policyInfoService.dao;

import java.sql.Timestamp;
import lombok.Data;

@Data
public class CreditmortgageDao {
  private String cert;
  private String partnercode;
  private String billingtype;
  private String policyno;
  private String policytype;
  private String cardname;
  private String cardtype;
  private String merchantcode;
  private String creditno;
  private String relationtype;
  private String ownername;
  private String expiredate;
  private String entrydate;
  private String entrytime;
  private String status;
  private String statusdate;
  private String userid;
  private Timestamp recordtimestamp;
  private Timestamp journaltimestamp;
}
