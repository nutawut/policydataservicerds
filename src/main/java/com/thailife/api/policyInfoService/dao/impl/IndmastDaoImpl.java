package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.thailife.api.policyInfoService.mapper.RiderDetailMapper;
import com.thailife.api.policyInfoService.model.RiderDetailM;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.IndmastDao;
import com.thailife.api.policyInfoService.mapper.IndmastMapper;
import com.thailife.api.policyInfoService.mapper.PolicyDetailMapper;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;

@Service
public class IndmastDaoImpl {
  private static final Log log = new Log();
  private static final String className = "IndmastDaoImpl";

  public IndmastDao findByPolicyno(String policyno) throws Exception {
    log.printDebug(className, "findByPolicyNo : " + policyno);
    IndmastDao result = null;
    Connection con = DBConnection.con(DBproperties.getDBPolicy(), "");
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select");
      sql.append(" * ");
      sql.append(" from mstpolicy.indmast ");
      sql.append(" where policyno = ? ");

      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policyno);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result = IndmastMapper.indmastMapper(rs);
      }
    } catch (Exception e) {
      log.printError(className, "findByPolicyno error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }

    return result;
  }

  public List<IndmastDao> findByPolicynoList(String policyno) throws Exception {
    log.printDebug(className, "findByPolicyNo : " + policyno);
    List<IndmastDao> result = new ArrayList<>();
    Connection con = DBConnection.con(DBproperties.getDBPolicy(), "");
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select");
      sql.append(" * ");
      sql.append(" from mstpolicy.indmast ");
      sql.append(" where policyno = ? ");
      log.printDebug(className, "sql: " + sql);
      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policyno);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result.add(IndmastMapper.indmastMapper(rs));
      }
    } catch (Exception e) {
      log.printError(className, "findByPolicyno error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }

    return result;
  }

  public static PolicyDetailM searchPolicyDetailByPolicyno(String policyno) throws SQLException {
    log.printInfo(className, "searchPolicyDetailByPolicyno : " + policyno);
    Connection con = null;
    PolicyDetailM policyM = null;
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select policystatus1, policystatus2, policystatusdate1, policystatusdate2,"
              + " oldpolicystatus1, oldpolicystatus2, oldpolicystatusdate1, oldpolicystatusdate2,"
              //              + " payperiod, duedate, cast(null as numeric) as tpdpremium, cast(null
              // as numeric) as addpremium,"
              + " payperiod, cast(null as varchar) as duedate, cast(null as numeric) as tpdpremium, cast(null as numeric) as addpremium,"
              + " sum as lifesum, cast(null as numeric) as extratpdpremium, effectivedate, cast(null as numeric) as accidentsum, plancode, mode"
              + " from mstpolicy.indmast where policyno = ? order by recordtimestamp desc limit 1";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      // System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        policyM = PolicyDetailMapper.policyDetailMapper(rs);
      }
    } catch (SQLException e) {
      log.printError(
          className, "searchPolicyDetailByPolicyno error SQLException : " + e.getMessage());
      e.printStackTrace();
    } catch (Throwable e) {
      e.printStackTrace();
    }finally{
      con.close();
    }
    return policyM;
  }

  public static ArrayList<RiderDetailM> searchRiderDetailByPolicyno(String policyno) {
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.getConnectionPolicy();
      String query =
          "select ridertype, ridersum, riderpremium, riderstatus,"
              + " riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from mstpolicy.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from mstpolicy.indrider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return list;
  }
}
