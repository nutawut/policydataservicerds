package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.CreditmortgageDao;
import com.thailife.api.policyInfoService.mapper.CreditmortgageMapper;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;

@Service
public class CreditmortgageDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl"; 
  
  public CreditmortgageDao findByCert(String certNo) throws Exception {
    log.printDebug(className, "findByCert : " + certNo);
    CreditmortgageDao result = new CreditmortgageDao();
    Connection con = DBConnection.con(DBproperties.getDBPos(), DBproperties.getSchemafnapaybank());
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select");
      sql.append(" * ");
      sql.append(" from fnapaybank.creditmortgage ");
      sql.append(" where cert = ?");
      
      PreparedStatement pst = con
              .prepareStatement(sql.toString());
      pst.setString(1, certNo);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result = CreditmortgageMapper.creditmortgageMapper(rs);
      }
    }
    catch (Exception e) {
      log.printError(className, "findByCert error Exception : " + e.getMessage());
    }
    finally {
      con.close();
    }
    
    return result;
  }
  
  
}
