package com.thailife.api.policyInfoService.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import lombok.Data;

@Data
public class WhlmastDao {

  private String policyno;
  private BigDecimal benefitrate;
  private String branch;
  private String contactaddressid;
  private String contactstartdate;
  private String duedate;
  private String effectivedate;
  private String hivflag;
  private BigDecimal insuredage;
  private String invalid;
  private Timestamp journaltimestamp;
  private String lapseflag;
  private BigDecimal lifepremium;
  private String localaddressid;
  private String medical;
  private String mode;
  private String nameid;
  private BigDecimal occupationcode;
  private String occupationtype;
  private String oldpolicystatus1;
  private String oldpolicystatus2;
  private String oldpolicystatusdate1;
  private String oldpolicystatusdate2;
  private String paydate;
  private String payperiod;
  private String plancode;
  private String planpa;
  private String policystatus1;
  private String policystatus2;
  private String policystatusdate1;
  private String policystatusdate2;
  private Timestamp recordtimestamp;
  private String remark;
  private String rpno;
  private String salesid;
  private BigDecimal sum;
  private String tabianlapseflag;
}
