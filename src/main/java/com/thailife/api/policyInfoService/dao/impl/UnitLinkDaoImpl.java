package com.thailife.api.policyInfoService.dao.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.mapper.PolicyDetailMapper;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service

public class UnitLinkDaoImpl {
  private static final Log log = new Log();
  private static final String className = "UnitLinkDaoImpl";
  
  public UnitLinkDao findByPolicyno(String policyno) throws Exception {
    log.printDebug(className, "findByPolicyno : " + policyno);
    UnitLinkDao result = new UnitLinkDao();
    Connection con = DBConnection.getConnectionPolicy();
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select row_to_json(tmp) as json ");
      sql.append(" from ( select * from unitlink.unitlink where policyno = ? ) tmp");
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyno);
        try (ResultSet rs = pst.executeQuery()) {
          ObjectMapper mapper =
              new ObjectMapper()
                  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          while (rs.next()) {
            result = mapper.readValue(rs.getString("json"), new TypeReference<UnitLinkDao>() {});
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, "findByPolicyno error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }

    return result;
  }

  public static PolicyDetailM searchPolicyDetailByPolicyno(String policyno) throws SQLException {
    log.printDebug(className, "searchPolicyDetailByPolicyno : " + policyno);
    Connection con = null;
    PolicyDetailM policyM = null;
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select policystatus1, policystatus2, policystatusdate1, policystatusdate2,"
              + " oldpolicystatus1, oldpolicystatus2, oldpolicystatusdate1, oldpolicystatusdate2,"
              + " payperiod, duedate, cast(null as numeric) as tpdpremium, cast(null as numeric) as addpremium,"
              + " cast(null as numeric) as lifesum, cast(null as numeric) as extratpdpremium, effectivedate,"
              + " cast(null as numeric) as accidentsum , plancode, mode"
              + " from unitlink.unitlink where policyno = ? order by recordtimestamp desc limit 1";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        policyM = PolicyDetailMapper.policyDetailMapper(rs);
      }
    } catch (SQLException e) {
      log.printError(className, "searchPolicyDetailByPolicyno error SQLException : " + e.getMessage());
      e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "searchPolicyDetailByPolicyno error Throwable : " + e.getMessage());
      e.printStackTrace();
    }finally{
      con.close();
    }
    return policyM;
  }

  
}
