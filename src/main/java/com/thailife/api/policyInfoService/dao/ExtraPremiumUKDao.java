package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;


@Data
public class ExtraPremiumUKDao {
    private UlExtraPremiumPKUKDao id;
    private BigDecimal emrate;
    private BigDecimal eprate;
    private BigDecimal epurate;
    private BigDecimal extrapremium;
    private String journaltimestamp;
    private String mode;
    private BigDecimal nextpdextrapremium;
    private String noofyear;
    private String recordtimestamp;
}
