package com.thailife.api.policyInfoService.dao;

import lombok.Data;

@Data
public class UlExtraPremiumPKUKDao {
  private String policyno;
  private String extratype;
  private String startyear;

  public UlExtraPremiumPKUKDao() {
  }
  public String getPolicyno() {
    return this.policyno;
  }
  public void setPolicyno(String policyno) {
    this.policyno = policyno;
  }
  public String getExtratype() {
    return this.extratype;
  }
  public void setExtratype(String extratype) {
    this.extratype = extratype;
  }
  public String getStartyear() {
    return this.startyear;
  }
  public void setStartyear(String startyear) {
    this.startyear = startyear;
  }

  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof UlExtraPremiumPKUKDao)) {
      return false;
    }
    UlExtraPremiumPKUKDao castOther = (UlExtraPremiumPKUKDao)other;
    return
            this.policyno.equals(castOther.policyno)
                    && this.extratype.equals(castOther.extratype)
                    && this.startyear.equals(castOther.startyear);
  }

  public int hashCode() {
    final int prime = 31;
    int hash = 17;
    hash = hash * prime + this.policyno.hashCode();
    hash = hash * prime + this.extratype.hashCode();
    hash = hash * prime + this.startyear.hashCode();

    return hash;
  }
}
