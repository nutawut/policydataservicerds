package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.UlExtrapremuimDao;
import com.thailife.api.policyInfoService.mapper.UlipExtraPremiumMapper;
import com.thailife.api.policyInfoService.util.Log;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class UlExtraPremiumDaoImpl {
  private final Log log = new Log();
  private final String className = "UlExtraPremiumDaoImpl";
  private final String scheme = "universal.";
  private final String table = "ulextrapremium";

  public List<UlExtrapremuimDao> findByIdPolicynoAndIdExtratype(String policyNo, String riderType) {
    try (Connection con = DBConnection.getConnectionPolicy()) {
      return findByIdPolicynoAndIdExtratype(policyNo, riderType, con);
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, e.getMessage());
      return null;
    }
  }

  public List<UlExtrapremuimDao> findByIdPolicynoAndIdExtratype(
      String policyNo, String riderType, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM ").append(scheme).append(table);
    sql.append(" WHERE policyno = ? and ridertype = ? ");
    log.printDebug(className, "sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setString(1, policyNo);
      pst.setString(2, riderType);
      try (ResultSet rs = pst.executeQuery()) {
        List<UlExtrapremuimDao> tmp = new ArrayList<>();
        while (rs.next()) {
          tmp.add(UlipExtraPremiumMapper.mapper(rs));
        }
        return tmp;
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("errorMessage: " + e.getMessage());
    }
  }
}
