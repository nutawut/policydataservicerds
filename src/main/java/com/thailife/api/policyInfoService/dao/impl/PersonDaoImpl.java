package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.model.PartyPersonM;
import com.thailife.api.policyInfoService.model.PersonM;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.PersonDao;
import com.thailife.api.policyInfoService.mapper.PersonMapper;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;

@Service
public class PersonDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";
  public PersonDao findByPersonid(String personId) throws Exception {
    log.printDebug(className, "findByPersonid : " + personId);
    PersonDao result = new PersonDao();
    Connection con =
        DBConnection.con(DBproperties.getDBPolicy(), DBproperties.getSchemaMstperson());
    try {
      result = getExecuteQuery(personId, con);
    } catch (Exception e) {
      log.printError(className, "findByPersonid error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }

    return result;
  }

  public PersonDao findByPersonId(String personId, Connection con) throws Exception {
    try {
      log.printDebug(className, "findByPersonId : " + personId);
      PolicyUtil.setDefaultConnection(con);
      return getExecuteQuery(personId, con);
    } catch (Exception e) {
      log.printError(className, "findByPersonId error SQLException : " + e.getMessage());
      throw e;
    }
  }

  public List<PersonM> selectByNameId(String nameid) {
    log.printDebug(className, "selectByNameId : " + nameid);
    List<PersonM> list = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    sql.append(" select row_to_json(tmp) as json ");
    sql.append(" from (select * from mstperson.person  ");
    sql.append("        where personid = (select personid from mstperson.name where nameid = ? ) ");
    sql.append("    ) tmp  ");
    try (Connection con = DBConnection.getConnectionPolicy()) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, nameid);
        try (ResultSet rs = pst.executeQuery()) {
          ObjectMapper mapper =
              new ObjectMapper()
                  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          while (rs.next()) {
            PersonM personM =
                mapper.readValue(rs.getString("json"), new TypeReference<PersonM>() {});
            if (personM != null) list.add(personM);
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "selectByNameId error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public PartyPersonM findByPartyid(int partyId) throws SQLException {
    log.printDebug(className, "findByPartyid : " + partyId);
    Connection con = null;
    try {
      con = DBConnection.con(PolicyUtil.getDBCustomer(), "");
      String table = "party.person";
      String query = "select * from " + table + " where party_id = ? ";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setInt(1, partyId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        PartyPersonM personM = PersonMapper.convertToPersonM(rs);

        return personM;
      }
    } catch (SQLException e) {
      log.printError(className, "findByPartyid error SQLException : " + e.getMessage());
      e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "findByPartyid error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      con.close();
    }
    return null;
  }

  private PersonDao getExecuteQuery(String personId, Connection con) throws SQLException {
    log.printDebug(className, "getExecuteQuery : " + personId);
    PersonDao result = new PersonDao();
    StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append(" * ");
    sql.append(" from mstperson.person ");
    sql.append(" where personid = ?");

    PreparedStatement pst = con.prepareStatement(sql.toString());
    pst.setString(1, personId);
    ResultSet rs = pst.executeQuery();
    if (rs.next()) {
      result = PersonMapper.personMapper(rs);
    }
    return result;
  }

  public static String findNameIdByCitizenId(String citizenId) {
    try (Connection con = DBConnection.getConnectionPolicy()) {
      StringBuilder sql = new StringBuilder();
      sql.append(" select nameid as nameId ");
      sql.append(" from mstperson.person p ");
      sql.append(" inner join mstperson.name n on p.personid = n.personid ");
      sql.append(" where p.referenceid = ? ");
      System.out.println("sql = " + sql.toString());
      try (PreparedStatement pst = con.prepareStatement(sql.toString()); ) {
        pst.setString(1, citizenId);
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return rs.getString("nameId");
          }
        }
      }
     }catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
