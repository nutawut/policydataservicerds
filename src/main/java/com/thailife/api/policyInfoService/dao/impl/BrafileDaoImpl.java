package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.BrafileDao;
import com.thailife.api.policyInfoService.mapper.BrafileMapper;
import com.thailife.api.policyInfoService.util.Log;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class BrafileDaoImpl {
  private final String scheme = "commontable.";
  private final String table = "brafile";
  private final Log log = new Log();
  private final String className = "BrafileDaoImpl";

  public BrafileDao findByBranch(String branch, Connection con) {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM ").append(scheme).append(table);
    sql.append(" WHERE branch = ? ");
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, branch);
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return BrafileMapper.mapper(rs);
          }
        }
      } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, e.getMessage());
      return null;
    }
    return null;
  }

  public Map<String, String> searchAllBranch(Connection con) throws SQLException {
    Map<String, String> map = new HashMap<>();
    try {
      String query = "select branch, branchname from ".concat(scheme).concat(table);
      PreparedStatement pst = con.prepareStatement(query);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        map.put(rs.getString("branch"), rs.getString("branchname"));
      }
    } catch (Throwable e) {
      log.printError(className, e.getMessage());
      throw e;
    }
    return map;
  }
}
