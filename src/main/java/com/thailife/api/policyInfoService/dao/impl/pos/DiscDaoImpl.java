package com.thailife.api.policyInfoService.dao.impl.pos;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.model.DiscM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
@Service

public class DiscDaoImpl {
  private final Log log = new Log();
  private final String className = "DiscDaoImpl";
  public List<DiscM> selectByRpno(String rpno) {
    log.printDebug(className, "selectBy Rpno : " + rpno);
    List<DiscM> list = new ArrayList<>();
    try(Connection con = DBConnection.getConnectionPos()) {
      String query = PolicyUtil.setRowToJson("select * from collection.disc where rpno = ? ");
      //log.debug(String.format("sql selectByRpno : %s",query));
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, rpno);
        try (ResultSet rs = pst.executeQuery()) {
          ObjectMapper mapper =
              new ObjectMapper()
                  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          while (rs.next()) {
            DiscM statusueM = mapper.readValue(rs.getString("json"), new TypeReference<DiscM>() {});
            list.add(statusueM);
          }
        }
      }

    } catch (Throwable e) {
      log.printError(className, "selectByRpno error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public String getDiscountAmount(String rpno) throws Exception {
    log.printDebug(className, "getDiscountAmount Rpno : " + rpno);
    try(Connection con = DBConnection.getConnectionPos()) {
      String query = "select discountamount from collection.disc where rpno = ? ";
      //log.debug(String.format("sql selectByRpno : %s",query));
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, rpno);
        try (ResultSet rs = pst.executeQuery()) {
          return rs.next() ? rs.getString("discountamount") : "0";
        }
      }
    } catch (Throwable e) {
      log.printError(className, "getDiscountAmount error SQLException : " + e.getMessage());
      //log.error(String.format("Error getDiscountAmount : %s",e.getMessage()));
      throw e;
    }
  }
}
