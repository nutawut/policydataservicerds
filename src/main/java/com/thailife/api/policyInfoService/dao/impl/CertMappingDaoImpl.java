package com.thailife.api.policyInfoService.dao.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.CertMappingDao;
import com.thailife.api.policyInfoService.mapper.CertMappingMapper;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;


@Service
public class CertMappingDaoImpl {
  private final Log log = new Log();
  private final String className = "CertMappingDaoImpl";
  
  public CertMappingDao findByPolicyNoAndCertNo(String policyNo, String certNo, Connection con) throws Exception {
    CertMappingDao certMappingDao = new CertMappingDao();
    StringBuilder sql = new StringBuilder();
    log.printDebug(className, "findByPolicyNoAndCertNo  policyNo " + policyNo + "certNo :" + certNo);
    try {
      sql.append(" select *");
      sql.append(" from mortgage.certmapping u");
      sql.append(" where u.policyno = ? and u.certno = ?");
      PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(sql.toString()));
      pst.setString(1, policyNo);
      pst.setString(2, certNo);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        certMappingDao =
            new ObjectMapper()
                .readValue(rs.getString("json"), new TypeReference<CertMappingDao>() {});
      } else {
        return null;
      }
    } catch (SQLException e) {
      log.printError(className, "findByPolicyNoAndCertNo error SQLException : " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return certMappingDao;
  }

  public String getRpolicynoByPolicynoAndCertno(String policyno, String certno, Connection con) {
    log.printDebug(className, "getRpolicynoByPolicynoAndCertno  policyNo " + policyno + "certNo :" + certno);
    try{
      String query = "select rpolicyno from mortgage.certmapping where policyno = ? and certno = ?";
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, policyno);
        pst.setString(2, certno);
        System.out.println("sql:" + pst);
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return rs.getString("rpolicyno");
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "getRpolicynoByPolicynoAndCertno error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return null;
  }

  public static HashMap<String, String> getPolicyNoAndCertNoByRcertNo(String rcertNo, Connection con) throws Exception {
    HashMap<String, String> result = new HashMap<>();
    try {
      StringBuilder query = new StringBuilder();
      query.append(" select policyno, certno from mortgage.certmapping where rcertno = ? ");
      PreparedStatement pst = con.prepareStatement(query.toString());
      pst.setString(1,rcertNo);
      System.out.printf("sql: " + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result.put("policyNo",rs.getString("policyno"));
        result.put("certNo",rs.getString("certno"));
      }
    }catch (Exception e){
      throw e;
    }
    return result;
  }
}
