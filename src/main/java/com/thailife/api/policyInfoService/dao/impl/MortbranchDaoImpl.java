package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.MortbranchDao;
import com.thailife.api.policyInfoService.mapper.MortbranchMapper;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;

@Service
public class MortbranchDaoImpl {
  private final Log log = new Log();
  private final String className = "MortbranchDaoImpl";
  
  public MortbranchDao findByCode(String code) throws Exception {
    MortbranchDao result = new MortbranchDao();
    log.printDebug(className, "findByCode : " + code);
    try (Connection con =
        DBConnection.con(DBproperties.getDBPolicy(), DBproperties.getSchemaMortgage())) {
      StringBuilder sql = new StringBuilder();
      sql.append(" select");
      sql.append(" * ");
      sql.append(" from mortbranch ");
      sql.append(" where code = ? ");

      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, code);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result = MortbranchMapper.mortbranchMapper(rs);
      }
    } catch (Exception e) {
      log.printError(className, "findByCode error SQLException : " + e.getMessage());
    }

    return result;
  }
}
