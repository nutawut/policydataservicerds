package com.thailife.api.policyInfoService.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import lombok.Data;
@Data
public class SubunitlinkDao {
  private String policyno;
  private String premiumtype;
  private String duedate;
  private String effectivedate;
  private Timestamp journaltimestamp;
  private String oldpolicystatus1;
  private String oldpolicystatus2;
  private String oldpolicystatusdate1;
  private String oldpolicystatusdate2;
  private String paydate;
  private String payperiod;
  private String policystatus1;
  private String policystatus2;
  private String policystatusdate1;
  private String policystatusdate2;
  private BigDecimal premium;
  private Timestamp recordtimestamp;
  private String rpno;
  private BigDecimal sum;
}
