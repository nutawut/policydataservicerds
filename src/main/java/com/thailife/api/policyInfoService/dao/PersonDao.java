package com.thailife.api.policyInfoService.dao;

import java.sql.Timestamp;
import lombok.Data;

@Data
public class PersonDao {
  private String personid;
  private String birthdate;
  private String bluecard;
  private String customerid;
  private Timestamp journaltimestamp;
  private Timestamp recordtimestamp;
  private String referenceid;
  private String referencetype;
  private String reserve;
  private String sex;
}
