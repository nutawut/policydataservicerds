package com.thailife.api.policyInfoService.dao.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.UlrctrlDao;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class UlrctrlDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";
  
  public List<UlrctrlDao> findByPolicyNo(String policyno, String paydate, Integer limit) {
    log.printDebug(className, "findByPolicyNo : " + policyno + " paydate :" +paydate+ " limit :"+limit);
    List<UlrctrlDao> list = new ArrayList<>();
    Connection con = null;
    try {
      con = DBConnection.getConnectionUnitLink();
      String query =
          "select ulrctrl.* "
              + "from universal.ulrctrl ulrctrl "
              + "where ulrctrl.policyno = ? "
              + "and currentstatus in ('P', 'B', 'E') "
              + "and to_date(paydate, 'YYYYMMDD') "
              + ">= to_date(?, 'YYYYMMDD') order by ulrctrl.paydate desc limit ?";
      try (PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(query))) {
        //log.info(String.format("SQL UlrctrlDaoImpl findByPolicyNo :%s", query));
        pst.setString(1, policyno);
        pst.setString(2, paydate);
        pst.setInt(3, limit);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            UlrctrlDao ulrctrlDao =
                new ObjectMapper()
                    .readValue(rs.getString("json"), new TypeReference<UlrctrlDao>() {});
            list.add(ulrctrlDao);
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "findByPolicyNo error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return list;
  }
}
