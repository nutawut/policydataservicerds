package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.LicenseDao;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;

@Service
public class LicenseDaoImpl {
  private final Log log = new Log();
  private final String className = "LicenseDaoImpl";
  public LicenseDao findByCitizenid(String citizenid) throws Exception {
    log.printDebug(className, "findByCitizenid : " + citizenid);
    LicenseDao data = new LicenseDao();
    Connection con = DBConnection.getConnectionSaleStruct();
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select row_to_json(tmp) as json ");
      sql.append(" from ( select * from license.license where citizenid = ? ) tmp");
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, citizenid);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            data = (new ObjectMapper().readValue(rs.getString("json"), new TypeReference<LicenseDao>() {}));
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, "LicenseDaoImpl error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }
    return data;
  }

}
