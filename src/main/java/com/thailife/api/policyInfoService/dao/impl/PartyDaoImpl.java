package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.PartyDao;
import com.thailife.api.policyInfoService.mapper.PartyMapper;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
@Service
public class PartyDaoImpl {
  private final Log log = new Log();
  private final String className = "PartyDaoImpl";
  public PartyDao findByPartyid(int partyId) throws SQLException {
    log.printDebug(className, "findByPartyid : " + partyId);
    Connection con = null;
    try {
        con = DBConnection.con(PolicyUtil.getDBCustomer(), "");
        String table = "party.party";
        String query = "select * from " + table
                + " where party_id = ? ";
        PreparedStatement pst = con.prepareStatement(query);
        pst.setInt(1, partyId);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
          PartyDao personM = PartyMapper.convertToPartyM(rs);
            
            return personM;
        }
    } catch (SQLException e) {
      log.printError(className, "findByPartyid error SQLException : " + e.getMessage());
        e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "findByPartyid error Throwable : " + e.getMessage());
        e.printStackTrace();
    } finally {
      con.close();
    }
    return null;
  }
    public static HashMap<String, String> findNameByGovId(String govid) throws Exception{
        HashMap<String, String> result = new HashMap<>();
        try(Connection con = DBConnection.getConnectionCustomer()){
            StringBuilder sql = new StringBuilder();
            sql.append(" select p.party_id, per.fname_th ,per.lname_th ,per.pname_th ");
            sql.append(" from party.party p ");
            sql.append(" left join party.person per on p.party_id = per.party_id ");
            sql.append(" where p.govt_id = ? ");
            PreparedStatement pst = con.prepareStatement(sql.toString());
            pst.setString(1,govid);
            System.out.println("findPartIdByPolicyNo SQL : " + sql);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                result.put("party_id", String.valueOf(rs.getLong("party_id")));
                result.put("fname_th", rs.getString("fname_th"));
                result.put("lname_th", rs.getString("lname_th"));
                result.put("pname_th", rs.getString("pname_th"));
                return result;
            }
        }
        return null;
    }
}
