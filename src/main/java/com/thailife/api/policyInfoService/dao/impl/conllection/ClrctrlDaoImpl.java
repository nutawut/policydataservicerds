package com.thailife.api.policyInfoService.dao.impl.conllection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.model.ClrctrlM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClrctrlDaoImpl {
  private final Log log = new Log();
  private final String className = "ClrctrlDaoImpl";
  public List<ClrctrlM> selectByPolicyNoAndCertNo(
      String policyno, String certno, String year, Integer limit, String paydate) {
    log.printDebug(className, "selectByPolicyNoAndCertNo  ");
    List<ClrctrlM> list = new ArrayList<>();
    Connection con = null;
    try {
      con = DBConnection.getConnectionCollection();
      String table = "receipt.clrctrl" + year;
      String query =
          "select * from "
              + table
              + " where policyno = ? and certno = ? and currentstatus in ('P', 'B', 'E')"
              + " and to_date(paydate, 'YYYYMMDD') >= to_date(?, 'YYYYMMDD')"
              + " order by "
              + table
              + ".paydate desc  limit ?";
      try (PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(query))) {
        pst.setString(1, policyno);
        pst.setString(2, certno);
        pst.setString(3, paydate);
        pst.setInt(4, limit);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            ClrctrlM clrctrlM =
                new ObjectMapper()
                    .readValue(rs.getString("json"), new TypeReference<ClrctrlM>() {});
            list.add(clrctrlM);
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "getPayDate error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return list;
  }

  public boolean checkTable(String year) {
    log.printDebug(className, "checkTable  year: " +year);
    Connection con = null;
    try {
      con = DBConnection.getConnectionCollection();
      String table = "clrctrl" + year;
      String query =
          "SELECT EXISTS(SELECT 1 FROM pg_tables WHERE schemaname = 'receipt' AND tablename = '"
              + table
              + "')";
      PreparedStatement pst = con.prepareStatement(query);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        return rs.getBoolean("exists");
      }
    } catch (Throwable e) {
      log.printError(className, "getPayDate error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return false;
  }

  public String getPayDate(String policyno, String certno) {
    log.printDebug(className, "getPayDate  policyno: " + policyno + " certno: " + certno);
    Connection con = null;
    try {
      con = DBConnection.getConnectionCollection();
      String query =
          "select COALESCE("
              + "		(	select paydate from receipt.clrctrl"
              + "			where policyno = ? and certno = ?"
              + "			order by paydate desc limit 1\n"
              + "		)"
              + "	, to_char(now() + interval '543 year', 'YYYYMMDD')"
              + "	) as paydate"
              + " from  receipt.clrctrl limit 1";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, certno);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        return rs.getString("paydate");
      }
    } catch (Throwable e) {
      log.printError(className, "getPayDate error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return null;
  }
}
