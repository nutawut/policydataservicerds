package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.ContactEmailDao;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
@Service
public class ContactEmailDaoImpl {
  private final Log log = new Log();
  private final String className = "ContactEmailDaoImpl";
//(schema = "contact", name = "Email")
  public ContactEmailDao findByEmailId(int emailId) throws PolicyException {
    {
      log.printDebug(className, "findByEmailId : " + emailId);
      ContactEmailDao data = null;
      StringBuilder sql = new StringBuilder();
      sql.append("select row_to_json(tmp) as json ");
      sql.append("from (select * from contact.Email where email_id = ? )tmp ");
      try (Connection con = DBConnection.getConnectionCustomer()) {
        try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
          data = new ContactEmailDao();
          pst.setInt(1, emailId);
          try (ResultSet rs = pst.executeQuery()) {
            ObjectMapper mapper =
                new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            if (rs.next()) {
              data = mapper.readValue(rs.getString("json"), new TypeReference<ContactEmailDao>() {});
            }
          }
        }
        return data;
      } catch (Exception e) {
        log.printError(className, "findByEmailId error SQLException : " + e.getMessage());
        e.printStackTrace();
        throw new PolicyException(e.getMessage());
      }
    }
  }

}
