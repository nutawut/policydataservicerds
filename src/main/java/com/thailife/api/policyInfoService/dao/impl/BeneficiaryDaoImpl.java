package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.BeneficiaryDao;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;
@Service
public class BeneficiaryDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";
  
  public List<BeneficiaryDao> findByPolicyno(String policyno) throws Exception {
    log.printDebug(className, "findByPolicyno : " + policyno);
    List<BeneficiaryDao> dataList = new ArrayList<BeneficiaryDao>();
    Connection con = DBConnection.getConnectionPolicy();
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select row_to_json(tmp) as json ");
      sql.append(" from ( select * from mstperson.beneficiary where policyno = ? ) tmp");
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyno);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            dataList.add(new ObjectMapper().readValue(rs.getString("json"), new TypeReference<BeneficiaryDao>() { }));
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, "getPolicyDetail error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }
    return dataList;
  }

  public List<BeneficiaryDao> findByPolicynoAndPolicytype(String policyno,String policyType) throws Exception {
    log.printDebug(className, "findByPolicynoAndPolicytype policyno : " + policyno + "policyType :" +policyType);
    List<BeneficiaryDao> dataList = new ArrayList<BeneficiaryDao>();
    Connection con = DBConnection.getConnectionPolicy();
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select row_to_json(tmp) as json ");
      sql.append(" from ( select * from mstperson.beneficiary where policyno = ? and policytype = ? ) tmp");
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyno);
        pst.setString(2, policyType);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            dataList.add(new ObjectMapper().readValue(rs.getString("json"), new TypeReference<BeneficiaryDao>() { }));
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, "Error CertImpl getPolicyDetail Exception : " + e.getMessage());
    } finally {
      con.close();
    }
    return dataList;
  }

}
