package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.RiderDao;
import com.thailife.api.policyInfoService.mapper.RiderDetailMapper;
import com.thailife.api.policyInfoService.mapper.RiderMapper;
import com.thailife.api.policyInfoService.model.RiderDetailM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RiderDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";

  public List<RiderDao> findByPolicyNo(String policyNo) throws Exception {
    try (Connection con = DBConnection.getConnectionPolicy()) {
      return findByPolicyNo(policyNo, con);
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, "findByPolicyNo: " + e.getMessage());
      return null;
    }
  }

  public List<RiderDao> findByPolicyNo(String policyNo, Connection con) throws Exception {
    log.printDebug(className, "findByPolicyNo : " + policyNo);
    List<RiderDao> list = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    try {
      PolicyUtil.setDefaultConnection(con);
      sql.append(" select *");
      sql.append(" from mstpolicy.rider r");
      sql.append(" where r.policyno = ?");
      // log.info(String.format("RiderDaoImpl:[findByPolicyNo] sql = %s", sql.toString()));
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyNo);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            list.add(RiderMapper.convertToRiderDao(rs));
          }
        }
      }

    } catch (Exception e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      throw e;
    }
    return list;
  }

  public static ArrayList<RiderDetailM> searchRiderDetailByPolicyno(String policyno) {
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.getConnectionPolicy();
      String query =
          "select ridertype, ridersum, riderpremium, riderstatus,"
              + " riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from unitlink.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from unitlink.rider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return list;
  }

  public static ArrayList<RiderDetailM> searchOLRiderDetailByPolicyno(String policyno) {
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.getConnectionPolicy();
      String query = "select ridertype, ridersum, riderpremium, riderstatus,"
              + " riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from mstpolicy.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from mstpolicy.rider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return list;
  }
}
