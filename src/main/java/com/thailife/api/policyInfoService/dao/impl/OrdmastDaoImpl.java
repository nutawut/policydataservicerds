package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.OrdmastDao;
import com.thailife.api.policyInfoService.mapper.OrdmastMapper;
import com.thailife.api.policyInfoService.mapper.PolicyDetailMapper;
import com.thailife.api.policyInfoService.model.CheckPolicyM;
import com.thailife.api.policyInfoService.model.CheckRiderM;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.StatusDisplay;
import insure.PlanSpec;
import insure.RiderType;
import insure.TLPlan;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import utility.support.DateInfo;
import utility.support.StatusInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Log4j2
@Service
public class OrdmastDaoImpl {
  public OrdmastDao findByPolicyno(String policyno) throws Exception {
    OrdmastDao result = null;
    Connection con =
        DBConnection.con(DBproperties.getDBPolicy(), "");
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select");
      sql.append(" * ");
      sql.append(" from mstpolicy.ordmast where policyno = ? ");

      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policyno);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        result = OrdmastMapper.ordmastMapper(rs);
      }
    } catch (Exception e) {
      log.error(String.format("Error CertImpl getPolicyDetail : %s", e.getMessage()));
    } finally {
      con.close();
    }

    return result;
  }

  public static PolicyDetailM searchPolicyDetailByPolicyno(String policyno) throws SQLException {
    Connection con = null;
    PolicyDetailM policyM = null;
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select policystatus1, policystatus2, policystatusdate1, policystatusdate2,"
              + " oldpolicystatus1, oldpolicystatus2, oldpolicystatusdate1, oldpolicystatusdate2,"
              + " payperiod, duedate, cast(null as numeric) as tpdpremium, cast(null as numeric) as addpremium,"
              // + " payperiod, cast(null as varchar) as duedate, cast(null as numeric) as
              // tpdpremium, cast(null as numeric) as addpremium,"
              + " sum as lifesum, cast(null as numeric) as extratpdpremium, effectivedate, cast(null as numeric) as accidentsum, mode, plancode"
              + " from mstpolicy.ordmast where policyno = ? order by recordtimestamp desc limit 1";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      // System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        policyM = PolicyDetailMapper.policyDetailMapper(rs);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (Throwable e) {
      e.printStackTrace();
    }finally{
      con.close();
    }
    return policyM;
  }

  public static PolicyDetailM searchPolicyDetailByPolicyno(String policyno, Connection con) {
    PolicyDetailM policyM = null;
    try {
      String query = "select policystatus1, policystatus2, policystatusdate1, policystatusdate2,"
              + " oldpolicystatus1, oldpolicystatus2, oldpolicystatusdate1, oldpolicystatusdate2,"
              + " payperiod, duedate, cast(null as numeric) as tpdpremium, cast(null as numeric) as addpremium,"
              + " sum as lifesum, cast(null as numeric) as extratpdpremium, effectivedate, cast(null as numeric) as accidentsum, mode,"
              + " plancode from mstpolicy.ordmast where policyno = ? order by recordtimestamp desc limit 1";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      System.out.println("searchPolicyDetailByPolicyno sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        policyM = PolicyDetailMapper.policyDetailMapper(rs);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return policyM;
  }

  public HashMap<String, CheckPolicyM> searchPolicyAndRiderByListPolicyno(
      ArrayList<String> listpolicy) {
    HashMap<String, CheckPolicyM> map = new HashMap<>();
    StringBuilder query = new StringBuilder();
    query.append("select ord.policyno, ord.plancode, ord.effectivedate,");
    query.append(" ord.policystatus1, ord.policystatusdate1, ord.sum, cast(ord.insuredage as integer),");
    query.append(" ord.duedate, ord.payperiod, r.ridertype, r.ridersum, r.riderpremium, r.riderstatus,");
    query.append(" r.riderstatusdate, r.effectivedate as ridereffectivedate, e.extrapremium");
    query.append(" from mstpolicy.ordmast ord left join mstpolicy.rider r on r.policyno = ord.policyno");
    query.append(" left join mstpolicy.extrapremium e on r.ridertype = e.extratype and e.policyno = ord.policyno");
    query.append(" where ord.policyno in ( ");
    query.append(listpolicy.toString().replace("[", "'").replace("]", "'").replace(", ", "','"));
    query.append(" )");
    try (Connection con = DBConnection.getConnectionPolicy()) {
      //log.debug(String.format("Sql searchPolicyAndRiderByListPolicyno : %s", query));
      try (PreparedStatement pst = con.prepareStatement(query.toString())) {
        try (ResultSet rs = pst.executeQuery()) {

          StatusDisplay statusDisplay = new StatusDisplay();
          while (rs.next()) {
            String policyno = rs.getString("policyno");
            CheckRiderM rider = new CheckRiderM();
            String ridercode = rs.getString("ridertype");
            String riderstatus = rs.getString("riderstatus");
            if (ridercode == null) continue;

            rider.setRidercode(ridercode);
            rider.setRidername(RiderType.getFullName(ridercode));
            rider.setRidersortname(RiderType.getShortName(ridercode));
            rider.setRidersum(rs.getBigDecimal("ridersum"));
            rider.setRiderpremium(rs.getBigDecimal("riderpremium"));
            rider.setRiderstatus(riderstatus);
            rider.setRiderstatusdesc(StatusInfo.riderStatus(riderstatus));
            rider.setRiderstatusdate(rs.getString("riderstatusdate"));
            rider.setStatusdisplay(statusDisplay.getStatusDisplay("RIDER", riderstatus));
            rider.setEffectivedate(rs.getString("ridereffectivedate"));
            rider.setExtrariderpremium(rs.getBigDecimal("extrapremium"));
            if (null == map.get(policyno)) {
              List<CheckRiderM> listofRider = new ArrayList<>();
              listofRider.add(rider);

              String plancode = rs.getString("plancode");
              TLPlan tlplan = PlanSpec.getPlan(plancode);
              String effectivedate = rs.getString("effectivedate");
              String policystatus1 = rs.getString("policystatus1");
              String insureYear = tlplan.endowmentYear(rs.getString("insuredage"));
              String statusdisplay = statusDisplay.getStatusDisplay("OL", policystatus1);
              String statusdesc = PolicyUtil.getStatus1Desc(policystatus1, "OL");
              String payperiod = rs.getString("payperiod");
              String duedatecore =
                  ("").equals(payperiod) || ("0").equals(payperiod) || null == payperiod
                      ? "00000000"
                      : DateInfo.nextMonth(payperiod + "01", 1);
              String duedate =
                  ("").equals(rs.getString("duedate")) || null == rs.getString("duedate")
                      ? duedatecore
                      : rs.getString("duedate");

              CheckPolicyM policy = new CheckPolicyM();
              policy.setPolicyno(policyno);
              policy.setPlancode(plancode);
              policy.setPlanname(tlplan.name());
              policy.setEffectivedate(effectivedate);
              policy.setMaturedate(insureYear);
              policy.setPolicystatusdate1(rs.getString("policystatusdate1"));
              policy.setPolicystatus1(policystatus1);
              policy.setPolicystatus1desc(statusdesc);
              policy.setStatusdisplay(statusdisplay);
              policy.setSum(rs.getBigDecimal("sum"));
              policy.setNextduedate(duedate);
              policy.setList_of_rider(listofRider);
              map.put(policyno, policy);
            } else {
              List<CheckRiderM> listofRider = map.get(policyno).getList_of_rider();
              listofRider.add(rider);
            }
          }
        }
      }
      return map;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
