package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.AnnualincomeDao;
import com.thailife.api.policyInfoService.mapper.AnnualinecomeMapper;
import com.thailife.api.policyInfoService.mapper.MortpolicyMapper;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


@Service
public class AnnualindecomeDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";

  public AnnualincomeDao findByPolicyNo(String policy) throws Throwable {
    log.printDebug(className, "findByPolicyNo : " + policy);
    AnnualincomeDao ann = null;
    StringBuilder sql = new StringBuilder();
    try (Connection con = DBConnection.con(PolicyUtil.getDBPolicy(), "mstpolicy")) {
      sql.append("select * from annualincome");
      sql.append(" where policy = ?");
      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policy);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        ann = AnnualinecomeMapper.convertToAnnualincomeDao(rs);
      }
    } catch (SQLException e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      throw new Throwable(e.getMessage());
    }
    return ann;
  }

  public AnnualincomeDao findUserByPolicyNoAndAnnuityDate(String policyno, String annuitydate)
      throws Throwable {
    log.printDebug(className, "findUserByPolicyNoAndAnnuityDate  policyno : " + policyno + " annuitydate : " + annuitydate);
    AnnualincomeDao ann = null;
    StringBuilder sql = new StringBuilder();
    try (Connection con = DBConnection.con(PolicyUtil.getDBCustomer(), "mstpolicy")) {
      sql.append(" select * from annualincome a");
      sql.append(" where a.policyno = ? and a.annuitydate = ? ");
      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policyno);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        ann = AnnualinecomeMapper.convertToAnnualincomeDao(rs);
      }
    } catch (SQLException e) {
      log.printError(className, "findUserByPolicyNoAndAnnuityDate error SQLException : " + e.getMessage());
      e.printStackTrace();
    } catch (Exception e) {
      log.printError(className, "findUserByPolicyNoAndAnnuityDate error SQLException : " + e.getMessage());
      throw new Throwable(e.getMessage());
    }
    return ann;
  }
}
