package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.thailife.api.policyInfoService.model.subModel.LifeDetailM;
import com.thailife.api.policyInfoService.util.PolicyException;
import lombok.extern.log4j.Log4j;
import manit.M;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.mapper.CertMapper;
import com.thailife.api.policyInfoService.mapper.PolicyDetailMapper;

@Service
public class CertDaoImpl {
  private final static Log log = new Log();
  private static final String className = "CertDaoImpl";
  public CertDao findByPolicynoAndCertno(String policyNo, String certno) throws Exception {
    return getMapPolicyDetail(policyNo,certno);
  }

  public static PolicyDetailM searchPolicyDetailByPolicynoAndCertno(String policyno, String certno) throws SQLException {
    Connection con = null;
    PolicyDetailM policyM = null;
    log.printDebug(className, "searchPolicyDetailByPolicynoAndCertno  policyno: " + policyno + " certno :" + certno);
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query =
          "select statcer as policystatus1, statcer2 as policystatus2,"
              + " statdate1 as policystatusdate1, statdate2 as policystatusdate2,"
              + " oldstatcert1 as oldpolicystatus1, oldstatcert2 as oldpolicystatus2,"
              + " oldstatcertdate1 as oldpolicystatusdate1, oldstatcertdate2 as oldpolicystatusdate2,"
              + " payperiod, duedate, tpdpremium, addpremium, lifesum, extratpdpremium, effectivedate, accidentsum, '' as plancode, mode"
              + " from mortgage.cert where policyno = ? and certno = ? order by recordtimestamp desc limit 1";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, certno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        policyM = PolicyDetailMapper.policyDetailMapper(rs);
      }
    } catch (SQLException e) {
      log.printError(className, "searchPolicyDetailByPolicynoAndCertno error SQLException : " + e.getMessage());
      e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "searchPolicyDetailByPolicynoAndCertno error Throwable : " + e.getMessage());
      e.printStackTrace();
    }finally{
      con.close();
    }
    return policyM;
  }

  public CertDao getMapPolicyDetail(String policyNo, String certNo) throws PolicyException {
    {
      log.printDebug(className, "getMapPolicyDetail  policyNo: " + policyNo + " certNo :" + certNo);
      CertDao certDao = null;
      StringBuilder sql = new StringBuilder();
      sql.append("select row_to_json(tmp) as json ");
      sql.append("from (select * from mortgage.cert where policyno = ? and certno = ? ) tmp ");
      try (Connection con = DBConnection.getConnectionPolicy()) {
        try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
          pst.setString(1, policyNo);
          pst.setString(2, certNo);
          try (ResultSet rs = pst.executeQuery()) {
            ObjectMapper mapper =
                new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            if (rs.next()) {
              certDao = mapper.readValue(rs.getString("json"), new TypeReference<CertDao>() {});
            }
          }
        }
        return certDao;
      } catch (Exception e) {
        log.printError(className, "getMapPolicyDetail error Exception : " + e.getMessage());
        e.printStackTrace();
        throw new PolicyException(e.getMessage());
      }
    }
  }
  
  public CertDao findByIdCertno(String policyNo) throws Exception {
    log.printDebug(className, "findByIdCertno  policyNo: " + policyNo);
    CertDao result = new CertDao();
    Connection con = DBConnection.con(DBproperties.getDBPolicy(), DBproperties.getSchemaMortgage());
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select * ");
      sql.append(" from cert where policyno = ? and certno = ?");

      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policyNo);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result = CertMapper.certMapper(rs);
      }
    } catch (Exception e) {
      log.printError(className, "findByIdCertno error Exception : " + e.getMessage());
    } finally {
      con.close();
    }

    return result;
  }

  public static LifeDetailM searchLifeDetailByPolicyNoAndCertNo(String policyno, String certno) throws Exception {
    LifeDetailM lifeDetailM = new LifeDetailM();
    try (Connection con = DBConnection.getConnectionPolicy()) {
      String query =
              "select c.em, c.lifesum, c.lifepremium, c.statcer, c.effectivedate, c.extrapremium "
                      + " from mortgage.cert c "
                      + " where c.certno = ? and c.policyno = ? ";
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, certno);
        pst.setString(2, policyno);
        System.out.println("sql:" + pst);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            lifeDetailM.setRidersum(rs.getString("lifesum"));
            lifeDetailM.setRiderpremium(rs.getString("lifePremium"));
            lifeDetailM.setRiderstatus(!M.itis(rs.getString("em"), '0')? "S" : "N");
            lifeDetailM.setRiderstatusdate(rs.getString("effectivedate"));
            lifeDetailM.setEffectivedate(rs.getString("effectivedate"));
            lifeDetailM.setExtrariderpremium(rs.getString("extrapremium"));
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception(String.format("searchLifeDetailByPolicyNoAndCertNo| policyNo: %s, certNo: %s", policyno, certno));
    }
    return lifeDetailM;
  }
 
}
