package com.thailife.api.policyInfoService.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import lombok.Data;

@Data
public class DetcertDao {
  private String policyno;
  private String certno;
  private String code;
  private String address1;
  private String address2;
  private String telephoneno;
  private String mariagestatus;
  private String recname1;
  private BigDecimal percent1;
  private BigDecimal relationshipcode1;
  private String recname2;
  private BigDecimal percent2;
  private BigDecimal relationshipcode2;
  private String recname3;
  private BigDecimal percent3;
  private BigDecimal relationshipcode3;
  private String analist;
  private String prmiseno;
  private String prmiseno2;
  private String loantype;
  private String med;
  private String reserve;
  private Timestamp recordtimestamp;
  private Timestamp journaltimestamp;
}
