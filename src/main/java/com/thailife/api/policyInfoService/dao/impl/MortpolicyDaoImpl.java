package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.MortpolicyDao;
import com.thailife.api.policyInfoService.mapper.MortpolicyMapper;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;


@Service
public class MortpolicyDaoImpl {
  private final Log log = new Log();
  private final String className = "MortpolicyDaoImpl";
  public MortpolicyDao findByPolicyno(String policyno) throws Exception {
    log.printDebug(className, "findByPolicyno : " + policyno);
    MortpolicyDao result = new MortpolicyDao();
    Connection con = DBConnection.con(DBproperties.getDBPolicy(), DBproperties.getSchemaMortgage());
    try {
      result = getExecuteQuery(policyno,con);
    } catch (Exception e) {
      log.printError(className, "findByPolicyno error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }
    return result;
  }

  public MortpolicyDao findByPolicyNo(String policyNo, Connection con) throws Exception {
    log.printDebug(className, "findByPolicyNo : " + policyNo);
    try {
      PolicyUtil.setDefaultConnection(con);
      return getExecuteQuery(policyNo,con);
    } catch (Exception e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      throw e;
    }
  }

  private MortpolicyDao getExecuteQuery(String policyNo, Connection con) throws SQLException {
    log.printDebug(className, "getExecuteQuery : " + policyNo);
    MortpolicyDao result = new MortpolicyDao();
    StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append(" * ");
    sql.append(" from mortgage.mortpolicy ");
    sql.append(" where policyno = ?");

    PreparedStatement pst = con.prepareStatement(sql.toString());
    pst.setString(1, policyNo);
    ResultSet rs = pst.executeQuery();
    if (rs.next()) {
      result = MortpolicyMapper.mortpolicyMapper(rs);
    }
    return result;
  }
}
