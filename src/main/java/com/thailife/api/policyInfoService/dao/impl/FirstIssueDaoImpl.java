package com.thailife.api.policyInfoService.dao.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.model.FirstIssueM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Service
public class FirstIssueDaoImpl {
  private final Log log = new Log();
  private final String className = "FirstIssueDaoImpl";
  
  public FirstIssueM getByPolicyNo(String policyNo) throws PolicyException {
    {
      log.printDebug(className, "getByPolicyNo : " + policyNo);
      FirstIssueM firstIssueM = null;
      StringBuilder sql = new StringBuilder();
      sql.append("select row_to_json(tmp) as json ");
      sql.append("from (select * from mstpolicy.firstissue where policyno = ? )tmp ");
      try (Connection con = DBConnection.getConnectionPolicy()) {
        try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
          firstIssueM = new FirstIssueM();
          pst.setString(1, policyNo);
          try (ResultSet rs = pst.executeQuery()) {
            ObjectMapper mapper =
                new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            if (rs.next()) {
              firstIssueM =
                  mapper.readValue(rs.getString("json"), new TypeReference<FirstIssueM>() {});
            }
          }
        }
        return firstIssueM;
      } catch (Exception e) {
        log.printError(className, "getByPolicyNo error Exception : " + e.getMessage());
        e.printStackTrace();
        throw new PolicyException(e.getMessage());
      }
    }
  }
}
