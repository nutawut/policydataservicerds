package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.UniversallifeDao;
import com.thailife.api.policyInfoService.mapper.PolicyDetailMapper;
import com.thailife.api.policyInfoService.mapper.UniversallifeMapper;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;

@Service
public class UniversallifeDaoImpl {
  private static final Log log = new Log();
  private static final String className = "AnnualindecomeDaoImpl";
  public UniversallifeDao findByPolicyno(String policyno) throws Exception {
    log.printDebug(className, "findByPolicyno : " + policyno);
    UniversallifeDao result = null;
    Connection con = DBConnection.con(DBproperties.getDBPolicy(), DBproperties.getSchemaUniversal());
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select");
      sql.append(" * ");
      sql.append(" from universal.universallife where policyno = ? ");
      
      PreparedStatement pst = con
              .prepareStatement(sql.toString());
      pst.setString(1, policyno);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        result = UniversallifeMapper.universallifeMapper(rs);
      }
    }
    catch (Exception e) {
      log.printError(className, "findByPolicyno error SQLException : " + e.getMessage());
    }
    finally {
      con.close();
    }
    
    return result;
  }
  
  public static PolicyDetailM searchPolicyDetailByPolicyno(String policyno) throws SQLException {
    log.printDebug(className, "searchPolicyDetailByPolicyno : " + policyno);
    Connection con = null;
    PolicyDetailM policyM = null;
    try {
      con = DBConnection.con(DBproperties.getDBPolicy(), "");
      String query = "select policystatus1, policystatus2, policystatusdate1, policystatusdate2,"
              + " oldpolicystatus1, oldpolicystatus2, oldpolicystatusdate1, oldpolicystatusdate2,"
              + " payperiod, duedate, cast(null as numeric) as tpdpremium, cast(null as numeric) as addpremium,"
              + " sum as lifesum, cast(null as numeric) as extratpdpremium, effectivedate, cast(null as numeric) as accidentsum,plancode, mode "
              + " from universal.universallife where policyno = ? order by recordtimestamp desc limit 1";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
          policyM = PolicyDetailMapper.policyDetailMapper(rs);
      }
    } catch (SQLException e) {
      log.printError(className, "searchPolicyDetailByPolicyno error SQLException : " + e.getMessage());
        e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "searchPolicyDetailByPolicyno error Throwable : " + e.getMessage());
        e.printStackTrace();
    }finally{
      con.close();
    }
    return policyM;
  }
  
  
}
