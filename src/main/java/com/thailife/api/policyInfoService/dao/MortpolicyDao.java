package com.thailife.api.policyInfoService.dao;

import java.sql.Timestamp;
import lombok.Data;

@Data
public class MortpolicyDao {
  private String policyno;
  private String effectivedate;
  private Timestamp journaltimestamp;
  private String name;
  private String planname;
  private String rate;
  private Timestamp recordtimestamp;
  private String riderplan;
  private String tlbranch;
  private String type;
}
