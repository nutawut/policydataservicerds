package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.UlRiderDao;
import com.thailife.api.policyInfoService.mapper.RiderDetailMapper;
import com.thailife.api.policyInfoService.mapper.UlriderMapper;
import com.thailife.api.policyInfoService.model.RiderDetailM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Service
public class UlRiderDaoImpl {
  private final Log log = new Log();
  private final String className = "UlRiderDaoImpl";

  public List<UlRiderDao> findByPolicyNo(String policyNo) throws Exception {
    log.printDebug(className, "findByPolicyNo : " + policyNo);
    List<UlRiderDao> daoList = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    try (Connection con = DBConnection.con(DBproperties.getDBPolicy(), "universal")) {
      sql.append(" select *");
      sql.append(" from ulrider u");
      sql.append(" where u.policyno = ? ");
      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policyNo);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        daoList.add(UlriderMapper.convertToUlRiderDao(rs));
      }
    } catch (Exception e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return daoList;
  }

  public static ArrayList<RiderDetailM> searchRiderDetailByPolicyno(String policyno) throws Exception {
    Connection con = null;
    ArrayList<RiderDetailM> list = new ArrayList<>();
    try {
      con = DBConnection.getConnectionPolicy();
      String query = "select ridertype, ridersum, riderpremium, riderstatus, riderstatusdate, effectivedate,"
              + " (select sum(extrapremium) from unitlink.extrapremium where policyno = ? and extratype = ridertype) as extrapremium"
              + " from universal.ulrider where policyno = ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderDetailMapper.riderDetailMapper(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }
}
