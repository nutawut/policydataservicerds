package com.thailife.api.policyInfoService.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** The persistent class for the unitlink database table. */
// @Table(schema = "unitlink", name = "unitlink")
@Data
public class UnitLinkDao implements Serializable {
  private static final long serialVersionUID = 1L;

  private String policyno;

  private BigDecimal benefitrate;

  private String branch;

  @JsonProperty(value = "class")
  private String class_;

  private String contactaddressid;

  private String contactstartdate;

  private String duedate;

  private String effectivedate;

  private String hivflag;

  private BigDecimal insuredage;

  private Date journaltimestamp;

  private String localaddressid;

  private String medical;

  private String mode;

  private String nameid;

  private String occupationcode;

  private String occupationtype;

  private String oldpolicystatus1;

  private String oldpolicystatus2;

  private String oldpolicystatusdate1;

  private String oldpolicystatusdate2;

  private String paydate;

  private String payperiod;

  private String plancode;

  private String policystatus1;

  private String policystatus2;

  private String policystatusdate1;

  private String policystatusdate2;

  private String portfoliomodel;

  private Date recordtimestamp;

  private String remark;

  private String rpno;

  private BigDecimal rpppremium;

  private BigDecimal rppsum;

  private String saleschannel;

  private String salesid;

  private String tabianlapseflag;
}
