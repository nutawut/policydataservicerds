package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.ContactPhoneDao;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
@Service

public class ContactPhoneDaoImpl {
  private final Log log = new Log();
  private final String className = "ContactPhoneDaoImpl";
  public ContactPhoneDao findByPhoneId(int phone_id) throws PolicyException {
    {
      log.printDebug(className, "findByPhoneId : " + phone_id);
      ContactPhoneDao contactPhoneDao = null;
      StringBuilder sql = new StringBuilder();
      sql.append("select row_to_json(tmp) as json ");
      sql.append("from (select * from contact.phone where phone_id = ? )tmp ");
      try (Connection con = DBConnection.getConnectionCustomer()) {
        try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
          contactPhoneDao = new ContactPhoneDao();
          pst.setInt(1, phone_id);
          try (ResultSet rs = pst.executeQuery()) {
            ObjectMapper mapper =
                new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            if (rs.next()) {
              contactPhoneDao = mapper.readValue(rs.getString("json"), new TypeReference<ContactPhoneDao>() {});
            }
          }
        }
        return contactPhoneDao;
      } catch (Exception e) {
        log.printError(className, "findByPhoneId error Exception : " + e.getMessage());
        e.printStackTrace();
        throw new PolicyException(e.getMessage());
      }
    }
  }

}
