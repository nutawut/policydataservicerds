package com.thailife.api.policyInfoService.dao.impl.policy;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.model.GuardianM;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Component
public class GuardianJdbc {
    public List<GuardianM> findGuardianByParentidNo(String parentidNo) throws Exception{
        List<GuardianM> result = new ArrayList<>();
        try(Connection con = DBConnection.con(PolicyUtil.getDBPolicy(), "")){
            StringBuilder sql = new StringBuilder();
            sql.append("select * from mstpolicy.guardian where parentidno = ? ");
            PreparedStatement pst = con.prepareStatement(sql.toString());
            pst.setString(1,parentidNo);
            System.out.println("findGuardianByParentidNo SQL : " + pst);
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                GuardianM guardian = new GuardianM();
                guardian.setPolicyno(rs.getString("policyno"));
                guardian.setPrename(rs.getString("prename"));
                guardian.setFirstname(rs.getString("firstname"));
                guardian.setLastname(rs.getString("lastname"));
                guardian.setBirthdate(rs.getString("birthdate"));
                guardian.setParentage(rs.getString("parentage"));
                guardian.setParentsex(rs.getString("parentsex"));
                guardian.setParentclass(rs.getString("parentclass"));
                guardian.setParentidno(rs.getString("parentidno"));
                guardian.setHivflag(rs.getString("hivflag"));
                guardian.setReserve(rs.getString("reserve"));
                result.add(guardian);
            }
        }catch (Exception e){
            throw e;
        }
        return result;
    }

    public String findRefIdGuardianByPolicyNo(String policyNo, Connection conPolicy)throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" select g.parentidno, g.policyno, p.referenceid");
        sql.append(" from mstpolicy.guardian g ");
        sql.append(" left join mstpolicy.ordmast o on o.policyno = g.policyno");
        sql.append(" left join mstperson.name n on o.nameid = n.nameid ");
        sql.append(" left join mstperson.person p on p.personid = n.personid");
        sql.append(" where g.policyno = ? ");
        PreparedStatement pst = conPolicy.prepareStatement(sql.toString());
        pst.setString(1, policyNo);
        System.out.println("findRefIdGuardianByPolicyNo SQL : " + pst);
        ResultSet rs = pst.executeQuery();
        if (rs.next()) {
            return rs.getString("referenceid");
        }
        return null;
    }
}
