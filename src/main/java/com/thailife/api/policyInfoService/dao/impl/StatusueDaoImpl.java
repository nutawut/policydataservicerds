package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.StatusueDao;
import com.thailife.api.policyInfoService.mapper.StatusueMapper;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StatusueDaoImpl {
  private final static Log log = new Log();
  private final static String className = "StatusueDaoImpl";
  
  public static List<StatusueDao> selectByPolicyno(String policyno) {
    log.printDebug(className, "selectByPolicyno : " + policyno);
    List<StatusueDao> list = new ArrayList<>();
    Connection con = null;
    try {
      con = DBConnection.con(PolicyUtil.getDBPos(), "");
      String table = "srvservice.statusue";
      String query = "select * from " + table + " where policyno = ? ";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        StatusueDao statusueM = StatusueMapper.convertToStatusueDao(rs);
        list.add(statusueM);
      }
    } catch (SQLException e) {
      log.printError(className, "selectByPolicyno error SQLException : " + e.getMessage());
      e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "selectByPolicyno error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return list;
  }
}
