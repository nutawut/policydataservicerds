package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class UlExtrapremuimDao {
    private UlExrtraPremuimPKDao id;
    private BigDecimal emrate;

    private BigDecimal eprate;

    private BigDecimal epurate;

    private BigDecimal extrapremium;

    private String journaltimestamp;

    private String mode;
    private String noofyear;
    private String recordtimestamp;
    private String nextpdextrapremium;
}
