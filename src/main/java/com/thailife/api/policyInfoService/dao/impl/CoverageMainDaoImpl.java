package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.mapper.CoverageMainMapper;
import com.thailife.api.policyInfoService.model.CoverageMainM;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CoverageMainDaoImpl {
  private final static Log log = new Log();
  private final static String className = "CoverageMainDaoImpl";
  
  public static ArrayList<CoverageMainM> searchCoverageMByRiderCode(String ridercode, int year) {
    log.printDebug(className, "searchCoverageMByRiderCode : ridercode " + ridercode + "year :" + year);
    ArrayList<CoverageMainM> list = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    try (Connection con = DBConnection.con(PolicyUtil.getDBCustomer(), "")) {
      sql.append(
          "select cmr.coverage_map_rider_year, cmr.coverage_suminsure, cm.coverage_group_code,");
      sql.append(
          " cm.coverage_name_detail_th, cm.coverage_name_detail_eng, cm.coverage_remark_th,");
      sql.append(" cm.coverage_remark_eng from superapp_tli.coverage_map_rider cmr");
      sql.append(
          " inner join superapp_tli.coverage_main cm on cm.coverage_group_code = cmr.coverage_group_code");
      sql.append(" and cmr.rider_code = ? and cmr.coverage_map_rider_year = ?");
      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, ridercode);
      pst.setInt(2, year);
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(CoverageMainMapper.convertToCoverageMainM(rs));
      }
    } catch (Throwable e) {
      log.printError(className, "searchCoverageMByRiderCode error SQLException : " + e.getMessage());
      e.printStackTrace();
    }
    return list;
  }

  public static ArrayList<CoverageMainM> searchCoverageMByRidercodeList(
      ArrayList<String> ridercode, ArrayList<Integer> year) throws Exception {
    log.printDebug(className, "searchCoverageMByRidercodeList ");
    ArrayList<CoverageMainM> list = new ArrayList<>();
    try (Connection con = DBConnection.con(PolicyUtil.getDBCustomer(), "")) {

      StringBuilder sql = new StringBuilder();
      sql.append(
          "select SUM(rs.coverage_suminsure) as coverage_suminsure, rs.coverage_group_code,");
      sql.append(
          " rs.coverage_map_rider_year, rs.coverage_name_detail_th, rs.coverage_name_detail_eng,");
      sql.append(" rs.coverage_remark_th, rs.coverage_remark_eng , rs.\"order\"  from (");
      sql.append(
          " select cmr.coverage_map_rider_year, cmr.coverage_suminsure, cm.coverage_group_code,");
      sql.append(
          " cm.coverage_name_detail_th, cm.coverage_name_detail_eng, cm.coverage_remark_th,");
      sql.append(" cm.coverage_remark_eng , cm.\"order\" from superapp_tli.coverage_map_rider cmr");
      sql.append(
          " inner join superapp_tli.coverage_main cm on cm.coverage_group_code = cmr.coverage_group_code");
      sql.append(" and (");

      for (int i = 0; i < ridercode.size(); i++) {
        sql.append("(cmr.rider_code = '");
        sql.append(ridercode.get(i));
        sql.append("' and cmr.coverage_map_rider_year = ");
        sql.append(year.get(i));
        sql.append(")");
        if (i != ridercode.size() - 1) {
          sql.append("or");
        }
      }
      sql.append(
          ") order by cm.order ) rs group by rs.coverage_group_code, rs.coverage_map_rider_year, rs.coverage_name_detail_th,");
      sql.append(
          " rs.coverage_name_detail_eng, rs.coverage_remark_th, rs.coverage_remark_eng , rs.\"order\" order by rs.\"order\"");

      PreparedStatement pst = con.prepareStatement(sql.toString());
      System.out.println("sql:" + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(CoverageMainMapper.convertToCoverageMainM(rs));
      }
    } catch (Exception e) {
      log.printError(className, "searchCoverageMByRidercodeList error SQLException : " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return list;
  }
}
