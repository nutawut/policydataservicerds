package com.thailife.api.policyInfoService.dao;

import lombok.Data;

@Data
public class PartyPhoneDao {
    public PartyPhoneDao() {}
    
    public PartyPhoneDao(String party_id, String phone_type, String sequence, String phone_id, String start_date,
            String end_date, String is_valid, String is_receive_sms, String create_user_code, String create_time,
            String update_user_code, String last_update, String system_id, String system_key) {
        super();
        this.party_id = party_id;
        this.phone_type = phone_type;
        this.sequence = sequence;
        this.phone_id = phone_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.is_valid = is_valid;
        this.is_receive_sms = is_receive_sms;
        this.create_user_code = create_user_code;
        this.create_time = create_time;
        this.update_user_code = update_user_code;
        this.last_update = last_update;
        this.system_id = system_id;
        this.system_key = system_key;
    }

    private String party_id = ""; 
    private String phone_type = "";      
    private String sequence = ""; 
    private String phone_id = ""; 
    private String start_date = "";      
    private String end_date = ""; 
    private String is_valid = ""; 
    private String is_receive_sms = "";  
    private String create_user_code = "";
    private String create_time = "";     
    private String update_user_code = "";
    private String last_update = "";     
    private String system_id = "";
    private String system_key = "";
        
    
}
