package com.thailife.api.policyInfoService.dao.impl.unitlink;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.UliprctrlDao;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class UliprctrlDaoImpl {
  private final Log log = new Log();
  private final String className = "UliprctrlDaoImpl";
  public List<UliprctrlDao> findByPolicyNo(String policyno, String paydate, Integer limit) {
    log.printDebug(className, "findByPolicyNo : " + policyno);
    List<UliprctrlDao> list = new ArrayList<>();
    Connection con = null;
    try {
      con = DBConnection.getConnectionPolicy();
    
      String query = "select uliprctrl.* "
          + "from unitlink.uliprctrl uliprctrl "
          + "where uliprctrl.policyno = ? "
          + "and currentstatus in ('P', 'B', 'E') "
          + "and to_date(paydate, 'YYYYMMDD') >= to_date(?, 'YYYYMMDD') "
          + "order by uliprctrl.paydate desc limit ?";
      try (PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(query))) {
        pst.setString(1, policyno);
        pst.setString(2, paydate);
        pst.setInt(3, limit);
        
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            UliprctrlDao uliprctrlDao =
                new ObjectMapper()
                    .readValue(rs.getString("json"), new TypeReference<UliprctrlDao>() {});
            list.add(uliprctrlDao);
          }
        }
      }
    } catch (Throwable e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
    return list;
  }
}
