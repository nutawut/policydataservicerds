package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.PartyPhoneDao;
import com.thailife.api.policyInfoService.mapper.PartyPhoneMapper;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
@Service
public class PartyPhoneDaoImpl {
  private final Log log = new Log();
  private final String className = "PartyPhoneDaoImpl";
  public PartyPhoneDao findByPartyidOrderByLastUpdateDesc(Integer partyId) throws SQLException {
    log.printDebug(className, "findByPartyidOrderByLastUpdateDesc : " + partyId);
    Connection con = null;
    try {
        con = DBConnection.con(PolicyUtil.getDBCustomer(), "");
        String table = "party.party_phone";
        String query = "select * from " + table
                + " where party_id = ? order by last_update desc limit 1";
        PreparedStatement pst = con.prepareStatement(query);
        pst.setInt(1, partyId);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
          PartyPhoneDao personM = PartyPhoneMapper.convertToPartyPhoneM(rs);
            
            return personM;
        }
    } catch (SQLException e) {
      log.printError(className, "findByPartyidOrderByLastUpdateDesc error SQLException : " + e.getMessage());
        e.printStackTrace();
    } catch (Throwable e) {
      log.printError(className, "findByPartyidOrderByLastUpdateDesc error Throwable : " + e.getMessage());
      e.printStackTrace();
    } finally {
        con.close();
    }
    return null;
}

}
