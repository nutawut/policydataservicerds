package com.thailife.api.policyInfoService.dao;

import lombok.Data;

@Data
public class StatusueDao {
  public String policyno = "";
  public String cashreturn = "";
  public String cashreturnint = "";
  public String closedate = "";
  public String closeyear = "";
  public String coverdate = "";
  public String extend = "";
  public String extendreturn = "";
  public String journaltimestamp = "";
  public String maturedate = "";
  public String paidup = "";
  public String paycashbranch = "";
  public String payreturndate = "";
  public String recordtimestamp = "";
  public String reserve = "";
  public String startdate = "";
  public String status = "";
  public String submitbranch = "";
}
