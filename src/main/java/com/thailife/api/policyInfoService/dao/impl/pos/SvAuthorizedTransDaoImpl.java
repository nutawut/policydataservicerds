package com.thailife.api.policyInfoService.dao.impl.pos;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

@Service
public class SvAuthorizedTransDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";
  public Map<String, String> findAuthorAndServiceType(String policyNo) throws PolicyException {
    log.printDebug(className, "findAuthorAndServiceType : " + policyNo);
    Map<String, String> map = new HashMap<>();
    String query =
        "select authorizedstatus,servicetype from pos.srvservice.svauthorizedtrans where policyno =?";
    try (Connection con = DBConnection.getConnectionPos()) {
      try (PreparedStatement pst = con.prepareStatement(query)) {
        pst.setString(1, policyNo);
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            map.put("authorizedstatus", rs.getString(1));
            map.put("servicetype", rs.getString(2));
          }
        }
      }
      return map;
    } catch (Exception e) {
      log.printError(className, "findAuthorAndServiceType error SQLException : " + e.getMessage());
      e.printStackTrace();
      throw new PolicyException(e.getMessage());
    }
  }
}
