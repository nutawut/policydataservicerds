package com.thailife.api.policyInfoService.dao.impl.policy;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.model.CheckPolicyActiveBeanM;
import com.thailife.api.policyInfoService.model.SumRiderM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class PolicyDaoImpl {
  private final Log log = new Log();
  private final String className = getClass().getName();

  public JSONObject getPolicyBaseTypeOL(String policyNo, Connection con) throws Exception {
    log.printDebug(
        className, "findDetailPolicy(BaseOL)ByPolicyNo param [policyNo :" + policyNo + "]");
    StringBuilder sql = new StringBuilder();
    sql.append("  select plancode  as planCode");
    sql.append("   , branch    as branch");
    sql.append("   , 'OL'      as policyType");
    sql.append("   , 'OL'      as baseType");
    sqlConditionPayperiod(sql);
    sql.append("  from mstpolicy.ordmast");
    sql.append("  where policyno = ?");
    try {
      PolicyUtil.setDefaultConnection(con);
      try (PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(sql.toString()))) {
        pst.setString(1, policyNo);
        log.printInfo(className, String.format("sql : %s", sql));
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return new JSONObject(rs.getString("json"));
          } else {
            return null;
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, "getPolicyBaseTypeOL error Exception : " + e.getMessage());
      throw e;
    }
  }

  public JSONObject getPolicy(String policyNo, String certNo, Connection con) throws Exception {
    log.printDebug(
        className,
        "findDetailPolicyByPolicyNo param [policyNo :" + policyNo + " | certNo :" + certNo + "]");

    JSONObject res = null;
    StringBuilder sql = new StringBuilder();

    sql.append("  select 'N' as planCode");
    sql.append("   ,'N' as branch ");
    sql.append("   ,'CL' as policyType ");
    sql.append("   ,'CL' as baseType ");
    sql.append("   ,c.payperiod as payPeriod ");
    sql.append("   ,coalesce(map.rcertno, '') as rcertno ");
    sql.append("   ,coalesce(map.rpolicyno, '') as rpolicyno");
    sql.append("   ,c.nameid");
    sql.append("   ,c.effectivedate");
    sql.append("   ,statcer  as policystatus1");
    sql.append("   ,statcer2 as policystatus2");
    sql.append("   ,oldstatcert1 as oldpolicystatus1");
    sql.append("   ,oldstatcert2 as oldpolicystatus2");
    sql.append("   ,statdate1 as policystatusdate1");
    sql.append("   ,statdate2 as policystatusdate2");
    sql.append("   ,lifesum  as sum");
    sql.append("   ,-1       as insuredage");
    sql.append("   ,mode     as mode");
    sql.append("   ,duedate  as duedate");
    sql.append("   ,maturedate as maturedate");
    sql.append("   ,age");
    sql.append("  from mortgage.cert c");
    sql.append("  left join mortgage.certmapping map");
    sql.append("  on c.policyno = map.policyno");
    sql.append("  and c.certno = map.certno");
    sql.append("  where c.certno = ?");
    sql.append("  and c.policyno = ?");
    sql.append("  union");
    sql.append("  select plancode");
    sql.append("  ,branch");
    sql.append("  ,'OL' as policyType");
    sql.append("  ,'IND' as baseType");
    sql.append("  , payperiod");
    sql.append("   , ''    as rcertno");
    sql.append("   , ''    as rpolicyno");
    sqlCondition(sql);
    sql.append("   , '0'   as mode");
    sql.append("   , ''    as duedate");
    sql.append("   , ''    as maturedate");
    sql.append("   , -1    as age");
    sql.append("  from mstpolicy.indmast");
    sql.append("  where policyno = ?");
    sql.append("  union");
    sql.append("  select plancode  as planCode");
    sql.append("   , branch    as branch");
    sql.append("   , 'OL'      as policyType");
    sql.append("   , 'WHL'     as baseType");
    sqlConditionPayperiod(sql);
    sql.append("  from mstpolicy.whlmast");
    sql.append("  where policyno = ?");
    sql.append("  union");
    sql.append("  select plancode  as planCode");
    sql.append("   , branch    as branch");
    sql.append("   , 'ULIP'    as policyType");
    sql.append("   , 'ULIP'    as baseType");
    sql.append("   , payperiod as payPeriod");
    sql.append("   , '' as rcertno");
    sql.append("   , '' as rpolicyno");
    sqlConditionNameId(sql);
    sql.append("   , rppsum    as sum");
    sql.append("   , insuredage");
    sql.append("   , mode");
    sql.append("   , duedate");
    sql.append("   , '' as maturedate");
    sql.append("   , -1 as age");
    sql.append("  from unitlink.unitlink");
    sql.append("  where policyno = ?");
    sql.append("  union");
    sql.append("  select plancode  as planCode");
    sql.append("   , branch    as branch");
    sql.append("   , 'OL'      as policyType");
    sql.append("   , 'UL'      as baseType");
    sql.append("   , payperiod as payPeriod");
    sql.append("   , '' as rcertno");
    sql.append("   , '' as rpolicyno");
    sqlCondition(sql);
    sql.append("   , mode      as mode");
    sql.append("   , duedate");
    sql.append("   , '' as maturedate");
    sql.append("   , -1 as age");
    sql.append("  from universal.universallife");
    sql.append("  where policyno = ?");
    sql.append("  LIMIT 1");

    try {
      PolicyUtil.setDefaultConnection(con);
      try (PreparedStatement pst = con.prepareStatement(PolicyUtil.setRowToJson(sql.toString()))) {
        pst.setString(1, certNo);
        for (int i = 2; i < 7; i++) {
          pst.setString(i, policyNo);
        }
        // log.debug(String.format("sql : %s", sql.toString()));
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            res = new JSONObject(rs.getString("json"));
          }
        }
      }
      // log.info("[End] getPolicy");
    } catch (Exception e) {
      log.printError(className, "getPolicy error Exception : " + e.getMessage());
      throw e;
    }
    return res;
  }

  private void sqlConditionPayperiod(StringBuilder sql) {
    sql.append("   , payperiod as payPeriod");
    sql.append("   , '' as rcertno");
    sql.append("   , '' as rpolicyno");
    sqlCondition(sql);
    sql.append("   , mode");
    sql.append("   , duedate");
    sql.append("   , '' as maturedate");
    sql.append("   , -1 as age");
  }

  private void sqlConditionNameId(StringBuilder sql) {
    sql.append("   , nameid");
    sql.append("   , effectivedate");
    sql.append("   , policystatus1");
    sql.append("   , policystatus2");
    sql.append("   , oldpolicystatus1");
    sql.append("   , oldpolicystatus2");
    sql.append("   , policystatusdate1");
    sql.append("   , policystatusdate2");
  }

  private void sqlCondition(StringBuilder sql) {
    sqlConditionNameId(sql);
    sql.append("   , sum");
    sql.append("   , insuredage");
  }

  public List<SumRiderM> getRiders(String policyNo, String certNo, Connection con)
      throws Exception {
    List<SumRiderM> list = new ArrayList<>();
    log.printDebug(
        className,
        "[Start] getRiders param : [ policyNo : " + policyNo + " | certNo : " + certNo + " ]");
    StringBuilder sql = new StringBuilder();
    sql.append("select ridertype,sum as ridersum ");
    sql.append("from mortgage.certrider ");
    sql.append("where certno = ? and policyno = ? ");
    sql.append("union ");
    sql.append("select ridertype,ridersum ");
    sql.append("from mstpolicy.rider ");
    sql.append("where policyno = ? ");
    sql.append("union ");
    sql.append("select ridertype,ridersum ");
    sql.append("from mstpolicy.indrider ");
    sql.append("where policyno = ? ");
    sql.append("union ");
    sql.append("select ridertype,ridersum ");
    sql.append("from universal.ulrider ");
    sql.append("where policyno = ? ");
    sql.append("union ");
    sql.append("select ridertype,ridersum ");
    sql.append("from unitlink.rider ");
    sql.append("where policyno = ? ");
    try {
      PolicyUtil.setDefaultConnection(con);
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        // log.debug(String.format("Sql : %s", sql.toString()));
        pst.setString(1, certNo);
        for (int i = 2; i < 7; i++) {
          pst.setString(i, policyNo);
        }
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            list.add(new SumRiderM(rs.getString(1), rs.getBigDecimal(2)));
          }
        }
      }
      // log.info("[End] getRiders");
      return list;
    } catch (Exception e) {
      log.printError(className, "getRiders error Exception : " + e.getMessage());
      throw e;
    }
  }

  public static List<CheckPolicyActiveBeanM> findStatusByNameId(String nameId) {
    List<CheckPolicyActiveBeanM> beanMS = new ArrayList<>();
    try (Connection con = DBConnection.getConnectionPolicy()) {
      StringBuilder sql = new StringBuilder();
      sql.append(" select  policystatus1 as policyStatus1, policystatus2 as policyStatus2, ");
      sql.append(" oldpolicystatus1 as oldStatus1, oldpolicystatus2 as oldStatus2, ");
      sql.append(" 'mstpolicy.indmast' as table, ");
      sql.append(" 'OL' as policyType, 'IND' as baseType ");
      sql.append(" from mstpolicy.indmast where nameid = ? ");
      sql.append(" union ");
      sql.append(" select  policystatus1 as policyStatus1, policystatus2 as policyStatus2, ");
      sql.append(" oldpolicystatus1 as oldStatus1, oldpolicystatus2 as oldStatus2, ");
      sql.append(" 'mstpolicy.ordmast' as table, ");
      sql.append(" 'OL' as policyType, 'OL' as baseType ");
      sql.append(" from mstpolicy.ordmast where nameid = ? ");
      sql.append(" union ");
      sql.append(" select  policystatus1 as policyStatus1, policystatus2 as policyStatus2, ");
      sql.append(" oldpolicystatus1 as oldStatus1, oldpolicystatus2 as oldStatus2, ");
      sql.append(" 'mstpolicy.whlmast' as table, ");
      sql.append(" 'OL' as policyType, 'WHL' as baseType ");
      sql.append(" from mstpolicy.whlmast where nameid = ? ");
      sql.append(" union ");
      sql.append(" select  policystatus1 as policyStatus1, policystatus2 as policyStatus2, ");
      sql.append(" oldpolicystatus1 as oldStatus1, oldpolicystatus2 as oldStatus2, ");
      sql.append(" 'unitlink.unitlink' as table, ");
      sql.append(" 'ULIP' as policyType, 'ULIP' as baseType ");
      sql.append(" from unitlink.unitlink where nameid = ? ");
      sql.append(" union ");
      sql.append(" select  policystatus1 as policyStatus1, policystatus2 as policyStatus2, ");
      sql.append(" oldpolicystatus1 as oldStatus1, oldpolicystatus2 as oldStatus2, ");
      sql.append(" 'universal.universallife' as table, ");
      sql.append(" 'OL' as policyType, 'UL' as baseType ");
      sql.append(" from universal.universallife where nameid = ? ");
      sql.append(" union ");
      sql.append(" select  statcer as policyStatus1, statcer2 as policyStatus2, ");
      sql.append(" oldstatcert1 as oldStatus1, oldstatcert2 as oldStatus2, ");
      sql.append(" 'mortgage.cert' as table, ");
      sql.append(" 'CL' as policyType, 'CL' as baseType ");
      sql.append(" from mortgage.cert where nameid = ? ");
      System.out.println("sql = " + sql.toString());
      try (PreparedStatement pst = con.prepareStatement(sql.toString()); ) {
        for (int i = 1; i < 7; i++) {
          pst.setString(i, nameId);
        }
        try (ResultSet rs = pst.executeQuery(); ) {
          while (rs.next()) {
            CheckPolicyActiveBeanM bean = new CheckPolicyActiveBeanM();
            bean.setPolicyStatus1(rs.getString("policyStatus1"));
            bean.setPolicyStatus2(rs.getString("policyStatus2"));
            bean.setOldStatus1(rs.getString("oldStatus1"));
            bean.setOldStatus2(rs.getString("oldStatus2"));
            bean.setTable(rs.getString("table"));
            bean.setPolicyType(rs.getString("policyType"));
            bean.setBassType(rs.getString("baseType"));
            beanMS.add(bean);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return beanMS;
  }

  public static String getPlanCodeByPolicyno(String policyno, String certno, Connection con) {
    try {
      String query =
              "select plancode from mstpolicy.ordmast o where o.policyno = ? "
                      + "union "
                      + "select plancode from mstpolicy.indmast i  where i.policyno = ?"
                      + "union "
                      + "select plancode from mstpolicy.whlmast w  where w.policyno = ?"
                      + "union "
                      + "select plancode from unitlink.unitlink l  where l.policyno = ?"
                      + "union "
                      + "select plancode from universal.universallife u  where u.policyno = ?"
                      + "union "
                      + "select policyno plancode  from mortgage.cert c where c.certno like ? and  c.policyno like ?";
      PreparedStatement pst = con.prepareStatement(query);
      pst.setString(1, policyno);
      pst.setString(2, policyno);
      pst.setString(3, policyno);
      pst.setString(4, policyno);
      pst.setString(5, policyno);
      pst.setString(6, certno);
      pst.setString(7, policyno);
      System.out.println("sql = " + pst);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        return rs.getString("plancode");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return null;
  }

}
