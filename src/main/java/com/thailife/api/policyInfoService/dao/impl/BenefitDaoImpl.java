package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.BenefitDao;
import com.thailife.api.policyInfoService.mapper.BenefitMapper;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
@Service
public class BenefitDaoImpl {
  private final Log log = new Log();
  private final String className = "BenefitDaoImpl";
  private final static String queryI = "select rs.payyear, rs.paydate, rs.amount,"
      + " rs.interest, rs.payflag, rs.typediv, rs.branch from"
      + " (select payyear, paydate, amount, interest, payflag,'เงินปันผล' as typediv, branch"
      + " from mstpolicy.dvmastindi where policyno = :policyno union"
      + " select payyear, paydate, amount, interest, payflag, 'เงินจ่ายคืน' as typediv, branch"
      + " from mstpolicy.paycmastind where policyno = :policyno) rs order by rs.payyear";

  private final static String queryO = "select rs.payyear, rs.paydate, rs.amount,"
        + " rs.interest, rs.payflag, rs.typediv, rs.branch from"
        + " (select payyear, paydate, amount, interest, payflag, 'เงินบำนาญ' as typediv, branch"
        + "  from mstpolicy.annuitymast where policyno = :policyno union"
        + "  select payyear, paydate, amount, interest, payflag, 'เงินปันผล' as typediv, branch"
        + "  from mstpolicy.dvmastordo where policyno = :policyno union"
        + "  select payyear, paydate, amount, interest, payflag,'เงินจ่ายคืน' as typediv, branch"
        + "  from mstpolicy.paycmastord where policyno = :policyno) rs order by rs.payyear";
  
  private final static String queryW = "select rs.payyear, rs.paydate, rs.amount,"
        + " rs.interest, rs.payflag, rs.typediv, rs.branch from"
        + " (select payyear, paydate, amount, interest, payflag, 'เงินปันผล' as typediv, branch"
        + "  from mstpolicy.dvmastwhlw where policyno = :policyno union"
        + "  select payyear, paydate, amount, interest, payflag,'เงินจ่ายคืน' as typediv, branch"
        + "  from mstpolicy.paycmastwhl where policyno = :policyno) rs order by rs.payyear";
  
  private static Map<String, String> query = new HashMap<String, String>() {
    private static final long serialVersionUID = 1L;
    {
        put("I", queryI);
        put("O", queryO);
        put("W", queryW);
    }
  };
  
  public ArrayList<BenefitDao> searchBenefitMByPolicyNo(String policyno, String policyType,Connection con) throws SQLException {
    log.printDebug(className, "searchBenefitMByPolicyno policyno : " + policyno+ " policyType :" +policyType);
    ArrayList<BenefitDao> list = new ArrayList<>();
    try {
      PreparedStatement pst = con
          .prepareStatement(query.get(policyType).replace(":policyno", "'" + policyno + "'"));
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        BenefitDao benefitM = BenefitMapper.benefitMapper(rs);//BenefitUtil.convertToBenefitM(rs);
        list.add(benefitM);
      }
    } catch (Exception e) {
      log.printError(className, "searchBenefitMByPolicyno: " + e.getMessage());
    } finally {
      con.close();
    }
    return list;
  }
}
