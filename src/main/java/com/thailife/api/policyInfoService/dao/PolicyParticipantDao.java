package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Data
public class PolicyParticipantDao {
    private String policy_no;
    private String cert_no;
    private BigDecimal party_id;
    private Integer participant_role;
    private Date issue_date;
    private String create_user_code;
    private Timestamp create_time;
    private String update_user_code;
    private Timestamp last_update;
    private Integer system_id;
    private String system_key;
    private String personid;
}
