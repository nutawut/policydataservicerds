package com.thailife.api.policyInfoService.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import lombok.Data;
@Data
public class LicenseDao {
  private String citizenid;
  private String licenseno;
  private BigDecimal linkno;
  private String issuedate;
  private String expiredate;
  private String paydate;
  private String rhno;
  private BigDecimal amount;
  private String examdate;
  private String examtime;
  private String examno;
  private String examresult;
  private String examplace;
  private String offerdate;
  private String offertype;
  private String offerresult;
  private String offeruserid;
  private String offeruserbranchcode;
  private String offermodifydate;
  private String bookno;
  private String bookdate;
  private BigDecimal powertimes;
  private String remark;
  private String userid;
  private String userbranchcode;
  private String modifydate;
  private Timestamp recordtimestamp;
  private Timestamp journaltimestamp;
}
