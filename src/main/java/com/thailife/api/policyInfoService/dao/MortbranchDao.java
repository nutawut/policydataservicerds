package com.thailife.api.policyInfoService.dao;

import java.sql.Timestamp;
import lombok.Data;

@Data
public class MortbranchDao {
  private String code;
  private String address1;
  private String address2;
  private Timestamp journaltimestamp;
  private String name;
  private String parent;
  private String part;
  private String realcode;
  private Timestamp recordtimestamp;
  private String remark;
  private String reserve;
}
