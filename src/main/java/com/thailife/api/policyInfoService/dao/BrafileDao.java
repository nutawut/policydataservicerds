package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class BrafileDao {
    private String branch;
    private String branchname;
    private String provincecode;
    private String address1;
    private String address2;
    private String phoneno;
    private String groupcolumn;
    private String opened;
    private String opendate;
    private String closeddate;
    private String mainbranch;
    private String recordtimestamp;
    private String journaltimestamp;
}
