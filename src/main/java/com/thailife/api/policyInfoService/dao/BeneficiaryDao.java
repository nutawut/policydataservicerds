package com.thailife.api.policyInfoService.dao;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
@Data
public class BeneficiaryDao {
  
  private String policytype;
  private String policyno;
  private String sequence;
  private String prename;
  private String firstname;
  private String lastname;
  private BigDecimal relationshipcode;
  private BigDecimal percentshare;
  private String personflag;
  private String idno;
  private String birthdate;
  private Date recordtimestamp;
  private Date journaltimestamp;
}
