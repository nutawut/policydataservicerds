package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class DvmastordoDao {
  private String policyno;
  private String payyear;
  private String paydate;
  private String payflag;
  private String amount;
  private String interest;
  private String branch;
  private String reserve;
  private String recordtimestamp;
  private String journaltimestamp;

}
