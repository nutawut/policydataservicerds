package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CertRiderDao {
  private String policyno;
  private String certno;
  private String ridertype;
  private String effectivedate;
  private BigDecimal extrapremium;
  private Date journaltimestamp;
  private BigDecimal premium;
  private Date recordtimestamp;
  private String reserve;
  private String riderstatus;
  private String riderstatusdate;
  private BigDecimal sum;
}
