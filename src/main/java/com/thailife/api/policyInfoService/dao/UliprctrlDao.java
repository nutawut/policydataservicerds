package com.thailife.api.policyInfoService.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@Table(schema = "unitlink", name = "uliprctrl")
public class UliprctrlDao {
	private String rpno;
	private String policyno;
	private String effectivedate;
	private String effectivedatethisperiod;
	private String duedate;
	private String payperiod;
	private String paydate;
	private BigDecimal premium;
	private BigDecimal extraprem;
	private String sysdate;
	private String currentstatus;
	private String originalstatus;
	private String mode;
	private String time;
	private String requestdate;
	private String submitno;
	private String graceperiod;
	private String printeddate;
	private String goodfunddate;
	private String submitbranch;
	private String userid;
	private String policyyear;
	private String policymonth;
	private String reasoncode;
	private String moneyok;
	private Timestamp recordtimestamp;
	private Timestamp journaltimestamp;
}