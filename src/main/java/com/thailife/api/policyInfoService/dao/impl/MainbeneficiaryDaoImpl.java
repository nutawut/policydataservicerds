package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.MainbeneficiaryDao;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;

@Service
public class MainbeneficiaryDaoImpl {
  private final Log log = new Log();
  private final String className = "MainbeneficiaryDaoImpl";
  public MainbeneficiaryDao findByIdPolicynoAndIdCertno(String policyno, String certno) throws Exception {
    MainbeneficiaryDao result = new MainbeneficiaryDao();
    Connection con = DBConnection.getConnectionPolicy();
    log.printDebug(className, "findByIdPolicynoAndIdCertno policyno " + policyno +" certno :" +certno);
    try {
      StringBuilder sql = new StringBuilder();
      sql.append(" select row_to_json(tmp) as json ");
      sql.append(" from ( select * from mortgage.mainbeneficiary where policyno = ? and certno = ? ) tmp");
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyno);
        pst.setString(2, certno);
        try (ResultSet rs = pst.executeQuery()) {
          ObjectMapper mapper =
              new ObjectMapper()
                  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          while (rs.next()) {
            result = mapper.readValue(rs.getString("json"), new TypeReference<MainbeneficiaryDao>() {});
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, "findByIdPolicynoAndIdCertno error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }

    return result;
  }
}
