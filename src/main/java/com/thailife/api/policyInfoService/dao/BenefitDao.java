package com.thailife.api.policyInfoService.dao;

import java.math.BigDecimal;
import lombok.Data;
@Data
public class BenefitDao {
  private int payyear;
  private String paydate;
  private String payflag;
  private BigDecimal amount;
  private BigDecimal interest;
  private String branch;
  private String typediv;
}
