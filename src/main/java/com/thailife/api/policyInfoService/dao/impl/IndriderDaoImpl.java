package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.IndriderDao;
import com.thailife.api.policyInfoService.mapper.IndriderMapper;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class IndriderDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";
  
  public List<IndriderDao> findByPolicyNo(String policyNo, Connection con) throws Exception {
    List<IndriderDao> list = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    try {
      PolicyUtil.setDefaultConnection(con);
      sql.append(" select *");
      sql.append(" from mstpolicy.indrider i");
      sql.append(" where i.policyno = ? ");
      log.printDebug(className, "findByPolicyNo : " + policyNo);
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyNo);
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            list.add(IndriderMapper.convertToIndriderDao(rs));
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return list;
  }
}
