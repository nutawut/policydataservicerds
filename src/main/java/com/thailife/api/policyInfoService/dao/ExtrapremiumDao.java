package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ExtrapremiumDao  {
    private UlExrtraPremuimPKDao id;
    private BigDecimal emrate;
    private BigDecimal epfromem;
    private BigDecimal epunderwrt;
    private BigDecimal extrapremium;
    private String journaltimestamp;
    private String mode;
    private BigDecimal noofyear;
    private String recordtimestamp;
    private String startdate;
}
