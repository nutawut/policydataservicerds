package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.RiderDao;
import com.thailife.api.policyInfoService.dao.UlRiderDao;
import com.thailife.api.policyInfoService.mapper.RiderMapper;
import com.thailife.api.policyInfoService.mapper.UlriderMapper;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UlipRiderDaoImpl {
  private final Log log = new Log();
  private final String className = "AnnualindecomeDaoImpl";

  public List<RiderDao> findByPolicyNo(String policyNo) throws Exception {
    log.printDebug(className, "findByPolicyNo : " + policyNo);
    List<RiderDao> list = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    try (Connection con = DBConnection.con(PolicyUtil.getDBPolicy(), "unitlink")) {
      sql.append(" select *");
      sql.append(" from rider r");
      sql.append(" where r.policyno = ? ");
      PreparedStatement pst = con.prepareStatement(sql.toString());
      pst.setString(1, policyNo);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(RiderMapper.convertToRiderDao(rs));
      }

    } catch (PolicyException e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return list;
  }

  public List<UlRiderDao> findByPolicyNoUlRider(String policyNo) {
    try (Connection con = DBConnection.getConnectionPolicy()) {
      return findByPolicyNoUlRider(policyNo, con);
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, e.getMessage());
      return null;
    }
  }

  public List<UlRiderDao> findByPolicyNoUlRider(String policyNo, Connection con) throws Exception {
    log.printDebug(className, "findByPolicyNoUlRider : " + policyNo);
    List<UlRiderDao> list = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    sql.append(" select *");
    sql.append(" from rider r");
    sql.append(" where r.policyno = ? ");
    try (PreparedStatement pst = con.prepareStatement(sql.toString()); ) {
      pst.setString(1, policyNo);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        list.add(UlriderMapper.convertToUlRiderDao(rs));
      }
    } catch (PolicyException e) {
      log.printError(className, "findByPolicyNo error SQLException : " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return list;
  }
}
