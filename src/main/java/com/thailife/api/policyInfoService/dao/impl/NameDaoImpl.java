package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.NameDao;
import com.thailife.api.policyInfoService.mapper.NameMapper;
import lombok.extern.log4j.Log4j;

@Service
public class NameDaoImpl {
  private final Log log = new Log();
  private final String className = "NameDaoImpl";
  
  public NameDao findByNameid(String nameid) throws Exception {
    log.printDebug(className, "findByNameid : " + nameid);
    NameDao result = new NameDao();
    Connection con = DBConnection.getConnectionPolicy();
    try {
      result = getExecuteQuery(nameid, con);
    } catch (Exception e) {
      log.printError(className, "findByNameid error SQLException : " + e.getMessage());
    } finally {
      con.close();
    }
    return result;
  }

  public NameDao findByNameId(String nameid, Connection con) throws Exception {
    log.printDebug(className, "findByNameId : " + nameid);
    PolicyUtil.setDefaultConnection(con);
    try {
      return getExecuteQuery(nameid, con);
    } catch (Exception e) {
      log.printError(className, "findByNameId error SQLException : " + e.getMessage());
      throw e;
    }
  }

  private NameDao getExecuteQuery(String nameId, Connection con) throws SQLException {
    log.printDebug(className, "getExecuteQuery : " + nameId);
    NameDao result = new NameDao();
    StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append(" * ");
    sql.append(" from mstperson.name ");
    sql.append(" where nameid = ? ");

    PreparedStatement pst = con.prepareStatement(sql.toString());
    pst.setString(1, nameId);
    ResultSet rs = pst.executeQuery();
    if (rs.next()) {
      result = NameMapper.nameMapper(rs);
    }
    return result;
  }
}
