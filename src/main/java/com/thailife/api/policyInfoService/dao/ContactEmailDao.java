package com.thailife.api.policyInfoService.dao;

import java.io.Serializable;

import java.sql.Timestamp;
import lombok.Data;
@Data
public class ContactEmailDao implements Serializable {

    private static final long serialVersionUID = 1L;

    private int emailId;
    private String addrLine;
    private String createUserCode;
    private Timestamp createTime;
    private String updateUserCode;
    private Timestamp lastUpdate;
    private int systemId;
    private String systemKey;

}