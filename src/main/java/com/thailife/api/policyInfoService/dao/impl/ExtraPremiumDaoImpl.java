package com.thailife.api.policyInfoService.dao.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.ExtrapremiumDao;
import com.thailife.api.policyInfoService.mapper.ExtraPremiumMapper;
import com.thailife.api.policyInfoService.util.Log;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExtraPremiumDaoImpl {
  private final String scheme = "mstpolicy.";
  private final String table = "extrapremium";
  private final Log log = new Log();
  private final String className = "ExtraPremiumDaoImpl";

  public List<ExtrapremiumDao> findByIdPolicynoAndIdExtratype(String policyNo, String riderType) {
    try (Connection con = DBConnection.getConnectionPolicy()) {
      return findByIdPolicynoAndIdExtratype(policyNo, riderType, con);
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, e.getMessage());
      return null;
    }
  }

  public List<ExtrapremiumDao> findByIdPolicynoAndIdExtratype(
      String policyNo, String riderType, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM ").append(scheme).append(table);
    sql.append(" WHERE policyno = ? and ridertype = ? ");
    log.printDebug(className, "sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setString(1, policyNo);
      pst.setString(2, riderType);
      try (ResultSet rs = pst.executeQuery()) {
        List<ExtrapremiumDao> tmp = new ArrayList<>();
        while (rs.next()) {
          tmp.add(ExtraPremiumMapper.mapper(rs));
        }
        return tmp;
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("errorMessage: " + e.getMessage());
    }
  }
}
