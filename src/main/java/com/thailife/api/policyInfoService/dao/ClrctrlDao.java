package com.thailife.api.policyInfoService.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;

/** The persistent class for the clrctrl database table. */
// @Table(schema = "receipt", name = "clrctrl")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClrctrlDao {
  @JsonProperty(value = "rpno")
  private String rpno;

  @JsonProperty(value = "policyno")
  private String policyno;

  @JsonProperty(value = "certno")
  private String certno;

  @JsonProperty(value = "payperiod")
  private String payperiod;

  @JsonProperty(value = "effectivedate")
  private String effectivedate;

  @JsonProperty(value = "duedate")
  private String duedate;

  @JsonProperty(value = "paydate")
  private String paydate;

  @JsonProperty(value = "premium")
  private BigDecimal premium;

  @JsonProperty(value = "extraprem")
  private BigDecimal extraprem;

  @JsonProperty(value = "sysdate")
  private String sysdate;

  @JsonProperty(value = "currentstatus")
  private String currentstatus;

  @JsonProperty(value = "originalstatus")
  private String originalstatus;

  @JsonProperty(value = "mode")
  private String mode;

  @JsonProperty(value = "time")
  private String time;

  @JsonProperty(value = "submitno")
  private String submitno;

  @JsonProperty(value = "requestdate")
  private String requestdate;

  @JsonProperty(value = "graceperiod")
  private String graceperiod;

  @JsonProperty(value = "printeddate")
  private String printeddate;

  @JsonProperty(value = "submitbranch")
  private String submitbranch;

  @JsonProperty(value = "userid")
  private String userid;

  @JsonProperty(value = "reasoncode")
  private String reasoncode;

  @JsonProperty(value = "moneyok")
  private String moneyok;

  @JsonProperty(value = "recordtimestamp")
  private Timestamp recordtimestamp;

  @JsonProperty(value = "journaltimestamp")
  private Timestamp journaltimestamp;
}
