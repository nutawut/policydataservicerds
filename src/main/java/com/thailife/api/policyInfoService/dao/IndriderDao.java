package com.thailife.api.policyInfoService.dao;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class IndriderDao {
    private String policyno;
    private String ridertype;
    private String effectivedate;
    private Timestamp journaltimestamp;
    private Timestamp recordtimestamp;
    private BigDecimal riderpremium;
    private String riderstatus;
    private String riderstatusdate;
    private BigDecimal ridersum;
}
