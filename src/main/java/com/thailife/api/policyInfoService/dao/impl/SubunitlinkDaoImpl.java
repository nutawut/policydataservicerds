package com.thailife.api.policyInfoService.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.SubunitlinkDao;
import com.thailife.api.policyInfoService.mapper.SubunitlinkMapper;
import com.thailife.api.policyInfoService.model.SubunitlinkPK;
import com.thailife.api.policyInfoService.util.DBproperties;
import com.thailife.api.policyInfoService.util.Log;
import lombok.extern.log4j.Log4j;

@Service
public class SubunitlinkDaoImpl {
  private final Log log = new Log();
  private final String className = "SubunitlinkDaoImpl";

  public SubunitlinkDao findById(SubunitlinkPK pk) {
    log.printDebug(className, "findById : " + pk);
    SubunitlinkDao result = null;
    try (Connection con = DBConnection.con(DBproperties.getDBPolicy(), "")) {
      StringBuilder sql = new StringBuilder();
      sql.append(" select");
      sql.append(" * ");
      sql.append(" from unitlink.subunitlink ");
      sql.append(" where policyno = ?  and premiumtype = ?");
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, pk.getPolicyno());
        pst.setString(2, pk.getPremiumtype());
        try (ResultSet rs = pst.executeQuery()) {
          while (rs.next()) {
            result = SubunitlinkMapper.subunitlinkMapper(rs);
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, "findById error SQLException : " + e.getMessage());
    }
    return result;
  }

  public SubunitlinkDao findByPolicyNo(String policyNo) {
    StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append(" * ");
    sql.append(" from unitlink.subunitlink ");
    sql.append(" where policyno = ? ");
    log.printDebug(className, sql.toString());
    try (Connection con = DBConnection.getConnectionPolicy()) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyNo);
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return SubunitlinkMapper.subunitlinkMapper(rs);
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, e.getMessage());
      return null;
    }
    return null;
  }
}
