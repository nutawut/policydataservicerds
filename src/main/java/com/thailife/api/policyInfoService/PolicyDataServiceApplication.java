package com.thailife.api.policyInfoService;

import com.thailife.api.policyInfoService.config.DBConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.util.Date;

@SpringBootApplication
public class PolicyDataServiceApplication extends SpringBootServletInitializer
    implements CommandLineRunner {
  @Autowired private Environment env;

  public static void main(String[] args) {
    SpringApplication.run(PolicyDataServiceApplication.class, args);
  }

  @PostConstruct
  public void warmConnection() {
    Connection con = null;
    try {
      con = DBConnection.getConnectionPolicy();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      DBConnection.close(con);
    }
  }

  @Override
  public void run(String... args){
    System.out.println(String.format("Active profile property: %s", env.getProperty("env")));
    System.out.println(String.format("Default date timezone: %s", new Date()));

  }
}
