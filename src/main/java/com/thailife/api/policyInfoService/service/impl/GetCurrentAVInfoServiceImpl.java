package com.thailife.api.policyInfoService.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.thailife.api.policyInfoService.controller.pol13.CurrentAVInfoController;
import com.thailife.log.ExtLogger;
import manit.M;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.IndmastDao;
import com.thailife.api.policyInfoService.dao.OrdmastDao;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.dao.UniversallifeDao;
import com.thailife.api.policyInfoService.dao.WhlmastDao;
import com.thailife.api.policyInfoService.dao.impl.CertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.IndmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.OrdmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UnitLinkDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UniversallifeDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.WhlmastDaoImpl;
import com.thailife.api.policyInfoService.model.CurrentAVM;
import com.thailife.api.policyInfoService.model.request.CurrentAVInfoRequest;
import com.thailife.api.policyInfoService.model.response.CurrentAVInfoResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.GetCurrentAVInfoService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import layout.bean.ws.pos.FundAccountBean;
import lombok.extern.log4j.Log4j;
import manit.rte.Result;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;

@Service
public class GetCurrentAVInfoServiceImpl implements GetCurrentAVInfoService {
  private final ExtLogger log = ExtLogger.create(GetCurrentAVInfoServiceImpl.class);
  private final String className = "GetCurrentAVInfoServiceImpl";
  private final String Service = "GetCurrentAVInfoPol13";
  private String tranId;

  @Autowired OrdmastDaoImpl ordmastDaoImpl;
  @Autowired IndmastDaoImpl indmastDaoImpl;
  @Autowired WhlmastDaoImpl whlmastDaoImpl;
  @Autowired UnitLinkDaoImpl unitLinkDaoImpl;
  @Autowired UniversallifeDaoImpl universallifeDaoImpl;
  @Autowired CertDaoImpl certDaoImpl;

  @Override
  public ServiceResults<CurrentAVInfoResponse> getResponse(CurrentAVInfoRequest req) {
    ServiceResults<CurrentAVInfoResponse> response = new ServiceResults<>();
    WebServiceMsg wServiceMsg = new WebServiceMsg();
    try {

      if (typeOk(req)) {
        response.setSuccess(true);
        response.setResult(getFundAccountByPolicy(req.getPolicyno()));
      } else {
        response.setResult(null);
        response.setSuccess(true);
      }
    } catch (Exception e) {
      log.error(Service, tranId, className, "getResponse Error Exception : " + e.getMessage());
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setSuccess(false);
    }
    return response;
  }

  private boolean typeOk(CurrentAVInfoRequest req) throws Exception {
    String policyno = req.getPolicyno();
    String certno = req.getCertno();
    String type = ProductType.typeGetTable(req.getType());
    log.debug(
        Service,
        tranId,
        className,
        "typeOk == " + policyno + "|" + certno + "|" + type + "|" + req.getType() + "|");

    if (("ordmast").equalsIgnoreCase(type)) {
      OrdmastDao ord = ordmastDaoImpl.findByPolicyno(policyno);
      if (ord.getPolicyno() != null) return true;
    } else if (("indmast").equalsIgnoreCase(type)) {
      IndmastDao ind = indmastDaoImpl.findByPolicyno(policyno);
      if (ind.getPolicyno() != null) return true;
    } else if (("whlmast").equalsIgnoreCase(type)) {
      WhlmastDao whl = whlmastDaoImpl.findByPolicyno(policyno);
      if (whl.getPolicyno() != null) return true;
    } else if (("unitlink").equalsIgnoreCase(type)) {
      UnitLinkDao unitlink = unitLinkDaoImpl.findByPolicyno(policyno);
      if (unitlink.getPolicyno() != null) return true;
    } else if (("universallife").equalsIgnoreCase(type)) {
      UniversallifeDao universallife = universallifeDaoImpl.findByPolicyno(policyno);
      if (universallife.getPolicyno() != null) return true;

    } else if (("mortgage").equalsIgnoreCase(type)) {
      CertDao cert = certDaoImpl.findByPolicynoAndCertno(policyno, certno);
      if (cert.getPolicyno() != null) return true;
    }
    return false;
  }

  private CurrentAVInfoResponse getFundAccountByPolicy(String policyNo) {
    System.out.println("getFundAccountByPolicy == " + policyNo);
    PublicRte.setRemote(true);
    CurrentAVInfoResponse result;
    Result rs =
        PublicRte.getClientResult(
            "blunitlink", "rte.bl.dataservice.pos.RteAVCerrentInfo", policyNo);
//    Result rs =
//            PublicRte.getClientResult(
//                    "10.102.63.50", "rte.bl.dataservice.pos.RteAVCerrentInfo", policyNo);
    log.debug(
        Service, tranId, className, "call blunitlink  : " + rs.status() + "|" + rs.value() + "|");
    if (rs.status() == 0 && rs.value() != null) {
      if (rs.value() instanceof ArrayList) {
//        CurrentAVM current = null;
//        ArrayList<CurrentAVM> current_av_list = null;

        if (((ArrayList) rs.value()).size() > 0) {
          if (rs.value() instanceof ArrayList) {
            CurrentAVM current = null;
            ArrayList<CurrentAVM> current_av_list = null;

            if (((ArrayList) rs.value()).size() > 0) {
              current_av_list = new ArrayList<>();
              ArrayList<FundAccountBean> list = (ArrayList<FundAccountBean>) rs.value();
              for (FundAccountBean fundAccountBean : list) {
                if(M.cmps(fundAccountBean.getInvestmentUnits(), "0") == 0)
                  continue;

                current = new CurrentAVM();
                current.setCode(fundAccountBean.getFundName().replaceAll("(.*)\\((.*)\\)", "$2"));
                current.setName(fundAccountBean.getFundName().replaceAll("(.*)\\(.*\\)", "$1"));
                current.setNavdate(
                        checkExpireNavDate(DateInfo.unformatDate(fundAccountBean.getNavDate())));
                current.setAv(set2Digits(fundAccountBean.getAmount()));
                current.setNav(set4Digits(fundAccountBean.getUnitPrice()));
                current.setUnit(set4Digits(fundAccountBean.getInvestmentUnits()));
                current_av_list.add(current);
              }
              result = new CurrentAVInfoResponse();
              result.setCurrent_av_list(current_av_list);
              System.out.println("getFundAccountByPolicy result " + result);
              return result;
            }
          }
        }
      }
    }
    log.debug(Service, tranId, className, "getFundAccountByPolicy null ");
    return null;
  }

  private BigDecimal set2Digits(String number) {
    try {
      BigDecimal twoDigits = new BigDecimal(number);
      return twoDigits.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    } catch (Exception e) {
      e.printStackTrace();
      return new BigDecimal("0.00");
    }
  }
  private BigDecimal set4Digits(String number) {
    try {
      BigDecimal Digits4 = new BigDecimal(number);
      return Digits4.setScale(4, BigDecimal.ROUND_HALF_EVEN);
    } catch (Exception e) {
      e.printStackTrace();
      return new BigDecimal("0.0000");
    }
  }
  private String checkExpireNavDate(String navDate) {
    String[] nav = navDate.split("/");
    if (("0000").equals(nav[0])) {
      return "00000000";
    } else {
      return navDate;
    }
  }
}
