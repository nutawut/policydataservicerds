package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.controller.pol15.InsertConsentController;
import com.thailife.api.policyInfoService.dao.ContactEmailDao;
import com.thailife.api.policyInfoService.dao.ContactPhoneDao;
import com.thailife.api.policyInfoService.dao.PartyDao;
import com.thailife.api.policyInfoService.dao.PartyPhoneDao;
import com.thailife.api.policyInfoService.dao.impl.*;
import com.thailife.api.policyInfoService.dao.impl.customer.PolicyParticipantDaoImpl;
import com.thailife.api.policyInfoService.model.PartyEmailDao;
import com.thailife.api.policyInfoService.model.PartyPersonM;
import com.thailife.api.policyInfoService.model.request.InsertConsentRequest;
import com.thailife.api.policyInfoService.model.response.InsertConsentResponse;
import com.thailife.api.policyInfoService.model.response.PolicyM;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.InsertConsentService;
import com.thailife.api.policyInfoService.util.*;
import com.thailife.log.ExtLogger;
import com.thailife.log.RequestLog;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InsertConsentServiceImpl implements InsertConsentService {
  private final ExtLogger log = ExtLogger.create(InsertConsentServiceImpl.class);
  private final String className = "InsertConsentServiceImpl";
  private final String Service = "InsertConsent";
  private String tranId;
  
  @Autowired PartyDaoImpl partyDaoImpl;
  @Autowired PersonDaoImpl personDaoImpl;
  @Autowired PartyEmailDaoImpl partyEmailDaoImpl;
  @Autowired PartyPhoneDaoImpl partyPhoneDaoImpl;
  @Autowired ContactPhoneDaoImpl contactPhoneDaoImpl;
  @Autowired ContactEmailDaoImpl contactEmailDaoImpl;
  @Autowired PolicyParticipantDaoImpl policyParticipantDaoImpl;
  @Autowired CallWS callWS;
  
  @Override
  public ServiceResults<InsertConsentResponse> getResponse(InsertConsentRequest req, String tran_Id) {
    ServiceResults<InsertConsentResponse> response = new ServiceResults<InsertConsentResponse>();
    WebServiceMsg wServiceMsg = new WebServiceMsg();
    InsertConsentResponse result = null;
    tranId = tran_Id;
    try{
      Integer partyId = Integer.parseInt(req.getCustomer_id());
      List<PolicyM> policys = req.getPolicy();
      //check owner policy by citizen
      PartyDao partyDoa = partyDaoImpl.findByPartyid(Integer.parseInt(req.getCustomer_id()));
      if(partyDoa == null)
      {
        log.info("InsertConsentServiceImpl","InsertConsent"+ tranId+ "party == null customer_id:" + partyId  + " [ValidateExistingPolicy] ");
        response.setResult(null);
        response.setResponseDescription(wServiceMsg.getMessageByCode("400")[1]);
        response.setResponseCode(wServiceMsg.getMessageByCode("400")[0]);
        response.setSuccess(false);
        return response;
      }

      String citizenId = partyDoa.getGovt_id();
      if (CheckPolicyStatus.ValidateExistingPolicy(req.getPolicy(),
              citizenId)) {
        log.info("InsertConsentServiceImpl", tranId + "CheckPolicyStatus.ValidateExistingPolicy",
                "customer_id:" + partyId  ,  " [ValidateExistingPolicy]");

        response.setResult(null);
        response.setResponseDescription(wServiceMsg.getMessageByCode("CORE-001")[1]);
        response.setResponseCode(wServiceMsg.getMessageByCode("CORE-001")[0]);
        response.setSuccess(true);
        return response;
      }
      for(PolicyM policy : policys) {
        if(policy.getPolicyType().equals("M") || policy.getPolicyType().equals("CL")) {
          if(policy.getPolicyType().equals("M") && !policyParticipantDaoImpl.checkExsitPolicyByPolicyNoAndCertNo(policy.getPolicyNo(), policy.getCertNo())
              .equals(policy.getPolicyType())) {
            response.setResult(null);
            response.setResponseDescription(wServiceMsg.getMessageByCode("400")[1]);
            response.setResponseCode(wServiceMsg.getMessageByCode("400")[0]);
            response.setSuccess(false);
            return response;
          }
        }else {
          if(!policyParticipantDaoImpl.checkExsitPolicyByPolicyNo(policy.getPolicyNo())
              .equals(policy.getPolicyType())) {
            response.setResult(null);
            response.setResponseDescription(wServiceMsg.getMessageByCode("400")[1]);
            response.setResponseCode(wServiceMsg.getMessageByCode("400")[0]);
            response.setSuccess(false);
            
            return response;
          }
        }
      }
      //System.out.println("searchConsentResponse partyId : " + partyId );
      PartyDao party = partyDaoImpl.findByPartyid(partyId);
      //System.out.println("searchConsentResponse party : " + party );
      PartyPersonM person = personDaoImpl.findByPartyid((partyId));
      //System.out.println("searchConsentResponse person : " + person );
      PartyEmailDao partyEmail = partyEmailDaoImpl.findByPartyidOrderByLastUpdateDesc(partyId);
      //System.out.println("searchConsentResponse partyEmail : " + partyEmail );
      PartyPhoneDao partyPhone = partyPhoneDaoImpl.findByPartyidOrderByLastUpdateDesc(partyId);
      //System.out.println("searchConsentResponse partyPhone : " + partyPhone );
     
      String refId = party.getGovt_id();
      String refType = ("THA").equalsIgnoreCase(party.getIssue_country()) ? "R" : "T";
      String pname = req.getPreName();
      String fname = req.getFirstName();
      String lname = req.getLastName();
      String birthDate = person.getBirth_date().toString();
      String mobileNo = "";
      String addrline = "";
      if (null != partyPhone) {
          ContactPhoneDao phone = contactPhoneDaoImpl.findByPhoneId(Integer.parseInt(partyPhone.getPhone_id()));
          if(phone != null)
          mobileNo = phone.getPhoneNumber();
      }
      if (null != partyEmail) {
          ContactEmailDao email = contactEmailDaoImpl.findByEmailId(Integer.parseInt(partyEmail.getEmail_id()));
          if(email != null)
          addrline = email.getAddrLine();
      }

      String context = "/ConsentWs/rest/consent/insert/";
      String request = getRequest(refId, refType, refId, pname, fname, lname, mobileNo, addrline, birthDate, policys);
      String resp = CallWithToken.Consent(context, request);
      log.info(Service, tranId, className, "call ws " + context);
      log.info(Service, tranId, className, "call ws Req" + request);
//      String resp = callWS.callCSVWsWithToken(context, request);
      log.info(Service, tranId, className, "call ws Resp" + resp);
      //String resp = CallWithToken.CSVApplication(context, request);
      //String resp = callWS.callWS("http://10.102.60.112:8280/ConsentWs/rest/consent/insert/", request);
      JSONObject json = new JSONObject(resp);
      String status = (String) json.get("status");
      if (("0").equals(status)) {
        result = new InsertConsentResponse();
        String link = (String) json.get("link");
        result.setLink(link);
        response.setResult(result);
        response.setResponseDescription(wServiceMsg.getMessageByCode("200")[1]);
        response.setResponseCode(wServiceMsg.getMessageByCode("200")[0]);
        response.setSuccess(true);
      }else {
        response.setResult(null);
        response.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        response.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        response.setSuccess(true);
      }
    }catch (Exception e) {
      e.printStackTrace();
      log.error(Service, tranId, className,"Error getResponse : "+ e.getMessage());
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setSuccess(false);
    }
    return response;
  }

  private static String getRequest(String refId, String refType, String taxID, String pname, String fname,
    String lname, String telephone, String email, String bdate, List<PolicyM> policys) {

    String policyreq = "[";
    boolean flag = false;
    for (PolicyM p : policys) {
        policyreq += "{";
        policyreq += "\"policyNo\": \"" + p.getPolicyNo() + "\",";
        policyreq += "\"certNo\": \"" + p.getCertNo() + "\",";
        policyreq += "\"policyType\": \"" + p.getPolicyType() + "\",";
        policyreq += "\"flag\": \"" + p.getFlag() + "\"";
        policyreq += "},";
        flag = true;
    }
    if (flag) {
        policyreq = policyreq.substring(0, policyreq.length() - 1);
    }
    policyreq += "]";
    String request = "{\"referenceID\": \"" + refId + "\", \"referenceType\": \"" + refType + "\", \"taxID\": \""
            + taxID + "\", \"preName\": \"" + pname + "\", \"firstName\": \"" + fname + "\", \"lastName\": \""
            + lname + "\", \"telephone\": \"" + telephone + "\", \"email\": \"" + email + "\", \"birthDate\": \""
            + bdate + "\", \"policy\":" + policyreq + "}\"";
  
    return request;
  }

}
