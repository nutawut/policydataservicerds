package com.thailife.api.policyInfoService.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.*;
import com.thailife.api.policyInfoService.dao.impl.*;
import com.thailife.api.policyInfoService.dao.impl.policy.PolicyDaoImpl;
import com.thailife.api.policyInfoService.model.PartyM;
import com.thailife.api.policyInfoService.model.PolicyM;
import com.thailife.api.policyInfoService.model.SumRiderM;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByCustomerIdResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SearchPolicyByCustomerIdService;
import com.thailife.api.policyInfoService.util.*;
import com.thailife.log.ExtLogger;
import insure.Insure;
import insure.PlanSpec;
import insure.PlanType;
import insure.TLPlan;
import manit.M;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParseException;
import org.springframework.stereotype.Service;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;
import utility.underwrite.BankAssure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

// @Log4j
@Service
public class SearchPolicyByCustomerIdServiceImpl implements SearchPolicyByCustomerIdService {
  private static final ExtLogger log = ExtLogger.create(SearchPolicyByCustomerIdServiceImpl.class);
  private final String className = "SearchPolicyByCustomerIdServiceImpl";
  private final String Service = "SearchPolicyByCustomerId";
  @Autowired private NameDaoImpl nameDaoImpl;
  @Autowired private PersonDaoImpl personDaoImpl;
  @Autowired private MortpolicyDaoImpl mortpolicyDaoImpl;
  @Autowired private CertMappingDaoImpl certMappingDaoImpl;
  @Autowired private PolicyDaoImpl policyDaoImpl;
  @Autowired private DetcertDaoImpl detcertDaoImpl;

  @Value("${PARTY_ENDPOINT}")
  private String partyEndpoint;

  final String NONEDATE = "00000000";

  @Override
  public ServiceResults<SearchPolicyByCustomerIdResponse> getResponse(
      SearchPolicyByCustomerIdRequest request, String tranId) {
    ServiceResults<SearchPolicyByCustomerIdResponse> response = new ServiceResults<>();
    SearchPolicyByCustomerIdResponse result;
    String customerId = request.getCustomer_id();

    try {
      result = initialService(customerId, tranId);
      response.setSuccess(true);
      if (result.getList_of_policy() == null || result.getList_of_policy().isEmpty()) {
        response.setResponseCode("204");
        response.setResult(result);
        response.setResponseDescription("ไม่พบข้อมูลที่ต้องการในระบบ");
      } else {
        response.setResponseCode("200");
        response.setResult(result);
        response.setResponseDescription("ดำเนินการเรียบร้อย");
      }
    } catch (Exception e) {
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | getResponse errorException : %s", Service, className, e.getMessage())));
      e.printStackTrace();
      response.setSuccess(false);
      response.setResponseCode("500");
      response.setResponseDescription("ระบบขัดข้องกรุณาลองใหม่อีกครั้ง");
    }
    return response;
  }

  @Override
  public List<PolicyM> getPolicyNo(String requestToEnquiryParty, String tranId) throws Exception {
    try {
      String path = "/EnquiryPartyServices/searchCustomerByPartyId";
      log.info(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | Call %s%n Request : %s",
                  Service, className, path, requestToEnquiryParty)));
      URL url = new URL(partyEndpoint + "/EnquiryPartyServices/searchCustomerByPartyId");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Content-Type", "application/json");

      OutputStream os = conn.getOutputStream();
      os.write(requestToEnquiryParty.getBytes());
      os.flush();

      if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
        throw new RuntimeException(
            "Failed : HTTP error code : "
                + conn.getResponseCode()
                + " "
                + conn.getResponseMessage());
      }

      BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

      String output;
      StringBuilder text = new StringBuilder();
      while ((output = br.readLine()) != null) {
        text.append(output);
      }
      log.info(
          PolicyUtil.pattenLog(
              tranId, String.format("%s:%s | Call %s%n : %s", Service, className, path, text)));
      List<PolicyM> policyList = new ArrayList<>();
      JSONObject json = new JSONObject(text.toString());
      JSONObject responseStatus = json.getJSONObject("responseStatus");
      if (responseStatus.get("errorCode").equals("200")) {
        JSONObject responseRecord = json.getJSONArray("responseRecord").getJSONObject(0);
        if (!("null").equals(responseRecord.get("policyNumber").toString())) {
          JSONArray policyNumber = responseRecord.getJSONArray("policyNumber");
          for (int i = 0; i < policyNumber.length(); i++) {
            JSONObject s_policy = policyNumber.getJSONObject(i);
            if (s_policy.getInt("participantRole") == 1) {
              policyList.add(
                  new PolicyM(s_policy.getString("policy_No"), s_policy.getString("cert_No")));
            }
          }
        }
      }
      conn.disconnect();
      return policyList;

    } catch (IOException | JsonParseException | JSONException e) {
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | error getPolicyNo Exception: %s", Service, className, e.getMessage())));
      e.printStackTrace();
      throw new PolicyException(e.getMessage());
    }
  }

  private SearchPolicyByCustomerIdResponse initialService(String customerId, String tranId)
      throws Exception {
    SearchPolicyByCustomerIdResponse searchPolicyByCustomerIdResponse =
        new SearchPolicyByCustomerIdResponse();
    try (Connection con = DBConnection.getConnectionPolicy()) {

      PartyM party = new PartyM();
      party.setPartyId(customerId);

      HeaderData headerData = new HeaderData();
      headerData.setMessageId(String.valueOf(System.nanoTime()));
      headerData.setSentDateTime(DateFormat.now(DateFormat.DDMMYYYY_HHMMSS));

      DataServiceRequestT<PartyM> requests = new DataServiceRequestT<>();
      requests.setRequestRecord(party);
      requests.setHeaderData(headerData);
      String requestToEnquiryParty = new ObjectMapper().writeValueAsString(requests);

      List<PolicyM> list = getPolicyNo(requestToEnquiryParty, tranId);
      if (list != null && list.size() > 0) {
        setDetailPolicy(list, con, tranId);
        list = sortDataByGroup(list, tranId);
        searchPolicyByCustomerIdResponse.setList_of_policy(list);
      }
      return searchPolicyByCustomerIdResponse;
    } catch (Exception ex) {
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | error initialService Exception: %s ",
                  Service, className, ex.getMessage())));
      ex.printStackTrace();
      throw ex;
    }
  }

  private void setDetailPolicy(List<PolicyM> list, Connection con, String tranId) throws Exception {
    try {
      if (!list.isEmpty()) {
        PublicRte.setRemote(true);
        StatusDisplay statusDisplay = new StatusDisplay();
        CstPolicyJson constant = new CstPolicyJson();
        for (PolicyM policy : list) {
          String policyno = policy.getPolicyNo();
          String certno = policy.getCertNo();
          JSONObject policyjson = getPolicyData(policyno, certno, con);
          if (policyjson == null) continue;

          NameDao name = nameDaoImpl.findByNameid(policyjson.getString(constant.nameId));
          if (name == null || StringUtils.isBlank(name.getNameid())) continue;
          PersonDao person = personDaoImpl.findByPersonid(name.getPersonid());
          MortpolicyDao mortPolicy = mortpolicyDaoImpl.findByPolicyno(policyno);
          String planCode =
              policyjson.has(constant.planCode)
                  ? policyjson.getString(constant.planCode)
                  : policyno;
          String branch =
              policyjson.has(constant.branch)
                  ? policyjson.get(constant.branch).toString()
                  : utility.underwrite.BankAssure.tlBranchFromPlan(planCode);
          System.out.println("branch==" + branch);
          String baseType =
              policyjson.has(constant.baseType) ? policyjson.getString(constant.baseType) : "";
          String policyType =
              policyjson.has(constant.policyType) ? policyjson.getString(constant.policyType) : "";
          String type = ProductType.plancodeGetType(planCode, branch, policyType, baseType);
          String effectiveDate = policyjson.getString(constant.effectiveDate);

          String status1 =
              policyjson.has(constant.policyStatus1)
                  ? policyjson.getString(constant.policyStatus1)
                  : policyjson.getString(constant.statCer);
          String status2 =
              policyjson.has(constant.policyStatus2)
                  ? policyjson.getString(constant.policyStatus2)
                  : policyjson.getString(constant.statCer2);
          String oldstatus1 =
              policyjson.has(constant.oldPolicyStatus1)
                  ? policyjson.getString(constant.oldPolicyStatus1)
                  : policyjson.getString(constant.oldStatCert1);
          String oldstatus2 =
              policyjson.has(constant.oldPolicyStatus2)
                  ? policyjson.getString(constant.oldPolicyStatus2)
                  : policyjson.getString(constant.oldStatCert2);
          String[] arrstatus =
              PolicyUtil.getStatusForShowCustomer(
                  status1, "", status2, "", oldstatus1, "", oldstatus2, "", true);
          String policystatus1 = arrstatus[0];
          String policystatusdate1 =
              policyjson.has(constant.policyStatusDate1)
                  ? policyjson.getString(constant.policyStatusDate1)
                  : policyjson.getString(constant.statDate1);
          policystatusdate1 = policystatusdate1.trim().equals("") ? "00000000" : policystatusdate1;
          String sum =
              policyjson.has(constant.sum)
                  ? policyjson.getBigDecimal(constant.sum).toString()
                  : policyjson.has(constant.lifeSum)
                      ? policyjson.getBigDecimal(constant.lifeSum).toString()
                      : policyjson.has(constant.rppSum)
                          ? policyjson.get(constant.rppSum).toString()
                          : "0";
          String insuredAge =
              policyjson.has(constant.insuredAge)
                  ? policyjson.getInt(constant.insuredAge) + ""
                  : policyjson.getInt(constant.age) + "";
          String payPeriod =
              policyjson.has(constant.payPeriod)
                  ? policyjson.getString(constant.payPeriod).trim()
                  : "";
          String duedatecore =
              payPeriod.equals("") || payPeriod.equals("0")
                  ? NONEDATE
                  : DateInfo.nextMonth(payPeriod + "01", 1);
          String mode = policyjson.has(constant.mode) ? policyjson.getString(constant.mode) : "";
          String dueDate =
              policyjson.has(constant.dueDate)
                  ? policyjson.getString(constant.dueDate).trim().equals("")
                      ? duedatecore
                      : policyjson.getString(constant.dueDate)
                  : duedatecore;
          if (policystatus1.equals("F")
              || policystatus1.equals("M")
              || policystatus1.equals("T")
              || mode.equals("9")) dueDate = "00000000";
          String policyStatus1desc = PolicyUtil.getStatus1Desc(policystatus1, policyType);
          TLPlan tlplan = PlanSpec.getPlan(planCode);
          String insureYear =
              tlplan == null
                  ? policyjson.has(constant.age) ? policyjson.get(constant.age).toString() : "0"
                  : tlplan.endowmentYear(insuredAge);
          String matureDate;
          if (PlanType.isPAPlan(planCode) || PlanType.isYRT(planCode) || PlanType.isDMPAPlan(planCode)) {
            String yearPeriod = payPeriod.trim().length() > 2 ? payPeriod.substring(0, 2) : "00";
            effectiveDate = PolicyUtil.effectiveData(effectiveDate, yearPeriod);
            effectiveDate =
                (!M.dateok(effectiveDate) ? M.nextdate(effectiveDate, -1) : effectiveDate);
            matureDate =
                Insure.matureDate(effectiveDate, insureYear, planCode, person.getBirthdate());
          } else if (policyType.equals("CL")) {
            effectiveDate = PolicyUtil.calculateEffectiveDateCL(effectiveDate, payPeriod);
            matureDate =
                policyjson.has(constant.matureDate)
                        && StringUtils.isNotEmpty(policyjson.getString(constant.matureDate))
                    ? policyjson.getString(constant.matureDate)
                    : Insure.matureDate(effectiveDate, insureYear, planCode, person.getBirthdate());
          } else {
            matureDate =
                policyjson.has(constant.matureDate)
                        && StringUtils.isNotEmpty(policyjson.getString(constant.matureDate))
                    ? policyjson.getString(constant.matureDate)
                    : Insure.matureDate(effectiveDate, insureYear, planCode, person.getBirthdate());
          }

          policy.setPolicyNo(policyno);
          policy.setPlanCode(planCode);
          policy.setSum(Integer.parseInt(sum));
          policy.setEffectiveDate(effectiveDate);
          policy.setNextDueDate(dueDate);
          policy.setPolicyStatus1(policystatus1);
          policy.setPolicyStatusDate1(policystatusdate1);
          policy.setMatureDate(matureDate);

          String planname, oldCertNo = "", oldPolicyNo = "";
          if (policyType.equals("CL")) {
            planname = getPlanName(planCode);
            planname = planname.equals("") ? mortPolicy.getPlanname() : planname;
            String code = detcertDaoImpl.getCodeByPolicynoAndCertno(policyno, certno, con);
            String rpolicyno =
                certMappingDaoImpl.getRpolicynoByPolicynoAndCertno(policyno, certno, con);
            if (code == null) code = "";
            if (rpolicyno == null) rpolicyno = planCode;

            System.out.println("rpolicyno : " + rpolicyno);
            oldPolicyNo = utility.underwrite.bankassurecomps.PolicyCL.codeToBA(rpolicyno, code);
            if (oldPolicyNo == null) oldPolicyNo = planCode;

          } else {
            planname = tlplan != null ? tlplan.name() : null;
          }
          oldPolicyNo = null == oldPolicyNo ? planCode : oldPolicyNo;
          String setType = type.equalsIgnoreCase("OTHER") ? policyType : type;
          CertMappingDao certMapping =
              certMappingDaoImpl.findByPolicyNoAndCertNo(policyno, certno, con);
          if (null != certMapping) {
            oldCertNo = certMapping.getRcertno();
          }

          policy.setType(setType);
          policy.setScreenType(ProductType.typeGetScreenType(type, policyType, planCode));
          policy.setStatusDisplay(
              statusDisplay.getStatusDisplay(
                  type,
                  policystatus1,
                  policyjson.has(constant.policyStatusDate2)
                      ? policyjson.getString(constant.policyStatusDate2)
                      : ""));
          policy.setGroupPolicyStatus(statusDisplay.getGroupPolicyStatus(type, policystatus1));
          policy.setPolicyStatus1desc(policyStatus1desc);
          policy.setRiderSum(getRiderSum(policyno, certno, con, tranId));
          policy.setPreName(utility.prename.Prename.getAbb(name.getPrename()));
          policy.setFirstname(name.getFirstname());
          policy.setLastname(name.getLastname());
          policy.setPlanName(planname != null ? planname.replace("ตะกาฟุล(", "ตะกาฟุล (") : "");
          policy.setOldCertNo(oldCertNo);
          policy.setOldPolicyNo(oldPolicyNo);
          policy.setPayPeriod(policyjson.getString(constant.payPeriod));

          System.out.println(policy);
        }
      }
    } catch (Exception e) {
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | error setDetailPolicy Exception: %s ",
                  Service, className, e.getMessage())));
      e.printStackTrace();
      throw e;
    }
  }

  public JSONObject getPolicyData(String policyNo, String certNo, Connection con) throws Exception {
    JSONObject policyDetail = policyDaoImpl.getPolicyBaseTypeOL(policyNo, con);
    if (policyDetail != null) {
      return policyDetail;
    } else {
      return policyDaoImpl.getPolicy(policyNo, certNo, con);
    }
  }

  private String getPlanName(String planCode) {
    PublicRte.setRemote(true);
    String bankCode = BankAssure.bankCodeFromPlan(planCode);
    if (bankCode == null) return "";
    return BankAssure.name(bankCode).trim();
  }

  private int getRiderSum(String policyno, String certno, Connection con, String tranId)
      throws Exception {
    log.info(
        PolicyUtil.pattenLog(
            tranId,
            String.format(
                "%s:%s | getRiderSum by policyno = %s certno = %s",
                Service, className, policyno, certno)));
    int sum = 0;
    try {
      List<SumRiderM> list = policyDaoImpl.getRiders(policyno, certno, con);
      for (SumRiderM tmp : list) {
        if (isME(tmp.getType())) sum = sum + tmp.getRiderSum().intValue();
      }
    } catch (Exception e) {
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | error getRiderSum Exception: %s", Service, className, e.getMessage())));
      throw e;
    }
    return sum;
  }

  private boolean isME(String ridertype) {
    return ridertype.equals("ME") || ridertype.equals("ME1") || ridertype.equals("ME2");
  }

  private List<PolicyM> sortDataByGroup(List<PolicyM> list, String tranId) {
    List<PolicyM> list1 = new ArrayList<>();
    List<PolicyM> list2 = new ArrayList<>();
    List<PolicyM> list3 = new ArrayList<>();
    for (PolicyM policy : list) {
      if (policy.getPlanCode() == null) {
        policy.setPlanCode("data not found");
        log.debug(
            PolicyUtil.pattenLog(
                tranId,
                String.format(
                    "%s:%s | data not found planCode by policyNo : %s",
                    Service, className, policy.getPolicyNo())));
        continue;
      }
      switch (policy.getGroupPolicyStatus()) {
        case "1":
          list1.add(policy);
          break;
        case "2":
          list2.add(policy);
          break;
        case "3":
          list3.add(policy);
          break;
      }
    }
    if (!list1.isEmpty()) sortEffectivedate(list1);
    if (!list2.isEmpty()) sortEffectivedate(list2);

    List<PolicyM> newList = new ArrayList<>();
    Stream.of(list1, list2, list3).forEach(newList::addAll);
    return newList;
  }

  private void sortEffectivedate(List<PolicyM> list) { // DESC
    list.sort(
        (a0, a1) ->
            Integer.parseInt(a1.getEffectiveDate()) - Integer.parseInt(a0.getEffectiveDate()));
  }

  private static class CstPolicyJson {
    private final String nameId = "nameid";
    private final String planCode = "plancode";
    private final String branch = "branch";
    private final String baseType = "basetype";
    private final String policyType = "policytype";
    private final String effectiveDate = "effectivedate";
    private final String policyStatus1 = "policystatus1";
    private final String statCer = "statcer";
    private final String policyStatus2 = "policystatus2";
    private final String statCer2 = "statcer2";
    private final String oldPolicyStatus1 = "oldpolicystatus1";
    private final String oldStatCert1 = "oldstatcert1";
    private final String oldPolicyStatus2 = "oldpolicystatus2";
    private final String oldStatCert2 = "oldstatcert2";
    private final String policyStatusDate1 = "policystatusdate1";
    private final String statDate1 = "statdate1";
    private final String policyStatusDate2 = "policystatusdate2";
    private final String sum = "sum";
    private final String lifeSum = "lifesum";
    private final String rppSum = "rppsum";
    private final String insuredAge = "insuredage";
    private final String age = "age";
    private final String payPeriod = "payperiod";
    private final String dueDate = "duedate";
    private final String mode = "mode";
    private final String matureDate = "maturedate";
  }
}
