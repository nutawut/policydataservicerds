package com.thailife.api.policyInfoService.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.thailife.api.policyInfoService.model.ListOfProtection;
import com.thailife.api.policyInfoService.model.TempGetCoverageOverviewM;
import com.thailife.api.policyInfoService.model.request.GetCoverageOverviewRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetCoverageOverviewResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface GetCoverageOverviewService {
  public ServiceResults<ListOfProtection> getResponse(
      RequestMessages<GetCoverageOverviewRequest> request) throws Throwable;

}
