package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.controller.pol17.GetPolicyTypeController;
import com.thailife.api.policyInfoService.dao.impl.OrdmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.customer.PolicyParticipantDaoImpl;
import com.thailife.api.policyInfoService.model.CheckPolicyM;
import com.thailife.api.policyInfoService.model.CheckRiderM;
import com.thailife.api.policyInfoService.model.PolicyInfoM;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.CheckPolicyRiderLifefitResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.CheckPolicyRiderLifefitService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.log.ExtLogger;
import insure.Insure;
import insure.PlanType;
import insure.RiderType;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CheckPolicyRiderLifefitServiceImpl implements CheckPolicyRiderLifefitService {
  private final ExtLogger log = ExtLogger.create(CheckPolicyRiderLifefitServiceImpl.class);
  private final String className = "SearchPolicyOnlyByPartyIdController";
  private final String Service = "SearchPolicyListOnlyByCustomerIdImp";
  private String tranId;
  
  @Autowired PolicyParticipantDaoImpl policyParticipantDaoImpl;
  @Autowired OrdmastDaoImpl ordmastDaoImpl;

  @Override
  public ServiceResults<CheckPolicyRiderLifefitResponse> getResponse(
      SearchPolicyByCustomerIdRequest req, String tran_Id) {
    ServiceResults<CheckPolicyRiderLifefitResponse> response = new ServiceResults<>();
    tranId = tran_Id;
    try {
      CheckPolicyRiderLifefitResponse result =
          checkPolicyRiderLifefit(Long.parseLong(req.getCustomer_id()));
      response.setSuccess(true);
      if (result.getList_of_policy() == null || result.getList_of_policy().isEmpty()) {
        response.setResponseCode("204");
        response.setResult(result);
        response.setResponseDescription("ไม่พบข้อมูลที่ต้องการในระบบ");
      } else {
        response.setResponseCode("200");
        response.setResult(result);
        response.setResponseDescription("ดำเนินการเรียบร้อย");
      }
    } catch (Exception ex) {
      log.error(Service, tranId, className, "error Exception "+ex.getMessage());
      response.setSuccess(false);
      response.setResponseCode("500");
      response.setResponseDescription("ระบบขัดข้องกรุณาลองใหม่อีกครั้ง");
    }
    return response;
  }

  private CheckPolicyRiderLifefitResponse checkPolicyRiderLifefit(long customer_id) {
    CheckPolicyRiderLifefitResponse res = new CheckPolicyRiderLifefitResponse();
    res.setList_of_policy(getListOfPolicyByCustomer(customer_id));
    return res;
  }

  private List<CheckPolicyM> getListOfPolicyByCustomer(long customer_id) {
    try {
      //log.info(String.format("Get Policy By CustomerId :%s", customer_id));
      List<PolicyInfoM> listPolicy = policyParticipantDaoImpl.searchPolicyByPartyId(customer_id);
      ArrayList<String> listPolicyno = new ArrayList<>();
      for (PolicyInfoM info : listPolicy) {
        listPolicyno.add(info.getPolicy_no());
      }
      HashMap<String, CheckPolicyM> mapPolicy =
          ordmastDaoImpl.searchPolicyAndRiderByListPolicyno(listPolicyno);
      List<CheckPolicyM> list = new ArrayList<>();
      BigDecimal ridersum = BigDecimal.ZERO;
      boolean isLifefit = false;
      log.debug(Service, tranId, className,String.format("listPolicy size :%s", listPolicy.size()));
      for (PolicyInfoM info : listPolicy) {
        isLifefit = false;
        CheckPolicyM policy = mapPolicy.get(info.getPolicy_no());
        if (policy == null) {
          continue;
        }
        log.debug(Service, tranId, className, String.format("CheckPolicy Model :%s", policy.toString()));
        String maturedate = Insure.matureDate(policy.getEffectivedate(), policy.getMaturedate(), policy.getPlancode(), info.getBirth_date());
        policy.setPrename(info.getPname_th());
        policy.setFirstname(info.getFname_th());
        policy.setLastname(info.getLname_th());
        policy.setMaturedate(maturedate);
        String plancode = policy.getPlancode();
        if (PlanType.isLifeFit(plancode)) {
          policy.setFlag("T");
          isLifefit = true;
        }
        for (CheckRiderM rider : policy.getList_of_rider()) {
          String ridercode = rider.getRidercode();
          if (RiderType.isLifeFit(ridercode)) {
            rider.setFlag("T");
            isLifefit = true;
          }
          ridersum = ridersum.add(rider.getRidersum());
        }
        String policytype = ProductType.getPolicyType(plancode, "OL");
        String screentype = ProductType.typeGetScreenType("OL", policytype, plancode);
        policy.setScreentype(screentype);
        policy.setRidersum(ridersum);
        if (isLifefit) {
          list.add(policy);
        }
      }
      return list;
    } catch (Exception ex) {
      log.error(Service, tranId, className, " getListOfPolicyByCustomer Error Exception : "+ ex.getMessage());
      throw ex;
    }
  }
}
