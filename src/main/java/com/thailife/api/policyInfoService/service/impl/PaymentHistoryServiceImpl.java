package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.controller.pol12.GetCoverageOverviewController;
import com.thailife.api.policyInfoService.dao.*;
import com.thailife.api.policyInfoService.dao.impl.*;
import com.thailife.api.policyInfoService.dao.impl.conllection.ClrctrlDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.conllection.IrctrlDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.conllection.OrctrlDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.conllection.WrctrlDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.pos.DiscDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.unitlink.UliprctrlDaoImpl;
import com.thailife.api.policyInfoService.model.*;
import com.thailife.api.policyInfoService.model.request.PaymentHistoryRequest;
import com.thailife.api.policyInfoService.model.response.PaymentHistoryResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PaymentHistoryService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import com.thailife.log.ExtLogger;
import insure.Insure;
import manit.M;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utility.personnel.personnel.TLOrganizeInfo;
import utility.personnel.support.ZArray;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PaymentHistoryServiceImpl implements PaymentHistoryService {
  private final ExtLogger log = ExtLogger.create(PaymentHistoryServiceImpl.class);
  private final String className = "PaymentHistoryController";
  private final String Service = "GetPaymentHistoryPol11";
  private String tranId;

  @Autowired UlrctrlDaoImpl ulrctrlDaoImpl;
  @Autowired UliprctrlDaoImpl uliprctrlDaoImpl;
  @Autowired OrdmastDaoImpl ordmastDaoImpl;
  @Autowired IndmastDaoImpl indmastDaoImpl;
  @Autowired WhlmastDaoImpl whlmastDaoImpl;
  @Autowired UniversallifeDaoImpl universallifeDaoImpl;
  @Autowired UnitLinkDaoImpl unitLinkDaoImpl;
  @Autowired CertDaoImpl certDaoImpl;
  @Autowired FirstIssueDaoImpl firstIssueDaoImpl;
  @Autowired OrctrlDaoImpl orctrlDaoImpl;
  @Autowired DiscDaoImpl discDaoImpl;
  @Autowired IrctrlDaoImpl irctrlDaoImpl;
  @Autowired WrctrlDaoImpl wrctrlDaoImpl;
  @Autowired ClrctrlDaoImpl clrctrlDaoImpl;
  private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy", new Locale("th", "TH"));
  private final SimpleDateFormat sdfMonth = new SimpleDateFormat("MM", new Locale("th", "TH"));
  private final SimpleDateFormat sdfDay = new SimpleDateFormat("dd", new Locale("th", "TH"));
  private final SimpleDateFormat sdfCond = new SimpleDateFormat("yyyyMMdd");
  private final Date date = new Date();
  private final Integer currentyear = Integer.parseInt(sdf.format(date));
  private final Integer currentMonth = Integer.parseInt(sdfMonth.format(date));
  private final Integer currentDay = Integer.parseInt(sdfDay.format(date));

  @Override
  public ServiceResults<PaymentHistoryResponse> getPaymentHistory(
      PaymentHistoryRequest req, String tran_Id) throws Exception {
    tranId = tran_Id;
    ServiceResults<PaymentHistoryResponse> paymentHistoryResponse = new ServiceResults<>();
    PaymentHistoryResponse result = new PaymentHistoryResponse();
    WebServiceMsg wServiceMsg = new WebServiceMsg();
    List<PaymentM> listPayment = new ArrayList<>();
    String policyno = req.getPolicyno();
    String certno = req.getCertno();
    String type = ProductType.typeGetTable(req.getType());
    String effectivedate = req.getEffectivedate();
    try {

      FirstIssueM f = firstIssueDaoImpl.getByPolicyNo(policyno);
      log.debug(
          Service,
          tranId,
          className,
          "type : " + type + " f : " + policyno + "|" + effectivedate + "|" + f);

      if (effectivedate != null
          && effectivedate.trim().length() > 0
          && f != null
          && f.getFirstpaydate() != null
          && f.getFirstpaydate().trim().length() > 0
          && M.cmps(f.getFirstpaydate(), effectivedate) == -1) effectivedate = f.getFirstpaydate();
      log.debug(Service, tranId, className, "f : " + policyno + "|" + effectivedate + "|" + f);
      int limit = 24;
      boolean isLimit = true;

      if (("ordmast").equalsIgnoreCase(type)) {
        OrdmastDao ordmast = ordmastDaoImpl.findByPolicyno(policyno);
        if (ordmast.getEffectivedate() != null) {
          if (effectivedate != null && effectivedate.trim().length() > 0 && f == null)
            effectivedate = ordmast.getEffectivedate();
          PrepareDateM prepareDateM = prepareDate(ordmast.getPaydate(), ordmast.getEffectivedate());
          Calendar cal = Calendar.getInstance();
          if (null != effectivedate && !("").equals(effectivedate)) {
            effectivedate = getEffectiveDate(effectivedate, prepareDateM, cal);
            String yearReq = effectivedate.substring(0, 4);
            int len = (prepareDateM.getYear() - Integer.parseInt(yearReq)) + 3;
            prepareDateM.setYearTable(new String[len]);
            prepareDateM.getYearTable()[0] = "";
            for (int i = 0; i < len - 1; i++) {
              prepareDateM.getYearTable()[i + 1] = String.valueOf(prepareDateM.getYear() - i);
            }
            prepareDateM.setPayDateCond(effectivedate);
            isLimit = false;
            limit = 999;
          }
          try (Connection conCol = DBConnection.getConnectionCollection()) {
            for (int i = 0; i < prepareDateM.getYearTable().length; i++) {
              boolean istable =
                  orctrlDaoImpl.checkTable(String.valueOf(prepareDateM.getYearTable()[i]), conCol);
              if (istable) {
                List<OrctrlM> orctrlM =
                    orctrlDaoImpl.selectByPolicyNo(
                        policyno,
                        (String.valueOf(prepareDateM.getYearTable()[i])),
                        limit,
                        prepareDateM.getPayDateCond(),
                        conCol);
                for (OrctrlM item : orctrlM) {
                  log.debug(Service, tranId, className, "item : " + item.getPayperiod());
                  PaymentM payment = new PaymentM();
                  String payPeriod = item.getPayperiod();
                  payPeriod = getPayPeriod(payPeriod);
                  String effectiveDate = ordmast.getEffectivedate();
                  String duedate =
                      ("00000000").equals(effectiveDate)
                          ? effectiveDate
                          : item.getPayperiod() + "01";
                  //   : Insure.dueDate(item.getMode(), effectiveDate, item.getPayperiod());
                  int extrapremium = item.getExtraprem() == null ? 0 : item.getExtraprem();
                  int discountpremium = getDiscountLifeFit(item.getRpno());
                  int totalpremium = item.getPremium() - discountpremium;
                  int premium = item.getPremium() - extrapremium;
                  String branch = TLOrganizeInfo.getBranchNameAll(item.getSubmitbranch());
                  setPaymentM(
                      payment,
                      payPeriod,
                      effectiveDate,
                      item.getPaydate(),
                      item.getRpno(),
                      premium,
                      extrapremium,
                      discountpremium,
                      totalpremium,
                      branch,
                      duedate,
                      item.getSysdate(),
                      item.getMode(),
                      "",
                      item.getSubmitno());
                  listPayment.add(payment);
                  limit--;
                  if (limit == 0 && isLimit) {
                    break;
                  }
                }
              }
            }
          }
        }
      } else if (("indmast").equalsIgnoreCase(type)) {
        IndmastDao indmast = indmastDaoImpl.findByPolicyno(policyno);
        if (indmast != null) {
          PrepareDateM prepareDateM = prepareDate(indmast.getPaydate(), indmast.getEffectivedate());
          Calendar cal = Calendar.getInstance();
          if (null != effectivedate && !("").equals(effectivedate)) {
            effectivedate = getEffectiveDate(effectivedate, prepareDateM, cal);
            String yearReq = effectivedate.substring(0, 4);
            int len = (prepareDateM.getYear() - Integer.parseInt(yearReq)) + 2;
            prepareDateM.setYearTable(new String[len]);
            prepareDateM.getYearTable()[0] = "";
            for (int i = 0; i < len - 1; i++) {
              prepareDateM.getYearTable()[i + 1] = String.valueOf(prepareDateM.getYear() - i);
            }
            prepareDateM.setPayDateCond(effectivedate);
            isLimit = false;
            limit = 999;
          }
          for (int i = 0; i < prepareDateM.getYearTable().length; i++) {
            boolean istable =
                irctrlDaoImpl.checkTable(String.valueOf(prepareDateM.getYearTable()[i]));
            if (istable) {
              List<IrctrlM> irctrlM =
                  IrctrlDaoImpl.selectByPolicyNo(
                      policyno,
                      (String.valueOf(prepareDateM.getYearTable()[i])),
                      limit,
                      prepareDateM.getPayDateCond());
              for (IrctrlM item : irctrlM) {
                PaymentM payment = new PaymentM();
                String payPeriod = item.getPayperiod();
                payPeriod = getPayPeriod(payPeriod);
                String effectiveDate = indmast.getEffectivedate();
                String duedate =
                    ("00000000").equals(effectiveDate)
                        ? effectiveDate
                        : Insure.dueDate("9", effectiveDate, item.getPayperiod());

                Integer extrapremium = null;
                Integer discountpremium = getDiscountLifefit(item.getRpno());
                Integer totalpremium = item.getPremium().intValue() - discountpremium;
                Integer premium =
                        item.getPremium().intValue() - (extrapremium == null ? 0 : extrapremium);
                String branch = TLOrganizeInfo.getBranchNameAll(item.getSubmitbranch());
                setPaymentM(
                    payment,
                    payPeriod,
                    effectiveDate,
                    item.getPaydate(),
                    item.getRpno(),
                    premium,
                    extrapremium,
                    discountpremium,
                    totalpremium,
                    branch,
                    duedate,
                    item.getSysdate(),
                    "0",
                    "",
                    item.getSubmitno());
                listPayment.add(payment);
                limit--;
                if (limit == 0 && isLimit) {
                  break;
                }
              }
              if (limit == 0 && isLimit) {
                break;
              }
            }
          }
        }
      } else if (("whlmast").equalsIgnoreCase(type)) {
        WhlmastDao whlmast = whlmastDaoImpl.findByPolicyno(policyno);
        if (whlmast != null) {
          PrepareDateM prepareDateM = prepareDate(whlmast.getPaydate(), whlmast.getEffectivedate());
          Calendar cal = Calendar.getInstance();
          if (null != effectivedate && !("").equals(effectivedate)) {
            effectivedate = getEffectiveDate(effectivedate, prepareDateM, cal);
            String yearReq = effectivedate.substring(0, 4);
            int len = (prepareDateM.getYear() - Integer.parseInt(yearReq)) + 2;
            prepareDateM.setYearTable(new String[len]);
            prepareDateM.getYearTable()[0] = "";
            for (int i = 0; i < len - 1; i++) {
              prepareDateM.getYearTable()[i + 1] = String.valueOf(prepareDateM.getYear() - i);
            }
            prepareDateM.setPayDateCond(effectivedate);
            isLimit = false;
            limit = 999;
          }
          for (int i = 0; i < prepareDateM.getYearTable().length; i++) {
            boolean istable =
                wrctrlDaoImpl.checkTable(String.valueOf(prepareDateM.getYearTable()[i]));
            if (istable) {
              List<WrctrlM> wrctrlM =
                  WrctrlDaoImpl.selectByPolicyNo(
                      policyno,
                      (String.valueOf(prepareDateM.getYearTable()[i])),
                      limit,
                      prepareDateM.getPayDateCond());
              for (WrctrlM item : wrctrlM) {
                PaymentM payment = new PaymentM();
                String payPeriod = item.getPayperiod();
                payPeriod = getPayPeriod(payPeriod);
                String effectiveDate = whlmast.getEffectivedate();
                String duedate =
                    ("00000000").equals(effectiveDate)
                        ? effectiveDate
                        : Insure.dueDate(item.getMode(), effectiveDate, item.getPayperiod());
                Integer extrapremium = item.getExtraprem();
                Integer discountpremium = getDiscountLifeFit(item.getRpno());
                Integer totalpremium = item.getPremium() - discountpremium;
                Integer premium = item.getPremium() - (extrapremium == null ? 0 : extrapremium);
                String branch = TLOrganizeInfo.getBranchNameAll(item.getSubmitbranch());
                setPaymentM(
                    payment,
                    payPeriod,
                    effectiveDate,
                    item.getPaydate(),
                    item.getRpno(),
                    premium,
                    extrapremium,
                    discountpremium,
                    totalpremium,
                    branch,
                    duedate,
                    item.getSysdate(),
                    item.getMode(),
                    "",
                    item.getSubmitno());
                listPayment.add(payment);
                limit--;
                if (limit == 0 && isLimit) {
                  break;
                }
              }
              if (limit == 0 && isLimit) {
                break;
              }
            }
          }
        }
      } else if (("universallife").equalsIgnoreCase(type)) {
        log.debug(Service, tranId, className, "universallife " + policyno + "|");
        UniversallifeDao universallife = universallifeDaoImpl.findByPolicyno(policyno);
        if (universallife != null) {
          log.debug(
              Service,
              tranId,
              className,
              "universallife != null " + universallife + "|" + universallife.getPaydate() + "|");
          String paydateMaster = universallife.getPaydate();
          Integer year = Integer.parseInt(paydateMaster.substring(0, 4));
          if (year == 0) {
            year = currentyear;
          }
          String effectivedateMaster = universallife.getEffectivedate();
          String paydateCond =
              (year - 2) + paydateMaster.substring(4, 6) + paydateMaster.substring(6, 8);
          log.debug(
              Service,
              tranId,
              className,
              "universallife effectivedateMaster:" + effectivedateMaster);
          log.debug(Service, tranId, className, "universallife paydateCond:" + paydateCond);

          if (null != effectivedate && !("").equals(effectivedate)) {
            if (!(!effectivedateMaster.equals(effectivedate)
                && Integer.parseInt(effectivedateMaster) != 0)) {
              Date effectivedateTmp = new SimpleDateFormat("yyyyMMdd").parse(effectivedate);
              Calendar cal = Calendar.getInstance();
              cal.setTime(effectivedateTmp);
              cal.add(Calendar.YEAR, -1);
              effectivedateTmp = cal.getTime();
              effectivedate = sdfCond.format(effectivedateTmp);
            }
            paydateCond = effectivedate;
            isLimit = false;
            limit = 999;
          }
          log.debug(
              Service,
              tranId,
              className,
              "universallife findByPolicyno :" + policyno + "|" + paydateCond + "|" + limit + "|");
          List<UlrctrlDao> ulrctrl = ulrctrlDaoImpl.findByPolicyNo(policyno, paydateCond, limit);

          log.debug(
              Service,
              tranId,
              className,
              "universallife ulrctrl :"
                  + ulrctrl
                  + "|"
                  + (ulrctrl != null ? ulrctrl.size() : null)
                  + "|");
          for (UlrctrlDao item : ulrctrl) {
            PaymentM payment = new PaymentM();
            String payPeriod = item.getPayperiod();
            payPeriod = getPayPeriod(payPeriod);
            Integer extrapremium = item.getExtraprem().intValue();
            Integer discountpremium = getDiscountLifeFit(item.getRpno());
            Integer totalpremium = item.getPremium().intValue() - discountpremium;
            Integer premium =
                item.getPremium().intValue() - (extrapremium == null ? 0 : extrapremium);
            String branch = TLOrganizeInfo.getBranchNameAll(item.getSubmitbranch());
            setPaymentM(
                payment,
                payPeriod,
                universallife.getEffectivedate(),
                item.getPaydate(),
                item.getRpno(),
                premium,
                extrapremium,
                discountpremium,
                totalpremium,
                branch,
                item.getDuedate(),
                item.getSysdate(),
                item.getMode(),
                "",
                item.getSubmitno());
            listPayment.add(payment);
            limit--;
            if (limit == 0 && isLimit) {
              break;
            }
          }
          System.out.println(
              "universallife listPayment :"
                  + listPayment
                  + "|"
                  + (listPayment != null ? listPayment.size() : null)
                  + "|");
        }
      } else if (("unitlink").equalsIgnoreCase(type)) {
        UnitLinkDao unitlink = unitLinkDaoImpl.findByPolicyno(policyno);
        if (unitlink != null) {
          String paydateMaster = unitlink.getPaydate();
          Integer year = Integer.parseInt(paydateMaster.substring(0, 4));
          if (year == 0) {
            year = currentyear;
          }
          String effectivedateMaster = unitlink.getEffectivedate();
          String paydateCond =
              (year - 2) + paydateMaster.substring(4, 6) + paydateMaster.substring(6, 8);
          if (null != effectivedate && !("").equals(effectivedate)) {
            if (!(!effectivedateMaster.equals(effectivedate)
                && Integer.parseInt(effectivedateMaster) != 0)) {
              Date effectivedateTmp = new SimpleDateFormat("yyyyMMdd").parse(effectivedate);
              Calendar cal = Calendar.getInstance();
              cal.setTime(effectivedateTmp);
              cal.add(Calendar.YEAR, -1);
              effectivedateTmp = cal.getTime();
              effectivedate = sdfCond.format(effectivedateTmp);
            }
            paydateCond = effectivedate;
            isLimit = false;
            limit = 999;
          }
          List<UliprctrlDao> uliprctrl =
              uliprctrlDaoImpl.findByPolicyNo(policyno, paydateCond, limit);
          for (UliprctrlDao item : uliprctrl) {
            PaymentM payment = new PaymentM();
            String payPeriod = item.getPayperiod();
            payPeriod = getPayPeriod(payPeriod);
            Integer extrapremium = item.getExtraprem().intValue();
            Integer discountpremium = getDiscountLifeFit(item.getRpno());
            Integer totalpremium = item.getPremium().intValue() - discountpremium;
            Integer premium =
                item.getPremium().intValue() - (extrapremium == null ? 0 : extrapremium);

            String branch = TLOrganizeInfo.getBranchNameAll(item.getSubmitbranch());
            setPaymentM(
                payment,
                payPeriod,
                unitlink.getEffectivedate(),
                item.getPaydate(),
                item.getRpno(),
                premium,
                extrapremium,
                discountpremium,
                totalpremium,
                branch,
                item.getDuedate(),
                item.getSysdate(),
                item.getMode(),
                "",
                item.getSubmitno());
            listPayment.add(payment);
            limit--;
            if (limit == 0 && isLimit) {
              break;
            }
          }
        }
      } else if (("mortgage").equalsIgnoreCase(type)) { // CL
        CertDao cert = certDaoImpl.getMapPolicyDetail(policyno, certno);
        System.out.println("POL_DB_11  cert : " + cert);
        if (cert != null) {
          String paydateMaster = cert.getPaydate1();
          if (("00000000").equals(paydateMaster)) {
            paydateMaster = clrctrlDaoImpl.getPayDate(policyno, certno);
          }
          int year = Integer.parseInt(paydateMaster.substring(0, 4));
          int month = Integer.parseInt(paydateMaster.substring(4, 6));
          int day = Integer.parseInt(paydateMaster.substring(6, 8));
          String effectivedateMaster = cert.getEffectivedate();
          log.debug(
              Service,
              tranId,
              className,
              "POL_DB_11 CL effectivedateMaster : "
                  + effectivedateMaster
                  + "|"
                  + year
                  + "|"
                  + month
                  + "|"
                  + day
                  + "|");

          Date paydateM = new SimpleDateFormat("yyyy/MM/dd").parse(year + "/" + month + "/" + day);
          Calendar cal = Calendar.getInstance();
          cal.setTime(paydateM);
          cal.add(Calendar.YEAR, -2);
          Date paydateAgo = cal.getTime();
          String paydateCond = sdfCond.format(paydateAgo);
          System.out.println(
              "POL_DB_11 CL paydateCond : " + paydateCond + "|paydateAgo:" + paydateAgo + "|");
          String[] yearTable =
              new String[] {
                "", String.valueOf(year), String.valueOf(year - 1), String.valueOf(year - 2)
              };
          ZArray.show(yearTable);
          if (null != effectivedate && !("").equals(effectivedate)) {
            if (!(!effectivedateMaster.equals(effectivedate)
                && Integer.parseInt(effectivedateMaster) != 0)) {
              Date effectivedateTmp = new SimpleDateFormat("yyyyMMdd").parse(effectivedate);
              cal.setTime(effectivedateTmp);
              cal.add(Calendar.YEAR, -1);
              effectivedateTmp = cal.getTime();
              effectivedate = sdfCond.format(effectivedateTmp);
            }
            String yearReq = effectivedate.substring(0, 4);
            int len = (year - Integer.parseInt(yearReq)) + 2;
            log.debug(Service, tranId, className, "yearReq:" + yearReq + "|len:" + len + "|");
            yearTable = new String[len];
            yearTable[0] = "";
            for (int i = 0; i < len - 1; i++) {
              yearTable[i + 1] = String.valueOf(year - i);
            }
            paydateCond = effectivedate;
            isLimit = false;
            limit = 999;
          }
          log.debug(
              Service,
              tranId,
              className,
              "POL_DB_11 CL yearTable : "
                  + yearTable.length
                  + "|effectivedate:"
                  + effectivedate
                  + "|effectivedateMaster:"
                  + effectivedateMaster
                  + "|");
          for (String s : yearTable) {
            boolean istable = clrctrlDaoImpl.checkTable(String.valueOf(s));
            if (istable) {
              log.debug(
                  Service,
                  tranId,
                  className,
                  "POL_DB_11 CL ClrctrlImpl.selectByPolicynoAndCertno policyno:"
                      + policyno
                      + "|certno:"
                      + certno
                      + "|yearTable:"
                      + s
                      + "|limit:"
                      + limit
                      + "|paydateCond:"
                      + paydateCond
                      + "|");
              List<ClrctrlM> clrctrlM =
                  clrctrlDaoImpl.selectByPolicyNoAndCertNo(
                      policyno, certno, (String.valueOf(s)), limit, paydateCond);
              for (ClrctrlM item : clrctrlM) {
                PaymentM payment = new PaymentM();
                String payPeriod = item.getPayperiod();
                payPeriod =
                    null != payPeriod
                        ? (payPeriod.length() == 6
                            ? payPeriod.substring(0, 4) + "/" + payPeriod.substring(4, 6)
                            : payPeriod.length() == 4
                                ? payPeriod.substring(0, 2) + "/" + payPeriod.substring(2, 4)
                                : payPeriod)
                        : null;
                Integer extrapremium = item.getExtraprem();
                Integer discountpremium = getDiscountLifeFit(item.getRpno());
                Integer totalpremium = item.getPremium() - discountpremium;
                Integer premium = item.getPremium() - (extrapremium == null ? 0 : extrapremium);
                String branch = TLOrganizeInfo.getBranchNameAll(item.getSubmitbranch());

                String p = premium == null ? "0.00" : M.endot(premium.toString(), 2);
                String e = extrapremium == null ? "0.00" : M.endot(extrapremium.toString(), 2);
                String d =
                    discountpremium == null ? "0.00" : M.endot(discountpremium.toString(), 2);
                String t = totalpremium == null ? "0.00" : M.endot(totalpremium.toString(), 2);

                payment.setPay_period(payPeriod);
                payment.setEffective_date(cert.getEffectivedate());
                payment.setPay_date(item.getPaydate());
                payment.setReceipt_no(item.getRpno());
                payment.setPremium(M.edits(p));
                payment.setExtrapremium(M.edits(e));
                payment.setDiscountpremium(M.edits(d));
                payment.setTotalpremium(M.edits(t));
                payment.setPayment_channel(branch);
                payment.setDuedate(item.getDuedate());
                payment.setReceipt_sys_date(item.getSysdate());
                listPayment.add(payment);
                limit--;
                if (limit == 0 && isLimit) {
                  break;
                }
              }
              if (limit == 0 && isLimit) {
                break;
              }
            }
          }
        }
      }
//      if (!("unitlink").equalsIgnoreCase(type) && !("universallife").equalsIgnoreCase(type)) {
        sortPayPeriod(listPayment);
//      }

      if (listPayment.size() == 0) {
        paymentHistoryResponse.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        paymentHistoryResponse.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        paymentHistoryResponse.setSuccess(true);
        return paymentHistoryResponse;
      }
      result.setList_payment_history(listPayment);
      result.setType(req.getType());

      paymentHistoryResponse.setResponseCode(wServiceMsg.getMessageByCode("200")[0]);
      paymentHistoryResponse.setResponseDescription(wServiceMsg.getMessageByCode("200")[1]);
      paymentHistoryResponse.setSuccess(true);
      paymentHistoryResponse.setResult(result);
    } catch (Exception e) {
      paymentHistoryResponse.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      paymentHistoryResponse.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      paymentHistoryResponse.setSuccess(false);
      paymentHistoryResponse.setResult(null);
      e.printStackTrace();
      throw new Exception(
          "GetPaymentHistory|policyno:" + policyno + " certno:" + certno + " type:" + type, e);
    }
    return paymentHistoryResponse;
  }

  private String getEffectiveDate(String effectivedate, PrepareDateM prepareDateM, Calendar cal)
      throws ParseException {
    if (!(!prepareDateM.getEffectiveDateMaster().equals(effectivedate)
        && Integer.parseInt(prepareDateM.getEffectiveDateMaster()) != 0)) {
      Date effectivedateTmp = new SimpleDateFormat("yyyyMMdd").parse(effectivedate);
      cal.setTime(effectivedateTmp);
      cal.add(Calendar.YEAR, -1);
      effectivedateTmp = cal.getTime();
      effectivedate = sdfCond.format(effectivedateTmp);
    }
    return effectivedate;
  }

  private void setPaymentM(
      PaymentM payment,
      String payPeriod,
      String effectiveDate,
      String payDate,
      String rpno,
      Integer premium,
      Integer extraPremium,
      Integer discountPremium,
      Integer totalPremium,
      String branch,
      String dueDate,
      String sysDate,
      String payMode,
      String payCondtion,
      String submitNo) {
    payment.setPay_period(payPeriod);
    payment.setEffective_date(effectiveDate);
    payment.setPay_date(payDate);
    payment.setReceipt_no(rpno);
    payment.setPremium(M.edits(premium != null ? premium + ".00" : "0.00"));
    payment.setExtrapremium(M.edits(extraPremium != null ? extraPremium + ".00" : "0.00"));
    payment.setDiscountpremium(M.edits(discountPremium != null ? discountPremium + ".00" : "0.00"));
    payment.setTotalpremium(M.edits(totalPremium != null ? totalPremium + ".00" : "0.00"));
    payment.setPayment_channel(branch);
    payment.setDuedate(dueDate);
    payment.setReceipt_sys_date(sysDate);
    payment.setPay_mode(PolicyUtil.descriptionByMode(payMode));
    payment.setPay_condition(payCondtion);
    payment.setSubmitNo(submitNo);
  }

  private PrepareDateM prepareDate(String payDate, String effectiveDate) throws ParseException {
    PrepareDateM prepareDateM = new PrepareDateM();
    try {
      prepareDateM.setPayDateMaster(payDate);
      prepareDateM.setYear(Integer.parseInt(payDate.substring(0, 4)));
      prepareDateM.setMonth(Integer.parseInt(payDate.substring(4, 6)));
      prepareDateM.setDay(Integer.parseInt(payDate.substring(6, 8)));
      prepareDateM.setEffectiveDateMaster(effectiveDate);
      if (prepareDateM.getYear() == 0) {
        prepareDateM.setYear(currentyear);
      }
      if (prepareDateM.getMonth() == 0) {
        prepareDateM.setMonth(currentMonth);
      }
      if (prepareDateM.getDay() == 0) {
        prepareDateM.setDay(currentDay);
      }
      prepareDateM.setPayDateM(
          new SimpleDateFormat("yyyy/MM/dd")
              .parse(
                  prepareDateM.getYear()
                      + "/"
                      + prepareDateM.getMonth()
                      + "/"
                      + prepareDateM.getDay()));
      Calendar cal = Calendar.getInstance();
      cal.setTime(prepareDateM.getPayDateM());
      cal.add(Calendar.YEAR, -2);
      prepareDateM.setPayDateAgo(cal.getTime());
      prepareDateM.setPayDateCond(sdfCond.format(prepareDateM.getPayDateAgo()));
      prepareDateM.setYearTable(
          new String[] {
            "",
            String.valueOf(prepareDateM.getYear()),
            String.valueOf(prepareDateM.getYear() - 1),
            String.valueOf(prepareDateM.getYear() - 2)
          });
    } catch (Exception e) {
      log.error(
          effectiveDate, effectiveDate, payDate, "Error Exception prepareDate : " + e.getMessage());
      throw e;
    }
    return prepareDateM;
  }

  private String getPayPeriod(String payPeriod) {
    return null != payPeriod
        ? (payPeriod.length() == 6
            ? payPeriod.substring(0, 4) + "/" + payPeriod.substring(4, 6)
            : payPeriod.length() == 4
                ? payPeriod.substring(0, 2) + "/" + payPeriod.substring(2, 4)
                : payPeriod)
        : null;
  }

//  private void sortPayPeriod(List<PaymentM> list) {
//    list.sort((a0, a1) -> a1.getPay_period().compareTo(a0.getPay_period()));
//  }
  private void sortPayPeriod(List<PaymentM> list) {
    Collections.sort(
            list,
            new Comparator<PaymentM>() {
              @Override
              public int compare(PaymentM a0, PaymentM a1) {
//                return a1.getPay_date().compareTo(a0.getPay_date());
                if(a1.getPay_date().compareTo(a0.getPay_date()) == 0){
                  return a1.getPay_period().compareTo(a0.getPay_period());
                }else{
                  return a1.getPay_date().compareTo(a0.getPay_date());
                }
              }
            });
  }

  private Integer getDiscountLifeFit(String rpno) throws Exception {
    //    String discount = "0";
    //    List<DiscM> discMs = discDaoImpl.selectByRpno(rpno);
    //    if (discMs.size() == 1) {
    //      discount = discMs.get(0).getDiscountamount();
    //    }
    return Integer.parseInt(discDaoImpl.getDiscountAmount(rpno));
  }
  public Integer getDiscountLifefit(String rpno) {
    String discount = "0";
    List<DiscM> discMs = discDaoImpl.selectByRpno(rpno);
    if (discMs != null && discMs.size() == 1) {
      discount = discMs.get(0).getDiscountamount();
    }
    return new Integer(discount != null ? discount : "0");
  }
}
