package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.dao.*;
import com.thailife.api.policyInfoService.dao.impl.*;
import com.thailife.api.policyInfoService.service.NextPayPeriodService;
import com.thailife.api.policyInfoService.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NextPayPeriodServiceImpl implements NextPayPeriodService {
  private final Log log = new Log();
  private final String className = "NextPayPeriodServiceImpl";
  @Autowired CallWS callWS;
  @Autowired UniversallifeDaoImpl universalLifeDao;
  @Autowired UnitLinkDaoImpl unitLinkDao;
  @Autowired IndmastDaoImpl indMastDao;
  @Autowired WhlmastDaoImpl whlMastDao;
  @Autowired OrdmastDaoImpl ordMastDao;

  public String getPaydescUL(String policyNo) throws Exception {
    log.printDebug(className, "getPaydescUL: START");
    String paydescUL;

    try {
      UniversallifeDao p = universalLifeDao.findByPolicyno(policyNo);
      String[] status =
          PolicyUtil.getStatusForShowCustomer(
              p.getPolicystatus1(), p.getPolicystatusdate1(),
              p.getPolicystatus2(), p.getPolicystatusdate2(),
              p.getOldpolicystatus1(), p.getOldpolicystatusdate1(),
              p.getOldpolicystatus2(), p.getOldpolicystatusdate2());

      if (status != null) paydescUL = status[0];
      else paydescUL = p.getPolicystatus1();

    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, "getPaydescUL: " + e.getMessage());
      throw new Exception("getPaydescUL|policyNo:" + policyNo, e);
    }
    log.printDebug(className, "getPaydescUL: END");
    return paydescUL;
  }

  public String getPaydescULIP(String policyNo) throws Exception {
    log.printDebug(className, "getPaydescULIP: START");
    String paydescULIP;
    try {
      UnitLinkDao p = unitLinkDao.findByPolicyno(policyNo);
      String[] status =
          PolicyUtil.getStatusForShowCustomer(
              p.getPolicystatus1(), p.getPolicystatusdate1(),
              p.getPolicystatus2(), p.getPolicystatusdate2(),
              p.getOldpolicystatus1(), p.getOldpolicystatusdate1(),
              p.getOldpolicystatus2(), p.getOldpolicystatusdate2());

      if (status != null) paydescULIP = status[0];
      else paydescULIP = p.getPolicystatus1();

    } catch (Exception e) {
      e.printStackTrace();
      log.printError(className, "getPaydescULIP: " + e.getMessage());
      throw new Exception("getPaydescULIP|policyNo:" + policyNo, e);
    }
    log.printDebug(className, "getPaydescULIP: END");
    return paydescULIP;
  }

  @Override
  public String getPaydescOL(String policyNo) throws Exception {
    log.printDebug(className, "getPaydescOL: START");
    String paydescOL;
    try {
      OrdmastDao p = ordMastDao.findByPolicyno(policyNo);
      String[] status =
          PolicyUtil.getStatusForShowCustomer(
              p.getPolicystatus1(), p.getPolicystatusdate1(),
              p.getPolicystatus2(), p.getPolicystatusdate2(),
              p.getOldpolicystatus1(), p.getOldpolicystatusdate1(),
              p.getOldpolicystatus2(), p.getOldpolicystatusdate2());

      if (status != null) paydescOL = status[0];
      else paydescOL = p.getPolicystatus1();

    } catch (Exception e) {
      e.printStackTrace();
      log.printDebug(className, "getPaydescOL: " + e.getMessage());
      throw new Exception("getPaydescOL|policyNo:" + policyNo, e);
    }
    log.printDebug(className, "getPaydescOL: END");
    return paydescOL;
  }

  @Override
  public String getPaydescIND(String policyNo) throws Exception {
    log.printDebug(className, "getPaydescIND: START");
    String paydescIND;
    try {
      IndmastDao p = indMastDao.findByPolicyno(policyNo);
      String[] status =
          PolicyUtil.getStatusForShowCustomer(
              p.getPolicystatus1(), p.getPolicystatusdate1(),
              p.getPolicystatus2(), p.getPolicystatusdate2(),
              p.getOldpolicystatus1(), p.getOldpolicystatusdate1(),
              p.getOldpolicystatus2(), p.getOldpolicystatusdate2());

      if (status != null) paydescIND = status[0];
      else paydescIND = p.getPolicystatus1();

    } catch (Exception e) {
      e.printStackTrace();
      log.printDebug(className, "getPaydescIND: " + e.getMessage());
      throw new Exception("getPaydescIND|policyNo:" + policyNo, e);
    }
    log.printDebug(className, "getPaydescIND: END");
    return paydescIND;
  }

  @Override
  public String getPaydescWHL(String policyNo) throws Exception {
    log.printDebug(className, "getPaydescWHL: START");
    String paydescWHL;
    try {
      WhlmastDao p = whlMastDao.findByPolicyno(policyNo);
      String[] status =
          PolicyUtil.getStatusForShowCustomer(
              p.getPolicystatus1(), p.getPolicystatusdate1(),
              p.getPolicystatus2(), p.getPolicystatusdate2(),
              p.getOldpolicystatus1(), p.getOldpolicystatusdate1(),
              p.getOldpolicystatus2(), p.getOldpolicystatusdate2());

      if (status != null) paydescWHL = status[0];
      else paydescWHL = p.getPolicystatus1();

    } catch (Exception e) {
      e.printStackTrace();
      log.printDebug(className, "getPaydescWHL: " + e.getMessage());
      throw new Exception("getPaydescWHL|policyNo:" + policyNo, e);
    }
    log.printDebug(className, "getPaydescWHL: END");
    return paydescWHL;
  }
}
