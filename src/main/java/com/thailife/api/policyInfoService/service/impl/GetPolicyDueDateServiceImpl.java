package com.thailife.api.policyInfoService.service.impl;

import java.math.BigDecimal;
import java.util.*;

import com.thailife.log.ExtLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.IndmastDao;
import com.thailife.api.policyInfoService.dao.OrdmastDao;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.dao.UniversallifeDao;
import com.thailife.api.policyInfoService.dao.WhlmastDao;
import com.thailife.api.policyInfoService.dao.impl.CertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.IndmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.OrdmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UnitLinkDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UniversallifeDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.WhlmastDaoImpl;
import com.thailife.api.policyInfoService.model.Policy;
import com.thailife.api.policyInfoService.model.request.GetPolicyDueDateRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetPolicyDueDateResponse;
import com.thailife.api.policyInfoService.model.response.PolicyResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.GetPolicyDueDateService;
import com.thailife.api.policyInfoService.util.ApiConfigM;
import com.thailife.api.policyInfoService.util.CallService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import lombok.extern.log4j.Log4j;
import manit.M;
import manit.rte.Result;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;
@Service
public class GetPolicyDueDateServiceImpl implements GetPolicyDueDateService {
    private final ExtLogger log = ExtLogger.create(GetPolicyDueDateServiceImpl.class);
    private final String className = "GetPolicyDueDateServiceImpl";
  private final String Service = "GetPolicyDueDate";
  private String tranId;
  
  @Autowired CallService callService;
  @Autowired OrdmastDaoImpl ordmastDaoImpl;
  @Autowired IndmastDaoImpl indmastDaoImpl;
  @Autowired WhlmastDaoImpl whlmastDaoImpl;
  @Autowired UniversallifeDaoImpl universallifeDaoImpl;
  @Autowired UnitLinkDaoImpl unitLinkDaoImpl;
  @Autowired CertDaoImpl certDaoImpl;
  @Override
  public ServiceResults<GetPolicyDueDateResponse> getResponse(
      RequestMessages<GetPolicyDueDateRequest> req) {
    WebServiceMsg wMessage = new WebServiceMsg();
    ServiceResults<GetPolicyDueDateResponse> response = new ServiceResults<GetPolicyDueDateResponse>();
    tranId = req.getHeaderData().getMessageId();
    try {
      String transactionId = req.getHeaderData().getMessageId();
      String customerId = req.getRequestRecord().getCustomer_id();
     
      List<PolicyResponse> listPol16 = new ArrayList<PolicyResponse>();

      GetPolicyDueDateResponse getPolicyDueDateResp = new GetPolicyDueDateResponse();
      getPolicyDueDateResp.setList_of_policy(listPol16);
      
      ApiConfigM apiConfigM = callService.readPoperties();        
      // call api pol_db_01
      JSONObject jsonRespPol01 =  getPolDB01(customerId,transactionId, req.getHeaderData().getSentDateTime() ,apiConfigM);
      //System.out.println("GetPolicyDueDate jsonRespPol01 : " + jsonRespPol01 + "|");
      JSONObject respStatusPol01 = jsonRespPol01.getJSONObject("responseStatus");
      if (!respStatusPol01.getString("errorCode").equals("200")) {
        response.setResult(getPolicyDueDateResp);
        response.setResponseCode(wMessage.getMessageByCode("204")[0]);
        response.setResponseDescription(wMessage.getMessageByCode("204")[1]);
        response.setSuccess(true);
        return response;
        
      } 
      JSONObject respRecordPol01 = jsonRespPol01.getJSONObject("responseRecord");
      JSONArray dataListPol01 = respRecordPol01.getJSONArray("list_of_policy");
      //System.out.println("GetPolicyDueDate respRecordPol01 : " + respRecordPol01 + "|");
      ///////////////////////////////////////////////////////////////////////////////////
      
      // Filter โดยการกรอง nextduedate จาก POL_DB_01 นำ Field nextduedate
      // คำนวณจากวันปัจจุบัน 30 วัน           

      List<Policy> listPol01 = new ArrayList<Policy>();
      ///////////////////////////////////////////////////////////////////////////////////
      for (int i = 0; i < dataListPol01.length(); i++) { 
          //System.out.println("dataListPol01 : " + i);
          JSONObject objPol01 = dataListPol01.getJSONObject(i);
          JSONObject policyjson = getPolicyData(objPol01.getString("policyno"), objPol01.getString("certno"));
          listPol01 = setListPol01(policyjson , objPol01 , listPol01);                 
      }
      //System.out.println("listPol01 :  " + listPol01); 
      if (listPol01 != null && !listPol01.isEmpty()) {
          //System.out.println("listPol01 :  " + listPol01.size());
          
          for (int j = 0; j < listPol01.size(); j++) {
              PolicyResponse pol16Data = new PolicyResponse(); 
              JSONObject jsonRespPol03 =  getPolDB03(listPol01.get(j).getPolicyno(), listPol01.get(j).getCertno(),
                      listPol01.get(j).getType(), req.getHeaderData().getMessageId(), req.getHeaderData().getSentDateTime(), apiConfigM);
              JSONObject respStatusPol03 = jsonRespPol03.getJSONObject("responseStatus");
              if (!respStatusPol03.getString("errorCode").equals("200")) {
                response.setResult(getPolicyDueDateResp);
                response.setResponseCode(wMessage.getMessageByCode("204")[0]);
                response.setResponseDescription(wMessage.getMessageByCode("204")[1]);
                response.setSuccess(true);
                return response;
              } 
              JSONObject respRecordPol03 = jsonRespPol03.getJSONObject("responseRecord");                     
              JSONArray dataListPol03 = respRecordPol03.getJSONArray("nextPayPeriodList");
              JSONObject objPol03 = dataListPol03.getJSONObject(0);
              int totalpremium = objPol03.getInt("totalpremium");
              String nextduedate = objPol03.getString("nextduedate");
              ///System.out.println("totalpremium : " + totalpremium +"|"+objPol03.getInt("totalpremium")+"|");
              if(totalpremium > 0 && nextduedateBetween(31, nextduedate , 30)) 
              {
                  pol16Data.setPolicyno(listPol01.get(j).getPolicyno());
                  pol16Data.setCertno(listPol01.get(j).getCertno());
                  pol16Data.setPlancode(listPol01.get(j).getPlancode());
                  pol16Data.setPlanname(listPol01.get(j).getPlanname());
                  pol16Data.setType(listPol01.get(j).getType());
                  pol16Data.setNextduedate(nextduedate);
                  pol16Data.setTotalpremium(new BigDecimal(totalpremium));
                  listPol16.add(pol16Data);
              }
          }
      }

      if (listPol16.isEmpty()) {
        response.setResult(getPolicyDueDateResp);
        response.setResponseCode(wMessage.getMessageByCode("204")[0]);
        response.setResponseDescription(wMessage.getMessageByCode("204")[1]);
        response.setSuccess(true);
        return response;

      } else {
          Collections.sort(listPol16, new Comparator<PolicyResponse>(){
              public int compare(PolicyResponse o1, PolicyResponse o2){
                  return Integer.parseInt(o1.getNextduedate()) - Integer.parseInt(o2.getNextduedate());
              }
          });
        response.setResult(getPolicyDueDateResp);
        response.setSuccess(true);
        return response;
      }
    }
    catch (Exception e) {
        e.printStackTrace();
      response.setResponseCode(wMessage.getMessageByCode("500")[0]);
      response.setResponseDescription(wMessage.getMessageByCode("500")[1]);
      response.setSuccess(false);
      log.error(Service, tranId, className, "Error Exception getResponse : " + e.getMessage());
    }
    return response;
  }
  
  public JSONObject getPolDB01(String customerID ,String messageID , String sentDateTime , ApiConfigM apiConfigM ) throws Exception {
    String pol01 = "/PolicyDataService/SearchPolicyByCustomerId";
    String reqPol01 = getRequestPol01(customerID, messageID ,sentDateTime );
    String urlPol01 = apiConfigM.getENDPOINT() + pol01;
//  String urlPol01 = "http://10.102.63.35:8080/PolicyDataService/SearchPolicyByCustomerId";.
    log.info(Service, tranId, className, "call ws " + urlPol01 + " Req : " + reqPol01);
    String respPol01 = callService.callWS(urlPol01, reqPol01);
    return new JSONObject(respPol01);
  }
  private String getRequestPol01(String customer_id, String messageID , String sentDateTime) {
    String request = "{" + "\"headerData\": {" + "  \"messageId\": \"" + messageID + "\","
            + " \"sentDateTime\": \"" + sentDateTime + "\"" + "},"
            + " \"requestRecord\": { " + " \"customer_id\": \"" + customer_id + "\"" + "}" + "} ";
    return request;
  }
  
  private JSONObject getPolicyData(String policyno, String certno) throws Exception {

    OrdmastDao ordmast = ordmastDaoImpl.findByPolicyno(policyno);
    if (ordmast != null && ordmast.getPolicyno() != null) {
        JSONObject json = new JSONObject(ordmast);
        json.put("policytype", "OL");
        json.put("basetype", "OL");
        return json;
    }
    IndmastDao indmast = indmastDaoImpl.findByPolicyno(policyno);
    if (indmast != null && indmast.getPolicyno() != null) {
        JSONObject json = new JSONObject(indmast);
        json.put("policytype", "OL");
        json.put("basetype", "IND");
        return json;
    }
    WhlmastDao whlmast = whlmastDaoImpl.findByPolicyno(policyno);
    if (whlmast != null && whlmast.getPolicyno() != null) {
        JSONObject json = new JSONObject(whlmast);
        json.put("policytype", "OL");
        json.put("basetype", "WHL");
        return json;
    }
    UniversallifeDao universallife = universallifeDaoImpl.findByPolicyno(policyno);
    if (universallife != null && universallife.getPolicyno() != null) {
        JSONObject json = new JSONObject(universallife);
        json.put("policytype", "OL");
        json.put("basetype", "UL");
        return json;
    }
    UnitLinkDao unitlink = unitLinkDaoImpl.findByPolicyno(policyno);
    if (unitlink != null && unitlink.getPolicyno() != null) {
        JSONObject json = new JSONObject(unitlink);
        json.put("policytype", "ULIP");
        json.put("basetype", "ULIP");
        return json;
    }
    if (PolicyUtil.validateString(policyno) && PolicyUtil.validateString(certno)) {
        CertDao cert = certDaoImpl.findByPolicynoAndCertno(policyno, certno);
        if (cert.getPolicyno() != null) {
            JSONObject json = new JSONObject(cert);
            json.put("policytype", "CL");
            json.put("basetype", "CL");
            return json;
        }
    }
    return null;
  }
  
  private List<Policy> setListPol01(JSONObject policyjson, JSONObject objPol01 ,List<Policy> listPol01 ) throws Exception {
    
    if(policyjson != null )
    {   
        boolean flag = true;
        String status = "";
        String basetype = policyjson.has("basetype") ? policyjson.getString("basetype") : "";
        String payperiod = policyjson.has("payperiod") ? policyjson.getString("payperiod").trim() : "";
        String mode = policyjson.has("mode") ? policyjson.getString("mode") : "";
        String policyNo = objPol01.getString("policyno");
        if (basetype.equals("CL")) {
            status = policyjson.getString("statcer"); 
        } else {
            status = policyjson.getString("policystatus1");
        } 
        //System.out.println("policyjson:" + policyjson+"|");
        //System.out.println("basetype:" + basetype + "|payperiod:" + payperiod + "|mode:" + mode + "|policyNo:" + policyNo + "|status:" +status+"|");
        if (!basetype.equals("IND") && !basetype.equals("CL") && (isIFB(status))) 
        {
            if (!(basetype.equals("UL") && mode.equals("9")) && !(basetype.equals("ULIP") && mode.equals("9"))) {

                if (basetype.equals("OL") ) {
                    flag = masterPremium(payperiod, mode, policyNo);
                    System.out.println("flag : " + flag);
                    if (flag) {                                                             
                        listPol01.add(setPolicy(objPol01.getString("policyno") ,objPol01.getString("certno") , objPol01.getString("plancode") , 
                                objPol01.getString("planname") ,objPol01.getString("type") , objPol01.getString("nextduedate"),status ));
                    }
                } else {
                        
                    listPol01.add(setPolicy(objPol01.getString("policyno") ,objPol01.getString("certno") , objPol01.getString("plancode") , 
                            objPol01.getString("planname") ,objPol01.getString("type") , objPol01.getString("nextduedate"),status ));
                }
            }
        }
    }
    return listPol01;
  }
  
  public boolean masterPremium(String payperiod, String mode, String policyNo) throws Exception {

    String payPeriod = nextPayPeriod(payperiod, mode);
    String[] s = new String[] { policyNo, payPeriod };
    PublicRte.setRemote(true);
    Result result = PublicRte.getClientResult("blmaster", "rte.bl.master.RteMasterPremium", s);
    if (result.status() == 0) {
        if (result.value() instanceof Vector) {
            Vector<Object> v = (Vector) result.value();
            if (v.size() > 9) {
                if (M.cmps((String) v.get(9), "0") > 0)
                    return true;
                else
                    return false;
            } else
                throw new Exception("");

        } else if (result.value() instanceof String) {
            throw new Exception(result.value().toString());
        }
    } else
        throw new Exception(result.value().toString());
    return false;
  }
  
  public static String nextPayPeriod(String payPeriod, String mode) {

    String year = payPeriod.substring(0, 2);
    String period = payPeriod.substring(2);
    switch (mode.charAt(0)) {
    case '1':
        year = M.inc(year);
        break;
    case '0':
        period = M.inc(period);
        if (M.cmps(period, "12") > 0) {
            period = "01";
            year = M.inc(year);
        }
        break;
    case '2':
        period = M.inc(period);
        if (M.cmps(period, "02") > 0) {
            period = "01";
            year = M.inc(year);
        }
        break;
    case '4':
        period = M.inc(period);
        if (M.cmps(period, "04") > 0) {
            period = "01";
            year = M.inc(year);
        }
        break;
    }
    return String.valueOf(year) + period;
  }
  
  public JSONObject getPolDB03(String policyno, String certno, String type ,String messageID , String sentDateTime , ApiConfigM apiConfigM ) throws Exception {

    String pol03 = "/PolicyDataService/NextPayPeriod";
    String urlPol03 = apiConfigM.getENDPOINT() + pol03;
//  String urlPol03 = "http://10.102.63.35:8080/PolicyDataService/NextPayPeriod";
    String reqPol03 = getRequestPol03(policyno, certno, type, messageID, sentDateTime);
    //System.out.println("reqPol03 : " +reqPol03);
    String respPol03 = callService.callWS(urlPol03, reqPol03);
    //System.out.println("respPol03 : " +respPol03);
    return new JSONObject(respPol03);
  }
  private String getRequestPol03(String policyno, String certno, String type,
      String messageID , String sentDateTime) {
  String request = "{" + "\"headerData\": {" + "  \"messageId\": \"" + messageID + "\","
          + " \"sentDateTime\": \"" + sentDateTime + "\"" + "},"
          + " \"requestRecord\": { " + "      \"nextPayPeriod\": [ " + "{ " + " \"policyno\": \"" + policyno
          + "\"," + " \"certno\": \"" + certno + "\"," + " \"type\": \"" + type + "\"" + "} ] }" + "} ";
  return request;
  }

  private boolean nextduedateBetween(int before, String nextduedate, int after) {
    String sysDate = DateInfo.sysDate();
    String bDate = M.nextdate(sysDate, before*-1);
    String aDate = M.nextdate(sysDate, after);
    //System.out.println("nextduedate : " + nextduedate );
    ///System.out.println("bDate : " + bDate );
    //System.out.println("aDate : " + aDate );
    if(M.cmps(nextduedate, bDate) >= 0 && M.cmps(nextduedate, aDate) <= 0)
        return true;
    return false;
  }
  private boolean isIFB(String status) {
    if(status != null && status.matches("^[IFBifb]$") )
        return true;
    return false;
  }
  private Policy setPolicy(String policyno , String certno , String plancode , String  planname , String type , String nextduedate , String status) 
  {
      Policy policy =  new Policy();
      policy.setCertno(certno);
      policy.setPolicyno(policyno);
      policy.setPlancode(plancode);
      policy.setPlanname(planname);
      policy.setType(type);
      policy.setNextduedate(nextduedate);
      policy.setPolicystatus1(status);
      return policy;
  }
}
