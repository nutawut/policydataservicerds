package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.PaymentHistoryRequest;
import com.thailife.api.policyInfoService.model.response.PaymentHistoryResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface PaymentHistoryService {

	public ServiceResults<PaymentHistoryResponse> getPaymentHistory(PaymentHistoryRequest req, String tran_Id) throws Exception;

}
