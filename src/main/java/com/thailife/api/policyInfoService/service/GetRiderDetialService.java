package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.util.PolicyException;

public interface GetRiderDetialService {
  ServiceResults<RiderDetialResponse> getResponse(PolicyDetailRequest req, String tranId) throws PolicyException;
  ResponseMessages<RiderDetialResponse> service(RequestMessages<RiderDetailRequest> request) throws PolicyException;
}
