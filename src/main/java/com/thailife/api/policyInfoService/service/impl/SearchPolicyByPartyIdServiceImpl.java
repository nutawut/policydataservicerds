package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.CertMappingDao;
import com.thailife.api.policyInfoService.dao.PolicyParticipantDao;
import com.thailife.api.policyInfoService.dao.impl.CertMappingDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.DetcertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.customer.PolicyParticipantDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.policy.PolicyDaoImpl;
import com.thailife.api.policyInfoService.model.PolicyOnlyM;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByPartyIdResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SearchPolicyByPartyIdService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.ArrayList;

@Service
public class SearchPolicyByPartyIdServiceImpl implements SearchPolicyByPartyIdService {
  private final ExtLogger log = ExtLogger.create(SearchPolicyByPartyIdServiceImpl.class);
  private final String className = "SearchPolicyOnlyByPartyIdController";
  private final String Service = "SearchPolicyListOnlyByCustomerIdImp";
  private String tranId;
  
  @Autowired PolicyParticipantDaoImpl policyParticipantDaoImpl;

  @Override
  public ServiceResults<SearchPolicyByPartyIdResponse> getResponse(
      SearchPolicyByCustomerIdRequest request, String tran_Id) {
    ServiceResults<SearchPolicyByPartyIdResponse> response = new ServiceResults<>();
    SearchPolicyByPartyIdResponse result = new SearchPolicyByPartyIdResponse();
    tranId = tran_Id;
    try {
      result.setList_of_policy(getListOfPolicyByCustomer(Long.parseLong(request.getCustomer_id())));
      response.setSuccess(true);
      if (result.getList_of_policy() == null || result.getList_of_policy().isEmpty()) {
        response.setResponseCode("204");
        response.setResult(result);
        response.setResponseDescription("ไม่พบข้อมูลที่ต้องการในระบบ");
      } else {
        response.setResponseCode("200");
        response.setResult(result);
        response.setResponseDescription("ดำเนินการเรียบร้อย");
      }
      return response;
    } catch (Exception ex) {
      log.error(Service, tranId, className, "Error Exception :" +  ex.getMessage());
      response.setSuccess(false);
      response.setResponseCode("500");
      response.setResponseDescription("ระบบขัดข้องกรุณาลองใหม่อีกครั้ง");
    }
    return response;
  }

  private ArrayList<PolicyOnlyM> getListOfPolicyByCustomer(long customer_id) throws Exception {
    ArrayList<PolicyOnlyM> list_of_policy = null;

      ArrayList<PolicyParticipantDao> list =
          policyParticipantDaoImpl.findByPartyidOrderByLastUpdateDesc(customer_id);
    try(Connection con = DBConnection.con(PolicyUtil.getDBPolicy(), "")){
      if (list != null) {
        list_of_policy = new ArrayList<>();
        for (PolicyParticipantDao policyParticipantM : list) {
          String policyNo = policyParticipantM.getPolicy_no();
          String certNo = policyParticipantM.getCert_no();
          String oldPolicyNo = getOldPolicyNo(policyNo , certNo, con);
          String oldCertNo = getOldCertNo(policyNo , certNo, con);
          list_of_policy.add(
              new PolicyOnlyM(policyNo, certNo, oldCertNo, oldPolicyNo));
        }
      }
      return list_of_policy;

    } catch (Exception ex) {
      log.error(Service, tranId, className, "Error getListOfPolicyByCustomer :"+ ex.getMessage());
      throw ex;
    }
  }
  private String getOldPolicyNo(String policyno , String certno, Connection con) {
    System.out.println("getOldPolicyNo Start : " + policyno +"|"+certno+"|");
    String plancode = PolicyDaoImpl.getPlanCodeByPolicyno(policyno,  certno, con);
    plancode = ( plancode == null || plancode.trim().length()== 0 ? policyno : plancode );
    DetcertDaoImpl detcertDaoImpl = new DetcertDaoImpl();
    CertMappingDaoImpl certMappingDaoImpl = new CertMappingDaoImpl();
    if(certno != null && certno.trim().length() > 0)
    {
      String code = detcertDaoImpl.getCodeByPolicynoAndCertno(policyno, certno, con);
      String rpolicyno = certMappingDaoImpl.getRpolicynoByPolicynoAndCertno(policyno, certno, con);
      if(code == null)
        code ="";
      if(rpolicyno == null)
        rpolicyno = plancode ;

      System.out.println("rpolicyno : " + rpolicyno );
      String oldPolicyNo = utility.underwrite.bankassurecomps.PolicyCL.codeToBA(rpolicyno  , code);
      if(oldPolicyNo==null)
        oldPolicyNo = plancode;
      return null == oldPolicyNo ? plancode : oldPolicyNo;
    }
    return "";
  }

  private String getOldCertNo(String policyno , String certno, Connection con) throws Exception {
    CertMappingDaoImpl certMappingDaoImpl = new CertMappingDaoImpl();
    CertMappingDao certMapping = certMappingDaoImpl.findByPolicyNoAndCertNo(policyno, certno, con);
    if (null != certMapping) {
      return certMapping.getRcertno();
    }
    return "";
  }
}
