package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.controller.pol12.GetCoverageOverviewController;
import com.thailife.api.policyInfoService.dao.StatusueDao;
import com.thailife.api.policyInfoService.dao.impl.CoverageMainDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.StatusueDaoImpl;
import com.thailife.api.policyInfoService.model.*;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.request.GetCoverageOverviewRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetCoverageOverviewResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.GetCoverageOverviewService;
import com.thailife.api.policyInfoService.util.*;
import com.thailife.log.ExtLogger;
import lombok.extern.log4j.Log4j;
import manit.rte.Result;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utility.rteutility.PublicRte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;


@Service
public class GetCoverageOverviewServiceImpl implements GetCoverageOverviewService {
  private final ExtLogger log = ExtLogger.create(GetCoverageOverviewServiceImpl.class);
  private final String className = "GetCoverageOverviewServiceImpl";
  private final String Service = "GetPaymentHistoryPol11";
  private String tranId;
  
  @Autowired CallWS callWS;
  @Autowired WebServiceMsg wMessage;

  @Override
  public ServiceResults<ListOfProtection> getResponse(
      RequestMessages<GetCoverageOverviewRequest> req) {

    GetCoverageOverviewResponse getCoverageOverview = new GetCoverageOverviewResponse();
    ListOfProtection listOfProtection = new ListOfProtection();
    ServiceResults<ListOfProtection> response = new ServiceResults<>();
    tranId = req.getHeaderData().getMessageId();
    try {
      String contextPol01 = "/PolicyDataService/SearchPolicyByCustomerId";
      String request = null;
      String customer_id = req.getRequestRecord().getCustomerId();
      request = getRequest(customer_id, req);
      // ทำการยิง webservice
      byte[] postData = request.getBytes(StandardCharsets.UTF_8);
      int postDataLength = postData.length;
      log.info(Service, tranId, className, "call ws POL_DB_01 Req : " + request);
      String response1 = callWS(setConnectionCallWS(contextPol01, postDataLength), request);
      log.info(Service, tranId, className, "call ws POL_DB_01 Resp : " + response1);
      JSONObject json = new JSONObject(response1);
      JSONObject responseStatus = json.getJSONObject("responseStatus");
      if (!responseStatus.getString("errorCode").equals("200")) {
        response.setResponseCode("204");
        response.setSuccess(true);
        response.setResponseDescription("ไม่พบข้อมูลที่ต้องการในระบบ");
        log.info(Service, tranId, className, "POL_DB_01 Not found");
        return response;
      }
      JSONObject responseRecord = json.getJSONObject("responseRecord");

      JSONArray datalistPolicy = responseRecord.getJSONArray("list_of_policy");
      String typeGlobal = null;
      String policyNo = "";
      String certNo = "";
      String policystatus1 = "";
      Number lifesum = 0;
      Number sumR06 = 0.0;
      boolean isR06 = false;
      CoverageDetailM coverageDetail = new CoverageDetailM();

      HashMap<String, CoverageDetailM> mapCoverageDetail = new HashMap<>();
      HashMap<String, GetCoverageOverviewResponse> sumByGroup = new HashMap<>();
      HashMap<String, MapGroupRiderM> mapGroupRider = new HashMap<>();
      HashMap<String, String> mapEffectiveDate = new HashMap<>();
      // initial value
      listOfProtection.setList_of_protection(new ArrayList<GetCoverageOverviewResponse>());
      response.setResult(listOfProtection);
      response.setResponseCode("200");
      response.setResponseDescription("ดำเนินการเรียบร้อย");
      response.setSuccess(true);

      System.out.println(
          "********************************* START *********************************");
      System.out.println("DEBUG :: policy length = " + datalistPolicy.length());
      System.out.println("********************************* START *********************************");
      System.out.println("DEBUG :: policy length = " + datalistPolicy.length());
      JSONObject obj = new JSONObject();
      String type = null;
      String strDatePolicyInActive = null;
      int minDate = 0;
      String policyDate = "";
      int countPolicyActive = 0;
      for (int i = 0; i < datalistPolicy.length(); i++) {
        System.out.println("-------------- Policy ");
        System.out.println("DEBUG :: lifesum before = " + lifesum);
        System.out.println("DEBUG :: sumR06 before = " + sumR06);
        System.out.println("DEBUG :: policy_no = " + datalistPolicy.getJSONObject(i).getString("policyno"));
        System.out.println("DEBUG :: policy_status = " + datalistPolicy.getJSONObject(i).getString("policystatus1"));
        obj = datalistPolicy.getJSONObject(i);
        //check status Policy Active
        if (!isPolicyNotActive(obj)) {
          policystatus1 = obj.getString("policystatus1");
          typeGlobal = obj.getString("type");
          policyDate = obj.has("effectivedate") ? obj.getString("effectivedate") : "";
          try {
            System.out.println("i ============= " + i + "|" + datalistPolicy.length() + "|");
            System.out.println("obj ============= " + obj.toString());
            policyNo = obj.getString("policyno");
            certNo = obj.getString("certno");
          } catch (Exception e) {
            if (i != datalistPolicy.length() - 1)
              continue;
          }

          String group = null;
          type = null;
          String strDate = null;
          String endDate = null;
          int coVerage = 0;
          int creditLimit = 0;
          String ridergroup = null;
          String payperiod = obj.getString("payperiod");
          countPolicyActive++;
          if (getTypeChaek(typeGlobal, policystatus1)) {
            lifesum = obj.getNumber("sum");
            sumR06 = lifesum.intValue() + sumR06.intValue();
            System.out.println("DEBUG :: sumR06 after = " + sumR06);
            JSONArray dataListOfRider = callWSPolDb05(request, policyNo, certNo, typeGlobal, req);
//					HashMap<String, MapGroupRider> mapGroupRider = new HashMap<>();
            if (dataListOfRider != null) {
              for (int j = 0; j < dataListOfRider.length(); j++) {
                JSONObject object = dataListOfRider.getJSONObject(j);
                String riderType = object.getString("ridercode");
                boolean added = true;
                String riderstatus = object.getString("riderstatus");
                // BทุพพลภาพมีผลบังคับNรับปกติมีผลบังคับSรับต่ำกว่ามาตรฐานมีผลบังคับEงดเว้นการชำระเบี้ยมีผลบังคับB,N,S,E
                if (riderstatus.equals("N") || riderstatus.equals("B") || riderstatus.equals("S") || riderstatus.equals("E")) {
                  ridergroup = PolicyUtil.getRiderCode(riderType);
                  if (ridergroup.equals("R01")) {
                    type = "R01";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";// "25501101";
                    endDate = "25621101";
                    coVerage = object.has("ridersum") ? object.getInt("ridersum") : 0;// 500000;
                    creditLimit = 0;
                  } else if (ridergroup.equals("R02")) {
                    type = "R02";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";
                    endDate = "25621101";
                    coVerage = object.has("ridersum") ? object.getInt("ridersum") : 0;
                    creditLimit = 0;
                    // getCoverageOverview.setCoveragename(object.getString("ความคุ้มครองโรคร้ายแรง"));
                  } else if (ridergroup.equals("R03")) {
                    type = "R03";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";
                    endDate = "25621101";
                    coVerage = object.has("ridersum") ? object.getInt("ridersum") : 0;
                    creditLimit = 0;
                    // getCoverageOverview.setCoveragename(object.getString("คุ้มครองค่ารักษาพยาบาล"));
                  } else if (ridergroup.equals("R04")) {
                    type = "R04";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";
                    endDate = "25621101";
                    coVerage = object.has("ridersum") ? object.getInt("ridersum") : 0;
                    creditLimit = 0;
                    // getCoverageOverview.setCoveragename(object.getString("ชดเชยการสูญเสียรายได้"));
                  } else if (ridergroup.equals("R05")) {
                    type = "R05";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";
                    endDate = "25621101";
                    coVerage = object.has("ridersum") ? object.getInt("ridersum") : 0;
                    creditLimit = 0;
                    // getCoverageOverview.setCoveragename(object.getString("คุ้มครองค่ารักษาพยาบาลจากอุบัติเหตุ"));
                  } else if (ridergroup.equals("R06")) {
                    type = "R06";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";
                    endDate = "25621101";
                    coVerage = object.has("sum") ? object.getInt("sum") : 0;
                    int ridersum = object.has("ridersum") ? object.getInt("ridersum") : 0;
                    creditLimit = 0;
                    sumR06 = sumR06.intValue() + ridersum;
                  } else if (ridergroup.equals("R07")) {
                    type = "R07";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";
                    endDate = "25621101";
                    coVerage = object.has("ridersum") ? object.getInt("ridersum") : 0;
                    creditLimit = 0;
                    // getCoverageOverview.setCoveragename(object.getString("ความคุ้มครองกรณีทุพพลภาพ"));
                  } else if (ridergroup.equals("R08")) {
                    type = "R08";
                    group = PolicyUtil.getRiderMessage(ridergroup);
                    strDate = object.has("effectivedate") ? object.getString("effectivedate") : "";
                    endDate = "25621101";
                    coVerage = object.has("ridersum") ? object.getInt("ridersum") : 0;
                    creditLimit = 0;
                    // getCoverageOverview.setCoveragename(object.getString("คุ้มครองโรคมะเร็ง"));
                  } else {
                    added = false;
                    type = "";
                  }
                  System.out.println("Debug :: ridertype = " + riderType + ", ridergroup = " + ridergroup + ", added = " + added + "|" + type + "|");
                  if (added) {
                    getCoverageOverview = new GetCoverageOverviewResponse();
                    getCoverageOverview.setCoverageName(group);
                    getCoverageOverview.setCoverageCode(type);
                    getCoverageOverview.setCoverageStartDate(strDate);
                    getCoverageOverview.setCoverageEndDate(endDate);
                    getCoverageOverview.setCoverageRemain(coVerage);
                    getCoverageOverview.setCoverageUsed(creditLimit);
                    System.out.println("1 lifesum ============= " + type + "|" + lifesum + "|" + object.getLong("ridersum") + "|");

                    if (type.equals("R06")) {
                      isR06 = true;
                    } else
                      isR06 = false;

                    if (sumByGroup.containsKey(type)) {
                      long sum = sumByGroup.get(type).getCoverageSum();
                      sum = sum + object.getLong("ridersum");
                      getCoverageOverview.setCoverageSum(sum);
                    } else {
                      getCoverageOverview.setCoverageSum(object.getLong("ridersum"));
                    }
//									System.out.println("DEBUG :: sumR06 after = " + sumR06);
                    System.out.println("type lifesum ============= " + type + "|" + getCoverageOverview.toString() + "|");
                    sumByGroup.put(type, getCoverageOverview);

                    // 09092020
                    if (null == mapGroupRider.get(type)) {
                      mapGroupRider.put(type, new MapGroupRiderM());
                    }
                    int year = 0;
                    if ((("J01").equalsIgnoreCase(riderType) || ("J02").equalsIgnoreCase(riderType)
                            || ("J03").equalsIgnoreCase(riderType))
                            && (null != payperiod && payperiod.length() >= 4)) {
                      year = Integer.parseInt(payperiod.substring(0, 2));
                    }
                    MapGroupRiderM mapGR = mapGroupRider.get(type);
                    ArrayList<String> listridercode = mapGR.getListridercode();
                    ArrayList<Integer> listyear = mapGR.getListyear();
                    listridercode.add(riderType);
                    listyear.add(year);
                    mapGroupRider.put(type, mapGR);

                  }
                  if(strDate == null || strDate.equals(" ") || strDate.equals("")){
                    strDate = obj.getString("effectivedate");
                  }
                } else {
                  strDate = obj.getString("effectivedate");
                }
              }
            } else {
              strDate = obj.getString("effectivedate");
            }

          } else if (checkPolicyType(policystatus1)) {

            String sum = getLifeSumStatusAERU(policyNo, policystatus1);
            // lifesum = 0.00; //รอ confirm ค่าว่าดึงจากไหน
            System.out.println("AERU ======== " + policyNo + "|" + policystatus1 + "|" + sum + "|");
            lifesum = Integer.parseInt(sum); // รอ confirm ค่าว่าดึงจากไหน
            System.out.println("lifesum : " + lifesum + " | sumR06.intValue() : " + sumR06.intValue());
            sumR06 = lifesum.intValue() + sumR06.intValue();
            strDate = obj.getString("effectivedate");
          }else{
            strDate = obj.getString("effectivedate");
          }
          if(minDate == 0 ){
            minDate = strDate != null && (!strDate.equals("")) && (!strDate.equals(" ")) ? Integer.parseInt(strDate): minDate ;
          }else{
            minDate = strDate != null && (!strDate.equals("") && (!strDate.equals(" "))) && Integer.parseInt(strDate) < minDate ? Integer.parseInt(policyDate) : minDate;
          }
        }else{
          if(strDatePolicyInActive == null || strDatePolicyInActive.trim().length() == 0){
            strDatePolicyInActive = obj.getString("effectivedate");
          }else{
            strDatePolicyInActive = Integer.parseInt(obj.getString("effectivedate")) < Integer.parseInt(strDatePolicyInActive) ?
                    obj.getString("effectivedate") : strDatePolicyInActive;
          }

        }

        if (i == (datalistPolicy.length() - 1)) {
          System.out.println("i == datalistpolicy.length() - 1 : " +"|"+type+"|"+isR06+"|");
          if (isR06) {
            getCoverageOverview = sumByGroup.get("R06");
            getCoverageOverview.setCoverageSum(sumR06.intValue());
            sumByGroup.remove("R06");
            sumByGroup.put(type, getCoverageOverview);
          } else {
            type = "R06";

            getCoverageOverview = new GetCoverageOverviewResponse();
            getCoverageOverview.setCoverageName(PolicyUtil.getRiderMessage("R06"));
            getCoverageOverview.setCoverageCode(type); //
            getCoverageOverview.setCoverageStartDate(countPolicyActive != 0 ? String.valueOf(minDate) : strDatePolicyInActive);
            getCoverageOverview.setCoverageEndDate("25621101"); //
            getCoverageOverview.setCoverageRemain(0); //
            getCoverageOverview.setCoverageUsed(0);
            getCoverageOverview.setCoverageSum(sumR06.longValue());

            sumByGroup.put(type, getCoverageOverview);
          }
        }


        System.out.println(
                "******************************************************************" + sumByGroup.values());
        List<GetCoverageOverviewResponse> list_of_data = new ArrayList<GetCoverageOverviewResponse>(
                sumByGroup.values());
        listOfProtection.setList_of_protection(list_of_data);
        response.setResult(listOfProtection);
//        response.setResponseStatus(PolicyUtil.responseStatusSuccess());
        response.setResponseCode("200");
        response.setResponseDescription("ดำเนินการเรียบร้อย");
        response.setSuccess(true);
      }

      for (int j = 1; j <= 8; j++) {
        ArrayList<CoverageDetailM> list = new ArrayList<>();
        MapGroupRiderM mapGR = mapGroupRider.get("R0" + j);
        if(null != mapGR) {
          ArrayList<String> listridercode = mapGR.getListridercode();
          System.out.println("R0" + j+ "|policyNo : " + policyNo +"|certNo : "+certNo+"|listridercode : "+listridercode+"|");
          ArrayList<Integer> listyear = mapGR.getListyear();
          ArrayList<CoverageMainM> coverageMainM =
              CoverageMainDaoImpl.searchCoverageMByRidercodeList(listridercode, listyear);
          for (CoverageMainM bean : coverageMainM) {

            if(null == bean.getCoverage_suminsure() || bean.getCoverage_suminsure().toString().equals("0") )
              continue;
//						System.out.println("policyNo : " + policyNo +"|certNo : "+certNo+"|listridercode : "+listridercode+"|");
//						System.out.println("getCoverage_group_code :"+ bean.getCoverage_group_code() +"|"+bean.getCoverage_suminsure()+"|");

//						HashMap<String, CoverageDetail> mapCoverageDetail = new HashMap<>();
            coverageDetail = mapCoverageDetail.get("R0" + j +"|"+bean.getCoverage_group_code());
            if(coverageDetail == null)
            {
              System.out.println("coverageDetail == null" +"|"+"R0" + j +"|"+ bean.getCoverage_group_code() +"|"+bean.getCoverage_suminsure()+"|");
              coverageDetail = new CoverageDetailM();
              mapCoverageDetail.put("R0" + j +"|"+ bean.getCoverage_group_code(), coverageDetail);
            }
            else
              System.out.println("coverageDetail != null | "+"R0" + j +"|"+ bean.getCoverage_group_code() + coverageDetail+"|"+bean.getCoverage_suminsure()+"|");
//						System.out.println("bean != null | " + bean+"|");

            coverageDetail.setCoverageGroupCode(null == bean.getCoverage_group_code() ? "" : bean.getCoverage_group_code());
            coverageDetail.setCoverageNameDetailTh(null == bean.getCoverage_name_detail_th() ? "" : bean.getCoverage_name_detail_th());
            coverageDetail.setCoverageNameDetailEn(null == bean.getCoverage_name_detail_eng() ? "": bean.getCoverage_name_detail_eng());
            coverageDetail.setCoverageRemarkTh(	null == bean.getCoverage_remark_th() ? "" : bean.getCoverage_remark_th());
            coverageDetail.setCoverageRemarkEn(	null == bean.getCoverage_remark_eng() ? "" : bean.getCoverage_remark_eng());
            coverageDetail.setCoverageSumDetail(null ==  bean.getCoverage_suminsure() ? new BigDecimal(0) : bean.getCoverage_suminsure().add(coverageDetail.getCoverageSumDetail())  );
//						System.out.println("coverageDetail : " + coverageDetail);
            list.add(coverageDetail);
          }
          GetCoverageOverviewResponse coverageres = sumByGroup.get("R0" + j);
          System.out.println("coverageres " + "R0" + j + "|" + coverageres.toString());
          coverageres.setCoverageDetail(list);
        }
      }
      System.out.println("mapEffectiveDate = " + mapEffectiveDate.toString());
      System.out.println(response.toString());
      System.out.println("********************************* END *********************************");
    } catch (Exception e) {
      response.setSuccess(false);
      response.setResponseCode("500");
      response.setResponseDescription("ระบบขัดข้องกรุณาลองใหม่อีกครั้ง");
      response.setResult(null);
      log.error(Service, tranId, className, "GetCoverageOverviewBusiness Error Exception : " + e.getMessage());
             
      return response;
    }
    return response;
  }

  private String checkEffectiveDate(HashMap<String, String> effectiveDate) {
    int i = 0, current = 0;
    String empty = null;
    for (String s : effectiveDate.values()) {
      if (PolicyUtil.validateString(s)) {
        if (i == 0) {
          current = Integer.parseInt(s);
        }
        if (current > Integer.parseInt(s)) {
          current = Integer.parseInt(s);
        }
      } else {
        empty = s;
      }
      i++;
    }
    return empty == null ? String.valueOf(current) : empty;
  }

  private boolean checkRiderStatusNBSE(String riderStatus) {
    return riderStatus.equals("N")
        || riderStatus.equals("B")
        || riderStatus.equals("S")
        || riderStatus.equals("E");
  }

  private String getRequest(String customer_id, RequestMessages<GetCoverageOverviewRequest> req) {
    return "{"
        + "\"headerData\": {"
        + "	\"messageId\": \""
        + req.getHeaderData().getMessageId()
        + "\","
        + "	\"sentDateTime\": \""
        + req.getHeaderData().getSentDateTime()
        + "\""
        + "},"
        + "	\"requestRecord\": { "
        + "		\"customer_id\": \""
        + customer_id
        + "\""
        + "}"
        + "} ";
  }

  // mettod call web service
  private String callWS(HttpURLConnection connection, String request) throws Exception {
    // System.out.println("call service ...");
    OutputStream os = null;
    BufferedReader br = null;
    try {
      os = connection.getOutputStream();
      os.write(request.getBytes(StandardCharsets.UTF_8));
      System.out.println("callWS request : " + request);
      if (connection.getResponseCode() == HttpURLConnection.HTTP_OK
          || connection.getResponseCode() == HttpURLConnection.HTTP_CREATED)
        ; // donothing
      else {
        br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
        String tmp = "";
        StringBuilder output = new StringBuilder();
        while ((tmp = br.readLine()) != null) output.append(tmp);
        throw new Exception(
            "Failed : HTTP error code : " + connection.getResponseCode() + "\n, Error : " + output);
      }
      br =
          new BufferedReader(
              new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
      String tmp = "";
      StringBuilder output = new StringBuilder();
      while ((tmp = br.readLine()) != null) output.append(tmp);
      return output.toString();
    } catch (Exception e) {
      // log.error(e.getMessage());
      throw e;
    } finally {
      if (os != null) os.close();
      if (br != null) br.close();
      if (connection != null) connection.disconnect();
    }
  }

  private boolean getTypeChaek(String typeGlobal, String policyStatus1) {
    // type
    if (typeGlobal.toUpperCase().startsWith("CL")
        && (policyStatus1.toUpperCase().equals("I")
            || policyStatus1.toUpperCase().equals("N")
            || policyStatus1.toUpperCase().equals("T"))) {

      return true;
    } else
      return policyStatus1.toUpperCase().equals("B")
          || policyStatus1.toUpperCase().equals("F")
          || policyStatus1.toUpperCase().equals("H")
          || policyStatus1.toUpperCase().equals("I")
          || policyStatus1.toUpperCase().equals("J")
          || policyStatus1.toUpperCase().equals("N")
          // || policyStatus1.toUpperCase().equals("P")
          || policyStatus1.toUpperCase().equals("W");
  }

  private JSONArray callWSPolDb05(String request, String policyNo, String certNo, String typeGlobal,
                                  RequestMessages<GetCoverageOverviewRequest> req) {
    try {
      request = getRequestPol05(policyNo, certNo, typeGlobal, req);
      String responsepol05 = "";
      byte[] postDatapol05 = request.getBytes(StandardCharsets.UTF_8);
      int postDataLengthpol05 = postDatapol05.length;
      String contextpol05 = "/PolicyDataService/GetRiderDetial";
      CallService callService = new CallService();
      ApiConfigM apiConfigM = callService.readPoperties();
      URL urlPol05 = new URL(apiConfigM.getENDPOINT() + contextpol05);
      HttpURLConnection connectionpol05 = (HttpURLConnection) urlPol05.openConnection();
      connectionpol05.setRequestMethod("POST");
      connectionpol05.setDoOutput(true);
      connectionpol05.setRequestProperty("Content-Type", "application/json");
      connectionpol05.setRequestProperty("Authorization", "");
      connectionpol05.setRequestProperty("Content-Length", Integer.toString(postDataLengthpol05));
      connectionpol05.setUseCaches(false);
      connectionpol05.setRequestProperty("charset", "utf-8");
      responsepol05 = callWS(connectionpol05, request);
      JSONObject json2 = new JSONObject(responsepol05);
      JSONObject responseStatus = json2.getJSONObject("responseStatus");
      if (responseStatus.getString("errorCode").equals("200")) {
        JSONObject responseRecord2 = json2.getJSONObject("responseRecord");
        return responseRecord2.getJSONArray("list_of_rider");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  private HttpURLConnection setConnectionCallWS(String context, int lengthRequest)
      throws IOException {
    CallService callService = new CallService();
    ApiConfigM apiConfigM = callService.readPoperties();
    URL urlPol05 = new URL(apiConfigM.getENDPOINT() + context);
    System.out.println("url = " + urlPol05);
    HttpURLConnection connection = (HttpURLConnection) urlPol05.openConnection();
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("Authorization", "");
    connection.setRequestProperty("Content-Length", Integer.toString(lengthRequest));
    connection.setUseCaches(false);
    connection.setRequestProperty("charset", "utf-8");
    return connection;
  }

  private String getRequestPol05(String policy_no, String cert_no, String type,
                                 RequestMessages<GetCoverageOverviewRequest> req) {
    String request = "{" + "\"headerData\": {" +

            "	\"messageId\": \"" + req.getHeaderData().getMessageId() + "\"," + "	\"sentDateTime\": \""
            + req.getHeaderData().getSentDateTime() + "\"" + "}," + " \"requestRecord\": { " + " \"policyno\": \""
            + policy_no + "\" " + ",\"certno\": \"" + cert_no + "\"" + ",\"type\": \"" + type + "\"" + "}" + "} ";

    return request;
  }

  private boolean checkPolicyType(String policyStatus1) {
    return ("A").equalsIgnoreCase(policyStatus1)
        || ("E").equalsIgnoreCase(policyStatus1)
        || ("R").equalsIgnoreCase(policyStatus1)
        || ("U").equalsIgnoreCase(policyStatus1);
  }

  public String getLifeSumStatusAERU(String policyNo, String status) {
    String sum = "0";
    if (checkPolicyStatusUE(status)) {
      List<StatusueDao> list = StatusueDaoImpl.selectByPolicyno(policyNo);

      if (list != null && list.size() > 0) {
        StatusueDao statusue = list.get(0);
        if (status.equalsIgnoreCase("E")) {
          sum = statusue.getExtend().toString();
        } else if (status.equalsIgnoreCase("U")) {
          sum = statusue.getPaidup().toString();
        }
      }
    } else {
      if (status.equalsIgnoreCase("A")) {
        sum = getExtend(policyNo);
      } else if (status.equalsIgnoreCase("R")) {
        sum = getPaidUp(policyNo);
      }
    }
    return sum;
  }

  private String getPaidUp(String policyNo) {
    PublicRte.setRemote(true);
    Result result =
        PublicRte.getClientResult(
            "blmaster", "rte.bl.service.wawauthorize.CalculateApl", new String[] {policyNo});
    System.out.println(
        policyNo
            + " CalculateApl policyNo : "
            + policyNo
            + "|"
            + result.status()
            + "|"
            + result.value()
            + "|");

    if (result.status() == 0) {
      Vector<Object> rs = (Vector<Object>) result.value();
      System.out.println("totalPaidUp : " + rs.get(24));
      return (String) rs.get(24);
    }
    return "0";
  }

  private String getExtend(String policyNo) {
    PublicRte.setRemote(true);
    Result result =
        PublicRte.getClientResult(
            "blmaster", "rte.bl.service.wawauthorize.CalculateApl", new String[] {policyNo});
    System.out.println(
        policyNo
            + " CalculateApl policyNo : "
            + policyNo
            + "|"
            + result.status()
            + "|"
            + result.value()
            + "|");

    if (result.status() == 0) {
      Vector<Object> rs = (Vector<Object>) result.value();
      System.out.println("NSA : " + rs.get(16));
      return (String) rs.get(16);
    }
    return "0";
  }

  private boolean checkPolicyStatusUE(String policyStatus1) {
    return ("E").equalsIgnoreCase(policyStatus1) || ("U").equalsIgnoreCase(policyStatus1);
  }

  private boolean isPolicyNotActive(JSONObject obj) {
    try{
      String[] values = null;
      String type = obj.getString("type").split("-")[1];
      switch (type){
        case "CL" :
          values = new String[]{"C","D","L","M","S","V","W","X"};
          break;
        case "OL" :
        case "IND" :
        case "PA" :
          values = new String[]{"C","D","G","K","L","M","O","P","S","T","V","X","Y","Z","#","*"};
          break;
      }
      if(values != null){
        return Arrays.asList(values).contains(obj.getString("policystatus1"));
      }

    }
    catch (Exception e){
      throw e;
    }
    return false;
  }
}
