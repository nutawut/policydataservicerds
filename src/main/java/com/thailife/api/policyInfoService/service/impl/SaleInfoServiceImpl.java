package com.thailife.api.policyInfoService.service.impl;

import java.util.Vector;

import com.thailife.api.policyInfoService.controller.pol06.SaleInfoController;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.IndmastDao;
import com.thailife.api.policyInfoService.dao.LicenseDao;
import com.thailife.api.policyInfoService.dao.OrdmastDao;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.dao.UniversallifeDao;
import com.thailife.api.policyInfoService.dao.WhlmastDao;
import com.thailife.api.policyInfoService.dao.impl.CertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.IndmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.LicenseDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.OrdmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UnitLinkDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UniversallifeDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.WhlmastDaoImpl;
import com.thailife.api.policyInfoService.model.request.SaleInfoRequest;
import com.thailife.api.policyInfoService.model.response.SaleInfoResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SaleInfoService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import lombok.extern.log4j.Log4j;
import manit.rte.Result;
import utility.rteutility.PublicRte;
@Service
public class SaleInfoServiceImpl implements SaleInfoService {
    private final ExtLogger log = ExtLogger.create(SaleInfoServiceImpl.class);
    private final String className = "SaleInfoServiceImpl";
  private final String Service = "GetSaleInfo";
  private String tranId;
  
  @Autowired OrdmastDaoImpl ordmastDaoImpl;
  @Autowired WhlmastDaoImpl whlmastDaoImpl;
  @Autowired IndmastDaoImpl indmastDaoImpl;
  @Autowired UniversallifeDaoImpl universallifeDaoImpl;
  @Autowired UnitLinkDaoImpl unitLinkDaoImpl;
  @Autowired CertDaoImpl certDaoImpl;
  @Autowired LicenseDaoImpl licenseDaoImpl;
  
  @Override
  public ServiceResults<SaleInfoResponse> getResponse(SaleInfoRequest req, String tran_Id) {
    ServiceResults<SaleInfoResponse> response = new ServiceResults<>();
    //SaleInfoResponse saleInfo = new SaleInfoResponse();
    String policyno = req.getPolicyno();
    String type = ProductType.typeGetTable(req.getType());
    WebServiceMsg wServiceMsg = new WebServiceMsg();
    tranId = tran_Id;
    try {
      
      response.setResponseCode(wServiceMsg.getMessageByCode("200")[0]);
      response.setResponseDescription(wServiceMsg.getMessageByCode("200")[1]);
      response.setSuccess(true);
      SaleInfoResponse saleInfo = getSaleInfo(policyno, type);
      if(saleInfo == null){
          response.setResponseCode("POL-06-002");
          response.setResponseDescription("ไม่พบข้อมูลที่ต้องการในระบบ");
          response.setSuccess(true);
      }
      response.setResult(saleInfo);
      
    }catch (Exception e) {
      e.printStackTrace();
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setSuccess(false);
    }
    return response;
  }

  private SaleInfoResponse getSaleInfo(String policyno, String type) throws Exception {
    
    SaleInfoResponse saleInfo = new SaleInfoResponse();
    String salesId = null;
    
    if (("ordmast").equalsIgnoreCase(type)) {
      OrdmastDao ordmast = ordmastDaoImpl.findByPolicyno(policyno);
      salesId = ordmast != null ? ordmast.getSalesid() : null;
    } else if (("whlmast").equalsIgnoreCase(type)) {
      WhlmastDao whlmast = whlmastDaoImpl.findByPolicyno(policyno);
      salesId = whlmast != null ? whlmast.getSalesid() : null;
    } else if (("indmast").equalsIgnoreCase(type)) {
      IndmastDao indmast = indmastDaoImpl.findByPolicyno(policyno);
      salesId = indmast != null ? indmast.getSalesid() : null;
    } else if (("universallife").equalsIgnoreCase(type)) {
      UniversallifeDao universallife = universallifeDaoImpl.findByPolicyno(policyno);
      salesId = universallife != null ? universallife.getSalesid() : null;
    } else if (("unitlink").equalsIgnoreCase(type)) {
      UnitLinkDao unitlink = unitLinkDaoImpl.findByPolicyno(policyno);
      salesId = unitlink != null ? unitlink.getSalesid(): null;
    } else if (("mortgage").equalsIgnoreCase(type)) { // CL
        CertDao cert = certDaoImpl.findByIdCertno(policyno);
        if (cert != null && cert.getPolicyno() != null) {
          saleInfo.setPrename("");
          saleInfo.setFirstname("");
          saleInfo.setLastname("");
          saleInfo.setTelephone("");
          saleInfo.setLicenseno("");
        }
    }
    if (salesId != null) {
        PublicRte.setRemote(true);
        Result rs = PublicRte.getResult("searchsales", "rte.search.sales.SearchSalesData",
                new Object[] { salesId, "S", "I" });
        log.error(Service, tranId, className, "searchsales rs : " + rs);
        if (rs.status() == 0) {
            Vector<String[]> s = (Vector<String[]>) rs.value();
            if (s.size() != 0) {
                String[] data = s.get(0);
                String citizenid = data[0];

                LicenseDao license =licenseDaoImpl.findByCitizenid(citizenid);
                String licenseno = license.getLicenseno() == null ? "" : license.getLicenseno();

                saleInfo.setPrename(data[1].trim());
                saleInfo.setFirstname(data[2].trim());
                saleInfo.setLastname(data[3].trim());
                saleInfo.setTelephone(data[9].trim());
                saleInfo.setLicenseno(licenseno.trim());
            }
        }
        
        return saleInfo;
    }
    
    return null;
  }

}
