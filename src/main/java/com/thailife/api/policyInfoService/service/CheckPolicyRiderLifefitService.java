package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.CheckPolicyRiderLifefitResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface CheckPolicyRiderLifefitService {
    ServiceResults<CheckPolicyRiderLifefitResponse> getResponse(SearchPolicyByCustomerIdRequest req, String tranId);
}
