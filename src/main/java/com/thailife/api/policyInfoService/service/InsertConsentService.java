package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.InsertConsentRequest;
import com.thailife.api.policyInfoService.model.response.InsertConsentResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface InsertConsentService {

  ServiceResults<InsertConsentResponse> getResponse(InsertConsentRequest req, String tranId);

}
