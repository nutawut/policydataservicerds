package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.CurrentAVInfoRequest;
import com.thailife.api.policyInfoService.model.response.CurrentAVInfoResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface GetCurrentAVInfoService {

  ServiceResults<CurrentAVInfoResponse> getResponse(CurrentAVInfoRequest req);

}
