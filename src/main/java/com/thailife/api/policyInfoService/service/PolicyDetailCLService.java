package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.response.PolicyDetailCLResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface PolicyDetailCLService {
  ServiceResults<PolicyDetailCLResponse> getResponse(PolicyDetailRequest req, String tranId);
}
