package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.CertRiderDao;
import com.thailife.api.policyInfoService.model.request.NextPayPeriodListRequest;
import com.thailife.api.policyInfoService.model.response.NextPayPeriodListResponse;
import com.thailife.api.policyInfoService.model.response.NextPayPeriodResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.util.PolicyException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface NextPayPeriodService {

  public String getPaydescUL(String policyno) throws Exception;

  public String getPaydescULIP(String policyno) throws Exception;

  public String getPaydescOL(String policyno) throws Exception;
  public String getPaydescIND(String policyno) throws Exception;
  public String getPaydescWHL(String policyno) throws Exception;
}
