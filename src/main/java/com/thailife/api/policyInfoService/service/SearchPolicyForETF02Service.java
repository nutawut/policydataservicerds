package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.response.SearchPolicyResponse;

import java.util.List;

public interface SearchPolicyForETF02Service {
    List<SearchPolicyResponse> searchPolicyForETF02Service(
            List<SearchPolicyResponse> request, String transactionId) throws Exception;
}
