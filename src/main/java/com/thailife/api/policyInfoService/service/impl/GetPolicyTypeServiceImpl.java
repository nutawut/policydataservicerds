package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.controller.pol17.GetPolicyTypeController;
import com.thailife.log.ExtLogger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.IndmastDao;
import com.thailife.api.policyInfoService.dao.OrdmastDao;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.dao.UniversallifeDao;
import com.thailife.api.policyInfoService.dao.WhlmastDao;
import com.thailife.api.policyInfoService.dao.impl.CertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.IndmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.OrdmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UnitLinkDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UniversallifeDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.WhlmastDaoImpl;
import com.thailife.api.policyInfoService.model.request.PolicyTypeRequest;
import com.thailife.api.policyInfoService.model.response.PolicyTypeResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.GetPolicyTypeService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import utility.rteutility.PublicRte;

@Service
public class GetPolicyTypeServiceImpl implements GetPolicyTypeService {
  private final ExtLogger log = ExtLogger.create(GetPolicyTypeServiceImpl.class);

  private final String className = "GetPolicyTypeController";
  private final String Service = "GetPolicyType";
  private String tranId;

  @Autowired OrdmastDaoImpl ordmastDaoImpl;
  @Autowired IndmastDaoImpl indmastDaoImpl;
  @Autowired WhlmastDaoImpl whlmastDaoImpl;
  @Autowired UniversallifeDaoImpl universallifeDaoImpl;
  @Autowired UnitLinkDaoImpl unitLinkDaoImpl;
  @Autowired CertDaoImpl certDaoImpl;

  @Override
  public ServiceResults<PolicyTypeResponse> getResponse(PolicyTypeRequest req, String tran_Id) {
    ServiceResults<PolicyTypeResponse> response = new ServiceResults<>();
    PolicyTypeResponse policyType;
    WebServiceMsg wServiceMsg = new WebServiceMsg();
    String policyNo = req.getPolicyno();
    String certNo = req.getCertno();
    tranId = tran_Id;
    try {
      // PublicRte.setRemote(true);
      policyType = getPolicyTypeResponse(policyNo, certNo);
      if (policyType != null) {
        response.setResult(policyType);
        response.setResponseCode(wServiceMsg.getMessageByCode("200")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("200")[1]);
        response.setSuccess(true);
      } else {
        response.setResult(policyType);
        response.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        response.setSuccess(true);
      }

    } catch (Exception e) {
      log.error(Service, tranId, className, "Error Exception getResponse : " + e.getMessage());
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setSuccess(false);
    }
    return response;
  }

  @Override
  public PolicyTypeResponse getPolicyTypeResponse(String policyNo, String certNo) {
    PolicyTypeResponse policyType = new PolicyTypeResponse();
    try {
      PublicRte.setRemote(true);
      JSONObject policyDetailJson = getPolicyData(policyNo, certNo);
      if (policyDetailJson != null) {
        String planCode =
            policyDetailJson.has("plancode") ? policyDetailJson.getString("plancode") : policyNo;
        String branch =
            policyDetailJson.has("branch")
                ? policyDetailJson.get("branch").toString()
                : utility.underwrite.BankAssure.tlBranchFromPlan(planCode);
        String policyTypeStr =
            policyDetailJson.has("policytype") ? policyDetailJson.getString("policytype") : "";
        String baseType =
            policyDetailJson.has("basetype") ? policyDetailJson.getString("basetype") : "";
        String type = ProductType.plancodeGetType(planCode, branch, policyTypeStr, baseType);
        type = type.equalsIgnoreCase("OTHER") ? policyTypeStr : type;
        policyType.setScreentype(ProductType.typeGetScreenType(type, policyTypeStr, planCode));
        policyType.setType(type);
        return policyType;
      }
    } catch (Exception e) {
      log.error(
          "getPolicyTypeResponse",
          tranId,
          className,
          "Error Exception getResponse : " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return null;
  }

  private JSONObject getPolicyData(String policyno, String certno) {
    try {
      if (!PolicyUtil.StringIsNullOrEmpty(policyno) && !PolicyUtil.StringIsNullOrEmpty(certno)) {
        // CertPK pk = new CertPK(policyno, certno);
        CertDao cert = certDaoImpl.findByPolicynoAndCertno(policyno, certno);
        if (cert != null && !PolicyUtil.StringIsNullOrEmpty(cert.getPolicyno())) {
          JSONObject json = new JSONObject(cert);
          json.put("policytype", "CL");
          json.put("basetype", "CL");
          json.put("payperiod", cert.getPayperiod());
          return json;
        }
      }
      OrdmastDao ordmast = ordmastDaoImpl.findByPolicyno(policyno);
      if (ordmast != null && !PolicyUtil.StringIsNullOrEmpty(ordmast.getPolicyno())) {
        JSONObject json = new JSONObject(ordmast);
        json.put("plancode", ordmast.getPlancode());
        json.put("branch", ordmast.getBranch());
        json.put("policytype", "OL");
        json.put("basetype", "OL");
        json.put("payperiod", ordmast.getPayperiod());
        return json;
      }
      IndmastDao indmast = indmastDaoImpl.findByPolicyno(policyno);
      if (indmast != null && !PolicyUtil.StringIsNullOrEmpty(indmast.getPolicyno())) {
        JSONObject json = new JSONObject(indmast);
        json.put("plancode", indmast.getPlancode());
        json.put("branch", indmast.getBranch());
        json.put("policytype", "OL");
        json.put("basetype", "IND");
        json.put("payperiod", indmast.getPayperiod());
        return json;
      }
      WhlmastDao whlmast = whlmastDaoImpl.findByPolicyno(policyno);
      if (whlmast != null && !PolicyUtil.StringIsNullOrEmpty(whlmast.getPolicyno())) {
        JSONObject json = new JSONObject(whlmast);
        json.put("plancode", whlmast.getPlancode());
        json.put("branch", whlmast.getBranch());
        json.put("policytype", "OL");
        json.put("basetype", "WHL");
        json.put("payperiod", whlmast.getPayperiod());
        return json;
      }
      UniversallifeDao universallife = universallifeDaoImpl.findByPolicyno(policyno);
      if (universallife != null && !PolicyUtil.StringIsNullOrEmpty(universallife.getPolicyno())) {
        JSONObject json = new JSONObject(universallife);
        json.put("plancode", universallife.getPlancode());
        json.put("branch", universallife.getBranch());
        json.put("policytype", "OL");
        json.put("basetype", "UL");
        json.put("payperiod", universallife.getPayperiod());
        return json;
      }
      UnitLinkDao unitlink = unitLinkDaoImpl.findByPolicyno(policyno);
      if (unitlink != null && !PolicyUtil.StringIsNullOrEmpty(unitlink.getPolicyno())) {
        JSONObject json = new JSONObject(unitlink);
        json.put("plancode", unitlink.getPlancode());
        json.put("branch", unitlink.getBranch());
        json.put("policytype", "ULIP");
        json.put("basetype", "ULIP");
        json.put("payperiod", unitlink.getPayperiod());
        return json;
      }
    } catch (Exception e) {
      log.error(
          Service,
          tranId,
          className,
          "Error GetPolicyTypeServiceImpl getPolicyData : " + e.getMessage());
    }
    return null;
  }
}
