package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.response.PolicyDetailOLResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface PolicyDetailOLService {
  public ServiceResults<PolicyDetailOLResponse> getResponse(PolicyDetailRequest req, String tranId);
}
