package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.GetDividendInfoRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetDividendInfoListResponse;
import com.thailife.api.policyInfoService.model.response.GetDividendInfoResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

public interface GetDividendInfoService {

  String getBranchTest(String branch, Connection con) throws Exception;

  List<GetDividendInfoResponse> getDividendInfo(String policyNo, String certNo) throws Exception;

  ResponseMessages<GetDividendInfoListResponse> service(
      RequestMessages<GetDividendInfoRequest> request);

  BigDecimal sumBenefitAccruedByStatusReceive(List<GetDividendInfoResponse> listBenefit);

  void setTransactionId(String transactionId);
}
