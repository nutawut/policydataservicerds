package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByPartyIdResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface SearchPolicyByPartyIdService {
  public ServiceResults<SearchPolicyByPartyIdResponse> getResponse(
      SearchPolicyByCustomerIdRequest request, String tranId);
}
