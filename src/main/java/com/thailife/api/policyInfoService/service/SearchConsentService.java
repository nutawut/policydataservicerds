package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.SearchConsentRequest;
import com.thailife.api.policyInfoService.model.response.SearchConsentResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface SearchConsentService {

  ServiceResults<SearchConsentResponse> getResponse(SearchConsentRequest req, String tranId);

}
