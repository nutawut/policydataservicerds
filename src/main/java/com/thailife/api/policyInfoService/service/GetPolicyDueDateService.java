package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.GetPolicyDueDateRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetPolicyDueDateResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface GetPolicyDueDateService {

  ServiceResults<GetPolicyDueDateResponse> getResponse(RequestMessages<GetPolicyDueDateRequest> requestRecord);

}
