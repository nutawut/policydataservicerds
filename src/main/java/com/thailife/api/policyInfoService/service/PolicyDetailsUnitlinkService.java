package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.response.PolicyDetailsUnitlinkResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface PolicyDetailsUnitlinkService {
  public ServiceResults<PolicyDetailsUnitlinkResponse> getResponse(PolicyDetailRequest req, String tranId);
}
