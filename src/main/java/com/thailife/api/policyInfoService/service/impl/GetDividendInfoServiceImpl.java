package com.thailife.api.policyInfoService.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.*;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.BenefitDao;
import com.thailife.api.policyInfoService.dao.BrafileDao;
import com.thailife.api.policyInfoService.dao.impl.BenefitDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.BrafileDaoImpl;
import com.thailife.api.policyInfoService.model.request.GetDividendInfoRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.*;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.iServiceRTE.IServiceRtePOL;
import master.master.MasterFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.service.GetDividendInfoService;
import utility.support.StatusInfo;

@Service
public class GetDividendInfoServiceImpl implements GetDividendInfoService {
  private static final String className = "GetDividendInfoServiceImpl";
  private static final String sericeName = "GetDividendInfo";
  private static final Log log = new Log();
  private String transactionId;
  @Autowired private BrafileDaoImpl braFileDao;
  @Autowired private BenefitDaoImpl benefitDao;

  public String getBranchTest(String branch, Connection con) throws Exception {
    String branchName;
    try {
      BrafileDao braFile = braFileDao.findByBranch(branch, con);
      if (braFile != null) branchName = braFile.getBranchname();
      else branchName = "";

    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("getBranchTest|branch:" + branch, e);
    }
    return branchName;
  }

  @Override
  public List<GetDividendInfoResponse> getDividendInfo(String policyNo, String certNo)
      throws Exception {
    log.printInfo(className, "<== [Start] GetDividendInfo Service ==>");
    log.printDebug(
        className, String.format("param [ PolicyNo => %s | CertNo => %s ]", policyNo, certNo));
    List<GetDividendInfoResponse> list = new ArrayList<>();
    List<DividendResponse> listDividend = new ArrayList<>();
    try (Connection con = DBConnection.getConnectionBusProduct()) {
      PolicyUtil.checkRemote();
      IServiceRtePOL wan = new IServiceRtePOL();
      wan.findPolicy(policyNo);
      wan.getMasterDetail();
      MasterFile file = new MasterFile();
      String policyType = file.findMasterType(1, new String[] {policyNo});
      log.printDebug(className, "PolicyType ==> " + policyType);
      if (policyType.equals("")) {
        throw new PolicyException(String.format("PolicyNo : %s cannot found PolicyType", policyNo));
      } else {
        if (!"L".equals(policyType) && !"U".equals(policyType)) {
          Vector<?> detailPolicy = wan.getDividend();
          if (detailPolicy.size() > 0) {
            for (Object o : detailPolicy) {
              String[] b = (String[]) o;
              log.printDebug(className, "Array detailPolicy from Rte : " + Arrays.toString(b));
              String typeDiv = getTypeDividend(b[0]);

              listDividend.add(setDividendResponse(b, typeDiv, policyType));
            }
          } else {
            ArrayList<BenefitDao> listBenefitM;
            try (Connection conPol = DBConnection.getConnectionPolicy()) {
              listBenefitM = benefitDao.searchBenefitMByPolicyNo(policyNo, policyType, conPol);
            }

            Map<String, String> branch = braFileDao.searchAllBranch(con);
            for (BenefitDao b : listBenefitM) {
              String channel =
                  !("").equals(b.getBranch().trim()) ? branch.get(b.getBranch().trim()) : "";
              GetDividendInfoResponse infoResponse = new GetDividendInfoResponse();
              infoResponse.setBenefit_year(b.getPayyear());
              infoResponse.setBenefit_premium((b.getAmount().add(b.getInterest()).setScale(2)));
              infoResponse.setBenefit_effective_date(setEffectiveDate(b.getPaydate()));
              infoResponse.setBenefit_amount_receive(b.getAmount().setScale(2));
              infoResponse.setBenefit_interest(b.getInterest().setScale(2));
              infoResponse.setBenefit_receivedstatus(("2").equals(b.getPayflag()) ? "1" : "2");
              infoResponse.setBenefit_type(b.getTypediv());
              infoResponse.setBenefit_channel_receive(channel);
              infoResponse.setBenefit_status(b.getPayflag());
              infoResponse.setBenefit_statusdesc(StatusInfo.receiveDvPayStatus(b.getPayflag()));
              list.add(infoResponse);
            }
          }
        }

        if (listDividend.size() > 0) {
          String channel;
          for (DividendResponse tmp : listDividend) {
            if (!tmp.getBranch().trim().equals("")) {
              String branchcode =
                  tmp.getBranch().equals("0.00") || tmp.getBranch().equals("0")
                      ? "000"
                      : tmp.getBranch();
              channel = getBranchTest(branchcode, con);
            } else {
              channel = "";
            }
            log.printDebug(
                className, String.format("DividendResponse.getYear() : %s", tmp.getYear()));
            list.add(setGetDividendInfoResponse(tmp, channel));
          }
        }
      }
    } catch (Exception e) {
      log.printError(className, String.format("Error => %s", e.getMessage()));
      throw e;
    } finally {
      log.printInfo(className, "<== [End] GetDividendInfo Service ==>");
    }
    return list;
  }

  @Override
  public BigDecimal sumBenefitAccruedByStatusReceive(List<GetDividendInfoResponse> listBenefit) {
    BigDecimal sumBenefitAccrued = BigDecimal.ZERO;
    try {
      for (GetDividendInfoResponse temp : listBenefit) {
        if ("1".equals(temp.getBenefit_receivedstatus())) {
          sumBenefitAccrued =
              sumBenefitAccrued.add(PolicyUtil.getDefaultBigDecimal(temp.getBenefit_premium()));
        }
      }
      log.printDebug(
          className,
          "TransactionId : " + transactionId + " | SumBenefitAccrued => " + sumBenefitAccrued);
    } catch (Exception e) {
      log.printError(
          className,
          String.format(
              "TransactionId : %s | Error sumBenefitAccruedByStatusReceive => %s",
              transactionId, e.getMessage()));
      e.printStackTrace();
      throw e;
    }
    return sumBenefitAccrued;
  }

  private GetDividendInfoResponse setGetDividendInfoResponse(DividendResponse tmp, String channel) {
    GetDividendInfoResponse getDividendInfo = new GetDividendInfoResponse();
    getDividendInfo.setBenefit_year(
        (tmp.getYear()).matches("^[0-9]+$") ? Integer.parseInt(tmp.getYear()) : 0);
    getDividendInfo.setBenefit_premium(doubleToBigDecimal(tmp.getAmount() + tmp.getInterest()));
    getDividendInfo.setBenefit_effective_date(setEffectiveDate(tmp.getDate()));
    getDividendInfo.setBenefit_amount_receive(doubleToBigDecimal(tmp.getAmount()));
    getDividendInfo.setBenefit_interest(doubleToBigDecimal(tmp.getInterest()));
    getDividendInfo.setBenefit_type(tmp.getDividendType());
    getDividendInfo.setBenefit_channel_receive(channel);
    getDividendInfo.setBenefit_status(tmp.getPayFlagStatus());
    getDividendInfo.setBenefit_statusdesc(tmp.getPayFlagStatusDesc());
    getDividendInfo.setBenefit_receivedstatus(tmp.getPayFlag());
    return getDividendInfo;
  }

  private static String setEffectiveDate(String val) {
    return val.substring(6, 8)
        .concat("/")
        .concat(val.substring(4, 6))
        .concat("/")
        .concat(val.substring(0, 4));
  }

  private DividendResponse setDividendResponse(String[] b, String typeDiv, String policyType) {
    DividendResponse dividend = new DividendResponse();
    dividend.setDividendType(typeDiv);
    dividend.setYear(b[1]);
    dividend.setDate(b[2]);
    dividend.setAmount(Double.parseDouble(b[3]));
    dividend.setInterest(Double.parseDouble(b[4]));
    dividend.setPayFlag(b[5].equals("Y") ? "2" : "1");
    dividend.setPolicyType(policyType);
    dividend.setBranch(b[8]);
    dividend.setPayFlagStatus(b[7]);
    dividend.setPayFlagStatusDesc(b[6]);
    return dividend;
  }

  private String getTypeDividend(String typeDividend) {
    switch (typeDividend) {
      case "1":
      case "D":
        // typeDiv = "D";
        return "เงินปันผล";
      case "0":
      case "P":
        // typeDiv = "P";
        return "เงินจ่ายคืน";
      case "2":
      case "A":
        // typeDiv = "A";
        return "เงินบำนาญ";
      default:
        return typeDividend;
    }
  }

  @Override
  public ResponseMessages<GetDividendInfoListResponse> service(
      RequestMessages<GetDividendInfoRequest> request) {
    List<GetDividendInfoResponse> list;
    GetDividendInfoListResponse getDividendInfoList = new GetDividendInfoListResponse();
    ResponseMessages<GetDividendInfoListResponse> response = new ResponseMessages<>();
    transactionId = request.getHeaderData() != null ? request.getHeaderData().getMessageId() : "";
    log.printInfo(sericeName, transactionId, className, "error validate Customer_id is invalid");
    try {
      if (!PolicyUtil.validateHeaderData(request.getHeaderData())
          || validateRequest(request.getRequestRecord())) {
        response.setResponseStatus(PolicyUtil.responseValidateError());
      } else {
        String policyNo = request.getRequestRecord().getPolicyno();
        String certNo = request.getRequestRecord().getCertno();
        list = getDividendInfo(policyNo, certNo);

        if (list.size() > 0) {
          setTransactionId(request.getHeaderData().getMessageId());
          BigDecimal sumBenefitAccrued = sumBenefitAccruedByStatusReceive(list);
          getDividendInfoList.setList_of_benefit(list);
          getDividendInfoList.setBenefit_accrued(sumBenefitAccrued.setScale(2, RoundingMode.FLOOR));
          response.setResponseRecord(getDividendInfoList);
          // set ResponseStatus success
          response.setResponseStatus(PolicyUtil.responseStatusSuccess());
        } else {
          getDividendInfoList.setList_of_benefit(list);
          response.setResponseRecord(getDividendInfoList);
          response.setResponseStatus(PolicyUtil.responseDataNotFound());
        }
      }

    } catch (PolicyException e) {
      log.printError(className, "errorMessage: " + e.getMessage());
      e.printStackTrace();
      response.setResponseStatus(PolicyUtil.responseDataNotFound());
    } catch (Exception e) {
      e.printStackTrace();
      response.setResponseStatus(PolicyUtil.responseInternalServerError());
      log.printError(className, "errorMessage: " + e.getMessage());
    } finally {
      response.setHeaderData(PolicyUtil.headerDataResponse(request.getHeaderData()));
      log.printInfo(className, " service [END]");
    }
    return response;
  }

  private boolean validateRequest(GetDividendInfoRequest request) {
    if (request == null) {
      log.printInfo(
          sericeName, transactionId, className, "error validate requestRecord is invalid");
      return true;
    } else {
      if (request.getPolicyno() == null) {
        log.printInfo(sericeName, transactionId, className, "error validate policyNo is invalid");
        return true;
      } else {
        if (request.getPolicyno().isEmpty()) {
          log.printInfo(sericeName, transactionId, className, "error validate policy is invalid");
          return true;
        }
        if (request.getPolicyno().length() > 8) {
          log.printInfo(sericeName, transactionId, className, "error validate policyNo is invalid");
          return true;
        }
      }

      if (!request.getCertno().isEmpty()) {
        log.printInfo(sericeName, transactionId, className, "error validate certno is invalid");
        return true;
      }
      if (request.getType() == null) {
        log.printInfo(sericeName, transactionId, className, "error validate type is invalid");
        return true;
      } else {
        if (request.getType().isEmpty()) {
          log.printInfo(sericeName, transactionId, className, "error validate type is invalid");
          return true;
        }
        if (request.getType().length() > 30) {
          log.printInfo(sericeName, transactionId, className, "error validate type is invalid");
          return true;
        }
      }
      if (request.getType().equalsIgnoreCase("CL")) {
        log.printDebug(className, "invalid Type Check type and policy must match");
        return true;
      }
      String type = request.getType().toUpperCase();
      String[] typeList = type.split("-");
      if (typeList.length > 1) {
        if (typeList[1].split("_")[0].equals("CL")) {
          log.printDebug(className, "invalid Check type and policy must match");
          return true;
        }
      }
    }
    return false;
  }

  private BigDecimal doubleToBigDecimal(double d) {
    return BigDecimal.valueOf(d).setScale(2, RoundingMode.HALF_EVEN);
  }

  @Override
  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }
}
