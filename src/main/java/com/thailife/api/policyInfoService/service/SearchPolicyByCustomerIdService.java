package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.PolicyM;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByCustomerIdResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

import java.util.List;

public interface SearchPolicyByCustomerIdService {
    ServiceResults<SearchPolicyByCustomerIdResponse> getResponse(SearchPolicyByCustomerIdRequest request, String transactionId) throws Exception;
    List<PolicyM> getPolicyNo(String requestToEnquiryParty, String tranId) throws Exception;
}
