package com.thailife.api.policyInfoService.service;

import java.util.List;
import com.thailife.api.policyInfoService.model.request.BeneficiaryInfoRequest;
import com.thailife.api.policyInfoService.model.response.BeneficiaryInfoResponse;

public interface BeneficiaryInfoService {

  List<BeneficiaryInfoResponse> getResponse(BeneficiaryInfoRequest req, String tran_id);

}
