package com.thailife.api.policyInfoService.service.impl;

import manit.M;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.thailife.api.policyInfoService.dao.IndmastDao;
import com.thailife.api.policyInfoService.dao.NameDao;
import com.thailife.api.policyInfoService.dao.OrdmastDao;
import com.thailife.api.policyInfoService.dao.PersonDao;
import com.thailife.api.policyInfoService.dao.UniversallifeDao;
import com.thailife.api.policyInfoService.dao.WhlmastDao;
import com.thailife.api.policyInfoService.dao.impl.IndmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.NameDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.OrdmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.PersonDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UniversallifeDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.WhlmastDaoImpl;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.response.PolicyDetailOLResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PolicyDetailOLService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.api.policyInfoService.util.StatusDisplay;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import insure.Insure;
import insure.PlanSpec;
import insure.PlanType;
import insure.TLPlan;
import utility.support.StatusPolicy;
// @Log4j
@Service
public class PolicyDetailOLServiceImpl implements PolicyDetailOLService {
  private static final Log log = new Log();
  private static final String className = "PolicyDetailOLController";
  private static final String Service = "PolicyDetailsOL";

  @Autowired OrdmastDaoImpl ordmastDaoImpl;
  @Autowired IndmastDaoImpl indmastDaoImpl;
  @Autowired WhlmastDaoImpl whlmastDaoImpl;
  @Autowired UniversallifeDaoImpl universallifeDaoImpl;
  @Autowired NameDaoImpl nameDaoImpl;
  @Autowired PersonDaoImpl personDaoImpl;

  @Override
  public ServiceResults<PolicyDetailOLResponse> getResponse(
      PolicyDetailRequest req, String transactionId) {
    WebServiceMsg wServiceMsg = new WebServiceMsg() {};
    ServiceResults<PolicyDetailOLResponse> response = new ServiceResults<>();
    try {
      PolicyDetailOLResponse policyDetailsResponse =
          getPolicyDetailOL(req.getPolicyno(), req.getType(), transactionId);
      if (policyDetailsResponse != null) {
        response.setResponseCode(wServiceMsg.getMessageByCode("200")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("200")[1]);
        response.setSuccess(true);
        response.setResult(policyDetailsResponse);
      }else {
        response.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        response.setSuccess(true);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(
          Service, transactionId, className, "getResponse Error Exception : " + e.getMessage());
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setSuccess(false);
    }
    return response;
  }

  public PolicyDetailOLResponse getPolicyDetailOL(String policyNo, String type, String tranId)
      throws Exception {
    PolicyDetailOLResponse policyDetailsResponse = new PolicyDetailOLResponse();
    try {
      StatusDisplay statusDisplay = new StatusDisplay();
      JSONObject policyjson = getPolicyData(policyNo, type, tranId);
      if (policyjson == null) {
        return null;
      }
      String plancode = policyjson.getString("plancode");
      String nameid = policyjson.getString("nameid");

      NameDao name = nameDaoImpl.findByNameid(nameid);
      PersonDao person = personDaoImpl.findByPersonid(name.getPersonid());
      TLPlan tlplan = PlanSpec.getPlan(plancode);

      int sum = policyjson.has("sum") ? policyjson.getInt("sum") : policyjson.getInt("rppsum");
      int insuredage = policyjson.getInt("insuredage");
      String mode = policyjson.has("mode") ? policyjson.getString("mode") : null;
      String payperiod = policyjson.getString("payperiod");
      String yearPeriod =
          payperiod != null && payperiod.trim().length() > 2 ? payperiod.substring(0, 2) : "00";

      mode = utility.support.StatusInfo.modeStatus(mode);
      String status1 = policyjson.getString("policystatus1");
      String status2 = policyjson.getString("policystatus2");
      String oldstatus1 = policyjson.getString("oldpolicystatus1");
      String oldstatus2 = policyjson.getString("oldpolicystatus2");
      String statusDate1 =
          policyjson.has("policystatusdate1")
              ? policyjson.getString("policystatusdate1")
              : "00000000";

      String statusDate2 =
          policyjson.has("policystatusdate2")
              ? policyjson.getString("policystatusdate2")
              : "00000000";
      String oldStatusDate1 =
          policyjson.has("oldpolicystatusdate1")
              ? policyjson.getString("oldpolicystatusdate1")
              : "00000000";

      String oldStatusDate2 =
          policyjson.has("oldpolicystatusdate2")
              ? policyjson.getString("oldpolicystatusdate2")
              : "00000000";

      String[] arrstatus =
          PolicyUtil.getStatusForShowCustomer(
              status1,
              statusDate1,
              status2,
              statusDate2,
              oldstatus1,
              oldStatusDate1,
              oldstatus2,
              oldStatusDate2);
      String policystatus1 = arrstatus[0];
      String policystatus2 = arrstatus[2];
      String effectivedate = policyjson.getString("effectivedate");

      String policystatus1desc = StatusPolicy.statusDesc(policystatus1);
      policystatus1desc = policystatus1desc == null ? "" : policystatus1desc;
      String policystatus2desc = StatusPolicy.statusDesc(policystatus2);
      policystatus2desc = policystatus2desc == null ? "" : policystatus2desc;

      String remark = "";
      if (status1 != null && status2 != null) {
        // เพื่อแสดง remark ตามแบบ core โดยเพิ่ม planCode เข้าไป
        remark =
            StatusPolicy.fullStatusDesc(
                arrstatus[0], arrstatus[1], arrstatus[2], arrstatus[3], plancode);
      }

      String insureYear = tlplan.endowmentYear(insuredage + "");
      String maturedate =
          Insure.matureDate(effectivedate, insureYear, plancode, person.getBirthdate());
      String endownmentyear = tlplan.endowmentYear(insuredage + "");
      int topuppremium = PlanType.planType(plancode) == 'U' ? policyjson.getInt("topuppremium") : 0;
      int lifepremium =
          PlanType.planType(plancode) == 'U'
              ? policyjson.getInt("regularpremium")
              : policyjson.has("lifepremium")
                  ? policyjson.getInt("lifepremium")
                  : policyjson.getInt("rpppremium");

      if (PlanType.isPAPlan(plancode) || PlanType.isYRT(plancode) || PlanType.isDMPAPlan(plancode)) {
        effectivedate = PolicyUtil.effectiveData(effectivedate, yearPeriod);
        effectivedate = (!M.dateok(effectivedate) ? M.nextdate(effectivedate, -1) : effectivedate);
        maturedate = Insure.matureDate(effectivedate, insureYear, plancode, person.getBirthdate());
      }

      policyDetailsResponse.setEffectivedate(effectivedate);
      policyDetailsResponse.setEndownmentyear(endownmentyear);
      policyDetailsResponse.setFirstname(name.getFirstname());
      policyDetailsResponse.setInsureage(insuredage);
      policyDetailsResponse.setLastname(name.getLastname());
      policyDetailsResponse.setLifepremium(lifepremium);
      policyDetailsResponse.setMaturedate(maturedate);
      policyDetailsResponse.setMode(mode);
      policyDetailsResponse.setPlancode(plancode);
      policyDetailsResponse.setPlanname(tlplan.name().replace("ตะกาฟุล(", "ตะกาฟุล ("));
      policyDetailsResponse.setPolicystatus1(policystatus1);
      policyDetailsResponse.setPolicystatus1desc(policystatus1desc);
      policyDetailsResponse.setPolicystatus2(policystatus2);
      policyDetailsResponse.setPolicystatus2desc(policystatus2desc);
      policyDetailsResponse.setPrename(utility.prename.Prename.getAbb(name.getPrename()));
      policyDetailsResponse.setRemark(remark);
      policyDetailsResponse.setSex(person.getSex());
      policyDetailsResponse.setStatusdisplay(
          statusDisplay.getStatusDisplay(
              type,
              policystatus1,
              policyjson.has("policystatusdate2")
                  ? policyjson.getString("policystatusdate2")
                  : ""));
      policyDetailsResponse.setSum(sum);
      policyDetailsResponse.setTopuppremium(topuppremium);
      policyDetailsResponse.setType(type);
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(
          Service, tranId, className, "getPolicyDetailOL Error Exception : " + e.getMessage());
      throw e;
    }
    return policyDetailsResponse;
  }

  private JSONObject getPolicyData(String policyno, String type, String tranId) {
    try {
      String table = ProductType.typeGetTable(type);
      table = table == null ? "" : table;
      System.out.println("table==" + table);
      ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
      switch (table) {
        case "ordmast":
          OrdmastDao ordmast = ordmastDaoImpl.findByPolicyno(policyno);
          if (ordmast != null) {
            String json = ow.writeValueAsString(ordmast);
            return new JSONObject(json);
          }
          break;

        case "indmast":
          IndmastDao indmast = indmastDaoImpl.findByPolicyno(policyno);
          if (indmast != null) {
            JSONObject json = new JSONObject(ow.writeValueAsString(indmast));
            json.put("mode", "0");
            return json;
          }
          break;

        case "whlmast":
          WhlmastDao whlmast = whlmastDaoImpl.findByPolicyno(policyno);
          if (whlmast != null) {
            return new JSONObject(ow.writeValueAsString(whlmast));
          }
          break;

        case "universallife":
          UniversallifeDao universallife = universallifeDaoImpl.findByPolicyno(policyno);
          if (universallife != null) {
            return new JSONObject(ow.writeValueAsString(universallife));
          }
          break;
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(
          Service, tranId, className, "getPolicyData Error Exception : " + e.getMessage());
    }
    return null;
  }
}
