package com.thailife.api.policyInfoService.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.thailife.api.policyInfoService.controller.pol14.SearchConsentController;
import com.thailife.log.ExtLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.dao.PartyDao;
import com.thailife.api.policyInfoService.dao.impl.PersonDaoImpl;
import com.thailife.api.policyInfoService.model.CustomerM;
import com.thailife.api.policyInfoService.model.PartyPersonM;
import com.thailife.api.policyInfoService.model.request.SearchConsentRequest;
import com.thailife.api.policyInfoService.model.response.PolicyM;
import com.thailife.api.policyInfoService.model.response.SearchConsentResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SearchConsentService;
import com.thailife.api.policyInfoService.util.CallWS;
import com.thailife.api.policyInfoService.util.CallWithToken;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import lombok.extern.log4j.Log4j;
import com.thailife.api.policyInfoService.dao.impl.PartyDaoImpl;
@Service

public class SearchConsentServiceImpl implements SearchConsentService {
  private final ExtLogger log = ExtLogger.create(SearchConsentController.class);
  private final String className = "SearchConsentServiceImpl";
  private final String Service = "SearchConsent";
  private String tranId;
  
  @Autowired PersonDaoImpl personDaoImpl;
  @Autowired PartyDaoImpl partyDaoImpl;
  @Autowired CallWS callWS;
  
  @Override
  public ServiceResults<SearchConsentResponse> getResponse(SearchConsentRequest req , String tran_Id) {
    ServiceResults<SearchConsentResponse> response = new ServiceResults<>();
    SearchConsentResponse result = null;
    WebServiceMsg wServiceMsg = new WebServiceMsg() {};
    String partyId = req.getCustomer_id();
    tranId = tran_Id;
    try {
      PartyDao party = partyDaoImpl.findByPartyid(Integer.parseInt(partyId));
      if(party == null) {
        response.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        response.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        response.setSuccess(true);
        return response;
      }
      PartyPersonM person = personDaoImpl.findByPartyid(Integer.parseInt(partyId));
      if(person == null) {
        response.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        response.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        response.setSuccess(true);
        return response;
      }
      String govtId = party.getGovt_id();
      String birthDate = person.getBirth_date().toString().replaceAll("-", "");
      String context = "/ConsentWs/rest/consent/search/";
      String request = getRequest(govtId, birthDate);
      log.info(Service, tranId, className, "call ws : " + context);
      log.info(Service, tranId, className, "Req : " + request);
      String resp = CallWithToken.Consent(context, request);
      log.info(Service, tranId, className, "Resp : " + resp);
      JSONObject json = new JSONObject(resp);
      String status = (String) json.get("status");
      if (("0").equals(status)) {
        result = new SearchConsentResponse();
        JSONObject data = json.getJSONObject("data");
        
        result.setPolicy(setListPolicy(data));
        result.setCustomer(setCustomer(data, govtId));
      }
      
      response.setSuccess(true);
      response.setResult(result);
    }
    catch (Exception e) {
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setSuccess(false);
    }
    return response;
  }
  
  private CustomerM setCustomer(JSONObject data, String govtId) {
    CustomerM customerM = new CustomerM();
    JSONObject customer = data.getJSONObject("customer");
    if(null != customer) {
        customerM.setPreName(customer.getString("preNameText"));
        customerM.setFirstName(customer.getString("firstName"));
        customerM.setLastName(customer.getString("lastName"));
        customerM.setGov_id(govtId);
    }
    return customerM;
  }
  
  private List<PolicyM> setListPolicy(JSONObject data){
    JSONArray policys = data.getJSONArray("policy");
    List<PolicyM> listPolicy = new ArrayList<>();
    for (int i = 0; i < policys.length(); i++) {
        JSONObject policy = (JSONObject) policys.get(i);
        PolicyM policyM = new PolicyM();
        policyM.setPolicyNo(policy.getString("policyNo"));
        policyM.setCertNo(policy.getString("certNo"));
        policyM.setPolicyType(policy.getString("policyType"));
        policyM.setFlag(policy.getString("flag"));
        policyM.setPlanName(policy.getString("planName").replace("ตะกาฟุล(" , "ตะกาฟุล ("));
        listPolicy.add(policyM);
    }
    return listPolicy;
  }
  
  
  private static String getRequest(String refId, String bdate) {
    String request = "{\"referenceID\":\"" + refId + "\", \"birthDate\":\"" + bdate + "\"}";
    return request;
  }

}
