package com.thailife.api.policyInfoService.service.impl;

import com.thailife.log.ExtLogger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.thailife.api.policyInfoService.dao.NameDao;
import com.thailife.api.policyInfoService.dao.PersonDao;
import com.thailife.api.policyInfoService.dao.SubunitlinkDao;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.dao.impl.NameDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.PersonDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.SubunitlinkDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.UnitLinkDaoImpl;
import com.thailife.api.policyInfoService.model.SubunitlinkPK;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.response.PolicyDetailsUnitlinkResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PolicyDetailsUnitlinkService;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.StatusDisplay;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import insure.Insure;
import insure.PlanSpec;
import insure.TLPlan;
import utility.support.StatusPolicy;

@Service
public class PolicyDetailsUnitlinkServiceImpl implements PolicyDetailsUnitlinkService {
  private static final ExtLogger log = ExtLogger.create(PolicyDetailsUnitlinkServiceImpl.class);
  private final String className = "PolicyDetailsUnitlinkServiceImpl";
  private final String Service = "PolicyDetailsUnitLink";

  @Autowired UnitLinkDaoImpl unitlinkDaoImpl;
  @Autowired NameDaoImpl nameDaoImpl;
  @Autowired PersonDaoImpl personDaoImpl;
  @Autowired SubunitlinkDaoImpl subunitlinkDaoImpl;

  @Override
  public ServiceResults<PolicyDetailsUnitlinkResponse> getResponse(
      PolicyDetailRequest req, String tran_Id) {
    ServiceResults<PolicyDetailsUnitlinkResponse> response = new ServiceResults<>();
    String policyno = req.getPolicyno();
    String type = req.getType();
    WebServiceMsg wServiceMsg = new WebServiceMsg() {};

    try {
      JSONObject policyjson = getPolicyData(policyno, tran_Id);

      log.info(
          PolicyUtil.pattenLog(
              tran_Id,
              String.format("%s:%s | getPolicyJson:  %s", Service, className, policyjson)));

      if (policyjson == null) {
        response.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        response.setSuccess(false);
        return response;
      }

      PolicyDetailsUnitlinkResponse PolicyDetailsUnitlink =
          getPolicyDetailsUnitlink(policyjson, policyno, type, tran_Id);
      if (PolicyDetailsUnitlink != null) {
        response.setResponseCode(wServiceMsg.getMessageByCode("200")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("200")[1]);
        response.setSuccess(true);
        response.setResult(PolicyDetailsUnitlink);
      } else {
        throw new Exception("PolicyDetailsUnitlink is null ");
      }

    } catch (Exception e) {
      log.error(
          PolicyUtil.pattenLog(
              tran_Id,
              String.format(
                  "%s:%s | getResponse Error Exception: %s", Service, className, e.getMessage())));
      e.printStackTrace();
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setSuccess(false);
    }
    return response;
  }

  private PolicyDetailsUnitlinkResponse getPolicyDetailsUnitlink(
      JSONObject policyJson, String policyNo, String type, String tranId) {
    PolicyDetailsUnitlinkResponse policyDetailsResponse = new PolicyDetailsUnitlinkResponse();
    try {
      StatusDisplay statusDisplay = new StatusDisplay();

      String plancode = policyJson.getString("plancode");
      String nameid = policyJson.getString("nameid");

      NameDao name = nameDaoImpl.findByNameid(nameid);
      PersonDao person = personDaoImpl.findByPersonid(name.getPersonid());
      SubunitlinkDao subunitlink = subunitlinkDaoImpl.findById(new SubunitlinkPK(policyNo, "RSP"));
      TLPlan tlplan = PlanSpec.getPlan(plancode);

      String sum =
          policyJson.has("sum") ? policyJson.getInt("sum") + "" : policyJson.getInt("rppsum") + "";
      int insuredage = policyJson.getInt("insuredage");
      String mode = policyJson.has("mode") ? policyJson.getString("mode") : null;
      mode = utility.support.StatusInfo.modeStatus(mode);
      String status1 = policyJson.getString("policystatus1").trim();
      String status2 = policyJson.getString("policystatus2").trim();
      String oldstatus1 = policyJson.getString("oldpolicystatus1").trim();
      String oldstatus2 = policyJson.getString("oldpolicystatus2").trim();
      String[] arrstatus =
          PolicyUtil.getStatusForShowCustomer(
              status1, "", status2, "", oldstatus1, "", oldstatus2, "");
      String policystatus1 = arrstatus[0];
      String policystatus2 = arrstatus[2];
      String effectivedate = policyJson.getString("effectivedate");
      String insureYear = tlplan.endowmentYear(insuredage + "");
      String maturedate =
          Insure.matureDate(effectivedate, insureYear, plancode, person.getBirthdate());
      String endownmentyear = tlplan.endowmentYear(insuredage + "");
      int rpppremium = policyJson.getInt("rpppremium");
      int rppsum = policyJson.getInt("rppsum");
      String policystatus1desc =
          StatusPolicy.statusDesc(
              policystatus1); // statusDisplay.getPolicyDesc(type, policystatus1);
      policystatus1desc = policystatus1desc == null ? "" : policystatus1desc;
      String policystatus2desc =
          StatusPolicy.statusDesc(
              policystatus2); // statusDisplay.getPolicyDesc(type, policystatus2);
      policystatus2desc = policystatus2desc == null ? "" : policystatus2desc;
      String remark = policystatus1desc + policystatus2desc;
      int rspsum = subunitlink == null ? 0 : subunitlink.getSum().intValue();
      int rsppremium = subunitlink == null ? 0 : subunitlink.getPremium().intValue();
      String rspstatus = subunitlink == null ? "" : subunitlink.getPolicystatus1();
      String rspstatusdesc = subunitlink == null ? "" : StatusPolicy.statusDesc(rspstatus);

      policyDetailsResponse.setPrename(utility.prename.Prename.getAbb(name.getPrename()));
      policyDetailsResponse.setFirstname(name.getFirstname());
      policyDetailsResponse.setLastname(name.getLastname());
      policyDetailsResponse.setSex(person.getSex());
      policyDetailsResponse.setType(type);
      policyDetailsResponse.setRppsum(rppsum);
      policyDetailsResponse.setRpppremium(rpppremium);
      policyDetailsResponse.setRspsum(rspsum);
      policyDetailsResponse.setRsppremium(rsppremium);
      policyDetailsResponse.setInsureage(insuredage);
      policyDetailsResponse.setEffectivedate(effectivedate);
      policyDetailsResponse.setMaturedate(maturedate);
      policyDetailsResponse.setPlancode(plancode);
      policyDetailsResponse.setPlanname(tlplan.name().replace("ตะกาฟุล(", "ตะกาฟุล ("));
      policyDetailsResponse.setSum(sum);
      policyDetailsResponse.setMode(mode);
      policyDetailsResponse.setPolicystatus1(policystatus1);
      policyDetailsResponse.setPolicystatus1desc(policystatus1desc);
      policyDetailsResponse.setPolicystatus2(policystatus2);
      policyDetailsResponse.setPolicystatus2desc(policystatus2desc);
      policyDetailsResponse.setRemark(remark);
      policyDetailsResponse.setStatusdisplay(
          statusDisplay.getStatusDisplay(
              type,
              policystatus1,
              policyJson.has("policystatusdate2")
                  ? policyJson.getString("policystatusdate2")
                  : ""));
      policyDetailsResponse.setEndownmentyear(endownmentyear);
      policyDetailsResponse.setRspstatus(rspstatus);
      policyDetailsResponse.setRspstatusdesc(rspstatusdesc);
    } catch (Exception e) {
      e.printStackTrace();
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | getPolicyDetailsUnitlink Error Exception: %s",
                  Service, className, e.getMessage())));
      return null;
    }
    return policyDetailsResponse;
  }

  private JSONObject getPolicyData(String policyno, String tranId) {
    try {
      UnitLinkDao unitlink = unitlinkDaoImpl.findByPolicyno(policyno);
      ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
      if (unitlink != null) {
        return new JSONObject(ow.writeValueAsString(unitlink));
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format(
                  "%s:%s | getPolicyData Error Exception: %s",
                  Service, className, e.getMessage())));
    }

    return null;
  }
}
