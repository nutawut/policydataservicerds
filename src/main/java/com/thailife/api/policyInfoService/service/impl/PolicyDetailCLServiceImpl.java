package com.thailife.api.policyInfoService.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.dao.CreditmortgageDao;
import com.thailife.api.policyInfoService.dao.DetcertDao;
import com.thailife.api.policyInfoService.dao.MortbranchDao;
import com.thailife.api.policyInfoService.dao.MortpolicyDao;
import com.thailife.api.policyInfoService.dao.NameDao;
import com.thailife.api.policyInfoService.dao.PersonDao;
import com.thailife.api.policyInfoService.dao.impl.CertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.CreditmortgageDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.DetcertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.MortbranchDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.MortpolicyDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.NameDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.PersonDaoImpl;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.response.PolicyDetailCLResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PolicyDetailCLService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.StatusDisplay;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import insure.PlanType;
import manit.M;
import utility.grouplife.mortgage.Data;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;
import utility.underwrite.BankAssure;

@Service
public class PolicyDetailCLServiceImpl implements PolicyDetailCLService {
  private static final Log log = new Log();
  private static final String className = "PolicyDetailCLServiceImpl";
  private static final String Service = "PolicyDetailsCL";

  @Autowired CertDaoImpl certDaoImpl;
  @Autowired PersonDaoImpl personDaoImpl;
  @Autowired MortpolicyDaoImpl mortpolicyDaoImpl;
  @Autowired CreditmortgageDaoImpl creditmortgageDaoImpl;
  @Autowired NameDaoImpl nameDaoImpl;
  @Autowired DetcertDaoImpl detcertDaoImpl;
  @Autowired MortbranchDaoImpl mortbranchDaoImpl;

  @Override
  public ServiceResults<PolicyDetailCLResponse> getResponse(
      PolicyDetailRequest req, String transactionId) {
    ServiceResults<PolicyDetailCLResponse> response = new ServiceResults<>();
    WebServiceMsg wServiceMsg = new WebServiceMsg() {};
    try {

      PolicyDetailCLResponse policyDetail =
          getPolicyDetailCL(req.getPolicyno(), req.getCertno(), req.getType(), transactionId);
      if (policyDetail != null) {
        response.setResponseCode(wServiceMsg.getMessageByCode("200")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("200")[1]);
        response.setSuccess(true);
        response.setResult(policyDetail);
      } else {
        response.setResponseCode(wServiceMsg.getMessageByCode("204")[0]);
        response.setResponseDescription(wServiceMsg.getMessageByCode("204")[1]);
        response.setSuccess(true);
      }

    } catch (Exception e) {
      response.setResponseDescription(wServiceMsg.getMessageByCode("500")[1]);
      response.setResponseCode(wServiceMsg.getMessageByCode("500")[0]);
      response.setSuccess(false);
      e.printStackTrace();
      log.printError(Service, transactionId, className, "Error Exception" + e.getMessage());
    }
    return response;
  }

  public PolicyDetailCLResponse getPolicyDetailCL(
      String policyNo, String certNo, String type, String tranId) {
    PolicyDetailCLResponse PolicyDetail = new PolicyDetailCLResponse();
    try {
      StatusDisplay statusDisplay = new StatusDisplay();
      CertDao cert = certDaoImpl.findByPolicynoAndCertno(policyNo, certNo);
      if (cert == null) {
        throw new PolicyException("cert not found");
      }
      String planCode = policyNo;
      String nameId = cert.getNameid();
      NameDao name = nameDaoImpl.findByNameid(nameId);
      PersonDao person = personDaoImpl.findByPersonid(name.getPersonid());
      MortpolicyDao mortpolicy = mortpolicyDaoImpl.findByPolicyno(policyNo);
      CreditmortgageDao creditmortgage = creditmortgageDaoImpl.findByCert(certNo);

      String planName = getPlanName(planCode);
      planName = "".equals(planName) ? mortpolicy.getPlanname() : planName;
      String sum = cert.getLifesum().toString();
      int insuredAge = cert.getAge();
      String mode = cert.getMode().trim().equals("") ? "9" : cert.getMode();
      mode = utility.support.StatusInfo.modeStatus(mode);
      String effectivedate = cert.getEffectivedate();
      String maturedate = cert.getMaturedate();
      String status1 = cert.getStatcer().trim();
      String status2 = cert.getStatcer2().trim();
      String oldstatus1 = cert.getOldstatcert1().trim();
      String oldstatus2 = cert.getOldstatcert2().trim();
      String[] arrstatus =
          PolicyUtil.getStatusForShowCustomer(
              status1, "", status2, "", oldstatus1, "", oldstatus2, "");
      String statcer = arrstatus[0];
      String statcerdesc = getStatCer(statcer);
      //      statcerdesc = statcerdesc == null ? "" : statcerdesc;
      String statcer2 = arrstatus[2];
      String statcer2desc = getStatCer2(statcer2);
      //      statcer2desc = statcer2desc == null ? "" : statcer2desc;
      String remark = statcerdesc + statcer2desc;
      String endownmentyear =
          cert.getPeriod().equals("00") || cert.getPeriod().equals(" ")
              ? DateInfo.calyymmdd(effectivedate, maturedate).substring(0, 2)
              : cert.getPeriod();
      if (PlanType.isConsumerFinanceMRTAPlan(planCode)) endownmentyear = "01";
      String policyholder = mortpolicy.getName();
      String tlbranch = BankAssure.tlBranchFromPlan(policyNo);

      if (BankAssure.TLBRANCH_COOP.equals(tlbranch)
          || BankAssure.TLBRANCH_COOP_TLP.equals(tlbranch)) {
        DetcertDao detcert = detcertDaoImpl.findByPolicynoAndCertno(policyNo, certNo);
        if (detcert != null) {
          MortbranchDao Mortbranch = mortbranchDaoImpl.findByCode(detcert.getCode());
          policyholder = Mortbranch != null ? Mortbranch.getName() : policyholder;
        }
      }
      String creditcardnumber = creditmortgage == null ? null : creditmortgage.getCreditno();
      creditcardnumber = setFormatCreditCard(creditcardnumber);
      PolicyDetail.setPrename(utility.prename.Prename.getAbb(name.getPrename()));
      PolicyDetail.setFirstname(name.getFirstname());
      PolicyDetail.setLastname(name.getLastname());
      PolicyDetail.setSex(person.getSex());
      PolicyDetail.setPolicyholder(policyholder);
      PolicyDetail.setCreditcardnumber(creditcardnumber);
      PolicyDetail.setType(type);
      PolicyDetail.setInsureage(insuredAge);
      PolicyDetail.setEffectivedate(
          PolicyUtil.calculateEffectiveDateCL(effectivedate, cert.getPayperiod()));
      PolicyDetail.setMaturedate(maturedate);
      PolicyDetail.setPlancode(planCode);
      PolicyDetail.setPlanname(planName.replace("ตะกาฟุล(", "ตะกาฟุล ("));
      PolicyDetail.setSum(sum);
      PolicyDetail.setMode(mode);
      PolicyDetail.setStatcer(statcer);
      PolicyDetail.setStatcerdesc(statcerdesc);
      PolicyDetail.setStatcer2(statcer2);
      PolicyDetail.setStatcer2desc(statcer2desc);
      PolicyDetail.setRemark(remark);
      PolicyDetail.setStatusdisplay(statusDisplay.getStatusDisplay(type, statcer, ""));
      PolicyDetail.setEndownmentyear(
          endownmentyear != null ? M.edits(endownmentyear) : endownmentyear);
    } catch (Exception e) {
      e.printStackTrace();
      log.printError(Service, tranId, className, "Error Exception" + e.getMessage());
      return null;
    }
    return PolicyDetail;
  }

  private String getPlanName(String plancode) {
    PublicRte.setRemote(true);
    String bankCode = BankAssure.bankCodeFromPlan(plancode);
    if (bankCode == null) return "";
    return BankAssure.name(bankCode, plancode).trim();
  }

  private String getStatCer(String status) {
    String[] statCer = Data.statCer;
    for (int i = 1; i < statCer.length; i++) {
      String[] string = statCer[i].split("-");
      if (status.equals(string[0].trim())) return string[1].trim();
    }
    return "";
  }

  private String getStatCer2(String status) {
    String[] statCer = Data.statCer2;
    for (int i = 1; i < statCer.length; i++) {
      String[] string = statCer[i].split("-");
      if (status.equals(string[0].trim())) return string[1].trim();
    }
    return "";
  }

  private String setFormatCreditCard(String creditcardnumber) {
    if (creditcardnumber != null && M.numeric(creditcardnumber) && creditcardnumber.length() == 16)
      return creditcardnumber.substring(0, 4) + "xxxxxxxx" + creditcardnumber.substring(12);
    return creditcardnumber;
  }
}
