//package com.thailife.api.policyInfoService.service.impl;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import com.thailife.api.policyInfoService.controller.pol05.GetRiderDetialController;
//import com.thailife.api.policyInfoService.dao.*;
//import com.thailife.api.policyInfoService.dao.impl.*;
//import com.thailife.api.policyInfoService.dao.impl.unitlink.UliprctrlDaoImpl;
//import com.thailife.api.policyInfoService.model.request.RequestMessages;
//import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
//import com.thailife.api.policyInfoService.model.response.ResponseMessages;
//import com.thailife.api.policyInfoService.util.*;
//import com.thailife.api.policyInfoService.util.newRiderDetail.NewRiderDetailServiceImpl;
//import com.thailife.log.ExtLogger;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import com.thailife.api.policyInfoService.model.RiderM;
//import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
//import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
//import com.thailife.api.policyInfoService.model.response.ServiceResults;
//import com.thailife.api.policyInfoService.service.GetRiderDetialService;
//import insure.RiderType;
//import manit.M;
//import utility.claim.Benefit;
//import utility.support.DateInfo;
//import utility.support.StatusInfo;
//
//@Service
//public class GetRiderDetialServiceImpl implements GetRiderDetialService {
//  private static final String className = "GetRiderDetialServiceImpl";
//  private final ExtLogger log = ExtLogger.create(GetRiderDetialServiceImpl.class);
//
//  @Autowired CertRiderDaoImpl certRiderDaoImpl;
//  @Autowired CertDaoImpl certDaoImpl;
//  @Autowired OrdmastDaoImpl ordmastDaoImpl;
//  @Autowired WhlmastDaoImpl whlmastDaoImpl;
//  @Autowired IndmastDaoImpl indmastDaoImpl;
//  @Autowired UnitLinkDaoImpl unitLinkDaoImpl;
//  @Autowired UniversallifeDaoImpl universallifeDaoImpl;
//  @Autowired RiderDaoImpl riderDao;
//  @Autowired UliprctrlDaoImpl uliprctrlDaoImpl;
//  @Autowired UlipRiderDaoImpl ulipRiderDaoImpl;
//  @Autowired UlipExtraPremuimDaoImpl ulipExtraPremuimDao;
//  @Autowired ExtraPremiumDaoImpl extraPremiumDao;
//  @Autowired UlExtraPremiumDaoImpl ulExtraPremiumDao;
//  List<String> listPolicyStatus = Arrays.asList("A", "E", "R", "U");
//
//  @Override
//  public ResponseMessages<RiderDetialResponse> service(RequestMessages<RiderDetailRequest> request){
//    log.info(className, "GetRiderDetialServiceImpl [START]");
//    ResponseMessages<RiderDetialResponse> response = new ResponseMessages<>(null);
//    try {
//      if (validateRequest(request)) {
//        response.setResponseStatus(PolicyUtil.responseValidateError());
//      } else {
//        response.setResponseRecord(NewRiderDetailServiceImpl.getResponse(request.getRequestRecord()));
//        response.setResponseStatus(PolicyUtil.responseStatusSuccess());
//      }
//    } catch (PolicyException e) {
//      log.error(className, e.getMessage());
//      response.setResponseStatus(PolicyUtil.responseDataNotFound());
//    } catch (Exception e) {
//      log.error(className, e.getMessage());
//      response.setResponseStatus(PolicyUtil.responseInternalServerError());
//    }finally{
//      response.setHeaderData(PolicyUtil.headerDataResponse(request.getHeaderData()));
//      log.info(className, "GetRiderDetialServiceImpl [END]");
//    }
//    return response;
//  }
//
//  private  boolean validateRequest(RequestMessages<RiderDetailRequest> request){
//    if (request == null){
//      log.debug(className, "invalid request is null. ");
//      return true;
//    }else{
//      if (!PolicyUtil.validateHeaderData(request.getHeaderData())){
//        log.debug(className, "invalid headerData. ");
//        return true;
//      }
//      if (validateBody(request.getRequestRecord())){
//        log.debug(className, "invalid requestRecord. ");
//        return true;
//      }
//    }
//    return false;
//  }
//
//  private boolean validateBody(RiderDetailRequest body){
//    if (body == null){
//      log.debug(className,"invalid requestRecord is null. ");
//      return true;
//    }else{
//      if (PolicyUtil.StringIsNullOrEmpty(body.getPolicyno())){
//        log.debug(className,"invalid policyNo is null or empty. ");
//        return true;
//      }else {
//        if (body.getPolicyno().length() > 8){
//          log.debug(className,"invalid policyNo length> 8 ");
//          return true;
//        }
//      }
//      if (PolicyUtil.StringIsNullOrEmpty(body.getCertno()) && body.getPolicyno().length() < 8){
//        log.debug(className,"invalid certNo is null or empty and policyNo length < 8 ");
//        return true;
//      }
//      if (PolicyUtil.StringIsNullOrEmpty(body.getType())){
//        log.debug(className,"invalid type is null or empty ");
//        return true;
//      }
//      if (ProductType.typeGetTable(body.getType()) == null){
//        log.debug(className,"invalid typeGetTable by Type is null or empty ");
//        return true;
//      }
//    }
//    return false;
//  }
//
//  @Override
//  public ServiceResults<RiderDetialResponse> getResponse(PolicyDetailRequest req, String tranId)
//      throws PolicyException {
//    RiderDetialResponse riderDetialResponse = new RiderDetialResponse();
//    ServiceResults<RiderDetialResponse> response = new ServiceResults<>();
//    String policyNo = req.getPolicyno();
//    String certNo = req.getCertno();
//    String type = req.getType();
//    try {
//      StatusDisplay statusDisplay = new StatusDisplay();
//      String tableType = ProductType.typeGetTable(type);
//      JSONArray riderArray = getRiderData(policyNo, certNo, type);
//      List<RiderM> list = new ArrayList<>();
//      String riderGroup;
//      log.debug(className, "DEBUG :: tableType = " + tableType);
//      log.debug(className, "DEBUG :: riderArray = " + riderArray);
//      if (riderArray == null && type.contains("CL")) { // กรณี CL และ ไม่พบ rider
//        CertDao cert = certDaoImpl.findByPolicynoAndCertno(policyNo, certNo);
//        String ridercode;
//        String ridername;
//        String ridersortname;
//        int ridersum;
//        int riderpremium;
//        String riderstatus;
//        String riderstatusdesc;
//        String riderstatusdate;
//        String statusdisplay;
//        String effectivedate;
//        String policyStatus = "";
//        int extrariderpremium;
//        if (cert.getTpdpremium().intValue() > 0) {
//
//          String status =certNoDao(cert);
//
//          //					policyStatus = statusDisplay.statusDisplayCL(cert.getStatcer());
//          policyStatus = statusDisplay.statusDisplayCL(status);
//          ridercode = "TPD";
//          ridername = "ทุพพลภาพถาวรสิ้นเชิง";
//          ridersortname = "TPD";
//          ridersum = cert.getLifesum().intValue();
//          riderpremium = cert.getTpdpremium().intValue();
//          riderstatus =
//              "N"; // (จากโปรแกรมจะ Default เป็น N ตลอด แต่การเคลมต้องดูจากสถานะกรมธรรม์เป็นหลัก)
//          if (listPolicyStatus.contains(cert.getStatcer())) riderstatus = "A";
//          riderstatusdesc = "ปกติ"; // (ไม่มีระบุไว้แต่โดยทั่วไป สถานะ N คือ 'ปกติ'
//          // แต่กรณีนี้ต้องดูสถานะกรมธรรม์ประกอบ)
//          riderstatusdate = cert.getEffectivedate(); // (มีผล N พร้อมกับการเริ่มของสัญญาหลัก)
//          statusdisplay = statusDisplay.getStatusDisplay("RIDER", riderstatus);
//          effectivedate =
//              cert.getEffectivedate(); // CL : ไม่ได้ระบุวันเริ่มมีผลบังคับของสัญญาแนบท้าย
//          // (ใช้วันเดียวกับ master
//          extrariderpremium = cert.getExtratpdpremium().intValue();
//          somethingMethod(list, ridercode, ridername, ridersortname, ridersum, riderpremium, riderstatus, riderstatusdesc, riderstatusdate, statusdisplay, effectivedate, policyStatus, extrariderpremium);
//        }
//        if (cert.getAddpremium().intValue() > 0) {
//          ridercode = "ADD";
//          ridername = "การเสียชีวิต และการสูญเสียอวัยวะจากอุบัติเหตุ";
//          ridersortname = "ADD";
//          ridersum = cert.getAccidentsum().intValue();
//          riderpremium = cert.getAddpremium().intValue();
//          riderstatus =
//              "N"; // (จากโปรแกรมจะ Default เป็น N ตลอด แต่การเคลมต้องดูจากสถานะกรมธรรม์เป็นหลัก)
//          if (listPolicyStatus.contains(cert.getStatcer())) riderstatus = "A";
//          riderstatusdesc = "ปกติ"; // (ไม่มีระบุไว้แต่โดยทั่วไป สถานะ N คือ 'ปกติ'
//          // แต่กรณีนี้ต้องดูสถานะกรมธรรม์ประกอบ)ridergroup
//          riderstatusdate = cert.getEffectivedate(); // (มีผล N พร้อมกับการเริ่มของสัญญาหลัก)
//          statusdisplay = statusDisplay.getStatusDisplay("RIDER", riderstatus);
//          effectivedate =
//              cert.getEffectivedate(); // CL : ไม่ได้ระบุวันเริ่มมีผลบังคับของสัญญาแนบท้าย
//          // (ให้ใช้วันเดียวกับ master)
//          extrariderpremium = 0;
//          somethingMethod(list, ridercode, ridername, ridersortname, ridersum, riderpremium, riderstatus, riderstatusdesc, riderstatusdate, statusdisplay, effectivedate, policyStatus, extrariderpremium);
//        }
//      } else {
//        if (riderArray == null || riderArray.isEmpty()) {
//          throw new PolicyException("riderArray empty");
//        }
//        /*หา policy status*/
//        String policyStatus = "";
//        if (("ordmast").equalsIgnoreCase(tableType)) {
//          OrdmastDao ordmast = ordmastDaoImpl.findByPolicyno(policyNo);
//          if (null != ordmast) {
//            String status1 = ordmast.getPolicystatus1();
//            String status2 = ordmast.getPolicystatus2();
//            String oldstatus1 = ordmast.getOldpolicystatus1();
//            String oldstatus2 = ordmast.getOldpolicystatus2();
//            String[] arrstatus =
//                PolicyUtil.getStatusForShowCustomer(
//                    status1, "", status2, "", oldstatus1, "", oldstatus2, "");
//            String status = arrstatus[0];
//            policyStatus = statusDisplay.statusDisplayOL(status);
//          }
//        } else if (("indmast").equalsIgnoreCase(tableType)) {
//          IndmastDao indmast = indmastDaoImpl.findByPolicyno(policyNo);
//          if (null != indmast) {
//            String status1 = indmast.getPolicystatus1();
//            String status2 = indmast.getPolicystatus2();
//            String oldstatus1 = indmast.getOldpolicystatus1();
//            String oldstatus2 = indmast.getOldpolicystatus2();
//            String[] arrstatus =
//                PolicyUtil.getStatusForShowCustomer(
//                    status1, "", status2, "", oldstatus1, "", oldstatus2, "");
//            String status = arrstatus[0];
//            policyStatus = statusDisplay.statusDisplayOL(status);
//          }
//        } else if (("whlmast").equalsIgnoreCase(tableType)) {
//          WhlmastDao whlmast = whlmastDaoImpl.findByPolicyno(policyNo);
//          if (null != whlmast) {
//            String status1 = whlmast.getPolicystatus1();
//            String status2 = whlmast.getPolicystatus2();
//            String oldstatus1 = whlmast.getOldpolicystatus1();
//            String oldstatus2 = whlmast.getOldpolicystatus2();
//            String[] arrstatus =
//                PolicyUtil.getStatusForShowCustomer(
//                    status1, "", status2, "", oldstatus1, "", oldstatus2, "");
//            String status = arrstatus[0];
//            policyStatus = statusDisplay.statusDisplayOL(status);
//          }
//        } else if (("unitlink").equalsIgnoreCase(tableType)) {
//          UnitLinkDao unitlink = unitLinkDaoImpl.findByPolicyno(policyNo);
//          if (null != unitlink) {
//            String status1 = unitlink.getPolicystatus1();
//            String status2 = unitlink.getPolicystatus2();
//            String oldstatus1 = unitlink.getOldpolicystatus1();
//            String oldstatus2 = unitlink.getOldpolicystatus2();
//            String[] arrstatus =
//                PolicyUtil.getStatusForShowCustomer(
//                    status1, "", status2, "", oldstatus1, "", oldstatus2, "");
//            String status = arrstatus[0];
//            policyStatus = statusDisplay.statusDisplayOL(status);
//          }
//        } else if (("universallife").equalsIgnoreCase(tableType)) {
//          UniversallifeDao universallife = universallifeDaoImpl.findByPolicyno(policyNo);
//          if (null != universallife) {
//            String status1 = universallife.getPolicystatus1();
//            String status2 = universallife.getPolicystatus2();
//            String oldstatus1 = universallife.getOldpolicystatus1();
//            String oldstatus2 = universallife.getOldpolicystatus2();
//            String[] arrstatus =
//                PolicyUtil.getStatusForShowCustomer(
//                    status1, "", status2, "", oldstatus1, "", oldstatus2, "");
//            String status = arrstatus[0];
//            policyStatus = statusDisplay.statusDisplayOL(status);
//          }
//        } else if (("mortgage").equalsIgnoreCase(tableType)) {
//          CertDao cert = certDaoImpl.findByPolicynoAndCertno(policyNo, certNo);
//          if (null != cert) {
//            //						policyStatus = statusDisplay.statusDisplayCL(cert.getStatcer());
//           String status = certNoDao(cert);
//            policyStatus = statusDisplay.statusDisplayOL(status);
//          }
//        }
//
//        for (int i = 0; i < riderArray.length(); i++) {
//          JSONObject riderjson = riderArray.getJSONObject(i);
//          String ridercode = riderjson.getJSONObject("id").getString("ridertype");
//          if (caseIgnore(ridercode)) continue;
//          String ridername = RiderType.getFullName(RiderType.isIslam(ridercode), ridercode);
//          String ridersortname = RiderType.getShortName(ridercode, RiderType.isPARider(ridercode));
//          int ridersum =
//              riderjson.has("sum") ? riderjson.getInt("sum") : riderjson.getInt("ridersum");
//          int riderpremium =
//              riderjson.has("extrapremium")
//                  ? (riderjson.getInt("premium") + riderjson.getInt("extrapremium"))
//                  : riderjson.getInt("riderpremium");
//          String riderstatus = riderjson.getString("riderstatus");
//          riderstatus = checkRiderStatus(tableType, policyNo, riderstatus);
//          String riderstatusdesc =
//              ("ไม่มีผลบังคับ").equals(policyStatus)
//                  ? "ไม่มีผลบังคับ"
//                  : StatusInfo.riderStatus(riderstatus); // ridercode;//
//          String riderstatusdate = riderjson.getString("riderstatusdate");
//          String statusdisplay =
//              ("ไม่มีผลบังคับ").equals(policyStatus)
//                  ? "ไม่มีผลบังคับ"
//                  : statusDisplay.getStatusDisplay("RIDER", riderstatus);
//          String effectivedate =
//              riderjson.has("effectivedate") ? riderjson.getString("effectivedate") : "";
//          int extrariderpremium =
//              riderjson.has("extrapremium")
//                  ? riderjson.getInt("extrapremium")
//                  : getExtraPremium(policyNo, type, ridercode);
//
//          if (ridercode.equals("รพ") && ridername.length() > 5)
//            ridername = ridername.substring(0, ridername.length() - 5);
//          riderGroup = PolicyUtil.getRiderCode(ridercode);
//          RiderM riderM = new RiderM();
//          riderM.setRidercode(ridercode);
//          riderM.setRidername(ridername);
//          riderM.setRidersortname(ridersortname);
//          riderM.setRidersum(getRiderSumByRiderCode(riderGroup, ridercode, ridersum));
//          //					riderM.setRidersum(riderGroup.equals("R03")  ? (int)(Double.parseDouble(new
//          // utility.claim.Benefit().getRbBenefit(ridercode)) ) : ridersum);
//          riderM.setRiderpremium(riderpremium);
//          // riderM.setRiderpremium(riderpremium);
//          riderM.setRiderstatus(riderstatus);
//          riderM.setRiderstatusdesc(riderstatusdesc);
//          riderM.setRiderstatusdate(riderstatusdate);
//          riderM.setStatusdisplay(statusdisplay);
//          riderM.setEffectivedate(effectivedate);
//          riderM.setExtrariderpremium(extrariderpremium);
//          riderM.setLabel_status(getLabel_status(riderGroup, ridername));
//          list.add(riderM);
//        }
//      }
//      riderDetialResponse.setList_of_rider(list);
//    } catch (Exception e) {
//      e.printStackTrace();
//      log.error(className, e.getMessage());
//      throw new PolicyException(
//          "getResponse|policyNo:" + policyNo + " certNo:" + certNo + " type:" + type);
//    }
//    response.setResult(riderDetialResponse);
//    return response;
//  }
//
//  private String certNoDao(CertDao cert) {
//    String status1 = cert.getStatcer();
//    String status2 = cert.getStatcer2();
//    String oldstatus1 = cert.getOldstatcert1();
//    String oldstatus2 = cert.getOldstatcert2();
//    String[] arrstatus =
//        PolicyUtil.getStatusForShowCustomer(
//            status1, "", status2, "", oldstatus1, "", oldstatus2, "");
//    return arrstatus[0];
//  }
//
//  private void somethingMethod(List<RiderM> list, String ridercode, String ridername, String ridersortname, int ridersum, int riderpremium, String riderstatus, String riderstatusdesc, String riderstatusdate, String statusdisplay, String effectivedate, String policyStatus, int extrariderpremium) throws IOException {
//    String ridergroup;
//    ridergroup = PolicyUtil.getRiderCode(ridercode);
//
//    RiderM riderM = new RiderM();
//    riderM.setRidercode(ridercode);
//    riderM.setRidername(ridername);
//    riderM.setRidersortname(ridersortname);
//
//    riderM.setRidersum(getRiderSumByRiderCode(ridergroup, ridercode, ridersum));
//    riderM.setRiderpremium(riderpremium);
//    riderM.setRiderstatus(riderstatus);
//    riderM.setRiderstatusdesc(
//        ("ไม่มีผลบังคับ").equals(policyStatus) ? "ไม่มีผลบังคับ" : riderstatusdesc);
//    riderM.setRiderstatusdate(riderstatusdate);
//    riderM.setStatusdisplay(
//        ("ไม่มีผลบังคับ").equals(policyStatus) ? "ไม่มีผลบังคับ" : statusdisplay);
//    riderM.setEffectivedate(effectivedate);
//    riderM.setExtrariderpremium(extrariderpremium);
//    riderM.setLabel_status(getLabel_status(ridergroup, ridername));
//    list.add(riderM);
//  }
//
//  private String checkRiderStatus(String tableType, String policyNo, String status)
//      throws Exception {
//    if (status.equals("T") || status.equals("A")) return status;
//    String duedate;
//    String policyStatus = "";
//    String gracePeriod = "";
//    if (("ordmast").equalsIgnoreCase(tableType)) {
//      OrdmastDao ordmast = ordmastDaoImpl.findByPolicyno(policyNo);
//      duedate = ordmast.getDuedate();
//      policyStatus = ordmast.getPolicystatus1();
//      gracePeriod = M.nextdate(duedate, 31);
//    } else if (("indmast").equalsIgnoreCase(tableType)) {
//      IndmastDao indmast = indmastDaoImpl.findByPolicyno(policyNo);
//      String payperiod = indmast.getPayperiod();
//      String duedatecore =
//          payperiod.equals("") || payperiod.equals("0")
//              ? "00000000"
//              : DateInfo.nextMonth(payperiod + "01", 1);
//      duedate = duedatecore;
//      policyStatus = indmast.getPolicystatus1();
//      gracePeriod = M.nextdate(duedate, 60);
//    } else if (("whlmast").equalsIgnoreCase(tableType)) {
//      WhlmastDao whlmast = whlmastDaoImpl.findByPolicyno(policyNo);
//      duedate = whlmast.getDuedate();
//      policyStatus = whlmast.getPolicystatus1();
//      gracePeriod = M.nextdate(duedate, 31);
//    }
//    if (policyStatus.equals("F") && M.sysdate().compareTo(gracePeriod) > 0) status = "A";
//    if (listPolicyStatus.contains(policyStatus)) status = "A";
//    return status;
//  }
//
//  private boolean caseIgnore(String ridercode) {
//    List<String> ignore = Arrays.asList("106", "107", "108");
//    return ignore.contains(ridercode);
//  }
//
//  private JSONArray getRiderData(String policyno, String certno, String policytype)
//      throws Exception {
//    List<CertRiderDao> certrider;
//
//    if (PolicyUtil.validateString(policyno) && PolicyUtil.validateString(certno)
//        || policytype.contains("CL")) {
//      if (PolicyUtil.validateString(certno))
//        certrider = certRiderDaoImpl.findByIdPolicynoAndIdCertno(policyno, certno);
//      else certrider = certRiderDaoImpl.findByIdPolicyno(policyno);
//      if (certrider != null && !certrider.isEmpty()) {
//        log.debug(className, "certrider");
//        return new JSONArray(certrider);
//      }
//    } else if (policytype.contains("ULIP")) {
//      List<RiderDao> ulipRider = ulipRiderDaoImpl.findByPolicyNo(policyno);
//      if (ulipRider != null && !ulipRider.isEmpty()) {
//        log.printDebug(className, "ulipRider");
//        return new JSONArray(ulipRider);
//      }
//    } else {
//      List<RiderDao> rider = riderDao.findByPolicyNo(policyno);
//      if (rider != null && !rider.isEmpty()) {
//        System.out.println("rider");
//        return new JSONArray(rider);
//      }
//      List<IndmastDao> indrider = indmastDaoImpl.findByPolicynoList(policyno);
//      if (indrider != null && !indrider.isEmpty()) {
//        System.out.println("indrider");
//        return new JSONArray(indrider);
//      }
//
//      List<UlRiderDao> ulrider = ulipRiderDaoImpl.findByPolicyNoUlRider(policyno);
//      if (ulrider != null && !ulrider.isEmpty()) {
//        System.out.println("ulrider");
//        return new JSONArray(ulrider);
//      }
//    }
//    return null;
//  }
//
//  private int getLabel_status(String ridergroup, String ridername) {
//    int group = 0;
//    if (ridername.contains("ตะกาฟุล") || ridername.contains("หลักประกัน")) group = 3;
//    else if (ridergroup.equals("R03")) group = 1;
//    else if (ridergroup.equals("R04")) group = 2;
//    return group;
//  }
//
//  private int getExtraPremium(String policyno, String policytype, String ridertype)
//      throws Exception {
//    int sum = 0;
//    if (policytype.contains("ULIP")) {
//      List<ExtraPremiumUKDao> ulipExtrapremium =
//          ulipExtraPremuimDao.findByIdPolicyNoAndIdExtraType(policyno, ridertype);
//      for (ExtraPremiumUKDao extrapremium : ulipExtrapremium) {
//        sum = sum + extrapremium.getExtrapremium().intValue();
//      }
//      if (!ulipExtrapremium.isEmpty()) return sum;
//    } else {
//      sum = 0;
//      List<ExtrapremiumDao> extrapremiums =
//          extraPremiumDao.findByIdPolicynoAndIdExtratype(policyno, ridertype);
//      for (ExtrapremiumDao extrapremium : extrapremiums) {
//        sum = sum + extrapremium.getExtrapremium().intValue();
//      }
//      if (!extrapremiums.isEmpty()) return sum;
//
//      sum = 0;
//      List<UlExtrapremuimDao> ulExtrapremiums =
//          ulExtraPremiumDao.findByIdPolicynoAndIdExtratype(policyno, ridertype);
//      for (UlExtrapremuimDao ulextrapremium : ulExtrapremiums) {
//        sum = sum + ulextrapremium.getExtrapremium().intValue();
//      }
//      if (!ulExtrapremiums.isEmpty()) return sum;
//    }
//    return sum;
//  }
//
//  public int getRiderSumByRiderCode(String ridergroup, String ridercode, int riderSum) {
//    if (ridergroup.equals("R03")) {
//      if (Benefit.getHealthCode(ridercode)) {
//        return (int) (Double.parseDouble(Benefit.getRbBenefit(ridercode)));
//      } else if (Benefit.getVpCode(ridercode)) {
//        return (int) (Double.parseDouble(Benefit.getVpIpdRbBenefit(ridercode)));
//      } else if (RiderType.isSP_RefundPlus(ridercode) || RiderType.isSPGoldPlus(ridercode)) {
//        return (int) (Double.parseDouble(RiderType.getRoomExpense(ridercode)));
//      }
//    }
//    return riderSum;
//  }
//}
