package com.thailife.api.policyInfoService.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.util.*;
import com.thailife.log.ExtLogger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.model.request.GetLoanPremumRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetLoanPremumResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.GetLoanPremiumService;
import layout.bean.ws.pos.CalLoanBeanSet;
import layout.bean.ws.pos.CalRequestLoanBean;
import layout.bean.ws.pos.LoanCalcDetailTypeBean;
import manit.M;
import manit.rte.Result;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;

@Service
public class GetLoanPremiumServiceImpl implements GetLoanPremiumService {
  private static final ExtLogger log = ExtLogger.create(GetLoanPremiumServiceImpl.class);

  private final String className = "GetLoanPremiumController";
  private final String Service = "GetLoanPremium";
  private String tranId;
  @Autowired CallWS callWS;

  @Override
  public ResponseMessages<GetLoanPremumResponse> service(
      RequestMessages<GetLoanPremumRequest> request) {
    GetLoanPremumResponse getLoanPremumResponse =
        new GetLoanPremumResponse(); // ฝั่ง get data จาก ที่ยิงมา
    ResponseMessages<GetLoanPremumResponse> response =
        new ResponseMessages<>(getLoanPremumResponse); // นำ model มา
    log.info(className, " service [START] ");
    try {
      // validate request data
      if (validateRequest(request)) {
        response.setResponseStatus(PolicyUtil.responseValidateError());
      } else {
        // validate request data UL
        String type = request.getRequestRecord().getType();
        log.debug(className, "type: " + type);
        if (type.toUpperCase().contains("-UL-")) {
          // "ระบบยังไม่รองรับการคำนวณสิทธ์เงินกู้สำหรับกรมธรรม์ Universal Life"));
          getLoanPremumResponse = getCalculateLoan(request.getRequestRecord().getPolicyno());
          response = new ResponseMessages<>(getLoanPremumResponse);
          if (getLoanPremumResponse != null) {
            response.setResponseStatus(PolicyUtil.responseStatusSuccess());
          } else {
            response.setResponseStatus(PolicyUtil.responseDataNotFound());
          }
        } else {
          String requestStr, policyNo, date;
          String context = "/interfaceCsv/rest/calculate/calculateLoan/1.0";

          Locale lc = new Locale("th", "TH");

          date = new SimpleDateFormat("dd/MM/yyyy", lc).format(new Date());
          policyNo = request.getRequestRecord().getPolicyno();
          requestStr = getRequest(policyNo, date);
          log.debug(className, "callWs: {{ENDPOINT}}" + context +"|\nrequest: " + requestStr);
          String resp = CallWithToken.CSVApplication(context, requestStr);
          log.debug(className, "responseWs: " + resp);
          // set data output
          JSONObject json = new JSONObject(resp);
          JSONObject responseRecord = json.getJSONObject("responseRecord");

          // data not found return 204
          if (responseRecord.isNull("calRequestLoanMs")) {
            response.setResponseStatus(PolicyUtil.responseDataNotFound());
          } else {
            JSONObject calRequestLoanMs =
                responseRecord.getJSONArray("calRequestLoanMs").getJSONObject(0);
            JSONObject loanCalcDetailTypes =
                calRequestLoanMs.getJSONArray("loanCalcDetailTypes").getJSONObject(0);
            getLoanPremumResponse = setLoanPremium(loanCalcDetailTypes);
            long totalAll = Long.parseLong(loanCalcDetailTypes.getString("comLoanAmount"));
            getLoanPremumResponse.setTotalloanall(totalAll);
            getLoanPremumResponse.setComloanamount(
                Long.parseLong(loanCalcDetailTypes.getString("payLoanAmount"))
                    + Long.parseLong(loanCalcDetailTypes.getString("duty")));

            // if payLoanAmount is less than 0 or a minus number change it to 0
            if (Long.parseLong(loanCalcDetailTypes.getString("payLoanAmount")) <= 0) {
              getLoanPremumResponse.setPayloanamount(0);
              if (totalAll > 0) getLoanPremumResponse.setComloanamount(0);
            } else {
              getLoanPremumResponse.setPayloanamount(
                  Long.parseLong(loanCalcDetailTypes.getString("payLoanAmount")));
            }
            response.setResponseRecord(getLoanPremumResponse);
            response.setResponseStatus(PolicyUtil.responseStatusSuccess());
          }
        }
      }
    } catch (PolicyException e) {
      e.printStackTrace();
      log.error(className, e.getMessage());
      response.setResponseStatus(PolicyUtil.responseDataNotFound());
    } catch (Exception e) {
      e.printStackTrace();
      log.error(className, e.getMessage());
      response.setResponseStatus(PolicyUtil.responseInternalServerError());
    } finally {
      response.setHeaderData(PolicyUtil.headerDataResponse(request.getHeaderData()));
      log.info(className, " service [END].");
    }
    return response;
  }

  private boolean validateRequest(RequestMessages<GetLoanPremumRequest> req) {
    if (req == null) {
      log.debug(className, " invalid request is null.");
      return true;
    } else {
      if (!PolicyUtil.validateHeaderData(req.getHeaderData())) {
        log.debug(className, " invalid headerDate. ");
        return true;
      }
      return validateBody(req.getRequestRecord());
    }
  }

  private boolean validateBody(GetLoanPremumRequest request) {
    if (request == null) {
      log.debug(className, " invalid request is null ");
      return true;
    } else {
      if (PolicyUtil.StringIsNullOrEmpty(request.getPolicyno())) {
        log.debug(className, " invalid policyNo is empty or null ");
        return true;
      } else {
        if (request.getPolicyno().length() > 8) {
          log.debug(className, " invalid policyNo length more than 8");
          return true;
        }
      }
      if (request.getType() == null) {
        log.debug(className, " invalid type is null");
        return true;
      } else {
        if (request.getType().length() > 30) {
          log.debug(className, " invalid type length more than 30");
          return true;
        }
      }
    }
    return false;
  }

  public ServiceResults<GetLoanPremumResponse> getResponse(
      RequestMessages<GetLoanPremumRequest> req) {
    ServiceResults<GetLoanPremumResponse> response = new ServiceResults<>();
    GetLoanPremumResponse loanPremium;
    WebServiceMsg wMessage = new WebServiceMsg();
    String policyNo = req.getRequestRecord().getPolicyno();
    tranId = req.getHeaderData().getMessageId();
    try {

      if (req.getRequestRecord().getType().toUpperCase().contains("-UL-")) {
        loanPremium = getCalculateLoan(policyNo);
        if (loanPremium != null) {
          response.setResult(loanPremium);
          response.setResponseCode(wMessage.getMessageByCode("200")[0]);
          response.setResponseDescription(wMessage.getMessageByCode("200")[1]);
        } else {
          response.setResult(null);
          response.setResponseCode(wMessage.getMessageByCode("204")[0]);
          response.setResponseDescription(wMessage.getMessageByCode("204")[1]);
        }
        response.setSuccess(true);
        return response;
      }

      response.setResult(getLoanPremium(req.getRequestRecord()));
      response.setResponseCode(wMessage.getMessageByCode("200")[0]);
      response.setResponseDescription(wMessage.getMessageByCode("200")[1]);
      response.setSuccess(true);
    } catch (Exception e) {
      response.setResponseCode(wMessage.getMessageByCode("500")[0]);
      response.setResponseDescription(wMessage.getMessageByCode("500")[1]);
      response.setSuccess(false);
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
    }
    return response;
  }

  private GetLoanPremumResponse getLoanPremium(GetLoanPremumRequest req) throws Exception {
    GetLoanPremumResponse loanPremium;

    Locale lc = new Locale("th", "TH");
    String policyNo = req.getPolicyno();
    String date = new SimpleDateFormat("dd/MM/yyyy", lc).format(new Date());
    String context = "/interfaceCsv/rest/calculate/calculateLoan/1.0";
    String request = getRequest(policyNo, date);
    String resp = CallWithToken.CSVApplication(context, request);
    // set data output
    JSONObject json = new JSONObject(resp);
    JSONObject responseRecord = json.getJSONObject("responseRecord");

    // data not found return
    if (responseRecord.isNull("calRequestLoanMs")) {
      return null;
    }
    JSONObject calRequestLoanMs = responseRecord.getJSONArray("calRequestLoanMs").getJSONObject(0);
    JSONObject loanCalcDetailTypes =
        calRequestLoanMs.getJSONArray("loanCalcDetailTypes").getJSONObject(0);
    loanPremium = setLoanPremium(loanCalcDetailTypes);
    loanPremium.setComloanamount(
        Long.parseLong(loanCalcDetailTypes.getString("payLoanAmount"))
            + Long.parseLong(loanCalcDetailTypes.getString("duty")));

    long totalAll = Long.parseLong(loanCalcDetailTypes.getString("comLoanAmount"));
    loanPremium.setTotalloanall(totalAll);

    if (Long.parseLong(loanCalcDetailTypes.getString("payLoanAmount")) <= 0) {
      loanPremium.setPayloanamount(0);
      if (totalAll > 0) loanPremium.setComloanamount(0);
    } else {
      loanPremium.setPayloanamount(Long.parseLong(loanCalcDetailTypes.getString("payLoanAmount")));
    }

    return loanPremium;
  }

  private GetLoanPremumResponse setLoanPremium(JSONObject loanCalcDetailTypes) {
    GetLoanPremumResponse loanPremium = new GetLoanPremumResponse();
    loanPremium.setLoandate(loanCalcDetailTypes.getString("loanDate"));
    loanPremium.setLoanamount(Long.parseLong(loanCalcDetailTypes.getString("loanAmount")));
    loanPremium.setIntfromdate(loanCalcDetailTypes.getString("intFromDate"));
    loanPremium.setInttodate(loanCalcDetailTypes.getString("intToDate"));
    loanPremium.setIntstatus(loanCalcDetailTypes.getString("intStatus"));
    loanPremium.setInterest(Long.parseLong(loanCalcDetailTypes.getString("interest")));
    loanPremium.setTotalloan(Long.parseLong(loanCalcDetailTypes.getString("totalLoan")));
    loanPremium.setCalculatedate(loanCalcDetailTypes.getString("calculateDate"));
    loanPremium.setLoanyear(Long.parseLong(loanCalcDetailTypes.getString("loanYear")));
    loanPremium.setInterestrate(Double.parseDouble(loanCalcDetailTypes.getString("interestRate")));
    loanPremium.setDuty(Long.parseLong(loanCalcDetailTypes.getString("duty")));

    return loanPremium;
  }

  private GetLoanPremumResponse getCalculateLoan(String policyNo) {
    GetLoanPremumResponse getLoanPremium = null;

    String[] s = new String[] {policyNo, DateInfo.sysDate()};
    PublicRte.setRemote(true);
    Result result =
        PublicRte.getClientResult("blmaster", "rte.bl.dataservice.pos.RteCalculateLoan", s);

    log.debug(
        Service,
        tranId,
        className,
        policyNo + " rs blmaster : " + result.status() + "|" + result.value() + "|");

    if (result.status() == 0 && result.value() instanceof CalLoanBeanSet) {
      CalLoanBeanSet beanSet = (CalLoanBeanSet) result.value();
      getLoanPremium = new GetLoanPremumResponse();
      System.out.println("getLoanPremium " + getLoanPremium);
      CalRequestLoanBean bean = beanSet.getCalRequestLoanBean();
      LoanCalcDetailTypeBean loan = bean.getLoanCalcDetailTypeBean();

      long totalLoanAll = Long.parseLong(loan.getComLoanAmount());
      long totalLoan = Long.parseLong(loan.getLoanAmount()) + Long.parseLong(loan.getInterest());
      long comLoanAmount = totalLoanAll - totalLoan;

      log.debug(Service, tranId, className, "DEBUG :: totalLoanAll = " + totalLoanAll);
      log.debug(Service, tranId, className, "DEBUG :: totalLoan = " + totalLoan);
      log.debug(Service, tranId, className, "DEBUG :: comLoanAmount = " + comLoanAmount);
      comLoanAmount = totalLoanAll - totalLoan;

      if (totalLoanAll > 0) {
        getLoanPremium.setCalculatedate(loan.getCalculateDate());
        getLoanPremium.setIntfromdate(loan.getIntFromDate());
        getLoanPremium.setIntstatus(loan.getIntStatus());
        getLoanPremium.setInttodate(loan.getIntToDate());
        getLoanPremium.setLoandate(loan.getLoanDate());
        getLoanPremium.setLoanamount(Long.parseLong(loan.getLoanAmount()));
        getLoanPremium.setLoanyear(Long.parseLong(loan.getLoanYear()));
        getLoanPremium.setPayloanamount(Long.parseLong(loan.getPayLoanAmount()));
        getLoanPremium.setTotalloan(totalLoan); // loanAmount + interest
        getLoanPremium.setComloanamount(comLoanAmount < 0 ? 0 : comLoanAmount);
        getLoanPremium.setDuty(Long.parseLong(loan.getDuty()));
        getLoanPremium.setInterest(Long.parseLong(loan.getInterest()));
        getLoanPremium.setInterestrate(Double.parseDouble(loan.getInterestRate()));
        getLoanPremium.setTotalloanall(totalLoanAll);
        getLoanPremium.setGrossloanamount(
            Long.parseLong(M.addnum(loan.getPayLoanAmount(), loan.getDuty())));
      } else {
        getLoanPremium.setCalculatedate("00/00/0000");
        getLoanPremium.setIntfromdate("00/00/0000");
        getLoanPremium.setIntstatus("");
        getLoanPremium.setInttodate("00/00/0000");
        getLoanPremium.setLoandate("00/00/0000");
        getLoanPremium.setLoanamount(0);
        getLoanPremium.setLoanyear(0);
        getLoanPremium.setPayloanamount(0);
        getLoanPremium.setTotalloan(0); // loanAmount + interest
        getLoanPremium.setComloanamount(0);
        getLoanPremium.setDuty(0);
        getLoanPremium.setInterest(0);
        getLoanPremium.setInterestrate(0.0);
        getLoanPremium.setTotalloanall(0);
        getLoanPremium.setGrossloanamount(0);
      }
    }

    return getLoanPremium;
  }

  private String getRequest(String policyNo, String date) {
    return "{"
        + "\"headerData\": {"
        + "},"
        + " \"requestRecord\": { "
        + " \"policyNo\": \""
        + policyNo
        + "\" ,\"date\": \""
        + date
        + "\""
        + "}"
        + "} ";
  }
}
