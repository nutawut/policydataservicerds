package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.SaleInfoRequest;
import com.thailife.api.policyInfoService.model.response.SaleInfoResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface SaleInfoService {

  ServiceResults<SaleInfoResponse> getResponse(SaleInfoRequest req, String tranId);

}
