package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.GetLoanPremumRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetLoanPremumResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface GetLoanPremiumService {

  ResponseMessages<GetLoanPremumResponse> service(RequestMessages<GetLoanPremumRequest> req);
}
