package com.thailife.api.policyInfoService.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.thailife.api.policyInfoService.controller.pol11.PaymentHistoryController;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thailife.api.policyInfoService.dao.BeneficiaryDao;
import com.thailife.api.policyInfoService.dao.DetcertDao;
import com.thailife.api.policyInfoService.dao.MainbeneficiaryDao;
import com.thailife.api.policyInfoService.dao.impl.BeneficiaryDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.DetcertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.MainbeneficiaryDaoImpl;
import com.thailife.api.policyInfoService.model.request.BeneficiaryInfoRequest;
import com.thailife.api.policyInfoService.model.response.BeneficiaryInfoResponse;
import com.thailife.api.policyInfoService.service.BeneficiaryInfoService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import utility.prename.Prename;
import utility.text.RelationshipCode;
@Service

public class BeneficiaryInfoServiceImpl implements BeneficiaryInfoService {
    private final ExtLogger log = ExtLogger.create(BeneficiaryInfoServiceImpl.class);
    private final String className = "SaleInfoController";
  private final String Service = "GetSaleInfoPOl07";
  private String tranId;
  
  @Autowired DetcertDaoImpl detcertDaoImpl;
  @Autowired BeneficiaryDaoImpl beneficiaryDaoImpl;
  @Autowired MainbeneficiaryDaoImpl mainbeneficiaryDaoImpl;
  
  @Override
  public List<BeneficiaryInfoResponse> getResponse(BeneficiaryInfoRequest req, String tran_Id) {
    List<BeneficiaryInfoResponse> listBeneficiary =  new ArrayList<BeneficiaryInfoResponse>();
    String policyno = req.getPolicyno();
    String certno = req.getCertno();
    String type = req.getType();
    tranId = tran_Id;
    try {
      String policyType = PolicyUtil.getPolicyTypeByType(type);
      Float sum = 0.00f;
      if (certno != null && !("").equals(certno.trim())) { // CL
        //log.printInfo(Service, tranId, className, "CL");
          DetcertDao detcert = detcertDaoImpl.findByPolicynoAndCertno(policyno, certno);

          if (detcert != null) {
              MainbeneficiaryDao mainbeneficiaryM = mainbeneficiaryDaoImpl.findByIdPolicynoAndIdCertno(policyno, certno);
              String mainbeneficiary = mainbeneficiaryM != null ? mainbeneficiaryM.getName() : "";

              Integer amount = 0;

              String recname1 = detcert.getRecname1();
              BigDecimal relationcode1 = detcert.getRelationshipcode1();
              amount = recname1 != null && !("").equals(recname1.trim()) ? amount + 1 : amount;

              String recname2 = detcert.getRecname2();
              BigDecimal relationcode2 = detcert.getRelationshipcode2();
              amount = recname2 != null && !("").equals(recname2.trim()) ? amount + 1 : amount;

              String recname3 = detcert.getRecname3();
              BigDecimal relationcode3 = detcert.getRelationshipcode3();
              amount = recname3 != null && !("").equals(recname3.trim()) ? amount + 1 : amount;

              if (recname1 != null && !("").equals(recname1.trim())) {
                  String[] name = recname1.split(" ");
                  System.out.println("recname1==" + name.length);
                  String fname = name[0];
                  String lname = "";// name.length > 1 ? name[1] :
                  for (int i = 1; i < name.length; i++) {
                      if (name[i].trim().contains("("))
                          continue;
                      if (lname.equals(""))
                          lname = name[i].trim();
                      else
                          lname = lname + " " + name[i].trim();
                  }
                  String relation = (null == relationcode1 || ("").equals(relationcode1.toString())) ? "" : RelationshipCode.getRelationshipName(relationcode1.toString());
                  Float percentshare = detcert.getPercent1().floatValue();
                  if (percentshare == 0.00) {
                      String[] percent = String.valueOf(100.00 / amount).split("\\.");
                      percentshare = Float.parseFloat(
                              percent[0] + "." + percent[1].substring(0, Math.min(percent[1].length(), 2)));
                  }
                  sum += percentshare;
                  BeneficiaryInfoResponse beneficiary = new BeneficiaryInfoResponse();
                  beneficiary.setSequence("1");
                  beneficiary.setPrename("");
                  beneficiary.setFirstname(fname);
                  beneficiary.setLastname(lname);
                  beneficiary.setRelationship(relation);
                  beneficiary.setPercentshare(percentshare);
                  beneficiary.setMainbeneficiary(mainbeneficiary);

                  listBeneficiary.add(beneficiary);
              }

              if (recname2 != null && !("").equals(recname2.trim())) {
                  String[] name = recname2.split(" ");
                  String fname = name[0];
                  String lname = "";
                  for (int i = 1; i < name.length; i++) {
                      if (name[i].trim().contains("("))
                          continue;
                      if (lname.equals(""))
                          lname = name[i].trim();
                      else
                          lname = lname + " " + name[i].trim();
                  }
                  String relation = (null == relationcode2 || ("").equals(relationcode2.toString())) ? "" : RelationshipCode.getRelationshipName(relationcode2.toString());
                  Float percentshare = detcert.getPercent2().floatValue();
                  if (percentshare == 0.00) {
                      String[] percent = String.valueOf(100.00 / amount).split("\\.");
                      percentshare = Float.parseFloat(
                              percent[0] + "." + percent[1].substring(0, Math.min(percent[1].length(), 2)));
                  }
                  sum += percentshare;
                  BeneficiaryInfoResponse beneficiary = new BeneficiaryInfoResponse();
                  beneficiary.setSequence("2");
                  beneficiary.setPrename("");
                  beneficiary.setFirstname(fname);
                  beneficiary.setLastname(lname);
                  beneficiary.setRelationship(relation);
                  beneficiary.setPercentshare(percentshare);
                  beneficiary.setMainbeneficiary(mainbeneficiary);

                  listBeneficiary.add(beneficiary);
              }

              if (recname3 != null && !("").equals(recname3.trim())) {
                  String[] name = recname3.split(" ");
                  String fname = name[0];
                  String lname = "";
                  for (int i = 1; i < name.length; i++) {
                      if (name[i].trim().contains("("))
                          continue;
                      if (lname.equals(""))
                          lname = name[i].trim();
                      else
                          lname = lname + " " + name[i].trim();
                  }
                  String relation = (null == relationcode3 || ("").equals(relationcode3.toString())) ? "" : RelationshipCode.getRelationshipName(relationcode3.toString());
                  Float percentshare = detcert.getPercent3().floatValue();
                  if (percentshare == 0.00) {
                      String[] percent = String.valueOf(100.00 / amount).split("\\.");
                      percentshare = Float.parseFloat(
                              percent[0] + "." + percent[1].substring(0, Math.min(percent[1].length(), 2)));
                  }
                  sum += percentshare;
                  BeneficiaryInfoResponse beneficiary = new BeneficiaryInfoResponse();
                  beneficiary.setSequence("3");
                  beneficiary.setPrename("");
                  beneficiary.setFirstname(fname);
                  beneficiary.setLastname(lname);
                  beneficiary.setRelationship(relation);
                  beneficiary.setPercentshare(percentshare);
                  beneficiary.setMainbeneficiary(mainbeneficiary);

                  listBeneficiary.add(beneficiary);
              }
              sum = Float.parseFloat(String.valueOf(sum).substring(0, 5));
              Integer len = (int) ((100.00 - sum) * 100);
              for (int i = 0; i < len; i++) {
                  Float percentshare = (listBeneficiary.get(i).getPercentshare() + 0.01f);
                  listBeneficiary.get(i).setPercentshare(percentshare);
              }
          }
      } else {
          //System.out.println("policyType : " + policyType);
          List<BeneficiaryDao> beneficiarys = null;
          if (policyType == null)
            beneficiarys = beneficiaryDaoImpl.findByPolicyno(policyno);
          else
            beneficiarys = beneficiaryDaoImpl.findByPolicynoAndPolicytype(policyno, policyType);
            System.out.println("beneficiarys : " + beneficiarys == null ? beneficiarys : beneficiarys.size() + "|");

          for (BeneficiaryDao b : beneficiarys) {
            String prename = b.getPrename();
            prename = (null == prename || ("").equals(prename) || ("000000").equals(prename)) ? "" : Prename.getAbb(b.getPrename());
            String relation = (null == b.getRelationshipcode() || ("").equals(b.getRelationshipcode().toString())) ? "" 
                    : RelationshipCode.getRelationshipName(b.getRelationshipcode().toString());
            Float percentshare = b.getPercentshare().floatValue();
            if (percentshare == 0.00) {
                String[] percent = String.valueOf(100.00 / beneficiarys.size()).split("\\.");
                percentshare = Float.parseFloat(
                        percent[0] + "." + percent[1].substring(0, Math.min(percent[1].length(), 2)));
            }
            sum += percentshare;
            BeneficiaryInfoResponse beneficiary = new BeneficiaryInfoResponse();
            beneficiary.setSequence(b.getSequence());
            beneficiary.setPrename(prename);
            beneficiary.setFirstname(b.getFirstname());
            beneficiary.setLastname(b.getLastname());
            beneficiary.setRelationship(relation);
            beneficiary.setPercentshare(percentshare);
            beneficiary.setMainbeneficiary("");

            listBeneficiary.add(beneficiary);
          }

          if (beneficiarys.size() > 0) {
              sum = Float.parseFloat(String.valueOf(sum).substring(0, 5));
              int len = (int) ((100.00 - sum) * 100);
              for (int i = 0; i < len; i++) {
                  Float percentshare = (listBeneficiary.get(i).getPercentshare() + 0.01f);
                  listBeneficiary.get(i).setPercentshare(percentshare);
              }
          }
      }
    }catch (Exception e) {
      e.printStackTrace();
      log.error(Service, tranId, className, "BeneficiaryInfoServiceImpl Error Exception : "+ e.getMessage());
    }
    return listBeneficiary;
  }

}
