package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.model.request.ActualValuePolicyRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.ActualValuePolicyResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.ProductType;
import manit.rte.Result;
import org.springframework.stereotype.Service;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;
import utility.ul.AV;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;

@Service
public class ActualValuePolicyService {
  private final Log log = new Log();
  private final String className = "ActualValuePolicyService";

  public ResponseMessages<ActualValuePolicyResponse> service(
      RequestMessages<ActualValuePolicyRequest> request) {
    ResponseMessages<ActualValuePolicyResponse> response = new ResponseMessages<>();
    log.printInfo(className, " service [START] ");
    try {
      if (validateRequest(request)) {
        response.setResponseStatus(PolicyUtil.responseValidateError());
      } else {
        String policyNo = request.getRequestRecord().getPolicyNo();
        String type =
            ProductType.typeGetTable(request.getRequestRecord().getType()); // getType  -ULIP-

        log.printDebug(className, " type: " + type + "|[START]");
        boolean isSuccess = true;
        switch (type) {
          case "universallife":
            response.setResponseRecord(universalLife(policyNo));
            break;
          case "unitlink":
            response.setResponseRecord(unitLink(policyNo));
            break;
          default:
            response.setResponseRecord(defaultResponseRecord());
            isSuccess = false;
            break;
        }
        log.printDebug(className, "type: " + type + "|[END]");
        if (isSuccess) {
          response.setResponseStatus(PolicyUtil.responseStatusSuccess());
        } else {
          response.setResponseStatus(PolicyUtil.responseDataNotFound());
        }
      }
    } catch (Exception e) {
      log.printError(className, "errorMessage: " + e.getMessage());
      response.setResponseRecord(defaultResponseRecord());
      response.setResponseStatus(PolicyUtil.responseInternalServerError());
      e.printStackTrace();
    } finally {
      response.setHeaderData(PolicyUtil.headerDataResponse(request.getHeaderData()));
      log.printInfo(className, " service [END] ");
    }
    return response;
  }

  private ActualValuePolicyResponse unitLink(String policyNo) {
    ActualValuePolicyResponse actualValuePolicyResponse = new ActualValuePolicyResponse();
    double sumTotle;
    Double d1;
    String value;
    PolicyUtil.checkRemote();

    Result rs =
        calRTE(
            new String[] {"searchsales", "rte.bl.unitlink.service.cv.RteFrmAVCerrentInfo"},
            new Object[] {"simulateToWithdraw", policyNo, "0"});

    Object[] xs = (Object[]) rs.value();
    ArrayList<Object[]> listAllPol = (ArrayList<Object[]>) xs[0];

    for (int x = 0; x < listAllPol.size(); x++) {
      Object[] ttt = listAllPol.get(x);
      d1 = new Double(ttt[1].toString());
      value = new DecimalFormat("#.####").format(d1);

      if (x == 0) { // Rpp
        actualValuePolicyResponse.setRpAv(Double.valueOf(value));
      } else if (x == 1) { // Rsp
        actualValuePolicyResponse.setRspAv(Double.valueOf(value));
      } else { // Top_up
        actualValuePolicyResponse.setTpAv(Double.valueOf(value));
      }
    }

    Double rpAv = setDouble(actualValuePolicyResponse.getRpAv());
    Double rspAv = setDouble(actualValuePolicyResponse.getRspAv());
    Double TpAv = setDouble(actualValuePolicyResponse.getTpAv());

    sumTotle = rpAv + rspAv + TpAv;
    actualValuePolicyResponse.setTotalAv(sumTotle);

    return actualValuePolicyResponse;
  }

  private ActualValuePolicyResponse universalLife(String policyNo) throws Exception {
    ActualValuePolicyResponse actualValuePolicyResponse = new ActualValuePolicyResponse();
    String sysDate = DateInfo.sysDate();
    AV av = new AV(true);
    String value_rp_av, value_tp_av;
    double sum = 0.0;
    PolicyUtil.checkRemote();
    Result rs =
        calRTE(
            new String[] {"searchsales", "rte.bl.universal.master.FrmCurrentAV"},
            new Object[] {policyNo, sysDate});
    log.printDebug(className, "result[RTE]: " + rs);

    Vector new_vrp = av.getAvRpSeries(policyNo, sysDate);
    log.printDebug(className, "AV ==> getAvRpSeries: " + new_vrp);

    for (int j = 0; j < new_vrp.size(); j++) {
      /*String[] tmpTPCV = new String[7];*/
      value_rp_av = getStringVector(new_vrp, j);
      actualValuePolicyResponse.setRpAv(Double.valueOf(value_rp_av));
    }

    Vector vtp = av.avTpSeries(policyNo, sysDate);

    for (int i = 0; i < vtp.size(); i++) {
      /*String[] tmpTPCV = new String[5];*/
      value_tp_av = getStringVector(vtp, i);

      sum += Double.parseDouble(value_tp_av);
    }
    actualValuePolicyResponse.setTpAv(sum);
    actualValuePolicyResponse.setRspAv(0.0000);

    Double rpAv = setDouble(actualValuePolicyResponse.getRpAv());
    Double rspAv = setDouble(actualValuePolicyResponse.getRspAv());
    Double TpAv = setDouble(actualValuePolicyResponse.getTpAv());
    actualValuePolicyResponse.setTotalAv(rpAv + rspAv + TpAv);

    return actualValuePolicyResponse;
  }

  private String getStringVector(Vector vtp, int i) {
    double tp_av;
    String value_tp_av;
    String[] ttp = (String[]) vtp.elementAt(i);
    log.printDebug(
        className,
        "ttp-------rp ==> " + ttp[0] + "|" + ttp[1] + "|" + ttp[2] + "|" + ttp[3] + "|" + ttp[4]);
    tp_av = new Double(ttp[3]);
    value_tp_av = new DecimalFormat("#.####").format(tp_av);
    return value_tp_av;
  }

  private Result calRTE(String[] nameRTE, Object[] data) {
    return PublicRte.getResult(nameRTE[0], nameRTE[1], data);
  }

  private Double setDouble(Double val) {
    return BigDecimal.valueOf(val).setScale(2, RoundingMode.HALF_UP).doubleValue();
  }

  private ActualValuePolicyResponse defaultResponseRecord() {
    ActualValuePolicyResponse actualValuePolicyResponse = new ActualValuePolicyResponse();
    actualValuePolicyResponse.setRpAv(0.0);
    actualValuePolicyResponse.setRspAv(0.0);
    actualValuePolicyResponse.setTpAv(0.0);
    actualValuePolicyResponse.setTotalAv(0.0);
    return actualValuePolicyResponse;
  }

  private boolean validateRequest(RequestMessages<ActualValuePolicyRequest> request) {
    if (request == null) {
      log.printDebug(className, " request invalid is null.");
      return true;
    } else {
      if (!PolicyUtil.validateHeaderData(request.getHeaderData())) {
        log.printDebug(className, " headerData invalid.");
        return true;
      }
      if (validateBody(request.getRequestRecord())) {
        log.printDebug(className, " requestRecord invalid.");
        return true;
      }
    }
    return false;
  }

  private boolean validateBody(ActualValuePolicyRequest body) {
    if (body == null) {
      log.printDebug(className, " requestRecord invalid is null. ");
      return true;
    } else {
      if (!PolicyUtil.validateString(body.getPolicyNo())) {
        log.printDebug(className, " policyNo invalid is null or empty. ");
        return true;
      }
      if (!PolicyUtil.validateString(body.getType())) {
        log.printDebug(className, " type invalid is null or empty. ");
        return true;
      }
    }
    return false;
  }
}
