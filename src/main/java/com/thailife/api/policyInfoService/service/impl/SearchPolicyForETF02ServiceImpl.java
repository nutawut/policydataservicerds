package com.thailife.api.policyInfoService.service.impl;

import com.thailife.api.policyInfoService.model.BenefitRecordM;
import com.thailife.api.policyInfoService.model.response.GetDividendInfoResponse;
import com.thailife.api.policyInfoService.model.response.PolicyTypeResponse;
import com.thailife.api.policyInfoService.model.response.SearchPolicyResponse;
import com.thailife.api.policyInfoService.service.GetDividendInfoService;
import com.thailife.api.policyInfoService.service.GetPolicyTypeService;
import com.thailife.api.policyInfoService.service.SearchPolicyForETF02Service;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
@Log4j2
@Service
public class SearchPolicyForETF02ServiceImpl implements SearchPolicyForETF02Service {
  @Autowired private GetDividendInfoService getDividendInfoService;
  @Autowired private GetPolicyTypeService getPolicyTypeService;

  @Override
  public List<SearchPolicyResponse> searchPolicyForETF02Service(
      List<SearchPolicyResponse> request, String transactionId) throws Exception {
    log.info("<== [Start] SearchPolicyForETF02Service ==>");
    String sum;
    try {
      log.debug(
          String.format(
              "TransactionId : %s | list Request size : %s", transactionId, request.size()));
      for (SearchPolicyResponse item : request) {
        PolicyTypeResponse policyTypeResponse =
            getPolicyTypeService.getPolicyTypeResponse(item.getPolicyNo(), item.getCertNo());
        if (policyTypeResponse != null) {
          log.debug(
              String.format(
                  "TransactionId => %s | PolicyType => %s", transactionId, policyTypeResponse));
          String policyType =
              PolicyUtil.validateString(policyTypeResponse.getType())
                  ? policyTypeResponse.getType()
                  : "";
          item.setPolicyType(policyType);
          item.setBenefitRecord(setPolicyTypeInBenefitRecord(item.getBenefitRecord(), policyType));
          List<GetDividendInfoResponse> responsePolicyInfo = new ArrayList<>();
          // adjust do Dividend not CL
          if (!PolicyUtil.validateString(item.getCertNo())) {
            responsePolicyInfo = getDividendInfoService.getDividendInfo(item.getPolicyNo(), item.getCertNo());
          }
          if (!responsePolicyInfo.isEmpty()) {
            log.debug(
                String.format(
                    "TransactionId => %s | DividendInfo => %s", transactionId, responsePolicyInfo));
            BigDecimal sumBenefit = getDividendInfoService.sumBenefitAccruedByStatusReceive(responsePolicyInfo);
/*            for (GetDividendInfoResponse tmp : responsePolicyInfo) {
              if ("1".equals(tmp.getBenefit_receivedstatus())) {
                sumBenefit =
                    sumBenefit.add(PolicyUtil.getDefaultBigDecimal(tmp.getBenefit_premium()));
              }
            }*/
            sum = String.valueOf(sumBenefit.setScale(2, RoundingMode.FLOOR));
            item.setBenefitPremium(sum != null && sum.equals("0.00") ? "" : sum);
          } else {
            item.setBenefitPremium("");
          }
        }
      }
    } catch (Exception e) {
      log.error(String.format("TransactionId : %s Error : %s", transactionId, e.getMessage()));
      e.printStackTrace();
      throw e;
    } finally {
      log.info("<== [End] SearchPolicyForETF02Service ==>");
    }
    return request;
  }

  private List<BenefitRecordM> setPolicyTypeInBenefitRecord(
      List<BenefitRecordM> benefitRecordMList, String policyType) {
    try {
      if (benefitRecordMList != null && benefitRecordMList.size() != 0) {
        for (BenefitRecordM item : benefitRecordMList) {
          item.setPolicy_type(policyType);
        }
      }
      return benefitRecordMList;
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }
}
