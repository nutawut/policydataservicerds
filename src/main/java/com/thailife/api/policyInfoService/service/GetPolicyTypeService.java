package com.thailife.api.policyInfoService.service;

import com.thailife.api.policyInfoService.model.request.PolicyTypeRequest;
import com.thailife.api.policyInfoService.model.response.PolicyTypeResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;

public interface GetPolicyTypeService {

  ServiceResults<PolicyTypeResponse> getResponse(PolicyTypeRequest requestRecord, String tranId);

  public PolicyTypeResponse getPolicyTypeResponse(String policyNo, String certNo);

}
