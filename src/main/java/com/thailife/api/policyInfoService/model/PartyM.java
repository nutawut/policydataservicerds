package com.thailife.api.policyInfoService.model;

import java.io.Serializable;

public class PartyM implements Serializable {
    private static final long serialVersionUID = 1L;
    private String partyId;

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

}
