package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class FirstIssueM {
	private String policyno ;        
	private String policydate ;      
	private String firstpaydate ;    
	private String payridercomm ;    
	private String scoreapproved ;   
	private String noofpage ;        
	private String pagescan ;        
	private String payer ;           
	private String reserve ;         
	private String recordtimestamp ; 
	private String journaltimestamp ;
}