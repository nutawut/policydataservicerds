package com.thailife.api.policyInfoService.model.response;

import lombok.Data;
@Data

public class PolicyDetailCLResponse {

  private String prename;
  private String firstname;
  private String lastname;
  private String sex;
  private String policyholder;
  private String creditcardnumber;
  private String type;
  private int insureage;
  private String effectivedate;
  private String maturedate;
  private String plancode;
  private String planname;
  private String sum;
  private String mode;
  private String statcer;
  private String statcerdesc;
  private String statcer2;
  private String statcer2desc;
  private String remark;
  private String statusdisplay;
  private String endownmentyear;


}
