package com.thailife.api.policyInfoService.model.subModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LifeDetailM {
  private String ridercode;
  private String ridername;
  private String ridersortname;
  private String ridersum;
  private String riderpremium;
  private String riderstatus;
  private String riderstatusdesc;
  private String riderstatusdate;
  private String statusdisplay;
  private String effectivedate;
  private String extrariderpremium;
}
