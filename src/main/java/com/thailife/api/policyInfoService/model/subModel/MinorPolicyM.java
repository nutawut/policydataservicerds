package com.thailife.api.policyInfoService.model.subModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MinorPolicyM {
    private String policyno;
    private String certno;
    private String type ;
    private String plancode;
    private String planname;
    private String policystatus1;
    private String effectivedate;
    private String nextduedate;
    private String statusdisplay;
    private String screentype;
    private String oldCertNo;
    private String oldPolicyNo;
    private Integer sum;

}
