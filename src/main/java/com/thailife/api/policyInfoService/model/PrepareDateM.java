package com.thailife.api.policyInfoService.model;

import lombok.Data;

import java.util.Date;
@Data
public class PrepareDateM {
  String payDateMaster;
  Integer year;
  Integer month;
  Integer day;
  String effectiveDateMaster;
  Date payDateM;
  Date payDateAgo;
  String payDateCond;
  String[] yearTable;
}
