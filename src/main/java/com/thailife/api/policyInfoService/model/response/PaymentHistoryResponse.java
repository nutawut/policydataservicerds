package com.thailife.api.policyInfoService.model.response;

import com.thailife.api.policyInfoService.model.PaymentM;
import lombok.Data;

import java.util.List;

@Data
public class PaymentHistoryResponse {

  private List<PaymentM> list_payment_history;
  private String type;
}
