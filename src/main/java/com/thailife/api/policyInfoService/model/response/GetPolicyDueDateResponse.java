package com.thailife.api.policyInfoService.model.response;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Data
public class GetPolicyDueDateResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1631794015540650701L;
	private List<PolicyResponse> list_of_policy;

	
}