package com.thailife.api.policyInfoService.model.request;

import lombok.Data;

@Data
public class GetPolicyDueDateRequest {
	
	private String customer_id;

}
