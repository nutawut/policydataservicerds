package com.thailife.api.policyInfoService.model.request;

import java.util.List;
import com.thailife.api.policyInfoService.model.response.PolicyM;

public class InsertConsentRequest {
	private String customer_id;
	private String preName;
	private String firstName;
	private String lastName;
	private List<PolicyM> policy;

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getPreName() {
		return preName;
	}

	public void setPreName(String preName) {
		this.preName = preName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<PolicyM> getPolicy() {
		return policy;
	}

	public void setPolicy(List<PolicyM> policy) {
		this.policy = policy;
	}

}
