package com.thailife.api.policyInfoService.model;

public class WrctrlM {

	private String rpno;
	private String policyno;
	private String effectivedate;
	private String payperiod;
	private String paydate;
	private Integer premium;
	private Integer extraprem;
	private String sysdate;
	private String currentstatus;
	private String originalstatus;
	private String mode;
	private String time;
	private String requestdate;
	private String submitno;
	private String graceperiod;
	private String printeddate;
	private String submitbranch;
	private String userid;
	private String reasoncode;
	private String moneyok;
	private String recordtimestamp;
	private String journaltimestamp;

	public String getRpno() {
		return rpno;
	}

	public void setRpno(String rpno) {
		this.rpno = rpno;
	}

	public String getPolicyno() {
		return policyno;
	}

	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}

	public String getEffectivedate() {
		return effectivedate;
	}

	public void setEffectivedate(String effectivedate) {
		this.effectivedate = effectivedate;
	}

	public String getPayperiod() {
		return payperiod;
	}

	public void setPayperiod(String payperiod) {
		this.payperiod = payperiod;
	}

	public String getPaydate() {
		return paydate;
	}

	public void setPaydate(String paydate) {
		this.paydate = paydate;
	}

	public Integer getPremium() {
		return premium;
	}

	public void setPremium(Integer premium) {
		this.premium = premium;
	}

	public Integer getExtraprem() {
		return extraprem;
	}

	public void setExtraprem(Integer extraprem) {
		this.extraprem = extraprem;
	}

	public String getSysdate() {
		return sysdate;
	}

	public void setSysdate(String sysdate) {
		this.sysdate = sysdate;
	}

	public String getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(String currentstatus) {
		this.currentstatus = currentstatus;
	}

	public String getOriginalstatus() {
		return originalstatus;
	}

	public void setOriginalstatus(String originalstatus) {
		this.originalstatus = originalstatus;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getRequestdate() {
		return requestdate;
	}

	public void setRequestdate(String requestdate) {
		this.requestdate = requestdate;
	}

	public String getSubmitno() {
		return submitno;
	}

	public void setSubmitno(String submitno) {
		this.submitno = submitno;
	}

	public String getGraceperiod() {
		return graceperiod;
	}

	public void setGraceperiod(String graceperiod) {
		this.graceperiod = graceperiod;
	}

	public String getPrinteddate() {
		return printeddate;
	}

	public void setPrinteddate(String printeddate) {
		this.printeddate = printeddate;
	}

	public String getSubmitbranch() {
		return submitbranch;
	}

	public void setSubmitbranch(String submitbranch) {
		this.submitbranch = submitbranch;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getReasoncode() {
		return reasoncode;
	}

	public void setReasoncode(String reasoncode) {
		this.reasoncode = reasoncode;
	}

	public String getMoneyok() {
		return moneyok;
	}

	public void setMoneyok(String moneyok) {
		this.moneyok = moneyok;
	}

	public String getRecordtimestamp() {
		return recordtimestamp;
	}

	public void setRecordtimestamp(String recordtimestamp) {
		this.recordtimestamp = recordtimestamp;
	}

	public String getJournaltimestamp() {
		return journaltimestamp;
	}

	public void setJournaltimestamp(String journaltimestamp) {
		this.journaltimestamp = journaltimestamp;
	}
}
