package com.thailife.api.policyInfoService.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CoverageMainM {
  private String coverage_group_code;
  private String coverage_name_detail_th;
  private String coverage_name_detail_eng;
  private String coverage_remark_th;
  private String coverage_remark_eng;
  private BigDecimal coverage_suminsure;
}
