package com.thailife.api.policyInfoService.model.request;

import lombok.Data;

@Data
public class Pol05RequestM {
    private String policyno;
    private String certno;
    private String type;
}
