package com.thailife.api.policyInfoService.model.response;

import java.util.List;

public class ResponseMessagesList<T> {

	private HeaderData headerData;
	private List<T> responseRecord;
	private ResponseStatus responseStatus;

	public ResponseMessagesList() {
		super();
		this.headerData = new HeaderData();
		this.responseStatus = new ResponseStatus();
	}

	public ResponseMessagesList(List<T> responseRecord) {
		super();
		this.responseRecord = responseRecord;
		this.headerData = new HeaderData();
		this.responseStatus = new ResponseStatus();
	}

	@Override
	public String toString() {
		return "ResponseMessages [headerData=" + headerData + ", responseRecord=" + responseRecord
				+ ", responseStatuss=" + responseStatus + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((headerData == null) ? 0 : headerData.hashCode());
		result = prime * result + ((responseRecord == null) ? 0 : responseRecord.hashCode());
		result = prime * result + ((responseStatus == null) ? 0 : responseStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseMessagesList<?> other = (ResponseMessagesList<?>) obj;
		if (headerData == null) {
			if (other.headerData != null)
				return false;
		} else if (!headerData.equals(other.headerData))
			return false;
		if (responseRecord == null) {
			if (other.responseRecord != null)
				return false;
		} else if (!responseRecord.equals(other.responseRecord))
			return false;
		if (responseStatus == null) {
			if (other.responseStatus != null)
				return false;
		} else if (!responseStatus.equals(other.responseStatus))
			return false;
		return true;
	}

	public HeaderData getHeaderData() {
		return headerData;
	}

	public void setHeaderData(HeaderData headerData) {
		this.headerData = headerData;
	}

	public List<T> getResponseRecord() {
		return responseRecord;
	}

	public void setResponseRecord(List<T> responseRecord) {
		this.responseRecord = responseRecord;
	}

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatuss) {
		this.responseStatus = responseStatuss;
	}
}
