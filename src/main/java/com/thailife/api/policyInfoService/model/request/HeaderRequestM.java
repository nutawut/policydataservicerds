package com.thailife.api.policyInfoService.model.request;

import java.io.Serializable;
import com.thailife.api.policyInfoService.model.response.HeaderData;

public class HeaderRequestM implements Serializable {

	private static final long serialVersionUID = 1L;

	private HeaderData headerData;

	public HeaderData getHeaderData() {
		return headerData;
	}

	public void setHeaderData(HeaderData headerData) {
		this.headerData = headerData;
	}

}
