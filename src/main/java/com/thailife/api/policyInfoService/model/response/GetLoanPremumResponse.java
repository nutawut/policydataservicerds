package com.thailife.api.policyInfoService.model.response;

import lombok.Data;

@Data
public class GetLoanPremumResponse {
	private String loandate;
	private long loanamount;
	private String intfromdate;
	private String inttodate;
	private String intstatus;
	private long interest;
	private long totalloan;
	private String calculatedate;
	private long comloanamount;
	private long loanyear;
	private double interestrate;
	private long duty;
	private long payloanamount;
	private long totalloanall;
	private long grossloanamount;
	

}
