package com.thailife.api.policyInfoService.model.response;

import lombok.Data;
@Data
public class PolicyDetailOLResponse {

  private String prename;
  private String firstname;
  private String lastname;
  private String sex;
  private String type;
  private int lifepremium;
  private int topuppremium;
  private int insureage;
  private String effectivedate;
  private String maturedate;
  private String plancode;
  private String planname;
  private int sum;
  private String mode;
  private String policystatus1;
  private String policystatus1desc;
  private String policystatus2;
  private String policystatus2desc;
  private String remark;
  private String statusdisplay;
  private String endownmentyear;

}
