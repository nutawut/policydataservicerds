package com.thailife.api.policyInfoService.model.response;

import lombok.Data;

@Data
public class PolicyTypeResponse {
	
	private String type;
	private String screentype;

}
