package com.thailife.api.policyInfoService.model.response;

import java.util.List;
import com.thailife.api.policyInfoService.model.CurrentAVM;
import lombok.Data;
@Data

public class CurrentAVInfoResponse {
	private List<CurrentAVM> current_av_list;
}
