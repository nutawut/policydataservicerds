package com.thailife.api.policyInfoService.model;

import com.thailife.api.policyInfoService.model.response.GetCoverageOverviewResponse;
import lombok.Data;

import java.util.List;

@Data
public class ListOfProtection {
  List<GetCoverageOverviewResponse> list_of_protection;
}
