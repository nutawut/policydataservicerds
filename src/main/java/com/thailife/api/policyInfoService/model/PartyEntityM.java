package com.thailife.api.policyInfoService.model;

public class PartyEntityM {
	public PartyEntityM() {}

	 public PartyEntityM(String party_id, String party_type, String full_name_th, String full_name_en, String issue_country,
                         String govt_id, String govt_id_type, String is_sms, String create_user_code, String create_time,
                         String update_user_code, String last_update, String system_id, String system_key) {
		super();
		this.party_id = party_id;
		this.party_type = party_type;
		this.full_name_th = full_name_th;
		this.full_name_en = full_name_en;
		this.issue_country = issue_country;
		this.govt_id = govt_id;
		this.govt_id_type = govt_id_type;
		this.is_sms = is_sms;
		this.create_user_code = create_user_code;
		this.create_time = create_time;
		this.update_user_code = update_user_code;
		this.last_update = last_update;
		this.system_id = system_id;
		this.system_key = system_key;
	}
	public String party_id = "";        
	 public String party_type = "";      
	 public String full_name_th = "";    
	 public String full_name_en = "";    
	 public String issue_country = "";   
	 public String govt_id = "";         
	 public String govt_id_type = "";    
	 public String is_sms = "";          
	 public String create_user_code = "";
	 public String create_time = "";     
	 public String update_user_code = "";
	 public String last_update = "";     
	 public String system_id = "";       
	 public String system_key = "";
	public String getParty_id() {
		return party_id;
	}
	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}
	public String getParty_type() {
		return party_type;
	}
	public void setParty_type(String party_type) {
		this.party_type = party_type;
	}
	public String getFull_name_th() {
		return full_name_th;
	}
	public void setFull_name_th(String full_name_th) {
		this.full_name_th = full_name_th;
	}
	public String getFull_name_en() {
		return full_name_en;
	}
	public void setFull_name_en(String full_name_en) {
		this.full_name_en = full_name_en;
	}
	public String getIssue_country() {
		return issue_country;
	}
	public void setIssue_country(String issue_country) {
		this.issue_country = issue_country;
	}
	public String getGovt_id() {
		return govt_id;
	}
	public void setGovt_id(String govt_id) {
		this.govt_id = govt_id;
	}
	public String getGovt_id_type() {
		return govt_id_type;
	}
	public void setGovt_id_type(String govt_id_type) {
		this.govt_id_type = govt_id_type;
	}
	public String getIs_sms() {
		return is_sms;
	}
	public void setIs_sms(String is_sms) {
		this.is_sms = is_sms;
	}
	public String getCreate_user_code() {
		return create_user_code;
	}
	public void setCreate_user_code(String create_user_code) {
		this.create_user_code = create_user_code;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getUpdate_user_code() {
		return update_user_code;
	}
	public void setUpdate_user_code(String update_user_code) {
		this.update_user_code = update_user_code;
	}
	public String getLast_update() {
		return last_update;
	}
	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}
	public String getSystem_id() {
		return system_id;
	}
	public void setSystem_id(String system_id) {
		this.system_id = system_id;
	}
	public String getSystem_key() {
		return system_key;
	}
	public void setSystem_key(String system_key) {
		this.system_key = system_key;
	}     
	 
	 
}
