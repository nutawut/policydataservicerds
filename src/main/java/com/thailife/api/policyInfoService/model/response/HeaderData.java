package com.thailife.api.policyInfoService.model.response;

import java.io.Serializable;
import com.thailife.api.policyInfoService.util.PolicyUtil;


public class HeaderData implements Serializable {

	private static final long serialVersionUID = 1L;

	private String messageId;	
	private String sentDateTime;	
	private String responseDateTime;	
	private final String version = PolicyUtil.getVersion();

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getSentDateTime() {
		return sentDateTime;
	}

	public void setSentDateTime(String sentDateTime) {
		this.sentDateTime = sentDateTime;
	}

	public String getResponseDateTime() {
		return responseDateTime;
	}

	public void setResponseDateTime(String responseDateTime) {
		this.responseDateTime = responseDateTime;
	}

	public String getVersion() {
		return version;
	}

	
}
