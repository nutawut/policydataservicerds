package com.thailife.api.policyInfoService.model.response;

import java.io.Serializable;
import lombok.Data;
@Data
public class PolicyDetailsUnitlinkResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String prename;
	private String firstname;
	private String lastname;
	private String sex;
	private String type;
	private int rspsum;
	private int rsppremium;
	private int rppsum;
	private int rpppremium;
	private int insureage;
	private String effectivedate;
	private String maturedate;
	private String plancode;
	private String planname;
	private String sum;
	private String mode;
	private String policystatus1;
	private String policystatus1desc;
	private String policystatus2;
	private String policystatus2desc;
	private String remark;
	private String statusdisplay;
	private String endownmentyear;
	private String rspstatus;
	private String rspstatusdesc;
}
