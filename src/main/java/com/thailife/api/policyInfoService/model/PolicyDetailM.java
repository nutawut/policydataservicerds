package com.thailife.api.policyInfoService.model;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class PolicyDetailM {

  private String policystatus1;
  private String policystatus2;
  private String policystatusdate1;
  private String policystatusdate2;
  private String oldpolicystatus1;
  private String oldpolicystatus2;
  private String oldpolicystatusdate1;
  private String oldpolicystatusdate2;
  private String payperiod;
  private String duedate;
  private String effectivedate;
  private BigDecimal tpdpremium;
  private BigDecimal addpremium;
  private BigDecimal lifesum;
  private BigDecimal extratpdpremium;
  private BigDecimal accidentsum;
  private String planCode;
  private String mode;
}
