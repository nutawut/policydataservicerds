package com.thailife.api.policyInfoService.model.request;

import java.io.Serializable;

public class SearchPolicyByCustomerIdRequest implements Serializable {
  private static final long serialVersionUID = 1L;
  private String customer_id;

  @Override
  public String toString() {
    return "SearchPolicyByCustomerIdRequest{" +
            "customer_id='" + customer_id + '\'' +
            '}';
  }

  public String getCustomer_id() {
    return customer_id;
  }

  public void setCustomer_id(String customer_id) {
    this.customer_id = customer_id;
  }
}
