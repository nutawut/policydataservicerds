package com.thailife.api.policyInfoService.model.response;

import com.thailife.api.policyInfoService.model.subModel.MinorNameM;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Getter
@Setter
@ToString
public class GetMinorPolicyInfoResponse {
    private List<MinorNameM> list_of_Minor_name;
}
