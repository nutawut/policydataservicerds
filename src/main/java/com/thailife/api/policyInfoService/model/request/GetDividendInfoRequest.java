package com.thailife.api.policyInfoService.model.request;

import lombok.Data;

@Data
public class GetDividendInfoRequest {
	
	private String policyno;
	private String certno;
	private String type;
	
	
}
