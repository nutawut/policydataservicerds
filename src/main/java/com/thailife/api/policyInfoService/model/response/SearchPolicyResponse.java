package com.thailife.api.policyInfoService.model.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.thailife.api.policyInfoService.model.BenefitRecordM;

import java.util.List;

public class SearchPolicyResponse {

    @JsonAlias({"policyno"})
    private String policyNo;

    @JsonAlias({"certno"})
    private String certNo;

    @JsonAlias({"policy_type"})
    private String policyType;

    @JsonAlias({"plan_code"})
    private String planCode;

    @JsonAlias({"plan_name"})
    private String planName;

    @JsonAlias({"function_group"})
    private String functionGroup;

    @JsonAlias({"pay_type"})
    private String payType;

    @JsonAlias({"tranfer_type"})
    private String tranferType;

    @JsonAlias({"bank_code"})
    private String bankCode;

    @JsonAlias({"bank_account"})
    private String bankAccount;

    @JsonAlias({"account_name"})
    private String accountName;

    @JsonAlias({"bank_branch"})
    private String bankBranch;

    @JsonAlias({"from_system"})
    private String fromSystem;

    @JsonAlias({"receiver_type"})
    private String receiverType;

    @JsonAlias({"effective_date"})
    private String effectiveDate;

    private String benefitPremium;

    private String delegate;

    @JsonAlias({"invalid_status"})
    private String invalidStatus;

    @JsonAlias({"last_update"})
    private String lastUpdate;

    private List<BenefitRecordM> benefitRecord;

    @Override
    public String toString() {
        return "SearchPolicyResponse{" +
                "policyNo='" + policyNo + '\'' +
                ", certNo='" + certNo + '\'' +
                ", policyType='" + policyType + '\'' +
                ", planCode='" + planCode + '\'' +
                ", planName='" + planName + '\'' +
                ", functionGroup='" + functionGroup + '\'' +
                ", payType='" + payType + '\'' +
                ", tranferType='" + tranferType + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", bankAccount='" + bankAccount + '\'' +
                ", accountName='" + accountName + '\'' +
                ", bankBranch='" + bankBranch + '\'' +
                ", fromSystem='" + fromSystem + '\'' +
                ", receiverType='" + receiverType + '\'' +
                ", effectiveDate='" + effectiveDate + '\'' +
                ", benefitPremium='" + benefitPremium + '\'' +
                ", delegate='" + delegate + '\'' +
                ", invalidStatus='" + invalidStatus + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                ", benefitRecord=" + benefitRecord +
                '}';
    }

    public String getDelegate() {
        return delegate;
    }

    public void setDelegate(String delegate) {
        this.delegate = delegate;
    }

    public String getInvalidStatus() {
        return invalidStatus;
    }

    public void setInvalidStatus(String invalidStatus) {
        this.invalidStatus = invalidStatus;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getBenefitPremium() {
        return benefitPremium;
    }

    public void setBenefitPremium(String benefitPremium) {
        this.benefitPremium = benefitPremium;
    }

    public List<BenefitRecordM> getBenefitRecord() {
        return benefitRecord;
    }

    public void setBenefitRecord(List<BenefitRecordM> benefitRecord) {
        this.benefitRecord = benefitRecord;
    }

    public String getPolicyType() {
        return policyType;
    }

    public void setPolicyType(String policyType) {
        this.policyType = policyType;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getFunctionGroup() {
        return functionGroup;
    }

    public void setFunctionGroup(String functionGroup) {
        this.functionGroup = functionGroup;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getTranferType() {
        return tranferType;
    }

    public void setTranferType(String tranferType) {
        this.tranferType = tranferType;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getFromSystem() {
        return fromSystem;
    }

    public void setFromSystem(String fromSystem) {
        this.fromSystem = fromSystem;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

}
