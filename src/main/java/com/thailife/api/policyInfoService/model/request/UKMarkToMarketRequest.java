package com.thailife.api.policyInfoService.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UKMarkToMarketRequest {
    private String policyNo;
    private String calflag;
    private String channel;

}
