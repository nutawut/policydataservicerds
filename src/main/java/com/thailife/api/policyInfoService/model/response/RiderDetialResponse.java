package com.thailife.api.policyInfoService.model.response;

import java.util.List;
import com.thailife.api.policyInfoService.model.RiderM;
import com.thailife.api.policyInfoService.model.subModel.LifeDetailM;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RiderDetialResponse {

	List<RiderM> list_of_rider;

	List<LifeDetailM> life_details;

}
