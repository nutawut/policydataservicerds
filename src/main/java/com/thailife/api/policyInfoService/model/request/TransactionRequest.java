package com.thailife.api.policyInfoService.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
@Getter
@Setter
@ToString
public class TransactionRequest {
  private Long id;
  private Long transactionId;
  private String customerId;
  private String referenceId;
  private String requestJson;
  private String responseJson;
  private Date createDatetime;
  private String createBy;
  private String transactionType;
  private String thirdParty;
  private String channel;

  public TransactionRequest() {}

  public TransactionRequest(String requestJson, String createBy, String transactionType) {
    this.requestJson = requestJson;
    this.createBy = createBy;
    this.transactionType = transactionType;
  }

  public TransactionRequest(Long id, String customerId, String referenceId, String responseJson) {
    this.id = id;
    this.customerId = customerId;
    this.referenceId = referenceId;
    this.responseJson = responseJson;
  }
  public TransactionRequest(Long id, String customerId, String referenceId, String responseJson, String thirdParty) {
    this.id = id;
    this.customerId = customerId;
    this.referenceId = referenceId;
    this.responseJson = responseJson;
    this.thirdParty = thirdParty;
  }
  public TransactionRequest(Long id, String customerId, String referenceId, String responseJson, String thirdParty, String channel) {
    this.id = id;
    this.customerId = customerId;
    this.referenceId = referenceId;
    this.responseJson = responseJson;
    this.thirdParty = thirdParty;
    this.channel = channel;
  }
  public TransactionRequest(String customerId, String referenceId, String requestJson, String createBy, String transactionType) {
    this.customerId = customerId;
    this.referenceId = referenceId;
    this.requestJson = requestJson;
    this.createBy = createBy;
    this.transactionType = transactionType;
  }

  public TransactionRequest(
      Long id,
      Long transactionId,
      String customerId,
      String referenceId,
      String requestJson,
      String responseJson,
      Date createDatetime,
      String createBy,
      String transactionType) {
    this.id = id;
    this.transactionId = transactionId;
    this.customerId = customerId;
    this.referenceId = referenceId;
    this.requestJson = requestJson;
    this.responseJson = responseJson;
    this.createDatetime = createDatetime;
    this.createBy = createBy;
    this.transactionType = transactionType;
  }
}
