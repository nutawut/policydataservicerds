package com.thailife.api.policyInfoService.model.response;

import com.thailife.api.policyInfoService.model.PolicyM;

import java.io.Serializable;
import java.util.List;

public class SearchPolicyByCustomerIdResponse implements Serializable {

  private static final long serialVersionUID = -1631794015540650701L;
  private List<PolicyM> list_of_policy;

  @Override
  public String toString() {
    return "SearchPolicyByCustomerIdResponse{" + "list_of_policy=" + list_of_policy + '}';
  }

  public List<PolicyM> getList_of_policy() {
    return list_of_policy;
  }

  public void setList_of_policy(List<PolicyM> list_of_policy) {
    this.list_of_policy = list_of_policy;
  }
}
