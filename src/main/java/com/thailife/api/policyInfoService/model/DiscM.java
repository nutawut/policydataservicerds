package com.thailife.api.policyInfoService.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DiscM {
  public String journaltimestamp = "";
  public String recordtimestamp = "";
  public String statusdate = "";
  public String status = "";
  public String discountamount = "";
  public String discountrate = "";
  public String extrapremium = "";
  public String premium = "";
  public String sysdate = "";
  public String rpno = "";
  public String payperiod = "";
  public String policyno = "";
  public String citizenid = "";
}
