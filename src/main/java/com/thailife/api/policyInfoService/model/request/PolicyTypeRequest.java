package com.thailife.api.policyInfoService.model.request;

import lombok.Data;

@Data
public class PolicyTypeRequest {
	private String policyno;
	private String certno;
}
