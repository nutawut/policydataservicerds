package com.thailife.api.policyInfoService.model.request;

public class RiderTypeRequest {
	
	private String riderType;

	public String getRiderType() {
		return riderType;
	}

	public void setRiderType(String riderType) {
		this.riderType = riderType;
	}
	
}
