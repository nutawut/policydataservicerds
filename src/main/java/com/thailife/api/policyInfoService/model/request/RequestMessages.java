package com.thailife.api.policyInfoService.model.request;

import com.thailife.api.policyInfoService.model.response.HeaderData;

public class RequestMessages<T> {

	private HeaderData headerData;
	private T requestRecord;

	public HeaderData getHeaderData() {
		return headerData;
	}

	public void setHeaderData(HeaderData headerData) {
		this.headerData = headerData;
	}

	public T getRequestRecord() {
		return requestRecord;
	}

	public void setRequestRecord(T requestRecord) {
		this.requestRecord = requestRecord;
	}

	@Override
	public String toString() {
		return "RequestMessages [headerData=" + headerData + ", requestRecord=" + requestRecord + "]";
	}

}
