package com.thailife.api.policyInfoService.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BenefitRecordM {
    @JsonProperty("policyType")
    @JsonAlias("policy_type")
    private String policy_type;
    @JsonProperty("serviceType")
    @JsonAlias("service_type")
    private String service_type;
    @JsonProperty("policyNo")
    @JsonAlias("policyno")
    private String policyno;
    @JsonProperty("certNo")
    @JsonAlias("certno")
    private String certno;

    @Override
    public String toString() {
        return "BenefitRecordM{" +
                "policy_type='" + policy_type + '\'' +
                ", service_type='" + service_type + '\'' +
                ", policyno='" + policyno + '\'' +
                ", certno='" + certno + '\'' +
                '}';
    }

    public String getPolicy_type() {
        return policy_type;
    }

    public void setPolicy_type(String policy_type) {
        this.policy_type = policy_type;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getPolicyno() {
        return policyno;
    }

    public void setPolicyno(String policyno) {
        this.policyno = policyno;
    }

    public String getCertno() {
        return certno;
    }

    public void setCertno(String certno) {
        this.certno = certno;
    }
}
