package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class PaymentM {

  private String pay_period;
  private String effective_date;
  private String pay_date;
  private String receipt_no;
  private String premium;
  private String extrapremium;
  private String discountpremium;
  private String totalpremium; // premium + extrapremium - discountpremium
  private String payment_channel;
  private String duedate;
  private String receipt_sys_date;

  private String pay_mode;
  private String pay_condition;
  private String submitNo;
}
