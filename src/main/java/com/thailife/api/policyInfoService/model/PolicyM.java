package com.thailife.api.policyInfoService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PolicyM {
  @JsonProperty(value = "policyno")
  private String policyNo;

  @JsonProperty(value = "certno")
  private String certNo;

  @JsonProperty(value = "plancode")
  private String planCode;

  @JsonProperty(value = "planname")
  private String planName;

  private String type;

  @JsonProperty(value = "screentype")
  private String screenType;

  @JsonProperty(value = "effectivedate")
  private String effectiveDate;

  @JsonProperty(value = "maturedate")
  private String matureDate;

  @JsonProperty(value = "policystatusdate1")
  private String policyStatusDate1;

  @JsonProperty(value = "policystatus1")
  private String policyStatus1;

  @JsonProperty("policystatus1desc")
  private String policyStatus1desc;

  @JsonProperty(value = "statusdisplay")
  private String statusDisplay;

  private Number sum;

  @JsonProperty(value = "prename")
  private String preName;

  private String firstname;
  private String lastname;

  @JsonProperty(value = "nextduedate")
  private String nextDueDate;

  @JsonProperty(value = "ridersum")
  private Number riderSum;

  private String GroupPolicyStatus;
  private String oldCertNo;
  private String oldPolicyNo;

  @JsonProperty(value = "payperiod")
  private String payPeriod;

  public PolicyM(String policyNo, String certNo) {
    this.policyNo = policyNo;
    this.certNo = certNo;
  }
}
