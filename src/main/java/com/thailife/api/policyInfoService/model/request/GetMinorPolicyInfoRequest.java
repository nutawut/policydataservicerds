package com.thailife.api.policyInfoService.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GetMinorPolicyInfoRequest {
    private String customer_id;
}
