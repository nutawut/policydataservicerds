package com.thailife.api.policyInfoService.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SumRiderM {
private String type;
private BigDecimal  riderSum;
}
