package com.thailife.api.policyInfoService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CustomerIdM {
  @JsonProperty(value = "customer_id")
  private String customerId;
}
