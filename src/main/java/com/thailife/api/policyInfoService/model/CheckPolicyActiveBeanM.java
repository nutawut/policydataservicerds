package com.thailife.api.policyInfoService.model;

public class CheckPolicyActiveBeanM {
  private String policyStatus1;
  private String policyStatus2;
  private String oldStatus1;
  private String oldStatus2;
  private String table;
  private String policyType;
  private String bassType;

  public String getPolicyType() {
    return policyType;
  }

  public void setPolicyType(String policyType) {
    this.policyType = policyType;
  }

  public String getBassType() {
    return bassType;
  }

  public void setBassType(String bassType) {
    this.bassType = bassType;
  }

  public String getPolicyStatus1() {
    return policyStatus1;
  }

  public void setPolicyStatus1(String policyStatus1) {
    this.policyStatus1 = policyStatus1;
  }

  public String getPolicyStatus2() {
    return policyStatus2;
  }

  public void setPolicyStatus2(String policyStatus2) {
    this.policyStatus2 = policyStatus2;
  }

  public String getOldStatus1() {
    return oldStatus1;
  }

  public void setOldStatus1(String oldStatus1) {
    this.oldStatus1 = oldStatus1;
  }

  public String getOldStatus2() {
    return oldStatus2;
  }

  public void setOldStatus2(String oldStatus2) {
    this.oldStatus2 = oldStatus2;
  }

  public String getTable() {
    return table;
  }

  public void setTable(String table) {
    this.table = table;
  }

  @Override
  public String toString() {
    return "CheckPolicyActiveBeanM{" +
            "policyStatus1='" + policyStatus1 + '\'' +
            ", policyStatus2='" + policyStatus2 + '\'' +
            ", oldStatus1='" + oldStatus1 + '\'' +
            ", oldStatus2='" + oldStatus2 + '\'' +
            ", table='" + table + '\'' +
            ", policyType='" + policyType + '\'' +
            ", bassType='" + bassType + '\'' +
            '}';
  }
}
