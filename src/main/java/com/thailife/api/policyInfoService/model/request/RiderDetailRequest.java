package com.thailife.api.policyInfoService.model.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class RiderDetailRequest implements Serializable {
    private String policyno;
    private String certno;
    private String type;

}
