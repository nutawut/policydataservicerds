package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class RiderDetailM {
  private String ridertype;
  private Integer ridersum;
  private Integer riderpremium;
  private String riderstatus;
  private String riderstatusdate;
  private String effectivedate;
  private Integer extrapremium;
}
