package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class PartyPersonM {
    public PartyPersonM() {}
    public PartyPersonM(String party_id, String pname_th, String fname_th, String lname_th, String pname_en, String fname_en,
            String lname_en, String mar_stat, String blood_group, String gender, String birth_date, String citizen,
            String race, String religion, String smoker_stat, String height, String weight,
            String highest_education_level, String car_type, String create_user_code, String create_time,
            String update_user_code, String last_update, String system_id, String system_key) {
        super();
        this.party_id = party_id;
        this.pname_th = pname_th;
        this.fname_th = fname_th;
        this.lname_th = lname_th;
        this.pname_en = pname_en;
        this.fname_en = fname_en;
        this.lname_en = lname_en;
        this.mar_stat = mar_stat;
        this.blood_group = blood_group;
        this.gender = gender;
        this.birth_date = birth_date;
        this.citizen = citizen;
        this.race = race;
        this.religion = religion;
        this.smoker_stat = smoker_stat;
        this.height = height;
        this.weight = weight;
        this.highest_education_level = highest_education_level;
        this.car_type = car_type;
        this.create_user_code = create_user_code;
        this.create_time = create_time;
        this.update_user_code = update_user_code;
        this.last_update = last_update;
        this.system_id = system_id;
        this.system_key = system_key;
    }
    private String party_id="";              
    private String pname_th="";              
    private String fname_th="";              
    private String lname_th="";              
    private String pname_en="";              
    private String fname_en="";              
    private String lname_en="";              
    private String mar_stat="";              
    private String blood_group="";           
    private String gender="";                
    private String birth_date="";            
    private String citizen="";               
    private String race="";                  
    private String religion="";              
    private String smoker_stat="";           
    private String height="";                
    private String weight="";                
    private String highest_education_level;
    private String car_type="";              
    private String create_user_code="";      
    private String create_time="";           
    private String update_user_code="";      
    private String last_update="";           
    private String system_id="";             
    private String system_key="";
    
    
    
}
