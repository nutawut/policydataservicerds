package com.thailife.api.policyInfoService.model.subModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Getter
@Setter
@ToString
public class MinorNameM {
    private String customer_id;
    private String prename;
    private String firstname;
    private String lastname;
    private Integer total_policy;
    private List<MinorPolicyM> list_of_policy;
}
