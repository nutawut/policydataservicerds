package com.thailife.api.policyInfoService.model.response;

import lombok.Data;

@Data
public class DividendResponse {
	
	private String dividendType;
    private String year;
    private String date;
    private Double amount;
    private Double interest;
    private String payFlag;
    private String policyType;
    private String branch;
    private String payFlagStatus;
    private String payFlagStatusDesc;
	
}
