package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class Policy {

	private String policyno;
	private String certno;
	private String plancode;
	private String planname;
	private String type;
	private String screentype;
	private String effectivedate;
	private String maturedate;
	private String policystatusdate1;
	private String policystatus1;
	private String policystatus1desc;
	private String statusdisplay;
	private Number sum;
	private String prename;
	private String firstname;
	private String lastname;
	private String nextduedate;
	private Number ridersum;
	private String GroupPolicyStatus;
	private String oldCertNo;
	private String oldPolicyNo;
	private String payperiod;
}
