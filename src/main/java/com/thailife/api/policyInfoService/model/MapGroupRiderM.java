package com.thailife.api.policyInfoService.model;

import java.util.ArrayList;

public class MapGroupRiderM {
  private ArrayList<String> listridercode = new ArrayList<>();
  private ArrayList<Integer> listyear = new ArrayList<>();

  public ArrayList<String> getListridercode() {
    return listridercode;
  }

  public void setListridercode(ArrayList<String> listridercode) {
    this.listridercode = listridercode;
  }

  public ArrayList<Integer> getListyear() {
    return listyear;
  }

  public void setListyear(ArrayList<Integer> listyear) {
    this.listyear = listyear;
  }
}
