package com.thailife.api.policyInfoService.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
@Getter
@Setter
@ToString
public class GuardianM {
    private String policyno;
    private String birthdate;
    private String firstname;
    private String hivflag;
    private Timestamp journaltimestamp;
    private String lastname;
    private String parentage;
    private String parentclass;
    private String parentidno;
    private String parentsex;
    private String prename;
    private Timestamp recordtimestamp;
    private String reserve;
}
