package com.thailife.api.policyInfoService.model.response;

import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@ToString
public class GetDividendInfoListResponse {

  private List<GetDividendInfoResponse> list_of_benefit;
  private BigDecimal benefit_accrued;

  public List<GetDividendInfoResponse> getList_of_benefit() {
    return list_of_benefit;
  }

  public void setList_of_benefit(List<GetDividendInfoResponse> list_of_benefit) {
    this.list_of_benefit = list_of_benefit;
  }

  public BigDecimal getBenefit_accrued() {
    return benefit_accrued;
  }

  public void setBenefit_accrued(BigDecimal benefit_accrued) {
    this.benefit_accrued = benefit_accrued;
  }
}
