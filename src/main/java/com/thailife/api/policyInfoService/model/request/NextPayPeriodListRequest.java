package com.thailife.api.policyInfoService.model.request;

import java.util.List;

public class NextPayPeriodListRequest {
  private List<NextPayPeriodRequest> NextPayPeriod;

  public List<NextPayPeriodRequest> getNextPayPeriod() {
    return NextPayPeriod;
  }

  public void setNextPayPeriod(List<NextPayPeriodRequest> nextPayPeriod) {
    NextPayPeriod = nextPayPeriod;
  }
}
