package com.thailife.api.policyInfoService.model;

import java.math.BigDecimal;

public class CheckRiderM {
	private String ridercode;
	private String ridername;
	private String ridersortname;
	private BigDecimal ridersum;
	private BigDecimal riderpremium;
	private String riderstatus;
	private String riderstatusdesc;
	private String riderstatusdate;
	private String statusdisplay;
	private String effectivedate;
	private BigDecimal extrariderpremium;
	private String flag = "F";

	public String getRidercode() {
		return ridercode;
	}

	public void setRidercode(String ridercode) {
		this.ridercode = ridercode;
	}

	public String getRidername() {
		return ridername;
	}

	public void setRidername(String ridername) {
		this.ridername = ridername;
	}

	public String getRidersortname() {
		return ridersortname;
	}

	public void setRidersortname(String ridersortname) {
		this.ridersortname = ridersortname;
	}

	public BigDecimal getRidersum() {
		return ridersum;
	}

	public void setRidersum(BigDecimal ridersum) {
		this.ridersum = ridersum;
	}

	public BigDecimal getRiderpremium() {
		return riderpremium;
	}

	public void setRiderpremium(BigDecimal riderpremium) {
		this.riderpremium = riderpremium;
	}

	public String getRiderstatus() {
		return riderstatus;
	}

	public void setRiderstatus(String riderstatus) {
		this.riderstatus = riderstatus;
	}

	public String getRiderstatusdesc() {
		return riderstatusdesc;
	}

	public void setRiderstatusdesc(String riderstatusdesc) {
		this.riderstatusdesc = riderstatusdesc;
	}

	public String getRiderstatusdate() {
		return riderstatusdate;
	}

	public void setRiderstatusdate(String riderstatusdate) {
		this.riderstatusdate = riderstatusdate;
	}

	public String getStatusdisplay() {
		return statusdisplay;
	}

	public void setStatusdisplay(String statusdisplay) {
		this.statusdisplay = statusdisplay;
	}

	public String getEffectivedate() {
		return effectivedate;
	}

	public void setEffectivedate(String effectivedate) {
		this.effectivedate = effectivedate;
	}

	public BigDecimal getExtrariderpremium() {
		return extrariderpremium;
	}

	public void setExtrariderpremium(BigDecimal extrariderpremium) {
		this.extrariderpremium = extrariderpremium;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

}
