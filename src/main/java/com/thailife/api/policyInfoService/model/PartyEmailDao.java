package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class PartyEmailDao {
	public PartyEmailDao() {}
	
    public PartyEmailDao(String party_id, String email_id, String start_date, String end_date, String is_attach_allow,
			String is_deliverable, String create_user_code, String create_time, String update_user_code,
			String last_update, String system_id, String system_key) {
		super();
		this.party_id = party_id;
		this.email_id = email_id;
		this.start_date = start_date;
		this.end_date = end_date;
		this.is_attach_allow = is_attach_allow;
		this.is_deliverable = is_deliverable;
		this.create_user_code = create_user_code;
		this.create_time = create_time;
		this.update_user_code = update_user_code;
		this.last_update = last_update;
		this.system_id = system_id;
		this.system_key = system_key;
	}

	public String party_id = "";        
    public String email_id = "";        
    public String start_date = "";      
    public String end_date = "";        
    public String is_attach_allow = ""; 
    public String is_deliverable = "";  
    public String create_user_code = "";
    public String create_time = "";     
    public String update_user_code = "";
    public String last_update = "";     
    public String system_id = "";       
    public String system_key = "";
}
