package com.thailife.api.policyInfoService.model.response;

import com.thailife.api.policyInfoService.model.subModel.MarkToMarketM;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Getter
@Setter
@ToString
public class UKMarkToMarketResponse {
    private String policyNo;
    private String summaryInvestValue;
    private String summaryUnrealizeValue;
    private String summaryPercentGainLoss;
    private List<MarkToMarketM> listMarkToMarket;
}
