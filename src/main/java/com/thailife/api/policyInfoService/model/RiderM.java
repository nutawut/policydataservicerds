package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class RiderM {

  private String ridercode;
  private String ridername;
  private String ridersortname;
  private int ridersum;
  private int riderpremium;
  private String riderstatus;
  private String riderstatusdesc;
  private String riderstatusdate;
  private String statusdisplay;
  private String effectivedate;
  private int extrariderpremium;
  private int label_status;
  private String riderstatuscore;
  private String riderstatusdesccore;
}
