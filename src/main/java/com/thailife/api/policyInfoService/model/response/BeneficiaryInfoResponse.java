package com.thailife.api.policyInfoService.model.response;

import lombok.Data;

@Data
public class BeneficiaryInfoResponse {
	private String sequence;
	private String prename;
	private String firstname;
	private String lastname;
	private String relationship;
	private Float percentshare;
	private String mainbeneficiary;

}
