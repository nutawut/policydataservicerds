package com.thailife.api.policyInfoService.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CoverageDetailM {
    private String coverageGroupCode;
    private String coverageNameDetailTh;
    private String coverageNameDetailEn;
    private BigDecimal coverageSumDetail = new BigDecimal(0);
    private String coverageRemarkTh;
    private String coverageRemarkEn;
}
