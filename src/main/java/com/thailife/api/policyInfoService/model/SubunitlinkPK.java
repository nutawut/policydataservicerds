package com.thailife.api.policyInfoService.model;

import java.io.Serializable;

/**
 * The primary key class for the subunitlink database table.
 * 
 */
public class SubunitlinkPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String policyno;

	private String premiumtype;

	public SubunitlinkPK(String policyno, String premiumtype) {
		super();
		this.policyno = policyno;
		this.premiumtype = premiumtype;
	}
	public SubunitlinkPK() {
	}
	public String getPolicyno() {
		return this.policyno;
	}
	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}
	public String getPremiumtype() {
		return this.premiumtype;
	}
	public void setPremiumtype(String premiumtype) {
		this.premiumtype = premiumtype;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SubunitlinkPK)) {
			return false;
		}
		SubunitlinkPK castOther = (SubunitlinkPK)other;
		return 
			this.policyno.equals(castOther.policyno)
			&& this.premiumtype.equals(castOther.premiumtype);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.policyno.hashCode();
		hash = hash * prime + this.premiumtype.hashCode();
		
		return hash;
	}
}