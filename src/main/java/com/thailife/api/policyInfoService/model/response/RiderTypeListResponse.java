package com.thailife.api.policyInfoService.model.response;

import java.util.List;

public class RiderTypeListResponse {
	
	private List<RiderTypeResponse> list_rider;

	public List<RiderTypeResponse> getList_rider() {
		return list_rider;
	}

	public void setList_rider(List<RiderTypeResponse> list_rider) {
		this.list_rider = list_rider;
	}

}
