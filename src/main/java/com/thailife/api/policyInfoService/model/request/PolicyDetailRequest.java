package com.thailife.api.policyInfoService.model.request;

import java.io.Serializable;
import lombok.Data;

@Data
public class PolicyDetailRequest implements Serializable{/**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String policyno;
  private String certno;
  private String type;
  
}
