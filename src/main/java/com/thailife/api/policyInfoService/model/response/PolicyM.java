package com.thailife.api.policyInfoService.model.response;

import lombok.Data;

@Data
public class PolicyM {
  private String policyNo;
  private String certNo;
  private String policyType;
  private String flag;
  private String planName;
}
