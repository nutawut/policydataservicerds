package com.thailife.api.policyInfoService.model.response;

import com.thailife.api.policyInfoService.model.CheckPolicyM;
import lombok.Data;


import java.util.List;

@Data
public class CheckPolicyRiderLifefitResponse {
	private List<CheckPolicyM> list_of_policy;
}
