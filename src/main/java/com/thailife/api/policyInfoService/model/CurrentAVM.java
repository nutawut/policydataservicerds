package com.thailife.api.policyInfoService.model;

import java.math.BigDecimal;
import lombok.Data;
@Data
public class CurrentAVM {
	private String code;
	private String name;
	private String navdate;
	private BigDecimal nav;
	private BigDecimal unit;
	private BigDecimal av;
}
