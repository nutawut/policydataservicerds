package com.thailife.api.policyInfoService.model.request;

public class NextPayPeriodRequest {
  private String policyno;

  private String certno;

  private String type;

  public String getPolicyno() {
    return policyno;
  }

  public void setPolicyno(String policyno) {
    this.policyno = policyno;
  }

  public String getCertno() {
    return certno;
  }

  public void setCertno(String certno) {
    this.certno = certno;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "NextPayPeriodRequest{"
        + "policyno='"
        + policyno
        + '\''
        + ", certno='"
        + certno
        + '\''
        + ", type='"
        + type
        + '\''
        + '}';
  }
}
