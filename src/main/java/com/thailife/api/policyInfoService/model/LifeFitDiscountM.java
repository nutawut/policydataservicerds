package com.thailife.api.policyInfoService.model;

public class LifeFitDiscountM {
	private String rider_code;
	private int premium;
	private int discount;

	public String getRider_code() {
		return rider_code;
	}

	public void setRider_code(String rider_code) {
		this.rider_code = rider_code;
	}

	public int getPremium() {
		return premium;
	}

	public void setPremium(int premium) {
		this.premium = premium;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

}
