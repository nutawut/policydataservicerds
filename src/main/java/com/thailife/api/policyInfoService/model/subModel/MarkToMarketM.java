package com.thailife.api.policyInfoService.model.subModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MarkToMarketM {
    private String fundCode;
    private String fundName;
    private String unitAmount;
    private String unitPrice;
    private String unrealizeValue;
    private String gainLoss;
    private String percentGainLoss;
    private String investValue;
    private String averagePrice;

}
