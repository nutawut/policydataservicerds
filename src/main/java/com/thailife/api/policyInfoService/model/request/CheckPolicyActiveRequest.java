package com.thailife.api.policyInfoService.model.request;

public class CheckPolicyActiveRequest {
    private String citizenId;

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    @Override
    public String toString() {
        return "CheckPolicyActiveRequest{" +
                "citizenId='" + citizenId + '\'' +
                '}';
    }
}
