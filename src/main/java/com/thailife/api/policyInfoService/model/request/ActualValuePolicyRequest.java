package com.thailife.api.policyInfoService.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ActualValuePolicyRequest {
    private String policyNo;
    private String type;
}
