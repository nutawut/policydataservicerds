package com.thailife.api.policyInfoService.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PolicyOnlyM {

  private String policyno;
  private String certno;
  private String oldCertNo;
  private String oldPolicyNo;
}
