package com.thailife.api.policyInfoService.model.response;

import java.sql.SQLException;
import java.util.List;

public class ServiceResultsList<T> {

	public static final String SYS_UNEXPECTED_EXCEPTION = "SYS_00001";
	// SQL_00002=No record affected.
	public static final String SQL_NO_RECORD_AFFECTED = "SQL_00002";
	// SQL_00003=Record already exist.
	public static final String SQL_RECORD_ALREADY_EXIST = "SQL_00003";
	// SQL_00004=Empty result.
	public static final String SQL_EMPTY_RESULT = "SQL_00004";

	private List<T> resultList;
	private boolean success = false;
	private String responseCode;
	private String responseDescription;
	private Throwable throwable;
	private String etc1;
	private String etc2;
	private String etc3;

	public ServiceResultsList() {

	}

	public ServiceResultsList(String errorCode, Throwable throwable) {
		if (throwable instanceof SQLException) {
			SQLException sqlException = (SQLException) throwable;
			this.responseCode = "" + sqlException.getErrorCode();
		} else {
			this.responseCode = errorCode;
		}
		this.success = false;
		this.throwable = throwable;
		this.responseDescription = throwable.getLocalizedMessage();
	}

	public ServiceResultsList(Throwable throwable) {
		if (throwable instanceof SQLException) {
			SQLException sqlException = (SQLException) throwable;
			this.responseCode = "" + sqlException.getErrorCode();
		} else {
			this.responseCode = SYS_UNEXPECTED_EXCEPTION;
		}
		this.success = false;
		this.throwable = throwable;
		this.responseDescription = throwable.getLocalizedMessage();
	}

	public ServiceResultsList(boolean b) {
		this.success = b;
	}

	public ServiceResultsList(List<T> resultList) {
		this.resultList = resultList;
		this.success = true;
	}

	public List<T> getResult() {
		return resultList;
	}

	public void setResult(List<T> resultList) {
		this.resultList = resultList;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
		this.success = false;
	}

	@Override
	public String toString() {
		return "ServiceResult [result=" + resultList + ", success=" + success + ", responseCode=" + responseCode
				+ ", responseDescription=" + responseDescription + ", throwable=" + throwable + "]";
	}

	public String getEtc1() {
		return etc1;
	}

	public void setEtc1(String etc1) {
		this.etc1 = etc1;
	}

	public String getEtc2() {
		return etc2;
	}

	public void setEtc2(String etc2) {
		this.etc2 = etc2;
	}

	public String getEtc3() {
		return etc3;
	}

	public void setEtc3(String etc3) {
		this.etc3 = etc3;
	}

}
