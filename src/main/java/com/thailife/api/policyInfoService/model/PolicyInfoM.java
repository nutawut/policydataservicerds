package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class PolicyInfoM {

  private String policy_no;
  private String pname_th;
  private String fname_th;
  private String lname_th;
  private String birth_date;
}
