package com.thailife.api.policyInfoService.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CertRiderM {

	private String policyno;
	private String certno;
	private String ridertype;
	private String sum;
	private String premium;
	private String extrapremium;
	private String riderstatus;
	private String riderstatusdate;
	private String effectivedate;
	private String reserve;
	private String recordtimestamp;
	private String journaltimestamp;

}
