package com.thailife.api.policyInfoService.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thailife.api.policyInfoService.model.CoverageDetailM;
import lombok.Data;

import java.util.ArrayList;

@Data
public class GetCoverageOverviewResponse {
    @JsonProperty(value = "coveragename")
    private String coverageName;
    @JsonProperty(value = "coveragecode")
    private String coverageCode;
    @JsonProperty("coveragesum")
    private long coverageSum;
    @JsonProperty(value = "coveragestartdate")
    private String coverageStartDate;
    @JsonProperty(value = "coverageenddate")
    private String coverageEndDate;
    @JsonProperty(value = "coverageremain")
    private long coverageRemain;
    @JsonProperty(value = "coverageused")
    private long coverageUsed;
    private ArrayList<CoverageDetailM> coverageDetail = new ArrayList<>();
}
