package com.thailife.api.policyInfoService.model.response;

import java.math.BigDecimal;
import lombok.Data;
@Data
public class GetDividendInfoResponse {
	private Integer benefit_year;
	private BigDecimal benefit_premium;
	private String benefit_effective_date;
	private BigDecimal benefit_amount_receive;
	private BigDecimal benefit_interest;
	private String benefit_receivedstatus;
	private String benefit_type;
	private String benefit_channel_receive;
	private String benefit_status;
	private String benefit_statusdesc;
	
}
