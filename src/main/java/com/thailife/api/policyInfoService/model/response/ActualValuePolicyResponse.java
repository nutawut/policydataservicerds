package com.thailife.api.policyInfoService.model.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ActualValuePolicyResponse {
    private Double rpAv;
    private Double rspAv;
    private Double tpAv;
    private Double totalAv;
}
