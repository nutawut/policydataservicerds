package com.thailife.api.policyInfoService.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonM {
  public String personid;
  public String referenceid;
  public String referencetype;
  public String birthdate;
  public String sex;
  public String customerid;
  public String bluecard;
  public String reserve;
  public String recordtimestamp;
  public String journaltimestamp;
}
