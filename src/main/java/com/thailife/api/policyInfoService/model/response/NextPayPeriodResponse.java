package com.thailife.api.policyInfoService.model.response;

import com.thailife.api.policyInfoService.model.LifeFitDiscountM;

import java.math.BigDecimal;
import java.util.List;

public class NextPayPeriodResponse {
	private String nextduedate;
	private String payperiod;
	private BigDecimal lifepremium;
	private BigDecimal riderpremium;
	private String interestrate;
	private int lifefit_lifepremium_discount;
	private int lifefit_riderpremium_discount;
	private String lifefitflag;
//	private String lifefitdiscount;
//	private String lifefitflag;
	private String type;
	private BigDecimal totalpremium;
	private String paydesc;
	private String premiumflag;
	private String overdueflag;
	
	
	private String extralifepremium;
	private String extrariderpremium;
	private String mode;
	private String modedesc;
	private String rsppremium;
	private String tppremium;
	private List<LifeFitDiscountM> lifefitdiscountlist;
	private String flagPayment;

	//add20210303
	private String plancode;
	private String planname;
	private String nextduedate2;

	public String getPaydesc() {
		return paydesc;
	}
	public void setPaydesc(String paydesc) {
		this.paydesc = paydesc;
	}
	public String getPremiumflag() {
		return premiumflag;
	}
	public void setPremiumflag(String premiumflag) {
		this.premiumflag = premiumflag;
	}
	public String getOverdueflag() {
		return overdueflag;
	}
	public void setOverdueflag(String overdueflag) {
		this.overdueflag = overdueflag;
	}
	public String getNextduedate() {
		return nextduedate;
	}
	public void setNextduedate(String nextduedate) {
		this.nextduedate = nextduedate;
	}
	public String getPayperiod() {
		return payperiod;
	}
	public void setPayperiod(String payperiod) {
		this.payperiod = payperiod;
	}
	public BigDecimal getLifepremium() {
		return lifepremium;
	}
	public void setLifepremium(BigDecimal lifepremium) {
		this.lifepremium = lifepremium;
	}
	public BigDecimal getRiderpremium() {
		return riderpremium;
	}
	public void setRiderpremium(BigDecimal riderpremium) {
		this.riderpremium = riderpremium;
	}
	public String getInterestrate() {
		return interestrate;
	}
	public void setInterestrate(String interestrate) {
		this.interestrate = interestrate;
	}
	public String getLifefitflag() {
		return lifefitflag;
	}
	public void setLifefitflag(String lifefitflag) {
		this.lifefitflag = lifefitflag;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BigDecimal getTotalpremium() {
		return totalpremium;
	}
	public void setTotalpremium(BigDecimal totalpremium) {
		this.totalpremium = totalpremium;
	}
	public String getExtralifepremium() {
		return extralifepremium;
	}
	public void setExtralifepremium(String extralifepremium) {
		this.extralifepremium = extralifepremium;
	}
	public String getExtrariderpremium() {
		return extrariderpremium;
	}
	public void setExtrariderpremium(String extrariderpremium) {
		this.extrariderpremium = extrariderpremium;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getModedesc() {
		return modedesc;
	}
	public void setModedesc(String modedesc) {
		this.modedesc = modedesc;
	}
	public String getRsppremium() {
		return rsppremium;
	}
	public void setRsppremium(String rsppremium) {
		this.rsppremium = rsppremium;
	}
	public String getTppremium() {
		return tppremium;
	}
	public void setTppremium(String tppremium) {
		this.tppremium = tppremium;
	}
	public int getLifefit_lifepremium_discount() {
		return lifefit_lifepremium_discount;
	}
	public void setLifefit_lifepremium_discount(int lifefit_lifepremium_discount) {
		this.lifefit_lifepremium_discount = lifefit_lifepremium_discount;
	}
	public int getLifefit_riderpremium_discount() {
		return lifefit_riderpremium_discount;
	}
	public void setLifefit_riderpremium_discount(int lifefit_riderpremium_discount) {
		this.lifefit_riderpremium_discount = lifefit_riderpremium_discount;
	}
	public List<LifeFitDiscountM> getLifefitdiscountlist() {
		return lifefitdiscountlist;
	}
	public void setLifefitdiscountlist(List<LifeFitDiscountM> lifefitdiscountlist) {
		this.lifefitdiscountlist = lifefitdiscountlist;
	}
	public String getFlagPayment() {
		return flagPayment;
	}
	public void setFlagPayment(String flagPayment) {
		this.flagPayment = flagPayment;
	}

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public String getPlanname() {
		return planname;
	}

	public void setPlanname(String planname) {
		this.planname = planname;
	}

	public String getNextduedate2() {
		return nextduedate2;
	}

	public void setNextduedate2(String nextduedate2) {
		this.nextduedate2 = nextduedate2;
	}
}
