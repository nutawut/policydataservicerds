package com.thailife.api.policyInfoService.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class CheckPolicyM {
  private String policyno;
  private String plancode;
  private String planname;
  private String effectivedate;
  private String maturedate;
  private String policystatusdate1;
  private String policystatus1;
  private String policystatus1desc;
  private String statusdisplay;
  private BigDecimal sum;
  private String prename;
  private String firstname;
  private String lastname;
  private String nextduedate;
  private BigDecimal ridersum;
  private String screentype;
  private String flag = "F";
  private List<CheckRiderM> list_of_rider;
}
