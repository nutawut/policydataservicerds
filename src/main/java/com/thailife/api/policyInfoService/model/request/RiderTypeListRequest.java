package com.thailife.api.policyInfoService.model.request;

import java.util.ArrayList;
import java.util.List;

public class RiderTypeListRequest {
	
	private List<RiderTypeRequest> RiderTypeList = new ArrayList<RiderTypeRequest>();

	public List<RiderTypeRequest> getRiderTypeList() {
		return RiderTypeList;
	}

	public void setRiderTypeList(List<RiderTypeRequest> riderTypeList) {
		RiderTypeList = riderTypeList;
	}

}
