package com.thailife.api.policyInfoService.model.response;

import java.util.List;

public class NextPayPeriodListResponse {
	
	private List<NextPayPeriodResponse> nextPayPeriodList;

	public List<NextPayPeriodResponse> getNextPayPeriodList() {
		return nextPayPeriodList;
	}

	public void setNextPayPeriodList(List<NextPayPeriodResponse> nextPayPeriodList) {
		this.nextPayPeriodList = nextPayPeriodList;
	}


	
}
