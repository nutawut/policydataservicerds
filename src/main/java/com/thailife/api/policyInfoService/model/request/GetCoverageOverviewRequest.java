package com.thailife.api.policyInfoService.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GetCoverageOverviewRequest {
  @JsonProperty(value = "policyno")
  private String policyNo;
  @JsonProperty(value = "certno")
  private String certNo;
  @JsonProperty(value = "customer_id")
  private String customerId;
}
