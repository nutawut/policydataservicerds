package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class TempGetCoverageOverviewM {
  private String group;
  private String type;
  private String strDate;
  private String endDate;
  private int coverage = 0;
  private int creditLit = 0;
}
