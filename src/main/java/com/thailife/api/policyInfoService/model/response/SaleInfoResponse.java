package com.thailife.api.policyInfoService.model.response;

import lombok.Data;

@Data
public class SaleInfoResponse {
	private String prename;
	private String firstname;
	private String lastname;
	private String telephone;
	private String licenseno;
}
