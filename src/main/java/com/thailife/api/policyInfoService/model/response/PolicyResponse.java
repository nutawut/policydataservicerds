package com.thailife.api.policyInfoService.model.response;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
@Data
public class PolicyResponse {
	
	private String policyno;
	private String certno;
	private String plancode;
	private String planname;
	private String type;
	private String nextduedate;
	private BigDecimal totalpremium;
	
	
	
}
