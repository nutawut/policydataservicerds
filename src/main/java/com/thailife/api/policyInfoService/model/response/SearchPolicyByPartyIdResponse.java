package com.thailife.api.policyInfoService.model.response;

import com.thailife.api.policyInfoService.model.PolicyOnlyM;
import lombok.Data;

import java.util.List;
@Data
public class SearchPolicyByPartyIdResponse {
  private List<PolicyOnlyM> list_of_policy;
}
