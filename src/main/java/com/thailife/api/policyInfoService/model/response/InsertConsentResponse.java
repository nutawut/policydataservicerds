package com.thailife.api.policyInfoService.model.response;

public class InsertConsentResponse {
	private String link;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
