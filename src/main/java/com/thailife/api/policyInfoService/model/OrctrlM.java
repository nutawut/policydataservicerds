package com.thailife.api.policyInfoService.model;

import lombok.Data;

@Data
public class OrctrlM {

  private String rpno;
  private String policyno;
  private String effectivedate;
  private String payperiod;
  private String paydate;
  private Integer premium;
  private Integer extraprem;
  private String sysdate;
  private String currentstatus;
  private String originalstatus;
  private String mode;
  private String time;
  private String requestdate;
  private String submitno;
  private String graceperiod;
  private String printeddate;
  private String submitbranch;
  private String userid;
  private String reasoncode;
  private String moneyok;
  private String recordtimestamp;
  private String journaltimestamp;
}
