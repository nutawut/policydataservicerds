package com.thailife.api.policyInfoService.model.response;

import java.io.Serializable;
import java.util.List;
import com.thailife.api.policyInfoService.model.CustomerM;
import lombok.Data;
@Data
public class SearchConsentResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<PolicyM> policy;
	private CustomerM customer;

	

}
