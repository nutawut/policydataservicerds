package com.thailife.api.policyInfoService.model.request;

import lombok.Data;

@Data
public class PaymentHistoryRequest {
	private String policyno;
	private String certno;
	private String type;
	private String effectivedate;
}
