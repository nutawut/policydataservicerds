package com.thailife.api.policyInfoService.model.request;

import lombok.Data;

@Data
public class GetLoanPremumRequest {
	private String policyno;
	private String certno;
	private String type;
}
