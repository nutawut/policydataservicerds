package com.thailife.api.policyInfoService.model.response;

public class RiderTypeResponse {
	
	private String riderType;
	private String riderName;
	private String riderShortName;
	
	public String getRiderType() {
		return riderType;
	}
	public void setRiderType(String riderType) {
		this.riderType = riderType;
	}
	public String getRiderName() {
		return riderName;
	}
	public void setRiderName(String riderName) {
		this.riderName = riderName;
	}
	public String getRiderShortName() {
		return riderShortName;
	}
	public void setRiderShortName(String riderShortName) {
		this.riderShortName = riderShortName;
	}
	
}
