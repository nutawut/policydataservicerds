package com.thailife.api.policyInfoService.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import insure.PlanType;
import utility.rteutility.PublicRte;
import utility.support.Branch;
import utility.underwrite.BankAssure;

public class ProductType {
	public static Map<String, String> map;
	public static Map<String, String> screenType;
	public static Map<String, String> Type;

	public static String typeGetTable(String type) {
		type = type.toUpperCase();
		map = new HashMap<String, String>();
		map.put("AG-OL-ANNUITY", "ordmast");
		map.put("AG-OL-ENDOWNMENT", "ordmast");
		map.put("AG-OL-TERM", "ordmast");
		map.put("AG-OL-HIGHSAVING", "ordmast");
		map.put("AG-OL-ANTICIPATED", "ordmast");
		map.put("AG-OL-WHOLE", "ordmast");
		map.put("AG-OL-SINGLE", "ordmast");
		map.put("AG-OL-OLDAGE", "whlmast");
		map.put("AG-OL-IND", "indmast");
		map.put("AG-OL-PA", "ordmast");
		map.put("AG-OL-MORTGAGE", "ordmast");
		map.put("AG-OL-UL", "universallife");
		map.put("AG-ULIP-ULIP", "unitlink");
		map.put("AG-ULIP-ULIP", "unitlink");
		map.put("AG-OL_TAKAFUL-ENDOWNMENT", "ordmast");
		map.put("AG-OL_TAKAFUL-WHOLE", "ordmast");
		map.put("AG-OL-LIFEFIT", "ordmast");
		map.put("CB-OL-ANNUITY", "ordmast");
		map.put("CB-OL-ENDOWNMENT", "ordmast");
		map.put("CB-OL-GIO", "ordmast");
		map.put("CB-OL-TERM", "ordmast");
		map.put("CB-OL-WHOLE", "ordmast");
		map.put("CB-OL-OLDAGE", "whlmast");
		map.put("CB-OL-PARTNERTERM", "mortgage");
		map.put("CB-CL-MORTGAGE", "mortgage");
		map.put("CB-CL-PERSONALLOAN", "mortgage");
		map.put("CB-CL-ODPN", "mortgage");
		map.put("GB-OL-GIO", "ordmast");
		map.put("GB-OL_TAKAFUL-GIO", "ordmast");
		map.put("GB-OL_TAKAFUL-ENDOWNMENT", "ordmast");
		map.put("GB-OL_TAKAFUL-WHOLE", "ordmast");
		map.put("GB-CL-MORTGAGE", "mortgage");
		map.put("GB-CL_TAKAFUL-MORTGAGE", "mortgage");
		map.put("LS-CL-LEASING", "mortgage");
		map.put("CF-OL-ENDOWNMENT", "ordmast");
		map.put("CF-OL-GIO", "ordmast");
		map.put("CF-OL-TERM", "ordmast");
		map.put("CF-CL-TERM", "mortgage");
		map.put("DM-OL-ENDOWNMENT", "ordmast");
		map.put("DM-OL-SENIOR", "ordmast");
		map.put("DM-OL-ANNUITY", "ordmast");
		map.put("DM-OL-PA", "ordmast");
		map.put("CL","mortgage");
		map.put("CL-TAKAFUL","mortgage");
		map.put("ULIP","unitlink");
		map.put("DM-OL-OL", "ordmast");
		map.put("AG-UL-UL", "universallife");
		String result = map.get(type);
		if (result == null) {
			if (type.contains("-OL-"))
				result = "ordmast";
			if (type.contains("-IND-"))
				result = "indmast";
			if (type.contains("-WHL-"))
				result = "whlmast";
			if (type.contains("-ULIP-"))
				result = "unitlink";
			if (type.contains("-OL_TAKAFUL-"))
				result = "ordmast";
			if (type.contains("-CL-"))
				result = "mortgage";
			if (type.contains("-CL_TAKAFUL-"))
				result = "mortgage";
			if (type.contains("-UL-"))
				result = "universallife";
		}
		return result;
	}

	public static String typeGetScreenType(String type,String policytype,String plancode) {
		type = type.toUpperCase();
		screenType = new HashMap<>();
		screenType.put("AG-OL-ANNUITY", "OL");
		screenType.put("AG-OL-ENDOWNMENT", "OL");
		screenType.put("AG-OL-TERM", "OL");
		screenType.put("AG-OL-HIGHSAVING", "OL");
		screenType.put("AG-OL-ANTICIPATED", "OL");
		screenType.put("AG-OL-WHOLE", "OL");
		screenType.put("AG-OL-SINGLE", "OL");
		screenType.put("AG-OL-OLDAGE", "OL");
		screenType.put("AG-OL-IND", "OL");
		screenType.put("AG-OL-PA", "OL");
		screenType.put("AG-OL-MORTGAGE", "OL");
		screenType.put("AG-OL-UL", "OL");
		screenType.put("AG-ULIP-ULIP", "ULIP");
		screenType.put("AG-ULIP-ULIP", "ULIP");
		screenType.put("AG-OL_TAKAFUL-ENDOWNMENT", "OL-TAKAFUL");
		screenType.put("AG-OL_TAKAFUL-WHOLE", "OL-TAKAFUL");
		screenType.put("AG-OL-LIFEFIT", "OL");
		screenType.put("CB-OL-ANNUITY", "OL");
		screenType.put("CB-OL-ENDOWNMENT", "OL");
		screenType.put("CB-OL-GIO", "OL");
		screenType.put("CB-OL-TERM", "OL");
		screenType.put("CB-OL-WHOLE", "OL");
		screenType.put("CB-OL-OLDAGE", "OL");
		screenType.put("CB-OL-PARTNERTERM", "OL");
		screenType.put("CB-CL-MORTGAGE", "CL");
		screenType.put("CB-CL-PERSONALLOAN", "CL");
		screenType.put("CB-CL-ODPN", "CL");
		screenType.put("GB-OL-GIO", "OL");
		screenType.put("GB-OL-PERSONALLOAN", "OL");
		screenType.put("GB-OL_TAKAFUL-GIO", "OL-TAKAFUL");
		screenType.put("GB-OL_TAKAFUL-ENDOWNMENT", "OL-TAKAFUL");
		screenType.put("GB-OL_TAKAFUL-WHOLE", "OL-TAKAFUL");
		screenType.put("GB-CL-MORTGAGE", "CL");
		screenType.put("GB-CL_TAKAFUL-MORTGAGE", "CL-TAKAFUL");
		screenType.put("LS-CL-LEASING", "CL");
		screenType.put("CF-OL-ENDOWNMENT", "OL");
		screenType.put("CF-OL-GIO", "OL");
		screenType.put("CF-OL-TERM", "OL");
		screenType.put("CF-CL-TERM", "CONSUMER-FINANCE");
		screenType.put("DM-OL-ENDOWNMENT", "OL");
		screenType.put("DM-OL-SENIOR", "OL");
		screenType.put("DM-OL-ANNUITY", "OL");
		screenType.put("DM-OL-PA", "OL");
		screenType.put("OL", "OL");
		screenType.put("OL_TAKAFUL", "OL-TAKAFUL");
		screenType.put("CL", "CL");
		screenType.put("CL_TAKAFUL", "CL-TAKAFUL");
		screenType.put("ULIP", "ULIP");
		screenType.put("OTHER", policytype);
		String screen = screenType.get(type);
		if(screen==null)
			screen = type.split("-")[1].replace("_", "-");
		if(isOL(screen))
			screen = "OL";
		if(PlanType.isConsumerFinanceMRTAPlan(plancode))
			screen = "CONSUMER-FINANCE";
		return screen;
//		String[] typeS = type.split("-");
//		if(type.toUpperCase().contains("TAKAFUL"))
//		{
//			return typeS[1]+"-TAKAFUL";
//		}		
//		else if(typeS[1].toUpperCase().equals("ULIP") )
//		{
//			return "ULIP";
//		}
//		else if(typeS[1].toUpperCase().equals("CL"))
//		{
//			if(typeS[0].toUpperCase().equals("CF"))
//				return "CONSUMER-FINANCE";
//			else
//				return "CL";
//		}
//		return "OL";
	}
	public static boolean isOL(String type) {
		List<String> list = Arrays.asList("IND", "WHL", "UL");
		return list.contains(type);
}
	public static boolean isType(String type) {
		List<String> list = Arrays.asList("OL", "OL-TAKAFUL", "CL", "CL-TAKAFUL", "ULIP", "AG-OL-ANNUITY",
				"AG-OL-ENDOWNMENT", "AG-OL-TERM", "AG-OL-HIGHSAVING", "AG-OL-ANTICIPATED", "AG-OL-WHOLE",
				"AG-OL-SINGLE", "AG-OL-OLDAGE", "AG-OL-IND", "AG-OL-PA", "AG-OL-MORTGAGE", "AG-OL-UL", "AG-ULIP-ULIP",
				"AG-ULIP-ULIP", "AG-OL_TAKAFUL-ENDOWNMENT", "AG-OL_TAKAFUL-WHOLE", "AG-OL-LIFEFIT", "CB-OL-ANNUITY",
				"CB-OL-ENDOWNMENT", "CB-OL-GIO", "CB-OL-TERM", "CB-OL-WHOLE", "CB-OL-OLDAGE", "CB-OL-PARTNERTERM",
				"CB-CL-MORTGAGE", "CB-CL-PERSONALLOAN", "CB-CL-ODPN", "GB-OL-GIO", "GB-OL-PERSONALLOAN",
				"GB-OL_TAKAFUL-GIO", "GB-OL_TAKAFUL-ENDOWNMENT", "GB-OL_TAKAFUL-WHOLE", "GB-CL-MORTGAGE",
				"GB-CL-PERSONALLOAN", "GB-CL_TAKAFUL-MORTGAGE", "LS-CL-LEASING", "CF-OL-ENDOWNMENT", "CF-OL-GIO",
				"CF-OL-TERM", "CF-CL-TERM", "DM-OL-ENDOWNMENT", "DM-OL-SENIOR", "DM-OL-ANNUITY", "DM-OL-PA");
		return list.equals(type);
	}
	public static boolean isOther(String type) {
		List<String> list = Arrays.asList("OL", "OL-TAKAFUL", "CL", "CL-TAKAFUL", "ULIP","OTHER");
		return list.contains(type);
	}
	public static String plancodeGetType(String planCode, String branch,String policytype,String basetype) {
//		AG	"1. ต้องผ่านการ Check (DM,CB,GB,LS,CF) มาก่อน
//			 2. isNonMed1 หรือ isNonMed2"
//		CB	isADC
//		GB	isGOV
//		LS	isTLP
//		CF	isConsumerFinance
//		DM	isDM
		String channel = getChannelType(branch);
		String maintype = getPolicyType(planCode, basetype);
		String subtype = getSubType(planCode,policytype);  
		String type = channel+maintype+subtype; 
		System.out.println("type>>"+type);
		return type;
	}

	public static String getChannelType(String branch) {
		PublicRte.setRemote(true);
//		CB = เข้าไปดูว่า Branch Code อยู่ใน TLICommercialPartner หรือไม่ ?
//				   CF = เข้าไปดูว่า Branch Code อยู่ใน TLIConsumerFinancePartner หรือไม่ ?
//				   LS = เข้าไปดูว่า Branch Code อยู่ใน TLILeasingPartner หรือไม่ ?
//				   GB = เข้าไปดูว่า Branch Code อยู่ใน TLIGovernmentPartner หรือไม่ ?
		
		String channel = "OTHER";
		if (Branch.isDM(branch)) // DM isDM
			channel = "DM";
		else if(Branch.isConsumerFinance(branch))
			channel = "CF";
		else if (Branch.isADC(branch)) // CB isADC
		{
			if (isCB(branch))
				channel = "CB";
			else if (isCF(branch))
				channel = "CF";
			else if (isLS(branch))
				channel = "LS";
			else if (isGB(branch))
				channel = "GB";
		} else
			channel = "AG";
		
		channel = channel.equals("") ? "" : channel + "-";
		return channel;
	}
	public static boolean isCB(String check) {
		List<String> list = new ArrayList<>();
		for (String[] arr : BankAssure.TLICommercialPartner()) {
			for (String branch : arr) {
				list.add(branch);
			}
		}
		return list.contains(check);
	}
	public static boolean isCF(String check) {
		List<String> list = new ArrayList<>();
		for (String[] arr : BankAssure.TLIConsumerFinancePartner()) {
			for (String branch : arr) {
				list.add(branch);
			}
		}
		return list.contains(check);
	}
	public static boolean isLS(String check) {
		List<String> list = new ArrayList<>();
		for (String[] arr : BankAssure.TLILeasingPartner()) {
			for (String branch : arr) {
				list.add(branch);
			}
		}
		return list.contains(check);
	}
	public static boolean isGB(String check) {
		List<String> list = new ArrayList<>();
		for (String[] arr : BankAssure.TLIGovernmentPartner()) {
			for (String branch : arr) {
				list.add(branch);
			}
		}
		return list.contains(check);
	}
	public static String getPolicyType(String planCode,String basetype) {
		String type = basetype;
		if (PlanType.isULIP(planCode))
			type = "ULIP";
		else if (PlanType.isTakafulCL(planCode))
			type = "CL_TAKAFUL";
		else if (PlanType.isIslamPlan(planCode))
			type = "OL_TAKAFUL";
		type = type.equals("")?"":type+"-";
		return type;
	}
	public static String getSubType(String planCode,String policytype) {
		String type = policytype;
		if (PlanType.isPensionPlan(planCode))
			type = "ANNUITY";
		else if (PlanType.isEndowmentPlan(planCode))
			type = "ENDOWNMENT";
		else if (PlanType.isGIOPlan(planCode))
			type = "GIO";
		else if (PlanType.isTermPlan(planCode))
			type = "TERM";
		else if (PlanType.isUL(planCode))
			type = "UL";
		else if (PlanType.isULIP(planCode))
			type = "ULIP";
		else if (PlanType.isULIPSingle(planCode))
			type = "ULIP";
		else if (PlanType.isLifeFit(planCode))
			type = "LIFEFIT";
		else if (PlanType.isWholeLifePlan(planCode))
			type = "WHOLE";
		else if (PlanType.isConsumerFinanceOLPlan(planCode))
			type = "PARTNERTEAM";
		else if (PlanType.isBankMRTA(planCode))
			type = "MORTGAGE";
		else if (PlanType.isTakafulCL(planCode))
			type = "MORTGAGE";
		else if (PlanType.isLeasingPlan(planCode))
			type = "LEASING";
		else if (PlanType.isSeniorPlan(planCode))
			type = "SENIOR";
		else if (PlanType.isPAPlan(planCode))
			type = "PA";
		else if (PlanType.isHighSavingPlan(planCode))
			type = "HIGHSAVING";
		else if (PlanType.isSinglePremiumPlan(planCode))
			type = "SINGLE";
		else if (PlanType.isOldAgePlan(planCode))
			type = "OLDAGE";
		else if (PlanType.isIndustrialPlan(planCode))
			type = "IND";
		return type;
	}
	public static void main(String[] args) {
//		CB = เข้าไปดูว่า Branch Code อยู่ใน TLICommercialPartner หรือไม่ ?
//				   CF = เข้าไปดูว่า Branch Code อยู่ใน TLIConsumerFinancePartner หรือไม่ ?
//				   LS = เข้าไปดูว่า Branch Code อยู่ใน TLILeasingPartner หรือไม่ ?
//				   GB = เข้าไปดูว่า Branch Code อยู่ใน TLIGovernmentPartner หรือไม่ ?
		PublicRte.setRemote(true);
		System.out.println("CB");
		for (String[] string : BankAssure.TLICommercialPartner()) {
//			System.out.println("length=="+string.length);
			for (String string2 : string) {
				System.out.println(string2);
			}
		}
		
		System.out.println("CF");
		for (String[] string : BankAssure.TLIConsumerFinancePartner()) {
//			System.out.println("length=="+string.length);
			for (String string2 : string) {
				System.out.println(string2);
			}
		}
		
		System.out.println("LS");
		for (String[] string : BankAssure.TLILeasingPartner()) {
//			System.out.println("length=="+string.length);
			for (String string2 : string) {
				System.out.println(string2);
			}
		}
		
		System.out.println("GB");
		for (String[] string : BankAssure.TLIGovernmentPartner()) {
//			System.out.println("length=="+string.length);
			for (String string2 : string) {
				System.out.println(string2);
			}
		}
	}
}
