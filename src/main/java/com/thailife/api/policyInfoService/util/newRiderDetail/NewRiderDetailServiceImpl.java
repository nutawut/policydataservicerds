//package com.thailife.api.policyInfoService.util.newRiderDetail;
//
//
//import com.thailife.api.policyInfoService.model.PolicyDetailM;
//import com.thailife.api.policyInfoService.model.RiderDetailM;
//import com.thailife.api.policyInfoService.model.RiderM;
//import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
//import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
//import com.thailife.api.policyInfoService.util.Log;
//import com.thailife.api.policyInfoService.util.PolicyException;
//import com.thailife.api.policyInfoService.util.ProductType;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//
//public class NewRiderDetailServiceImpl {
//	private static final Log log = new Log();
//	public static String className = "NewRiderDetailServiceImpl";
//
//	public static RiderDetialResponse getResponse(Serializable req) throws PolicyException {
//		log.printInfo(className, "getResponse [START]");
//		RiderDetialResponse response = new RiderDetialResponse();
//		RiderDetailRequest request = (RiderDetailRequest) req;
//		String policyNo = request.getPolicyno();
//		String certNo = request.getCertno();
//		String type = request.getType();
//		try {
//			String tableType = ProductType.typeGetTable(type);
//			NewRiderDetailResponseController controller = new NewRiderDetailResponseController(policyNo, certNo, tableType);
//			ArrayList<RiderDetailM> riders = controller.getRiderData();
//			System.out.println("Debug :: riders = " + riders.toString());
//			PolicyDetailM policyDetail = controller.getPolicyDetail();
//			System.out.println("Debug :: policyDetail = " + policyDetail.toString());
//			ArrayList<RiderM> list;
//			if (riders.isEmpty() && !type.contains("CL")) {
//				throw new PolicyException("riders empty");
//			} else {
//				list = NewRiderDetailResponseController.setListRiderM(policyDetail, riders);
//			}
//			response.setList_of_rider(list);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new PolicyException("getResponse|policyNo:" + policyNo + " certNo:" + certNo + " type:" + type);
//		}
//		log.printInfo(className, "getResponse [END]");
//		return response;
//	}
//
//}
