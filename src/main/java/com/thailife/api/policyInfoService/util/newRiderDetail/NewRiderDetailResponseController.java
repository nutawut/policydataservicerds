package com.thailife.api.policyInfoService.util.newRiderDetail;

import com.thailife.api.policyInfoService.controller.pol05.GetRiderDetialController;
import com.thailife.api.policyInfoService.dao.impl.*;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.model.RiderDetailM;
import com.thailife.api.policyInfoService.model.RiderM;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.StatusDisplay;
import com.thailife.log.ExtLogger;
import insure.Insure;
import insure.PlanType;
import insure.RiderType;
import layout.bean.mstpolicy.PolicyoptionBean;
import manit.M;
import manit.rte.Result;
import master.master.MasterInquire;
import utility.claim.Benefit;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;
import utility.support.StatusInfo;

import java.sql.SQLException;
import java.util.*;

import static com.thailife.api.policyInfoService.controller.pol05.RiderDetailServiceImpl.policyStatus;
import static utility.support.StatusInfo.riderStatus;

public class NewRiderDetailResponseController {
	private static final ExtLogger log = ExtLogger.create(NewRiderDetailResponseController.class);

	private String className = "NewRiderDetailResponseController";
	private static String policyno;
	private static String certno;
	private static String tableType;
	private static final List<String> listPolicyStatus = Arrays.asList(new String[] { "A", "E", "R", "U" });
	private static final List<String> listRidercode = Arrays.asList(new String[] { "รพ", "ตรพ", "รพG", "รพL", "HA", "HA1", "HB", "HB1", "HB2", "HB3", "HB4", "HCG",
			"HCL", "HC", "HC1", "HC2", "HC3", "HC4", "HD", "HD1", "HI" });

	public NewRiderDetailResponseController(String policyno, String certno, String tableType) {
		this.policyno = policyno;
		this.certno = certno;
		this.tableType = tableType;
	}


	public ArrayList<RiderDetailM> getRiderData() throws Exception {
		log.info(className, "getRiderData [START]");
		ArrayList<RiderDetailM> list = new ArrayList<>();
		if (("mortgage").equals(tableType)) {
			if (PolicyUtil.validateString(certno)) {
				list = CertRiderDaoImpl.searchRiderDetailByPolicynoAndCertno(policyno, certno);
			} else {
				list = CertRiderDaoImpl.searchRiderDetailByPolicyno(policyno);
			}
		} else if (("ordmast").equals(tableType) || ("whlmast").equals(tableType)) {
			list = RiderDaoImpl.searchOLRiderDetailByPolicyno(policyno);
		} else if (("indmast").equals(tableType)) {
			list = IndmastDaoImpl.searchRiderDetailByPolicyno(policyno);
		} else if (("unitlink").equals(tableType)) {
			list = RiderDaoImpl.searchRiderDetailByPolicyno(policyno);
		} else if (("universallife").equals(tableType)) {
			list = UlRiderDaoImpl.searchRiderDetailByPolicyno(policyno);
		}
		log.info(className, "getRiderData [END]");
		return list;
	}

	public PolicyDetailM getPolicyDetail() throws SQLException {
		PolicyDetailM policyDetail = new PolicyDetailM();
		if (("mortgage").equals(tableType)) {
			policyDetail = CertDaoImpl.searchPolicyDetailByPolicynoAndCertno(policyno, certno);
		} else if (("ordmast").equals(tableType)) {
			policyDetail = OrdmastDaoImpl.searchPolicyDetailByPolicyno(policyno);
		} else if (("whlmast").equals(tableType)) {
			policyDetail = WhlmastDaoImpl.searchPolicyDetailByPolicyno(policyno);
		} else if (("indmast").equals(tableType)) {
			policyDetail = IndmastDaoImpl.searchPolicyDetailByPolicyno(policyno);
		} else if (("unitlink").equals(tableType)) {
			policyDetail = UnitLinkDaoImpl.searchPolicyDetailByPolicyno(policyno);
		} else if (("universallife").equals(tableType)) {
			policyDetail = UniversallifeDaoImpl.searchPolicyDetailByPolicyno(policyno);
		}
		return policyDetail;
	}

	public static ArrayList<RiderM> setListRiderM(PolicyDetailM policy, ArrayList<RiderDetailM> riders, String type)
			throws Exception {
		ArrayList<RiderM> list = new ArrayList<>();
		ArrayList<RiderM> activeList = new ArrayList<>();
		ArrayList<RiderM> inActiveList = new ArrayList<>();

		String policyStatus = policyStatus(policy);
		String riderCode, riderName, riderSortName;
		int riderSum, riderPremium, extraRiderPremium;
		String riderStatus, riderStatusDesc, riderStatusDate;
		String statusDisPlay, effectiveDate, riderGroup;
		String riderStatuscore, riderStatusDesccore;
		/* check riderPremium from RTE */
		String mode = policy.getMode();
		System.out.println("mode : " + mode);
		String nextPayPeriod = "";
		String policyType = PolicyUtil.getPolicyTypeByType(type);

		if ("O".equalsIgnoreCase(policyType)
				|| "U".equalsIgnoreCase(policyType)
				|| "L".equalsIgnoreCase(policyType)) {
			nextPayPeriod =
					mode.equals("")
							? policy.getPayperiod()
							: MasterInquire.nextPayPeriod(policy.getPayperiod(), mode, policyType);
			//              : "01";
			callRteMasterPremium(riders, policyno, nextPayPeriod, policy.getEffectivedate(), policyType);
		}


		if (null == riders || riders.isEmpty()) {
			if (policy.getTpdpremium().intValue() > 0) {
				riderCode = "TPD";
				riderName = "ทุพพลภาพถาวรสิ้นเชิง";
				riderSortName = "TPD";
				riderSum = policy.getLifesum().intValue();
				riderPremium = policy.getTpdpremium().intValue();
				// (จากโปรแกรมจะ Default เป็น N ตลอด แต่การเคลมต้องดูจากสถานะกรมธรรม์เป็นหลัก)
				riderStatus = riderStatus(policy.getPolicystatus1());

				riderStatusDesc = ("ไม่มีผลบังคับ").equals(policyStatus) ? "ไม่มีผลบังคับ" : "ปกติ";
				riderStatusDate = policy.getEffectivedate(); // (มีผล N พร้อมกับการเริ่มของสัญญาหลัก)
				statusDisPlay =
						("ไม่มีผลบังคับ").equals(policyStatus)
								? "ไม่มีผลบังคับ"
								: new StatusDisplay().getStatusDisplay("RIDER", riderStatus, "");
				effectiveDate =
						policy.getEffectivedate(); // CL : ไม่ได้ระบุวันเริ่มมีผลบังคับของสัญญาแนบท้าย
				extraRiderPremium = policy.getExtratpdpremium().intValue();
				riderGroup = PolicyUtil.getRiderCode(riderCode);

				list.add(
						setRider(
								riderCode,
								riderName,
								riderSortName,
								riderSum,
								riderPremium,
								riderStatus,
								riderStatusDesc,
								riderStatusDate,
								statusDisPlay,
								effectiveDate,
								extraRiderPremium,
								riderGroup));
			}
			if (policy.getAddpremium().intValue() > 0) {
				riderCode = "ADD";
				riderName = "การเสียชีวิต และการสูญเสียอวัยวะจากอุบัติเหตุ";
				riderSortName = "ADD";
				riderSum = policy.getAccidentsum().intValue();
				riderPremium = policy.getAddpremium().intValue();
				// (จากโปรแกรมจะ Default เป็น N ตลอด แต่การเคลมต้องดูจากสถานะกรมธรรม์เป็นหลัก)
				riderStatus = riderStatus(policy.getPolicystatus1());

				riderStatusDesc = ("ไม่มีผลบังคับ").equals(policyStatus) ? "ไม่มีผลบังคับ" : "ปกติ";
				riderStatusDate = policy.getEffectivedate(); // (มีผล N พร้อมกับการเริ่มของสัญญาหลัก)
				statusDisPlay =
						("ไม่มีผลบังคับ").equals(policyStatus)
								? "ไม่มีผลบังคับ"
								: new StatusDisplay().getStatusDisplay("RIDER", riderStatus, "");
				effectiveDate =
						policy.getEffectivedate(); // CL : ไม่ได้ระบุวันเริ่มมีผลบังคับของสัญญาแนบท้าย
				extraRiderPremium = 0;
				riderGroup = PolicyUtil.getRiderCode(riderCode);

				list.add(
						setRider(
								riderCode,
								riderName,
								riderSortName,
								riderSum,
								riderPremium,
								riderStatus,
								riderStatusDesc,
								riderStatusDate,
								statusDisPlay,
								effectiveDate,
								extraRiderPremium,
								riderGroup));
			}
		} else {
			for (RiderDetailM rider : riders) {
				riderCode = rider.getRidertype();
				if (caseIgnore(riderCode)) {
					continue;
				}
				riderName = RiderType.getFullName(RiderType.isIslam(riderCode), riderCode);
				if (listRidercode.contains(riderCode) && riderName.endsWith("วันละ")) {
					riderName = (riderName.substring(0, riderName.length() - 5)).trim();
				} else if (listRidercode.contains(riderCode) && riderName.endsWith("เวลา")) {
					riderName = (riderName.substring(0, riderName.length() - 4)).trim();
				}

				riderSortName = getShortName(policyno, riderCode, type);
				riderSum = calculateRiderSum(policy, riderCode, rider);
				log.access("Rider Status from db => " + rider.getRiderstatus());
				if ("S".equalsIgnoreCase(rider.getRiderstatus())
						|| "N".equalsIgnoreCase(rider.getRiderstatus())) {
					riderPremium = rider.getRiderpremium();
				} else {
					riderPremium = 0;
				}

				riderStatus =
						checkRiderStatus(
								tableType,
								rider.getRiderstatus(),
								policy.getPayperiod(),
								policy.getDuedate(),
								policy.getPolicystatus1());
				riderStatusDesc =
						("ไม่มีผลบังคับ").equals(policyStatus)
								? "ไม่มีผลบังคับ"
								: riderStatus(riderStatus);
				//        riderStatusDesc = StatusInfo.riderStatus(riderStatus);

				riderStatusDate = rider.getRiderstatusdate();
				statusDisPlay =
						("ไม่มีผลบังคับ").equals(policyStatus)
								? "ไม่มีผลบังคับ"
								: new StatusDisplay().getStatusDisplay("RIDER", riderStatus, "");
				//        statusDisPlay = new StatusDisplay().getStatusDisplay("RIDER", riderStatus, "");
				effectiveDate = rider.getEffectivedate();
				extraRiderPremium = rider.getExtrapremium();
				riderGroup = PolicyUtil.getRiderCode(riderCode);

				riderStatuscore = rider.getRiderstatus();
				riderStatusDesccore = riderStatus(riderStatuscore);
				RiderM riderM =
						setRider(
								riderCode,
								riderName,
								riderSortName,
								riderSum,
								riderPremium,
								riderStatus,
								riderStatusDesc,
								riderStatusDate,
								statusDisPlay,
								effectiveDate,
								extraRiderPremium,
								riderGroup);

				riderM.setRiderstatuscore(riderStatuscore);
				riderM.setRiderstatusdesccore(riderStatusDesccore);

				if (riderStatus.equalsIgnoreCase("T")) {
					inActiveList.add(riderM);
				} else {
					activeList.add(riderM);
				}
			}
			setAndSortRiderList(list, activeList);
			setAndSortRiderList(list, inActiveList);
		}
		return list;
	}
	private static void setAndSortRiderList(ArrayList<RiderM> list, ArrayList<RiderM> tmpList) {
		sortByRiderType(tmpList);
		list.addAll(tmpList);
	}
	private static void sortByRiderType(ArrayList<RiderM> list) {
		Collections.sort(
				list,
				new Comparator<RiderM>() {
					@Override
					public int compare(RiderM riderM, RiderM t1) {
						return riderM.getRidercode().compareTo(t1.getRidercode());
					}
				});
		//    list.sort(Comparator.comparing(RiderM::getRidercode));
	}
	private static RiderM setRider(
			String riderCode,
			String riderName,
			String riderSortName,
			int riderSum,
			int riderPremium,
			String riderStatus,
			String riderStatusDesc,
			String riderStatusDate,
			String statusDisPlay,
			String effectiveDate,
			int extraRiderPremium,
			String riderGroup) throws Exception {
		RiderM riderM = new RiderM();
		ExtractSetterRider(
				riderM,
				riderCode,
				riderName,
				riderSortName,
				riderPremium,
				riderStatus,
				riderStatusDesc,
				riderStatusDate,
				statusDisPlay,
				effectiveDate,
				extraRiderPremium,
				getRiderSumByRiderCode(riderGroup, riderCode, riderSum));
		riderM.setLabel_status(getLabelStatus(riderGroup, riderName, riderCode));
		return riderM;
	}
	static void ExtractSetterRider(
			RiderM riderM,
			String riderCode,
			String riderName,
			String riderSortName,
			int riderPremium,
			String riderStatus,
			String riderStatusDesc,
			String riderStatusDate,
			String statusDisPlay,
			String effectiveDate,
			int extraRiderPremium,
			int riderSumByRiderCode) {
		riderM.setRidercode(riderCode);
		riderM.setRidername(riderName);
		riderM.setRidersortname(riderSortName);
		riderM.setRidersum(riderSumByRiderCode);
		riderM.setRiderpremium(riderPremium);
		riderM.setRiderstatus(riderStatus);
		riderM.setRiderstatusdesc(riderStatusDesc);
		riderM.setRiderstatusdate(riderStatusDate);
		riderM.setStatusdisplay(statusDisPlay);
		riderM.setEffectivedate(effectiveDate);
		riderM.setExtrariderpremium(extraRiderPremium);
	}

	public static String checkRiderStatus(
			String tableType, String status, String payPeriod, String dueDate, String policyStatus) {
		if (status.equals("T") || status.equals("A")) {
			return status;
		}
		String gracePeriod = "";
		if (("ordmast").equalsIgnoreCase(tableType) || ("whlmast").equalsIgnoreCase(tableType)) {
			gracePeriod = M.nextdate(dueDate, 31);
		} else if (("indmast").equalsIgnoreCase(tableType)) {
			dueDate =
					payPeriod.equals("") || payPeriod.equals("0")
							? "00000000"
							: DateInfo.nextMonth(payPeriod + "01", 1);
			gracePeriod = M.nextdate(dueDate, 60);
		}
		if (policyStatus.equals("F") && DateInfo.sysDate().compareTo(gracePeriod) > 0) {
			status = "A";
		} else if (listPolicyStatus.contains(policyStatus)) {
			status = "A";
		} else if (status.equals("F")
				&& gracePeriod != null
				&& gracePeriod.trim().length() == 8
				&& M.cmps(gracePeriod, DateInfo.sysDate()) == 1) {
			status = "A";
		}
		log.access("Cal Rider Status => " + status);
		return status;
	}

	private static boolean caseIgnore(String ridercode) throws Exception {
		List<String> ignore = Arrays.asList(new String[] { "106", "107", "108" });
		if (ignore.contains(ridercode)) {
			return true;
		}
		return false;
	}

	public static int getRiderSumByRiderCode(String ridergroup, String ridercode, int riderSum) {
		return NewRiderDetailResponseController.ExtraGetRiderSumByRiderCode(ridergroup, ridercode, riderSum);
	}

	static int ExtraGetRiderSumByRiderCode(String riderGroup, String riderCode, int riderSum) {
		if (riderGroup.equals("R03")) {
			if (Benefit.getHealthCode(riderCode)) {
				return (int) (Double.parseDouble(Benefit.getRbBenefit(riderCode)));
			} else if (Benefit.getVpCode(riderCode)) {
				return (int) (Double.parseDouble(Benefit.getVpIpdRbBenefit(riderCode)));
			} else if (RiderType.isSP_RefundPlus(riderCode) || RiderType.isSPGoldPlus(riderCode)) {
				return (int) (Double.parseDouble(RiderType.getRoomExpense(riderCode)));
			} else {
				String riders = "0";
				String riderType = riderCode;
				if (RiderType.isAnySP(riderType)) // want sp plus show riderType from rider file
				{
					if (riderType.compareTo("S1K") == 0 || riderType.compareTo("H1K") == 0) riders = "10000";
					else if (RiderType.isSP_RefundPlus(riderType))
						riders = RiderType.getRoomExpense(riderType);
					else riders = M.multiply(riderType.substring(1), "100", 0);
				} else if (RiderType.isSPGoldPlus(riderType)
						|| RiderType.isHPGoldPlus(riderType)
						|| RiderType.isHealthFitDD(riderType)) {
					riders = RiderType.getRoomExpense(riderType);
				} else if (RiderType.isHealthFitUltra(riderType)) {
					riders = String.valueOf(riderSum);
				} else if (RiderType.isHealthFitVipDeduct(riderType)) {
					riders = RiderType.getRoomExpense(riderType);
				} else {
					System.out.println(
							"ZZ = " + riderType + " " + riders + " " + RiderType.isSPGold(riderType));
					if (riderType.charAt(0) == 'D'
							|| riderType.charAt(0) == 'C'
							|| riderType.compareTo("HA") == 0
							|| riderType.charAt(0) == 'T'
							|| riderType.compareTo("A03") == 0) riderType = Insure.riderMapping(riderType);
					if (RiderType.isSPGold(riderType)
							|| RiderType.isHealthFit99(riderType)
							|| RiderType.isHealthFit99RefundPlus(riderType)) {
						riders = RiderType.getRoomExpense(riderType);
					} else if (!riderType.contains(M.stou("คบ"))) riders = String.valueOf(riderSum);
				}
				riderSum = Integer.parseInt(riders);
			}
		}
		return riderSum;
	}

	private static int getLabelStatus(String ridergroup, String ridername, String ridercode) throws Exception {
		int group = 0;
		if (ridername.contains("ตะกาฟุล") || ridername.contains("หลักประกัน")) {
			group = 3;
		} else if (ridergroup.equals("R03")) {
			group = 1;
		} else if (ridergroup.equals("R04") || listRidercode.contains(ridercode)) {
			group = 2;
		}
		return group;
	}

	public static void callRteMasterPremium(
			ArrayList<RiderDetailM> riders,
			String policyNo,
			String payPeriod,
			String effDate,
			String policyType)
			throws Exception {
		String rteClass = "blmaster";
		String rtePath;
		String[] data;
		if ("O".equalsIgnoreCase(policyType)) {
			rtePath = "rte.bl.master.RteMasterPremium";
			data = new String[] {policyNo, payPeriod};
		} else if ("U".equalsIgnoreCase(policyType)) {
			rtePath = "rte.bl.universal.master.RteUniversalPremium";
			data = new String[] {policyNo, payPeriod, effDate};
		} else {
			rtePath = "rte.bl.unitlink.master.RteUnitLinkPremium";
			data = new String[] {policyNo, payPeriod, effDate};
		}
		log.info(
				String.format(
						"Call RTE =>[%n Host : %s,%n Domain : %s ,%n Param : %s%n]",
						rteClass, rtePath, Arrays.toString(data)));
		Vector<?> vrid = new Vector<>();
		try {
			Result rs = PublicRte.getResult(rteClass, rtePath, data);
			if (rs.status() == 0) {
				Vector<?> rv = (Vector<?>) rs.value();
				vrid = (Vector<?>) rv.elementAt(0);
			}
			for (RiderDetailM tmp : riders) {
				log.info("riderType from DB for match rte => " + tmp.getRidertype());
				getRiderPrem(vrid, tmp, tmp.getRidertype().trim(), policyType);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	public static void getRiderPrem(
			Vector<?> vRider, RiderDetailM rider, String riderType, String policyType) {
		for (int i = 0; i < vRider.size(); i++) {
			String[] ar = (String[]) vRider.elementAt(i);
			log.access("riderType: " + ar[0]);
			log.access("riderPremium: " + ar[1]);
			log.access("riderExtraPremium: " + ar[2]);
			if (!ar[0].equals(M.stou("ชว"))) {
				if ("O".equalsIgnoreCase(policyType)
						&& ((ar[0].compareTo("C15") == 0 && riderType.compareTo("C19") == 0)
						|| (ar[0].compareTo("C16") == 0 && riderType.compareTo("C20") == 0)
						|| (ar[0].compareTo("C08") == 0 && riderType.compareTo("C17") == 0)
						|| (ar[0].compareTo("C09") == 0 && riderType.compareTo("C18") == 0))) {
					rider.setRiderpremium(Integer.parseInt(ar[1]));
				}
				if (riderType.trim().compareTo(ar[0].trim()) == 0) {
					rider.setRiderpremium(Integer.parseInt(ar[1]));
				}
			}
		}
	}

	private static int calculateRiderSum(PolicyDetailM policy, String riderCode, RiderDetailM rider) {
		String planCode = policy.getPlanCode();
		String payPeriod = policy.getPayperiod();
		if (PlanType.isPAPlan(planCode)) {
			if ((riderCode.compareTo("DD") == 0 || riderCode.compareTo("DD4") == 0)
					&& !PlanType.isFixDD(planCode)
					&& M.ctoi(payPeriod.substring(0, 2)) > 1) {
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
				String yr = payPeriod.substring(0, 2);
				if (M.ctoi(yr) >= 6) {
					yr = "06";
				}
				System.out.println(
						">>>> "
								+ rider.getRidersum()
								+ " "
								+ M.dec(yr)
								+ " "
								+ M.multiply(M.dec(yr), "0.05", 2));
				String incSum =
						M.multiply(rider.getRidersum().toString(), M.multiply(M.dec(yr), "0.05", 2), 0);
				return Integer.parseInt(M.addnum(rider.getRidersum().toString(), incSum));
			}
		}
		return rider.getRidersum();
	}
	public static String getShortName(String policyNo, String riderCode, String type) {
		if (PolicyUtil.getPolicyTypeByType(type).equals("M")) {
			//      return RiderType.getShortName(riderCode, RiderType.isPARider(riderCode));
			return RiderType.getShortName(riderCode, PlanType.isMRTAPAPlan(policyNo));
		}
		String riderType = riderCode;
		if (RiderType.isAnySP(riderType)) // want sp plus show riderType from rider file
		{
			if (riderType.substring(0, 1).equals(M.stou("ภ"))
					|| riderType.substring(0, 1).equals(M.stou("ส"))
					|| riderType.substring(0, 1).equals(M.stou("พ"))) {
				if (!RiderType.isSPPlus(riderType)) {
					riderType = RiderType.getShortName(riderType);
				}
			} else {
				riderType = RiderType.getShortName(riderType);
			}
		} else if (RiderType.isSPGoldPlus(riderType)
				|| RiderType.isHPGoldPlus(riderType)
				|| RiderType.isHealthFitDD(riderType)) {
			String deduct = RiderType.getDeduct(riderType);
			riderType = RiderType.getShortName(riderType);
			if (!M.itis(deduct, '0')) riderType = riderType + "(D" + M.divide(deduct, "1000", 0) + ")";
		} else if (RiderType.isHealthFitUltra(riderType)) {
			String[] str = new String[] {policyNo, riderType, RiderType.OPTION_TYPE_HEALTH_FIT_ULTRA};
			Result res =
					PublicRte.getResult("blcunderwrite", "rte.bl.underwrite.master.RtePolicyOption", str);
			System.out.println(res.value());

			if (res.status() == 0) {
				Object[] obj = (Object[]) res.value();
				PolicyoptionBean policyoptionBean = (PolicyoptionBean) obj[0];
				if (policyoptionBean != null) {
					char zone = policyoptionBean.getOptionCode().charAt(0);
					String descZone = RiderType.getGeographicalZoneInShortDesc(zone);
					riderType = RiderType.getShortName(riderType);
					riderType = riderType + "(" + descZone + ")";
				}
			}
		} else {
			if (PolicyUtil.getPolicyTypeByType(type).equals("U")
					|| PolicyUtil.getPolicyTypeByType(type).equals("L")) {
				if (riderType.charAt(0) == 'D'
						|| riderType.charAt(0) == 'C'
						|| riderType.compareTo("HA") == 0
						|| riderType.charAt(0) == 'T'
						|| RiderType.isADB(riderType)) riderType = Insure.riderMapping(riderType);
			} else {
				if (riderType.charAt(0) == 'D'
						|| riderType.charAt(0) == 'C'
						|| riderType.compareTo("HA") == 0
						|| riderType.charAt(0) == 'T'
						|| riderType.compareTo("A03") == 0) riderType = Insure.riderMapping(riderType);
			}
			if (RiderType.isSPGold(riderType)) {
				riderType = Insure.riderMapping(riderType);
			}
		}
		return riderType;
	}

}
