package com.thailife.api.policyInfoService.util;

import org.springframework.stereotype.Component;

@Component
public class WebServiceMsg {
  protected String saveSuccess = "บันทึกข้อมูลสำเร็จ";
  protected String searchSuccess = "ดำเนินการเรียบร้อย";
  protected String customerNotFound = "ไม่พบข้อมูลในระบบ";
  protected String systemError = "ระบบขัดข้องกรุณาลองใหม่อีกครั้ง";
  protected String validateError = "ระบุข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง";
  protected String waitingBank = "ระบบขัดข้องกรุณาลองใหม่อีกครั้ง";

  protected String dataNotFound = "ไม่พบข้อมูลที่ต้องการในระบบ";
  protected String duplicateError = "duplicate";
  protected String success = "success";
  protected String error = "error";
  protected String incorrect = "incorrect data";
  protected String core01 = "ข้อมูลไม่ตรงในระบบ";

  protected String DataNotMatch = "การค้นหาไม่ถูกต้อง";

  protected String[][] message =
      new String[][] {
        {"200", searchSuccess},
        {"204", dataNotFound},
        {"500", systemError},
        {"401", validateError},
        // {"500", waitingBank},
        {"400", DataNotMatch},
        {"CORE-001", core01}
      };

  public String[][] getMessage() {
    return message;
  }

  public String[] getMessageByCode(String errorcode) {
    if (message != null) {
      for (String[] strings : message) {
        if (strings[0].equalsIgnoreCase(errorcode)) return strings;
      }
    }
    return new String[] {"", ""};
  }
}
