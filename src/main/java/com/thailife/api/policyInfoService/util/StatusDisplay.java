package com.thailife.api.policyInfoService.util;

import utility.tlcimbt.D;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class StatusDisplay {
  public Map<String, String> StatusDisplayOL;
  public Map<String, String> StatusDisplayCL;
  public Map<String, String> StatusDisplayRider;
  public Map<String, String> GroupPolicyStatusOL;
  public Map<String, String> GroupPolicyStatusCL;
  public Map<String, String> GroupPolicyStatusRider;
  public Map<String, String> policyDescOL;
  public Map<String, String> policyDescCL;
  public Map<String, String> policyDescRider;

  private String active = "มีผลบังคับ";
  private String inActive = "ไม่มีผลบังคับ";

  enum GroupPolicyStatusOL {
    A(1),
    B(1),
    C(2),
    D(2),
    E(1),
    F(1),
    G(2),
    H(1),
    I(1),
    J(1),
    K(2),
    L(3),
    M(2),
    N(1),
    O(3),
    P(1),
    R(1),
    S(2),
    T(2),
    U(1),
    V(3),
    W(1),
    X(2),
    Y(2),
    Z(3),
    SHARP(2),
    STAR(2);
    public int value;

    private GroupPolicyStatusOL(int value) {
      this.value = value;
    }
  }

  enum GroupPolicyStatusCL {
    C(2),
    D(2),
    I(1),
    L(3),
    M(2),
    N(1),
    S(2),
    T(1),
    V(2),
    W(2),
    X(2);
    public int value;

    private GroupPolicyStatusCL(int value) {
      this.value = value;
    }
  }

  enum GroupPolicyStatusRider {
    A(2),
    B(1),
    N(1),
    S(1),
    T(2),
    W(2),
    X(2),
    E(1);
    public int value;

    private GroupPolicyStatusRider(int value) {
      this.value = value;
    }
  }

  public StatusDisplay() {
    StatusDisplayOL = new HashMap<>();
    StatusDisplayOL.put("A", active);
    StatusDisplayOL.put("B", active);
    StatusDisplayOL.put("C", inActive);
    StatusDisplayOL.put("D", inActive);
    StatusDisplayOL.put("E", active);
    StatusDisplayOL.put("F", active);
    StatusDisplayOL.put("G", inActive);
    StatusDisplayOL.put("H", active);
    StatusDisplayOL.put("I", active);
    StatusDisplayOL.put("J", active);
    StatusDisplayOL.put("K", inActive);
    StatusDisplayOL.put("L", inActive);
    StatusDisplayOL.put("M", inActive);
    StatusDisplayOL.put("N", active);
    StatusDisplayOL.put("O", inActive);
    StatusDisplayOL.put("P", active);
    StatusDisplayOL.put("R", active);
    StatusDisplayOL.put("S", inActive);
    StatusDisplayOL.put("T", inActive);
    StatusDisplayOL.put("U", active);
    StatusDisplayOL.put("V", "ไม่ต้องแสดงข้อมูลกรมธรรม์");
    StatusDisplayOL.put("W", active);
    StatusDisplayOL.put("X", inActive);
    StatusDisplayOL.put("Y", inActive);
    StatusDisplayOL.put("Z", inActive);
    StatusDisplayOL.put("#", inActive);
    StatusDisplayOL.put("*", inActive);

    StatusDisplayCL = new HashMap<>();
    StatusDisplayCL.put("C", inActive);
    StatusDisplayCL.put("D", inActive);
    StatusDisplayCL.put("I", active);
    StatusDisplayCL.put("L", inActive);
    StatusDisplayCL.put("M", inActive);
    StatusDisplayCL.put("N", active);
    StatusDisplayCL.put("S", inActive);
    StatusDisplayCL.put("T", active);
    StatusDisplayCL.put("V", inActive);
    StatusDisplayCL.put("W", inActive);
    StatusDisplayCL.put("X", inActive);

    StatusDisplayRider = new HashMap<>();
    StatusDisplayRider.put("A", inActive);
    StatusDisplayRider.put("B", active);
    StatusDisplayRider.put("N", active);
    StatusDisplayRider.put("S", active);
    StatusDisplayRider.put("T", inActive);
    StatusDisplayRider.put("W", inActive);
    StatusDisplayRider.put("X", inActive);
    StatusDisplayRider.put("E", active);

    GroupPolicyStatusOL = new HashMap<>();
    GroupPolicyStatusOL.put("A", "1");
    GroupPolicyStatusOL.put("B", "1");
    GroupPolicyStatusOL.put("C", "2");
    GroupPolicyStatusOL.put("D", "2");
    GroupPolicyStatusOL.put("E", "1");
    GroupPolicyStatusOL.put("F", "1");
    GroupPolicyStatusOL.put("G", "2");
    GroupPolicyStatusOL.put("H", "1");
    GroupPolicyStatusOL.put("I", "1");
    GroupPolicyStatusOL.put("J", "1");
    GroupPolicyStatusOL.put("K", "2");
    GroupPolicyStatusOL.put("L", "2");
    GroupPolicyStatusOL.put("M", "2");
    GroupPolicyStatusOL.put("N", "1");
    GroupPolicyStatusOL.put("O", "2");
    GroupPolicyStatusOL.put("P", "1");
    GroupPolicyStatusOL.put("R", "1");
    GroupPolicyStatusOL.put("S", "2");
    GroupPolicyStatusOL.put("T", "2");
    GroupPolicyStatusOL.put("U", "1");
    GroupPolicyStatusOL.put("V", "2");
    GroupPolicyStatusOL.put("W", "1");
    GroupPolicyStatusOL.put("X", "2");
    GroupPolicyStatusOL.put("Y", "2");
    GroupPolicyStatusOL.put("Z", "2");
    GroupPolicyStatusOL.put("#", "2");
    GroupPolicyStatusOL.put("*", "2");

    GroupPolicyStatusCL = new HashMap<>();
    GroupPolicyStatusCL.put("C", "2");
    GroupPolicyStatusCL.put("D", "2");
    GroupPolicyStatusCL.put("I", "1");
    GroupPolicyStatusCL.put("L", "2");
    GroupPolicyStatusCL.put("M", "2");
    GroupPolicyStatusCL.put("N", "1");
    GroupPolicyStatusCL.put("S", "2");
    GroupPolicyStatusCL.put("T", "1");
    GroupPolicyStatusCL.put("V", "2");
    GroupPolicyStatusCL.put("W", "2");
    GroupPolicyStatusCL.put("X", "2");

    GroupPolicyStatusRider = new HashMap<>();
    GroupPolicyStatusRider.put("A", "2");
    GroupPolicyStatusRider.put("B", "1");
    GroupPolicyStatusRider.put("N", "1");
    GroupPolicyStatusRider.put("S", "1");
    GroupPolicyStatusRider.put("T", "2");
    GroupPolicyStatusRider.put("W", "2");
    GroupPolicyStatusRider.put("X", "2");
    GroupPolicyStatusRider.put("E", "1");

    policyDescOL = new HashMap<>();
    policyDescOL.put("A", "ปิดบัญชีขยายเวลาอัตโนมัติ");
    policyDescOL.put("B", "ผอป. ทุพพลภาพ");
    policyDescOL.put("C", "บังคับเวนคืน (ยังไม่ตัดเงินกู้จากแฟ้ม)");
    policyDescOL.put("D", "มรณกรรม");
    policyDescOL.put("E", "ปิดบัญชีขยายเวลาโดยการร้องขอ");
    policyDescOL.put("F", "ชำระครบ");
    policyDescOL.put("G", "ครบกำหนดส่งเข้ากองทุนฯ");
    policyDescOL.put("H", "Holiday (ULIP)");
    policyDescOL.put("I", active);
    policyDescOL.put("J", "กู้ชำระเบี้ยอัตโนมัติ (APL)");
    policyDescOL.put("K", "ครบสัญญา สำหรับ Term และ PA");
    policyDescOL.put("L", "ขาดผลบังคับ");
    policyDescOL.put("M", "รับเงินครบสัญญา");
    policyDescOL.put("N", "ไม่มีการจ่ายใดๆ");
    policyDescOL.put("O", "ขาดผลบังคับจากการขยายเวลา");
    policyDescOL.put("P", "เปลี่ยนแบบประกัน");
    policyDescOL.put("R", "ปิดบัญชีมูลค่าเงินสำเร็จอัตโนมัติ");
    policyDescOL.put("S", "เวนคืนกรมธรรม์");
    policyDescOL.put("T", "ครบกำหนด (ยังไม่รับเงิน)");
    policyDescOL.put("U", "ปิดบัญชีเงินสำเร็จโดยการร้องขอ");
    policyDescOL.put("V", "ข้อมูลไม่ถูกต้อง (invalid)");
    policyDescOL.put("W", "PA ที่บริษัทไม่รับปีต่อไป");
    policyDescOL.put("X", "บอกล้างสัญญา");
    policyDescOL.put("Y", "บังคับเวนคืน (ตัดเงินกู้จากแฟ้มแล้ว)");
    policyDescOL.put("Z", "ขาดผลบังคับเกิน 5 ปี");
    policyDescOL.put("#", "ยกเลิก Free Look Period");
    policyDescOL.put("*", "ยกเลิกอื่นๆ");

    policyDescCL = new HashMap<>();
    policyDescCL.put("C", "ยกเลิก");
    policyDescCL.put("D", "มรณกรรม");
    policyDescCL.put("I", active);
    policyDescCL.put("L", "ขาดผลบังคับ");
    policyDescCL.put("M", "ครบสัญญา");
    policyDescCL.put("N", "ไม่มีการจ่ายใดๆ");
    policyDescCL.put("S", "เวนคืนกรมธรรม์");
    policyDescCL.put("T", "ทุพพลภาพ");
    policyDescCL.put("V", "บอกล้างสัญญา");
    policyDescCL.put("W", "บอกเลิกสัญญา");
    policyDescCL.put("X", "บอกล้างสัญญา รปก");

    policyDescRider = new HashMap<>();
    policyDescRider.put("A", "สิ้นสุดสัญญา ตามระยะคุ้มครอง");
    policyDescRider.put("B", "ทุพพลภาพ");
    policyDescRider.put("N", "รับปกติ");
    policyDescRider.put("S", "รับต่ำกว่ามาตรฐาน");
    policyDescRider.put("T", "สิ้นสุดสัญญา ตามการร้องขอ");
    policyDescRider.put("W", "บอกเลิกสัญญา");
    policyDescRider.put("X", "บอกล้างสัญญา");
    policyDescRider.put("E", "งดเว้นการชำระเบี้ย");
  }

  public String statusDisplayOL(String status) {
    return StatusDisplayOL.get(status);
  }

  public String statusDisplayCL(String status) {
    return StatusDisplayCL.get(status);
  }

  public String statusDisplayRider(String status) {
    return StatusDisplayRider.get(status);
  }

  public String getStatusDisplay(String type, String status) {
    if (type.contains("OL")
        || type.contains("ULIP")
        || type.equalsIgnoreCase("OTHER")
        || type.contains("IND")
        || type.contains("WHL")
        || type.contains("ORD")
        || type.contains("UL")) {
      return StatusDisplayOL.get(status);
    } else if (type.contains("CL")) {
      return StatusDisplayCL.get(status);
    } else if (type.equals("RIDER")) {
      return StatusDisplayRider.get(status);
    }
    return null;
  }

  public String getStatusDisplay(String type, String status, String policyStatusDate2) {
    System.out.println(
            "Type : " + type + ", Status :" + status + ", PolicyStatusDate2 :" + policyStatusDate2);
    if (type.contains("OL")
            || type.contains("ULIP")
            || type.equalsIgnoreCase("OTHER")
            || type.contains("IND")
            || type.contains("WHL")
            || type.contains("ORD")
            || type.contains("UL")) {
      ArrayList<String> statusList = new ArrayList<>(Arrays.asList("A", "E", "R", "U"));
      if (statusList.contains(status)) {
        String today = PolicyUtil.nowTH();
        System.out.printf("Now Date : %s , PolicyStatusDate2 : %s%n", today, policyStatusDate2);
        if (PolicyUtil.validateString(policyStatusDate2)) {
          if (Integer.parseInt(today) >= Integer.parseInt(policyStatusDate2)) {
            return inActive;
          } else {
            return active;
          }
        }
      }
      return StatusDisplayOL.get(status);
    } else if (type.contains("CL")) {
      return StatusDisplayCL.get(status);
    } else if (type.equals("RIDER")) {
      return StatusDisplayRider.get(status);
    }
    return null;
  }

  public String getGroupPolicyStatus(String type, String status) {
    if (type.contains("OL")
        || type.contains("ULIP")
        || type.equalsIgnoreCase("OTHER")
        || type.contains("IND")
        || type.contains("WHL")
        || type.contains("ORD")
        || type.contains("UL")) {
      {
        System.out.println("case OL");
        System.out.println("return==" + GroupPolicyStatusOL.get(status));
        return GroupPolicyStatusOL.get(status);
      }
    } else if (type.contains("CL")) {
      System.out.println("return==" + GroupPolicyStatusCL.get(status));
      return GroupPolicyStatusCL.get(status);
    } else if (type.equals("RIDER")) {
      System.out.println("return==" + GroupPolicyStatusRider.get(status));
      return GroupPolicyStatusRider.get(status);
    }
    return null;
  }

  public String getPolicyDesc(String type, String status) {
    if (type.contains("OL")
        || type.contains("ULIP")
        || type.equalsIgnoreCase("OTHER")
        || type.contains("IND")
        || type.contains("WHL")
        || type.contains("ORD")
        || type.contains("UL")) {
      return policyDescOL.get(status);
    } else if (type.contains("CL")) return policyDescCL.get(status);
    else if (type.equals("RIDER")) return policyDescRider.get(status);
    return null;
  }

  public String getActiveOrInActive(String policyStatusDate2) {
    String nowDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
    if (!PolicyUtil.formatDateYYYYMMDD(policyStatusDate2)) return null;
    return Integer.parseInt(nowDate) <= Integer.parseInt(policyStatusDate2) ? active : inActive;
  }
}
