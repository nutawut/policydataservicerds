package com.thailife.api.policyInfoService.util;

public class DBproperties {
    public static String getDBPolicy() {
        return PolicyUtil.callProperties("DBPolicy");
    }

    public static String getDBPos() {
        return PolicyUtil.callProperties("DBPos");
    }

    public static String getDBCustomer() {
        return PolicyUtil.callProperties("DBCustomer");
    }

    public static String getDBClaim() {
        return PolicyUtil.callProperties("DBClaim");
    }

    public static String getDBUnitLink() {
        return PolicyUtil.callProperties("DBUnitlink");
    }

    public static String getDBCollection() {
        return PolicyUtil.callProperties("DBCollection");
    }

    public static String getDBBusProduct() {
        return PolicyUtil.callProperties("DBBusProduct");
    }

    public static String getDBSaleStruct() {
        return PolicyUtil.callProperties("DBSaleStruct");
    }

    public static String getSchemaMortgage() {
        return PolicyUtil.callProperties("SchemaMortgage");
    }

    public static String getSchemafnapaybank() {
        return PolicyUtil.callProperties("SchemaFnapaybank");
    }

    public static String getSchemaMstpolicy() {
        return PolicyUtil.callProperties("SchemaMstpolicy");
    }

    public static String getSchemaMstperson() {
        return PolicyUtil.callProperties("SchemaMstperson");
    }

    public static String getSchemaUniversal() {
        return PolicyUtil.callProperties("SchemaUniversal");
    }
    public static String getSchemaUnitlink() {
      return PolicyUtil.callProperties("SchemaUnitlink");
    }

}
