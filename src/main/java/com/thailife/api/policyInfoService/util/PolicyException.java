package com.thailife.api.policyInfoService.util;

public class PolicyException extends Exception {

	private static final long serialVersionUID = 1L;
	String code;
	String message;

	public PolicyException(String message) {
		this.message = message;
	}

	public PolicyException(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public PolicyException(PolicyException e) {
		this.code = e.getCode();
		this.message = e.getMessage();
	}

	public PolicyException(Exception e) {
		this.message = e.getMessage();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
