package com.thailife.api.policyInfoService.util;

import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
@Service
public class CallService {
  private final Log log = new Log();
  private final String className = "CallService";
//	final static Logger log = Logger.getLogger(CallService.class);

	public ApiConfigM readPoperties() throws IOException {
//		log.info("readPoperties");
		final File fo = new File(CallService.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		final String PATH = fo.getPath();
		final String configpath = PATH + "/api.config.properties";

		Properties prop = new Properties();
		try {
//			log.info(configpath);
			FileInputStream input = new FileInputStream(configpath);
			prop.load(input);
			ApiConfigM apiConfigM = new ApiConfigM();
			apiConfigM.setUrl_token(prop.getProperty("url_token"));
			apiConfigM.setConsumer_Key(prop.getProperty("Consumer_Key"));
			apiConfigM.setConsumer_Secret(prop.getProperty("Consumer_Secret"));
			apiConfigM.setGATEWAY_WS(prop.getProperty("GATEWAY_WS"));
			apiConfigM.setENDPOINT(prop.getProperty("ENDPOINT"));
			apiConfigM.setPARTY_ENDPOINT(prop.getProperty("PARTY_ENDPOINT"));
			apiConfigM.setCONSENT_KEY(prop.getProperty("Consent_Key"));
			apiConfigM.setCONSENT_SECRET(prop.getProperty("Consent_Secret"));
			return apiConfigM;
		} catch (IOException io) {
//			log.error(io.getMessage());
			throw io;
		}
	}

	public String callToken(String url_token, String Consumer_Key, String Consumer_Secret) throws Exception {
//		log.info("callToken=="+url_token+" Key="+Consumer_Key+" Secret="+Consumer_Secret);
		URL url = new URL(url_token);
		String request = "grant_type=client_credentials";
		request += "&client_id=" + Consumer_Key;
		request += "&client_secret=" + Consumer_Secret;
		byte[] postData = request.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		connection.setUseCaches(false);
		connection.setRequestProperty("charset", "utf-8");

		return callWS(connection, request);
	}

	public String callWSwithToken(String url_ws, String request, String authorization) throws Exception {
		URL url = new URL(url_ws);
		byte[] postData = request.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
//		log.info("callWSwithToken=="+url_ws);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Authorization", authorization);
		connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		connection.setUseCaches(false);
		connection.setRequestProperty("charset", "utf-8");

		return callWS(connection, request);
	}
	
	public String testcallWSwithToken(String url_ws, String request) throws Exception {
		System.out.println("req = "+ request);
		URL url = new URL(url_ws);
		byte[] postData = request.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		connection.setUseCaches(false);
		connection.setRequestProperty("charset", "utf-8");

		return callWS(connection, request);
	}

	public String callWS(String url_ws, String request) throws Exception {
		URL url = new URL(url_ws);
		byte[] postData = request.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/json");
//		connection.setRequestProperty("Authorization", authorization);
		connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		connection.setUseCaches(false);
		connection.setRequestProperty("charset", "utf-8");
		log.printDebug(className, "callWS url : " + url_ws);
		return callWS(connection, request);
	}

	private String callWS(HttpURLConnection connection, String request) throws Exception {
//		System.out.println("call service ...");
		OutputStream os = null;
		BufferedReader br = null;
		try {
			os = connection.getOutputStream();
			os.write(request.getBytes("UTF-8"));

			br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String tmp = "", output = "";
			while ((tmp = br.readLine()) != null)
				output += tmp;			
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK || connection.getResponseCode() == HttpURLConnection.HTTP_CREATED)
				;// donothing
			else {
				throw new Exception("Failed : HTTP error code : " + connection.getResponseCode() + "\n, Error : " + output);
			}
			return output;
		} catch (Exception e) {
		  e.printStackTrace();
//			log.error(e.getMessage());
			throw e;
		} finally {
			if (os != null)
				os.close();
			if (br != null)
				br.close();
			if (connection != null)
				connection.disconnect();
		}
	}

	public String getAuthorization(String text) throws Exception {
		JSONObject token = new JSONObject(text);
		String access_token = "";
		String token_type = "";
		if (token.has("access_token"))
			access_token = (String) token.get("access_token");
		if (token.has("token_type"))
			token_type = (String) token.get("token_type");
		return token_type + " " + access_token;
	}
}
