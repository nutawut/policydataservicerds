package com.thailife.api.policyInfoService.util;

public class ApiConfigM {
  private String consumer_Key;
  private String consumer_Secret;
  private String url_token;
  private String GATEWAY_WS;
  private String ENDPOINT;
  private String PARTY_ENDPOINT;
  private String CONSENT_KEY;
  private String CONSENT_SECRET;

  @Override
  public String toString() {
    return "ApiConfigM{" +
            "consumer_Key='" + consumer_Key + '\'' +
            ", consumer_Secret='" + consumer_Secret + '\'' +
            ", url_token='" + url_token + '\'' +
            ", GATEWAY_WS='" + GATEWAY_WS + '\'' +
            ", ENDPOINT='" + ENDPOINT + '\'' +
            ", PARTY_ENDPOINT='" + PARTY_ENDPOINT + '\'' +
            ", CONSENT_KEY='" + CONSENT_KEY + '\'' +
            ", CONSENT_SECRET='" + CONSENT_SECRET + '\'' +
            '}';
  }

  public String getConsumer_Key() {
    return consumer_Key;
  }

  public void setConsumer_Key(String consumer_Key) {
    this.consumer_Key = consumer_Key;
  }

  public String getConsumer_Secret() {
    return consumer_Secret;
  }

  public void setConsumer_Secret(String consumer_Secret) {
    this.consumer_Secret = consumer_Secret;
  }

  public String getUrl_token() {
    return url_token;
  }

  public void setUrl_token(String url_token) {
    this.url_token = url_token;
  }

  public String getGATEWAY_WS() {
    return GATEWAY_WS;
  }

  public void setGATEWAY_WS(String gATEWAY_WS) {
    GATEWAY_WS = gATEWAY_WS;
  }

  public String getENDPOINT() {
    return ENDPOINT;
  }

  public void setENDPOINT(String eNDPOINT) {
    ENDPOINT = eNDPOINT;
  }

  public String getPARTY_ENDPOINT() {
    return PARTY_ENDPOINT;
  }

  public void setPARTY_ENDPOINT(String pARTY_ENDPOINT) {
    PARTY_ENDPOINT = pARTY_ENDPOINT;
  }

  public String getCONSENT_KEY() {
    return CONSENT_KEY;
  }

  public void setCONSENT_KEY(String cONSENT_KEY) {
    CONSENT_KEY = cONSENT_KEY;
  }

  public String getCONSENT_SECRET() {
    return CONSENT_SECRET;
  }

  public void setCONSENT_SECRET(String cONSENT_SECRET) {
    CONSENT_SECRET = cONSENT_SECRET;
  }

}
