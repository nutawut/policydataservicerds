package com.thailife.api.policyInfoService.util;

import java.util.Arrays;

public class PolicyStatus {

	public static boolean isN1N2(String status1, String status2) {
		if (("N").equalsIgnoreCase(status1) && (("1").equals(status2) || ("2").equals(status2))) {
			return true;
		}
		return false;
	}

	public static boolean isI(String status) {
		if (("I").equalsIgnoreCase(status)) {
			return true;
		}
		return false;
	}

	public static boolean isF(String status) {
		if (("F").equalsIgnoreCase(status)) {
			return true;
		}
		return false;
	}

	public static boolean isB(String status) {
		if (("B").equalsIgnoreCase(status)) {
			return true;
		}
		return false;
	}

	public static boolean isIFB(String status) {
		if (("I").equalsIgnoreCase(status) || ("F").equalsIgnoreCase(status) || ("B").equalsIgnoreCase(status)) {
			return true;
		}
		return false;
	}

	public static boolean isInactivePolicyOl(String status1, String status2){
		if(Arrays.asList("D","G","K","P","V","X","Z","#","*").contains(status1.toUpperCase()))
			return true;

		if(status2 != null && !status2.equals("") && Arrays.asList("D","G").contains(status2.toUpperCase()))
			return true;

		return false;
	}
}
