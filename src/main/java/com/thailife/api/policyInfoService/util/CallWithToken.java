package com.thailife.api.policyInfoService.util;

public class CallWithToken {
	public static String CSVApplication(String context, String request) throws Exception {
		try {
			CallService callService = new CallService();
			ApiConfigM apiConfigM = callService.readPoperties();
			System.out.println("GATEWAY_WS==" + apiConfigM.getGATEWAY_WS());
			String json_token = callService.callToken(apiConfigM.getGATEWAY_WS() + apiConfigM.getUrl_token(),
					apiConfigM.getConsumer_Key(), apiConfigM.getConsumer_Secret());
			String authorization = callService.getAuthorization(json_token);
			if (authorization.trim().isEmpty())
				throw new Exception("Not found token");
			String response = callService.callWSwithToken(apiConfigM.getGATEWAY_WS() + context, request, authorization);
			return response;
		} catch (Exception e) {
			System.out.println("CSVApplicationError : " + e);
			throw new Exception("CSVApplication|context:" + context + " request:" + request, e);
		}
	}
	
	public static String Consent(String context, String request) throws Exception {
		try {
			CallService callService = new CallService();
			ApiConfigM apiConfigM = callService.readPoperties();
			System.out.println("apiConfigM.getGATEWAY_WS() : " + apiConfigM.getGATEWAY_WS());
			System.out.println("apiConfigM.getUrl_token() : " + apiConfigM.getUrl_token());
			System.out.println("apiConfigM.getCONSENT_KEY() : " + apiConfigM.getConsumer_Key());
			System.out.println("apiConfigM.getCONSENT_SECRET() : " + apiConfigM.getConsumer_Secret());
			String json_token = callService.callToken(apiConfigM.getGATEWAY_WS() + apiConfigM.getUrl_token(), apiConfigM.getCONSENT_KEY(), apiConfigM.getCONSENT_SECRET());
			System.out.println("json_token : " + json_token);
			String authorization = callService.getAuthorization(json_token);
			if (authorization.trim().isEmpty())
				throw new Exception("Not found token");
			String response = callService.callWSwithToken(apiConfigM.getGATEWAY_WS() + context, request, authorization);
			return response;
		} catch (Exception e) {
			System.out.println("Consent : " + e);
			throw new Exception("Consent|context:" + context + " request:" + request, e);
		}
	}

	public static void main(String[] args) throws Exception {
		String context = "/interfaceCsv/rest/calculate/policyPremium/1.0";
		String req = "{\n" + "    \"headerData\": {\n" + "        \"messageId\": \"3fe0a3c63603b7ea\",\n"
				+ "        \"sentDateTime\": \"03-11-2017 09:08:18.066\"\n" + "        },\n"
				+ "     \"requestRecord\": {\n" + "     }\n" + "}";
		String res = CallWithToken.CSVApplication(context, req);
		System.out.println(res);
		System.out.println("done.");
	}
}
