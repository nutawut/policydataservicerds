package com.thailife.api.policyInfoService.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.TransactionRequest;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import lombok.extern.log4j.Log4j2;
import org.json.JSONArray;
import org.json.JSONObject;
import utility.grouplife.mortgage.Data;
import utility.personnel.support.ZArray;
import utility.rteutility.PublicRte;
import utility.support.StatusPolicy;

import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.chrono.ThaiBuddhistChronology;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;

@Log4j2
public class PolicyUtil {

  public static ResponseStatus responseStatusSuccess() {
    return newResponseStatus("200", "S", "ดำเนินการเรียบร้อย");
  }

  public static ResponseStatus responseValidateError() {
    return newResponseStatus("400", "E", "การค้นหาไม่ถูกต้อง");
  }

  public static ResponseStatus responseDataNotFound() {
    return newResponseStatus("204", "E", "ไม่พบข้อมูลที่ต้องการในระบบ");
  }

  public static HeaderData headerDataResponse(HeaderData headerData) {

    if (headerData == null) headerData = new HeaderData();
    headerData.setResponseDateTime(DateFormat.now(DateFormat.DDMMYYYY_HHMMSS));
    return headerData;
  }

  public static ResponseStatus responseInternalServerError() {
    return newResponseStatus("500", "E", "ระบบขัดข้องกรุณาลองใหม่อีกครั้ง");
  }

  public static ResponseStatus newResponseStatus(
      String statusCode, String errorCode, String errorMessage) {
    return new ResponseStatus(errorCode, statusCode, errorMessage);
  }

  public static String callProperties(String key) {
    String value = "";
    // try (InputStream input =
    // PolicyUtil.class.getClassLoader().getResourceAsStream("api.config.properties"))
    // {
    try (InputStream input =
        PolicyUtil.class.getClassLoader().getResourceAsStream("application.properties")) {
      Properties prop = new Properties();
      // load a properties file
      prop.load(input);
      // get the property value and print it out///
      value = prop.getProperty(key);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return value;
  }

  public static String now() {
    SimpleDateFormat wsFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    String responseDateTime = wsFormat.format(new Date());
    return responseDateTime;
  }

  public static String[] getStatusForShowCustomer(
      String status1,
      String statusdate1,
      String status2,
      String statusdate2,
      String oldStatus1,
      String oldStatusdate1,
      String oldStatus2,
      String oldStatusdate2) {
    return getStatusForShowCustomer(
        status1,
        statusdate1,
        status2,
        statusdate2,
        oldStatus1,
        oldStatusdate1,
        oldStatus2,
        oldStatusdate2,
        false);
  }

  public static String[] getStatusForShowCustomer(
      String status1,
      String statusdate1,
      String status2,
      String statusdate2,
      String oldStatus1,
      String oldStatusdate1,
      String oldStatus2,
      String oldStatusdate2,
      boolean showD) {
    String[] status = null;
    if (null != status1 && null != status2) {
      status = new String[] {status1, statusdate1, status2, statusdate2};
      System.out.println("before status:");
      ZArray.show(status);
      if (status1.trim().equalsIgnoreCase("N")
          && (status2.trim().equals("1") || status2.trim().equals("2"))) {
        status[0] = oldStatus1;
        status[1] = oldStatusdate1;
        status[2] = oldStatus2;
        status[3] = oldStatusdate2;
      } else if (showD && "D".equalsIgnoreCase(status2.trim())) {
        status[0] = status2;
      }
      System.out.println("after status:");
      ZArray.show(status);
    }
    return status;
  }

  public static boolean StringIsNullOrEmpty(final String s) {
    // Null-safe, short-circuit evaluation.
    return s == null || s.trim().isEmpty();
  }

  public static String calculateEffectiveDateCL(String effectivedate, String payperiod) {
    try {
      Date effdate = new SimpleDateFormat("yyyyMMdd").parse(effectivedate);
      Integer year =
          payperiod != null && payperiod.trim().length() > 2
              ? Integer.parseInt(payperiod.substring(0, 2)) - 1
              : 0;
      Calendar cal = Calendar.getInstance();
      cal.setTime(effdate);
      cal.add(Calendar.YEAR, +year);
      effdate = cal.getTime();
      effectivedate = new SimpleDateFormat("yyyyMMdd").format(effdate);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return effectivedate;
  }

  public static String nowTH() {
    LocalDate date = LocalDate.now();
    DateTimeFormatter toThai =
        DateTimeFormatter.ofPattern("yyyyMMdd").withChronology(ThaiBuddhistChronology.INSTANCE);
    return toThai.format(date);
  }

  public static String addDataL(String data, String key, int len) {

    if (data.trim().length() < len) {
      for (int i = data.length(); i < len; i++) {
        data = key + data;
      }
    }
    return data;
  }

  public static String getVersion() {
    return callProperties("VERSION");
  }

  public static String getDBCustomer() {
    return callProperties("DBCustomer");
  }

  public static String getDBPolicy() {
    return callProperties("DBPolicy");
  }

  public static String getDBUnitlink() {
    return callProperties("DBUnitlink");
  }

  public static String getDBClaim() {
    return callProperties("DBClaim");
  }

  public static String getDBCollection() {
    return callProperties("DBCollection");
  }

  public static String getDBBusProduct() {
    return callProperties("DBBusProduct");
  }

  public static String getDBSaleStruct() {
    return callProperties("DBSaleStruct");
  }

  public static String getDBPos() {
    return callProperties("DBPos");
  }

  public static String getRiderCode(String ridertype) throws IOException {
    final File fo =
        new File(PolicyUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
    final String PATH = fo.getPath();
    final String configpath = PATH + "/mapRider.json";
    // final String configpath = "C:\\Program Files\\Apache Software Foundation\\Tomcat
    // 8.5.53\\webapps\\PolicyDataService\\WEB-INF\\classes\\mapRider.json";
    BufferedReader br = null;
    try {
      br =
          new BufferedReader(
              new InputStreamReader(new FileInputStream(configpath), StandardCharsets.UTF_8));
      String tmp = "", output = "";
      while ((tmp = br.readLine()) != null) output += tmp;
      JSONArray jsonArr = new JSONArray(output);
      for (int i = 0; i < jsonArr.length(); i++) {
        Object element = jsonArr.get(i);
        JSONObject object = new JSONObject(element.toString());
        if (object.getString("type").equals(ridertype)) return object.getString("code");
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (br != null) br.close();
    }
    return "R09";
  }

  public static String getRiderMessage(String ridercode) throws IOException {
    final File fo =
        new File(PolicyUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
    final String PATH = fo.getPath();
    final String configpath = PATH + "/mapRider.json";
    BufferedReader br = null;
    try {
      br =
          new BufferedReader(
              new InputStreamReader(new FileInputStream(configpath), StandardCharsets.UTF_8));
      String tmp = "", output = "";
      while ((tmp = br.readLine()) != null) output += tmp;
      JSONArray jsonArr = new JSONArray(output);
      for (int i = 0; i < jsonArr.length(); i++) {
        Object element = jsonArr.get(i);
        JSONObject object = new JSONObject(element.toString());
        if (object.getString("code").equals(ridercode)) return object.getString("message");
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (br != null) br.close();
    }
    return "";
  }

  public static String getPolicyTypeByType(String policyType) {
    String[] type = policyType.split("-");
    if (type.length >= 3) {
      switch (type[1].toUpperCase()) {
        case "IND":
          return "I";
        case "OL":
        case "OL_TAKAFUL":
          return "O";
        case "WHL":
          return "W";
        case "CL":
        case "CL_TAKAFUL":
          return "M";
        case "ULIP":
          return "L";
        case "UL":
          return "U";
      }
    }
    return null;
  }

  public static String setRowToJson(String str) {
    StringBuilder sql = new StringBuilder();
    sql.append(" select row_to_json(tmp) as json ");
    sql.append(" from ( ");
    sql.append(str);
    sql.append(" ) tmp ");
    return sql.toString();
  }

  public static boolean validateHeaderData(HeaderData header) {
    if (header == null) {
      log.error("Validate \"header\" error!, \"header\" can't be null or empty");
      return false;
    } else if (header.getMessageId() == null
        || header.getMessageId().equals("")
        || header.getMessageId().length() > 50) {
      log.error("Validate \"messageId\" error!, \"messageId\" can't be null or empty");
      return false;
    } else if (header.getSentDateTime() == null || header.getSentDateTime().equals("")) {
      log.error("Validate \"sentDateTime\" error!, \"sentDateTime\" can't be null or empty");
      return false;
    } else {
      return true;
    }
  }

  public static boolean validateInteger(String value) {
    return value != null && !value.trim().equals("") && isInt(value);
  }

  public static boolean validateString(String value) {
    return value != null && !value.trim().equals("");
  }

  public static boolean isInt(String s) {
    try {
      Integer.parseInt(s);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public static String getStatus1Desc(String status1, String type) {
    if (type.trim().equalsIgnoreCase("CL")) return getStatus1DescCL(status1);
    else return StatusPolicy.statusDesc(status1);
  }

  public static String getStatus1DescCL(String status1) {
    String[] star1 = Data.statCer;
    if (star1 != null) {
      for (String string : star1) {
        if (string.trim().length() > 1 && string.substring(0, 1).contains(status1))
          return string.replaceAll("^[\\w] - (.+)", "$1");
      }
    }
    return status1;
  }

  public static boolean validateString(String value, int len) {
    return value != null && !value.trim().equals("") && value.length() <= len;
  }

  public static String checkPayPeriodFormat(String payPeriod, int index) {
    if (payPeriod != null && payPeriod.length() == 4 && !payPeriod.contains("/")) {
      return payPeriod.substring(0, index) + "/" + payPeriod.substring(index, 4);
    }
    return payPeriod;
  }

  public static void setDefaultConnection(Connection con) throws Exception {
    if (con == null || con.isClosed()) {
      con = DBConnection.getConnectionPolicy();
    }
  }

  public static boolean formatDateYYYYMMDD(String date) {
    if (date == null || ("").equals(date)) return true;
    SimpleDateFormat yyyy_MM_dd = new SimpleDateFormat("yyyyMMdd");
    try {
      Date successDate = yyyy_MM_dd.parse(date);
      System.out.println("formatDateYYYYMMDD: " + successDate.toString());
    } catch (ParseException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  public static boolean isPolicyStatusAEIRU(String status) {
    if (null == status || ("").equals(status)) return false;
    System.out.println("isPolicyStatusAEIRU => " + status);
    return status.equals("A") || status.equals("E") || status.equals("R") || status.equals("U");
  }

  public static boolean checkRegisterOKByPolicyOLStatus(
      String stat1, String stat2, String ostat1, String ostat2) {
    String[] statusOK = new String[] {"A", "B", "E", "F", "H", "I", "J", "N", "R", "U", "V", "W"};
    if ((stat1 + stat2).equalsIgnoreCase("N1") || (stat1 + stat2).equalsIgnoreCase("N2"))
      stat1 = ostat1;
    for (String s : statusOK) {
      if (stat1.trim().toUpperCase().equals(s)) return true;
    }
    return false;
  }

  public static boolean checkRegisterOKByPolicyCLStatus(
      String stat1, String stat2, String ostat1, String ostat2) {
    String[] statusOK = new String[] {"I", "N", "T"};
    if ((stat1 + stat2).equalsIgnoreCase("N1") || (stat1 + stat2).equalsIgnoreCase("N2"))
      stat1 = ostat1;
    for (String s : statusOK) {
      if (stat1.trim().toUpperCase().equals(s)) return true;
    }
    return false;
  }

  public static BigDecimal getDefaultBigDecimal(BigDecimal b) {
    return b == null ? BigDecimal.ZERO : b;
  }

  public static int getHttpParseInt(String errorCode) {
    int http;
    try {
      http = Integer.parseInt(errorCode);
    } catch (Exception e) {
      log.error("getHttp: " + errorCode);
      e.printStackTrace();
      http = 500;
    }
    return http;
  }

  public static int getHttp(ResponseStatus status) {
    if (status == null || status.getErrorCode() == null) {
      return 500;
    } else {
      if (status.getErrorCode().equals("500")) {
        return 500;
      } else if (status.getErrorCode().equals("400")) {
        return 400;
      } else {
        return 200;
      }
    }
  }

  public static void checkRemote() {
    PublicRte.setRemote(true);
  }

  public static String descriptionByMode(String mode) {
    return utility.support.StatusInfo.modeStatus(mode);
  }

  public static boolean baseRequestIsNull(RequestMessages<?> requestMessages) {
    return requestMessages == null
        || requestMessages.getHeaderData() == null
        || requestMessages.getRequestRecord() == null;
  }

  public static void setResponseTime(HeaderData header){
    if(header == null){
      header = new HeaderData();
    }
    header.setResponseDateTime(now());
  }

  public static JSONObject getPolDB01(String customerID ,String messageID , String sentDateTime , ApiConfigM apiConfigM ) throws Exception {
    String pol01 = "/PolicyDataService/SearchPolicyByCustomerId";
    CallService callService = new CallService();
    String reqPol01 = getRequestPol01(customerID, messageID ,sentDateTime );
    String urlPol01 = apiConfigM.getENDPOINT() + pol01;
//		String urlPol01 = "http://10.102.63.35:8080/PolicyDataService/SearchPolicyByCustomerId";
    System.out.println("getPolDB01 callService : " + urlPol01 + "|" + reqPol01 + "|");
    String respPol01 = callService.callWS(urlPol01, reqPol01);
    System.out.println("respPolDB01 : " + respPol01);
    return new JSONObject(respPol01);
  }
  private static String getRequestPol01(String customer_id, String messageID , String sentDateTime) {
    String request = "{" + "\"headerData\": {" + "	\"messageId\": \"" + messageID + "\","
            + "	\"sentDateTime\": \"" + sentDateTime + "\"" + "},"
            + "	\"requestRecord\": { " + " \"customer_id\": \"" + customer_id + "\"" + "}" + "} ";
    return request;
  }

  public static JSONObject getPolDB03(String policyno, String certno, String type ,String messageID , String sentDateTime , ApiConfigM apiConfigM ) throws Exception {
    CallService callService = new CallService();
    String pol03 = "/PolicyDataService/NextPayPeriod";
    String urlPol03 = apiConfigM.getENDPOINT() + pol03;
//		String urlPol03 = "http://10.102.63.35:8080/PolicyDataService/NextPayPeriod";
    String reqPol03 = getRequestPol03(policyno, certno, type, messageID, sentDateTime);
    System.out.println("reqPol03 : " +reqPol03);
    String respPol03 = callService.callWS(urlPol03, reqPol03);
    System.out.println("respPol03 : " +respPol03);
    return new JSONObject(respPol03);
  }

  private static String getRequestPol03(String policyno, String certno, String type,
                                        String messageID , String sentDateTime) {
    String request = "{" + "\"headerData\": {" + "	\"messageId\": \"" + messageID + "\","
            + "	\"sentDateTime\": \"" + sentDateTime + "\"" + "},"
            + "	\"requestRecord\": { " + "		\"nextPayPeriod\": [ " + "{ " + " \"policyno\": \"" + policyno
            + "\"," + " \"certno\": \"" + certno + "\"," + " \"type\": \"" + type + "\"" + "} ] }" + "} ";
    return request;
  }
  public static String getUUID() {
    return UUID.randomUUID().toString();
  }
  private static HeaderData getHeader(String uuid) {
    HeaderData header = new HeaderData();
    header.setMessageId(uuid.equals("") || uuid.length() > 50 ? getUUID() : uuid);
    header.setSentDateTime(now());
    return header;
  }
  public static Long saveTransaction(TransactionRequest trans, String msgId) {
    log.info("================ [Start] SaveTransaction ================");
    try {
      CallService callService = new CallService();
      DataServiceRequestT<TransactionRequest> request = new DataServiceRequestT<>();
      request.setHeaderData(getHeader(msgId));
      request.setRequestRecord(trans);
      ApiConfigM api = callService.readPoperties();
      String url = new URL(api.getENDPOINT()).toString().concat("/CustomerCommonService/saveTransaction");
      String resJson =
              callService.callWS(
                      url,
                      convertObjectToJson(request));
      log.info(String.format("ResponseJson from CommonTransaction : %s", resJson));
      if (resJson != null) {
        return getLogId(new JSONObject(resJson).get("responseRecord").toString());
      } else {
        throw new Exception("Can't CallWsCommon response is null");
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(String.format("saveTransaction(CallWsCommon) Error : %s", e.getMessage()));
      return null;
    } finally {
      log.info("================ [End] SaveTransaction ================");
    }
  }
  public static void updateTransaction(TransactionRequest trans, String msgId) {
    log.info("================ [Start] UpdateTransaction ================");
    try {
      if (trans.getId() != null) {
        DataServiceRequestT<TransactionRequest> request =
                new DataServiceRequestT<>();
        request.setHeaderData(getHeader(msgId));
        request.setRequestRecord(trans);
        CallService callService = new CallService();
        ApiConfigM api = callService.readPoperties();
        String url = new URL(api.getENDPOINT()).toString().concat("/CustomerCommonService/updateTransaction");
        String resJson =
                callService.callWS(
                        url,
                        convertObjectToJson(request));
        log.info(String.format("ResponseJson from CommonTransaction : %s", resJson));
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(String.format("UpdateTransaction Error : %s", e.getMessage()));
    } finally {
      log.info("================ [End] UpdateTransaction ================");
    }
  }
  private static Long getLogId(String str) {
    return "null".equals(str) ? null : Long.valueOf(str);
  }
  public static String convertObjectToJson(Object o) {
    try {
      if (o == null) return "{ Object is null }";
      return new ObjectMapper().writeValueAsString(o);
    } catch (Exception e) {
      e.printStackTrace();
      return "{ Object is null }";
    }
  }

  public static String effectiveData(String effectiveDate, String yearPeriod){
    return Integer.parseInt(effectiveDate.substring(0, 4))
            + (Integer.parseInt(yearPeriod) - 1)
            + effectiveDate.substring(4, 8);
  }

  public static String pattenLog(String transaction, String message) {
    return transaction + " | " + message;
  }
}
