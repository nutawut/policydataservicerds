package com.thailife.api.policyInfoService.util.iServiceRTE;

import java.util.Vector;

import com.thailife.api.policyInfoService.dao.*;
import com.thailife.api.policyInfoService.dao.impl.*;
import layout.unitlink.master.Indmast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import insure.Insure;
import insure.PlanSpec;
import insure.PlanType;
import insure.TLPlan;
import manit.M;
import manit.Record;
import manit.rte.Result;
import service.service.SearchMaster;
import utility.address.AddressFnc;
import utility.cfile.TempFile;
import utility.personnel.support.ZArray;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;
import utility.support.StatusInfo;
import utility.support.StatusPolicy;

@Service
public class IServiceRtePOLV2 {

	@Autowired
	OrdmastDaoImpl ordmastRepository;

	@Autowired
	IndmastDaoImpl indmastRepository;

	@Autowired
	UniversallifeDaoImpl universallifeRepository;

	@Autowired
	WhlmastDaoImpl whlmastRepository;

	@Autowired
	UnitLinkDaoImpl unitlinkRepository;

	@Autowired
	SubunitlinkDaoImpl subunitlinkRepository;

	private String firstName = "", lastName = "", preName = "", sex = "", age = "";
	private String policyNo = "", policyType = "", plan = "", mode = "", branch = "", policyStatus1 = "",
			policyStatus2 = "", oldPolicyStatus1 = "";
	private String insuredAge = "", insureYear = "", effectiveDate = "";
	private String nextPeriod = "", payPeriod = "", nextPayPeriod = "", duedate = "";
	private String lastLifePremium = "", lastExtraLifePremium = "";
	private String sumRppUL = "", premRppUL = "", sumAssure = "";
	String nextDueDate = "", premiumAmount = "";
	private String[] addr = { "", "" };
	private byte[] mstFile, personFile;
	private TLPlan tlp;
	private PlanType ckPlan = new PlanType();
	private Vector md = new Vector(), vrid = new Vector();
	private Vector[] vName = new Vector[2];
	private IndmastDao indmast = null;
	private OrdmastDao ordmast = null;
	private UniversallifeDao ulmast = null;
	private UnitLinkDao ulip = null;
	private WhlmastDao whlmast = null;

	public String findMasterType(final String policyNo) {
		String s = "";
		Result result = PublicRte.getClientResult("blmaster", "rte.bl.master.FindTypePolicy", (Object) policyNo);
		if (result.status() == 0) {
			s = (String) result.value();
		}
		return s;
	}

	private int checkDataMaster(final String[] array) {
		Result result = PublicRte.getClientResult("blmaster", "rte.bl.master.FindMasterDetail", (Object) array);
		return result.status();
	}

	public void findPolicy(String policyNo) {
		this.policyNo = policyNo;
		if (vName[0] != null) {
			vName[0].removeAllElements();
			vName[1].removeAllElements();
			vName[0] = null;
			vName[1] = null;
		}
		policyType = findMasterType(policyNo);
		if (policyType.length() == 0)
			return;
		String[] param = { policyType, policyNo, "A" };
		if (checkDataMaster(param) != 0) {
			return;
		}
	}

	public String findTelephone(final String addressId) {
		final Vector vector = (Vector) PublicRte
				.getClientResult("blmaster", "rte.bl.master.FindTelephone", (Object) new String[] { addressId })
				.value();
		if (vector.elementAt(0) != null) {
			return AddressFnc.formatTelephoneString(vector);
		}
		return "";
	}

	public String[] getMasterDetail() throws Exception {
		String[] masterDetail = { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "" };
		String matureDate = "";
		lastLifePremium = "0";
		lastExtraLifePremium = "0";
		if (policyType.equals("I")) {
			IndmastDao indmast = indmastRepository.findByPolicyno(policyNo);
			if (null != indmast) {
//				mstFile = file.indmast.getBytes();
				plan = indmast.getPlancode().trim();
				policyStatus1 = indmast.getPolicystatus1();
				policyStatus2 = indmast.getPolicystatus2();
				oldPolicyStatus1 = indmast.getOldpolicystatus1();
				tlp = PlanSpec.getPlan(plan);
				insuredAge = indmast.getInsuredage().toString();
				insureYear = M.itoc(M.ctoi(tlp.endowmentYear(insuredAge)));
				payPeriod = indmast.getPayperiod();
				lastLifePremium = indmast.getLifepremium().toString();
				effectiveDate = indmast.getEffectivedate();
				mode = "0";
				sumAssure = indmast.getSum().toString();
				branch = indmast.getBranch();
				payPeriod = indmast.getPayperiod();
				nextPayPeriod = nextPayPeriod(payPeriod, "0    ", "I");

				masterDetail[0] = getPlanCode('I', plan);
				masterDetail[2] = matureDate(plan, effectiveDate, 'I', insuredAge);
				masterDetail[6] = nextPayPeriod.substring(3) + nextPayPeriod.substring(0, 2) + "01";
				masterDetail[8] = indmast.getLifepremium().toString();
				masterDetail[22] = nextPayPeriod.substring(3) + nextPayPeriod.substring(0, 2) + "01";
			} else {
				return new String[] { "null" };
			}
		} else if (policyType.equals("O")) {
			OrdmastDao ordmast = ordmastRepository.findByPolicyno(policyNo);
			if (null != ordmast) {
//				mstFile = file.ordmast.getBytes();
				mode = ordmast.getMode();
				plan = ordmast.getPlancode().trim();
				policyStatus1 = ordmast.getPolicystatus1();
				policyStatus2 = ordmast.getPolicystatus2();
				oldPolicyStatus1 = ordmast.getOldpolicystatus1();
				payPeriod = ordmast.getPayperiod();
				nextPeriod = nextPayPeriod(payPeriod, mode, "O");
				matureDate = "";
				tlp = PlanSpec.getPlan(plan);
				insuredAge = ordmast.getInsuredage().toString();
				insureYear = M.itoc(M.ctoi(tlp.endowmentYear(insuredAge)));
				effectiveDate = ordmast.getEffectivedate();
				sumAssure = ordmast.getSum().toString();
				branch = ordmast.getBranch();

				try {
					getMasterPremium(policyNo, payPeriod);
					lastLifePremium = getLastPremium("O", policyNo, mode, payPeriod, ordmast.getRpno());
					checkSinglePremiumPlan(plan, ordmast.getLifepremium().toString());
					matureDate = getMatureDate(plan);
					getNextDuedateAndPremium(plan, payPeriod, mode, effectiveDate);

				} catch (Exception e) {
					throw e;
				}

				masterDetail[0] = getPlanCode('O', plan);
				masterDetail[2] = matureDate;
				masterDetail[6] = nextDueDate;
				masterDetail[8] = premiumAmount;
				masterDetail[22] = ordmast.getDuedate();
			} else {
				return new String[] { "null" };
			}
		} else if (policyType.equals("U")) {
			UniversallifeDao ulmast = universallifeRepository.findByPolicyno(policyNo);
			if (null != ulmast) {
//				mstFile = file.ulmast.getBytes();
				mode = ulmast.getMode();
				plan = ulmast.getPlancode().trim();
				policyStatus1 = ulmast.getPolicystatus1();
				policyStatus2 = ulmast.getPolicystatus2();
				oldPolicyStatus1 = ulmast.getOldpolicystatus1();
				payPeriod = ulmast.getPayperiod();
				matureDate = "";
				effectiveDate = ulmast.getEffectivedate();
				nextPeriod = nextPayPeriod(payPeriod, mode, "O");
				tlp = PlanSpec.getPlan(plan);
				insuredAge = ulmast.getInsuredage().toString();
				insureYear = M.itoc(M.ctoi(tlp.endowmentYear(insuredAge)));
				sumAssure = ulmast.getSum().toString();
				branch = ulmast.getBranch();

				try {
					getUniversalPremium(policyNo, nextPeriod, effectiveDate);
					lastLifePremium = getLastPremium("U", policyNo, mode, payPeriod, ulmast.getRpno());
					checkSinglePremiumPlan(plan, "");
					matureDate = getMatureDate(plan);
					getNextDuedateAndPremiumUL(plan, payPeriod, mode, effectiveDate, nextPeriod);

				} catch (Exception e) {
					throw e;
				}

				masterDetail[0] = getPlanCode('U', plan);
				masterDetail[2] = matureDate;
				masterDetail[6] = nextDueDate;
				masterDetail[8] = premiumAmount;
				masterDetail[22] = ulmast.getDuedate();
				masterDetail[26] = ulmast.getTopuppremium().toString();
			} else {
				return new String[] { "null" };
			}
		} else if (policyType.equals("L")) {
			UnitLinkDao ulip = unitlinkRepository.findByPolicyno(policyNo);
			SubunitlinkDao subunitlink = subunitlinkRepository.findByPolicyNo(policyNo);
			if (null != ulip) {
//				mstFile = file.ulip.getBytes();
				plan = ulip.getPlancode().trim();
				policyStatus1 = ulip.getPolicystatus1();
				policyStatus2 = ulip.getPolicystatus2();
				oldPolicyStatus1 = ulip.getOldpolicystatus1();
				duedate = ulip.getDuedate();
				effectiveDate = ulip.getEffectivedate();
				mode = ulip.getMode();
				sumAssure = subunitlink.getSum().toString();
				branch = ulip.getBranch();
				payPeriod = ulip.getPayperiod();
				nextPayPeriod = nextPayPeriod(payPeriod, mode, "L");

				masterDetail[0] = getPlanCode('L', plan);
				masterDetail[2] = matureDate(plan, effectiveDate, 'L', insuredAge);
				masterDetail[6] = nextPayPeriod.substring(3) + nextPayPeriod.substring(0, 2) + "01";
				masterDetail[8] = subunitlink.getPremium().toString();
				masterDetail[13] = findTelephone(ulip.getLocaladdressid());
				masterDetail[22] = duedate;
				masterDetail[23] = ulip.getRppsum().toString();
				masterDetail[24] = ulip.getRpppremium().toString();
//				String[] uli = file.ulip.fieldName();
				Result rs = PublicRte.getResult("blmaster", "rte.bl.master.RteMasterPremium",
						new String[] { policyNo, payPeriod });
				if (rs.status() == 0) {
					md = (Vector) rs.value();
					vrid = (Vector) md.elementAt(0);
				}
			} else {
				return new String[] { "null" };
			}
		} else {
			WhlmastDao whlmast = whlmastRepository.findByPolicyno(policyNo);
			if (null != whlmast) {
//				mstFile = file.whlmast.getBytes();
				plan = whlmast.getPlancode().trim();
				payPeriod = whlmast.getPayperiod();
				nextPeriod = nextPayPeriod(payPeriod, whlmast.getMode(), "W");
				policyStatus1 = whlmast.getPolicystatus1();
				premiumAmount = whlmast.getLifepremium().toString();
				tlp = PlanSpec.getPlan(plan);
				insuredAge = whlmast.getInsuredage().toString();
				insureYear = M.itoc(M.ctoi(tlp.endowmentYear(insuredAge)));
				lastLifePremium = premiumAmount;
				effectiveDate = whlmast.getEffectivedate();
				mode = whlmast.getMode();
				sumAssure = whlmast.getSum().toString();

				masterDetail[0] = getPlanCode('O', plan);
				masterDetail[2] = whlMatureDate(plan, insuredAge, effectiveDate);
				masterDetail[6] = whlmast.getDuedate();
				masterDetail[7] = nextPeriod;
				masterDetail[8] = premiumAmount;
			} else {
				return new String[] { "null" };
			}
		}
		if ((("AIJLOR").indexOf(masterDetail[3]) < 0) && (!"H".equals(masterDetail[3]))) {
			nextDueDate = "00000000";
			nextPayPeriod = "0000";
			premiumAmount = "000000000";
			masterDetail[6] = nextDueDate;
			masterDetail[7] = nextPayPeriod;
			masterDetail[8] = premiumAmount;
		}

		sumRppUL = "";
		premRppUL = "";
//		sex = file.person.get("sex");
//		age = Insure.getAge(file.person.get("birthDate"), effectiveDate);
//		personFile = file.person.getBytes();
//		firstName = file.name.get("firstName");
//		lastName = file.name.get("lastName");
//		preName = utility.prename.Prename.getAbb(file.name.get("preName"));
//		addr = file.addr;

		masterDetail[1] = effectiveDate;
		masterDetail[3] = StatusPolicy.statusDesc(policyStatus1);
		masterDetail[4] = sumAssure;
		masterDetail[5] = mode;
		masterDetail[7] = nextPayPeriod;
//		masterDetail[9] = file.addr[0];
//		masterDetail[10] = file.addr[1];
		masterDetail[11] = firstName;
		masterDetail[12] = lastName;
		masterDetail[14] = masterDetail[3];
		masterDetail[15] = plan;
		masterDetail[16] = branch;
		masterDetail[17] = payPeriod;
		masterDetail[18] = policyStatus2;
		masterDetail[19] = oldPolicyStatus1;
		masterDetail[20] = StatusPolicy.statusDesc(masterDetail[19]);
		if (masterDetail[20] == null)
			masterDetail[20] = "";
		masterDetail[21] = preName;
		masterDetail[25] = policyType;

		return masterDetail;
	}

	public Vector getDividend() {
		Vector v = new Vector();
		String[] divData = { "", "", "", "", "", "" };

		Record condPay, dividen, annuity;
		Vector param = new Vector();
		param.add(mstFile);
		param.add(personFile);
		param.add(new String[] { policyType, DateInfo.sysDate().substring(0, 8), "Y", "N", "N", "T" });
		Result rs = PublicRte.getClientResult("blservice", "rte.bl.service.benefit.RteCalculateBenefit", param);
		Vector vInt = new Vector();
		if (rs.status() == 0) {
			vInt = (Vector) rs.value();
		} else {
			System.out.println("status = " + rs.status());
			System.out.println("result = " + rs.value());
		}

		Vector vDividen = new Vector<>();
		Vector vCondpay = new Vector<>();
		Vector vAnnuity = new Vector<>();

		try {
			vDividen = SearchMaster.searchDividen();
		} catch (Exception e) {
			System.out.println("Error vDividen " + e.getMessage());
			e.printStackTrace();
		}
		try {

			vCondpay = SearchMaster.searchPaycond();
		} catch (Exception e) {
			System.out.println("Error vCondpay " + e.getMessage());
			e.printStackTrace();
		}
		try {

			vAnnuity = SearchMaster.searchAnnuityMast();
		} catch (Exception e) {
			System.out.println("Error vAnnuity " + e.getMessage());
			e.printStackTrace();
		}
		// Vector vDividen = SearchMaster.searchDividen();
		// Vector vCondpay = SearchMaster.searchPaycond();
		// Vector vAnnuity = SearchMaster.searchAnnuityMast();
		Vector vSort;

		// vDividen.clone();
		// vCondpay.clone();
		// vAnnuity.clone();
		String[] temp = { "type", "policyNo", "payYear", "payDate", "payFlag", "amount", "interest", "branch",
				"fromApl" };
		int[] tempLen = { 1, 8, 2, 8, 1, 15, 10, 3, 1 };
		char[] fieldType = { 'T', 'T', 'T', 'T', 'T', 'N', 'N', 'T', 'T', 'T' };
		int[] fieldScale = { 0, 0, 0, 0, 0, 2, 2, 0, 0 };
		String[] tempkey0 = { "payYear" };
		TempFile tempFile = new TempFile(temp, tempLen);
		tempFile.setNoOfKey(1);
		tempFile.setKey(0, tempkey0);
		// ============================
		// Add Condpay
		// ============================
		if (vCondpay != null) {
			for (int i = 0; i < vCondpay.size(); i++) {
				condPay = (Record) vCondpay.elementAt(i);
				if (condPay == null)
					break;
				tempFile.newRecord();
				tempFile.set("type", "0");
				tempFile.set("policyNo", policyNo);
				tempFile.set("payYear", condPay.get("payYear"));
				tempFile.set("payDate", condPay.get("payDate"));
				tempFile.set("payFlag", condPay.get("payFlag"));
				tempFile.set("amount", condPay.get("amount"));
				tempFile.set("interest", condPay.get("interest"));
				tempFile.set("branch", condPay.get("branch"));
				tempFile.set("fromApl", condPay.get("fromApl"));
				tempFile.add();
			}
		}
		// ============================
		// Add Dividen
		// ============================
		if (vDividen != null) {
			for (int i = 0; i < vDividen.size(); i++) {
				dividen = (Record) vDividen.elementAt(i);
				if (dividen == null)
					break;
				tempFile.newRecord();
				tempFile.set("type", "1");
				tempFile.set("policyNo", dividen.get("policyNo"));
				tempFile.set("payYear", dividen.get("payYear"));
				tempFile.set("payDate", dividen.get("payDate"));
				tempFile.set("payFlag", dividen.get("payFlag"));
				tempFile.set("amount", dividen.get("amount"));
				tempFile.set("interest", dividen.get("interest"));
				tempFile.set("branch", dividen.get("branch"));
				tempFile.set("fromApl", dividen.get("reserve"));
				tempFile.add();
			}
		}
		// ============================
		// Add Annuity
		// ============================
		if (vAnnuity != null) {
			for (int i = 0; i < vAnnuity.size(); i++) {
				annuity = (Record) vAnnuity.elementAt(i);
				if (annuity == null)
					break;
				tempFile.newRecord();
				tempFile.set("type", "2");
				tempFile.set("policyNo", annuity.get("policyNo"));
				tempFile.set("payYear", annuity.get("payYear"));
				tempFile.set("payDate", annuity.get("payDate"));
				tempFile.set("payFlag", annuity.get("payFlag"));
				tempFile.set("amount", annuity.get("amount"));
				tempFile.set("interest", annuity.get("interest"));
				tempFile.set("branch", annuity.get("branch"));
				tempFile.set("fromApl", annuity.get("annualType"));
				tempFile.add();
			}
		}
		String payFlag = "N";
		String payDesc = "";
		int vIntIdx = 0;
		String interest = "";
		for (boolean st = tempFile.first(); st; st = tempFile.next()) {
			interest = tempFile.get("interest");
			if (tempFile.get("payFlag").equals("2")) {
				payFlag = "N";
				payDesc = StatusInfo.receiveDvPayStatus(tempFile.get("payFlag"));
				String[] data = null;
				if (vInt != null && vInt.size() > 0) {
					try {
						data = (String[]) vInt.elementAt(vIntIdx);
						ZArray.show(data);
						interest = data[4];
						if (data[7].trim().equals("")) {
							divData = new String[] { data[0], data[1], data[2], data[3], data[4], payFlag, payDesc,
									tempFile.get("payFlag"), tempFile.get("branch") };
							v.addElement(divData);
						}
						vIntIdx++;
					} catch (Exception e) {
						System.out.println("Error : " + e.toString());
					}
				} else {
					divData = new String[] { tempFile.get("type"), tempFile.get("payYear"), tempFile.get("payDate"),
							tempFile.get("amount"), interest, payFlag, payDesc, tempFile.get("payFlag"),
							tempFile.get("branch") };
					v.addElement(divData);
				}
			} else {
				payFlag = "Y";
				// Translate PlayFlag -> PayDesc
				payDesc = StatusInfo.receiveDvPayStatus(tempFile.get("payFlag"));
				divData = new String[] { tempFile.get("type"), tempFile.get("payYear"), tempFile.get("payDate"),
						tempFile.get("amount"), interest, payFlag, payDesc, tempFile.get("payFlag"),
						tempFile.get("branch") };
				v.addElement(divData);
			}

		}
		return v;
	}

	private String nextPayPeriod(String payPeriod, String mode, String type) {
		// INDMAST
		String showPeriod = "";
		if (type.equals("I")) {
			String year = payPeriod.substring(0, 4);
			String period = payPeriod.substring(4);
			if (M.cmps(period, "12") == 0) {
				period = "01";
				year = M.inc(year);
			} else
				period = M.inc(period);
			showPeriod = period + "/" + year;
		} else {
			String year = payPeriod.substring(0, 2);
			String period = payPeriod.substring(2);
			switch (mode.charAt(0)) {
			case '1':
				year = M.inc(year);
				break;
			case '0':
				period = M.inc(period);
				if (M.cmps(period, "12") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '2':
				period = M.inc(period);
				if (M.cmps(period, "02") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '4':
				period = M.inc(period);
				if (M.cmps(period, "04") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			}
			showPeriod = year + period;
		}
		return showPeriod;
	}

	private String getLifePrem(boolean extra) {
		String prem = "0";
		for (int i = 0; i < vrid.size(); i++) {
			String[] ar = (String[]) vrid.elementAt(i);
			if (ar[0].equals(M.stou("à¸Šà¸§")) || ar[0].equals(("RP"))) {
				if (extra)
					return ar[2];
				else {
					return ar[1];
				}
			}
		}
		return prem;
	}

	private String nextPayPeriodwithSlash(String payPeriod, String mode, String type) {
		String showPeriod = "";
		if (type.equals("I")) {
			String year = payPeriod.substring(0, 4);
			String period = payPeriod.substring(4);
			if (M.cmps(period, "12") == 0) {
				period = "01";
				year = M.inc(year);
			} else
				period = M.inc(period);
			showPeriod = period + "/" + year;
		} else {
			String year = payPeriod.substring(0, 2);
			String period = payPeriod.substring(2);
			switch (mode.charAt(0)) {
			case '1':
				year = M.inc(year);
				break;
			case '0':
				period = M.inc(period);
				if (M.cmps(period, "12") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '2':
				period = M.inc(period);
				if (M.cmps(period, "02") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '4':
				period = M.inc(period);
				if (M.cmps(period, "04") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			}
			showPeriod = year + "/" + period;
		}
		return showPeriod;

	}

	private String getPlanCode(char Type, String plan) {
		PlanSpec.planCodes(Type);
		TLPlan pc = PlanSpec.getPlan(plan);
		return pc.name();
	}

	private String matureDate(String plan, String effectiveDate, char policyType, String insuredAge) {
		PlanSpec.planCodes(policyType);
		TLPlan pc = PlanSpec.getPlan(plan);
		return M.addnum(effectiveDate.substring(0, 4), pc.endowmentYear(insuredAge)) + effectiveDate.substring(4);
	}

	private String whlMatureDate(String plan, String insuredAge, String effectiveDate) {
		TLPlan tl = PlanSpec.getPlan(plan);
		String insureYear = tl.endowmentYear(insuredAge);
//		if (insureYear.length() >= 0)
//			return (Insure.matureDate(effectiveDate, insureYear, plan, file.person.get("birthDate")));
		return (M.clears('0', 8));
	}

	private void getMasterPremium(String policyNo, String payPeriod) {
		Result rs = PublicRte.getResult("blmaster", "rte.bl.master.RteMasterPremium",
				new String[] { policyNo, payPeriod });
		if (rs.status() == 0) {
			md = (Vector) rs.value();
			vrid = (Vector) md.elementAt(0);
		}
	}

	private void getUniversalPremium(String policyNo, String nextPeriod, String effectiveDate) {
		Result rs = PublicRte.getResult("blmaster", "rte.bl.universal.master.RteUniversalPremium",
				new String[] { policyNo, nextPeriod, effectiveDate });
		if (rs.status() == 0) {
			md = (Vector) rs.value();
			vrid = (Vector) md.elementAt(0);
		}
	}

	private String getLastPremium(String policyType, String policyNo, String mode, String payPeriod, String rpNo) {
		Result pop = PublicRte.getResult("blreceipt", "rte.bl.receipt.LastPremium",
				new String[] { policyType, policyNo, mode, payPeriod, rpNo });
		String prem = "";
		if (pop.status() == 0) {
			prem = (String) pop.value();

		}
		return prem;
	}

	private void checkSinglePremiumPlan(String plan, String lifePremium) {
		String extraPrem = "0";
		String lifePrem = "";
		if (ckPlan.isSinglePremiumPlan(plan)) {
//			for (int i = 0; i < file.vExtra.size(); i++) {
//				Record rd = (Record) file.vExtra.elementAt(i);
//				if (rd.get("extraType").equals(M.mapt("=;"))) {
//					extraPrem = M.addnum(extraPrem, rd.get("extraPremium"));
//				}
//			}
			lifePrem = lifePremium;
		} else {
			lifePrem = getLifePrem(false);
			lastLifePremium = lifePrem;
			lastExtraLifePremium = getLifePrem(true);
		}
	}

	private String getMatureDate(String plan) {
		if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan)) {
			payPeriod = "0101";
			effectiveDate = M.addnum(effectiveDate.substring(0, 4), M.subnum(payPeriod.substring(0, 2), "1"))
					+ effectiveDate.substring(4);
			if (effectiveDate.substring(4, 8).compareTo("0229") == 0) {
				int days = DateInfo.daysInMonth(effectiveDate);
				effectiveDate = effectiveDate.substring(0, 6) + M.setlen(M.itoc(days), 2);
			}
			if (!M.dateok(effectiveDate))
				effectiveDate = effectiveDate.substring(0, 6) + M.subnum(effectiveDate.substring(6), "1");
			return M.addnum(effectiveDate.substring(0, 4), "1") + effectiveDate.substring(4);
		} else {
			return (String) md.elementAt(12);
		}
	}

	private void getNextDuedateAndPremium(String plan, String payPeriod, String mode, String effectiveDate) {
		if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan) || ckPlan.isSinglePremiumPlan(plan)
				|| ("MF").indexOf(policyStatus1) >= 0) {
			nextDueDate = "00000000";
			nextPayPeriod = "0000";
			premiumAmount = "000000000";
		} else {
			nextPayPeriod = nextPayPeriodwithSlash(payPeriod, mode, "O");
			nextDueDate = Insure.dueDate(mode, effectiveDate, nextPayPeriod);
			try {
				Result rsnext = PublicRte.getResult("blmaster", "rte.bl.master.RteMasterPremium",
						new String[] { policyNo, nextPayPeriod });
				if (rsnext.status() == 0) {
					Vector rv = (Vector) rsnext.value();
					vrid = (Vector) rv.elementAt(0);
					premiumAmount = (String) rv.elementAt(9);
				} else {
					System.out.println("rsnext.status = " + rsnext.status());
				}

			} catch (Exception e) {
				System.out.println("calNextPremium - " + e.getMessage());
			}
		}
	}

	private void getNextDuedateAndPremiumUL(String plan, String payPeriod, String mode, String effectiveDate,
			String nextPeriod) {
		if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan) || ckPlan.isSinglePremiumPlan(plan)
				|| ("MF").indexOf(policyStatus1) >= 0) {
			nextDueDate = "00000000";
			nextPayPeriod = "0000";
			premiumAmount = "000000000";
		} else {
			nextPayPeriod = nextPayPeriod(payPeriod, mode, "O");
			Insure insure = new Insure();
			nextDueDate = insure.dueDate(mode, effectiveDate, nextPeriod);
			try {
				Result rsnext = PublicRte.getResult("blmaster", "rte.bl.universal.master.RteUniversalPremium",
						new String[] { policyNo, nextPeriod, effectiveDate });
				if (rsnext.status() == 0) {
					Vector rv = (Vector) rsnext.value();
					vrid = (Vector) rv.elementAt(0);
					premiumAmount = (String) rv.elementAt(9);
				} else {
					System.out.println("rsnext.status = " + rsnext.status());
				}

			} catch (Exception e) {
				System.out.println("calNextPremium - " + e.getMessage());
			}
		}
	}

}
