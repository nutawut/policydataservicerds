package com.thailife.api.policyInfoService.util.iServiceRTE;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

import insure.Insure;
import insure.PlanSpec;
import insure.PlanType;
import insure.RiderType;
import insure.TLPlan;
import iservice.Rtaxdetailyyyy;
import layout.bean.ws.pos.CalExtendLoanBean;
import layout.bean.ws.pos.CalLoanBeanSet;
import layout.bean.ws.pos.CalRequestLoanBean;
import layout.bean.ws.pos.CalReturnLoanBean;
import layout.service.Rcompany;
import layout.unitlink.service.cv.ServicePaymentTransaction;
import manit.M;
import manit.Mview;
import manit.Record;
import manit.pdf.Mpdf;
import manit.rte.Result;
import master.master.MasterFile;
import service.service.SearchMaster;
import service.taxdeductible.iservice.TaxIService;
import utility.cfile.TempFile;
import utility.personnel.support.ZArray;
import utility.personnel.support.ZVector;
import utility.rteutility.PublicRte;
import utility.support.DateInfo;
import utility.support.StatusInfo;
import utility.support.StatusPolicy;
import utility.unitlink.service.Fund_Service;

public class IServiceRtePOL {
	int status = 0;
	boolean flagReceipt = false;
	MasterFile file;
	String policyNo = "";
	String policyType = "";
	Vector[] vName = new Vector[2];
	boolean foundFlag = false;
	Vector md = new Vector();
	Vector vrid = new Vector();
	String nextPeriod = "", payPeriod = "";
	PlanType ckPlan = new PlanType();
	String plan = "";
	String policyStatus1 = "";
	String taxyear = "";
	String effectiveDate = "";
	String insureYear = "";
	String sumAssure = "";
	String sex = "";
	String age = "";
	String transfertype = "";
	String branch = "000";
	String type = "";
	String remark = "";
	String firstName = "";
	String lastName = "";
	String preName = "";
	String mode = "";
	String[] addr = { "", "" };
	// public TsterFileLPlan tlp;
	TLPlan tlp;
	byte[] mstFile;
	byte[] personFile;
	String monthTaxCheck = DateInfo.sysDate().substring(4, 6);
	String yearTaxCheck = DateInfo.sysDate().substring(0, 4);

	public static void main(String[] args) throws Exception {
		// System.out.println("a");
		// String policyNo = "39848864"; /* Unitlink */
		// String policyNo = "35444615"; /* Other */
		// String policyNo = "39848864"; /* UWA */
		// String policyNo = "31112055"; /* UEA */
		// String policyNo = "29943133"; /* UZA */
		// String idNo = "3860200287726";
		// String idNo = "0000009360766";
		/*
		 * String policyNo = "37413024"; String idNo = "0000007635031";
		 */
		// String policyNo = "22947265";
		// String idNo = "0000008259567";

		PublicRte.setRemote(true);
		String policyNo = "90191531";
		IServiceRtePOL iservice = new IServiceRtePOL(); 
		// Vector v = iservice.getPolicy(idNo);
		// System.out.println("test getPolicy : " + idNo +"|"+v.size());

		iservice.findPolicy(policyNo);

		String[] masterDetail = iservice.getMasterDetail();
		ZArray.show(masterDetail);
//		Vector v = iservice.getLoan();
//		System.out.println("LOAN :::::::::::::::: ");
		System.out.println("Start :::::::::::::::: ");
		Vector v2 = iservice.getDividend();
		System.out.println("Dividend :::::::::::::::: ");
		ZVector.show(v2);
		/*
		 * System.out.println("is Rider :::::::::;"); ZVector.show(iservice.getRider());
		 */
		/*
		 * iservice.getPayHistory(policyNo);
		 * 
		 */
		// iservice.getPolicy(idNo);
		/*
		 * iservice.findPolicy(policyNo); String[] masterDetail =
		 * iservice.getMasterDetail();
		 * 
		 * System.out.println("Before vRider "); Vector vRider = iservice.getRider();
		 * 
		 * System.out.println("After vRider " + vRider); if (vRider != null) {
		 * ZVector.show(vRider); }
		 */
		/*
		 * PublicRte.setRemote(false); if (args.length == 1) new IServiceRteUL(args[0]);
		 * else if (args.length == 3) new IServiceRteUL(args[0], args[1], args[2]);
		 */
		/**
		 * PublicRte.setRemote(false); new IServiceRte("39895277");
		 **/

		/*
		 * //Rider Iservice UnitLinkPremium mp = new UnitLinkPremium("39895277", "0306",
		 * "25610620"); mp.premiumMaster("39895277", "0306", "25610620");
		 * //mp.getValue(); mp.calPremium();
		 * 
		 * // System.out.println(" <> "+mp.getValue().getClass()); Vector v =
		 * mp.rider();
		 * 
		 * 
		 * for(int i=0;i< v.size();i++) { System.out.println(":"+i+":"+v.elementAt(i));
		 * String [] rd = (String[])v.elementAt(i); for (int j = 0; j < rd.length; j++)
		 * { System.out.println("-*-*-*-"+rd[0]+" ** "+rd[1]+" *** "+rd[2]); } }
		 */

		// rb = checkChangeRider(riderfile.get("riderType"));
		// System.out.println("have change "+rb.get("riderType")+ "
		// "+rb.get("riderSum")+ " "+rb.get("riderStatusDate"));
		//
		// System.out.println("b "+mp.lifePremium());
		// System.out.println(" :: "+v.size());

		/**
		 * Result rs = PublicRte.getResult("blmaster", "rte.bl.master.FindTypePolicy",
		 * "22788186"); System.out.println(rs.value());
		 **/

		/**
		 * IServiceRte i = new IServiceRte(); i.showHistoryCondpay("32120768");
		 **/

		/**
		 * Result result =
		 * PublicRte.getClientResult("blunitlink","rte.bl.unitlink.service.cv.RteServicePaymentTransaction",
		 * new String[] {"32120768"}); if (result.status() != 0) { if(result.value()
		 * instanceof String) throw new Exception((String)result.value()); throw
		 * ((Exception)result.value()); }
		 **/

		/**
		 * Result result =
		 * PublicRte.getClientResult("blunitlink","rte.bl.unitlink.service.cv.RteServicePaymentTransaction",
		 * new String[] {"32120768"}); if (result.status() != 0) { if(result.value()
		 * instanceof String) throw new Exception((String)result.value()); throw
		 * ((Exception)result.value()); }
		 * //return((ArrayList<ServicePaymentTransaction>)result.value());
		 * ArrayList<ServicePaymentTransaction> a =
		 * (ArrayList<ServicePaymentTransaction>)result.value();
		 * 
		 * System.out.println(a.get(0).getAv()); System.out.println(result.status());
		 * System.out.println(result.value());
		 **/
	}

	/**
	 * public static void main(String[] args) { //PublicRte.setRemote(false);
	 * IServiceRte iservice = new IServiceRte(); iservice.findPolicy("34083926");
	 * //iservice.getPolicy("3860200287726"); //iservice.getRider(); //String[] a =
	 * iservice.getMasterDetail(); //System.out.println(Arrays.toString(a)); }
	 * 
	 * @throws Exception
	 **/

	public Vector getPolicyStat1(String idNo) throws Exception {
		System.out.println("getPolicyStatus1");
		Vector v = new Vector();
		try {
			String[] param = { "A", idNo };
			String policy = null;
			String braCode = null;
			vName[0] = new Vector();
			vName[1] = new Vector();
			PublicRte.setRemote(true);
			System.out.println("Before new MasterFile getPolicyStatus1");
			file = new MasterFile();
			System.out.println("Before findMasterByIdNo getPolicyStatus1");

			if (!file.findMasterByIdNo(param))
				System.out.println("not found");
			if (file.vSearchIdNo[0].size() != 0 || file.vSearchIdNo[1].size() != 0 || file.vSearchIdNo[2].size() != 0) {
				for (int i = 0; i < file.vSearchIdNo[0].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[0].elementAt(i);
					policy = fmast.get("policyNo") + "$" + fmast.get("policyStatus1");

					System.out.println("1policyno = " + policy);
					v.addElement(policy);
				}
				for (int i = 0; i < file.vSearchIdNo[1].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[1].elementAt(i);
					policy = fmast.get("policyNo") + "$" + fmast.get("policyStatus1");
					System.out.println("2policyno = " + policy);
					v.addElement(policy);
				}
				for (int i = 0; i < file.vSearchIdNo[2].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[2].elementAt(i);
					policy = fmast.get("policyNo") + "$" + fmast.get("policyStatus1");
					System.out.println("3policyno = " + policy);
					v.addElement(policy);
				}
			} else {
				System.out.println("not found policy");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("getPolicyStat1|idNo:" + idNo, e);
		}
		return v;
	}

	public Vector getPolicy(String idNo) throws Exception {
		System.out.println("getPolicy");
		Vector v = new Vector();
		try {
			String[] param = { "A", idNo };
			String policy = null;
			String braCode = null;
			vName[0] = new Vector();
			vName[1] = new Vector();
			PublicRte.setRemote(true);
			System.out.println("Before new MasterFile getPolicy ::::::  ");
			file = new MasterFile();
			file.findMasterByIdNo(param);
			System.out.println("Before findMasterByIdNo getPolicy :::::: ");
			System.out.println("test");
			System.out.println(file.vSearchIdNo[5].size());
			if (file.vSearchIdNo[0].size() != 0 || file.vSearchIdNo[1].size() != 0 || file.vSearchIdNo[2].size() != 0
					|| file.vSearchIdNo[3].size() != 0 || file.vSearchIdNo[5].size() != 0) {
				for (int i = 0; i < file.vSearchIdNo[0].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[0].elementAt(i);
					policy = fmast.get("policyNo");
					System.out.println("0policyno = " + policy);
					v.addElement(policy);
				}
				for (int i = 0; i < file.vSearchIdNo[1].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[1].elementAt(i);
					policy = fmast.get("policyNo");
					System.out.println("1policyno = " + policy);
					v.addElement(policy);
				}
				for (int i = 0; i < file.vSearchIdNo[2].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[2].elementAt(i);
					policy = fmast.get("policyNo");
					System.out.println("2policyno = " + policy);
					v.addElement(policy);
				}
				for (int i = 0; i < file.vSearchIdNo[3].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[3].elementAt(i);
					policy = fmast.get("policyNo");
					System.out.println("3policyno = " + policy);
					v.addElement(policy);
				}
				// HaGi Nutt Edit Unitlink 20170927
				for (int i = 0; i < file.vSearchIdNo[5].size(); i++) {
					Record fmast = (Record) file.vSearchIdNo[5].elementAt(i);
					policy = fmast.get("policyNo");
					System.out.println("5policyno = " + policy);
					v.addElement(policy);
				}

			} else {
				System.out.println("getPolicy not found policy");
			}
		} catch (Exception e) {
			throw new Exception("getPolicy|idNo:" + idNo, e);
		}
		return v;
	}

	public IServiceRtePOL() {
	}

	public IServiceRtePOL(String qid, String answer, String idNo) throws NumberFormatException, Exception {
		System.out.println("verify = " + verify(Integer.parseInt(qid), answer, idNo));
	}

	public IServiceRtePOL(String policyNo) {
		// System.out.println("1 IserviceRTE");
		findPolicy(policyNo);
		String[] s = getMasterDetail();
		// System.out.println(" ** ** ** ** "+s[25]);
		// System.out.println(" -- -- -- -- "+s[17]);
		/*
		 * System.out.println("foundFlag =  "+foundFlag); for (int i = 0; i < s.length;
		 * i++) { System.out.println("s["+i+"] = " + Translator.utos(s[i])); }
		 */
		Vector v = getRider();
		for (int i = 0; i < v.size(); i++) {
			String[] rid = (String[]) v.elementAt(i);
			System.out.println("rid[" + i + "] = " + M.stou(rid[0] + ":" + rid[1] + ":" + rid[2] + ":" + rid[3]));
		}

		/*
		 * try { UnitLinkPremium v = new UnitLinkPremium("28868504","0301",""); Vector
		 * av = v.getIndRider(); System.out.println("aa"+av.size());
		 * 
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		/*
		 * Vector r1 = getReceipt(); for (int i = 0; i < r1.size(); i++) { String[] rcpt
		 * = (String[]) r1.elementAt(i);
		 * System.out.println("rcpt["+i+"] = "+rcpt[0]+":"+rcpt[1]+":"+rcpt[2]); }
		 */

		/*
		 * Vector r = getReceipt("2547","2533"); for (int i = 0; i < r.size(); i++) {
		 * String[] rcpt = (String[]) r.elementAt(i);
		 * System.out.println("rcpt["+i+"] = "+rcpt[0]+":"+rcpt[1]+":"+rcpt[2]); }
		 * Vector d = getDividend(); System.out.println("dividend.size() = " +
		 * d.size()); for (int i = 0; i < d.size(); i++) { String[] dividend =
		 * (String[]) d.elementAt(i); System.out.println("dividend["+i+"] = " +
		 * dividend[0]+":"+dividend[1]+":"+dividend[2]+":"+dividend[3]+":"+dividend[4]+
		 * ":"+dividend[5]); }
		 */
		/*
		 * Vector l = getLoan(); System.out.println("loan.size() = " + l.size()); for
		 * (int i = 0; i < l.size(); i++) { String loan = (String) l.elementAt(i);
		 * System.out.println("loan["+i+"] = "+ loan); }
		 */
		/*
		 * String result = ""; if(monthTaxCheck.equals("01") ||
		 * monthTaxCheck.equals("02") || monthTaxCheck.equals("03") ||
		 * monthTaxCheck.equals("04")) { result =
		 * printTax(policyNo,M.subnum(yearTaxCheck, "1")); } else { result =
		 * printTax(policyNo,yearTaxCheck);
		 * 
		 * }
		 * 
		 * /*String resultPrintwarn = printWarn(policyNo);
		 * System.out.println("result = "+ resultPrintwarn );
		 */
	}

	String[] tempF1 = { "payPeriod", "payDate", "rpNo", "premium", "mode", "riderPremium" };
	String[] tempF2 = { "policyNo", "kname", "address1", "address2", "insurer", "payer", "planName", "mode", "pPayYear",
			"premium", "totalPremium", "signature", "position", "depart", "taxYear" };

	int[] tempL1 = { 7, 8, 12, 12, 30, 12 };
	int[] tempL2 = { 8, 100, 200, 200, 70, 70, 50, 20, 2, 12, 12, 70, 50, 50, 4 };

	String[] key1 = { "payPeriod" };
	String[] key2 = { "policyNo" };
	String[] key3 = { "payDate", "payPeriod" };
	TempFile[] temp = new TempFile[2];
	TempFile tp;
	String form = "";
	boolean checkEff = false;

	void openTempFile() {
		temp[0] = new TempFile(tempF1, tempL1);
		temp[0].setNoOfKey(2);
		temp[0].setKey(0, key1);
		temp[0].setKey(1, key3);

		temp[1] = new TempFile(tempF2, tempL2);
		temp[1].setNoOfKey(1);
		temp[1].setKey(0, key2);

		String[] tfieldss = { "policyNo", "planCode", "addType", "countPay" };
		int[] tlenss = { 8, 3, 2, 4 };

		tp = new TempFile(tfieldss, tlenss);
		tp.setNoOfKey(2);
		tp.setKey(0, new String[] { "addType", "countPay", "policyNo" });
		tp.setKey(1, new String[] { "policyNo" });
	}

	// public String printWarn(String policyNo)
	// {
	// String msg = "";
	// try
	// {
	// String catalina_home = System.getProperty("catalina.home");
	// String setPath = catalina_home+"/webapps/iservice/warn/";
	// if( policyNo.length() > 0 ){
	// PrintOnlyWarn printWarn
	// = new PrintOnlyWarn(policyNo);
	// String fileName = printWarn.pdfFileName();
	// if( catalina_home == null ){
	// msg = M.stou("Can't get path catalina_home at IServiceRte.printWarn()");
	// }else if( fileName.equals("") || fileName == null ){
	// msg = M.stou("Can't get path fileName at IServiceRte.printWarn()");
	// }else{
	// System.out.println("FileName : " + fileName);
	// String command = "mv "+fileName+" "+setPath;
	// System.out.println("Command : " + command);
	// Runtime.getRuntime().exec(command);
	// System.out.println("---- Finish to Move! ----");
	// msg = "success";
	// }
	// }
	// }catch( Exception e ){
	// e.printStackTrace();
	// System.out.println("Error IServiceRte.printWarn(): " + e.getMessage() );
	// msg = e.getMessage();
	// }
	// return msg;
	// }

	public String printTax(String policyNo, String taxyear) throws Exception {
		/* return String if not equal complete ==> error */
		/*
		 * findPolicy(policyNo); getMasterDetail(); this.taxyear = taxyear; checkEff =
		 * false; checkEff2 = false; try { System.out.println("step 1"); if
		 * (policyType.equals("I")) type = "ind"; else if (policyType.equals("W")) type
		 * = "whl"; else type = "ord"; openTempFile(); temp[0].removeAllData();
		 * temp[1].removeAllData(); System.out.println("step 2"); if (!ckNoPrint() ||
		 * plan.equals("NC") || plan.equals("ND") || plan.equals("TN") ||
		 * plan.equals("TP") || PlanType.isPAPlan(plan)) {
		 * System.out.println("Cannot print cer from this type of policy"); return
		 * M.stou(
		 * "à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸£à¸–à¹à¸Ÿà¸à¸­à¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸±à¸šà¹à¸Ÿà¸à¸­à¸‡à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸"
		 * ); } if (policyStatus1.equals("#") || policyStatus1.equals("N") ||
		 * policyStatus1.equals("*")) { System.out.println("Your policy is cancel");
		 * return M.stou(
		 * "à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸­à¸‡à¹à¸Ÿà¸à¹à¸Ÿà¸à¸²à¸™à¹à¸Ÿà¸à¹à¸Ÿà¸à¸¹à¸à¸¢à¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸´à¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸"
		 * ); } if (plan.equals("05") || plan.equals("08")) {
		 * System.out.println("Have special condition"); return M.
		 * stou("à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸­à¸™à¹„à¸‚à¸žà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ à¹à¸Ÿà¸à¹à¸Ÿà¸à¸¸à¸“à¸²à¸•à¸´à¸”à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ à¹à¸Ÿà¸à¹à¸Ÿà¸à¸§à¸™à¹à¸Ÿà¸à¹à¸Ÿà¸à¸´à¸à¹à¸Ÿà¸à¸£à¸œà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸²à¸›à¹à¸Ÿà¸à¸°à¸à¸±à¸™ à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ 02-2470247 à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ 3155 , 3156"
		 * ); } if (ckFreeLook()) { System.out.println("In freelook period"); return M.
		 * stou("à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ "
		 * )+policyNo+ M.
		 * stou(" à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹ƒà¸™à¸Šà¹à¸Ÿà¸à¸§à¸‡ freelook"
		 * ); } if (!isPolicyInYear()) {
		 * System.out.println("No have history of pay premium"); return M.
		 * stou("à¹à¸Ÿà¸à¹à¸Ÿà¸à¹ˆà¸žà¸šà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸±à¸•à¸´à¸à¹à¸Ÿà¸à¸£à¸Šà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸¢à¸‚à¸­à¸‡à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ "
		 * )+policyNo + M.stou(" à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸"); } if (
		 * !PlanType.isTaxDeductible(plan, sex, age, effectiveDate) ) {
		 * System.out.println("This plan cannot take premium to reduce tax"); return
		 * M.stou(
		 * "à¹à¸šà¸šà¹à¸Ÿà¸à¹à¸Ÿà¸à¸°à¸à¸±à¸™à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸£à¸–à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸±à¸à¸¥à¸”à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸­à¸™à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸"
		 * ); } System.out.println("step 3"); showData(); form = selectReport();
		 * System.out.println("step 4"); rcpVtr = new Vector(); //dlname =
		 * à¹à¸Ÿà¸à¸²à¸‡à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸
		 * à¹à¸Ÿà¸à¸¸à¸žà¹à¸Ÿà¸
		 * à¸¤à¸—à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ branchposition =
		 * à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸±à¸”à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸
		 * à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ if (!getRcpData()) //it's old method is
		 * showTable(policyNo, policyTye); return M.
		 * stou("à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸­à¸™à¹„à¸‚à¸žà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ à¹à¸Ÿà¸à¹à¸Ÿà¸à¸¸à¸“à¸²à¸•à¸´à¸”à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ à¹à¸Ÿà¸à¹à¸Ÿà¸à¸§à¸™à¹à¸Ÿà¸à¹à¸Ÿà¸à¸´à¸à¹à¸Ÿà¸à¸£à¸œà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸²à¸›à¹à¸Ÿà¸à¸°à¸à¸±à¸™ à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ 02-2470247 à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ 3155 , 3156"
		 * ); if (rcpVtr.size() == 0) return M.
		 * stou("à¹à¸Ÿà¸à¹à¸Ÿà¸à¹ˆà¸žà¸šà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸±à¸•à¸´à¸à¹à¸Ÿà¸à¸£à¸Šà¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸¢à¸‚à¸­à¸‡à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸ "
		 * )+policyNo; String print = "N"; getTruePeriod();
		 * System.out.println("step 5"); System.out.println("checkEff " + checkEff +
		 * " checkEff2 = " + checkEff2);
		 * //à¹à¸Ÿà¸à¸²à¸‡à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¹à¸Ÿà¸à¸£à¸”
		 * à¹à¸Ÿà¸ NormalView nmView = new
		 * NormalView(temp,form,transfertype,type,print,checkEff,M.
		 * mapt("okplbo zkl6dl,b9"),M.mapt("z^h0yfdkivk;6Fl"),branch,checkEff2);
		 * nmView.printR1(); System.out.println("step 6"); String catalina_home =
		 * System.getProperty("catalina.home"); Mpdf pdf = new Mpdf(new
		 * FileOutputStream(catalina_home+"/webapps/iservice/certificate/"+policyNo+
		 * ".pdf")); pdf.setPageSize(8.0d,11.0d); pdf.render(nmView, 0,
		 * nmView.getPageCount() - 1); System.out.println("step 7"); } catch (Exception
		 * e) { System.out.println("Error1 : "+ e.toString()); }
		 */
		try {
			String errMsg = "";
			TaxIService t = new TaxIService(policyNo, "000");
			errMsg = t.starPolicyNo(policyNo);
			System.out.println("show  errMsg.trim().length() >> " + errMsg.trim().length() + "    " + errMsg.trim());
			if (errMsg.trim().length() > 0)
				return errMsg;
			else {
				Mview nmView = t.getView();
				t.removeTempMasicFile();
				String catalina_home = System.getProperty("catalina.home");
				System.out.println("catalina.home=" + catalina_home);
				Mpdf pdf = new Mpdf(
						new FileOutputStream(catalina_home + "/webapps/iservice/certificate/" + policyNo + ".pdf"));
				// Mpdf pdf = new Mpdf(new FileOutputStream("/home/tleuser/"+policyNo+".pdf"));
				pdf.setPageSize(8.0d, 11.5d);
				pdf.render(nmView, 0, nmView.getPageCount() - 1);

			}

		} catch (Exception e) {
			System.out.println("printTax error : " + e.toString());
			throw new Exception("printTax|policyNo:" + policyNo + " taxyear:" + taxyear, e);
		}
		return "complete";
	}

	public String printTaxTong(String policyNo, String taxyear) throws Exception {
		try {
			String errMsg = "";
			TaxIService t = new TaxIService(policyNo, "000");
			errMsg = t.starPolicyNo(policyNo);
			System.out.println("show  errMsg.trim().length() >> " + errMsg.trim().length() + "    " + errMsg.trim());
			if (errMsg.trim().length() > 1)
				return errMsg;
			else {

				Mview nmView = t.getView();
				t.removeTempMasicFile();
				String catalina_home = System.getProperty("catalina.home");
				System.out.println("catalina.home=" + catalina_home);
				Mpdf pdf = new Mpdf(
						new FileOutputStream(catalina_home + "/webapps/iservice/certificate/" + policyNo + ".pdf"));
				// Mpdf pdf = new Mpdf(new FileOutputStream("/home/tleuser/"+policyNo+".pdf"));
				pdf.setPageSize(8.0d, 11.5d);
				pdf.render(nmView, 0, nmView.getPageCount() - 1);
				if ("C".equals(errMsg.trim()))
					return "C";
				else
					return "0";
			}

		} catch (Exception e) {
			System.out.println("printTax error : " + e.toString());
			throw new Exception("printTaxTong|policyNo:" + policyNo + " taxyear:" + taxyear, e);
//			return "printtaxerr " + e.getMessage();
		}
		// return "complete";

	}

	String insureyear;

	public void showData() {
		insureyear = "";
		if (policyType.equals("I"))
			insureyear = showInd();
		else if (policyType.equals("O"))
			insureyear = showOrd();
		else
			insureyear = showWhl();

	}

	public String showInd() {
		TLPlan tl = PlanSpec.getPlan(file.indmast.get("planCode"));
		return M.itoc(M.ctoi(tl.endowmentYear(file.indmast.get("insuredAge"))));
	}

	public String showOrd() {
		TLPlan tl = PlanSpec.getPlan(file.ordmast.get("planCode"));
		return M.itoc(M.ctoi(tl.endowmentYear(file.ordmast.get("insuredAge"))));
	}

	public String showWhl() {
		TLPlan tl = PlanSpec.getPlan(file.whlmast.get("planCode"));
		return M.itoc(M.ctoi(tl.endowmentYear(file.whlmast.get("insuredAge"))));
	}

	boolean payerFlag = false;
	boolean insurerFlag = true;
	boolean normalFlag = true;
	boolean evidentFlag = false;
	boolean companyFlag = false;

	public boolean ckCompany() {
		companyFlag = false;
		try {
			Result rs = PublicRte.getResult("blservice", "rte.bl.service.printtax.SearchFile",
					new String[] { "search", "bycompany@srvservice", policyNo });
			if (rs.status() == -1)
				throw new Exception((String) rs.value());
			else if (rs.status() == 0) {
				companyFlag = true;
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return false;
	}

	boolean checkEff2 = false;
	boolean bplancode = false;

	public String selectReport() {
		String form = "";
		if (M.cmps(effectiveDate, "25520101") >= 0)
			checkEff2 = true;
		if (companyFlag)
			form = "1";
		else if (plan.equals("NC") || plan.equals("ND"))
			form = "4";
		else
			form = "3";
		return form;
	}

	public String findTaxType() {
		return "N";
	}

	public boolean getRcpData() {
		boolean b = false;
		if (policyType.equals("I")) {
			b = getIndRcp();
		} else if (policyType.equals("O")) {
			b = getOrdRcp();
		} else {
			b = getWhlRcp();
		}
		return b;
	}

	public boolean getIndRcp() {
		try {
			temp[0].removeAllData();
			getWhlValue();
			rcpVtr = new Vector();
			boolean ok = temp[0].first();
			// String[] colmn = new String[7];
			while (ok) {
				String[] colmn = new String[7];
				colmn[0] = showMYfield(temp[0].get("payPeriod"), true);
				colmn[1] = Fmode(temp[0].get("mode"));
				colmn[2] = showDfield(temp[0].get("payDate"), true);
				if ((temp[0].get("rpNo")).charAt(0) == 'A' || (temp[0].get("rpNo")).charAt(0) == 'B'
						|| (temp[0].get("rpNo")).charAt(0) == 'C')
					colmn[3] = temp[0].get("rpNo").substring(0, 8);
				else
					colmn[3] = temp[0].get("rpNo");
				if (plan.equals("MC") || plan.equals("MD") || plan.equals("MB") || plan.equals("MA")) {
					if (checkEff2) {
						String mul = M.multiply(sumAssure, "0.00036", 2);
						int mm = mul.indexOf('.');
						if (M.cmps(mul.substring(mm + 1), "00") != 0) {
							if (M.cmps(mul.substring(mm + 1), "50") < 0) {
								mul = M.multiply(mul, "1", 0);
								mul = M.addnum(mul, "1");
							} else
								mul = M.multiply(mul, "1", 0);
							System.out.println("======== MUL ========== " + mul);
						}
						if (M.cmps(effectiveDate, "25520101") < 0) {
							colmn[4] = temp[0].get("premium");
							colmn[5] = "0";
						} else {
							colmn[4] = M.subnum(temp[0].get("premium"), mul, 0);
							colmn[5] = M.addnum(mul, "0", 0);
						}

					} else {
						if (M.cmps(effectiveDate, "25520101") < 0) {
							colmn[4] = M.addnum(temp[0].get("premium"), temp[0].get("riderPremium"));
							colmn[5] = "0";
						} else {
							colmn[4] = temp[0].get("premium");
							colmn[5] = temp[0].get("riderPremium");
						}
					}
				} else {
					if (M.cmps(effectiveDate, "25520101") < 0) {
						colmn[4] = M.addnum(temp[0].get("premium"), temp[0].get("riderPremium"));
						colmn[5] = "0";
					} else {
						colmn[4] = temp[0].get("premium");
						colmn[5] = temp[0].get("riderPremium");
					}
				}
				colmn[6] = M.addnum(colmn[4], colmn[5], 0);
				rcpVtr.add(colmn);

				ok = temp[0].next();
			}
		} catch (Exception e) {
			return false;
		}
		for (int i = 0; i < rcpVtr.size(); i++) {
			String[] s = (String[]) rcpVtr.elementAt(i);
			System.out.println("rpNo = " + s[3]);
		}
		return true;
	}

	public static String showYMfield(String s) {
		String year = s.substring(0, 2), period = s.substring(2, 4);
		return year + "/" + period;
	}

	public boolean getWhlRcp() {
		try {
			String payperiod;
			temp[0].removeAllData();
			getWhlValue();
			temp[0].changeKey(1);
			boolean ok = temp[0].first();
			while (ok) {
				String[] colmn = new String[7];
				payperiod = temp[0].get("payPeriod");
				colmn[0] = showYMfield(payperiod);
				colmn[1] = Fmode(temp[0].get("mode"));
				colmn[2] = showDfield(temp[0].get("payDate"), true);
				if ((temp[0].get("rpNo")).charAt(0) == 'A' || (temp[0].get("rpNo")).charAt(0) == 'B'
						|| (temp[0].get("rpNo")).charAt(0) == 'C')
					colmn[3] = temp[0].get("rpNo").substring(0, 8);
				else
					colmn[3] = temp[0].get("rpNo");
				if (M.cmps(effectiveDate, "25520101") < 0) {
					colmn[4] = M.addnum(temp[0].get("premium"), temp[0].get("riderPremium"));
					colmn[5] = "0";
				} else {
					colmn[4] = temp[0].get("premium");
					colmn[5] = temp[0].get("riderPremium");
				}
				colmn[6] = M.addnum(temp[0].get("premium"), temp[0].get("riderPremium"));
				rcpVtr.add(colmn);
				ok = temp[0].next();
			}
			temp[0].changeKey(0);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean getOrdRcp() {
		System.out.println("getOrdRcp taxyear=" + taxyear + " policyNo = " + policyNo);
		rcpVtr = new Vector();
		boolean ckncase = true;
		try {
			Result result = PublicRte.getResult("blservice", "rte.bl.service.printtax.SearchRepForPrintTax",
					new String[] { "boolrpoh", taxyear, policyNo });
			if (result.status() == -1) {
				System.out.println("result == -1 value = " + result.value());
				throw new Exception((String) result.value());
			} else if (result.status() == 0)
				ckncase = false;
			getOrdMain();
			temp[0].changeKey(1);
			boolean ok = temp[0].first();
			while (ok) {
				String[] colmn = new String[7];
				colmn[0] = showYMfield(temp[0].get("payPeriod"));
				colmn[1] = Fmode(temp[0].get("mode"));
				colmn[2] = showDfield(temp[0].get("payDate"), true);
				if ((temp[0].get("rpNo")).charAt(0) == 'A' || (temp[0].get("rpNo")).charAt(0) == 'B'
						|| (temp[0].get("rpNo")).charAt(0) == 'C')
					colmn[3] = temp[0].get("rpNo").substring(0, 8);
				else
					colmn[3] = temp[0].get("rpNo");
				if (M.cmps(effectiveDate, "25520101") < 0) {
					if (PlanType.isPAPlan(plan)) {
						colmn[4] = temp[0].get("premium");
						colmn[5] = temp[0].get("riderPremium");
					} else {
						colmn[4] = M.addnum(temp[0].get("premium"), temp[0].get("riderPremium"));
						colmn[5] = "0";
					}
				} else {
					colmn[4] = temp[0].get("premium");
					colmn[5] = temp[0].get("riderPremium");
				}
				colmn[6] = M.addnum(temp[0].get("premium"), temp[0].get("riderPremium"));
				rcpVtr.add(colmn);
				ok = temp[0].next();
			}
			temp[0].changeKey(0);

		} catch (Exception e) {
			return false;
		}
		System.out.println("rcpVtr.size() = " + rcpVtr.size());
		for (int i = 0; i < rcpVtr.size(); i++) {
			String[] s = (String[]) rcpVtr.elementAt(i);
			System.out.println("rpNo = " + s[3]);
		}

		return true;
	}

	public void getOrdMain() throws Exception {
		System.out.println("getOrdMain");
		try {
			temp[0].removeAllData();
			String payperiod, paydate;
			Result rs = PublicRte.getResult("blservice", "rte.bl.service.printtax.SearchRepForPrintTax",
					new String[] { "vectorperiod", taxyear, policyNo, type });
			if (rs.status() == -1) {
				System.out.println("getOrdMain status = -1 ==> taxyear = " + taxyear + " policyNo = " + policyNo
						+ " type = " + type);
				throw new Exception((String) rs.value());
			} else if (rs.status() == 0) {
				Vector vData = (Vector) rs.value();
				System.out.println("status = 0 size of data = " + vData.size() + " ==> taxyear = " + taxyear
						+ " policyNo = " + policyNo + " type = " + type);
				for (int i = 0; i < vData.size(); i++) {
					String[] array = (String[]) vData.elementAt(i);
					payperiod = array[0];
					paydate = array[1];
					temp[0].newRecord();
					temp[0].set("payPeriod", payperiod);
					temp[0].set("payDate", paydate);
					temp[0].set("rpNo", array[2]);
					temp[0].set("mode", array[4]);
					temp[0].set("premium", array[3]);
					temp[0].set("riderPremium", array[6]);
					temp[0].add();
				}
			} else {
				System.out.println("status = " + rs.status() + " ==> taxyear = " + taxyear + " policyNo = " + policyNo
						+ " type = " + type);
			}
		} catch (Exception e) {
			System.out.println("Error getOrdMain : " + e.toString());
			throw e;
		}
	}

	String[] smode = { M.stou("à¸£à¸²à¸¢à¹€à¸”à¸·à¸­à¸™"), M.stou("à¸£à¸²à¸¢ 3 à¹€à¸”à¸·à¸­à¸™ "),
			M.stou("à¸£à¸²à¸¢ 6 à¹€à¸”à¸·à¸­à¸™"), M.stou("à¸£à¸²à¸¢à¸›à¸µ"),
			M.stou("à¸Šà¸³à¸£à¸°à¸„à¸£à¸±à¹‰à¸‡à¹€à¸”à¸µà¸¢à¸§") };

	public String Fmode(String mode) {
		if (mode.equals("0"))
			return smode[0];
		else if (mode.equals("1"))
			return smode[3];
		else if (mode.equals("2"))
			return smode[2];
		else if (mode.equals("4"))
			return smode[1];
		else if (mode.equals("9"))
			return smode[4];
		return "-";
	}

	public String showMYfield(String s, boolean ok) {
		String year = "", month = "", my = "";
		if (ok) {
			month = s.substring(4, 6);
			year = s.substring(0, 4);
			my = year + "/" + month;
		} else {
			month = s.substring(2, 4);
			year = s.substring(0, 2);
			my = year + "/" + month;
		}
		return my;
	}

	public String showDfield(String s, boolean ok) {
		String year = "", month = "", day = "", df = "";

		if (ok) {
			day = s.substring(6);
			month = s.substring(4, 6);
			year = s.substring(0, 4);
			df = day + "/" + month + "/" + year;
		} else {
			day = s.substring(4);
			month = s.substring(2, 4);
			year = s.substring(0, 2);
			df = day + "/" + month + "/" + year;
		}
		return df;
	}

	public void getWhlValue() throws Exception {
		String payperiod, paydate;
		try {
			Result rs = PublicRte.getResult("blservice", "rte.bl.service.printtax,SearchRepForPrintTax",
					new String[] { "vectorperiod", taxyear, policyNo, type });
			if (rs.status() == -1)
				throw new Exception((String) rs.value());
			else if (rs.status() == 0) {
				Vector vdata = (Vector) rs.value();
				for (int i = 0; i < vdata.size(); i++) {
					String[] array = (String[]) vdata.elementAt(i);
					payperiod = array[0];
					paydate = array[1];
					String checkPremium = "000";
					temp[0].newRecord();
					temp[0].set("payPeriod", payperiod);
					temp[0].set("payDate", paydate);
					temp[0].set("rpNo", array[2]);
					temp[0].set("mode", array[4]);
					temp[0].set("premium", array[3]);
					temp[0].set("riderPremium", array[6]);
					temp[0].add();
				}
			}
		} catch (Exception e) {
			if (M.cmps(e.getMessage().trim(), "E99") == 0) {
				throw new Exception(M.stou(
						"à¸‡à¸§à¸”à¸Šà¸³à¸£à¸°à¸‹à¹‰à¸³à¹„à¸¡à¹ˆà¸ªà¸²à¸¡à¸²à¸£à¸–à¸­à¸­à¸à¸«à¸™à¸±à¸‡à¸ªà¸·à¸­à¸£à¸±à¸šà¸£à¸­à¸‡à¹„à¸”à¹‰ "));
			} else {
				throw new Exception(e.getMessage());
			}
		}
	}

	public void setChoice1(boolean b) {
		payerFlag = b;
		insurerFlag = b;
	}

	public void setChoice2(boolean b) {
	}

	public void setChoice3(boolean b) {
		normalFlag = b;
		evidentFlag = b;
	}

	public boolean ckNoPrint() {
		if (plan.equals("TP") || plan.equals("TN"))
			return false;
		return true;
	}

	public boolean ckFreeLook() {
		Result rs = PublicRte.getResult("blservice", "rte.bl.service.printtax.SearchFile",
				new String[] { "free", type, policyNo });
		if (rs.status() == -1) {
			;
		} else if (rs.status() == 1) {
			if (!((Boolean) rs.value()).booleanValue())
				return false;
		} else {
			if (((Boolean) rs.value()).booleanValue())
				return true;
		}
		return false;
	}

	public boolean isPolicyInYear() {
		boolean ok = false;
		try {
			System.out.println("Method isPolicyInYear taxyear = " + taxyear + " policyNo  = " + policyNo
					+ " policyType = " + type);
			Result rs = PublicRte.getResult("blservice", "rte.bl.service.printtax.SearchRepForPrintTax",
					new String[] { "boolrp", taxyear, policyNo, type });
			if (rs.status() == -1) {
				System.out.println("status = -1");
				throw new Exception((String) rs.value());
			} else if (rs.status() == 0)
				ok = true;
		} catch (Exception e) {
		}
		return ok;
	}

	public String ckPrinted() {
		String type = "";
		try {
			Result result = PublicRte.getResult("blservice", "rte.bl.service.printtax.SearchFile",
					new String[] { "taxx", "taxdetail" + taxyear + "@srvservice", policyNo });
			if (result.status() == -1)
				throw new Exception((String) result.value());
			else if (result.status() == 0) {
				Vector vdata = (Vector) result.value();
				Rtaxdetailyyyy tax = new Rtaxdetailyyyy();
				for (int i = 0; i < vdata.size(); i++) {
					tax.putBytes((byte[]) vdata.elementAt(i));
					type = tax.get("taxType");
					if ("EN".indexOf(type) >= 0)
						return type = M.stou(
								"à¸¡à¸µà¸à¸²à¸£à¸­à¸­à¸à¸«à¸™à¸±à¸‡à¸ªà¸·à¸­à¹ƒà¸™à¸™à¸²à¸¡à¸šà¸¸à¸„à¸„à¸¥à¹à¸¥à¹‰à¸§");
					else if ("CL".indexOf(type) >= 0)
						return type = M.stou(
								"à¸¡à¸µà¸à¸²à¸£à¸­à¸­à¸à¸«à¸™à¸±à¸‡à¸ªà¸·à¸­à¹ƒà¸™à¸™à¸²à¸¡à¸šà¸£à¸´à¸©à¸±à¸—à¹à¸¥à¹‰à¸§");

				}
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return type;
	}

	Vector rcpVtr = new Vector();

	public void getTruePeriod() {
		System.out.println("getTruePeriod rcpVtr.size() = " + rcpVtr.size());
		String period;
		int premium = 0, totalpremium = 0;
		int rows = rcpVtr.size();
		checkEff = false;
		String[] data = null;
		try {
			temp[0].removeAllData();
			temp[1].removeAllData();
			if (policyType.compareTo("I") == 0) {
				System.out.println("row" + rows);
				for (int i = 0; i < rows; i++) {
					data = (String[]) rcpVtr.elementAt(i);
					if (M.cmps(data[2], "25520101") >= 0)
						checkEff = true;
					period = data[0];
					premium = M.ctoi(data[4]);
					temp[0].newRecord();
					temp[0].set("payPeriod", period);
					temp[0].set("payDate", data[2]);
					temp[0].set("rpNo", data[3]);
					temp[0].set("premium", data[4]);
					temp[0].set("mode", data[1]);
					temp[0].set("riderPremium", data[5]);
					temp[0].add();
					if (plan.equals("MC") || plan.equals("MD") || plan.equals("MB") || plan.equals("MA")) {
						if (checkEff2) {
							String mul = M.multiply(sumAssure, "0.00036", 2);
							int mm = mul.indexOf('.');
							if (M.cmps(mul.substring(mm + 1), "00") != 0) {
								if (M.cmps(mul.substring(mm + 1), "50") < 0) {
									mul = M.multiply(mul, "1", 0);
									mul = M.addnum(mul, "1");
								} else
									mul = M.multiply(mul, "1", 0);
							}
							totalpremium += premium;
						} else
							totalpremium += M.ctoi(M.addnum(data[4], data[5]));
					} else if (plan.equals("ME") || plan.equals("MF") || plan.equals("DN")) {
						if (checkEff2)
							totalpremium += (premium);
						else
							totalpremium += M.ctoi(data[4]) + M.ctoi(data[5]);
					} else if (plan.equals("TS") || plan.equals("TU"))
						totalpremium += premium;
					else {
						if (checkEff2)
							totalpremium += premium;
						else
							totalpremium += M.ctoi(data[4]) + M.ctoi(data[5]);
					}
				}
			} else {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ORD " + rows);
				for (int i = 0; i < rows; i++) {
					data = (String[]) rcpVtr.elementAt(i);
					period = data[0];
					premium = M.ctoi(data[4]);
					if (M.cmps(data[2], "25520101") >= 0)
						checkEff = true;
					temp[0].newRecord();
					temp[0].set("payPeriod", period.substring(0, 2) + period.substring(3));
					temp[0].set("payDate", DateInfo.unformatDate(data[2]));
					temp[0].set("rpNo", data[3]);
					temp[0].set("premium", data[4]);
					temp[0].set("mode", data[1]);
					temp[0].set("riderPremium", data[5]);
					temp[0].add();
					if (plan.equals("MC") || plan.equals("MD") || plan.equals("MB") || plan.equals("MA")) {
						if (checkEff2) {
							String mul = M.multiply(sumAssure, "0.00036", 2);
							int mm = mul.indexOf('.');
							if (M.cmps(mul.substring(mm), "00") != 0) {
								if (M.cmps(mul.substring(mm + 1), "50") < 0) {
									mul = M.multiply(mul, "1", 0);
									mul = M.addnum(mul, "1");
								} else
									mul = M.multiply(mul, "1", 0);
							}
							totalpremium += premium;
						} else
							totalpremium += M.ctoi(M.addnum(data[4], data[5]));
					} else if (plan.equals("ME") || plan.equals("MF") || plan.equals("DN")) {
						if (checkEff2)
							totalpremium += (premium);
						else
							totalpremium += M.ctoi(data[4]) + M.ctoi(data[5]);
					} else if (plan.equals("TS") || plan.equals("TU"))
						totalpremium += premium;
					else {
						if (checkEff2)
							totalpremium += premium;
						else
							totalpremium += (M.ctoi(data[4]) + M.ctoi(data[5]));
					}
				}
				System.out.println("----Totalpremium of getTruePeriod  " + totalpremium);
			}
			String signature = "", position = "", departname = "", departcode = "";
			signature = M.stou("à¸™à¸²à¸‡à¸ªà¸²à¸§à¸­à¸ à¸´à¸£à¸”à¸µ à¸§à¸‡à¸©à¹Œà¸›à¸£à¸°à¹€à¸ªà¸£à¸´à¸");
			position = M.stou("à¸œà¸¹à¹‰à¸ˆà¸±à¸”à¸à¸²à¸£à¸­à¸²à¸§à¸¸à¹‚à¸ª");
			departname = M.stou("à¸ªà¹ˆà¸§à¸™à¸šà¸£à¸´à¸à¸²à¸£à¸œà¸¹à¹‰à¹€à¸­à¸²à¸›à¸£à¸°à¸à¸±à¸™");

			departcode = "000";
			String kname = getLetterName(), payer = "", compcode, compname = "",
					insurer = preName.trim() + firstName.trim() + " " + lastName.trim();

			payer = compname;
			data = (String[]) rcpVtr.elementAt(0);
			temp[1].newRecord();
			temp[1].set("policyNo", policyNo);
			temp[1].set("kname", kname);
			temp[1].set("address1", setBlank(addr[0]));
			temp[1].set("address2", addr[1]);
			temp[1].set("insurer", insurer);
			temp[1].set("payer", payer);
			temp[1].set("planName", tlp.name());
			temp[1].set("mode", mode);
			temp[1].set("pPayYear", insureYear);
			temp[1].set("premium", M.addnum(data[4].toString(), data[5].toString()));
			temp[1].set("totalPremium", M.itoc(totalpremium));
			temp[1].set("signature", signature);
			temp[1].set("position", position);
			temp[1].set("depart", departname);
			temp[1].set("taxYear", taxyear);
			temp[1].add();
		} catch (Exception e) {
			System.out.println("Error getTruePeriod : " + e.toString());
		}
	}

	public String setBlank(String data) {
		if (data.length() > 100)
			return data.substring(0, 100);
		else {
			data = data + M.clears(' ', 100 - data.length());
			return data;
		}
	}

	String compname = "";

	public String getLetterName() {
		System.out.println("==============getLetterName");
		String kname = "", compid = "";
		boolean isCompany = false;

		if (ckCompany())
			isCompany = true;
		try {
			if (isCompany) {
				Result result = PublicRte.getResult("blservice", "rte.bl.service.printtax.SearchFile",
						new String[] { "scompany", policyNo });
				System.out.println("--------- scompany        " + result.status());
				if (result.status() == -1)
					throw new Exception((String) result.value());
				else if (result.status() == 0) {
					Rcompany rb = new Rcompany();
					Vector vc = (Vector) result.value();
					rb.putBytes((byte[]) vc.elementAt(0));
					compname = rb.get("companyPreName") + " " + rb.get("companyName");
					kname = M.stou("à¸œà¸¹à¹‰à¸ˆà¸±à¸”à¸à¸²à¸£à¸à¹ˆà¸²à¸¢à¸à¸²à¸£à¹€à¸‡à¸´à¸™") + "("
							+ rb.get("companyPreName") + " " + rb.get("companyName") + ")";
				}
			} else {
				if ((preName.trim()).equals(""))
					return (preName.trim() + firstName.trim() + "  " + lastName.trim());
				if (M.cmps(preName, M.stou("à¸™à¸²à¸¢")) == 0 || M.cmps(preName, M.stou("à¸™à¸²à¸‡")) == 0
						|| M.cmps(preName, M.stou("à¸™à¸²à¸‡à¸ªà¸²à¸§")) == 0
						|| M.cmps(preName, M.stou("à¸”.à¸Š.")) == 0 || M.cmps(preName, M.stou("à¸”.à¸.")) == 0
						|| M.cmps(preName, M.stou("à¸™.à¸ª.")) == 0
						|| M.cmps(preName, M.stou("à¹€à¸”à¹‡à¸à¸«à¸à¸´à¸‡")) == 0
						|| M.cmps(preName, M.stou("à¹€à¸”à¹‡à¸à¸Šà¸²à¸¢")) == 0) {
					kname = M.stou("à¸„à¸¸à¸“") + firstName.trim() + "  " + lastName.trim();
				} else {
					kname = preName + firstName.trim() + "  " + lastName.trim();
				}
			}

			System.out.println("-------getLetterName2 " + kname);
		} catch (Exception e) {
			// Msg.msg(this, e.getMessage(), Msg.ERROR, Msg.DEFAULT);
		}
		return kname;
	}

	public void findPolicy(String policyNo) {
		flagReceipt = false;
		file = new MasterFile();
		findByPolicy(new String[] { policyNo });
	}

	public boolean isFound() {
		return foundFlag;
	}

	void findByPolicy(String[] buffer) {
		System.out.println("findByPolicy :::::::::::::::: ");
		ZArray.show(buffer);

		policyNo = buffer[0];
		if (vName[0] != null) {
			vName[0].removeAllElements();
			vName[1].removeAllElements();
			vName[0] = null;
			vName[1] = null;
		}
		policyType = file.findMasterType(1, buffer);
		System.out.println("Start check policyType >> " + policyType);
		if (policyType.length() == 0)
			return;
		System.out.println("1 check policyType >> " + policyType);
		String[] param = { policyType, policyNo, "A" };
		if (file.checkDataMaster(param) != 0) {
			return;
		} else {
			if (!file.findByPolicyNo(policyType)) {
				return;
			}
		}
		System.out.println("END check policyType >> " + policyType);
		try {
//			 SearchMaster.searchServiceMaster(policyType,policyNo);
		} catch (Exception e) {
			return;
		}
		foundFlag = true;
	}

	String lastLifePremium = "";
	String lastExtraLifePremium = "";
	String insuredAge = "";

	String sumRppUL = "";
	String premRppUL = "";
	String nextpayperiod = "";
	String duedate = "";

	public String[] getMasterDetail() {
		System.out.println("start IServiceRteUL getMasterDetail policyType: " + policyType);
		/*
		 * s[0] = planName s[1] = effectiveDate s[2] = matureDate s[3] = status s[4] =
		 * sumAssure (rspsum for unit link) s[5] = mode s[6] = nextDueDate s[7] =
		 * nextPayPeriod s[8] = premiumAmount (rspPreemium for unit link) s[9] = address
		 * line 1 s[10] = address line 2 s[11] = firstName s[12] = lastName s[13] =
		 * telephone s[14] = policyStatus1 s[15] = planCode s[16] = branch s[17] =
		 * payPeriod s[18] = policyStatus2 s[19] = oldPolicyStatus1 s[20] = status1
		 * (oldPolicyStatus1 convert) s[21] = prename s[22] = dueDate s[23] = rppSum
		 * s[24] = rppPremium s[25] = policyType s[26] = topupPremium (policyType == U)
		 */
		// System.out.println(policyType);
		String[] masterDetail = { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "" };
		String effDate = "";
		String matureDate = "";
		String payMode = "";
		String nextDueDate = "";
		String nextPayPeriod = "";
		String premiumAmount = "";
		lastLifePremium = "0";
		lastExtraLifePremium = "0";
		System.out.println("getMasterDetail policyType : " + policyType);
		if (!foundFlag)
			return new String[] { "null" };
		if (policyType.equals("I")) {
			plan = file.indmast.get("planCode").trim();
			mstFile = file.indmast.getBytes();
			policyStatus1 = file.indmast.get("policyStatus1");
			masterDetail[0] = getPlanCode('I', file.indmast.get("planCode").trim());
			masterDetail[1] = file.indmast.get("effectiveDate");
			masterDetail[2] = indMatureDate(file.indmast.get("planCode").trim(), file.indmast.get("effectiveDate"));
			masterDetail[3] = file.indmast.get("policyStatus1");
			masterDetail[4] = file.indmast.get("sum");
			masterDetail[5] = "0";
			nextPayPeriod = nextPayPeriod(file.indmast.get("payPeriod"), "0    ", "I");
			masterDetail[6] = nextPayPeriod.substring(3) + nextPayPeriod.substring(0, 2) + "01";
			masterDetail[7] = nextPayPeriod;
			masterDetail[8] = file.indmast.get("lifePremium");
			masterDetail[9] = file.addr[0];
			masterDetail[10] = file.addr[1];
			masterDetail[11] = file.name.get("firstName");
			masterDetail[12] = file.name.get("lastName");
			masterDetail[16] = file.indmast.get("branch");
			masterDetail[15] = file.indmast.get("planCode");
			masterDetail[17] = file.indmast.get("payPeriod");
			masterDetail[18] = file.indmast.get("policyStatus2");
			masterDetail[19] = file.indmast.get("oldPolicyStatus1");
			masterDetail[22] = nextPayPeriod.substring(3) + nextPayPeriod.substring(0, 2) + "01";
			tlp = PlanSpec.getPlan(file.indmast.get("planCode"));
			insureYear = M.itoc(M.ctoi(tlp.endowmentYear(file.indmast.get("insuredAge"))));
			insuredAge = file.indmast.get("insuredAge");
			payPeriod = file.indmast.get("payPeriod");
			lastLifePremium = file.indmast.get("lifePremium");
			masterDetail[25] = policyType;

		} else if (policyType.equals("O")) {
			payMode = file.ordmast.get("mode");
			mstFile = file.ordmast.getBytes();
			plan = file.ordmast.get("planCode").trim();
			policyStatus1 = file.ordmast.get("policyStatus1");
			masterDetail[0] = getPlanCode('O', file.ordmast.get("planCode").trim());
			masterDetail[1] = file.ordmast.get("effectiveDate");
			masterDetail[3] = file.ordmast.get("policyStatus1");
			masterDetail[4] = file.ordmast.get("sum");
			masterDetail[5] = file.ordmast.get("mode");
			payPeriod = file.ordmast.get("payPeriod");
			matureDate = "";
			effDate = file.ordmast.get("effectiveDate");
			// effDate =
			// M.addnum(effDate.substring(0,4),M.subnum(payPeriod.substring(0,2),"1"))+effDate.substring(4);
			nextPeriod = nextPayPeriod(payPeriod, payMode, "O");
			try {
				Result rs = PublicRte.getResult("blmaster", "rte.bl.master.RteMasterPremium",
						new String[] { policyNo, payPeriod });
				if (rs.status() == 0) {
					md = (Vector) rs.value();
					vrid = (Vector) md.elementAt(0);
				}
				Result pop = PublicRte.getResult("blreceipt", "rte.bl.receipt.LastPremium",
						new String[] { "O", policyNo, payMode, payPeriod, file.ordmast.get("rpNo") });
				String prem = "";
				String endPrem = "0";
				if (pop.status() == 0) {
					prem = (String) pop.value();
					endPrem = prem;
					lastLifePremium = prem;
				}
				String extraPrem = "0";
				String lifePrem = "";
				Insure insure = new Insure();
				if (ckPlan.isSinglePremiumPlan(plan)) {
					for (int i = 0; i < file.vExtra.size(); i++) {
						Record rd = (Record) file.vExtra.elementAt(i);
						if (rd.get("extraType").equals(M.mapt("=;"))) {
							extraPrem = M.addnum(extraPrem, rd.get("extraPremium"));
						}
					}
					lifePrem = file.ordmast.get("lifePremium");
				} else {
					lifePrem = getLifePrem(false);
					lastLifePremium = lifePrem;
					lastExtraLifePremium = getLifePrem(true);
				}
				if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan)) {
					payPeriod = "0101";
					effDate = M.addnum(effDate.substring(0, 4),
							M.subnum(file.ordmast.get("payPeriod").substring(0, 2), "1")) + effDate.substring(4);
					if (effDate.substring(4, 8).compareTo("0229") == 0) {
						int days = DateInfo.daysInMonth(effDate);
						effDate = effDate.substring(0, 6) + M.setlen(M.itoc(days), 2);
					}
					if (!M.dateok(effDate))
						effDate = effDate.substring(0, 6) + M.subnum(effDate.substring(6), "1");
					matureDate = M.addnum(effDate.substring(0, 4), "1") + effDate.substring(4);
				} else {
					matureDate = (String) md.elementAt(12);
				}

				if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan) || ckPlan.isSinglePremiumPlan(plan)
						|| ("MF").indexOf(file.ordmast.get("policyStatus1")) >= 0) {
					nextDueDate = "00000000";
					nextPayPeriod = "0000";
					premiumAmount = "000000000";
				} else {
					nextPayPeriod = nextPayPeriodwithSlash(file.ordmast.get("payPeriod"), file.ordmast.get("mode"),
							"O");
					nextDueDate = insure.dueDate(file.ordmast.get("mode"), effDate,
							nextPayPeriod.substring(0, 2) + nextPayPeriod.substring(3));
					try {
						Result rsnext = PublicRte.getResult("blmaster", "rte.bl.master.RteMasterPremium",
								new String[] { policyNo, nextPayPeriod.substring(0, 2) + nextPayPeriod.substring(3) });
						if (rsnext.status() == 0) {
							Vector rv = (Vector) rsnext.value();
							vrid = (Vector) rv.elementAt(0);
							premiumAmount = (String) rv.elementAt(9);
						} else {
							System.out.println("rsnext.status = " + rsnext.status());
						}

					} catch (Exception e) {
						System.out.println("calNextPremium - " + e.getMessage());
					}

					// premiumAmount
					// screen.setEnableNextPeriod
				}
				if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan)) {
					payPeriod = "0101";
					effDate = M.addnum(effDate.substring(0, 4),
							M.subnum(file.ordmast.get("payPeriod").substring(0, 2), "1")) + effDate.substring(4);
					if (effDate.substring(4, 8).compareTo("0229") == 0) {
						int days = DateInfo.daysInMonth(effDate);
						effDate = effDate.substring(0, 6) + M.setlen(M.itoc(days), 2);
					}
					if (!M.dateok(effDate))
						effDate = effDate.substring(0, 6) + M.subnum(effDate.substring(6), "1");
					matureDate = M.addnum(effDate.substring(0, 4), "1") + effDate.substring(4);
				} else {
					matureDate = (String) md.elementAt(12);
				}
			} catch (Exception e) {
				throw e;
			}
			masterDetail[2] = matureDate;
			masterDetail[6] = nextDueDate;
			masterDetail[7] = nextPayPeriod;
			masterDetail[8] = premiumAmount;
			System.out.println("nextDueDate = " + nextDueDate + " nextPayPeriod = " + nextPayPeriod);
			masterDetail[9] = file.addr[0];
			masterDetail[10] = file.addr[1];
			masterDetail[11] = file.name.get("firstName");
			masterDetail[12] = file.name.get("lastName");
			masterDetail[16] = file.ordmast.get("branch");
			masterDetail[15] = file.ordmast.get("planCode");
			masterDetail[17] = file.ordmast.get("payPeriod");
			masterDetail[18] = file.ordmast.get("policyStatus2");
			masterDetail[19] = file.ordmast.get("oldPolicyStatus1");
			masterDetail[22] = file.ordmast.get("dueDate");

			tlp = PlanSpec.getPlan(file.ordmast.get("planCode"));
			insureYear = M.itoc(M.ctoi(tlp.endowmentYear(file.ordmast.get("insuredAge"))));
			insuredAge = file.ordmast.get("insuredAge");
			payPeriod = file.ordmast.get("payPeriod");

			masterDetail[25] = policyType;
		}

		// edit by tong universal life
		else if (policyType.equals("U")) {

			System.out.println("test topup : " + file.ulmast.get("topUpPremium"));

			payMode = file.ulmast.get("mode");
			mstFile = file.ulmast.getBytes();
			plan = file.ulmast.get("planCode").trim();
			policyStatus1 = file.ulmast.get("policyStatus1");
			masterDetail[0] = getPlanCode('U', file.ulmast.get("planCode").trim());
			masterDetail[1] = file.ulmast.get("effectiveDate");
			masterDetail[3] = file.ulmast.get("policyStatus1");
			masterDetail[4] = file.ulmast.get("sum");
			masterDetail[5] = file.ulmast.get("mode");
			masterDetail[26] = file.ulmast.get("topUpPremium");
			payPeriod = file.ulmast.get("payPeriod");
			matureDate = "";
			effDate = file.ulmast.get("effectiveDate");
			// effDate =
			// M.addnum(effDate.substring(0,4),M.subnum(payPeriod.substring(0,2),"1"))+effDate.substring(4);
			nextPeriod = nextPayPeriod(payPeriod, payMode, "O");
			try {
				System.out.println(" md 12  payPeriod " + payPeriod + "|nextPeriod : " + nextPeriod);
				// Result rs = PublicRte.getResult("blmaster","rte.bl.master.RteMasterPremium",
				// new String[] {policyNo,payPeriod});
				// Big 11/09/2018
				// Result rs =
				// PublicRte.getResult("blmaster","rte.bl.universal.master.RteUniversalPremium",
				// new String[] {policyNo,payPeriod,masterDetail[1]});
				Result rs = PublicRte.getResult("blmaster", "rte.bl.universal.master.RteUniversalPremium",
						new String[] { policyNo, nextPeriod, masterDetail[1] });
				System.out.println(" md 12  rs.status()   " + rs.status() + "  " + policyNo);
				// System.out.println(" md 12 xxxxx "+(String)rs.value() );
				if (rs.status() == 0) {
					md = (Vector) rs.value();

					System.out.println(" md 12   }}>> " + md.get(12));
					vrid = (Vector) md.elementAt(0);
				}
				System.out.println("policyType.equals(\"U\") ======================================= " + "U" + "|"
						+ policyNo + "|" + payMode + "|" + payPeriod + "|" + file.ulmast.get("rpNo") + "|");
				Result pop = PublicRte.getResult("blreceipt", "rte.bl.receipt.LastPremium",
						new String[] { "U", policyNo, payMode, payPeriod, file.ulmast.get("rpNo") });

				System.out.println("Result pop " + pop.status() + "|" + pop.value() + "|");
				String prem = "";
				String endPrem = "0";
				if (pop.status() == 0) {
					prem = (String) pop.value();
					endPrem = prem;
					lastLifePremium = prem;
				}
				String extraPrem = "0";
				String lifePrem = "";
				Insure insure = new Insure();
				System.out.println("lastLifePremium===================" + lastLifePremium + "|");

				System.out.println(
						"plan============================" + plan + "|" + ckPlan.isSinglePremiumPlan(plan) + "|");
				if (ckPlan.isSinglePremiumPlan(plan)) {
					System.out.println("vExtra============================" + file + "|" + file.vExtra.size());
					for (int i = 0; i < file.vExtra.size(); i++) {
						Record rd = (Record) file.vExtra.elementAt(i);
						if (rd.get("extraType").equals(M.mapt("=;"))) {
							extraPrem = M.addnum(extraPrem, rd.get("extraPremium"));
						}
					}
					System.out.println("vExtra1============================" + file.ulmast + "|");
					// System.out.println("vExtra2============================" +
					// (file.ulmast.get("lifePremium")) +"|");
					// lifePrem = file.ulmast.get("lifePremium");
				} else {
					System.out.println("getLifePrem(false) : " + getLifePrem(false));
					lifePrem = getLifePrem(false);
					lastLifePremium = lifePrem;
					lastExtraLifePremium = getLifePrem(true);
				}
				System.out
						.println("planType : " + plan + (ckPlan.isPAPlan(plan) + "|" + ckPlan.isDMPAPlan(plan) + "|"));
				if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan)) {
					payPeriod = "0101";
					effDate = M.addnum(effDate.substring(0, 4),
							M.subnum(file.ulmast.get("payPeriod").substring(0, 2), "1")) + effDate.substring(4);
					if (effDate.substring(4, 8).compareTo("0229") == 0) {
						int days = DateInfo.daysInMonth(effDate);
						effDate = effDate.substring(0, 6) + M.setlen(M.itoc(days), 2);
					}
					if (!M.dateok(effDate))
						effDate = effDate.substring(0, 6) + M.subnum(effDate.substring(6), "1");
					matureDate = M.addnum(effDate.substring(0, 4), "1") + effDate.substring(4);
				} else {
					matureDate = (String) md.elementAt(12);
				}

				if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan) || ckPlan.isSinglePremiumPlan(plan)
						|| ("MF").indexOf(file.ulmast.get("policyStatus1")) >= 0) {
					nextDueDate = "00000000";
					nextPayPeriod = "0000";
					premiumAmount = "000000000";
				} else {

					System.out.println(
							" payPeriod  " + file.ulmast.get("payPeriod") + "   mode " + file.ulmast.get("mode"));
					nextPayPeriod = nextPayPeriod(file.ulmast.get("payPeriod"), file.ulmast.get("mode"), "O");
					// nextDueDate =
					// insure.dueDate(file.ulmast.get("mode"),effDate,nextPayPeriod.substring(0,2)+nextPayPeriod.substring(3));
					nextDueDate = insure.dueDate(file.ulmast.get("mode"), masterDetail[1], nextPeriod);
					System.out.println("nextDueDate       >>" + nextDueDate);
					try {
						// Big 11/09/2561
						// Result rsnext =
						// PublicRte.getResult("blmaster","rte.bl.universal.master.RteUniversalPremium",
						// new String[] {policyNo,payPeriod,masterDetail[1]});
						Result rsnext = PublicRte.getResult("blmaster", "rte.bl.universal.master.RteUniversalPremium",
								new String[] { policyNo, nextPeriod, masterDetail[1] });
						// Result rsnext =
						// PublicRte.getResult("blmaster","rte.bl.master.RteMasterPremium",new
						// String[]{policyNo,nextPayPeriod.substring(0,2)+nextPayPeriod.substring(3)});
						if (rsnext.status() == 0) {
							Vector rv = (Vector) rsnext.value();
							vrid = (Vector) rv.elementAt(0);
							premiumAmount = (String) rv.elementAt(9);
						} else {
							System.out.println("rsnext.status = " + rsnext.status());
						}

					} catch (Exception e) {
						System.out.println("calNextPremium - " + e.getMessage());
					}

					// premiumAmount
					// screen.setEnableNextPeriod
				}
				if (ckPlan.isPAPlan(plan) || ckPlan.isDMPAPlan(plan)) {
					payPeriod = "0101";
					effDate = M.addnum(effDate.substring(0, 4),
							M.subnum(file.ulmast.get("payPeriod").substring(0, 2), "1")) + effDate.substring(4);
					if (effDate.substring(4, 8).compareTo("0229") == 0) {
						int days = DateInfo.daysInMonth(effDate);
						effDate = effDate.substring(0, 6) + M.setlen(M.itoc(days), 2);
					}
					if (!M.dateok(effDate))
						effDate = effDate.substring(0, 6) + M.subnum(effDate.substring(6), "1");
					matureDate = M.addnum(effDate.substring(0, 4), "1") + effDate.substring(4);
					System.out.println("222");
				} else {
					System.out.println("333");
					matureDate = (String) md.elementAt(12);
				}
			} catch (Exception e) {
			}
			masterDetail[2] = matureDate;
			masterDetail[6] = nextDueDate;
			masterDetail[7] = nextPayPeriod;
			masterDetail[8] = premiumAmount;
			System.out.println("nextDueDate = " + nextDueDate + " nextPayPeriod = " + nextPayPeriod);
			masterDetail[9] = file.addr[0];
			masterDetail[10] = file.addr[1];
			masterDetail[11] = file.name.get("firstName");
			masterDetail[12] = file.name.get("lastName");
			tlp = PlanSpec.getPlan(file.ulmast.get("planCode"));
			insureYear = M.itoc(M.ctoi(tlp.endowmentYear(file.ulmast.get("insuredAge"))));
			insuredAge = file.ulmast.get("insuredAge");
			payPeriod = file.ulmast.get("payPeriod");
			masterDetail[16] = file.ulmast.get("branch");
			masterDetail[15] = file.ulmast.get("planCode");
			masterDetail[17] = file.ulmast.get("payPeriod");
			masterDetail[18] = file.ulmast.get("policyStatus2");
			masterDetail[19] = file.ulmast.get("oldPolicyStatus1");
			masterDetail[22] = file.ulmast.get("dueDate");
			masterDetail[25] = policyType;

		} else if (policyType.equals("L")) {

			plan = file.ulip.get("planCode").trim();
			mstFile = file.ulip.getBytes();
			policyStatus1 = file.ulip.get("policyStatus1");
			masterDetail[0] = getPlanCode('L', file.ulip.get("planCode").trim());
			masterDetail[1] = file.ulip.get("effectiveDate");
			masterDetail[2] = ulipMatureDate(file.ulip.get("planCode").trim(), file.ulip.get("effectiveDate"));
			masterDetail[3] = file.ulip.get("policyStatus1");
			masterDetail[4] = file.subunitlink.get("sum");
			// System.out.println(" suminsure :: "+file.ulip.get("rppSum"));
			// masterDetail[4] = file.ulip.get("sum");
			masterDetail[5] = file.ulip.get("mode");

			nextPayPeriod = nextPayPeriod(file.ulip.get("payPeriod"), file.ulip.get("mode"), "L");
			// nextDueDate =
			// insure.dueDate(file.ulmast.get("mode"),effDate,nextPayPeriod.substring(0,2)+nextPayPeriod.substring(3));
			// nextDueDate= insure.dueDate(file.ulip.get("mode"),masterDetail[1]
			// ,nextPeriod);
			System.out.println("nextPayPeriod :: " + nextPayPeriod);
			System.out.println("nextDueDate :: " + nextDueDate);

			// nextPayPeriod = nextPayPeriod(file.ulip.get("payPeriod"),"0","L");
			masterDetail[6] = nextPayPeriod.substring(3) + nextPayPeriod.substring(0, 2) + "01";
			masterDetail[7] = nextPayPeriod;
			System.out.println("nextPayPeriod20171228 :: " + nextPayPeriod);
			masterDetail[8] = file.subunitlink.get("premium");
			masterDetail[9] = file.addr[0];
			masterDetail[10] = file.addr[1];
			masterDetail[11] = file.name.get("firstName");
			masterDetail[12] = file.name.get("lastName");
			masterDetail[16] = file.ulip.get("branch");
			masterDetail[15] = file.ulip.get("planCode");
			masterDetail[17] = file.ulip.get("payPeriod");
			masterDetail[18] = file.ulip.get("policyStatus2");
			masterDetail[19] = file.ulip.get("oldPolicyStatus1");
			// System.out.println("DUEDATE :: "+file.ulip.get("dueDate"));
			masterDetail[22] = file.ulip.get("dueDate");
			// masterDetail[22] =
			// nextPayPeriod.substring(3)+nextPayPeriod.substring(0,2)+"01";
			masterDetail[23] = file.ulip.get("rppSum");

			/**
			 * System.out.println(file.ulip.get("rppSum"));
			 * System.out.println(file.ulip.get("rspSum"));
			 **/
			masterDetail[24] = file.ulip.get("rppPremium");
			// System.out.println("tel >>
			// "+file.findTelephone(file.ulip.get("localAddressID")));
			masterDetail[13] = file.findTelephone(file.ulip.get("localAddressID"));
			masterDetail[25] = policyType;
			// System.out.println(Arrays.toString(masterDetail));
			String[] uli = file.ulip.fieldName();
			/*
			 * for (int i = 0; i < uli.length; i++) {
			 * System.out.println(uli[i]+" <<>> "+file.ulip.get(uli[i])); }
			 */
			System.out.println(">>>>>>>> " + Arrays.toString(file.ulip.fieldName()));
			System.out.println(">>>>>>>> " + Arrays.toString(file.ulipremark.fieldName()));
			System.out.println(">>>>>>>> " + Arrays.toString(file.subunitlink.fieldName()));

			Result rs = PublicRte.getResult("blmaster", "rte.bl.master.RteMasterPremium",
					new String[] { policyNo, payPeriod });
			if (rs.status() == 0) {
				md = (Vector) rs.value();
				vrid = (Vector) md.elementAt(0);
			}

			/**
			 * try { Result rsnext =
			 * PublicRte.getResult("blmaster","rte.bl.unitlink.master.RteUnitLinkPremium",
			 * new String[] {policyNo,payPeriod,masterDetail[1]}); //Result rsnext =
			 * PublicRte.getResult("blmaster","rte.bl.master.RteMasterPremium",new
			 * String[]{policyNo,nextPayPeriod.substring(0,2)+nextPayPeriod.substring(3)});
			 * System.out.println("***"+ rsnext.status());
			 * System.out.println((String)rsnext.value()); if (rsnext.status() == 0) {
			 * 
			 * Vector rv = (Vector)rsnext.value(); vrid = (Vector)rv.elementAt(0); } } catch
			 * (Exception e) { // TODO: handle exception }
			 **/

			/*
			 * System.out.println(file.subunitlink.get("sum"));
			 * System.out.println(file.subunitlink.get("premium"));
			 * System.out.println(file.ulip.get("rppSum"));
			 * System.out.println(file.ulip.get("rppPremium"));
			 */
			/*
			 * s[0] = planName s[1] = effectiveDate s[2] = matureDate s[3] = status s[4] =
			 * sumAssure (rspsum for unit link) s[5] = mode s[6] = nextDueDate s[7] =
			 * nextPayPeriod s[8] = premiumAmount (rspPreemium for unit link) s[9] = address
			 * line 1 s[10] = address line 2 s[11] = firstName s[12] = lastName s[13] =
			 * telephone s[14] = policyStatus1 s[15] = planCode s[16] = branch s[17] =
			 * payPeriod s[18] = policyStatus2 s[19] = oldPolicyStatus1 s[20] = status1
			 * (oldPolicyStatus1 convert) s[21] = prename s[22] = dueDate s[23] = rppSum
			 * s[24] = rppPremium s[25] = policyType
			 */
			nextpayperiod = nextPayPeriod;
			duedate = file.ulip.get("dueDate");

		} else {
			plan = file.whlmast.get("planCode").trim();
			mstFile = file.whlmast.getBytes();
			payPeriod = file.whlmast.get("payPeriod");
			nextPeriod = nextPayPeriod(payPeriod, file.whlmast.get("mode"), "W");
			policyStatus1 = file.whlmast.get("policyStatus1");
			premiumAmount = file.whlmast.get("lifePremium");
			masterDetail[0] = getPlanCode('O', file.whlmast.get("planCode"));
			masterDetail[1] = file.whlmast.get("effectiveDate");
			masterDetail[2] = whlMatureDate();
			masterDetail[3] = file.whlmast.get("policyStatus1");
			masterDetail[4] = file.whlmast.get("sum");
			masterDetail[5] = file.whlmast.get("mode");
			masterDetail[6] = file.whlmast.get("dueDate");
			masterDetail[7] = nextPeriod;
			masterDetail[8] = premiumAmount;
			masterDetail[9] = file.addr[0];
			masterDetail[10] = file.addr[1];
			masterDetail[11] = file.name.get("firstName");
			masterDetail[12] = file.name.get("lastName");
			tlp = PlanSpec.getPlan(file.whlmast.get("planCode"));
			insureYear = M.itoc(M.ctoi(tlp.endowmentYear(file.whlmast.get("insuredAge"))));
			lastLifePremium = premiumAmount;
			insuredAge = file.whlmast.get("insuredAge");
			payPeriod = file.whlmast.get("payPeriod");
		}

		System.out.println("H".equals(masterDetail[3]));
		System.out.println(("AIJLOR").indexOf(masterDetail[3]));

		if ((("AIJLOR").indexOf(masterDetail[3]) < 0) && (!"H".equals(masterDetail[3]))) {
			nextDueDate = "00000000";
			nextPayPeriod = "0000";
			premiumAmount = "000000000";
			masterDetail[6] = nextDueDate;
			masterDetail[7] = nextPayPeriod;
			masterDetail[8] = premiumAmount;
		}
		effectiveDate = masterDetail[1];
		sumAssure = masterDetail[4];
		sumRppUL = masterDetail[23];
		premRppUL = masterDetail[24];
		masterDetail[14] = masterDetail[3];
		masterDetail[3] = StatusPolicy.statusDesc(masterDetail[3]);
		sex = file.person.get("sex");
		age = Insure.getAge(file.person.get("birthDate"), effectiveDate);
		personFile = file.person.getBytes();
		firstName = masterDetail[11];
		lastName = masterDetail[12];
		masterDetail[20] = StatusPolicy.statusDesc(masterDetail[19]);
		if (masterDetail[20] == null)
			masterDetail[20] = "";
		preName = utility.prename.Prename.getAbb(file.name.get("preName"));
		addr = file.addr;
		masterDetail[21] = preName;
		masterDetail[13] = file.findTelephone("");
		mode = masterDetail[5];

//		for (int i = 0; i <= 25; i++) {
//			System.out.println(" * hagi " + i + " * " + masterDetail[i]);
//
//		}
		return masterDetail;
	}

	public String whlMatureDate() {
		TLPlan tl = PlanSpec.getPlan(file.whlmast.get("planCode"));
		String insureYear = tl.endowmentYear(file.whlmast.get("insuredAge"));
		if (insureYear.length() >= 0)
			return (Insure.matureDate(file.whlmast.get("effectiveDate"), insureYear, file.whlmast.get("planCode"),
					file.person.get("birthDate")));
		return (M.clears('0', 8));
	}

	public String associate(String idNo, String policyNo) throws Exception {
		String isHasAssociate = "N";
		Vector polVtr = getPolicy(idNo);
		String pol = "";
		for (int i = 0; i < polVtr.size(); i++) {
			pol = (String) polVtr.elementAt(i);
			if (pol.equals(policyNo)) {
				isHasAssociate = "Y";
				break;
			}
		}

		return isHasAssociate;
	}

	public boolean ageOk(String policyNo, String age) {
		boolean ageOk = false;
		try {
			findPolicy(policyNo);
			String birthDate = file.person.get("birthDate");
			// String realAge = "";
			String realAge = Insure.getAge(birthDate, DateInfo.sysDate());
			if (age.equals(realAge) || age.equals(M.subnum(realAge, "1")) || age.equals(M.addnum(realAge, "1")))
				ageOk = true;
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return ageOk;
	}

	public boolean transOk(String policyNo, String answer) {
		boolean transOk = false;
		try {
			System.out.println("chk transOk policyNo = " + policyNo + " answer = " + answer);
			findPolicy(policyNo);
			String[] param = { policyType, policyNo, answer };
			Result result = PublicRte.getClientResult("blservice", "rte.bl.iservice.VerifyTransaction", param);
			if (result.status() == 0) {
				System.out.println("result.value = " + result.value());
				if (result.value().toString().equals("Y"))
					transOk = true;
			} else {
				System.out.println("result.status = " + result.status());
				System.out.println("result.value = " + result.value());
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return transOk;
	}

	public boolean riderOk(Vector polVtr, String answer) {
		boolean riderOk = false;
		boolean hasRider = false;
		try {
			String policyNo = "";
			String riderText = "";
			for (int i = 0; i < polVtr.size(); i++) {
				policyNo = (String) polVtr.elementAt(i);
				findPolicy(policyNo);
				if (file.vRider != null) {
					String riderType = "";
					String riderStatus = "";
					for (int j = 0; j < file.vRider.size(); j++) {
						Record r = (Record) file.vRider.elementAt(j);
						riderType = r.get("riderType");
						riderStatus = r.get("riderStatus");
						if (("NSBTWX").indexOf(riderStatus) >= 0) {
							if (!riderType.substring(0, 2).equals(M.mapt("mr")))
								hasRider = true;
							if (riderType.substring(0, 2).equals(M.mapt("ir")) && answer.equals("1"))
								return true;
							if (riderType.substring(0, 1).equals(M.mapt("v")) && answer.equals("2"))
								return true;
							if (riderType.substring(0, 1).equals(M.mapt("r")) && answer.equals("3"))
								return true;
							if (riderType.substring(0, 2).equals(M.mapt("8[")) && answer.equals("4"))
								return true;
							if (riderType.substring(0, 2).equals(M.mapt("S0")) && answer.equals("5"))
								return true;
							if (riderType.substring(0, 1).equals(M.mapt("l")) && answer.equals("6"))
								return true;
							if (riderType.substring(0, 2).equals(M.mapt("Cr")) && answer.equals("7"))
								return true;
							if (riderType.substring(0, 2).equals(M.mapt("mi")) && answer.equals("8"))
								return true;
							if (riderType.substring(0, 2).equals(M.mapt(";r")) && answer.equals("9"))
								return true;
						}
					}
				}
			}
			if (answer.equals("10") && !hasRider)
				return true;
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return riderOk;
	}

	public boolean lastPolYearOk(Vector polVtr, String answer) {
		boolean lastPolYearOk = false;
		try {
			String lastPolYear = "0000";
			String policyNo = "";
			for (int i = 0; i < polVtr.size(); i++) {
				policyNo = (String) polVtr.elementAt(i);
				findPolicy(policyNo);
				if (policyType.equals("I")) {
					if (M.cmps(file.indmast.get("effectiveDate").substring(0, 4), lastPolYear) > 0)
						lastPolYear = file.indmast.get("effectiveDate").substring(0, 4);
				} else if (policyType.equals("O")) {
					if (M.cmps(file.ordmast.get("effectiveDate").substring(0, 4), lastPolYear) > 0)
						lastPolYear = file.ordmast.get("effectiveDate").substring(0, 4);
				} else if (policyType.equals("W")) {
					if (M.cmps(file.whlmast.get("effectiveDate").substring(0, 4), lastPolYear) > 0)
						lastPolYear = file.whlmast.get("effectiveDate").substring(0, 4);
				} else if (policyType.equals("L")) {
					if (M.cmps(file.ulip.get("effectiveDate").substring(0, 4), lastPolYear) > 0)
						lastPolYear = file.ulip.get("effectiveDate").substring(0, 4);
				}

			}
			if (lastPolYear.equals(answer))
				lastPolYearOk = true;
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return lastPolYearOk;
	}

	public boolean receiptOk(String policyNo, String answer) {
		boolean receiptOk = false;
		try {
			findPolicy(policyNo);
			String rpNo = "";
			if (policyType.equals("I"))
				rpNo = file.indmast.get("rpNo");
			else if (policyType.equals("O"))
				rpNo = file.ordmast.get("rpNo");
			else if (policyType.equals("W"))
				rpNo = file.whlmast.get("rpNo");
			else if (policyType.equals("L"))
				rpNo = file.ulip.get("rpNo");
			if (rpNo.equals(answer))
				receiptOk = true;

		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return receiptOk;
	}

	public boolean contactAddrOk(String policyNo, String answer) {
		boolean contactAddrOk = false;
		try {
			findPolicy(policyNo);
			String addrText = file.address.get("address");
			int idx = 0;
			int i = 0;
			String s = "";
			String number = "";
			while ((idx = addrText.indexOf('\n')) > 0) {
				System.out.println(addrText);
				s = addrText.substring(0, idx).trim();
				addrText = addrText.substring(idx + 1).trim();
				if (s.length() > 0 && s.substring(0, 1).equals("N")) {
					number = s.substring(1);
					/*
					 * if (!M.numeric(number)) { i = 0; while (i < number.length()) { if
					 * (!M.numeric(number.substring(i,i+1))) { number = number.substring(0,i);
					 * break; } i++; } }
					 */
					if (number.equals(answer)) {
						return true;
					} else {
						return false;
					}
				}
			}
			System.out.println(addrText);
			if (addrText.length() > 0 && addrText.substring(0, 1).equals("N")) {
				number = addrText.substring(1);
				/*
				 * if (!M.numeric(number)) { i = 0; while (i < number.length()) { if
				 * (!M.numeric(number.substring(i,i+1))) { number = number.substring(0,i);
				 * break; } i++; } }
				 */
				if (number.equals(answer))
					return true;
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return contactAddrOk;
	}

	public boolean lastPremiumOk(String policyNo, String answer) {
		boolean lastPremiumOk = false;
		try {
			findPolicy(policyNo);
			String payDate = "";
			if (policyType.equals("I")) {
				payDate = file.indmast.get("payDate");
			} else if (policyType.equals("O")) {
				payDate = file.ordmast.get("payDate");
			} else if (policyType.equals("W")) {
				payDate = file.whlmast.get("payDate");
			} else if (policyType.equals("L")) {
				payDate = file.ulip.get("payDate");
			}
			String[] param = { "A", policyType, policyNo, payDate.substring(0, 4) };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			String premium = "0";
			if (file.rcpTmpFile.last()) {
				premium = file.rcpTmpFile.get("premium");
				if (Integer.parseInt(premium) == Integer.parseInt(answer))
					lastPremiumOk = true;
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return lastPremiumOk;
	}

	public boolean planNameOk(String policyNo, String answer) {
		boolean planNameOk = false;
		try {
			answer = answer.trim();
			// answer = M.stou(Translator.utos(answer.trim()));
			System.out.println("policyNo = " + policyNo + " answer = '" + answer + "'");
			findPolicy(policyNo);
			String planName = "";
			if (policyType.equals("I"))
				planName = getPlanCode('I', file.indmast.get("planCode"));
			else if (policyType.equals("O"))
				planName = getPlanCode('O', file.ordmast.get("planCode"));
			else if (policyType.equals("W"))
				planName = getPlanCode('W', file.whlmast.get("planCode"));
			else if (policyType.equals("L"))
				planName = getPlanCode('L', file.ulip.get("planCode"));
			int idx = planName.indexOf('[');
			if (idx >= 0)
				planName = planName.substring(0, idx).trim();
			idx = planName.indexOf('(');
			if (idx >= 0)
				planName = planName.substring(0, idx).trim();
			String atleast = planName;
			if ((idx = planName.indexOf(' ')) >= 0) {
				atleast = planName.substring(0, idx);
				while ((idx = planName.indexOf(' ')) >= 0)
					planName = planName.substring(0, idx) + planName.substring(idx + 1);
			}

			idx = answer.indexOf('[');
			if (idx >= 0)
				answer = answer.substring(0, idx).trim();
			idx = answer.indexOf('(');
			if (idx >= 0)
				answer = answer.substring(0, idx).trim();
			while ((idx = answer.indexOf(' ')) >= 0)
				answer = answer.substring(0, idx) + answer.substring(idx + 1);
			System.out.println("answer = '" + answer + "' planName = '" + planName + "'");
			System.out.println("length = " + atleast.length() + " answer length = " + answer.length() + " "
					+ planName.startsWith(answer));
			if (planName.startsWith(answer)) {
				if (answer.length() >= atleast.length())
					planNameOk = true;
			} else if (planName.startsWith(M.stou(answer))) {
				if (answer.length() >= atleast.length())
					planNameOk = true;
			}

		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return planNameOk;
	}

	public String verify(int question_id, String answer, String idNo) throws Exception {
		String isCorrect = "N";
		Vector polVtr = getPolicy(idNo);
		if (polVtr.size() == 0)
			return isCorrect;
		try {
			switch (question_id) {
			case 1:
				for (int i = 0; i < polVtr.size(); i++) {
					if (ageOk((String) polVtr.elementAt(i), answer)) {
						isCorrect = "Y";
						break;
					}
				}
				break;
			case 2:
				if (answer.equals("10"))
					isCorrect = "Y";
				for (int i = 0; i < polVtr.size(); i++) {
					if (transOk((String) polVtr.elementAt(i), answer)) {
						if (!answer.equals("10")) {
							isCorrect = "Y";
							break;
						}
					} else {
						if (answer.equals("10")) {
							isCorrect = "N";
							break;
						}
					}
				}
				// if (answer.equals("10") && !isCorrect.equals("N")) {
				// isCorrect = "Y";
				// }
				break;
			case 3:
				if (riderOk(polVtr, answer))
					isCorrect = "Y";
				break;
			case 4:
				if (lastPolYearOk(polVtr, answer))
					isCorrect = "Y";
				break;
			case 5:
				for (int i = 0; i < polVtr.size(); i++) {
					if (receiptOk((String) polVtr.elementAt(i), answer)) {
						isCorrect = "Y";
						break;
					}
				}
				break;
			case 6:
				for (int i = 0; i < polVtr.size(); i++) {
					if (contactAddrOk((String) polVtr.elementAt(i), answer)) {
						isCorrect = "Y";
						break;
					}
				}
				break;
			case 7:
				for (int i = 0; i < polVtr.size(); i++) {
					if (lastPremiumOk((String) polVtr.elementAt(i), answer)) {
						isCorrect = "Y";
						break;
					}
				}
				break;
			case 8:
				for (int i = 0; i < polVtr.size(); i++) {
					if (planNameOk((String) polVtr.elementAt(i), answer)) {
						isCorrect = "Y";
						break;
					}
				}
				break;
			default:
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return isCorrect;
	}

	public Vector getRider() {

		// System.out.println("Rider ******");
		Vector v = new Vector();
		if (file.vRider == null)
			return v;
		String riderType = "";
		String riderStatus = "";
		String[] riderData = { "", "", "", "" };
		/**
		 * riderData[0] = M.mapt("=;"); riderData[1] = sumAssure; riderData[2] =
		 * lastLifePremium; riderData[3] = lastExtraLifePremium;
		 * v.addElement(riderData);
		 **/

		if (policyType.equals("L")) {
			// UnitlinkPremium ul = new UnitlinkPremium();
			try {
				/**
				 * String[] a = ul.getUnitlinkPremium(policyNo); System.out.println("GetRider
				 * L");
				 **/
				riderData[0] = "RPP";
				riderData[1] = sumRppUL;
				riderData[2] = premRppUL;
				riderData[3] = "0";
				v.addElement(riderData);

				riderData = new String[] { "", "", "", "" };
				riderData[0] = "RSP";
				riderData[1] = sumAssure;
				riderData[2] = lastLifePremium;
				riderData[3] = lastExtraLifePremium;
				v.addElement(riderData);

			} catch (Exception e) {
				// TODO: handle exception
			}
		} else {
			riderData[0] = M.mapt("=;");
			riderData[1] = sumAssure;
			riderData[2] = lastLifePremium;
			riderData[3] = lastExtraLifePremium;

			System.out.println("riderData :::::::::::::::::::::::::::::::::::::");
			ZArray.show(riderData);
			v.addElement(riderData);
		}

		// if(!policyType.equals("L"))
		// {

		for (int i = 0; i < file.vRider.size(); i++) {
			Record r = (Record) file.vRider.elementAt(i);
			riderType = r.get("riderType");
			riderStatus = r.get("riderStatus");
			System.out.println("riderStatus ::::::::::::::: " + riderStatus + "|" + riderType + "|"
					+ (("NSB").indexOf(r.get("riderStatus"))) + "|");
			if (("NSB").indexOf(r.get("riderStatus")) >= 0) {
				String[] prem = null;
				System.out.println("--------**-*----" + (ckPlan.isSinglePremiumPlan(plan)) + "|");
				if (ckPlan.isSinglePremiumPlan(plan))
					prem = new String[] { r.get("riderType"), r.get("riderPremium"), "0" };

				else {
					// System.out.println("**--**--");
					if (!policyType.equals("L")) {
						prem = getRiderPrem(riderType.trim());
						System.out.println("getRiderPrem ::::::::::::: ");
						ZArray.show(prem);
					} else {
						// prem =new String[] {"0","0","0"};
						prem = getRiderPrem(riderType.trim());
						System.out.println("HaGi Nutt");
						ZArray.show(prem);

						// UnitLinkPremium mp = null;
						// try {
						// mp = new UnitLinkPremium(policyNo, nextpayperiod, duedate);
						// mp.premiumMaster(policyNo, nextpayperiod, duedate);
						// mp.calPremium();
						// } catch (Exception e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						//
						// Vector vRider = mp.rider();

					}

					/// prem =new String[] {"0","0","0"};

				}

				/*
				 * 25590912 fix tlcustomer if (RiderType.isAnySP(riderType)) if
				 * (riderType.substring(0,1).equals(M.stou("à¸ ")) ||
				 * riderType.substring(0,1).equals(M.stou("à¸ª"))) riderType =
				 * RiderType.getShortName(riderType); else riderType =
				 * RiderType.getShortName(riderType)+M.multiply(riderType.substring(1),"100",0);
				 * String riders = "0"; if (riderType.indexOf(M.stou("à¸¢à¸„à¸¢à¸š")) < 0)
				 * riders = r.get("riderSum");
				 */

				System.out.println("riderType1  : " + riderType + "|" + (RiderType.isAnySP(riderType)) + "|");
				String riders = "0";
				if (RiderType.isAnySP(riderType)) // want sp plus show riderType from rider file
				{
					if (riderType.compareTo("S1K") == 0 || riderType.compareTo("H1K") == 0)
						riders = "10000";
					else
						riders = M.multiply(riderType.substring(1), "100", 0);
					if (riderType.substring(0, 1).equals(M.stou("à¸ "))
							|| riderType.substring(0, 1).equals(M.stou("à¸ª"))
							|| riderType.substring(0, 1).equals(M.stou("à¸ž")))
						if (!RiderType.isSPPlus(riderType))
							riderType = RiderType.getShortName(riderType);
						else
							;
					else
						riderType = RiderType.getShortName(riderType);
				} else {
					// System.out.println("ZZ = "+riderType+ " "+riders + " "+
					// RiderType.isSPGold(riderType));
					if (riderType.charAt(0) == 'D' || riderType.charAt(0) == 'C' || riderType.compareTo("HA") == 0
							|| riderType.charAt(0) == 'T' || riderType.compareTo("A03") == 0)
						riderType = Insure.riderMapping(riderType);
					System.out.println("1RiderType.isSPGold(" + riderType + ")");
					/*
					 * if (RiderType.isSPGold(riderType) ) { riders =
					 * M.multiply(riderType.substring(1),"1000",0); riderType =
					 * Insure.riderMapping(riderType); }
					 */
					if (RiderType.isSPGold(riderType)) {
						riders = getRidersSPGold(riderType);
						riderType = Insure.riderMapping(riderType);
					} else if (riderType.indexOf(M.stou("à¸„à¸š")) < 0)
						riders = r.get("riderSum");
					System.out.println("2RiderType.isSPGold(" + riderType + ")");
				}

				System.out.println("**<>******************" + prem[1] + " //**// " + prem[2]);
				riderData = new String[] { riderType, riders, prem[1], prem[2] };
				System.out.println("add rider [] " + riderType + "|" + riders + "|" + prem[1] + "|" + prem[2] + "|");
				v.addElement(riderData);
			}
		}
		/*
		 * }else{ String riders = "0";
		 * 
		 * UnitLinkPremium mp = new UnitLinkPremium(policyNo, nextpayperiod, duedate);
		 * mp.premiumMaster(policyNo, nextpayperiod, duedate); //mp.getValue();
		 * mp.calPremium();
		 * 
		 * // System.out.println(" <> "+mp.getValue().getClass()); Vector vRider =
		 * mp.rider(); //System.out.println("a "+mp.extraLifePremium());
		 * 
		 * for(int i=0;i< vRider.size();i++) {
		 * System.out.println(":"+i+":"+vRider.elementAt(i)); String [] rd =
		 * (String[])vRider.elementAt(i); for (int j = 0; j < rd.length; j++) {
		 * 
		 * System.out.println("-*-*-*-"+rd[j]); riderData = new String[] {riderType,
		 * riders, prem[1], prem[2]}; v.addElement(riderData);
		 * 
		 * } }
		 * 
		 * 
		 * 
		 * 
		 * //System.out.println("*** eiei "+policyNo+" ** "+nextpayperiod+" ** "+duedate
		 * );
		 */

		return v;
	}

	private String getRidersSPGold(String riderType) {

		if (riderType.equals("G11") || riderType.equals("F01"))
			return "2000";
		else if (riderType.equals("G12") || riderType.equals("F02"))
			return "4000";
		else if (riderType.equals("G13") || riderType.equals("F03"))
			return "6000";
		else if (riderType.equals("G14") || riderType.equals("F04"))
			return "8000";
		else if (riderType.equals("G15") || riderType.equals("F05"))
			return "10000";
		return M.multiply(riderType.substring(1), "1000", 0);
	}

	String rptYearStart = "";

	public Vector getReceipt(String rptYearF, String rptYearL) {
		PublicRte.setRemote(true);
		Vector v = new Vector();
		String[] rcptData = { "", "", "" };
		String rptPeriod = "";
		if (policyType.equals("I")) {
			String payD = file.indmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);

			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "36");
			String[] param = { "A", "I", policyNo, rptYearStart };
			System.out.println("Status Receipt =======> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(4) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(0, 4),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}
		} else if (policyType.equals("O")) {
			String payD = file.ordmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "40");
			System.out
					.println("rptYearStart = " + rptYearStart + " rptYearF = " + rptYearF + " rptYearL = " + rptYearL);
			rptPeriod = M.addnum(rptYearStart, M.multiply(M.addnum(M.subnum(rptYearF, rptYearL), "1"), "12", 0));
			System.out.println("rptPeriod = " + rptPeriod);
			String[] param = { "A", "O", policyNo, rptPeriod };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}
		} else if (policyType.equals("U")) {
			String payD = file.ulmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "40");
			System.out
					.println("rptYearStart = " + rptYearStart + " rptYearF = " + rptYearF + " rptYearL = " + rptYearL);
			rptPeriod = M.addnum(rptYearStart, M.multiply(M.addnum(M.subnum(rptYearF, rptYearL), "1"), "12", 0));
			System.out.println("rptPeriod = " + rptPeriod);
			String[] param = { "A", "U", policyNo, rptPeriod };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}

		} else if (policyType.equals("L")) {
			String payD = file.ulip.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "40");
			System.out
					.println("rptYearStart = " + rptYearStart + " rptYearF = " + rptYearF + " rptYearL = " + rptYearL);
			rptPeriod = M.addnum(rptYearStart, M.multiply(M.addnum(M.subnum(rptYearF, rptYearL), "1"), "12", 0));
			System.out.println("rptPeriod = " + rptPeriod);
			String[] param = { "A", "L", policyNo, rptPeriod };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}

		} else {
			String payD = file.whlmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "36");
			String[] param = { "A", "W", policyNo, rptYearStart };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}
		}
		return v;

	}

	public Vector getReceipt() {
		System.out.println("getReceipt  policyType : " + policyType);
		PublicRte.setRemote(true);
		Vector v = new Vector();
		String[] rcptData = { "", "", "" };
		if (policyType.equals("I")) {
			String payD = file.indmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "36");
			String[] param = { "A", "I", policyNo, rptYearStart };
			System.out.println("Status Receipt =======> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(4) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(0, 4),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}
		} else if (policyType.equals("O")) {
			String payD = file.ordmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "40");
			String[] param = { "A", "O", policyNo, rptYearStart };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}
		} else if (policyType.equals("U")) {
			String payD = file.ulmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "40");
			String[] param = { "A", "U", policyNo, rptYearStart };

			System.out.println("UUUUUUUUU payD : " + payD + "|" + sysD + "|" + period + "|" + yearStart + "|"
					+ rptYearStart + "|");

			System.out.println("file : " + file + "|" + file.rcpTmpFile + "|");
			if (file == null)
				return v;
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}

		} else if (policyType.equals("L")) {
			String payD = file.ulip.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "40");
			String[] param = { "A", "L", policyNo, rptYearStart };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}

		} else {
			String payD = file.whlmast.get("payDate");
			String sysD = DateInfo.sysDate().substring(0, 8);
			String period = M.multiply(M.subnum(sysD.substring(0, 4), payD.substring(0, 4)), "12", 0);
			period = M.addnum(period, M.subnum(sysD.substring(4, 6), payD.substring(4, 6)));
			String yearStart = M.subnum(payD.substring(0, 4), "3");
			rptYearStart = M.addnum(period, "36");
			String[] param = { "A", "W", policyNo, rptYearStart };
			System.out.println("Status Receipt ===========> " + file.checkDataReceipt(param));
			for (boolean st = file.rcpTmpFile.last(); st; st = file.rcpTmpFile.previous()) {
				rcptData = new String[] {
						file.rcpTmpFile.get("payPeriod").substring(0, 2) + "/"
								+ file.rcpTmpFile.get("payPeriod").substring(2),
						file.rcpTmpFile.get("payDate"), file.rcpTmpFile.get("premium"), file.rcpTmpFile.get("rpNo") };
				v.addElement(rcptData);
			}
		}
		return v;
	}
	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public Vector getNotReceivedDividend() throws Exception
	{
		Vector v = new Vector<>();
		String receiveDate = DateInfo.sysDate().substring(0,8);
		String _blClassName = "rte.bl.service.benefit.RteBenefitITran";
		System.out.println( "EVE policyStatus1 = " + policyStatus1 );
		String[] _blParam = new String[]{"6", policyNo, receiveDate, payPeriod, policyStatus1, "Y", "Y", "Y"};
        Result rs = PublicRte.getClientResult("blservice",_blClassName,_blParam); 
		Vector vInt = new Vector();
		if (rs.status() == 0 && rs.value() instanceof Vector)
        {  
			vInt = (Vector) rs.value();
		} else if(rs.status() == 0 && rs.value() instanceof Object[])
        {
          	Object[] obj = (Object[])rs.value();
             v = (Vector)obj[0];            
        }else {
			System.out.println("status = " + rs.status());
			System.out.println("result = " +rs.value());
		}
        

		return v;
	}
	
	

    private String getPayTypeName(String payType)
    {
            return payType.equals("P") ? M.stou("เงินคืนฯ") : M.stou("ปันผล");
    }
	public Vector<String[]> getReceivedDividend() throws Exception
	{
		Vector<String[]> v = new Vector<>();
        String[] _blParam = new String[]{"3", policyNo};
        System.out.println("_blParam : ");
        ZArray.show(_blParam);
        Result result = service.service.ServiceCentralize.callBlRte("blservice","rte.bl.service.benefit.RteBenefitITran" , _blParam, true);
        System.out.println("searchPaidAlready result : "+result.status());
        Object[] obj = (Object[])result.value();
        
        Vector vDvmst = (Vector)obj[0];
        Vector vPayc = (Vector)obj[1];
        String sum = "0";
        String sum_amount = "0";
        String sum_interest =  "0";
        String sum_total = "0";
        if (!vPayc.isEmpty()) {
        	System.out.println("vPayc :::::::::: ");
                layout.master.Rpaycmast lpay = new layout.master.Rpaycmast();
                vPayc = lpay.setRecord(vPayc);
                for (Iterator<?> iter = vPayc.iterator(); iter.hasNext();) {
                        Record r = (Record)iter.next();
                        if (r.get("payFlag").charAt(0) == '2' || r.get("payFlag").charAt(0) == 'N')
                                continue;
                        String[] sr = new String[]{"", "0", M.clears('0', 8), "0", "0", "0", "0", "0", "0"};
                        sr[0] = getPayTypeName("P");
                        sr[1] = r.get("payYear");
                        sr[2] = r.get("payDate");
                        sr[3] = r.get("amount");
                        sr[4] = r.get("interest");
                        sum = M.addnum(sr[3], sr[4], 2);
                        sr[5] = sum;
                        sum_amount = M.addnum(sum_amount, r.get("amount"), 2);
                        sum_interest = M.addnum(sum_interest, r.get("interest"), 2);
                        
                        ZArray.show(sr);
                        v.add(sr);
                }
        }
        if (!vDvmst.isEmpty()) {
        	System.out.println("vDvmst :::::::::: ");
                layout.master.Rdvmast lpay = new layout.master.Rdvmast();
                vDvmst = lpay.setRecord(vDvmst);
                for (Iterator iter = vDvmst.iterator(); iter.hasNext();) {
                        Record r = (Record)iter.next();
                        if (r.get("payFlag").charAt(0) == '2' || r.get("payFlag").charAt(0) == 'N')
                                continue;
                        String[] sr = new String[]{"", "0", M.clears('0', 8), "0", "0", "0", "0", "0", "0"};
                        sr[0] = getPayTypeName("D");
                        sr[1] = r.get("payYear");
                        sr[2] = r.get("payDate");
                        sr[3] = r.get("amount");
                        sr[4] = r.get("interest");
                        sum = M.addnum(sr[3], sr[4], 2);
                        sr[5] = sum;
                        sum_amount = M.addnum(sum_amount, r.get("amount"), 2);
                        sum_interest = M.addnum(sum_interest, r.get("interest"), 2);

                        ZArray.show(sr);
                        v.add(sr);
                }
        }
        
        return v;
	}
	
	
	
	public Vector getDividendNew() throws Exception 
	{
		Vector v = new Vector();
		Vector<String[]> vReceive = getReceivedDividend();
		Vector<String[]> nReceived = getNotReceivedDividend();
		if(vReceive != null )
		{
			System.out.println("vReceive : " + vReceive.size());
			for (String[] s : vReceive) {
				ZArray.show(s);
				v.add(new String[] {"0" , s[1],s[2],s[3],s[4] , "Y","รับเงินแล้ว",s.length > 7? s[7]:"",s.length > 8?s[8]:""});
			}
		}
		if(nReceived != null )
		{
			System.out.println("nReceived : " + nReceived.size());
			for (String[] s : nReceived) {
				ZArray.show(s);
				v.add(new String[] {s[0] , s[1],s[2],s[3],s[4].equals(".00") ? "0.00":s[4] , "N","ยังไม่รับ",s.length > 7? s[7]:"",s.length > 8?s[8]:""});
			}
		}
		
		return v;
	}
	
	public Vector getDividend() throws Exception{
		PublicRte.setRemote(true);
		Vector v = new Vector();
		String[] divData = { "", "", "", "", "", "" };

		// 2561/11/02 
		Vector vInt = getDividendNew() ;
		if(vInt != null && !vInt.isEmpty())
			return vInt;
		
		Record condPay, dividen, annuity;
		String colmn[];
		Vector param = new Vector();
		param.add(mstFile);
		param.add(personFile);
		param.add(new String[] { policyType, DateInfo.sysDate().substring(0, 8), "Y", "N", "N", "T" });
		System.out.println("Before Call RTE");
		Result rs = PublicRte.getClientResult("blservice", "rte.bl.service.benefit.RteCalculateBenefit", param);
		System.out.println("After Call RTE");
		
//		Vector vInt = new Vector();
		if (rs.status() == 0) {
			vInt = (Vector) rs.value();
			System.out.println("result = " + rs.value());
			ZVector.show((Vector<String[]>)rs.value());
		} else {
			System.out.println("status = " + rs.status());
			System.out.println("result = " + rs.value());
		}
		System.out.println("SIZE >>>>> " + vInt.size() + "   " + rs.status());

		Vector vDividen = new Vector<>();
		Vector vCondpay = new Vector<>();
		Vector vAnnuity = new Vector<>();

		try {

			vDividen = SearchMaster.searchDividen(); 
		} catch (Exception e) {
			System.out.println("Error vDividen " + e.getMessage());
			e.printStackTrace();
		}
		try {

			vCondpay = SearchMaster.searchPaycond();
		} catch (Exception e) {
			System.out.println("Error vCondpay " + e.getMessage());
			e.printStackTrace();
		}
		try {

			vAnnuity = SearchMaster.searchAnnuityMast();
		} catch (Exception e) {
			System.out.println("Error vAnnuity " + e.getMessage());
			e.printStackTrace();
		}
//		 Vector vDividen = SearchMaster.searchDividen();
//		 Vector vCondpay = SearchMaster.searchPaycond();
//		 Vector vAnnuity = SearchMaster.searchAnnuityMast();
		Vector vSort;

		// vDividen.clone();
		// vCondpay.clone();
		// vAnnuity.clone();

		System.out.println("SIZE Dividen >>>>> " + vDividen.size());
		System.out.println("SIZE Condpay >>>>> " + vCondpay.size());
		System.out.println("SIZE Annuity >>>>> " + vAnnuity.size());
		String[] temp = { "type", "policyNo", "payYear", "payDate", "payFlag", "amount", "interest", "branch",
				"fromApl" };
		int[] tempLen = { 1, 8, 2, 8, 1, 15, 10, 3, 1 };
		char[] fieldType = { 'T', 'T', 'T', 'T', 'T', 'N', 'N', 'T', 'T', 'T' };
		int[] fieldScale = { 0, 0, 0, 0, 0, 2, 2, 0, 0 };
		String[] tempkey0 = { "payYear" };
		// TempFile tempFile = new TempFile(temp, fieldType , tempLen , fieldScale);
		TempFile tempFile = new TempFile(temp, tempLen);
		tempFile.setNoOfKey(1);
		tempFile.setKey(0, tempkey0);
		// ============================
		// Add Condpay
		// ============================
		if (vCondpay != null) {
			for (int i = 0; i < vCondpay.size(); i++) {
				condPay = (Record) vCondpay.elementAt(i);
				if (condPay == null)
					break;
				tempFile.newRecord();
				tempFile.set("type", "0");
				tempFile.set("policyNo", policyNo);
				tempFile.set("payYear", condPay.get("payYear"));
				tempFile.set("payDate", condPay.get("payDate"));
				tempFile.set("payFlag", condPay.get("payFlag"));
				tempFile.set("amount", condPay.get("amount"));
				tempFile.set("interest", condPay.get("interest"));
				tempFile.set("branch", condPay.get("branch"));
				tempFile.set("fromApl", condPay.get("fromApl"));
				tempFile.add();
			}
		}
		// ============================
		// Add Dividen
		// ============================
		if (vDividen != null) {
			for (int i = 0; i < vDividen.size(); i++) {
				dividen = (Record) vDividen.elementAt(i);
				if (dividen == null)
					break;
				tempFile.newRecord();
				tempFile.set("type", "1");
				tempFile.set("policyNo", dividen.get("policyNo"));
				tempFile.set("payYear", dividen.get("payYear"));
				tempFile.set("payDate", dividen.get("payDate"));
				tempFile.set("payFlag", dividen.get("payFlag"));
				tempFile.set("amount", dividen.get("amount"));
				tempFile.set("interest", dividen.get("interest"));
				tempFile.set("branch", dividen.get("branch"));
				tempFile.set("fromApl", dividen.get("reserve"));
				tempFile.add();
			}
		}
		// ============================
		// Add Annuity
		// ============================
		if (vAnnuity != null) {
			for (int i = 0; i < vAnnuity.size(); i++) {
				annuity = (Record) vAnnuity.elementAt(i);
				if (annuity == null)
					break;
				tempFile.newRecord();
				tempFile.set("type", "2");
				tempFile.set("policyNo", annuity.get("policyNo"));
				tempFile.set("payYear", annuity.get("payYear"));
				tempFile.set("payDate", annuity.get("payDate"));
				tempFile.set("payFlag", annuity.get("payFlag"));
				tempFile.set("amount", annuity.get("amount"));
				tempFile.set("interest", annuity.get("interest"));
				tempFile.set("branch", annuity.get("branch"));
				tempFile.set("fromApl", annuity.get("annualType"));
				tempFile.add();
			}
		}
		String payFlag = "N";
		String payDesc = "";
		System.out.println("v.size()=" + v.size());
		int vIntIdx = 0;
		String interest = "";
		System.out.println("vInt = " + vInt.size());
		for (boolean st = tempFile.first(); st; st = tempFile.next()) {
			System.out.println("Wan ==> policyNo = " + tempFile.get("policyNo"));
			System.out.println("Wan ==> Year = " + tempFile.get("payYear"));
			System.out.println("Wan ==> payFlag = " + tempFile.get("payFlag"));
			System.out.println("Wan ==> payDesc = " + StatusInfo.receiveDvPayStatus(tempFile.get("payFlag")));

			interest = tempFile.get("interest");
			if (tempFile.get("payFlag").equals("2")) {
				payFlag = "N";
				// Translate PlayFlag -> PayDesc
				payDesc = StatusInfo.receiveDvPayStatus(tempFile.get("payFlag"));
				String[] data = null;
				if (vInt != null && vInt.size() > 0) {
					try {
						data = (String[]) vInt.elementAt(vIntIdx);
						ZArray.show(data);
						interest = data[4];
						if (data[7].trim().equals("")) {
							divData = new String[] { data[0], data[1], data[2], data[3], data[4], payFlag, payDesc,
									tempFile.get("payFlag"), tempFile.get("branch") };
							v.addElement(divData);
						}
						vIntIdx++;
					} catch (Exception e) {
						System.out.println("Error : " + e.toString());
					}
				} else {
					divData = new String[] { tempFile.get("type"), tempFile.get("payYear"), tempFile.get("payDate"),
							tempFile.get("amount"), interest, payFlag, payDesc, tempFile.get("payFlag"),
							tempFile.get("branch") };
					v.addElement(divData);
				}
			} else {
				payFlag = "Y";
				// Translate PlayFlag -> PayDesc
				payDesc = StatusInfo.receiveDvPayStatus(tempFile.get("payFlag"));
				divData = new String[] { tempFile.get("type"), tempFile.get("payYear"), tempFile.get("payDate"),
						tempFile.get("amount"), interest, payFlag, payDesc, tempFile.get("payFlag"),
						tempFile.get("branch") };
				v.addElement(divData);
			}

		}
		System.out.println("Return get Dividend =>>>>> " + v.size());
		System.out.println("IserviceRtyWan = " + v.size());
		return v;
	}

	public Vector getLoan() {
		Vector v = new Vector();
		try {
			System.out.println("getLoan :: " + policyType + "|" + policyNo + "|" + DateInfo.sysDate().substring(0, 8)
					+ "|" + "B" + "|");
			Result rs = null;
			// if(policyType.equals("U"))
			rs = PublicRte.getResult("blservice", "rte.bl.dataservice.pos.RteCalculateLoan",
					new String[] { policyNo, DateInfo.sysDate().substring(0, 8) });
			// else
			// rs = PublicRte.getResult("blservice","rte.bl.service.loan.CalRequestLoan",new
			// String[]{policyType,policyNo,DateInfo.sysDate().substring(0,8),"B" });
			// Result rs =
			// PublicRte.getResult("blservice","rte.bl.dataservice.pos.RtePolicyLoan",policyNo
			// );
			// Result rs =
			// PublicRte.getResult("blservice","rte.bl.service.loan.CalRequestLoan",new
			// String[]{policyType,policyNo,DateInfo.sysDate().substring(0,8),"B" });
			System.out.println("rs :: " + rs.status() + "|" + rs.value() + "|");

			if (rs.status() == 0 /* && policyType.equals("U") */) {
				System.out.println("rs.value() UU : +" + rs.value());
				if (rs.value() instanceof CalLoanBeanSet) {
					CalLoanBeanSet bean = (CalLoanBeanSet) rs.value();
					CalExtendLoanBean ce = bean.getCalExtendLoanBean(); // à¸•à¹ˆà¸­à¸ªà¸±à¸à¸à¸²à¹€à¸‡à¸´à¸™à¸à¸¹à¹‰
																		// /à¸•à¹ˆà¸­à¸”à¸­à¸
					CalReturnLoanBean cb = bean.getCalReturnLoanBean(); // à¸„à¸·à¸™à¸ªà¸±à¸à¸à¸²à¹€à¸‡à¸´à¸™à¸à¸¹à¹‰
					CalRequestLoanBean cr = bean.getCalRequestLoanBean(); // à¹€à¹‰à¸‡à¸´à¸™à¸à¸¹à¹‰
					System.out.println("Remark : " + cr.getRemark());
					/*
					 * System.out.println("test ce  : getExtendAmount : " + ce.getExtendAmount() );
					 * System.out.println("test ce  : getExtendYear : " + ce.getExtendYear() );
					 * System.out.println("test ce  : getRemark : " + ce.getRemark() ); System.out.
					 * println("test ce  : getExtendLoanDetailTypeBean.getExtendAmount : " +
					 * ce.getExtendLoanDetailTypeBean().getExtendAmount() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getExtendDate : "
					 * + ce.getExtendLoanDetailTypeBean().getExtendDate() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getExtendYear : "
					 * + ce.getExtendLoanDetailTypeBean().getExtendYear() ); System.out.
					 * println("test ce  : getExtendLoanDetailTypeBean.getInterestRate : " +
					 * ce.getExtendLoanDetailTypeBean().getInterestRate() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getIntFromDate : "
					 * + ce.getExtendLoanDetailTypeBean().getIntFromDate() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getIntToDate : " +
					 * ce.getExtendLoanDetailTypeBean().getIntToDate() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getLoanAmount : "
					 * + ce.getExtendLoanDetailTypeBean().getLoanAmount() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getLoanDate : " +
					 * ce.getExtendLoanDetailTypeBean().getLoanDate() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getLoanDueDate : "
					 * + ce.getExtendLoanDetailTypeBean().getLoanDueDate() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getReceiptNo : " +
					 * ce.getExtendLoanDetailTypeBean().getReceiptNo() );
					 * System.out.println("test ce  : getExtendLoanDetailTypeBean.getRegisterNo : "
					 * + ce.getExtendLoanDetailTypeBean().getRegisterNo() );
					 * 
					 * System.out.println("test cb  : getInterest : " + cb.getInterest() );
					 * System.out.println("test cb  : getLoanAmount : " + cb.getLoanAmount() );
					 * System.out.println("test cb  : getRemark : " + cb.getRemark() );
					 * System.out.println("test cb  : getReturnLoan : " + cb.getReturnLoan() );
					 * System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getInterest() : " +
					 * cb.getReturnLoanDetailTypeBean().getInterest()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getInterestRate() : " +
					 * cb.getReturnLoanDetailTypeBean().getInterestRate()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getIntFromDate() : " +
					 * cb.getReturnLoanDetailTypeBean().getIntFromDate()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getLoanAmount() : " +
					 * cb.getReturnLoanDetailTypeBean().getLoanAmount()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getLoanDate() : " +
					 * cb.getReturnLoanDetailTypeBean().getLoanDate()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getLoanDueDate() : " +
					 * cb.getReturnLoanDetailTypeBean().getLoanDueDate()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getNetLoanAmount() : " +
					 * cb.getReturnLoanDetailTypeBean().getNetLoanAmount()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getReceiptNo() : " +
					 * cb.getReturnLoanDetailTypeBean().getReceiptNo()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getRegisterNo() : " +
					 * cb.getReturnLoanDetailTypeBean().getRegisterNo()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getReturnAmount() : " +
					 * cb.getReturnLoanDetailTypeBean().getReturnAmount()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getReturnDate() : " +
					 * cb.getReturnLoanDetailTypeBean().getReturnDate()); System.out.
					 * println("test cb  : getReturnLoanDetailTypeBean().getReturnType() : " +
					 * cb.getReturnLoanDetailTypeBean().getReturnType());
					 * 
					 * System.out.println("test cr  : getComLoanAmount : " + cr.getComLoanAmount()
					 * ); System.out.println("test cr  : getInterestRate : " + cr.getInterestRate()
					 * ); System.out.println("test cr  : getLoanAmount : " + cr.getLoanAmount() );
					 * System.out.println("test cr  : getPremAmount : " + cr.getPremAmount() );
					 * System.out.println("test cr  : getPremInterest : " + cr.getPremInterest() );
					 * System.out.println("test cr  : getRemark : " + cr.getRemark() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getCalculateDate() : " +
					 * cr.getLoanCalcDetailTypeBean().getCalculateDate() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getComLoanAmount() : " +
					 * cr.getLoanCalcDetailTypeBean().getComLoanAmount() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getDuty() : " +
					 * cr.getLoanCalcDetailTypeBean().getDuty() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getInterest() : "
					 * + cr.getLoanCalcDetailTypeBean().getInterest() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getInterestRate() : " +
					 * cr.getLoanCalcDetailTypeBean().getInterestRate() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getIntFromDate() : " +
					 * cr.getLoanCalcDetailTypeBean().getIntFromDate() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getIntStatus() : "
					 * + cr.getLoanCalcDetailTypeBean().getIntStatus() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getIntToDate() : "
					 * + cr.getLoanCalcDetailTypeBean().getIntToDate() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getLoanAmount() : " +
					 * cr.getLoanCalcDetailTypeBean().getLoanAmount() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getLoanDate() : "
					 * + cr.getLoanCalcDetailTypeBean().getLoanDate() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getLoanYear() : "
					 * + cr.getLoanCalcDetailTypeBean().getLoanYear() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getPayLoanAmount() : " +
					 * cr.getLoanCalcDetailTypeBean().getPayLoanAmount() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getReceiptNo() : "
					 * + cr.getLoanCalcDetailTypeBean().getReceiptNo() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getReceiver() : "
					 * + cr.getLoanCalcDetailTypeBean().getReceiver() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getReceiverType() : " +
					 * cr.getLoanCalcDetailTypeBean().getReceiverType() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getReferenceNo() : " +
					 * cr.getLoanCalcDetailTypeBean().getReferenceNo() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getReferenceType() : " +
					 * cr.getLoanCalcDetailTypeBean().getReferenceType() ); System.out.
					 * println("test cr  : getLoanCalcDetailTypeBean().getRegisterNo() : " +
					 * cr.getLoanCalcDetailTypeBean().getRegisterNo() );
					 * System.out.println("test cr  : getLoanCalcDetailTypeBean().getTotalLoan() : "
					 * + cr.getLoanCalcDetailTypeBean().getTotalLoan() );
					 */
					String loanAmount = cr.getLoanCalcDetailTypeBean().getLoanAmount();
					String oldLoanDate = cr.getLoanCalcDetailTypeBean().getLoanDate();
					String comLoanAmount = cr.getLoanCalcDetailTypeBean().getComLoanAmount();
					String loanYear = cr.getLoanCalcDetailTypeBean().getLoanYear();
					/*
					 * System.out.println("LOAN ::::::::::: " + policyStatus1 );
					 * System.out.println("LOAN loanAmount: " + loanAmount );
					 * System.out.println("LOAN oldLoanDate: " + oldLoanDate );
					 * System.out.println("LOAN comLoanAmount: " + comLoanAmount );
					 * System.out.println("LOAN loanYear: " + loanYear );
					 */
					if (!M.itis(loanAmount, '0')) {
						v.addElement(M.stou("à¸ˆà¸³à¸™à¸§à¸™à¹€à¸‡à¸´à¸™à¸•à¹‰à¸™ ") + M.edits(loanAmount) + ".- "
								+ M.stou("à¸§à¸±à¸™à¸—à¸µà¹ˆà¸à¸¹à¹‰ ") + oldLoanDate);
					} else {
						v.addElement(M.stou("à¹„à¸¡à¹ˆà¸¡à¸µà¹€à¸‡à¸´à¸™à¸à¸¹à¹‰à¹€à¸”à¸´à¸¡"));
					}

					if (("IFJB").indexOf(policyStatus1) >= 0) {
						if (M.itis(comLoanAmount, '0')) {
							v.addElement(M.stou("à¹„à¸¡à¹ˆà¸¡à¸µà¸ªà¸´à¸—à¸˜à¸´à¹Œà¸à¸¹à¹‰"));
						} else {
							v.addElement(M.stou("à¸à¸¹à¹‰à¸›à¸µà¸—à¸µà¹ˆ ") + loanYear + " "
									+ M.stou("à¸ˆà¸³à¸™à¸§à¸™à¹€à¸‡à¸´à¸™") + " " + M.edits(comLoanAmount) + ".- "
									+ M.stou("à¸šà¸²à¸—"));
						}
					} else {
						v.addElement(M.stou("à¸à¸£à¸¡à¸˜à¸£à¸£à¸¡à¹Œà¸ªà¸–à¸²à¸™à¸°")
								+ StatusPolicy.statusDesc(policyStatus1)
								+ M.stou("à¸™à¸µà¹‰à¹„à¸¡à¹ˆà¸¡à¸µà¸ªà¸´à¸—à¸˜à¸´à¹Œà¸à¸¹à¹‰"));
					}

				} else {
					System.out.println("rs.value() UUUU : " + rs.value());
					if (rs.value() instanceof String[])
						ZArray.show((String[]) rs.value());
				}
			}
			/*
			 * else if (rs.status() == 0 && !policyType.equals("U")) {
			 * System.out.println("!policyType.equals(\"U\") "); String[] data = (String[])
			 * rs.value(); ZArray.show(data); if (!M.itis(data[3],'0')) {
			 * v.addElement(M.stou("à¸ˆà¸³à¸™à¸§à¸™à¹€à¸‡à¸´à¸™à¸•à¹‰à¸™ ")+M.edits(data[3])
			 * +".- "+M.stou("à¸§à¸±à¸™à¸—à¸µà¹ˆà¸à¸¹à¹‰ ")+data[2].substring(6)+"/"+data[2
			 * ].substring(4,6)+"/"+data[2].substring(0,4)); } else {
			 * v.addElement(M.stou("à¹„à¸¡à¹ˆà¸¡à¸µà¹€à¸‡à¸´à¸™à¸à¸¹à¹‰à¹€à¸”à¸´à¸¡")); }
			 * if (("IFJB").indexOf(policyStatus1) >= 0 ) { if (M.itis(data[1],'0')) {
			 * v.addElement(M.stou("à¹„à¸¡à¹ˆà¸¡à¸µà¸ªà¸´à¸—à¸˜à¸´à¹Œà¸à¸¹à¹‰")); } else {
			 * v.addElement(M.stou("à¸à¸¹à¹‰à¸›à¸µà¸—à¸µà¹ˆ ")+data[0]+" " +
			 * M.stou("à¸ˆà¸³à¸™à¸§à¸™à¹€à¸‡à¸´à¸™") + " " + M.edits(data[1]) + ".- " +
			 * M.stou("à¸šà¸²à¸—")); } } else {
			 * v.addElement(M.stou("à¸à¸£à¸¡à¸˜à¸£à¸£à¸¡à¹Œà¸ªà¸–à¸²à¸™à¸°")+StatusPolicy.
			 * statusDesc(policyStatus1)+M.stou(
			 * "à¸™à¸µà¹‰à¹„à¸¡à¹ˆà¸¡à¸µà¸ªà¸´à¸—à¸˜à¸´à¹Œà¸à¸¹à¹‰")); } }
			 */
			else {
				System.out.println("else " + policyStatus1);
				v.addElement(M.stou("à¹„à¸¡à¹ˆà¸¡à¸µà¹€à¸‡à¸´à¸™à¸à¸¹à¹‰à¹€à¸”à¸´à¸¡"));
				String msg = rs.value().toString();
				System.out.println("else msg " + msg);
				if (("IFJB").indexOf(policyStatus1) >= 0) {
					if (msg.trim().equals("")) {
						v.addElement(M.stou("à¹„à¸¡à¹ˆà¸¡à¸µà¸ªà¸´à¸—à¸˜à¸´à¹Œà¸à¸¹à¹‰"));
					} else {
						v.addElement(rs.value().toString());
					}
				} else {
					v.addElement(
							M.stou("à¸à¸£à¸¡à¸˜à¸£à¸£à¸¡à¹Œà¸ªà¸–à¸²à¸™à¸°") + StatusPolicy.statusDesc(policyStatus1)
									+ M.stou("à¸™à¸µà¹‰à¹„à¸¡à¹ˆà¸¡à¸µà¸ªà¸´à¸—à¸˜à¸´à¹Œà¸à¸¹à¹‰"));
				}
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.toString());
		}
		return v;
	}

	String getLifePrem(boolean extra) {
		String prem = "0";
		for (int i = 0; i < vrid.size(); i++) {
			String[] ar = (String[]) vrid.elementAt(i);
			if (ar[0].equals(M.stou("à¸Šà¸§")) || ar[0].equals(("RP"))) {
				if (extra)
					return ar[2];
				else {
					System.out.println("LIFE PREMIUM = " + ar[1]);
					return ar[1];
				}
			}
		}
		return prem;
	}

	/*
	 * String[] getRiderPrem(String riderType) {
	 * System.out.println("--** -- "+vrid.size()); if(!"L".equals(policyType)) { for
	 * (int i = 0; i < vrid.size(); i++) {
	 * 
	 * System.out.println("<<>>"); String[] ar = (String[])vrid.elementAt(i);
	 * 
	 * System.out.println("ar[0] : "+ar[0]+": riderType :"+riderType);
	 * 
	 * if (!ar[0].equals(M.stou("à¸Šà¸§"))) {
	 * System.out.println("--***** -- "+ar[0]+"--"+ar[1]+"**"+ar[2]); if
	 * (riderType.equals(ar[0])) { return (new String[] {ar[0],ar[1],ar[2]}); } } }
	 * }else { //Rider Iservice UnitLinkPremium mp = null; try {
	 * 
	 * Result rs = PublicRte.getResult("blunitlink",
	 * "rte.bl.unitlink.batch.income.UnitlinkPremium", new String[] {policyNo,
	 * nextpayperiod, duedate}); if (rs.status() == -1) {
	 * System.out.println("status = -1"); //System.out.println(rs.value());
	 * 
	 * } else if (rs.status() == 0) System.out.println("true");
	 * 
	 * 
	 * //import rte.bl.unitlink.master.UnitLinkPremium; //mp = new
	 * UnitLinkPremium(policyNo, nextpayperiod, duedate); mp.premiumMaster(policyNo,
	 * nextpayperiod, duedate); //mp.getValue(); mp.calPremium(); } catch (Exception
	 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
	 * 
	 * 
	 * // System.out.println(" <> "+mp.getValue().getClass()); Vector v =
	 * mp.rider();
	 * 
	 * String [] rd = null; for(int i=0;i< v.size();i++) {
	 * System.out.println(":"+i+":"+v.elementAt(i)); rd = (String[])v.elementAt(i);
	 * for (int j = 0; j < rd.length; j++) {
	 * 
	 * //System.out.println("Nut -*-*-*-"+rd[0]+" ** "+rd[1]+" *** "+rd[2]); if
	 * (riderType.equals(rd[0])) { return (new String[] {rd[0],rd[1],rd[2]}); }
	 * 
	 * }
	 * 
	 * 
	 * }
	 * 
	 * 
	 * //return (new String[] {rd[0],rd[1],rd[2]}); } return (new String[]
	 * {"0","0","0"}); }
	 */

	String[] getRiderPrem(String riderType) {
		System.out.println("--** -- " + vrid.size());
		if (!"L".equals(policyType)) {
			for (int i = 0; i < vrid.size(); i++) {

				System.out.println("<<>>");
				String[] ar = (String[]) vrid.elementAt(i);

				System.out.println("ar[0] : " + ar[0] + ": riderType :" + riderType);

				if (!ar[0].equals(M.stou("à¸Šà¸§"))) {
					System.out.println("--***** -- " + ar[0] + "--" + ar[1] + "**" + ar[2]);
					if (riderType.equals(ar[0])) {
						return (new String[] { ar[0], ar[1], ar[2] });
					}
				}
			}
		} else {
			// Rider Iservice
			Result rs = PublicRte.getResult("blunitlink", "rte.bl.unitlink.master.RteUnitLinkPremium",
					new String[] { policyNo, nextpayperiod, duedate });
			if (rs.status() == -1) {
				System.out.println("status = -1");
			} else if (rs.status() == 0)
				System.out.println("true");

			Vector mp = (Vector) rs.value();
			System.out.println("UNITLINKPREMIUM L");

			Vector a = (Vector) mp.elementAt(0);
			System.out.println(a.elementAt(0));

			for (int i = 0; i < a.size(); i++) {
				String[] b = (String[]) a.elementAt(i);
				for (int j = 0; j < b.length; j++) {
					System.out.println(b[j]);
					if (riderType.equals(b[0])) {
						return (new String[] { b[0], b[1], b[2] });
					}
				}
			}

		}
		return (new String[] { "0", "0", "0" });
	}

	public String getPlanCode(char Type, String plan) {
		PlanSpec.planCodes(Type);
		TLPlan pc = PlanSpec.getPlan(plan);
		return pc.name();
	}

	public String indMatureDate(String plan, String effDate) {
		PlanSpec.planCodes('I');
		TLPlan pc = PlanSpec.getPlan(plan);
		return M.addnum(effDate.substring(0, 4), pc.endowmentYear(file.indmast.get("insuredAge")))
				+ effDate.substring(4);
	}

	// ulip
	public String ulipMatureDate(String plan, String effDate) {
		PlanSpec.planCodes('L');
		TLPlan pc = PlanSpec.getPlan(plan);
		return M.addnum(effDate.substring(0, 4), pc.endowmentYear(file.ulip.get("insuredAge"))) + effDate.substring(4);
	}

	/*
	 * public static String nextPayPeriod(String payPeriod, String mode, String
	 * type) { System.out.println("nextPayPeriod : payPeriod = "+ payPeriod +
	 * " mode = "+ mode + " type = " + type); String showPeriod = ""; if
	 * (type.equals("I")) { String year = payPeriod.substring(0,4); String period =
	 * payPeriod.substring(4); if (M.cmps(period,"12") == 0) { period = "01"; year =
	 * M.inc(year); } else { period = M.inc(period); } showPeriod = period+"/"+year;
	 * } else {
	 * 
	 * String year = payPeriod.substring(0,2); String period =
	 * payPeriod.substring(2); switch (mode.charAt(0)) { case '1' : year =
	 * M.inc(year); break; case '0' : period = M.inc(period); if
	 * (M.cmps(period,"12") > 0) { period = "01"; year = M.inc(year); } break; case
	 * '2' : period = M.inc(period); if (M.cmps(period,"02") > 0) { period = "01";
	 * year = M.inc(year); } break; case '4' : period = M.inc(period); if
	 * (M.cmps(period,"04") > 0) { period = "01"; year = M.inc(year); } break; }
	 * showPeriod = year+"/"+period; } return showPeriod; }
	 */
	public static String nextPayPeriod(String payPeriod, String mode, String type) {
		// INDMAST
		String showPeriod = "";
		if (type.equals("I")) {
			String year = payPeriod.substring(0, 4);
			String period = payPeriod.substring(4);
			if (M.cmps(period, "12") == 0) {
				period = "01";
				year = M.inc(year);
			} else
				period = M.inc(period);
			showPeriod = period + "/" + year;
			// showPeriod = year+period;
		} else {
			String year = payPeriod.substring(0, 2);
			String period = payPeriod.substring(2);
			switch (mode.charAt(0)) {
			case '1':
				year = M.inc(year);
				break;
			case '0':
				period = M.inc(period);
				if (M.cmps(period, "12") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '2':
				period = M.inc(period);
				if (M.cmps(period, "02") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '4':
				period = M.inc(period);
				if (M.cmps(period, "04") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			}
			// showPeriod = year+"/"+period;
			showPeriod = year + period;
		}
		return showPeriod;
	}

	public static String nextPayPeriodwithSlash(String payPeriod, String mode, String type) {
		String showPeriod = "";
		if (type.equals("I")) {
			String year = payPeriod.substring(0, 4);
			String period = payPeriod.substring(4);
			if (M.cmps(period, "12") == 0) {
				period = "01";
				year = M.inc(year);
			} else
				period = M.inc(period);
			showPeriod = period + "/" + year;
		} else {
			String year = payPeriod.substring(0, 2);
			String period = payPeriod.substring(2);
			switch (mode.charAt(0)) {
			case '1':
				year = M.inc(year);
				break;
			case '0':
				period = M.inc(period);
				if (M.cmps(period, "12") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '2':
				period = M.inc(period);
				if (M.cmps(period, "02") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			case '4':
				period = M.inc(period);
				if (M.cmps(period, "04") > 0) {
					period = "01";
					year = M.inc(year);
				}
				break;
			}
			showPeriod = year + "/" + period;
		}
		return showPeriod;

	}

	public void showHistoryCondpay(String policyNo) throws Exception {
		System.out.println();
		ArrayList<ServicePaymentTransaction> alser = Fund_Service.getServicePaymentTrans(policyNo);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> alser size : " + alser.size());
		// utility.screen.SCR.initTable(screen.tbCondpay);
		for (int x = 0; x < alser.size(); x++) {
			ServicePaymentTransaction rSer = alser.get(x);
			String serviceType = rSer.getServiceType();
			String approveDate = rSer.getApproveDate();
			String av = rSer.getAv();
			String srCharge = rSer.getSrCharge();
			String cv = rSer.getCv();
			String paymentDate = rSer.getPaymentDate();
			String paymentBy = rSer.getPaymentBy();
			// System.out.println("**"+serviceType);
			if (serviceType.equals("PW"))
				serviceType = M.stou("à¸–à¸­à¸™à¹€à¸‡à¸´à¸™à¸šà¸²à¸‡à¸ªà¹ˆà¸§à¸™à¸ˆà¸²à¸à¸à¸£à¸¡à¸˜à¸£à¸£à¸¡à¹Œ");
			else if (serviceType.equals("RC"))
				serviceType = M.stou("à¸¡à¸¹à¸¥à¸„à¹ˆà¸²à¹€à¸§à¸™à¸„à¸·à¸™à¸à¸£à¸¡à¸˜à¸£à¸£à¸¡à¹Œ");

			if (paymentBy.charAt(0) == 'T')
				paymentBy = M.stou("à¹‚à¸­à¸™à¹€à¸‡à¸´à¸™à¹€à¸‚à¹‰à¸²à¸šà¸±à¸à¸Šà¸µ");
			else if (paymentBy.charAt(0) == 'Q')
				paymentBy = M.stou("à¸ªà¹ˆà¸‡à¹€à¸Šà¹‡à¸„");

			// Object[] row = new Object[]{serviceType, approveDate, av, srCharge, cv,
			// paymentDate, paymentBy};
			// screen.tbCondpay.appendRow(row);
			System.out.println(" * " + x + " * " + serviceType);
			System.out.println(" * " + x + " * " + approveDate);
			System.out.println(" * " + x + " * " + av);
			System.out.println(" * " + x + " * " + srCharge);
			System.out.println(" * " + x + " * " + cv);
			System.out.println(" * " + x + " * " + paymentDate);
			System.out.println(" * " + x + " * " + paymentBy);

		}
	}

}
