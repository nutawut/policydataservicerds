package com.thailife.api.policyInfoService.util;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Component
public class CallWS {

  @Value("${url_token}")
  private String urlToken;

  @Value("${Consumer_Key}")
  private String consumerKey;

  @Value("${Consumer_Secret}")
  private String consumerSecret;

  @Value("${GATEWAY_WS}")
  private String gatewayWs;

  @Value("${ENDPOINT}")
  private String endPoint;

  @Value("${PARTY_ENDPOINT}")
  private String partyEndPoint;

  @Value("${Consent_Key}")
  private String consentKey;

  @Value("${Consent_Secret}")
  private String consentSecret;
  

  @Value("${SuperApp_Secret}")
  private String superAppSecret;

  @Value("${SuperApp_Key}")
  private String superAppKey;

//SuperApp_Key=xD9GjB0rJAXAt4TnnNIRfNDUuX8a
//SuperApp_Secret=TIOadXabsR9g48qFyuubIx5V7tka


  private static final Log log = new Log();
//
  

  public String callCSVWsWithToken(String context, String request) /*throws Exception*/ {
	  
	 return  callWsWithToken(context, request ,consumerKey, consumerSecret);
  }

  public String callSuperAppWsWithToken(String context, String request) /*throws Exception*/ {
	  
	 return  callWsWithToken(context, request ,superAppKey, superAppSecret);
  }
  
  public String callWsWithToken(String context, String request , String key , String secret) /*throws Exception*/ {
    try {
      //      ApiConfigM apiConfigM = readProperties();
      //      System.out.println(apiConfigM.toString());
      String jsonToken = callToken(gatewayWs.concat(urlToken), key, secret);

      String authorization = getAuthorization(jsonToken);

      if (StringUtils.isEmpty(authorization)) throw new Exception("Not found token");
 
      return callWsWithToken(gatewayWs.concat(context), request, authorization);

    } catch (Exception e) {
      //log.error(String.format("CallWS.callWsWithToken:[%s] ERROR %s", context, e.getMessage()));
      return null;
    }
  }
  
  

  private ApiConfigM readProperties() throws IOException {
    final File file =
        new File(CallWS.class.getProtectionDomain().getCodeSource().getLocation().getPath());
    final String PATH = file.getPath();
    final String configPath = PATH + "/api.config.properties";

    System.out.println("configPath = " + configPath);
    Properties prop = new Properties();
    try {
      FileInputStream input = new FileInputStream(configPath);
      prop.load(input);
      ApiConfigM apiConfigM = new ApiConfigM();
      apiConfigM.setUrl_token(prop.getProperty("url_token"));
      apiConfigM.setConsumer_Key(prop.getProperty("Consumer_Key"));
      apiConfigM.setConsumer_Secret(prop.getProperty("Consumer_Secret"));
      apiConfigM.setGATEWAY_WS(prop.getProperty("GATEWAY_WS"));
      apiConfigM.setENDPOINT(prop.getProperty("ENDPOINT"));
      apiConfigM.setPARTY_ENDPOINT(prop.getProperty("PARTY_ENDPOINT"));
      apiConfigM.setCONSENT_KEY(prop.getProperty("Consent_Key"));
      apiConfigM.setCONSENT_SECRET(prop.getProperty("Consent_Secret"));
      return apiConfigM;
    } catch (IOException e) {
      throw e;
    }
  }

  private String callToken(String url_token, String Consumer_Key, String Consumer_Secret)
      throws Exception {
    URL url = new URL(url_token);
    String request = "grant_type=client_credentials";
    request += "&client_id=" + Consumer_Key;
    request += "&client_secret=" + Consumer_Secret;
    byte[] postData = request.getBytes(StandardCharsets.UTF_8);
    int postDataLength = postData.length;
    System.out.println(
        "url_token = "
            + url_token
            + ", Consumer_Key = "
            + Consumer_Key
            + ", Consumer_Secret = "
            + Consumer_Secret);
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);
    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
    connection.setUseCaches(false);
    connection.setRequestProperty("charset", "utf-8");

    return callWS(connection, request);
  }

  private String callWS(HttpURLConnection connection, String request) throws Exception {
    OutputStream os = null;
    BufferedReader br = null;
    try {
      os = connection.getOutputStream();
      os.write(request.getBytes(StandardCharsets.UTF_8));
      if (connection.getResponseCode() == HttpURLConnection.HTTP_OK
          || connection.getResponseCode() == HttpURLConnection.HTTP_CREATED)
        ; // doNothing
      else {
        br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
        String tmp = "";
        StringBuilder output = new StringBuilder();
        while ((tmp = br.readLine()) != null) output.append(tmp);
        throw new Exception(
            "Failed : HTTP error code : " + connection.getResponseCode() + "\n, Error : " + output);
      }
      br =
          new BufferedReader(
              new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
      String tmp = "";
      StringBuilder output = new StringBuilder();
      while ((tmp = br.readLine()) != null) output.append(tmp);
      return output.toString();
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    } finally {
      if (os != null) os.close();
      if (br != null) br.close();
      if (connection != null) connection.disconnect();
    }
  }

  public String callWsWithToken(String url_ws, String request, String authorization)
      throws Exception {
    //log.info(String.format("CallWS.callWsWithToken:[request] %s || url_ws: %s", request, url_ws));
    URL url = new URL(url_ws);

    byte[] postData = request.getBytes(StandardCharsets.UTF_8);
    int postDataLength = postData.length;
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("Authorization", authorization);
    connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
    connection.setUseCaches(false);
    connection.setRequestProperty("charset", "utf-8");

    return callWS(connection, request);
  }

  public String callWS(String url_ws, String request) throws Exception {
    URL url = new URL(url_ws);
    byte[] postData = request.getBytes(StandardCharsets.UTF_8);
    int postDataLength = postData.length;
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
    connection.setUseCaches(false);
    connection.setRequestProperty("charset", "utf-8");
    log.printDebug("callWS", "callWS URL : " + url_ws);
    return callWS(connection, request);
  }

  public String getAuthorization(String text) throws Exception {
    JSONObject token = new JSONObject(text);
    String access_token = "";
    String token_type = "";
    if (token.has("access_token")) access_token = (String) token.get("access_token");
    if (token.has("token_type")) token_type = (String) token.get("token_type");
    return token_type + " " + access_token;
  }

  public static void main(String[] args) throws IOException {
    CallWS callWS = new CallWS();

    ApiConfigM apiConfigM = callWS.readProperties();
    System.out.println("apiConfigM.toString() = " + apiConfigM.toString());
  }
}
