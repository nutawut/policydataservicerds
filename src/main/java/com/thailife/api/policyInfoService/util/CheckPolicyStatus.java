package com.thailife.api.policyInfoService.util;

import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.impl.CertMappingDaoImpl;
import com.thailife.api.policyInfoService.model.response.PolicyM;
import com.thailife.log.ErrorLog;
import com.thailife.log.ExtLogger;
import thaillife.customertool.jdbc.policy.SearchPolicyStatusJdbc;
import thaillife.customertool.model.PolicyStatusM;
import thaillife.customertool.service.PolicyService;
import utility.underwrite.bankassurecomps.PolicyCL;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

public class CheckPolicyStatus {
    private static final ExtLogger logger = ExtLogger.create(CheckPolicyStatus.class);
    private static SearchPolicyStatusJdbc searchPolicyStatusJdbc = new SearchPolicyStatusJdbc();
    private static PolicyService policyService = new PolicyService();
    private static CertMappingDaoImpl certMappingDao =new CertMappingDaoImpl();
    public static Boolean ValidateExistingPolicy(List<PolicyM> listPolicy, String citizenId) {
        try(Connection con = DBConnection.getConnectionPolicy()) {
            //String policy = new PolicyStatusM();
            for (PolicyM policy : listPolicy) {
//                String policyNo = "";
//                String certNo = "";
                if(policy.getCertNo().length() > 0)
                {
                	if (policy.getCertNo().length() == 15) {//check Old CertNo 15 character 
                    	HashMap<String,String> pol = certMappingDao.getPolicyNoAndCertNoByRcertNo(policy.getCertNo(),con);
                		//                    policyNo = pol.get("policyNo");
                		//                    certNo = pol.get("certNo") ;
                		policy.setPolicyNo(pol.get("policyNo"));
                		policy.setCertNo(pol.get("certNo"));

                	}else {
                		policy.setPolicyNo(PolicyCL.baToCode(policy.getPolicyNo())); 
                	}
                }
                if(policy.getPolicyNo() == null || policy.getPolicyNo().equals("")){
                    return true;
                }
                List<PolicyStatusM> listPolicySearch = searchPolicyStatusJdbc.searchPolicyStatus(policy.getPolicyNo(), policy.getCertNo(), con);
                for (PolicyStatusM policySearch : listPolicySearch) {
                    String policyType = "OL";
                    if(policySearch.getCertno() == null || !policySearch.getCertno().equals("")){
                        policyType = "CL";
                    }
                    if(!policyService.isPolicyStatusIgnore(
                            policyType, policySearch.getStatus1(),
                            policySearch.getStatus2(),
                            policySearch.getOldstatus1(), policySearch.getOldstatus2())
                    ){
                        if (!policySearch.getReferenceid().equals(citizenId)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.fail(new ErrorLog("InsertConsent", "", "", "ValidateExistingPolicy", "", "Exception", e));
            return true;
        }
        return false;
    }
}