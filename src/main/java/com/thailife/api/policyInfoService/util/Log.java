package com.thailife.api.policyInfoService.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.controller.pol18.SearchPolicyOnlyByPartyIdController;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.log.ExtLogger;
import lombok.extern.log4j.Log4j2;

//@Log4j2
public class Log {
    private final ExtLogger log = ExtLogger.create(Log.class);

    public void info(DataServiceRequestT<?> req, String servicename) {
    ObjectMapper mapper = new ObjectMapper();
    String request;
    try {
      request = mapper.writeValueAsString(req);
      String message = servicename + "\n" + request;
      //			CreateLogFile();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void printError(String Service, String tranId, String className, String msg) {
    //      CreateLogFile();
    String logMsg =
        "<API:" + Service + "><" + "transaction: " + tranId + " ><" + className + "> " + msg;
    log.error(logMsg);
  }

  public void printError(String className, String msg) {
    //      CreateLogFile();
    String logMsg = "<" + className + "> " + msg;
    log.error(logMsg);
  }

  public void printInfo(String Service, String tranId, String className, String msg) {
    //      CreateLogFile();
    String logMsg =
        "<API:" + Service + "><" + "transaction: " + tranId + " ><" + className + "> " + msg;
    log.info(logMsg);
  }

  public void printInfo(String className, String msg) {
    //      CreateLogFile();
    String logMsg = "<" + className + "> " + msg;
    log.info(logMsg);
  }

  public void printDebug(String Service, String tranId, String className, String msg) {
    //      CreateLogFile();
    String logMsg =
        "<API:" + Service + "><" + "transaction: " + tranId + " ><" + className + "> " + msg;
    log.debug(logMsg);
  }

  public void printDebug(String className, String msg) {
    //      CreateLogFile();
    String logMsg = "<" + className + "> " + msg;
    log.debug(logMsg);
  }
}
