package com.thailife.api.policyInfoService.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import utility.database.M;

public class DateFormat {
	public static final String DDMMYYYY_HHMMSS = "dd-MM-yyyy HH:mm:ss";
	public static final String YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss";

	public static String now(String formate) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(formate);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}

	public static String unformatDate(String date) {
		if (date != null && date.trim().length() >= 8) {
			String[] d = date.split("/");
			if (d.length == 3 && M.dateok(d[2] + PolicyUtil.addDataL(d[1], "0", 2) + PolicyUtil.addDataL(d[0], "0", 2)))
				date = d[2] + PolicyUtil.addDataL(d[1], "0", 2) + PolicyUtil.addDataL(d[0], "0", 2);
		}
		return date;
	}
	public static String nowddmmyyyy() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DDMMYYYY_HHMMSS);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}
	public static String nowyyyymmdd() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(YYYYMMDD_HHMMSS);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}
}
