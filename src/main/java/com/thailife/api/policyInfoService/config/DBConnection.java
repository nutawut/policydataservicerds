package com.thailife.api.policyInfoService.config;

import com.thailife.api.policyInfoService.util.Contains;
import com.thailife.api.policyInfoService.util.DBproperties;
import utility.database.ConnectionBean;
import utility.database.DBConnectionFile;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static String url;
    private static String username;
    private static String password;

    public static Connection con(String DbName, String Schema) throws Exception {
        try {
//            setDetailDBByLocal(DbName);
            setDetailDBByServer(DbName);
            return DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("Can't connect to database.");
            throw e;
        }
    }

    private static void setDetailDBByServer(String mappingDB) {
        File fileEncrypted = new File("/c/resources/db/dbconfig.cfg");
        File EncryptKey = new File("/c/resources/db/dbkey.cfg");
        DBConnectionFile fileDB = new DBConnectionFile(fileEncrypted, EncryptKey, mappingDB);
        ConnectionBean con = fileDB.getConnectionBean();
        url =
                String.format(
                        "jdbc:postgresql://%s:%s/%s",
                        con.getHostName(), con.getPortNo(), con.getDatabaseName());
        username = con.getUserName();
        password = con.getPassword();
    }

    private static void setDetailDBByLocal(String mappingDB) {
        if (mappingDB.startsWith("DBPolicy") || mappingDB.startsWith("DBUnitlink")) {
            url = Contains.DBInfo.urlPol;
            username = Contains.DBInfo.usernamePol;
            password = Contains.DBInfo.passwordPol;
        } else if (mappingDB.startsWith("DBCollection") || mappingDB.startsWith("DBPOS")) {
            url = Contains.DBInfo.urlPos;
            username = Contains.DBInfo.usernamePos;
            password = Contains.DBInfo.passwordPos;
        } else if (mappingDB.startsWith("DBClaim")) {
            url = Contains.DBInfo.urlClaim;
            username = Contains.DBInfo.usernameClaim;
            password = Contains.DBInfo.passwordClaim;
        } else if (mappingDB.startsWith("DBCustomer")) {
            url = Contains.DBInfo.urlCus;
            username = Contains.DBInfo.usernameCus;
            password = Contains.DBInfo.passwordCus;
        } else if (mappingDB.startsWith("DBSaleStruct")) {
            url = Contains.DBInfo.urlSale;
            username = Contains.DBInfo.usernameSales;
            password = Contains.DBInfo.passwordSale;
        } else if (mappingDB.startsWith("DBBusProduct")) {
            url = Contains.DBInfo.urlProduct;
            username = Contains.DBInfo.usernameProduct;
            password = Contains.DBInfo.passwordProduct;
        }
    }

    public static Connection getConnectionPolicy() throws Exception {
        return con(DBproperties.getDBPolicy(), "");
    }

    public static Connection getConnectionCustomer() throws Exception {
        return con(DBproperties.getDBCustomer(), "");
    }

    public static Connection getConnectionClaim() throws Exception {
        return con(DBproperties.getDBClaim(), "");
    }

    public static Connection getConnectionUnitLink() throws Exception {
        return con(DBproperties.getDBUnitLink(), "");
    }

    public static Connection getConnectionCollection() throws Exception {
        return con(DBproperties.getDBCollection(), "collection");
    }

    public static Connection getConnectionBusProduct() throws Exception {
        return con(DBproperties.getDBBusProduct(), "");
    }

    public static Connection getConnectionSaleStruct() throws Exception {
        return con(DBproperties.getDBSaleStruct(), "");
    }

    public static Connection getConnectionPos() throws Exception {
        return con(DBproperties.getDBPos(), "");
    }


    public static void close(Connection con) {
        try {
            if (con != null && !con.isClosed()) {
                con.close();
                con = null;
                System.out.println("close connection.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

