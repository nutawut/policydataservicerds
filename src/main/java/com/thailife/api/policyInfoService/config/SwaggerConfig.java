package com.thailife.api.policyInfoService.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket produceApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.thailife.api.policyInfoService"))
        .paths(paths())
        .build();
  }

  // Describe your apis
  private ApiInfo apiInfo() {
    String version = "1.0";
    return new ApiInfoBuilder()
        .title("PolicyDataServiceRds Rest APIs")
        .description("This page lists all the rest apis for PolicyDataServiceRds.")
        .termsOfServiceUrl("")
        .contact(
            new Contact(
                "Thai Life Insurance Public Company Limited.",
                "https://www.thailife.com/",
                "chaianan.nee@thailife.com"))
        .license("สงวนสิทธิ์โดย บริษัท ไทยประกันชีวิต จำกัด (มหาชน)")
        .licenseUrl("xxxx")
        .version(version)
        .build();
  }

  // Only select apis that matches the given Predicates.
  private Predicate<String> paths() {
    // Match all paths except /error
    return Predicates.and(PathSelectors.any(), Predicates.not(PathSelectors.regex("/error.*")));
  }
}
