package com.thailife.api.policyInfoService.controller.cus36;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.RiderTypeListRequest;
import com.thailife.api.policyInfoService.model.request.RiderTypeRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.RiderTypeListResponse;
import com.thailife.api.policyInfoService.model.response.RiderTypeResponse;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByCustomerIdResponse;
import com.thailife.api.policyInfoService.util.DateFormat;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import insure.RiderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
public class RiderTypeController extends WebServiceControllerResponseT<
		RiderTypeListRequest, RiderTypeListResponse> {
	private final Log log = new Log();
	private String Service = "RiderType";
	private String className = "RiderTypeController";
	private String transactionId;
	@Autowired
	WebServiceMsg wMessage;

	@Override
	@RequestMapping(value = "/RiderType", method = RequestMethod.POST)
	public ResponseEntity<ResponseMessages<RiderTypeListResponse>> service(RequestMessages<RiderTypeListRequest> req) throws Exception {
		ResponseEntity<ResponseMessages<RiderTypeListResponse>> entityResponse = null;
		transactionId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
		List<RiderTypeRequest> riderTypeReq = req.getRequestRecord().getRiderTypeList();
		String data = "";
		for (RiderTypeRequest riderType : riderTypeReq) {
			String riderCode = riderType.getRiderType();
			data += "riderCode:" + riderCode + ",";
		}

		log.printInfo(Service, transactionId, className, "Service Req : " + req);

		// initial respones obj
		RiderTypeResponse riderTypeResp;
		List<RiderTypeResponse> listRiderTypeResp = new ArrayList<RiderTypeResponse>();
		RiderTypeListResponse RiderTypeList = new RiderTypeListResponse();
		RiderTypeList.setList_rider(listRiderTypeResp);
		ResponseMessages<RiderTypeListResponse> response = new ResponseMessages<>(
				RiderTypeList);

		// validate request data
		if (!validateRequestMessages(req)) {
			log.printError(transactionId, transactionId, className, "error validate");
			return getErrorValidateResponse(
					response,
					req.getHeaderData(),
					wMessage.getMessageByCode("400")[0],
					wMessage.getMessageByCode("400")[1]);
		}

		// set Header ResponseDateTime
		req.getHeaderData().setResponseDateTime(DateFormat.now(DateFormat.DDMMYYYY_HHMMSS));
		response.setHeaderData(req.getHeaderData());

		try {
			// System.out.println("----- Start RiderType ----- ");

			String ridercode = "";
			String ridername = "";
			String ridersortname = "";

			for (int i = 0; i < req.getRequestRecord().getRiderTypeList().size(); i++) {

				riderTypeResp = new RiderTypeResponse();
				ridercode = riderTypeReq.get(i).getRiderType();
				ridername = RiderType.getFullName(RiderType.isIslam(ridercode), ridercode);
				ridersortname = RiderType.getShortName(ridercode, RiderType.isPARider(ridercode));

				riderTypeResp.setRiderType(ridercode);
				riderTypeResp.setRiderName(ridername);
				riderTypeResp.setRiderShortName(ridersortname);

				listRiderTypeResp.add(riderTypeResp);
			}
			// if list empty return 204
			if (listRiderTypeResp.size() == 0) {
				return getNotFoundDataResponse(
						response,
						req.getHeaderData(),
						wMessage.getMessageByCode("204")[0],
						wMessage.getMessageByCode("204")[1]);
			} else {
				return getSuccessResponse(
						response,
						req.getHeaderData(),
						wMessage.getMessageByCode("200")[0],
						wMessage.getMessageByCode("200")[1]);
			}
			// set ResponseStatus success
		} catch (Exception e) {
			e.printStackTrace();
			log.printError(Service, transactionId, className, "error Exception : " + e.getMessage());
			// set ResponseStatus error
			return getErrorInternalServerResponse(
							response,
							req.getHeaderData(),
							wMessage.getMessageByCode("500")[0],
							wMessage.getMessageByCode("500")[1]);
		}finally {
			log.printInfo(Service, transactionId, className, "Service Done : " + response);
		}
	}


	private String getRequest(String riderType) {
		String request = "{" + "\"headerData\": {" + "}," + " \"requestRecord\": { " + " \"riderType\": \"" + riderType
				+ "\"}" + "} ";
		return request;
	}

	@Override
	protected boolean validateRequestRecordData(RiderTypeListRequest req) throws Exception {
		// check data request each index
		for (int i = 0; i < req.getRiderTypeList().size(); i++) {
			// check Policy_no
			if (req.getRiderTypeList().get(i).getRiderType() == null
					|| req.getRiderTypeList().get(i).getRiderType().equals("")) {
				log.printInfo(Service, transactionId, className, "error validate riderType invalid");
				return false;
			}
		}

		return true;
	}
}