package com.thailife.api.policyInfoService.controller.pol02;

import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.PolicyDetailOLResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PolicyDetailOLService;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;

@RestController
public class PolicyDetailOLController
    extends WebServiceControllerResponseT<PolicyDetailRequest, PolicyDetailOLResponse> {

  private final ExtLogger log = ExtLogger.create(PolicyDetailOLController.class);
  String service = "PolicyDetailsOL";
  String className = "PolicyDetailOLController";

  @Autowired PolicyDetailOLService policyOLService;

  @Override
  @CrossOrigin
  @RequestMapping(value = "/PolicyDetailsOL", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<PolicyDetailOLResponse>> service(
      @RequestBody RequestMessages<PolicyDetailRequest> req) {
    ResponseEntity<ResponseMessages<PolicyDetailOLResponse>> entityResponse;
    ResponseMessages<PolicyDetailOLResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg() {};
    String TRANSACTION_ID = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    try {
      if (!validateRequestMessages(req)) {
        log.debug(service, TRANSACTION_ID, className, "Validate Error");
        entityResponse =
            getErrorValidateResponse(
                resp,
                req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      } else {
        log.info(
            PolicyUtil.pattenLog(
                TRANSACTION_ID,
                String.format("%s:%s | Service start %nRequest: %s", service, className, req)));

        ServiceResults<PolicyDetailOLResponse> data =
            policyOLService.getResponse(req.getRequestRecord(), TRANSACTION_ID);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
        } else {
          if (data.getResult() == null) {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getNotFoundDataResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("204")[0],
                    wMessage.getMessageByCode("204")[1]);
          } else {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("200")[0],
                    wMessage.getMessageByCode("200")[1]);
          }
        }
      }
    } catch (Exception e) {
      log.error(
          PolicyUtil.pattenLog(
              TRANSACTION_ID,
              String.format("%s:%s | Error Exception : %s", service, className, e.getMessage())));
      e.printStackTrace();
      entityResponse =
          getSuccessResponse(
              resp,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
    }
    log.info(
        PolicyUtil.pattenLog(
            TRANSACTION_ID,
            String.format(
                "%s:%s | Service done %nResponse : %s", service, className, entityResponse)));
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(PolicyDetailRequest req) throws Exception {
    if (req == null) {
      return false;
    } else {
      if (PolicyUtil.StringIsNullOrEmpty(req.getPolicyno()) || req.getPolicyno().length() > 8) {
        log.debug(
            PolicyUtil.pattenLog(
                "", String.format("%s:%s | error validate policy inValid", service, className)));
        return false;
      }
      if (PolicyUtil.StringIsNullOrEmpty(req.getType()) || req.getType().length() > 30) {
        log.debug(
            PolicyUtil.pattenLog(
                "", String.format("%s:%s | error validate type inValid", service, className)));
        return false;
      }
    }
    return true;
  }
}
