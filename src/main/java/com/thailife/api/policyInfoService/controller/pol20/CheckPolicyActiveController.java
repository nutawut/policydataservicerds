package com.thailife.api.policyInfoService.controller.pol20;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.api.policyInfoService.model.request.CheckPolicyActiveRequest;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@CrossOrigin
@RestController
public class CheckPolicyActiveController {
  private String tranId;

  @PostMapping("/checkPolicyActive")
  public ResponseEntity<?> controller(
      @RequestBody DataServiceRequestT<CheckPolicyActiveRequest> request) throws Exception {
    ResponseMessages<?> response = new ResponseMessages<>();
    tranId = request.getHeaderData() != null ? request.getHeaderData().getMessageId() : "";
    if (validateRequest(request)) {
      response.setHeaderData(request.getHeaderData());
      response.setResponseStatus(PolicyUtil.responseValidateError());
      response.setResponseRecord(null);
      return ResponseEntity.status(400).body(response);
    }
    String transactionId = request.getHeaderData().getMessageId();
    String citizenId = request.getRequestRecord().getCitizenId();
    log.info("CheckPolicyActiveController"+
            transactionId+
            ""+
            "controller"+
            String.format("citizenId: %s |", citizenId)+
            " [Start]");
    try {
      CheckPolicyActiveServiceImpl serviceImpl = new CheckPolicyActiveServiceImpl(transactionId);
      boolean isActive = serviceImpl.service(citizenId);
      if (!isActive) {
        response.setResponseStatus(PolicyUtil.responseDataNotFound());
      } else {
        response.setResponseStatus(PolicyUtil.responseStatusSuccess());
      }

    } catch (Exception e) {
      log.error(
          String.format("CheckPolicyActiveController | [ERROR]: %s ", e.getMessage()));
      response.setResponseStatus(PolicyUtil.responseInternalServerError());
      log.error(
              "CheckPolicyActiveController | "+
              transactionId+
              "|"+
              "controller"+
              new ObjectMapper().writeValueAsString(request)+
              "|Exception"+
              e);
    } finally {
      response.setHeaderData(PolicyUtil.headerDataResponse(request.getHeaderData()));
    }
    log.info("CheckPolicyActiveController | [END].");
    log.info(
            "CheckPolicyActiveController | "+
            transactionId+
            "|"+
            "|controller"+
            "|"+
            "|CheckPolicyActiveController | [END].");
    String httpCode = response.getResponseStatus().getErrorCode();
    return ResponseEntity.status(Integer.parseInt("204".equals(httpCode) ? "200" : httpCode))
        .body(response);
  }

  private boolean validateRequest(DataServiceRequestT<CheckPolicyActiveRequest> req) {
    if (req.getHeaderData() == null || validateHeader(req.getHeaderData())) {
      return true;
    }
    if (req.getRequestRecord() == null) {
      return true;
    }
    return req.getRequestRecord().getCitizenId() == null
            || ("").equals(req.getRequestRecord().getCitizenId().trim())
            || req.getRequestRecord().getCitizenId().length() > 13;
  }

  private boolean validateHeader(HeaderData headerData) {
    if (headerData.getMessageId() == null
            || ("").equals(headerData.getMessageId().trim())
            || headerData.getMessageId().length() > 50) {
      return true;
    }
    return headerData.getSentDateTime() == null || ("").equals(headerData.getSentDateTime().trim());
  }
}
