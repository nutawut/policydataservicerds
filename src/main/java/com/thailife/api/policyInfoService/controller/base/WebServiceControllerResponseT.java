package com.thailife.api.policyInfoService.controller.base;


import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import org.springframework.http.ResponseEntity;

public abstract class WebServiceControllerResponseT<T, S> extends WebServiceController<T, S> {

  public abstract ResponseEntity<ResponseMessages<S>> service(RequestMessages<T> req)
      throws Exception;

  protected ResponseEntity<ResponseMessages<S>> getSuccessResponse(
      ResponseMessages<S> response, HeaderData headerData, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus("S", errorCode, errorMessage);
    return ResponseEntity.ok(getResponseMessage(response, headerData, status));
  }
  protected ResponseEntity<ResponseMessages<S>> getErrorResponse(
          ResponseMessages<S> response, HeaderData headerData, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus("E", errorCode, errorMessage);
    return ResponseEntity.ok(getResponseMessage(response, headerData, status));
  }
  protected ResponseEntity<ResponseMessages<S>> getErrorValidateResponse(
      ResponseMessages<S> response, HeaderData headerData, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus("E", errorCode, errorMessage);
    return ResponseEntity.status(400).body(getResponseMessage(response, headerData, status));
  }

  protected ResponseEntity<ResponseMessages<S>> getErrorDuplicateResponse(
      ResponseMessages<S> response, HeaderData headerData, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus("S", errorCode, errorMessage);
    return ResponseEntity.status(200).body(getResponseMessage(response, headerData, status));
  }

  protected ResponseEntity<ResponseMessages<S>> getErrorInternalServerResponse(
      ResponseMessages<S> response, HeaderData headerData, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus("E", errorCode, errorMessage);
    return ResponseEntity.status(500).body(getResponseMessage(response, headerData, status));
  }
  
  protected ResponseEntity<ResponseMessages<S>> getNotFoundDataResponse(
      ResponseMessages<S> response, HeaderData headerData, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus("E", errorCode, errorMessage);
    return ResponseEntity.status(200).body(getResponseMessage(response, headerData, status));
  }

  protected ResponseEntity<ResponseMessages<S>> getNotFoundDataResponseError(
          ResponseMessages<S> response, HeaderData headerData, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus("E", errorCode, errorMessage);
    return ResponseEntity.status(200).body(getResponseMessage(response, headerData, status));
  }

  private ResponseMessages<S> getResponseMessage(
      ResponseMessages<S> response, HeaderData headerData, ResponseStatus status) {
    response.setHeaderData(setResponseTime(headerData));
    response.setResponseStatus(status);
    return response;
  }
  
}
