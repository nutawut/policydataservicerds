package com.thailife.api.policyInfoService.controller.pol20;


import com.thailife.api.policyInfoService.dao.impl.PersonDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.policy.PolicyDaoImpl;
import com.thailife.api.policyInfoService.model.CheckPolicyActiveBeanM;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;

import java.util.List;
@Log4j2
public class CheckPolicyActiveServiceImpl {
  private final String transactionId;

  public CheckPolicyActiveServiceImpl(String transactionId) {
    this.transactionId = transactionId;
  }

  public boolean service(String citizenId) {
    log.info("CheckPolicyActiveServiceImpl"+ transactionId+"service"+" [Start]");
    boolean isActive = false;
    try {
      String nameId = PersonDaoImpl.findNameIdByCitizenId(citizenId);
      if (nameId != null) {
        List<CheckPolicyActiveBeanM> list = PolicyDaoImpl.findStatusByNameId(nameId);
        if (list.size() > 0) {
          for (CheckPolicyActiveBeanM item : list) {
            if (isActive) break;
            if (item.getPolicyType().equals("OL") || item.getPolicyType().equals("ULIP")) {
              isActive =
                  PolicyUtil.checkRegisterOKByPolicyOLStatus(
                      item.getPolicyStatus1(),
                      item.getPolicyStatus2(),
                      item.getOldStatus1(),
                      item.getOldStatus2());
            } else if (item.getPolicyType().equals("CL")) {
              isActive =
                  PolicyUtil.checkRegisterOKByPolicyCLStatus(
                      item.getPolicyStatus1(),
                      item.getPolicyStatus2(),
                      item.getOldStatus1(),
                      item.getOldStatus2());
            }
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return isActive;
  }
}
