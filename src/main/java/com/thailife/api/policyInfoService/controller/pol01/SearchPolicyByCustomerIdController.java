package com.thailife.api.policyInfoService.controller.pol01;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByCustomerIdResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SearchPolicyByCustomerIdService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

// @Log4j
@Component
@RestController
public class SearchPolicyByCustomerIdController
    extends WebServiceControllerResponseT<
        SearchPolicyByCustomerIdRequest, SearchPolicyByCustomerIdResponse> {
  private final ExtLogger log = ExtLogger.create(SearchPolicyByCustomerIdController.class);
  private final String className = "SearchPolicyByCustomerIdController";
  private final String Service = "SearchPolicyByCustomerId";
  // private static String tranId;
  @Autowired SearchPolicyByCustomerIdService searchPolicyByCustomerIdService;
  @Autowired WebServiceMsg wMessage;

  @Override
  @RequestMapping(value = "/SearchPolicyByCustomerId", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<SearchPolicyByCustomerIdResponse>> service(
      @RequestBody RequestMessages<SearchPolicyByCustomerIdRequest> req) throws Exception {
    String TRANSACTION_ID = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    ResponseEntity<ResponseMessages<SearchPolicyByCustomerIdResponse>> entityResponse;
    ResponseMessages<SearchPolicyByCustomerIdResponse> resp = new ResponseMessages<>();
    try {
      if (!validateRequestMessages(req)) {
        log.info(
            PolicyUtil.pattenLog(
                TRANSACTION_ID, String.format("%s:%s error validate", Service, className)));
        return getErrorValidateResponse(
            resp,
            req.getHeaderData(),
            wMessage.getMessageByCode("400")[0],
            wMessage.getMessageByCode("400")[1]);
      }
      log.info(
          PolicyUtil.pattenLog(
              TRANSACTION_ID,
              String.format("%s:%s Service Start %n request: %s", Service, className, req)));
      resp.setHeaderData(req.getHeaderData());
      ServiceResults<SearchPolicyByCustomerIdResponse> data =
          searchPolicyByCustomerIdService.getResponse(req.getRequestRecord(), TRANSACTION_ID);
      if (!data.isSuccess()) {
        entityResponse =
            getErrorInternalServerResponse(
                resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
      } else {
        resp.setResponseRecord(data.getResult());
        if (data.getResponseCode().equals("204")) {
          entityResponse =
              getNotFoundDataResponseError(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
        } else {
          entityResponse =
              getSuccessResponse(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
        }
      }
    } catch (Exception e) {
      log.error(
          PolicyUtil.pattenLog(
              TRANSACTION_ID,
              String.format(
                  "%s:%s Service ERROR %n Exception: %s", Service, className, e.getMessage())));
      e.printStackTrace();
      entityResponse =
          getErrorInternalServerResponse(
              resp,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
    }
    log.info(
        PolicyUtil.pattenLog(
            TRANSACTION_ID,
            String.format(
                "%s:%s Service done %n Response: %s", Service, className, entityResponse)));
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(SearchPolicyByCustomerIdRequest req)
      throws Exception {
    if (req.getCustomer_id().length() > 100 || !PolicyUtil.validateInteger(req.getCustomer_id())) {
      log.debug(
          PolicyUtil.pattenLog(
              "",
              String.format("%s:%s | error validate Customer_id is invalid", Service, className)));
      return false;
    }
    return true;
  }
}
