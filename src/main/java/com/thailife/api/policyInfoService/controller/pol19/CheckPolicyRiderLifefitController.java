package com.thailife.api.policyInfoService.controller.pol19;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.controller.pol01.SearchPolicyByCustomerIdController;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.CheckPolicyRiderLifefitResponse;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.CheckPolicyRiderLifefitService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CheckPolicyRiderLifefitController
    extends WebServiceControllerResponseT<
        SearchPolicyByCustomerIdRequest, CheckPolicyRiderLifefitResponse> {
  private final ExtLogger log = ExtLogger.create(CheckPolicyRiderLifefitController.class);
  private final String className = "SearchPolicyOnlyByPartyIdController";
  private final String Service = "SearchPolicyListOnlyByCustomerIdImp";
  private String tranId;
  
  @Autowired CheckPolicyRiderLifefitService checkPolicyRiderLifefitService;

  @Override
  @PostMapping("checkPolicyRiderLifefit")
  public ResponseEntity<ResponseMessages<CheckPolicyRiderLifefitResponse>> service(
      @RequestBody RequestMessages<SearchPolicyByCustomerIdRequest> req) {
    ResponseEntity<ResponseMessages<CheckPolicyRiderLifefitResponse>> entityResponse;
    ResponseMessages<CheckPolicyRiderLifefitResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg();
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    try {
      if (!validateRequestMessages(req)) {
        log.error(Service, tranId, className, "Error Validate");
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      } else {
 
        ServiceResults<CheckPolicyRiderLifefitResponse> data =
            checkPolicyRiderLifefitService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
        } else {
          resp.setResponseRecord(data.getResult());
          if (data.getResponseCode().equals("204")) {
            entityResponse =
                getNotFoundDataResponse(
                    resp,
                    req.getHeaderData(),
                    data.getResponseCode(),
                    data.getResponseDescription());
          } else {
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    data.getResponseCode(),
                    data.getResponseDescription());
          }
        }
      }
      
    } catch (Exception ex) {
      log.error(Service, tranId, className, "error Exception "+ex.getMessage());
      return getErrorInternalServerResponse(
          resp,
          req != null ? req.getHeaderData() : new HeaderData(),
          wMessage.getMessageByCode("500")[0],
          wMessage.getMessageByCode("500")[1]);
    }
    log.error(Service, tranId, className, "Service Resp : " + entityResponse);
    log.error(Service, tranId, className, "Service Done...");
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(SearchPolicyByCustomerIdRequest req) {
    if (req == null){
      log.info(Service, tranId, className, "error validate requestRecord inValid");
      return false;
    }
    if (req.getCustomer_id() == null || !req.getCustomer_id().matches("^[0-9]+$") || req.getCustomer_id().length() > 19){
      log.info(Service, tranId, className, "error validate requestRecord inValid");
      return false;
    }
    return true;
  }
}
