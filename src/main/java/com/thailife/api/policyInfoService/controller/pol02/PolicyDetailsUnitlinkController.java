package com.thailife.api.policyInfoService.controller.pol02;

import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.PolicyDetailsUnitlinkResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PolicyDetailsUnitlinkService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;

@RestController
public class PolicyDetailsUnitlinkController
    extends WebServiceControllerResponseT<PolicyDetailRequest, PolicyDetailsUnitlinkResponse> {
  private final ExtLogger log = ExtLogger.create(PolicyDetailsUnitlinkController.class);
  private final String className = "PolicyDetailsUnitlinkController";
  private final String Service = "PolicyDetailsUnitLink";

  @Autowired PolicyDetailsUnitlinkService policyDetailsUnitlinkService;

  @Override
  @CrossOrigin
  @RequestMapping(value = "/PolicyDetailsUnitLink", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<PolicyDetailsUnitlinkResponse>> service(
      @RequestBody RequestMessages<PolicyDetailRequest> req) {
    ResponseEntity<ResponseMessages<PolicyDetailsUnitlinkResponse>> entityResponse;
    ResponseMessages<PolicyDetailsUnitlinkResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg() {};
    String tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    try {
      if (!validateRequestMessages(req)) {
        log.debug(
            PolicyUtil.pattenLog(
                tranId, String.format("%s:%s | Validate Error", Service, className)));
        entityResponse =
            getErrorValidateResponse(
                resp,
                req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      } else {
        log.info(
            PolicyUtil.pattenLog(
                tranId,
                String.format("%s:%s | Service start %n request: %s", Service, className, req)));
        ServiceResults<PolicyDetailsUnitlinkResponse> data =
            policyDetailsUnitlinkService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
        } else {
          resp.setResponseRecord(data.getResult());
          entityResponse =
              getSuccessResponse(
                  resp,
                  req.getHeaderData(),
                  wMessage.getMessageByCode("200")[0],
                  wMessage.getMessageByCode("200")[1]);
        }
      }
    } catch (Exception e) {
      entityResponse =
          getSuccessResponse(
              resp,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format("%s:%s | Error Exception: %s", Service, className, e.getMessage())));
    }
    log.info(
        PolicyUtil.pattenLog(
            tranId,
            String.format(
                "%s:%s | Service done %n Response: %s", Service, className, entityResponse)));

    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(PolicyDetailRequest req) throws Exception {

    if (PolicyUtil.StringIsNullOrEmpty(req.getPolicyno()) || req.getPolicyno().length() > 8) {
      log.debug(
          PolicyUtil.pattenLog(
              "", String.format("%s:%s | error validate policy invalid", Service, className)));
      return false;
    }

    if (PolicyUtil.StringIsNullOrEmpty(req.getType())
        || req.getType().length() > 30
        || !req.getType().contains("ULIP")) {
      log.debug(
          PolicyUtil.pattenLog(
              "", String.format("%s:%s | error validate type invalid", Service, className)));
      return false;
    }

    return true;
  }
}
