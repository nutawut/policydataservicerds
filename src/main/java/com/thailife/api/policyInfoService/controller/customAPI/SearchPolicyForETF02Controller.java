package com.thailife.api.policyInfoService.controller.customAPI;

import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SearchPolicyResponse;
import com.thailife.api.policyInfoService.service.SearchPolicyForETF02Service;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@Log4j2
@CrossOrigin
@RestController
public class SearchPolicyForETF02Controller {
  @Autowired private SearchPolicyForETF02Service searchPolicyForETF02Service;

  @PostMapping("GetListPolTypeAndSumBenefit")
  public ResponseEntity<?> searchPolicyForETF02Controller(
      @RequestBody RequestMessages<List<SearchPolicyResponse>> requestT) {
    log.info("<== [Start] GetListPolTypeAndSumBenefit Controller ==>");
    ResponseMessages<List<SearchPolicyResponse>> responseT = new ResponseMessages<>();
    try {
      if (requestT != null
          && requestT.getRequestRecord() != null
          && requestT.getHeaderData() != null) {
        responseT.setResponseRecord(
            searchPolicyForETF02Service.searchPolicyForETF02Service(
                requestT.getRequestRecord(), requestT.getHeaderData().getMessageId())); // service
        responseT.setResponseStatus(PolicyUtil.responseStatusSuccess());
      } else {
        throw new NullPointerException("Request Is empty");
      }
    } catch (Exception e) {
      log.error(String.format("Error => %s", e.getMessage()));
      e.printStackTrace();
      responseT.setResponseStatus(PolicyUtil.responseInternalServerError());
    } finally {
      if (requestT != null) {
        responseT.setHeaderData(PolicyUtil.headerDataResponse(requestT.getHeaderData()));
      }
      log.info("<== [End] GetListPolTypeAndSumBenefit Controller ==>");
    }
    return ResponseEntity.ok(responseT);
  }
}
