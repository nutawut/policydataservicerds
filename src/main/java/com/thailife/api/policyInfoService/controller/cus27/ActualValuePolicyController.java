package com.thailife.api.policyInfoService.controller.cus27;

import com.thailife.api.policyInfoService.model.request.ActualValuePolicyRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.ActualValuePolicyResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.service.impl.ActualValuePolicyService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActualValuePolicyController {
  @Autowired ActualValuePolicyService aVPService;
  private final Log log = new Log();
  private final String className = "ActualValuePolicyController";

  @PostMapping("actualValuePolicy")
  public ResponseEntity<ResponseMessages<ActualValuePolicyResponse>> controller(
      @RequestBody RequestMessages<ActualValuePolicyRequest> request) {
    log.printInfo(className, " Controller [START]");
    /* TODO. call service */
    ResponseMessages<ActualValuePolicyResponse> response = aVPService.service(request);
    log.printInfo(className, "Controller [END]");
    return ResponseEntity.status(PolicyUtil.getHttp(response.getResponseStatus())).body(response);
  }
}
