package com.thailife.api.policyInfoService.controller.pol22;

import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.UKMarkToMarketRequest;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.model.response.UKMarkToMarketResponse;
import com.thailife.api.policyInfoService.model.subModel.MarkToMarketM;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
import manit.rte.Result;
import org.json.JSONArray;
import org.json.JSONObject;
import utility.rteutility.PublicRte;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
public class UKMarkToMarketServiceImpl {
  private static final String APP_NAME = "UKMarkToMarket";

  public ServiceResults<UKMarkToMarketResponse> getResponse(
      RequestMessages<UKMarkToMarketRequest> req) throws Exception {
    String transactionId = req.getHeaderData().getMessageId();
    String method = "getResponse";
    String thirtParty = "";
    ServiceResults<UKMarkToMarketResponse> response = new ServiceResults<>();
    UKMarkToMarketResponse lsUKMarktoMarket = null;
    ResponseStatus responseStatus = new ResponseStatus();

    try {
      String[] param =
          new String[] {
            req.getRequestRecord().getPolicyNo(),
            req.getRequestRecord().getCalflag(),
            req.getRequestRecord().getChannel()
          };
      log.info(
          String.format(
              "%s "
                  + APP_NAME
                  + "|"
                  + method
                  + "|"
                  + "call rte.bl.unitlink.accountvalue.MarkToMarket request : %s",
              transactionId,
              Arrays.toString(param)));
      PublicRte.setRemote(true);
      Result rs =
          PublicRte.getResult("blunitlink", "rte.bl.unitlink.accountvalue.MarkToMarket", param);
      // call error
      if (rs == null) {
        log.error(
            String.format(
                "%s "
                    + APP_NAME
                    + "|"
                    + method
                    + "|"
                    + "error rte.bl.unitlink.accountvalue.MarkToMarket return null",
                transactionId));
        responseStatus = PolicyUtil.responseInternalServerError();
      } else {
        thirtParty = String.valueOf(rs.status()) + "|" + rs.value();
        log.info(
            String.format(
                "%s "
                    + APP_NAME
                    + "|"
                    + method
                    + "|"
                    + "call rte.bl.unitlink.accountvalue.MarkToMarket resp statut : %s  values : %s",
                transactionId,
                rs.status(),
                rs.value()));
        // call success
        if (rs.status() == 0) {
          UKMarkToMarketResponse result = new UKMarkToMarketResponse();
          List<MarkToMarketM> lsMarkToMarket = new ArrayList<>();
          String data = (String) rs.value();
          JSONObject dataObj = new JSONObject(data);
          JSONArray lsObj = dataObj.getJSONArray("investDetail");
          for (int i = 0; i < lsObj.length(); i++) {
            JSONObject obj = lsObj.getJSONObject(i);
            MarkToMarketM markToMarketM = new MarkToMarketM();
            markToMarketM.setFundCode(obj.getString("fundCode"));
            markToMarketM.setFundName(obj.getString("fundName"));
            markToMarketM.setUnitAmount(obj.getString("unitAmount"));
            markToMarketM.setUnitPrice(obj.getString("unitPrice"));
            markToMarketM.setUnrealizeValue(obj.getString("unrealizeValue"));
            markToMarketM.setGainLoss(obj.getString("gainLoss"));
            markToMarketM.setPercentGainLoss(obj.getString("percentGainLoss"));
            markToMarketM.setInvestValue(obj.getString("investValue"));
            markToMarketM.setAveragePrice(obj.getString("averagePrice"));

            lsMarkToMarket.add(markToMarketM);
          }
          if (lsMarkToMarket.isEmpty()) {
            responseStatus = PolicyUtil.responseDataNotFound();
            response.setSuccess(false);
            return response;
          }
          result.setSummaryInvestValue(dataObj.getString("summaryInvestValue"));
          result.setSummaryUnrealizeValue(dataObj.getString("summaryUnrealizeValue"));
          result.setSummaryPercentGainLoss(dataObj.getString("summaryPercentGainLoss"));
          result.setPolicyNo(dataObj.getString("policyNo"));
          result.setListMarkToMarket(lsMarkToMarket);
          response.setResult(result);
          responseStatus = PolicyUtil.responseStatusSuccess();
          response.setSuccess(true);
          // call not success
        } else {
          response.setSuccess(false);
          responseStatus = PolicyUtil.newResponseStatus("500", "E", rs.value().toString());
        }
      }
    } catch (Exception e) {
      throw e;
    } finally {
      response.setResponseCode(responseStatus.getErrorCode());
      response.setResponseDescription(responseStatus.getErrorMessage());
      response.setEtc1(thirtParty);
    }
    return response;
  }

}
