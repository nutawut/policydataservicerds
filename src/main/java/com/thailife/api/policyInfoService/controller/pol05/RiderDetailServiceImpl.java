package com.thailife.api.policyInfoService.controller.pol05;

import com.thailife.api.policyInfoService.dao.impl.CertDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.CertRiderDaoImpl;
import com.thailife.api.policyInfoService.model.CertRiderM;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.model.RiderDetailM;
import com.thailife.api.policyInfoService.model.RiderM;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
import com.thailife.api.policyInfoService.model.subModel.LifeDetailM;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.api.policyInfoService.util.StatusDisplay;
import com.thailife.api.policyInfoService.util.newRiderDetail.NewRiderDetailResponseController;
import com.thailife.log.ExtLogger;
import utility.support.StatusInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RiderDetailServiceImpl {
    private final ExtLogger log = ExtLogger.create(GetRiderDetialController.class);

    public ResponseMessages<RiderDetialResponse> service(RequestMessages<RiderDetailRequest> req) throws PolicyException {
        ResponseMessages<RiderDetialResponse> response = new ResponseMessages<>();
        RiderDetialResponse responseRecord = new RiderDetialResponse();
        RiderDetailRequest request = req.getRequestRecord();
        String policyno = request.getPolicyno();
        String certno = request.getCertno();
        String type = request.getType();
        try {
            String tableType = ProductType.typeGetTable(type);
            NewRiderDetailResponseController resController =
                    new NewRiderDetailResponseController(policyno, certno, tableType);

            ArrayList<RiderDetailM> riders = resController.getRiderData();
            System.out.println("Debug :: riders = " + riders.toString());
            PolicyDetailM policyDetail = resController.getPolicyDetail();
            System.out.println("Debug :: policyDetail = " + policyDetail.toString());
            ArrayList<RiderM> list;
            if (riders.isEmpty() && !type.contains("CL")) {
                throw new PolicyException("riders empty");
            } else {
                list = NewRiderDetailResponseController.setListRiderM(policyDetail, riders, type);
                if ("mortgage".equals(tableType)) {
                    List<LifeDetailM> lifeDetailMS = new ArrayList<>();
                    LifeDetailM lifeDetailCert =
                            CertDaoImpl.searchLifeDetailByPolicyNoAndCertNo(policyno, certno);
                    CertRiderM certRiderM =
                            CertRiderDaoImpl.searchByPolicynoAndCertnoAndRiderType106(policyno, certno);
                    if (certRiderM != null) {
                        LifeDetailM lifeDetailCertRider = new LifeDetailM();
                        lifeDetailCertRider.setRidersum(certRiderM.getSum());
                        lifeDetailCertRider.setRiderpremium(certRiderM.getPremium());
                        lifeDetailCertRider.setRiderstatus(certRiderM.getRiderstatus());
                        lifeDetailCertRider.setRiderstatusdate(certRiderM.getRiderstatusdate());

                        lifeDetailCertRider.setEffectivedate(certRiderM.getEffectivedate());
                        lifeDetailCertRider.setExtrariderpremium(certRiderM.getExtrapremium());
                        setDefaultLifeDetail(policyDetail, lifeDetailMS, lifeDetailCertRider);

                        lifeDetailCert.setRiderstatusdate(certRiderM.getRiderstatusdate());
                        lifeDetailCert.setEffectivedate(certRiderM.getRiderstatusdate());
                    }
                    setDefaultLifeDetail(policyDetail, lifeDetailMS, lifeDetailCert);
                    sortLifeDetailByRiderStatus(lifeDetailMS);
                    responseRecord.setLife_details(lifeDetailMS);
                }
            }
            responseRecord.setList_of_rider(list);
            response.setResponseRecord(responseRecord);
            response.setResponseStatus(PolicyUtil.responseStatusSuccess());
        } catch (Exception e) {
            e.printStackTrace();
            // log.error(e.getMessage());
            throw new PolicyException(
                    "getResponse|policyno:" + policyno + " certno:" + certno + " type:" + type);
        }
        return response;
    }

    private static void setDefaultLifeDetail(PolicyDetailM policyDetail, List<LifeDetailM> lifeDetailMS, LifeDetailM lifeDetailCertRider) {
        String[] s;
        lifeDetailCertRider.setRidercode("LIFE");
        lifeDetailCertRider.setRidername("ชีวิต");
        lifeDetailCertRider.setRidersortname("ชีวิต");
        s = riderStatusDescStatusDisplay(policyDetail, lifeDetailCertRider.getRiderstatus());
        lifeDetailCertRider.setRiderstatusdesc(s[0]);
        lifeDetailCertRider.setStatusdisplay(s[1]);
        lifeDetailMS.add(lifeDetailCertRider);
    }

    private static String[] riderStatusDescStatusDisplay(
            PolicyDetailM policyDetail, String riderStatus) {
        String policyStatus = policyStatus(policyDetail);
        String riderStatusDesc, statusDisplay;
        riderStatusDesc =
                ("ไม่มีผลบังคับ").equals(policyStatus)
                        ? "ไม่มีผลบังคับ"
                        : StatusInfo.riderStatus(riderStatus);
        statusDisplay =
                ("ไม่มีผลบังคับ").equals(policyStatus)
                        ? "ไม่มีผลบังคับ"
                        : new StatusDisplay().getStatusDisplay("RIDER", riderStatus, "");
        return new String[] {riderStatusDesc, statusDisplay};
    }

    public static String policyStatus(PolicyDetailM policy) {
        String status =
                PolicyUtil.getStatusForShowCustomer(
                        policy.getPolicystatus1(),
                        "",
                        policy.getPolicystatus2(),
                        "",
                        policy.getOldpolicystatus1(),
                        "",
                        policy.getOldpolicystatus2(),
                        "")[0];
        return new StatusDisplay().statusDisplayCL(status);
    }

    private static void sortLifeDetailByRiderStatus(List<LifeDetailM> lifeDetailMS){
        Collections.sort(lifeDetailMS, new Comparator<LifeDetailM>() {
            @Override
            public int compare(LifeDetailM life1, LifeDetailM life2) {
                return life1.getRiderstatus().compareTo(life2.getRiderstatus());
            }
        });
//    list.sort(Comparator.comparing(RiderM::getRidercode));
    }
}
