package com.thailife.api.policyInfoService.controller.pol05;

import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.RiderDetailRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.RiderDetialResponse;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.ProductType;
import com.thailife.log.ErrorLog;
import com.thailife.log.ExtLogger;
import com.thailife.log.RequestLog;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

// @Log4j
@RestController
public class GetRiderDetialController {
  private final ExtLogger log = ExtLogger.create(GetRiderDetialController.class);
  private final String className = "GetRiderDetialController";
  private final String Service = "GetRiderDetial";
  private String tranId;

//  @Autowired GetRiderDetialService getRiderDetialService;
  private final RiderDetailServiceImpl riderDetialService = new RiderDetailServiceImpl();


  @PostMapping("/GetRiderDetial")
  @CrossOrigin
  public ResponseEntity<?> controller(@RequestBody RequestMessages<RiderDetailRequest> req)
      throws PolicyException {

    String policyno = req.getRequestRecord().getPolicyno();
    String certno = req.getRequestRecord().getCertno();
    String type = req.getRequestRecord().getType();
    String transactionId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    log.access(new RequestLog("GetRiderDetial", transactionId, "", "GetRiderDetial", "policyno:" + policyno + " certno:" + certno + " type:" + type, " [Start]"));
    ResponseMessages<RiderDetialResponse> response = new ResponseMessages<>(null);
    response.setHeaderData(PolicyUtil.headerDataResponse(req.getHeaderData()));
    if (!validate(req)) {
      response.setResponseStatus(PolicyUtil.responseValidateError());
      return ResponseEntity.status(400).body(response);
    }
    try {
      ResponseMessages<RiderDetialResponse> resp = riderDetialService.service(req);
      response.setResponseRecord(resp.getResponseRecord());
      response.setResponseStatus(PolicyUtil.responseStatusSuccess());
    } catch (Exception e) {
      e.printStackTrace();
      response.setResponseStatus(PolicyUtil.responseInternalServerError());
      log.fail(new ErrorLog("GetRiderDetial", transactionId, "", "GetRiderDetial", "", "Exception", e));
    }
    log.detail(new RequestLog("GetRiderDetial", transactionId, "", "GetRiderDetial", "", " GetRiderDetial done."));
    return ResponseEntity.status(Integer.parseInt(response.getResponseStatus().getErrorCode())).body(response);
  }

  private boolean validate(RequestMessages<RiderDetailRequest> req) {
    if (!PolicyUtil.validateHeaderData(req.getHeaderData()))
      return false;
    if (req.getHeaderData() == null || req.getRequestRecord() == null)
      return false;
    if (req.getRequestRecord().getPolicyno() == null || req.getRequestRecord().getPolicyno().equals("")
            || req.getRequestRecord().getPolicyno().length() > 8)
      return false;
    if ((req.getRequestRecord().getCertno() == null || req.getRequestRecord().getCertno().equals(""))
            && req.getRequestRecord().getPolicyno().length() < 8)
      return false;
    if (req.getRequestRecord().getType() == null || req.getRequestRecord().getType().equals(""))
      return false;
    return ProductType.typeGetTable(req.getRequestRecord().getType()) != null;
  }
}
