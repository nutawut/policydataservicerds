package com.thailife.api.policyInfoService.controller.pol04;

import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.thailife.api.policyInfoService.model.request.GetLoanPremumRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetLoanPremumResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.service.GetLoanPremiumService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;

@CrossOrigin
@RestController
public class GetLoanPremiumController {
  private static final ExtLogger logger = ExtLogger.create(GetLoanPremiumController.class);

  @Autowired GetLoanPremiumService getLoanPremiumService;

  @PostMapping("/GetLoanPremium")
  public ResponseEntity<?> controller(@RequestBody RequestMessages<GetLoanPremumRequest> request) {
    String className = "GetLoanPremiumController";
    logger.info(className, "GetLoanPremiumController [START]");
    ResponseMessages<GetLoanPremumResponse> response = getLoanPremiumService.service(request);
    logger.info(className, "GetLoanPremiumController [END]");
    return ResponseEntity.status(PolicyUtil.getHttp(response.getResponseStatus())).body(response);
  }
}
