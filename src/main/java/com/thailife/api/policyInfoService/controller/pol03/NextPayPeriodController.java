package com.thailife.api.policyInfoService.controller.pol03;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.dao.*;
import com.thailife.api.policyInfoService.dao.impl.*;
import com.thailife.api.policyInfoService.model.LifeFitDiscountM;
import com.thailife.api.policyInfoService.model.PersonM;
import com.thailife.api.policyInfoService.model.request.*;
import com.thailife.api.policyInfoService.model.response.NextPayPeriodListResponse;
import com.thailife.api.policyInfoService.model.response.NextPayPeriodResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.service.NextPayPeriodService;
import com.thailife.api.policyInfoService.util.*;
import com.thailife.log.ExtLogger;
import insure.Insure;
import insure.PlanSpec;
import insure.PlanType;
import insure.TLPlan;
import manit.M;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utility.grouplife.mortgage.MortgageService;
import utility.personnel.support.ZArray;
import utility.support.DateInfo;
import utility.support.StatusInfo;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RestController
public class NextPayPeriodController
    extends WebServiceControllerResponseT<NextPayPeriodListRequest, NextPayPeriodListResponse> {
  private static final String className = "NextPayPeriodController";
  private static final ExtLogger log = ExtLogger.create(NextPayPeriodController.class);
  @Autowired NextPayPeriodService nextPayPeriodService;
  private static final DecimalFormat df2 = new DecimalFormat("#.##");

  @Autowired CertDaoImpl certRepository;
  @Autowired CertRiderDaoImpl certriderRepository;
  @Autowired SubunitlinkDaoImpl subunitlinkRepository;
  @Autowired OrdmastDaoImpl ordmastRepository;
  @Autowired IndmastDaoImpl indmastRepository;
  @Autowired WhlmastDaoImpl whlmastRepository;
  @Autowired NameDaoImpl nameRepository;
  @Autowired PersonDaoImpl personRepository;
  @Autowired RiderDaoImpl riderRepository;
  @Autowired IndriderDaoImpl indriderRepository;
  @Autowired UniversallifeDaoImpl universallifeRepository;
  @Autowired UnitLinkDaoImpl unitlinkRepository;

  private String transactionId;
  private String serviceName = "NextPayPeriod";

  @RequestMapping(value = "/NextPayPeriod", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<NextPayPeriodListResponse>> service(
      @RequestBody RequestMessages<NextPayPeriodListRequest> req) {

    List<NextPayPeriodRequest> NextPayPeriod;
    // initial respones obj
    NextPayPeriodResponse nppResp;
    List<NextPayPeriodResponse> listNppResp = new ArrayList<>();
    NextPayPeriodListResponse NextPayPeriodList = new NextPayPeriodListResponse();
    NextPayPeriodList.setNextPayPeriodList(listNppResp);
    ResponseMessages<NextPayPeriodListResponse> response =
        new ResponseMessages<>(NextPayPeriodList);
    transactionId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    try {
      // validate request data
      if (!validateRequestMessages(req)) {
        response.setHeaderData(req.getHeaderData());
        response.setResponseStatus(PolicyUtil.responseValidateError());
        return ResponseEntity.status(400).body(response);
      }
      NextPayPeriod = req.getRequestRecord().getNextPayPeriod();
      StringBuilder data = new StringBuilder();
      for (NextPayPeriodRequest payPeriod : NextPayPeriod) {
        String policyno = payPeriod.getPolicyno();
        String certno = payPeriod.getCertno();
        String type = payPeriod.getType();
        data.append("policyno:")
            .append(policyno)
            .append(" certno:")
            .append(certno)
            .append(" type:")
            .append(type)
            .append(",");
      }
      log.info(className, "transaction: " + transactionId + "START data: " + data);

      // set Header ResponseDateTime
      req.getHeaderData().setResponseDateTime(PolicyUtil.now());
      response.setHeaderData(req.getHeaderData());

      String context = "/interfaceCsv/rest/calculate/policyPremium/1.0";
      String request;
      String policyNo;
      String type;
      String certNo;
      for (int i = 0; i < req.getRequestRecord().getNextPayPeriod().size(); i++) {

        // prepare data input
        nppResp = new NextPayPeriodResponse();
        policyNo = req.getRequestRecord().getNextPayPeriod().get(i).getPolicyno();
        type = req.getRequestRecord().getNextPayPeriod().get(i).getType();

        certNo = req.getRequestRecord().getNextPayPeriod().get(i).getCertno();

        // type CL return list.size = 0
        if (type.toUpperCase().contains("-CL-") || type.toUpperCase().contains("-CL_TAKAFUL-")) {
          System.out.println("-CL- : " + PlanType.isConsumerFinanceMRTAPlan(policyNo));
          if (!PlanType.isConsumerFinanceMRTAPlan(policyNo)) {
            nppResp.setExtralifepremium("0");
            nppResp.setLifefit_lifepremium_discount(0);
            nppResp.setLifefit_riderpremium_discount(0);
            nppResp.setLifepremium(new BigDecimal(0));
            nppResp.setRiderpremium(new BigDecimal(0));
            nppResp.setInterestrate("0");
            nppResp.setFlagPayment("");
            nppResp.setLifefitflag("");
            nppResp.setType(type);
            nppResp.setPaydesc("");
            nppResp.setOverdueflag("");
            nppResp.setExtralifepremium("0");
            nppResp.setExtrariderpremium("0");
            nppResp.setMode("");
            nppResp.setModedesc("");
            nppResp.setRsppremium("0");
            nppResp.setTppremium("");

            nppResp.setPremiumflag("N");
            nppResp.setTotalpremium(new BigDecimal(0));
            nppResp.setPayperiod("00/00");
            nppResp.setNextduedate("00000000");
            nppResp.setPlancode("");
            nppResp.setPlanname("");
            listNppResp.add(nppResp);
            continue;
          } else {
            nppResp = getNppRespCF(policyNo, certNo, nppResp, type);

            nppResp.setLifepremium(
                null == nppResp.getLifepremium() || nppResp.getLifepremium().equals("")
                    ? new BigDecimal(0)
                    : nppResp.getLifepremium());
            nppResp.setTotalpremium(
                null == nppResp.getTotalpremium() || nppResp.getTotalpremium().equals("")
                    ? new BigDecimal(0)
                    : nppResp.getTotalpremium());
            nppResp.setRiderpremium(
                null == nppResp.getRiderpremium() || nppResp.getRiderpremium().equals("")
                    ? new BigDecimal(0)
                    : nppResp.getRiderpremium());
            listNppResp.add(nppResp);
            continue;
          }
        }

        // set obj input
        request = getRequest(policyNo);

        // call POL03
        String resp = CallWithToken.CSVApplication(context, request);

        // set data output
        JSONObject json = new JSONObject(resp);
        JSONObject responseRecord = json.getJSONObject("responseRecord");
        System.out.println("responseRecord==" + responseRecord);

        // if data response = null skip
        if (responseRecord.isNull("nextDueDate")) {
          continue;
        }

        String nextPayPeriod = responseRecord.getString("nextPayPeriod");
        System.out.println("nextPayPeriod : " + nextPayPeriod);
        if (null != nextPayPeriod) {
          if (("0000").equals(nextPayPeriod)) {
            nppResp.setPayperiod("00/00");
          } else {
            String[] arrNextPayPeriod = nextPayPeriod.split("/");
            if (arrNextPayPeriod.length == 2) {
              nppResp.setPayperiod(
                  arrNextPayPeriod[1].length() > 2
                      ? arrNextPayPeriod[1] + "/" + arrNextPayPeriod[0]
                      : nextPayPeriod);
            } else {
              nppResp.setPayperiod(nextPayPeriod);
            }
          }
        }
        String nextduedate =
            responseRecord.getString("nextDueDate") == null
                    || responseRecord.getString("nextDueDate").equals("")
                ? ""
                : DateInfo.unformatDate(responseRecord.getString("nextDueDate"));
        nppResp.setNextduedate(nextduedate);
        Double lifePremium = responseRecord.getDouble("lifePremium");
        Double riderPremium = responseRecord.getDouble("riderPremium");
        BigDecimal lifePremiumRes = new BigDecimal(df2.format(lifePremium));
        BigDecimal riderPremiumRes = new BigDecimal(df2.format(riderPremium));
        nppResp.setLifepremium(lifePremiumRes);
        nppResp.setRiderpremium(riderPremiumRes);
        nppResp.setInterestrate(String.format("%.1f", responseRecord.getDouble("interestRate")));
        nppResp.setLifefitflag("");
        nppResp.setType(type);
        nppResp.setTotalpremium(new BigDecimal(responseRecord.getInt("premium")));

        nppResp.setExtralifepremium(responseRecord.getString("extraLifePremium"));
        nppResp.setExtrariderpremium(responseRecord.getString("extraRiderPremium"));
        nppResp.setRsppremium(responseRecord.getString("premiumRsp"));
        nppResp.setTppremium((responseRecord.getInt("topupPremiun")) + "");

        nppResp.setMode(responseRecord.getString("mode"));
        nppResp.setModedesc(responseRecord.getString("modeDesc"));
        nppResp.setLifefit_lifepremium_discount(
            responseRecord.getInt("lifefit_lifepremium_discount"));
        nppResp.setLifefit_riderpremium_discount(
            responseRecord.getInt("lifefit_riderpremium_discount"));
        nppResp.setLifefitflag(responseRecord.getString("lifefitflag"));

        List<LifeFitDiscountM> list = new ArrayList<>();
        JSONArray arr = responseRecord.getJSONArray("lifefitdiscountlist");
        for (int j = 0; j < arr.length(); j++) {
          LifeFitDiscountM lifefitdiscountM = new LifeFitDiscountM();
          lifefitdiscountM.setRider_code(arr.getJSONObject(j).getString("rider_code"));
          lifefitdiscountM.setPremium(arr.getJSONObject(j).getInt("premium"));
          lifefitdiscountM.setDiscount(arr.getJSONObject(j).getInt("discount"));
          list.add(lifefitdiscountM);
        }
        nppResp.setLifefitdiscountlist(list);
        nppResp.setFlagPayment(getFlagPayment(policyNo, certNo, type, nppResp.getNextduedate()));

        nppResp.setPaydesc(checkBalance(nppResp.getNextduedate()));
        nppResp.setPremiumflag(checkPremiumflag(nppResp.getNextduedate()));
        nppResp.setOverdueflag(checkOverdueflag(nppResp.getNextduedate()));

        // add field 20210303
        setPlanDetail(policyNo, certNo, type, nppResp);
        // เพิ่ม paydesc RP ใน UL (universallife) ->> (select policystatus1 from
        // universal.universallife as u) = H
        // paydesc = H //UL//22656889
        // RPP ใน UK (Unit Link) ->> (select policystatus1 from unitlink.unitlink) = H
        // paydesc = H //ULIP//22916986
        String typeChk = "";
        String[] typeList = type.toUpperCase().split("-");
        if (typeList.length > 1) {
          typeChk = typeList[1].toUpperCase();
        }
        String paydesc = getPaydesc(typeChk, policyNo);
        if (!paydesc.equals("")) {
          nppResp.setPaydesc(paydesc);
        }
        listNppResp.add(nppResp);
      }

      // if list empty return 204
      if (listNppResp.size() == 0) {
        response.setResponseStatus(PolicyUtil.responseDataNotFound());
        return ResponseEntity.status(200).body(response);
      } else {
        response.setResponseStatus(PolicyUtil.responseStatusSuccess());
      }
      // set ResponseStatus success
    } catch (PolicyException e) {
      e.printStackTrace();
      log.error(className, e.getMessage());
      // set ResponseStatus error
      response.setResponseStatus(PolicyUtil.responseDataNotFound());
      return ResponseEntity.status(200).body(response);
    } catch (Exception e) {
      e.printStackTrace();
      log.error(className, e.getMessage());
      // set ResponseStatus error
      response.setResponseStatus(PolicyUtil.responseInternalServerError());
      return ResponseEntity.status(500).body(response);
    } finally {
      PolicyUtil.setResponseTime(response.getHeaderData());
    }
    log.info(className, "NextPayPeriod done.");
    return ResponseEntity.ok(response);
  }

  private String getFlagPayment(String policyNo, String certNo, String type, String nextduedate)
      throws Exception {
    // - policyStatus1 != I,F || policyStatus2 = D,G,X,P,S -- return F ไม่รับชำระ
    // - ช็คจากระบบขออนุมัติงานเวนคืน ว่าไม่มีสถานะการอนุมัติเป็น “ระหว่างดำเนินการ”
    // ทำ API เพิ่ม มีสถานะ 2 หรือป่าว
    // เช็คจากระบบขออนุมัติงานปิดบัญชี ว่าไม่มีสถานะการอนุมัติเป็น
    // “ระหว่างดำเนินการ” ทำ API เพิ่ม --> ถัง POS -> svauthorizedtrans@sruservice
    // (authorizedStatus = R,W,Y) และ ServiceType = SR, UE --> return F ไม่รับชำระ
    // - เช็คงวดเบี้ยล่าสุดที่ถูกชำระแล้วตาม Master ถ้าชำระด้วยเช็ค สถานะเช็คต้อง
    // “ผ่าน” --> wait p'aoi
    System.out.println(
        "getFlagPayment : " + policyNo + "|" + certNo + "|" + type + "|" + nextduedate + "|");

    JSONObject jsonPolicy = getPolicy(policyNo, certNo, type);
    if (jsonPolicy != null) {
      String policyStatus1 =
          jsonPolicy.has("policystatus1")
              ? jsonPolicy.getString("policystatus1")
              : jsonPolicy.getString("statcer");
      String policyStatus2 =
          jsonPolicy.has("policystatus2")
              ? jsonPolicy.getString("policystatus2")
              : jsonPolicy.getString("statcer2");

      String opolicystatus1 =
          jsonPolicy.has("oldpolicystatus1")
              ? jsonPolicy.getString("oldpolicystatus1")
              : jsonPolicy.getString("oldstatcer1");
      String opolicystatus2 =
          jsonPolicy.has("policystatus2")
              ? jsonPolicy.getString("policystatus2")
              : jsonPolicy.getString("oldstatcer2");

      String[] status =
          PolicyUtil.getStatusForShowCustomer(
              policyStatus1, "", policyStatus2, "", opolicystatus1, "", opolicystatus2, "");
      policyStatus1 = status[0];
      //      policyStatus2 = status[2];
      if (policyStatus1.equals("F") && nextduedate != null && !nextduedate.equals("00000000")) {
        if (M.cmps(DateInfo.sysDate(), M.nextdate(nextduedate, 31)) > 0) return "F";
      }
      return "T";
    } else return "";
  }

  private NextPayPeriodResponse getNppRespCF(
      String policyNo, String certNo, NextPayPeriodResponse nppResp, String type) throws Exception {
    if (nppResp == null) nppResp = new NextPayPeriodResponse();

    CertDao cert = certRepository.getMapPolicyDetail(policyNo, certNo);
    if (cert != null) {
      List<CertRiderDao> certriders =
          certriderRepository.findByIdPolicynoAndIdCertno(policyNo, certNo);
      TLPlan ps = PlanSpec.getPlan(cert.getPolicyno());
      System.out.println("interestRate : " + ps.interestRate());

      if (cert.getMode().trim().equals("")) cert.setMode("9");

      nppResp.setMode(cert.getMode());
      nppResp.setModedesc(StatusInfo.modeStatus(cert.getMode()));
      nppResp.setType(type);
      // check format payPeriod if not "/" it will substring add "/"
      nppResp.setPayperiod(
          PolicyUtil.checkPayPeriodFormat(
              Insure.nextPayPeriod(cert.getPayperiod(), cert.getMode()), 2));

      nppResp.setNextduedate(cert.getDuedate());

      nppResp.setInterestrate(ps.interestRate());
      nppResp.setPaydesc(checkBalance(nppResp.getNextduedate()));
      nppResp.setPremiumflag(checkPremiumflag(nppResp.getNextduedate()));
      nppResp.setOverdueflag(checkOverdueflag(nppResp.getNextduedate()));

      nppResp.setExtralifepremium(cert.getExtrapremium().toString());
      nppResp.setExtrariderpremium("0");
      nppResp.setRsppremium("0");
      nppResp.setTppremium("0");
      nppResp.setLifefitflag("");
      nppResp.setFlagPayment("T");

      // add field 20210303
      nppResp.setPlancode(cert.getPolicyno());
      nppResp.setPlanname(ps.name());

      setCFPremiumNextPayPeriodResponse(nppResp, cert);
      setRiderPremium(certriders, nppResp);
    }

    return nppResp;
  }

  private void setRiderPremium(List<CertRiderDao> certriders, NextPayPeriodResponse nppResp) {
    if (nppResp == null) nppResp = new NextPayPeriodResponse();

    BigDecimal riderPremium = new BigDecimal(0);
    BigDecimal extraRiderPremium = new BigDecimal(0);
    if (certriders != null) {
      for (CertRiderDao certrider : certriders) {
        if (!certrider.getRiderstatus().equals("N")) continue;
        riderPremium = riderPremium.add(certrider.getPremium());
        extraRiderPremium = extraRiderPremium.add(certrider.getExtrapremium());
      }
    }
    nppResp.setRiderpremium(riderPremium);
    nppResp.setExtrariderpremium(extraRiderPremium.toString());
  }

  private void setCFPremiumNextPayPeriodResponse(NextPayPeriodResponse nppResp, CertDao cert) {

    List<PersonM> lPersonM = personRepository.selectByNameId(cert.getNameid());
    if (lPersonM != null && lPersonM.size() > 0) {
      PersonM personM = lPersonM.get(0);

      String mode = cert.getMode().trim().length() == 0 ? "1" : cert.getMode();
      String age =
          getAgeByPayPeriod(
              String.valueOf(cert.getAge()), Insure.nextPayPeriod(cert.getPayperiod(), mode));
      String sexCode = personM.getSex();
      String packageForm = mappingPackage(cert.getPackAge());
      packageForm = packageForm.trim().length() == 0 ? "0" : packageForm;
      String familyType = cert.getFamilytype().trim().length() == 0 ? "1" : cert.getFamilytype();
      String[] cfVal = null;
      try {
        System.out.println(
            "prepare cfVal : "
                + cert.getPolicyno()
                + "|"
                + mode
                + "|"
                + age
                + "|"
                + sexCode
                + "|"
                + packageForm
                + "|"
                + familyType
                + "|");
        cfVal =
            MortgageService.getAllConsumerFinance(
                cert.getPolicyno(), mode, age, sexCode, packageForm, familyType, cert.getCertno());
        System.out.println("cfVal ==================== ");
        ZArray.show(cfVal);

        if (cfVal != null && cfVal.length > 0) {
          String totalpremium = cfVal[0];
          if (cfVal.length > 1) {
            nppResp.setLifepremium(new BigDecimal(cfVal[1]));
          } else {
            totalpremium = M.endot(totalpremium, 2);
            nppResp.setLifepremium(cert.getLifepremium());
          }
          nppResp.setTotalpremium(new BigDecimal(totalpremium));

          if (nppResp.getRiderpremium() == null) nppResp.setRiderpremium(new BigDecimal(0));
          String riderPremium = "0";
          if (cfVal != null && cfVal.length > 2) riderPremium = M.addnum(cfVal[2], riderPremium);
          if (cfVal != null && cfVal.length > 3) riderPremium = M.addnum(cfVal[3], riderPremium);
          if (cfVal != null && cfVal.length > 4) riderPremium = M.addnum(cfVal[4], riderPremium);
          if (cfVal != null && cfVal.length > 5) riderPremium = M.addnum(cfVal[5], riderPremium);
          if (cfVal != null && cfVal.length > 6) riderPremium = M.addnum(cfVal[6], riderPremium);
          if (cfVal != null && cfVal.length > 13) riderPremium = M.addnum(cfVal[13], riderPremium);
          if (cfVal != null && cfVal.length > 14) riderPremium = M.addnum(cfVal[14], riderPremium);
          if (cfVal != null && cfVal.length > 15) riderPremium = M.addnum(cfVal[15], riderPremium);
          if (cfVal != null && cfVal.length > 19) riderPremium = M.addnum(cfVal[19], riderPremium);
          if (cfVal != null && cfVal.length > 20) riderPremium = M.addnum(cfVal[20], riderPremium);
          nppResp.setRiderpremium(new BigDecimal(riderPremium).add(nppResp.getRiderpremium()));
        } else {
          nppResp.setLifepremium(new BigDecimal(0));
          nppResp.setTotalpremium(new BigDecimal(0));
          nppResp.setRiderpremium(new BigDecimal(0));
        }

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private String mappingPackage(String packageNo) {
    if (M.numeric(packageNo)) return packageNo;

    if (packageNo.trim().length() == 0) return "";

    char c = packageNo.charAt(0);

    switch (c) {
      case 'A':
        return "10";
      case 'B':
        return "11";
      case 'C':
        return "12";
      case 'D':
        return "13";
      case 'E':
        return "14";
      case 'F':
        return "15";
      case 'G':
        return "16";
      case 'H':
        return "17";
      case 'I':
        return "18";
      case 'J':
        return "19";
      case 'K':
        return "20";
      default:
        return "";
    }
  }

  private String getAgeByPayPeriod(String ageAtIssued, String payPeriod) {
    if (payPeriod.length() != 4) {
      if (payPeriod.length() == 5 && payPeriod.charAt(2) != '/') return ageAtIssued;
    }
    String payAtYear = M.subnum(payPeriod.substring(0, 2), "1"); // ทดลง สำหรับปีที่ 1
    return M.addnum(ageAtIssued, payAtYear);
  }

  private String getPaydesc(String typeChk, String policyNo) throws Exception {
    String paydesc = "";
    if (typeChk.equals("UL")) {
      paydesc = nextPayPeriodService.getPaydescUL(policyNo);
      if (paydesc.equals("H") || isSpecialStatusForPaydesc(paydesc)) {
        return paydesc;
      }
    } else if (typeChk.equals("ULIP")) {
      paydesc = nextPayPeriodService.getPaydescULIP(policyNo);
      if (paydesc.equals("H") || isSpecialStatusForPaydesc(paydesc)) {
        return paydesc;
      }
    } else if (typeChk.equals("OL")) {
      paydesc = nextPayPeriodService.getPaydescOL(policyNo);
      if (isSpecialStatusForPaydesc(paydesc)) {
        return paydesc;
      }
    } else if (typeChk.equals("IND")) {
      paydesc = nextPayPeriodService.getPaydescIND(policyNo);
      if (isSpecialStatusForPaydesc(paydesc)) {
        return paydesc;
      }
    } else if (typeChk.equals("WHL")) {
      paydesc = nextPayPeriodService.getPaydescWHL(policyNo);
      if (isSpecialStatusForPaydesc(paydesc)) {
        return paydesc;
      }
    }
    return paydesc;
  }

  private boolean isSpecialStatusForPaydesc(String status) {
    if (status.equals("A")) return true;
    if (status.equals("E")) return true;
    if (status.equals("R")) return true;
    if (status.equals("U")) return true;
    return status.equals("J");
  }

  protected boolean validateRequestRecordData(NextPayPeriodListRequest req) throws Exception {
    // check data request each index
    NextPayPeriodRequest next = null;
    for (int i = 0; i < req.getNextPayPeriod().size(); i++) {
      // check Policy_no
      next = req.getNextPayPeriod().get(i);

      //			System.out.println("next : " +i +"|"+next+"|");
      if (next == null || next.getPolicyno() == null || next.getPolicyno().trim().equals("")) {
        log.info(serviceName, transactionId, className, "error validate policyNo is invalid");
        return false;
      } else if (PolicyUtil.getPolicyTypeByType(next.getType()) == null) {
        log.info(serviceName, transactionId, className, "error validate policyType is invalid");
        return false;
      }
      if ((next.getType().toUpperCase().contains("-CL-")
          || next.getType().toUpperCase().contains("-CL_TAKAFUL-"))) {
        if (next.getCertno() == null || next.getCertno().trim().length() == 0){
          log.info(serviceName, transactionId, className, "error validate certNo is invalid");
          return false;
        }
      }
    }
    return true;
  }

  private String checkBalance(String nextduedate) throws Exception {
    String outstandingBalance = "มียอดค้างชำระ";
    String nonOutstandingBalance = "ไม่มียอดค้างชำระ";
    if (nextduedate.trim().equals("00/00/0000")
        || nextduedate.trim().equals("00000000")) // จ่ายครบแล้ว
    return outstandingBalance;
    return nonOutstandingBalance;
  }

  private String checkPremiumflag(String nextduedate) throws Exception {
    if (nextduedate.trim().equals("00/00/0000")
        || nextduedate.trim().equals("00000000")) // จ่ายครบแล้ว
    return "N";
    return "Y";
  }

  private String checkOverdueflag(String nextduedate) throws Exception {
    if (checkHaveNextduedate(nextduedate)) return "Y";
    return "N";
  }

  private boolean checkHaveNextduedate(String nextduedate) throws Exception {
    System.out.println("checkHaveNextduedate : " + nextduedate);
    Date dueDate = null;
    if (nextduedate.trim().length() == 8)
      dueDate = new SimpleDateFormat("yyyyMMdd").parse(nextduedate);
    else dueDate = new SimpleDateFormat("dd/MM/yyyy").parse(nextduedate);

    Calendar cal = Calendar.getInstance();
    int year = cal.get(Calendar.YEAR);
    cal.set(Calendar.YEAR, year + 543);
    Date sysDate = cal.getTime();
    cal.setTime(dueDate);
    cal.add(Calendar.DATE, 7);
    Date overDueDate = cal.getTime();

    System.out.println("dueDate : " + dueDate);
    System.out.println("sysDate : " + sysDate);
    System.out.println("overDueDate : " + overDueDate);

    // if (nextduedate.trim().equals("00/00/0000")) {// จ่ายครบแล้ว
    // return false;
    // } /*
    // * else if(dueDate.compareTo(sysDate) > 0) {//ยังไม่ถึงกำหนดจ่าย return
    // false;}
    // */
    // else
    // ถึงวัน หรือเลยวัน กำหนดจ่าย
    if (overDueDate.compareTo(sysDate) <= 0) { // เลยวันกำหนดจ่ายเกิน 7 วัน
      return true;
    } else return dueDate.compareTo(sysDate) <= 0;
  }

  private String getRequest(String policyNo) {
    String request =
        "{"
            + "\"headerData\": {"
            + "},"
            + " \"requestRecord\": { "
            + " \"policyNo\": \""
            + policyNo
            + "\"}"
            + "} ";
    return request;
  }

  private JSONObject getPolicy(String policyno, String certno, String type) throws Exception {
    JSONObject json = getPolicyData(policyno, type);
    if (json == null) json = getPolicyData(policyno);
    if (json == null) json = getPolicyDetail(policyno, certno);
    return json;
  }

  private JSONObject getPolicyData(String policyNo, String type) throws Exception {
    String table = ProductType.typeGetTable(type);
    table = table == null ? "" : table;
    log.debug(className, "table==" + table);
    switch (table) {
      case "ordmast":
        OrdmastDao ordmast = ordmastRepository.findByPolicyno(policyNo);
        if (ordmast != null) {
          log.debug(className, DateFormat.nowddmmyyyy() + " policydata ordmast");
          return new JSONObject(ordmast);
        }
        break;

      case "indmast":
        IndmastDao indmast = indmastRepository.findByPolicyno(policyNo);
        if (indmast != null) {
          log.debug(className, DateFormat.nowddmmyyyy() + " policydata indmast");
          JSONObject json = new JSONObject(indmast);
          json.put("mode", "0");
          return json;
        }
        break;

      case "whlmast":
        WhlmastDao whlmast = whlmastRepository.findByPolicyno(policyNo);
        if (whlmast != null) {
          log.debug(className, DateFormat.nowddmmyyyy() + " policydata whlmast");
          return new JSONObject(whlmast);
        }
        break;

      case "universallife":
        UniversallifeDao universallife = universallifeRepository.findByPolicyno(policyNo);
        if (universallife != null) {
          log.debug(className, DateFormat.nowddmmyyyy() + " policydata universallife");
          return new JSONObject(universallife);
        }
        break;
    }
    return null;
  }

  private JSONObject getPolicyData(String policyNo) throws Exception {
    UnitLinkDao unitlink = unitlinkRepository.findByPolicyno(policyNo);
    if (unitlink != null) {
      log.debug(className, DateFormat.nowddmmyyyy() + " policydata unitlink");
      return new JSONObject(unitlink);
    }
    return null;
  }

  private JSONObject getPolicyDetail(String policyNo, String certNo) throws Exception {
    CertDao cert = certRepository.findByPolicynoAndCertno(policyNo, certNo);
    if (cert != null) return new JSONObject(cert);
    return null;
  }

  private void setPlanDetail(
      String policyNo, String certNo, String type, NextPayPeriodResponse nppRes) {
    String keyPlanCode = "plancode";
    try {
      JSONObject policy = getPolicy(policyNo, certNo, type);
      if (policy != null && policy.has(keyPlanCode)) {
        log.debug(className, "planCode => " + policy.getString(keyPlanCode));
        TLPlan plan = PlanSpec.getPlan(policy.getString(keyPlanCode));
        if (plan != null) {
          nppRes.setPlancode(policy.getString(keyPlanCode));
          nppRes.setPlanname(plan.name());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.info(className, String.format("Error setPlanDetail : %s", e.getMessage()));
    }
  }
}
