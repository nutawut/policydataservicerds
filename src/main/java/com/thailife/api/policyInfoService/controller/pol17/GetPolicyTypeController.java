package com.thailife.api.policyInfoService.controller.pol17;

import com.thailife.api.policyInfoService.controller.pol01.SearchPolicyByCustomerIdController;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.PolicyTypeRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.PolicyTypeResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.GetPolicyTypeService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;

@RestController
public class GetPolicyTypeController extends WebServiceControllerResponseT<PolicyTypeRequest, PolicyTypeResponse> {
  private final ExtLogger log = ExtLogger.create(GetPolicyTypeController.class);
  private final String className = "GetPolicyTypeController";
  private final String Service = "GetPolicyType";
  private String tranId;
  
  @Autowired GetPolicyTypeService getPolicyTypeService;
  
  @Override
  @CrossOrigin
  @RequestMapping(value = "/GetPolicyType", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<PolicyTypeResponse>> service(
      @RequestBody RequestMessages<PolicyTypeRequest> req) throws Exception {
    ResponseEntity<ResponseMessages<PolicyTypeResponse>> entityResponse = null;
    ResponseMessages<PolicyTypeResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg() {};
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() :"";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    try {
      if (!validateRequestMessages(req)) {
        log.error(Service, tranId, className, "Error Validate");
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      }else {
        ServiceResults<PolicyTypeResponse> data = getPolicyTypeService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp,
                  req.getHeaderData(),
                  data.getResponseCode(),
                  data.getResponseDescription());
        } else {
          if(data.getResponseCode().equals("204")) {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getNotFoundDataResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("204")[0],
                    wMessage.getMessageByCode("204")[1]);
          }else {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("200")[0],
                    wMessage.getMessageByCode("200")[1]);
          }
        }
      }
    }
    catch (Exception e) {      
      log.error(Service, tranId, className, "error Exception "+e.getMessage());
      entityResponse =
          getSuccessResponse(
              resp,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
    }
    log.error(Service, tranId, className, "Service Resp : " + entityResponse);
    log.error(Service, tranId, className, "Service Done...");
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(PolicyTypeRequest req) throws Exception {
    if(req == null){
      log.info(Service, tranId, className, "error validate requestRecord invalid");
      return false;
    }
    if (PolicyUtil.StringIsNullOrEmpty(req.getPolicyno())) {
      log.info(Service, tranId, className, "error validate policy invalid");
      return false;
    }
    if (req.getCertno() == null) {
      log.info(Service, tranId, className, "error validate certNo invalid");
      return false;
    }
    if (req.getPolicyno().length() > 8
          || req.getCertno().length() > 8) {
      log.info(Service, tranId, className, "error validate policy || certNo Over length");
      return false;
    }
    
    return true;
    }
  
}
