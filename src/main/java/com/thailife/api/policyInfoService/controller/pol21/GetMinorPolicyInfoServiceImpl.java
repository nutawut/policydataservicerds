package com.thailife.api.policyInfoService.controller.pol21;


import com.thailife.api.policyInfoService.config.DBConnection;
import com.thailife.api.policyInfoService.dao.PartyDao;
import com.thailife.api.policyInfoService.dao.impl.OrdmastDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.PartyDaoImpl;
import com.thailife.api.policyInfoService.dao.impl.policy.GuardianJdbc;
import com.thailife.api.policyInfoService.model.GuardianM;
import com.thailife.api.policyInfoService.model.PolicyDetailM;
import com.thailife.api.policyInfoService.model.request.GetMinorPolicyInfoRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.GetMinorPolicyInfoResponse;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.model.subModel.MinorNameM;
import com.thailife.api.policyInfoService.model.subModel.MinorPolicyM;
import com.thailife.api.policyInfoService.util.ApiConfigM;
import com.thailife.api.policyInfoService.util.CallService;
import com.thailife.api.policyInfoService.util.PolicyStatus;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
@Log4j2

public class GetMinorPolicyInfoServiceImpl {
    private PartyDaoImpl partyDaoImpl = new PartyDaoImpl();
    private GuardianJdbc guardianJdbc = new GuardianJdbc();
    private CallService callService = new CallService();
    private static final String APP_NAME = "GetMinorPolicyInfo";
    private String TRANSACTION_ID = "";

    public ServiceResults<GetMinorPolicyInfoResponse> getResponse(RequestMessages<GetMinorPolicyInfoRequest> req) throws Exception{
        TRANSACTION_ID = req.getHeaderData().getMessageId();
        ServiceResults<GetMinorPolicyInfoResponse> response = new ServiceResults<>();
        ResponseStatus responseStatus = new ResponseStatus();
        GetMinorPolicyInfoResponse minorPolicyInfoResponse = null;
        try{
            PartyDao party = partyDaoImpl.findByPartyid(Integer.parseInt(req.getRequestRecord().getCustomer_id()));
            log.info(APP_NAME,req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "",
                            "",
                            "GetMinorPolicyInfoServiceImpl",
                            "party :", party != null ? party.toString() : "null");
            if(party == null){
                log.info(
                                APP_NAME,
                                req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "",
                                "",
                                "getResponse",
                                "party is null","");
                responseStatus = PolicyUtil.responseDataNotFound();
                response.setResponseCode(responseStatus.getStatusCode());
                response.setSuccess(false);
                return response;
            }
            String govId = party.getGovt_id();
            if(govId.equals("N000000000000")){
                response.setSuccess(false);
                responseStatus = new ResponseStatus("E","POL-21-003", MessagePol21.ErrMessage.error21_003);
                log.info(APP_NAME,req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "",
                                "",
                                "get GovId",
                                "เลขบัตรผิด ","N000000000000");
                return response;
            }

            List<GuardianM> guardian = guardianJdbc.findGuardianByParentidNo(govId);
            log.info(APP_NAME,
                            req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "",
                            "",
                            "GetMinorPolicyInfoServiceImpl",
                            "list guardian :",guardian.toString());
            if(guardian.isEmpty()){
                response.setSuccess(false);
                log.info(
                                APP_NAME,
                                req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "",
                                "",
                                "getResponse",
                                "ไม่พบข้อมูลกรมธรรม์ผู้เยาว์","");
                responseStatus = new ResponseStatus("E","POL-21-002", MessagePol21.ErrMessage.error21_002);
                return response;
            }
            List<MinorNameM> lstMinorName = setMinorName(guardian, req.getHeaderData());
            if(lstMinorName != null && !lstMinorName.isEmpty()){
                responseStatus = new ResponseStatus("S","200", "ดำเนินการเรียบร้อย");
                response.setSuccess(true);
                minorPolicyInfoResponse = new GetMinorPolicyInfoResponse();
                minorPolicyInfoResponse.setList_of_Minor_name(lstMinorName);
            }else{
                response.setSuccess(false);
                responseStatus = new ResponseStatus("E","POL-21-002", MessagePol21.ErrMessage.error21_002);
            }

        }catch (Exception e){
            throw e;
        }finally{
            response.setResponseCode(responseStatus.getErrorCode());
            response.setResponseDescription(responseStatus.getErrorMessage());
            response.setResult(minorPolicyInfoResponse);
        }

        return response;
    }

    private List<MinorNameM> setMinorName(List<GuardianM> lstGuardian, HeaderData header) throws Exception{
        ApiConfigM apiConfigM = callService.readPoperties();
        List<MinorNameM> result = new ArrayList<>();
        try(Connection conPolicy = DBConnection.getConnectionPolicy()) {
            List<String> partyTmp = new ArrayList<>();
            for (GuardianM guardian : lstGuardian) {
                //เลขอ้างอิง Party ของผู้เยาว์
                MinorNameM minorNameM = new MinorNameM();
                String refMinor = guardianJdbc.findRefIdGuardianByPolicyNo(guardian.getPolicyno(), conPolicy);
                if(!PolicyUtil.validateString(refMinor))
                    continue;
                HashMap<String, String> name = partyDaoImpl.findNameByGovId(refMinor);
                if(name == null)
                    continue;
                String partyId = name.get("party_id");
                //check partyId ซ้ำ
                if(partyTmp.contains(name.get("party_id"))){
                    int index = partyTmp.indexOf(partyId);
                    List<MinorPolicyM> lsMinorPolicy = getMinorPolicy(partyId, guardian.getPolicyno(), header, apiConfigM);
                    for(MinorPolicyM minorPolicy:lsMinorPolicy){
                        result.get(index).getList_of_policy().add(minorPolicy);
                        result.get(index).setTotal_policy(result.get(index).getList_of_policy().size());
                    }
                    continue;
                }
                minorNameM.setCustomer_id(partyId);
                minorNameM.setFirstname(name.get("fname_th"));
                minorNameM.setLastname(name.get("lname_th"));
                minorNameM.setPrename(name.get("pname_th"));
                minorNameM.setList_of_policy(getMinorPolicy(partyId, guardian.getPolicyno(), header, apiConfigM));
                minorNameM.setTotal_policy(minorNameM.getList_of_policy().size());
                if(minorNameM.getTotal_policy() != 0){
                    partyTmp.add(partyId);
                    result.add(minorNameM);
                }
            }
        }
        return result;
    }

    private List<MinorPolicyM> getMinorPolicy(String partyId, String policyGuardian, HeaderData header, ApiConfigM apiConfigM) throws Exception {
        JSONArray lstPolDb01 = getDataPolDb01(partyId, header, apiConfigM);
        List<MinorPolicyM> minorPolicyMList = new ArrayList<>();
        if (lstPolDb01 != null && (!lstPolDb01.isEmpty())) {
            try(Connection con = DBConnection.getConnectionPolicy()){
                for (int i = 0; i < lstPolDb01.length(); i++) {
                    JSONObject objPol01 = lstPolDb01.getJSONObject(i);
                    String policyNo = objPol01.getString("policyno");
                    if (policyNo.equals(policyGuardian)) {
                        PolicyDetailM policyDetailM = OrdmastDaoImpl.searchPolicyDetailByPolicyno(policyNo, con);
                        String policyStatus1 = objPol01.getString("policystatus1");
                        String policyStatus2 = policyDetailM.getPolicystatus2();
                        if (!PolicyStatus.isInactivePolicyOl(policyStatus1, policyStatus2)) {
                            MinorPolicyM minorPolicyM = new MinorPolicyM();
                            minorPolicyM.setPolicyno(policyNo);
                            minorPolicyM.setCertno("");
                            minorPolicyM.setType(objPol01.getString("type"));
                            minorPolicyM.setPlancode(objPol01.getString("plancode"));
                            minorPolicyM.setPlanname(objPol01.getString("planname"));
                            minorPolicyM.setScreentype(objPol01.getString("screentype"));
                            minorPolicyM.setPolicystatus1(objPol01.getString("policystatus1"));
                            minorPolicyM.setEffectivedate(objPol01.getString("effectivedate"));
                            minorPolicyM.setStatusdisplay(objPol01.getString("statusdisplay"));
                            minorPolicyM.setOldCertNo(objPol01.getString("oldCertNo"));
                            minorPolicyM.setOldPolicyNo(objPol01.getString("oldPolicyNo"));
                            minorPolicyM.setSum(objPol01.has("sum") ? objPol01.getInt("sum") : 0);
                            //call POL_DB_03 for find nextduedate & paydesc
                            JSONObject poldb03Json = getDataPolDb03(minorPolicyM.getPolicyno(), minorPolicyM.getCertno(), minorPolicyM.getType(), header, apiConfigM);
                            String nextduedate = "";
                            if (poldb03Json != null && !poldb03Json.equals("")) {
                                nextduedate = poldb03Json.has("paydesc") ? Arrays.asList("I", "F").contains(poldb03Json.getString("paydesc")) ? poldb03Json.getString("nextduedate") : "" : "";
                            } else {
                                throw new Exception("api POL_DB_03 not success");
                            }
                            minorPolicyM.setNextduedate(nextduedate);
                            minorPolicyMList.add(minorPolicyM);
                        }
                    }
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return minorPolicyMList;
    }

    private JSONArray getDataPolDb01(String partyId, HeaderData reqHeader, ApiConfigM apiConfigM) throws Exception {
        String method = "getDataPolDb01";
        JSONObject respPolDB01 = PolicyUtil.getPolDB01(partyId
                ,reqHeader.getMessageId()
                ,reqHeader.getSentDateTime()
                , apiConfigM);
        JSONObject respStatusPol01 = respPolDB01.getJSONObject("responseStatus");
        if (respStatusPol01.getString("errorCode").equals("200")) {
            JSONObject respRecordPol01 = respPolDB01.getJSONObject("responseRecord");
            if(respRecordPol01 != null){
                JSONArray dataListPol01 = respRecordPol01.getJSONArray("list_of_policy");
                return dataListPol01;
            }
        }else {
            log.info(
                            APP_NAME,
                            reqHeader.getMessageId(),
                            "",
                            method,
                            "response pol_db01 is not success","");
        }
        return null;
    }

    private JSONObject getDataPolDb03(String policyNo,String certNo,String type, HeaderData reqHeader, ApiConfigM apiConfigM) throws Exception {
        String method = "getDataPolDb03";
        JSONObject respPolDB03 = PolicyUtil.getPolDB03(policyNo,certNo,type
                ,reqHeader.getMessageId()
                ,reqHeader.getSentDateTime()
                , apiConfigM);
        JSONObject respStatusPol03 = respPolDB03.getJSONObject("responseStatus");
        if (respStatusPol03.getString("errorCode").equals("200")) {
            JSONObject respRecordPol03 = respPolDB03.getJSONObject("responseRecord");
            if(respRecordPol03 != null){
                JSONArray dataListPol03 = respRecordPol03.getJSONArray("nextPayPeriodList");
                return dataListPol03.getJSONObject(0);
            }
        }else {
            log.info(
                            APP_NAME,
                            reqHeader.getMessageId(),
                            "",
                            method,
                            "response pol_db03 is not success","");
        }
        return null;
    }

    public static void main(String[] arg) throws Exception {
//        CallService callService = new CallService();
//        ApiConfigM apiConfigM = callService.readPoperties();
//        DataServiceRequestT<GetMinorPolicyInfoRequest> req = new DataServiceRequestT<>();
//        GetMinorPolicyInfoRequest subreq = new GetMinorPolicyInfoRequest();
//        subreq.setCustomer_id("3653834");
//        HeaderData headerData = new HeaderData();
//        headerData.setMessageId("test");
//        headerData.setSentDateTime("26-11-2019 15:36:18.066");
//        req.setRequestRecord(subreq);
//        req.setHeaderData(headerData);
//        GetMinorPolicyInfoServiceImpl service = new GetMinorPolicyInfoServiceImpl();
//        System.out.println(service.getResponse(req));

    }
}
