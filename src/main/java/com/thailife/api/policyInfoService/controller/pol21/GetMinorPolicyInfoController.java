package com.thailife.api.policyInfoService.controller.pol21;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.*;
import com.thailife.api.policyInfoService.model.response.*;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@CrossOrigin
@RestController
public class GetMinorPolicyInfoController
    extends WebServiceControllerResponseT<GetMinorPolicyInfoRequest, GetMinorPolicyInfoResponse> {
  private static final String APP_NAME = "GetMinorPolicyInfo";
  private String TRANSACTION_ID = "";

  @PostMapping("/GetMinorPolicyInfo")
  @Override
  public ResponseEntity<ResponseMessages<GetMinorPolicyInfoResponse>> service(
      @RequestBody RequestMessages<GetMinorPolicyInfoRequest> req) throws Exception {
    ResponseEntity<ResponseMessages<GetMinorPolicyInfoResponse>> entityResponse;
    ResponseMessages<GetMinorPolicyInfoResponse> resp = new ResponseMessages<>();
    String method = "getMinorPolicyInfoController";
    TRANSACTION_ID = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    log.info(
        APP_NAME,
        req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "",
        "",
        method,
        "",
        " [Start]");
    ResponseMessages<GetMinorPolicyInfoResponse> response = new ResponseMessages<>();
    String msgId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    Long logId =
        PolicyUtil.saveTransaction(
            new TransactionRequest(
                PolicyUtil.convertObjectToJson(req), APP_NAME, "PolicyDataService"),
            msgId);
    ResponseStatus responseStatus = null;
    try {
      if (!validateRequestMessages(req)) {
        responseStatus = PolicyUtil.responseValidateError();
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                responseStatus.getErrorCode(),
                responseStatus.getErrorMessage());
      } else {
        GetMinorPolicyInfoServiceImpl service = new GetMinorPolicyInfoServiceImpl();
        ServiceResults<GetMinorPolicyInfoResponse> data = service.getResponse(req);
        resp.setResponseRecord(data.getResult());
        if (data.isSuccess()) {
          entityResponse =
              getSuccessResponse(
                  resp,
                  req.getHeaderData(),
                  data.getResponseCode(),
                  data.getResponseDescription());
        }else {
          entityResponse =
                  getErrorResponse(
                          resp,
                          req.getHeaderData(),
                          data.getResponseCode(),
                          data.getResponseDescription());
        }
      }
    } catch (Exception e) {
      responseStatus = PolicyUtil.responseInternalServerError();
      e.printStackTrace();
      log.error(APP_NAME, TRANSACTION_ID, "", method, "error", "Exception", e);
      return getErrorInternalServerResponse(
          resp,
          req != null ? req.getHeaderData() : new HeaderData(),
          responseStatus.getErrorCode(),
          responseStatus.getErrorMessage());
    } finally {
      if (logId != null) {
        // update transaction
        PolicyUtil.updateTransaction(
            new TransactionRequest(logId, "", "", PolicyUtil.convertObjectToJson(response)), msgId);
      }
    }
    log.info(
        APP_NAME,
        req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "",
        "",
        method,
        "",
        " [End] response > " + response);
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(GetMinorPolicyInfoRequest req) throws Exception {
    // validate body
    if (!PolicyUtil.validateString(req.getCustomer_id(), 100)
        || !PolicyUtil.isInt(req.getCustomer_id())) {
      log.info(
          APP_NAME,
          TRANSACTION_ID,
          "",
          "validateRequestRecordData",
          "error validate request",
          "customer_id invalid");
      return false;
    }
    return true;
  }
}
