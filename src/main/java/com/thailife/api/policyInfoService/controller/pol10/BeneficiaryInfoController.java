package com.thailife.api.policyInfoService.controller.pol10;

import java.util.List;

import com.thailife.api.policyInfoService.controller.pol11.PaymentHistoryController;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.model.request.BeneficiaryInfoRequest;
import com.thailife.api.policyInfoService.model.request.DataServiceRequestT;
import com.thailife.api.policyInfoService.model.response.BeneficiaryInfoResponse;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.ResponseMessagesList;
import com.thailife.api.policyInfoService.model.response.ResponseStatus;
import com.thailife.api.policyInfoService.service.BeneficiaryInfoService;
import com.thailife.api.policyInfoService.util.DateFormat;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyException;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import lombok.extern.log4j.Log4j;
@Component
@RestController
public class BeneficiaryInfoController {
  private static final ExtLogger log = ExtLogger.create(BeneficiaryInfoController.class);
  private static final String className = "BeneficiaryInfoController";
  private static final String Service = "GetBeneficiaryInfoPOL10";
  private static String tranId;
  @Autowired WebServiceMsg wMessage;
  @Autowired BeneficiaryInfoService beneficiaryInfoService;

  @CrossOrigin
  @RequestMapping(value = "/GetBeneficiaryInfo", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessagesList<BeneficiaryInfoResponse>> GetBeneficiaryInfo(
      @RequestBody DataServiceRequestT<BeneficiaryInfoRequest> req) {
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() :"";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    if (!PolicyUtil.validateHeaderData(req.getHeaderData())) {
      log.error(Service, tranId, className, "Validate Error");
      return ResponseEntity.status(400)
          .body(setResponse(null, responseValidateError(), req.getHeaderData()));
    }
    if (!validateBody(req.getRequestRecord())) {
      log.error(Service, tranId, className, "Validate Error");
      return ResponseEntity.status(400)
          .body(setResponse(null, responseValidateError(), req.getHeaderData()));
    }

    ResponseMessagesList<BeneficiaryInfoResponse> response = new ResponseMessagesList<>();

    ResponseStatus responseStatus = null;
    List<BeneficiaryInfoResponse> records = null;
    try {
      records = beneficiaryInfoService.getResponse(req.getRequestRecord(), tranId);
      if (records.size() == 0) {
        responseStatus =
            new ResponseStatus(
                "E", wMessage.getMessageByCode("204")[0], wMessage.getMessageByCode("204")[1]);
        records = null;
        response = setResponse(records, responseStatus, req.getHeaderData());
        return ResponseEntity.status(200).body(response);
      } else {
        responseStatus =
            new ResponseStatus(
                "S", wMessage.getMessageByCode("200")[0], wMessage.getMessageByCode("200")[1]);
        response = setResponse(records, responseStatus, req.getHeaderData());
        return ResponseEntity.status(200).body(response);
      }
    } catch (Exception e) {
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
      e.printStackTrace();
      responseStatus =
          new ResponseStatus(
              "E", wMessage.getMessageByCode("500")[0], wMessage.getMessageByCode("500")[1]);
      response.setResponseStatus(responseStatus);
    }
    log.info(Service, tranId, className, "Service Resp : " + response);
    log.info(Service, tranId, className, "Service Done...");

    return ResponseEntity.status(Integer.parseInt(response.getResponseStatus().getErrorCode()))
        .body(response);
  }

  public static boolean validateBody(BeneficiaryInfoRequest req) {
    if (null == req) {
      return false;
    }
    if (req.getPolicyno() == null
        || ("").equals(req.getPolicyno())
        || req.getPolicyno().length() > 8) {
      log.error(
          Service,
          tranId,
          className,
          "Validate \"policyno\" error!, \"policyno\" can't be null and maximum length = 8");
      return false;
    } else if (req.getCertno() != null && req.getCertno().length() > 8) {
      log.error(
          Service,
          tranId,
          className,
          "Validate \"certno\" error!, \"certno\" can't be null and maximum length = 8");
      return false;
    } else if (req.getType() == null || ("").equals(req.getType()) || req.getType().length() > 30) {
      log.error(
          Service,
          tranId,
          className,
          "Validate \"type\" error!, \"type\" can't be null and maximum length = 30");
      return false;
      //      else if (!ProductType.isType(req.getType()))
      //          return false;
    } else {
      return true;
    }
  }

  public static ResponseStatus responseValidateError() {
    return newResponseStatus("400", "E", "การค้นหาไม่ถูกต้อง");
  }

  public static ResponseStatus newResponseStatus(
      String statusCode, String errorCode, String errorMessage) {
    ResponseStatus status = new ResponseStatus(errorCode, statusCode, errorMessage);
    return status;
  }

  public static ResponseMessagesList<BeneficiaryInfoResponse> setResponse(
      List<BeneficiaryInfoResponse> records, ResponseStatus responseStatus, HeaderData header) {
    ResponseMessagesList<BeneficiaryInfoResponse> response =
            new ResponseMessagesList<>();
    response.setResponseRecord(records);
    response.setResponseStatus(responseStatus);
    response.setHeaderData(headerDataResponse(header));
    return response;
  }

  public static HeaderData headerDataResponse(HeaderData headerData) {

    if (headerData == null) headerData = new HeaderData();
    headerData.setResponseDateTime(DateFormat.now(DateFormat.DDMMYYYY_HHMMSS));
    return headerData;
  }
}
