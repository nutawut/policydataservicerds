package com.thailife.api.policyInfoService.controller.pol06;

import com.thailife.api.policyInfoService.controller.pol11.PaymentHistoryController;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SaleInfoRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SaleInfoResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SaleInfoService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import lombok.extern.log4j.Log4j;

@RestController
public class SaleInfoController extends WebServiceControllerResponseT<SaleInfoRequest, SaleInfoResponse> {
  private final ExtLogger log = ExtLogger.create(SaleInfoController.class);
  private final String className = "SaleInfoController";
  private final String Service = "GetSaleInfo";
  private String tranId;
  
  @Autowired SaleInfoService saleInfoService;
  
  @Override
  @RequestMapping(value = "/GetSaleInfo", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<SaleInfoResponse>> service(
      @RequestBody RequestMessages<SaleInfoRequest> req) throws Exception {
    ResponseEntity<ResponseMessages<SaleInfoResponse>> entityResponse = null;
    ResponseMessages<SaleInfoResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg() {};
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    try {
      if (!validateRequestMessages(req)) {
        log.error(Service, tranId, className, "Validate Error");
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      }else {
        ServiceResults<SaleInfoResponse> data = saleInfoService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp,
                  req.getHeaderData(),
                  data.getResponseCode(),
                  data.getResponseDescription());
        } else {
          if(data.getResult() == null) {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getNotFoundDataResponse(
                    resp,
                    req.getHeaderData(),
                    "POL-06-002",
                    wMessage.getMessageByCode("204")[1]);
          }else {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("200")[0],
                    wMessage.getMessageByCode("200")[1]);
          }
        }
      }
    }
    catch (Exception e) {      
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
      entityResponse =
          getSuccessResponse(
              resp,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
    }

    log.info(Service, tranId, className, "Service Resp : " + entityResponse);
    log.info(Service, tranId, className, "Service Done...");
    return entityResponse;
   }

  @Override
  protected boolean validateRequestRecordData(SaleInfoRequest req) throws Exception {
    if(req == null ) return false;
    if (PolicyUtil.StringIsNullOrEmpty(req.getPolicyno()) || req.getPolicyno().length() > 8) {
      log.info(Service, tranId, className, "error validate policyNo invalit");
      return false;
    }
    else if (PolicyUtil.StringIsNullOrEmpty(req.getType()) || req.getType().length() > 30) {
      log.info(Service, tranId, className, "error validate type invalit");
      return false;
    }
    else
      return true;
  }

}
