package com.thailife.api.policyInfoService.controller.pol21;

public class MessagePol21 {
    public static class ErrMessage{
        public static String DATA_NOT_FOUND = "dataNotFound";
        public static String error21_002 = "ไม่พบข้อมูลกรมธรรม์ที่มีความคุ้มครอง";
        public static String error21_003 = "เลขบัตรประชาชนไม่ถูกต้อง";
    }
}
