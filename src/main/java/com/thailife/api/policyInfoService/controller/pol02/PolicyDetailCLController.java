package com.thailife.api.policyInfoService.controller.pol02;

import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.PolicyDetailRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.PolicyDetailCLResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PolicyDetailCLService;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;

@Component
@RestController
public class PolicyDetailCLController
    extends WebServiceControllerResponseT<PolicyDetailRequest, PolicyDetailCLResponse> {
  private final ExtLogger log = ExtLogger.create(PolicyDetailCLController.class);
  private final String className = "PolicyDetailCLController";
  private final String Service = "PolicyDetailsCL";
  @Autowired PolicyDetailCLService policyCLService;

  @Override
  @CrossOrigin
  @RequestMapping(value = "/PolicyDetailsCL", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<PolicyDetailCLResponse>> service(
      @RequestBody RequestMessages<PolicyDetailRequest> req) {
    ResponseEntity<ResponseMessages<PolicyDetailCLResponse>> entityResponse;
    ResponseMessages<PolicyDetailCLResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg() {};
    String tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    try {
      if (!validateRequestMessages(req)) {
        log.debug(
            PolicyUtil.pattenLog(
                tranId, String.format("%s:%s | validate Error", Service, className)));
        entityResponse =
            getErrorValidateResponse(
                resp,
                PolicyUtil.headerDataResponse(req.getHeaderData()),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      } else {
        log.info(
            PolicyUtil.pattenLog(
                tranId,
                String.format("%s:%s | Service start %n request: %s", Service, className, req)));
        ServiceResults<PolicyDetailCLResponse> data =
            policyCLService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
        } else {
          if (data.getResult() == null) {
            entityResponse =
                getNotFoundDataResponseError(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("204")[0],
                    wMessage.getMessageByCode("204")[1]);
          } else {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("200")[0],
                    wMessage.getMessageByCode("200")[1]);
          }
        }
      }
    } catch (Exception e) {
      log.error(
          PolicyUtil.pattenLog(
              tranId,
              String.format("%s:%s | Error Exception %s ", Service, className, e.getMessage())));
      entityResponse =
          getSuccessResponse(
              resp,
              PolicyUtil.headerDataResponse(req.getHeaderData()),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
    }

    log.info(
        PolicyUtil.pattenLog(
            tranId,
            String.format(
                "%s:%s | Service done %n Response : %s", Service, className, entityResponse)));

    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(PolicyDetailRequest req) throws Exception {
    if (req == null) {
      log.debug(
          PolicyUtil.pattenLog(
              "",
              String.format("%s:%s | Validate Error requestRecord invalid", Service, className)));
      return false;
    } else {
      if (PolicyUtil.StringIsNullOrEmpty(req.getPolicyno()) || req.getPolicyno().length() > 4) {
        log.debug(
            PolicyUtil.pattenLog(
                "", String.format("%s:%s | Validate Error policyNo invalid", Service, className)));
        return false;
      }
      if (PolicyUtil.StringIsNullOrEmpty(req.getCertno()) || req.getCertno().length() > 8) {
        log.debug(
            PolicyUtil.pattenLog(
                "", String.format("%s:%s | Validate Error certNo invalid", Service, className)));
        return false;
      }
      if (PolicyUtil.StringIsNullOrEmpty(req.getType())
          || req.getType().length() > 30
          || !req.getType().contains("CL")) {
        log.debug(
            PolicyUtil.pattenLog(
                "", String.format("%s:%s | Validate Error Type invalid", Service, className)));
        return false;
      }
    }
    return true;
  }
}
