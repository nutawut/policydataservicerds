package com.thailife.api.policyInfoService.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.model.response.SampleTestResponse;

@RestController
@Log4j2
public class testController {
  @RequestMapping("/PolicySampleTest")
  public SampleTestResponse Sample(
      @RequestParam(value = "name", defaultValue = "Robot") String name) {
    SampleTestResponse response = new SampleTestResponse();
    response.setId(1);
    response.setMessage("Your name is " + name);
    return response;
  }

  @GetMapping("testLog")
  public String testLog() {
    log.info("info log");
    log.debug("debug log");
    log.warn("warn log");
    log.error("error log");
    return "testLog success";
  }
}
