package com.thailife.api.policyInfoService.controller.pol18;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.controller.pol01.SearchPolicyByCustomerIdController;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchPolicyByCustomerIdRequest;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SearchPolicyByPartyIdResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SearchPolicyByPartyIdService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchPolicyOnlyByPartyIdController
    extends WebServiceControllerResponseT<
        SearchPolicyByCustomerIdRequest, SearchPolicyByPartyIdResponse> {
  private final ExtLogger log = ExtLogger.create(SearchPolicyOnlyByPartyIdController.class);
  private final String className = "SearchPolicyOnlyByPartyIdController";
  private final String Service = "SearchPolicyListOnlyByCustomerIdImp";
  private String tranId;
  @Autowired SearchPolicyByPartyIdService policyByPartyIdService;

  @Override
  @PostMapping("SearchPolicyListOnlyByCustomerIdImp")
  public ResponseEntity<ResponseMessages<SearchPolicyByPartyIdResponse>> service(
      @RequestBody RequestMessages<SearchPolicyByCustomerIdRequest> req) throws Exception {
    WebServiceMsg wMessage = new WebServiceMsg();
    ResponseEntity<ResponseMessages<SearchPolicyByPartyIdResponse>> entityResponse = null;
    ResponseMessages<SearchPolicyByPartyIdResponse> resp = new ResponseMessages<>();
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId(): "";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    try {
      if (!validateRequestMessages(req)) {
        log.error(Service, tranId, className, "Error Validate");
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      } else {
        ServiceResults<SearchPolicyByPartyIdResponse> data =
            policyByPartyIdService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
        } else {
          resp.setResponseRecord(data.getResult());
          if (data.getResponseCode().equals("204")) {
            entityResponse =
                getNotFoundDataResponse(
                    resp,
                    req.getHeaderData(),
                    data.getResponseCode(),
                    data.getResponseDescription());
          } else {
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    data.getResponseCode(),
                    data.getResponseDescription());
          }
        }
      }
      
    } catch (Exception ex) {
      log.error(Service, tranId, className, "error Exception "+ex.getMessage());
      return getErrorInternalServerResponse(
          resp,
          req != null ? req.getHeaderData() : new HeaderData(),
          wMessage.getMessageByCode("500")[0],
          wMessage.getMessageByCode("500")[1]);
    }
    log.info(Service, tranId, className, "Service Resp : " + entityResponse);
    log.info(Service, tranId, className, "Service Done...");
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(SearchPolicyByCustomerIdRequest req)
      throws Exception {
    if (req == null || req.getCustomer_id() == null || !req.getCustomer_id().matches("^[0-9]+$")) {
      log.info(Service, tranId, className, "error validate customer_id inValid");
      return false;
    }
    if (req.getCustomer_id().length() >= 20){
      log.info(Service, tranId, className, "error validate customer_id inValid");
      return false;
    }
    return true;
  }
}
