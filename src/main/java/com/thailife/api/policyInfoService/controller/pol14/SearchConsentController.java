package com.thailife.api.policyInfoService.controller.pol14;

import com.thailife.api.policyInfoService.service.impl.InsertConsentServiceImpl;
import com.thailife.log.ExtLogger;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.SearchConsentRequest;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.SearchConsentResponse;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.SearchConsentService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;

@RestController
public class SearchConsentController extends WebServiceControllerResponseT<SearchConsentRequest, SearchConsentResponse> {
  private final ExtLogger log = ExtLogger.create(SearchConsentController.class);
  private final String className = "SearchConsentController";
  private final String Service = "SearchConsent";
  private String tranId;
  
  @Autowired
  SearchConsentService searchConsentService;
  
  @Override
  @CrossOrigin
  @RequestMapping(value = "/SearchConsent", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<SearchConsentResponse>> service(
      @RequestBody RequestMessages<SearchConsentRequest> req) throws Exception {
    ResponseEntity<ResponseMessages<SearchConsentResponse>> entityResponse = null;
    ResponseMessages<SearchConsentResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg() {};
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId():"";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    try {
      log.error(Service, tranId, className, "Error Validate");
      if (!validateRequestMessages(req)) {
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      }else {
        ServiceResults<SearchConsentResponse> data = searchConsentService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp,
                  req.getHeaderData(),
                  data.getResponseCode(),
                  data.getResponseDescription());
        } else {
          if(data.getResult() == null) {
            entityResponse =
                getNotFoundDataResponseError(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("204")[0],
                    wMessage.getMessageByCode("204")[1]);
          }else {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("200")[0],
                    wMessage.getMessageByCode("200")[1]);
          }
        }
      }
    }
    catch (Exception e) {
      entityResponse =
          getErrorInternalServerResponse(
              resp,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
    }
    log.info(Service, tranId, className, "Service Resp : " + entityResponse);
    log.info(Service, tranId, className, "Service Done...");
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(SearchConsentRequest req) throws Exception {
    if(req == null){
      log.error(Service, tranId, className, "Validate requestRecord can't be null and maximum length = 100");
      return false;
    }
    if(req.getCustomer_id() == null){
      log.error(Service, tranId, className, "Validate \"customer_id\" error!, \"customer_id\" can't be null and maximum length = 100");
      return false;
    }
    if (req.getCustomer_id().length() > 100 || !PolicyUtil.validateInteger(req.getCustomer_id())) {
      log.error(Service, tranId, className, "Validate \"customer_id\" error!, \"customer_id\" can't be null and maximum length = 100");
      return false;
    }
    
    return true;
  }

}
