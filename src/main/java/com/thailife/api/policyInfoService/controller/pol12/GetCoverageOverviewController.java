package com.thailife.api.policyInfoService.controller.pol12;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.controller.pol13.CurrentAVInfoController;
import com.thailife.api.policyInfoService.model.ListOfProtection;
import com.thailife.api.policyInfoService.model.request.GetCoverageOverviewRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.impl.GetCoverageOverviewServiceImpl;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import com.thailife.log.ExtLogger;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//@Log4j
@RestController
public class GetCoverageOverviewController
    extends WebServiceControllerResponseT<GetCoverageOverviewRequest, ListOfProtection> {
  private final ExtLogger log = ExtLogger.create(GetCoverageOverviewController.class);
  private final String className = "GetCoverageOverviewController";
  private final String Service = "GetCoverageOverviewPOL12";
  private String tranId;
  
  @Autowired GetCoverageOverviewServiceImpl getCoverageOverviewService;
  @Autowired WebServiceMsg wMessage;

  @CrossOrigin
  @Override
  @RequestMapping(value = "/GetCoverageOverview", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<ListOfProtection>> service(
      @RequestBody RequestMessages<GetCoverageOverviewRequest> req) throws Exception {
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    ResponseEntity<ResponseMessages<ListOfProtection>> entityResponse = null;
    ResponseMessages<ListOfProtection> response = new ResponseMessages<>();
    if (!validateRequestMessages(req)) {
      log.error(Service, tranId, className, "Error Validate");
      return getErrorValidateResponse(
          response, req.getHeaderData(), wMessage.getMessageByCode("400")[0], "การค้นหาไม่ถูกต้อง");
    }
    String transactionId = req.getHeaderData().getMessageId();
    String customerId = req.getRequestRecord().getCustomerId();
    try {
      ServiceResults<ListOfProtection> data = getCoverageOverviewService.getResponse(req);
      response.setResponseRecord(data.getResult());
      if (!data.isSuccess()) {
        entityResponse =
            getErrorInternalServerResponse(
                response,
                req.getHeaderData(),
                data.getResponseCode(),
                data.getResponseDescription());
      } else {
        entityResponse =
            getSuccessResponse(
                response,
                req.getHeaderData(),
                data.getResponseCode(),
                data.getResponseDescription());
      }

      if (data.getResponseCode().equals("204")) {
        entityResponse =
            getNotFoundDataResponseError(
                response,
                req.getHeaderData(),
                data.getResponseCode(),
                data.getResponseDescription());
      }

    } catch (Exception e) {
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
      entityResponse =
          getErrorInternalServerResponse(
              response,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
    }
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(GetCoverageOverviewRequest req) throws Exception {
    if(req == null){
      log.info(Service, tranId, className, "error validate requestRecord in valid");
      return false;
    }
    if(req.getCustomerId() == null){
      log.info(Service, tranId, className, "error validate customerId in valid");
      return false;
    }
    if(("").equals(req.getCustomerId()) || req.getCustomerId().length() > 100 || !PolicyUtil.validateInteger(req.getCustomerId())){
      log.info(Service, tranId, className, "error validate customerId in valid");
      return false;
    }
    return true;
  }
}
