package com.thailife.api.policyInfoService.controller.pol11;

import com.thailife.api.policyInfoService.controller.pol12.GetCoverageOverviewController;
import com.thailife.log.ExtLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.PaymentHistoryRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.NextPayPeriodListResponse;
import com.thailife.api.policyInfoService.model.response.PaymentHistoryResponse;
import com.thailife.api.policyInfoService.model.response.PolicyDetailCLResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.PaymentHistoryService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import lombok.extern.log4j.Log4j;

@RestController
public class PaymentHistoryController extends WebServiceControllerResponseT<PaymentHistoryRequest, PaymentHistoryResponse> {
  private final ExtLogger log = ExtLogger.create(PaymentHistoryController.class);
  private final String className = "PaymentHistoryController";
  private final String Service = "GetPaymentHistoryPol11";
  private String tranId;
  
  @Autowired PaymentHistoryService paymentHistoryService;
  
  @Override
  @CrossOrigin
  @RequestMapping(value = "/GetPaymentHistory", method = RequestMethod.POST)
  public ResponseEntity<ResponseMessages<PaymentHistoryResponse>> service(
      @RequestBody RequestMessages<PaymentHistoryRequest> req) throws Exception {
    ResponseEntity<ResponseMessages<PaymentHistoryResponse>> entityResponse = null;
    ResponseMessages<PaymentHistoryResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg() {};
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() :"";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    try {
      if (!validateRequestMessages(req)) {
        log.error(Service, tranId, className, "Error Validate");
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                wMessage.getMessageByCode("400")[1]);
      }else {
        ServiceResults<PaymentHistoryResponse> data = paymentHistoryService.getPaymentHistory(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          entityResponse =
              getErrorInternalServerResponse(
                  resp,
                  req.getHeaderData(),
                  data.getResponseCode(),
                  data.getResponseDescription());
        } else {
          if(data.getResult() == null) {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getNotFoundDataResponseError(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("204")[0],
                    wMessage.getMessageByCode("204")[1]);
          }else {
            resp.setResponseRecord(data.getResult());
            entityResponse =
                getSuccessResponse(
                    resp,
                    req.getHeaderData(),
                    wMessage.getMessageByCode("200")[0],
                    wMessage.getMessageByCode("200")[1]);
          }
          
        }
      }
    }
    catch (Exception e) {
      entityResponse =
          getSuccessResponse(
              resp,
              req.getHeaderData(),
              wMessage.getMessageByCode("500")[0],
              wMessage.getMessageByCode("500")[1]);
      log.error(Service, tranId, className, "Error Exception : " + e.getMessage());
    }
    log.error(Service, tranId, className, "Service Resp : " + entityResponse);
    log.error(Service, tranId, className, "Service Done...");
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(PaymentHistoryRequest req) throws Exception {
      
    String effectivedate = req.getEffectivedate();
    if (PolicyUtil.StringIsNullOrEmpty(req.getPolicyno()) || req.getPolicyno().length() > 8) {
      //log.error("Validate \"policyno\" error!, \"policyno\" can't be null and maximum length = 8");
      log.error(Service, tranId, className, "Error Validate :" + "Validate \"policyno\" error!, \"policyno\" can't be null and maximum length = 8");
      return false;
    } 
    
    if (req.getCertno() != null && req.getCertno().length() > 8) {
      //log.error("Validate \"certno\" error!, \"certno\" can't be null and maximum length = 8");
      log.error(Service, tranId, className, "Error Validate : "+"Validate \"certno\" error!, \"certno\" can't be null and maximum length = 8");
      return false;
    } 
    
    if (PolicyUtil.StringIsNullOrEmpty(req.getType()) || req.getType().length() > 30) {
      //log.error("Validate \"type\" error!, \"type\" can't be null and maximum length = 30");
      log.error(Service, tranId, className, "Error Validate : "+"Validate \"type\" error!, \"type\" can't be null and maximum length = 30");
      return false;
    } 

    if (null != effectivedate && !("").equals(effectivedate)) {
        return checkDateFormat(effectivedate);
    } 

    return true;
  }
  
  private boolean checkDateFormat(String effectivedateStr) {
    if (effectivedateStr.length() != 8) {
        return false;
    } else if (Integer.parseInt(effectivedateStr.substring(4, 6)) > 12) {
        return false;
    } else if (Integer.parseInt(effectivedateStr.substring(6, 8)) > 31) {
        return false;
    } else {
        return true;
    }
}

}
