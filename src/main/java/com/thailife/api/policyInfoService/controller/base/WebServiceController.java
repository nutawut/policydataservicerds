package com.thailife.api.policyInfoService.controller.base;
import java.io.PrintWriter;
import java.io.StringWriter;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.response.HeaderData;
import com.thailife.api.policyInfoService.util.PolicyUtil;

/**
 * Level 0 T request; S response;
 */
public abstract class WebServiceController<T, S> {

	protected String transactionID;

	/** for Level 1 */
	protected abstract boolean validateRequestRecordData(T req) throws Exception;

	protected boolean validateRequestMessages(RequestMessages<T> t) throws Exception {
		if (!validateRequestMessagesBasic(t))
			return false;
		if (!validateRequestRecordData(t.getRequestRecord()))
			return false;
		return true;
	}

	public boolean validateRequestMessagesBasic(RequestMessages<T> req) {
		if (req == null)
			return false;
		if (!validateHeaderData(req.getHeaderData()))
			return false;
		if (!validateRequestRecord(req.getRequestRecord()))
			return false;

		return true;
	}

	protected boolean validateHeaderData(HeaderData header) {
		if (header == null)
			return false;
		if (header.getMessageId() == null || header.getMessageId().trim().length() == 0
				|| header.getMessageId().trim().length() > 50)
			return false;
		if (header.getSentDateTime() == null || header.getSentDateTime().trim().length() == 0)
			return false;
		return true;
	}

	protected boolean validateRequestRecord(T req) {
		if (req == null)
			return false;
		return true;
	}

	public HeaderData setResponseTime(HeaderData headerData) {
		if (headerData == null)
			headerData = new HeaderData();
		headerData.setResponseDateTime(PolicyUtil.now());
		return headerData;
	}

	public String getError(Exception e) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw, true);
		e.printStackTrace(pw);
		String error = sw.getBuffer().toString().split("\n")[0];
		return error;
	}
}
