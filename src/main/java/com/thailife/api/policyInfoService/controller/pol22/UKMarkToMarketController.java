package com.thailife.api.policyInfoService.controller.pol22;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.model.request.TransactionRequest;
import com.thailife.api.policyInfoService.model.request.UKMarkToMarketRequest;
import com.thailife.api.policyInfoService.model.response.*;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@Log4j2
@CrossOrigin
@RestController
public class UKMarkToMarketController extends WebServiceControllerResponseT<UKMarkToMarketRequest, UKMarkToMarketResponse> {
    private static final String APP_NAME = "UKMarkToMarket";
    private String transactionId = "";
    @PostMapping("/UKMarkToMarket")
    @Override
    public ResponseEntity<ResponseMessages<UKMarkToMarketResponse>> service(@RequestBody RequestMessages<UKMarkToMarketRequest> req) throws Exception {
        transactionId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
        String transactionId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
        String method = "ukMarkToMarketController";
        log.info(String.format("%s " + APP_NAME + "|" + method + "|" + "[START] req : %s", transactionId, req.toString()));
        Long logId =
                PolicyUtil.saveTransaction(
                        new TransactionRequest(PolicyUtil.convertObjectToJson(req), APP_NAME, "PolicyDataService"), transactionId);
        ResponseEntity<ResponseMessages<UKMarkToMarketResponse>> entityResponse;
        ResponseMessages<UKMarkToMarketResponse> resp = new ResponseMessages<>();
        int errorCode = 200;
        ResponseStatus responseStatus = null;
        String thirdParty = "";
        try {
            if (!validateRequestMessages(req)) {
                responseStatus = PolicyUtil.responseValidateError();
                entityResponse =
                        getErrorValidateResponse(
                                resp,
                                req == null ? null : req.getHeaderData(),
                                responseStatus.getErrorCode(),
                                responseStatus.getErrorMessage());
            }else{
                UKMarkToMarketServiceImpl service = new UKMarkToMarketServiceImpl();
                ServiceResults<UKMarkToMarketResponse> data = service.getResponse(req);
                resp.setResponseRecord(data.getResult());
                if (data.isSuccess()) {
                    entityResponse =
                            getSuccessResponse(
                                    resp,
                                    req.getHeaderData(),
                                    data.getResponseCode(),
                                    data.getResponseDescription());
                }else {
                    entityResponse =
                            getErrorResponse(
                                    resp,
                                    req.getHeaderData(),
                                    data.getResponseCode(),
                                    data.getResponseDescription());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return getErrorInternalServerResponse(
                    resp,
                    req != null ? req.getHeaderData() : new HeaderData(),
                    responseStatus.getErrorCode(),
                    responseStatus.getErrorMessage());
        }finally {
//            response.setHeaderData(PolicyUtil.headerDataResponse(req.getHeaderData()));
            if (logId != null) {
                // update transaction
                PolicyUtil.updateTransaction(
                        new TransactionRequest(logId, "", "", PolicyUtil.convertObjectToJson(resp), thirdParty), transactionId);
            }
        }
        log.info(String.format("%s " + APP_NAME + "|" + method + "|" + "[END] req : %s", transactionId, req.toString()));
        return entityResponse;
    }
    @Override
    protected boolean validateRequestRecordData(UKMarkToMarketRequest req) throws Exception {
        String method = "validate";
        //validate body
        if (req == null) {
            log.error(String.format("%s " + APP_NAME + "|" + method + "|" + "error validate requestRecord", transactionId));
            return false;
        }
        if (!PolicyUtil.validateString(req.getPolicyNo(),8)) {
            log.error(String.format("%s " + APP_NAME + "|" + method + "|" + "error validate policyNo", transactionId));
            return false;
        }
        if (!PolicyUtil.isInt(req.getPolicyNo())){
            log.error(String.format("%s " + APP_NAME + "|" + method + "|" + "error validate policyNo", transactionId));
            return false;
        }
        if (!PolicyUtil.validateString(req.getCalflag(),1) || !req.getCalflag().equals("M")) {
            log.error(String.format("%s " + APP_NAME + "|" + method + "|" + "error validate Calflag", transactionId));
            return false;
        }
        if (!PolicyUtil.validateString(req.getChannel(),1) || !req.getChannel().equals("E")) {
            log.error(String.format("%s " + APP_NAME + "|" + method + "|" + "error validate channel", transactionId));
            return false;
        }

        return true;
    }

}
