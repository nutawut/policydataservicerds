package com.thailife.api.policyInfoService.controller.pol15;

import com.thailife.api.policyInfoService.controller.base.WebServiceControllerResponseT;
import com.thailife.api.policyInfoService.controller.pol16.GetPolicyDueDateController;
import com.thailife.api.policyInfoService.model.request.*;
import com.thailife.api.policyInfoService.model.response.InsertConsentResponse;
import com.thailife.api.policyInfoService.model.response.PolicyM;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import com.thailife.api.policyInfoService.model.response.ServiceResults;
import com.thailife.api.policyInfoService.service.InsertConsentService;
import com.thailife.api.policyInfoService.service.NextPayPeriodService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;
import com.thailife.log.ExtLogger;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class InsertConsentController
    extends WebServiceControllerResponseT<InsertConsentRequest, InsertConsentResponse> {
  private final ExtLogger log = ExtLogger.create(InsertConsentController.class);
  private final String className = "InsertConsentController";
  private final String Service = "InsertConsent";
  private String tranId;
  
  @Autowired InsertConsentService insertConsentService;
  @Override
  @PostMapping("InsertConsent")
  public ResponseEntity<ResponseMessages<InsertConsentResponse>> service(
      @RequestBody RequestMessages<InsertConsentRequest> req) throws Exception {
    ResponseEntity<ResponseMessages<InsertConsentResponse>> entityResponse = null;
    ResponseMessages<InsertConsentResponse> resp = new ResponseMessages<>();
    WebServiceMsg wMessage = new WebServiceMsg();
    tranId = req.getHeaderData() != null ? req.getHeaderData().getMessageId() : "";
    log.info(Service, tranId, className, "Service start...");
    log.info(Service, tranId, className, "Service Req : " + req);
    try {
      if (!validateRequestMessages(req)) {
        log.error(Service, tranId, className, "Error Validate");
        entityResponse =
            getErrorValidateResponse(
                resp,
                req == null ? null : req.getHeaderData(),
                wMessage.getMessageByCode("400")[0],
                "การค้นหาไม่ถูกต้อง");
      } else {
        ServiceResults<InsertConsentResponse> data = 
           insertConsentService.getResponse(req.getRequestRecord(), tranId);
        if (!data.isSuccess()) {
          if(data.getResponseCode().equals("400")) {
            entityResponse =
                getErrorValidateResponse(
                    resp,
                    req == null ? null : req.getHeaderData(),
                    wMessage.getMessageByCode("400")[0],
                    "การค้นหาไม่ถูกต้อง");
          }else {
            entityResponse =
                getErrorInternalServerResponse(
                    resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
          }
          
        } else {
          resp.setResponseRecord(data.getResult());
          entityResponse =
              getSuccessResponse(
                  resp, req.getHeaderData(), data.getResponseCode(), data.getResponseDescription());
          if (data.getResponseCode().equals("204")) {
            entityResponse =
                getNotFoundDataResponse(
                    resp,
                    req.getHeaderData(),
                    data.getResponseCode(),
                    data.getResponseDescription());
          }
        }
      }
    } catch (Exception ex) {
      log.error(Service, tranId, className, "Exception Error message : " + ex.getMessage());
      return getErrorInternalServerResponse(
          resp,
          req.getHeaderData(),
          wMessage.getMessageByCode("500")[0],
          wMessage.getMessageByCode("500")[1]);
    }
    log.info(Service, tranId, className, "Service Resp : " + entityResponse);
    log.info(Service, tranId, className, "Service Done...");
    return entityResponse;
  }

  @Override
  protected boolean validateRequestRecordData(InsertConsentRequest req) throws Exception {
    if(req == null){
      log.error(Service, tranId, className,"Validate request record can't be null and maximum length = 100");
      return false;
    }
    if (req.getCustomer_id().length() > 100 || !PolicyUtil.validateInteger(req.getCustomer_id())) {
      log.error(Service, tranId, className,"Validate \"customer_id\" error!, \"customer_id\" can't be null and maximum length = 100");
      return false;
  }
  if (!PolicyUtil.validateString(req.getPreName(), 30)) {
    log.error(Service, tranId, className,"Validate \"preName\" error!, \"preName\" can't be null and maximum length = 30");
      return false;
  }
  if (!PolicyUtil.validateString(req.getFirstName(), 30)) {
    log.error(Service, tranId, className,"Validate \"firstName\" error!, \"firstName\" can't be null and maximum length = 30");
      return false;
  }
  if (!PolicyUtil.validateString(req.getLastName(), 30)) {
    log.error(Service, tranId, className,"Validate \"lastName\" error!, \"lastName\" can't be null and maximum length = 30");
      return false;
  }
  for(PolicyM policy : req.getPolicy()) {
    if(policy.getFlag().length() > 1)
      return false;
  }
    return true;
  }
  
}
