package com.thailife.api.policyInfoService.controller.pol07;

import com.thailife.api.policyInfoService.model.response.GetDividendInfoListResponse;
import com.thailife.api.policyInfoService.model.response.ResponseMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.thailife.api.policyInfoService.model.request.GetDividendInfoRequest;
import com.thailife.api.policyInfoService.model.request.RequestMessages;
import com.thailife.api.policyInfoService.service.GetDividendInfoService;
import com.thailife.api.policyInfoService.util.Log;
import com.thailife.api.policyInfoService.util.PolicyUtil;
import com.thailife.api.policyInfoService.util.WebServiceMsg;

@RestController
public class GetDividendInfoController {
  private final Log log = new Log();
  @Autowired GetDividendInfoService getDividendInfoService;
  @Autowired WebServiceMsg wMessage;

  @PostMapping(value = "/GetDividendInfo")
  public ResponseEntity<ResponseMessages<GetDividendInfoListResponse>> controller(
      @RequestBody RequestMessages<GetDividendInfoRequest> request) {
    String className = "GetDividendInfoController";
    log.printInfo(className, "GetDividendInfoController | [START] ");
    ResponseMessages<GetDividendInfoListResponse> response =
        getDividendInfoService.service(request);
    log.printInfo(className, "GetDividendInfoController | [END] ");
    return ResponseEntity.status(PolicyUtil.getHttp(response.getResponseStatus())).body(response);
  }
}
