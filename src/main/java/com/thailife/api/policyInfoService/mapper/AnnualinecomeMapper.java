package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.AnnualincomeDao;
import com.thailife.api.policyInfoService.entity.AnnualinecomeEntity;

import java.sql.ResultSet;

public class AnnualinecomeMapper extends AnnualinecomeEntity {
  public static AnnualincomeDao convertToAnnualincomeDao(ResultSet rs) throws Throwable {
    try {
      AnnualincomeDao result = new AnnualincomeDao();
      result.setAmount(rs.getBigDecimal(amount));
      result.setAnnualtype(rs.getString(annualType));
      result.setAnnuitydate(rs.getString(annuityDate));
      result.setCv(rs.getBigDecimal(cv));
      result.setJournaltimestamp(rs.getTimestamp(journalTimestamp));
      result.setNetcv(rs.getBigDecimal(netCv));
      result.setNetloan(rs.getBigDecimal(netLoan));
      result.setNetpension(rs.getBigDecimal(netPenSion));
      result.setNetprem(rs.getBigDecimal(netPrem));
      result.setPension(rs.getBigDecimal(pension));
      result.setPolicyno(rs.getString(policyNo));
      result.setRecordtimestamp(rs.getTimestamp(recordTimestamp));
      return result;
    } catch (Exception e) {
      throw new Throwable(e.getMessage());
    }
  }
}
