package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.PolicyParticipantDao;
import com.thailife.api.policyInfoService.entity.PolicyParticipantEntity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PolicyParticipantMapper extends PolicyParticipantEntity {

  public static PolicyParticipantDao setPolicyParticipantDaoList(ResultSet rs) throws SQLException {
    PolicyParticipantDao participantDao = new PolicyParticipantDao();
    participantDao.setPolicy_no(rs.getString(policyNo));
    participantDao.setCert_no(rs.getString(certNo));
    participantDao.setParty_id(rs.getBigDecimal(partyId));
    participantDao.setParticipant_role(rs.getInt(participantRole));
    participantDao.setIssue_date(rs.getDate(issueDate));
    participantDao.setCreate_user_code(rs.getString(createUserCode));
    participantDao.setCreate_time(rs.getTimestamp(createTime));
    participantDao.setUpdate_user_code(rs.getString(updateUserCode));
    participantDao.setLast_update(rs.getTimestamp(lastUpdate));
    participantDao.setSystem_id(rs.getInt(systemId));
    participantDao.setSystem_key(rs.getString(systemKey));
    participantDao.setPersonid(rs.getString(personId));

    return participantDao;
  }
}
