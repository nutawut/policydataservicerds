package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.OrdmastDao;
import com.thailife.api.policyInfoService.entity.OrdmastEntity;

public class OrdmastMapper extends OrdmastEntity {
  public static OrdmastDao ordmastMapper(ResultSet rs) throws SQLException {
    OrdmastDao result = new OrdmastDao();
    result.setBenefitrate(rs.getBigDecimal(benefitrate));
    result.setBranch(rs.getString(branch));
    result.setClass_(rs.getString(class_));
    result.setContactaddressid(rs.getString(contactaddressid));
    result.setContactstartdate(rs.getString(contactstartdate));
    result.setDuedate(rs.getString(duedate));
    result.setEffectivedate(rs.getString(effectivedate));
    result.setHivflag(rs.getString(hivflag));
    result.setInsuredage(rs.getBigDecimal(insuredage));
    result.setInvalid(rs.getString(invalid));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setLapseflag(rs.getString(lapseflag));
    result.setLifepremium(rs.getBigDecimal(lifepremium));
    result.setLocaladdressid(rs.getString(localaddressid));
    result.setMedical(rs.getString(medical));
    result.setMode(rs.getString(mode));
    result.setNameid(rs.getString(nameid));
    result.setOccupationcode(rs.getBigDecimal(occupationcode));
    result.setOccupationtype(rs.getString(occupationtype));
    result.setOldpolicystatus1(rs.getString(oldpolicystatus1));
    result.setOldpolicystatus2(rs.getString(oldpolicystatus2));
    result.setOldpolicystatusdate1(rs.getString(oldpolicystatusdate1));
    result.setOldpolicystatusdate2(rs.getString(oldpolicystatusdate2));
    result.setPaydate(rs.getString(paydate));
    result.setPayperiod(rs.getString(payperiod));
    result.setPlancode(rs.getString(plancode));
    result.setPlanpa(rs.getString(planpa));
    result.setPolicyno(rs.getString(policyno));
    result.setPolicystatus1(rs.getString(policystatus1));
    result.setPolicystatus2(rs.getString(policystatus2));
    result.setPolicystatusdate1(rs.getString(policystatusdate1));
    result.setPolicystatusdate2(rs.getString(policystatusdate2));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setRemark(rs.getString(remark));
    result.setRpno(rs.getString(rpno));
    result.setSaleschannel(rs.getString(saleschannel));
    result.setSalesid(rs.getString(salesid));
    result.setSum(rs.getBigDecimal(sum));
    result.setTabianlapseflag(rs.getString(tabianlapseflag));
    return result;
  }
}
