package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.RiderDao;
import com.thailife.api.policyInfoService.entity.RiderEntity;

import java.sql.ResultSet;

public class RiderMapper extends RiderEntity {
  public static RiderDao convertToRiderDao(ResultSet rs) throws Exception {
    RiderDao dao = new RiderDao();
    try {
      dao.setEffectivedate(rs.getString(effectiveDate));
      dao.setJournaltimestamp(rs.getTimestamp(journalTimestamp));
      dao.setMarker(rs.getString(marker));
      dao.setRecordtimestamp(rs.getTimestamp(recordTimestamp));
      dao.setRiderpremium(rs.getBigDecimal(riderPremium));
      dao.setRiderstatus(rs.getString(riderStatus));
      dao.setRiderstatusdate(rs.getString(riderStatusDate));
      dao.setRidersum(rs.getBigDecimal(riderSum));
      dao.setPolicyno(rs.getString(policyno));
      dao.setRidertype(rs.getString(ridertype));
    } catch (Exception e) {
      throw e;
    }
    return dao;
  }
}
