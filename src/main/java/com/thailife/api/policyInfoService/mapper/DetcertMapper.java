package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.DetcertDao;
import com.thailife.api.policyInfoService.entity.DetcertEntity;

public class DetcertMapper extends DetcertEntity {
  public static DetcertDao detcertMapper(ResultSet rs) throws SQLException {
    DetcertDao result = new DetcertDao();
    result.setAddress1(rs.getString(address1));
    result.setAddress2(rs.getString(address2));
    result.setAnalist(rs.getString(analist));
    result.setCertno(rs.getString(certno));
    result.setCode(rs.getString(code));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setLoantype(rs.getString(loantype));
    result.setMariagestatus(rs.getString(mariagestatus));
    result.setMed(rs.getString(med));
    result.setPercent1(rs.getBigDecimal(percent1));
    result.setPercent2(rs.getBigDecimal(percent2));
    result.setPercent3(rs.getBigDecimal(percent3));
    result.setPolicyno(rs.getString(policyno));
    result.setPrmiseno(rs.getString(prmiseno));
    result.setPrmiseno2(rs.getString(prmiseno2));
    result.setRecname1(rs.getString(recname1));
    result.setRecname2(rs.getString(recname2));
    result.setRecname3(rs.getString(recname3));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setRelationshipcode1(rs.getBigDecimal(relationshipcode1));
    result.setRelationshipcode2(rs.getBigDecimal(relationshipcode2));
    result.setRelationshipcode3(rs.getBigDecimal(relationshipcode3));
    result.setReserve(rs.getString(reserve));
    result.setTelephoneno(rs.getString(telephoneno));
    
    return result;
  }
}
