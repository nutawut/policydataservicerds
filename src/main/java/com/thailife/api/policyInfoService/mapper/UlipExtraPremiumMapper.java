package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.UlExtrapremuimDao;
import com.thailife.api.policyInfoService.entity.UlipExtraPremiumEntity;

import java.sql.ResultSet;

public class UlipExtraPremiumMapper extends UlipExtraPremiumEntity {
  public static UlExtrapremuimDao mapper(ResultSet rs) {
    try {
      UlExtrapremuimDao tmp = new UlExtrapremuimDao();
      tmp.setEmrate(rs.getBigDecimal(emrate));
      tmp.setEprate(rs.getBigDecimal(eprate));
      tmp.setEpurate(rs.getBigDecimal(epurate));
      tmp.setExtrapremium(rs.getBigDecimal(extrapremium));
      tmp.setJournaltimestamp(rs.getString(journaltimestamp));
      tmp.setMode(rs.getString(mode));
      tmp.setNoofyear(rs.getString(noofyear));
      tmp.setRecordtimestamp(rs.getString(recordtimestamp));
      tmp.setNextpdextrapremium(rs.getString(nextpdextrapremium));
      tmp.setId(UlipExtraPremiumPKMapper.mapper(rs));
      return tmp;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
