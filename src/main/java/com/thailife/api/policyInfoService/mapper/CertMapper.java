package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Period;
import com.thailife.api.policyInfoService.dao.CertDao;
import com.thailife.api.policyInfoService.entity.CertEntity;

public class CertMapper extends CertEntity {
  public static CertDao certMapper(ResultSet rs) throws SQLException {
    CertDao result = new CertDao();
    result.setPolicyno(rs.getString(policyno));
    result.setNameid(rs.getString(nameid));
    result.setLifesum(rs.getBigDecimal(lifesum));
    result.setAge(rs.getInt(age));
    result.setMode(rs.getString(mode));
    result.setEffectivedate(rs.getString(effectivedate));
    result.setMaturedate(rs.getString(maturedate));
    result.setStatcer(rs.getString(statcer));
    result.setStatcer2(rs.getString(statcer2));
    result.setOldstatcert1(rs.getString(oldstatcert1));
    result.setOldstatcert2(rs.getString(oldstatcert2));
    result.setPayperiod(rs.getString(payperiod));
    result.setPeriod(rs.getString(period));
    return result;
  }
}
