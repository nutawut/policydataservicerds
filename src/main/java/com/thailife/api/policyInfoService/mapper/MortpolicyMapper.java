package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.MortpolicyDao;
import com.thailife.api.policyInfoService.entity.MortpolicyEntity;

public class MortpolicyMapper extends MortpolicyEntity {
  public static MortpolicyDao mortpolicyMapper(ResultSet rs) throws SQLException {
    MortpolicyDao result = new MortpolicyDao();
    result.setEffectivedate(rs.getString(effectivedate));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setName(rs.getString(name));
    result.setPlanname(rs.getString(planname));
    result.setPolicyno(rs.getString(policyno));
    result.setRate(rs.getString(rate));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setRiderplan(rs.getString(riderplan));
    result.setTlbranch(rs.getString(tlbranch));
    result.setType(rs.getString(type));
    return result;
  }
}
