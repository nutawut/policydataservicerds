package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.entity.RiderDetailEntity;
import com.thailife.api.policyInfoService.model.RiderDetailM;

public class RiderDetailMapper extends RiderDetailEntity {
  public static RiderDetailM riderDetailMapper(ResultSet rs) throws SQLException {
    RiderDetailM result = new RiderDetailM();
    result.setRidertype(rs.getString(ridertype));
    result.setRidersum(rs.getInt(ridersum));
    result.setRiderpremium(rs.getInt(riderpremium));
    result.setRiderstatus(rs.getString(riderstatus));
    result.setRiderstatusdate(rs.getString(riderstatusdate));
    result.setEffectivedate(rs.getString(effectivedate));
    result.setExtrapremium(rs.getInt(extrapremium));
    return result;
    
  }
}
