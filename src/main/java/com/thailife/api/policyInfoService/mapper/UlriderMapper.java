package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.UlRiderDao;
import com.thailife.api.policyInfoService.entity.RiderEntity;

import java.sql.ResultSet;

public class UlriderMapper extends RiderEntity {
    public static UlRiderDao convertToUlRiderDao(ResultSet rs) throws Exception {
        UlRiderDao daoUl = new UlRiderDao();
        try{
            daoUl.setEffectivedate(rs.getString(effectiveDate));
            daoUl.setJournaltimestamp(rs.getTimestamp(journalTimestamp));
            daoUl.setMarker(rs.getString(marker));
            daoUl.setRecordtimestamp(rs.getTimestamp(recordTimestamp));
            daoUl.setRiderpremium(rs.getBigDecimal(riderPremium));
            daoUl.setRiderstatus(rs.getString(riderStatus));
            daoUl.setRiderstatusdate(rs.getString(riderStatusDate));
            daoUl.setRidersum(rs.getBigDecimal(riderSum));
            daoUl.setPolicyno(rs.getString(policyno));
            daoUl.setRidertype(rs.getString(ridertype));
        }catch (Exception e){
            throw e;
        }
        return daoUl;
    }
}
