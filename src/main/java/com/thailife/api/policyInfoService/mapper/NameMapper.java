package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.NameDao;
import com.thailife.api.policyInfoService.entity.NameEntity;

public class NameMapper extends NameEntity {
  public static NameDao nameMapper(ResultSet rs) throws SQLException {
    NameDao result = new NameDao();
    result.setFirstname(rs.getString(firstname));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setLastname(rs.getString(lastname));
    result.setNameid(rs.getString(nameid));
    result.setPersonid(rs.getString(personid));
    result.setPrename(rs.getString(prename));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setStartdate(rs.getString(startdate));
    
    return result;
  }
}
