package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.MortbranchDao;
import com.thailife.api.policyInfoService.entity.MortbranchEntity;

public class MortbranchMapper extends MortbranchEntity {
  public static MortbranchDao mortbranchMapper(ResultSet rs) throws SQLException {
    MortbranchDao result = new MortbranchDao();
    result.setAddress1(rs.getString(address1));
    result.setAddress2(rs.getString(address2));
    result.setCode(rs.getString(code));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setName(rs.getString(name));
    result.setParent(rs.getString(parent));
    result.setPart(rs.getString(part));
    result.setRealcode(rs.getString(realcode));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setRemark(rs.getString(remark));
    result.setReserve(rs.getString(reserve));
    return result;
  }
}
