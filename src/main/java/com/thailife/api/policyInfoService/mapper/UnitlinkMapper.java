package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.UnitLinkDao;
import com.thailife.api.policyInfoService.entity.UnitlinkEntity;

public class UnitlinkMapper extends UnitlinkEntity {
  public static UnitLinkDao unitlinkMapper(ResultSet rs) throws SQLException {
    UnitLinkDao result = new UnitLinkDao();
    result.setPolicyno(rs.getString(policyno));
    result.setBenefitrate(rs.getBigDecimal(benefitrate));
    result.setBranch(rs.getString(branch));
    result.setClass_(rs.getString(class_));
    result.setContactaddressid(rs.getString(contactaddressid));
    result.setContactstartdate(rs.getString(contactstartdate));
    result.setDuedate(rs.getString(duedate));
    result.setEffectivedate(rs.getString(effectivedate));
    result.setHivflag(rs.getString(hivflag));
    result.setInsuredage(rs.getBigDecimal(insuredage));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setLocaladdressid(rs.getString(localaddressid));
    result.setMedical(rs.getString(medical));
    result.setMode(rs.getString(medical));
    result.setNameid(rs.getString(nameid));
    result.setOccupationcode(rs.getString(occupationcode));
    result.setOccupationtype(rs.getString(occupationtype));
    result.setOldpolicystatus1(rs.getString(oldpolicystatus1));
    result.setOldpolicystatus2(rs.getString(oldpolicystatus2));
    result.setOldpolicystatusdate1(rs.getString(oldpolicystatusdate1));
    result.setOldpolicystatusdate2(rs.getString(oldpolicystatusdate2));
    result.setPaydate(rs.getString(paydate));
    result.setPayperiod(rs.getString(payperiod));
    result.setPlancode(rs.getString(payperiod));
    result.setPolicystatus1(rs.getString(policystatus1));
    result.setPolicystatus2(rs.getString(policystatus2));
    result.setPolicystatusdate1(rs.getString(policystatusdate1));
    result.setPolicystatusdate2(rs.getString(policystatusdate2));
    result.setPortfoliomodel(rs.getString(portfoliomodel));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setRemark(rs.getString(remark));
    result.setRpno(rs.getString(rpno));
    result.setRpppremium(rs.getBigDecimal(rpppremium));
    result.setRppsum(rs.getBigDecimal(rppsum));
    result.setSaleschannel(rs.getString(saleschannel));
    result.setSalesid(rs.getString(salesid));
    result.setTabianlapseflag(rs.getString(tabianlapseflag));
    
    return result;
  }
}
