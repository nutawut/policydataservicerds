package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import com.thailife.api.policyInfoService.entity.PolicyDetailEntity;
import com.thailife.api.policyInfoService.model.PolicyDetailM;

public class PolicyDetailMapper extends PolicyDetailEntity {
  public static PolicyDetailM policyDetailMapper(ResultSet rs) throws Throwable {
    try {
        PolicyDetailM policyM = new PolicyDetailM();
        policyM.setPolicystatus1(rs.getString(policystatus1));
        policyM.setPolicystatus2(rs.getString(policystatus2));
        policyM.setPolicystatusdate1(rs.getString(policystatusdate1));
        policyM.setPolicystatusdate2(rs.getString(policystatusdate2));
        policyM.setOldpolicystatus1(rs.getString(oldpolicystatus1));
        policyM.setOldpolicystatus2(rs.getString(oldpolicystatus2));
        policyM.setOldpolicystatusdate1(rs.getString(oldpolicystatusdate1));
        policyM.setOldpolicystatusdate2(rs.getString(oldpolicystatusdate2));
        policyM.setPayperiod(rs.getString(payperiod));
        policyM.setDuedate(rs.getString(duedate));
        policyM.setTpdpremium(rs.getBigDecimal(tpdpremium));
        policyM.setAddpremium(rs.getBigDecimal(addpremium));
        policyM.setLifesum(rs.getBigDecimal(lifesum));
        policyM.setExtratpdpremium(rs.getBigDecimal(extratpdpremium));
        policyM.setEffectivedate(rs.getString(effectivedate));
        policyM.setAccidentsum(rs.getBigDecimal(accidentsum));
        policyM.setPlanCode(rs.getString(planCode));
        policyM.setMode(rs.getString(mode));
        return policyM;
    } catch (Exception e) {
        throw new Throwable(e.getMessage());
    }
}
}
