package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.StatusueDao;
import com.thailife.api.policyInfoService.entity.StatusueEntity;

import java.sql.ResultSet;

public class StatusueMapper {
    private StatusueMapper() {
    }

    public static StatusueDao convertToStatusueDao(ResultSet rs) throws Throwable {
        try {
            StatusueDao statusueM = new StatusueDao();

            statusueM.setPolicyno(rs.getString(StatusueEntity.policyno));
            statusueM.setCashreturn(rs.getString(StatusueEntity.cashreturn));
            statusueM.setCashreturnint(rs.getString(StatusueEntity.cashreturnint));
            statusueM.setClosedate(rs.getString(StatusueEntity.closedate));
            statusueM.setCloseyear(rs.getString(StatusueEntity.closeyear));
            statusueM.setCoverdate(rs.getString(StatusueEntity.coverdate));
            statusueM.setExtend(rs.getString(StatusueEntity.extend));
            statusueM.setExtendreturn(rs.getString(StatusueEntity.extendreturn));
            statusueM.setJournaltimestamp(rs.getString(StatusueEntity.journaltimestamp));
            statusueM.setMaturedate(rs.getString(StatusueEntity.maturedate));
            statusueM.setPaidup(rs.getString(StatusueEntity.paidup));
            statusueM.setPaycashbranch(rs.getString(StatusueEntity.paycashbranch));
            statusueM.setPayreturndate(rs.getString(StatusueEntity.payreturndate));
            statusueM.setRecordtimestamp(rs.getString(StatusueEntity.recordtimestamp));
            statusueM.setReserve(rs.getString(StatusueEntity.reserve));
            statusueM.setStartdate(rs.getString(StatusueEntity.startdate));
            statusueM.setStatus(rs.getString(StatusueEntity.status));
            statusueM.setSubmitbranch(rs.getString(StatusueEntity.submitbranch));

            return statusueM;
        } catch (Exception e) {
            throw new Throwable(e.getMessage());
        }
    }
}
