package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.UlExrtraPremuimPKDao;
import com.thailife.api.policyInfoService.entity.UlipExtraPremiumPKEntity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UlipExtraPremiumPKMapper extends UlipExtraPremiumPKEntity {
  public static UlExrtraPremuimPKDao mapper(ResultSet rs) throws SQLException {
    UlExrtraPremuimPKDao tmp = new UlExrtraPremuimPKDao();
    tmp.setPolicyno(rs.getString(policyno));
    tmp.setExtratype(rs.getString(extratype));
    tmp.setStartyear(rs.getLong(startyear));
    return tmp;
  }
}
