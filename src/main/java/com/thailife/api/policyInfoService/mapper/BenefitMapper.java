package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import com.thailife.api.policyInfoService.dao.BenefitDao;
import com.thailife.api.policyInfoService.entity.BenefitEntity;

public class BenefitMapper extends BenefitEntity {
  public static BenefitDao benefitMapper(ResultSet rs) {
    BenefitDao result = new BenefitDao();
    try {
      //result.setPolicystatus1(rs.getString());
      result.setPayyear(rs.getInt(payyear));
      result.setPaydate(rs.getString(paydate));
      result.setPayflag(rs.getString(payflag));
      result.setAmount(rs.getBigDecimal(amount));
      result.setInterest(rs.getBigDecimal(interest));
      result.setBranch(rs.getString(branch));
      result.setTypediv(rs.getString(typediv));
    }catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }
}
