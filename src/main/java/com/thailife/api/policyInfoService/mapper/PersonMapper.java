package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.PersonDao;
import com.thailife.api.policyInfoService.entity.PersonEntity;
import com.thailife.api.policyInfoService.model.PartyPersonM;

public class PersonMapper extends PersonEntity {
  public static PersonDao personMapper(ResultSet rs) throws SQLException {
    PersonDao result = new PersonDao();
    result.setBirthdate(rs.getString(birthdate));
    result.setBluecard(rs.getString(bluecard));
    result.setCustomerid(rs.getString(customerid));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setPersonid(rs.getString(personid));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setReferenceid(rs.getString(referenceid));
    result.setReferencetype(rs.getString(referencetype));
    result.setReserve(rs.getString(reserve));
    result.setSex(rs.getString(sex));
    return result;
  }

  public static PartyPersonM convertToPersonM(ResultSet rs) throws Throwable {
    try {
      PartyPersonM person = new PartyPersonM();
        person.setParty_id(rs.getString("party_id"));               
        person.setPname_th(rs.getString("pname_th"));               
        person.setFname_th(rs.getString("fname_th"));               
        person.setLname_th(rs.getString("lname_th"));               
        person.setPname_en(rs.getString("pname_en"));               
        person.setFname_en(rs.getString("fname_en"));               
        person.setLname_en(rs.getString("lname_en"));               
        person.setMar_stat(rs.getString("mar_stat"));               
        person.setBlood_group(rs.getString("blood_group"));            
        person.setGender(rs.getString("gender"));                 
        person.setBirth_date(rs.getString("birth_date"));             
        person.setCitizen(rs.getString("citizen"));                
        person.setRace(rs.getString("race"));                   
        person.setReligion(rs.getString("religion"));               
        person.setSmoker_stat(rs.getString("smoker_stat"));            
        person.setHeight(rs.getString("height"));                 
        person.setWeight(rs.getString("weight"));                 
        person.setHighest_education_level(rs.getString("highest_education_level"));
        person.setCar_type(rs.getString("car_type"));               
        person.setCreate_user_code(rs.getString("create_user_code"));       
        person.setCreate_time(rs.getString("create_time"));            
        person.setUpdate_user_code(rs.getString("update_user_code"));       
        person.setLast_update(rs.getString("last_update"));            
        person.setSystem_id(rs.getString("system_id"));              
        person.setSystem_key(rs.getString("system_key"));             
        
        return person;
    } catch (Exception e) {
        throw new Throwable(e.getMessage());
    }
  }
}
