package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.CreditmortgageDao;
import com.thailife.api.policyInfoService.entity.CreditmortgageEntity;

public class CreditmortgageMapper extends CreditmortgageEntity {
  public static CreditmortgageDao creditmortgageMapper(ResultSet rs) throws SQLException {
    CreditmortgageDao result = new CreditmortgageDao();
    result.setBillingtype(rs.getString(billingtype));
    result.setCardname(rs.getString(cardname));
    result.setCardtype(rs.getString(cardtype));
    result.setCert(rs.getString(cert));
    result.setCreditno(rs.getString(creditno));
    result.setEntrydate(rs.getString(entrydate));
    result.setEntrytime(rs.getString(entrytime));
    result.setExpiredate(rs.getString(expiredate));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setMerchantcode(rs.getString(merchantcode));
    result.setOwnername(rs.getString(ownername));
    result.setPartnercode(rs.getString(partnercode));
    result.setPolicyno(rs.getString(policyno));
    result.setPolicytype(rs.getString(policytype));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setRelationtype(rs.getString(relationtype));
    result.setStatus(rs.getString(status));
    result.setStatusdate(rs.getString(statusdate));
    result.setUserid(rs.getString(userid));
    return result;
  }
}
