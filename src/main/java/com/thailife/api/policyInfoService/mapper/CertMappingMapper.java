package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.CertMappingDao;
import com.thailife.api.policyInfoService.entity.CertMappingEntity;

import java.sql.ResultSet;

public class CertMappingMapper extends CertMappingEntity {
  public static CertMappingDao convertToCertMappingDao(ResultSet rs) throws Exception {
    CertMappingDao dao = new CertMappingDao();
    try {
      dao.setPolicyno(rs.getString(policyNo));
      dao.setCertno(rs.getString(certNo));
      dao.setRpolicyno(rs.getString(rPolicyNo));
      dao.setRcertno(rs.getString(rCertNo));
      dao.setRecordtimestamp(rs.getTimestamp(recordTimestamp));
      dao.setJournaltimestamp(rs.getTimestamp(journalTimestamp));
    } catch (Exception e) {
      throw e;
    }
    return dao;
  }
}
