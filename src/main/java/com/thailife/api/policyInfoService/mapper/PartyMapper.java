package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import com.thailife.api.policyInfoService.dao.PartyDao;
import com.thailife.api.policyInfoService.entity.PartyEntity;

public class PartyMapper extends PartyEntity {
  public static PartyDao convertToPartyM(ResultSet rs) throws Throwable {
    try {
      PartyDao person = new PartyDao();               
        person.setParty_id(rs.getString(PartyEntity.party_id));        
        person.setParty_type(rs.getString(PartyEntity.party_type));      
        person.setFull_name_th(rs.getString(PartyEntity.full_name_th));    
        person.setFull_name_en(rs.getString(PartyEntity.full_name_en));    
        person.setIssue_country(rs.getString(PartyEntity.issue_country));   
        person.setGovt_id(rs.getString(PartyEntity.govt_id));         
        person.setGovt_id_type(rs.getString(PartyEntity.govt_id_type));    
        person.setIs_sms(rs.getString(PartyEntity.is_sms));          
        person.setCreate_user_code(rs.getString(PartyEntity.create_user_code));
        person.setCreate_time(rs.getString(PartyEntity.create_time));     
        person.setUpdate_user_code(rs.getString(PartyEntity.update_user_code));
        person.setLast_update(rs.getString(PartyEntity.last_update));     
        person.setSystem_id(rs.getString(PartyEntity.system_id));       
        person.setSystem_key(rs.getString(PartyEntity.system_key));      
        
        return person;
    } catch (Exception e) {
        throw new Throwable(e.getMessage());
    }
}
}
