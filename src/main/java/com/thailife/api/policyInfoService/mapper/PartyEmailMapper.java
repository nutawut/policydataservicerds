package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import com.thailife.api.policyInfoService.model.PartyEmailDao;

public class PartyEmailMapper {

  public static PartyEmailDao convertToPartyEmailM(ResultSet rs) throws Throwable {
        try {
          PartyEmailDao partyEmail = new PartyEmailDao();             
            partyEmail.setParty_id(rs.getString("party_id"));                
            partyEmail.setEmail_id(rs.getString("email_id"));        
            partyEmail.setStart_date(rs.getString("start_date"));      
            partyEmail.setEnd_date(rs.getString("end_date"));        
            partyEmail.setIs_attach_allow(rs.getString("is_attach_allow")); 
            partyEmail.setIs_deliverable(rs.getString("is_deliverable"));  
            partyEmail.setCreate_user_code(rs.getString("create_user_code"));
            partyEmail.setCreate_time(rs.getString("create_time"));     
            partyEmail.setUpdate_user_code(rs.getString("update_user_code"));
            partyEmail.setLast_update(rs.getString("last_update"));     
            partyEmail.setSystem_id(rs.getString("system_id"));       
            partyEmail.setSystem_key(rs.getString("system_key"));      
            return partyEmail;
        } catch (Exception e) {
            throw new Throwable(e.getMessage());
        }
    }

}
