package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.ExtraPremiumUKDao;
import com.thailife.api.policyInfoService.dao.ExtrapremiumDao;
import com.thailife.api.policyInfoService.entity.ExtraPremiumEntity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ExtraPremiumMapper extends ExtraPremiumEntity {
  public static ExtrapremiumDao mapper(ResultSet rs) throws SQLException {
    ExtrapremiumDao tmp = new ExtrapremiumDao();
    tmp.setExtrapremium(rs.getBigDecimal(extrapremium));
    tmp.setEmrate(rs.getBigDecimal(emrate));
    tmp.setMode(rs.getString(mode));
    tmp.setJournaltimestamp(rs.getString(journaltimestamp));
    tmp.setNoofyear(rs.getBigDecimal(noofyear));
    tmp.setRecordtimestamp(rs.getString(recordtimestamp));
    tmp.setEpfromem(rs.getBigDecimal(epfromem));
    tmp.setEpunderwrt(rs.getBigDecimal(epunderwrt));
    tmp.setStartdate(rs.getString(startdate));
    tmp.setId(UlipExtraPremiumPKMapper.mapper(rs));
    return tmp;
  }

  public static ExtraPremiumUKDao mapperUk(ResultSet rs) throws SQLException {
    ExtraPremiumUKDao tmp = new ExtraPremiumUKDao();
    tmp.setEprate(rs.getBigDecimal(eprate));
    tmp.setEpurate(rs.getBigDecimal(epurate));
    tmp.setEmrate(rs.getBigDecimal(emrate));
    tmp.setExtrapremium(rs.getBigDecimal(extrapremium));
    tmp.setJournaltimestamp(rs.getString(journaltimestamp));
    tmp.setMode(rs.getString(mode));
    tmp.setNextpdextrapremium(rs.getBigDecimal(nextpdextrapremium));
    tmp.setNoofyear(rs.getString(noofyear));
    tmp.setRecordtimestamp(rs.getString(recordtimestamp));
    return tmp;
  }
}
