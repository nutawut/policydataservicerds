package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.IndriderDao;
import com.thailife.api.policyInfoService.entity.IndriderEntity;

import java.sql.ResultSet;

public class IndriderMapper extends IndriderEntity {
  public static IndriderDao convertToIndriderDao(ResultSet rs) throws Exception {
    IndriderDao dao = new IndriderDao();
    try {

      dao.setEffectivedate(rs.getString(effectiveDate));
      dao.setJournaltimestamp(rs.getTimestamp(journalTimestamp));
      dao.setRecordtimestamp(rs.getTimestamp(recordTimestamp));
      dao.setRiderpremium(rs.getBigDecimal(riderPremium));
      dao.setRiderstatus(rs.getString(riderStatus));
      dao.setRiderstatusdate(rs.getString(riderStatusDate));
      dao.setRidersum(rs.getBigDecimal(riderSum));
      dao.setPolicyno(rs.getString(policyno));
      dao.setRidertype(rs.getString(ridertype));
    } catch (Exception e) {
      throw e;
    }
    return dao;
  }
}
