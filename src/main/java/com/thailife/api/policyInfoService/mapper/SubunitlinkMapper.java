package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thailife.api.policyInfoService.dao.SubunitlinkDao;
import com.thailife.api.policyInfoService.entity.SubunitlinkEntity;

public class SubunitlinkMapper extends SubunitlinkEntity {
  public static SubunitlinkDao subunitlinkMapper(ResultSet rs) throws SQLException {
    SubunitlinkDao result = new SubunitlinkDao();
    result.setPolicyno(rs.getString(policyno));
    result.setPremiumtype(rs.getString(premiumtype));
    result.setDuedate(rs.getString(duedate));
    result.setEffectivedate(rs.getString(effectivedate));
    result.setJournaltimestamp(rs.getTimestamp(journaltimestamp));
    result.setOldpolicystatus1(rs.getString(oldpolicystatus1));
    result.setOldpolicystatus2(rs.getString(oldpolicystatus2));
    result.setOldpolicystatusdate1(rs.getString(oldpolicystatusdate1));
    result.setOldpolicystatusdate2(rs.getString(oldpolicystatusdate2));
    result.setPaydate(rs.getString(paydate));
    result.setPayperiod(rs.getString(payperiod));
    result.setPolicystatus1(rs.getString(policystatus1));
    result.setPolicystatus2(rs.getString(policystatus2));
    result.setPolicystatusdate1(rs.getString(policystatusdate1));
    result.setPolicystatusdate2(rs.getString(policystatusdate2));
    result.setPremium(rs.getBigDecimal(premium));
    result.setRecordtimestamp(rs.getTimestamp(recordtimestamp));
    result.setRpno(rs.getString(rpno));
    result.setSum(rs.getBigDecimal(sum));
    
    return result;
  }
}
