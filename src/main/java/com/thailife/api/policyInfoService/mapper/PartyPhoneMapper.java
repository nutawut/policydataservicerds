package com.thailife.api.policyInfoService.mapper;

import java.sql.ResultSet;
import com.thailife.api.policyInfoService.dao.PartyPhoneDao;

public class PartyPhoneMapper {
  public static PartyPhoneDao convertToPartyPhoneM(ResultSet rs) throws Throwable {
    try {
      PartyPhoneDao partyPhoneM = new PartyPhoneDao();                    
      partyPhoneM.setParty_id(rs.getString("party_id"));
      partyPhoneM.setPhone_type(rs.getString("phone_type"));
      partyPhoneM.setSequence(rs.getString("sequence"));
      partyPhoneM.setPhone_id(rs.getString("phone_id"));
      partyPhoneM.setStart_date(rs.getString("start_date"));
      partyPhoneM.setEnd_date(rs.getString("end_date"));
      partyPhoneM.setIs_valid(rs.getString("is_valid"));
      partyPhoneM.setIs_receive_sms(rs.getString("is_receive_sms"));
      partyPhoneM.setCreate_user_code(rs.getString("create_user_code"));
      partyPhoneM.setCreate_time(rs.getString("create_time")); 
      partyPhoneM.setUpdate_user_code(rs.getString("update_user_code"));
      partyPhoneM.setLast_update(rs.getString("last_update")); 
      partyPhoneM.setSystem_id(rs.getString("system_id")); 
      partyPhoneM.setSystem_key(rs.getString("system_key"));
      return partyPhoneM;
    } catch (Exception e) {
        throw new Throwable(e.getMessage());
    }
}
}
