package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.DvmastordoDao;
import com.thailife.api.policyInfoService.entity.DvmastordoEntity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DvmastordoMapper extends DvmastordoEntity {
    public DvmastordoDao mapper(ResultSet rs) throws SQLException {
        DvmastordoDao temp = new DvmastordoDao();
        temp.setAmount(rs.getString(amount));
        temp.setBranch(rs.getString(branch));
        temp.setInterest(rs.getString(interest));
        temp.setJournaltimestamp(rs.getString(journalTimestamp));
        temp.setPaydate(rs.getString(payDate));
        temp.setPayflag(rs.getString(payFlag));
        temp.setPayyear(rs.getString(payYear));
        temp.setPolicyno(rs.getString(policyNo));
        temp.setRecordtimestamp(rs.getString(recordTimestamp));
        temp.setReserve(rs.getString(reserve));
        return temp;
    }
}
