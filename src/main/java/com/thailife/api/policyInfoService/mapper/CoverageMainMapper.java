package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.model.CoverageMainM;

import java.sql.ResultSet;

public class CoverageMainMapper {
    public static CoverageMainM convertToCoverageMainM(ResultSet rs) throws Exception{
        try {
            CoverageMainM bean = new CoverageMainM();
            bean.setCoverage_group_code(rs.getString("coverage_group_code"));
            bean.setCoverage_name_detail_th(rs.getString("coverage_name_detail_th"));
            bean.setCoverage_name_detail_eng(rs.getString("coverage_name_detail_eng"));
            bean.setCoverage_remark_th(rs.getString("coverage_remark_th"));
            bean.setCoverage_remark_eng(rs.getString("coverage_remark_eng"));
            bean.setCoverage_suminsure(rs.getBigDecimal("coverage_suminsure"));

            return bean;
        } catch (Exception e) {
            throw e;
        }
    }
}
