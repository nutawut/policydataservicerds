package com.thailife.api.policyInfoService.mapper;

import com.thailife.api.policyInfoService.dao.BrafileDao;
import com.thailife.api.policyInfoService.entity.BrafileEntity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BrafileMapper extends BrafileEntity {
  public static BrafileDao mapper(ResultSet rs) throws SQLException {
    BrafileDao tmp = new BrafileDao();
    tmp.setBranch(rs.getString(branch));
    tmp.setAddress1(rs.getString(address1));
    tmp.setAddress2(rs.getString(address2));
    tmp.setBranchname(rs.getString(branchName));
    tmp.setCloseddate(rs.getString(closedDate));
    tmp.setGroupcolumn(rs.getString(groupColumn));
    tmp.setJournaltimestamp(rs.getString(journalTimestamp));
    tmp.setMainbranch(rs.getString(mainBranch));
    tmp.setOpendate(rs.getString(openDate));
    tmp.setOpened(rs.getString(opened));
    tmp.setPhoneno(rs.getString(phoneNo));
    tmp.setProvincecode(rs.getString(provinceCode));
    tmp.setRecordtimestamp(rs.getString(recordTimestamp));

    return tmp;
  }
}
