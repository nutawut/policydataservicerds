package com.thailife.api.policyInfoService.entity;

public class NameEntity {
  public static final String firstname = "firstname";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String lastname = "lastname";
  public static final String nameid = "nameid";
  public static final String personid = "personid";
  public static final String prename = "prename";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String startdate = "startdate";
}
