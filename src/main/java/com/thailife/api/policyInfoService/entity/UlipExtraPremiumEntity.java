package com.thailife.api.policyInfoService.entity;

public class UlipExtraPremiumEntity {
  public static final String emrate = "emrate";
  public static final String eprate = "eprate";
  public static final String epurate = "epurate";
  public static final String extrapremium = "extrapremium";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String mode = "mode";
  public static final String nextpdextrapremium = "nextpdextrapremium";
  public static final String noofyear = "noofyear";
  public static final String recordtimestamp = "recordtimestamp";
}
