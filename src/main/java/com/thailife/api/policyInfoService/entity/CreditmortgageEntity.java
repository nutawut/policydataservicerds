package com.thailife.api.policyInfoService.entity;

public class CreditmortgageEntity {
  public static final String billingtype = "billingtype";
  public static final String cardname = "cardname";
  public static final String cardtype = "cardtype";
  public static final String cert = "cert";
  public static final String creditno = "creditno";
  public static final String entrydate = "entrydate";
  public static final String entrytime = "entrytime";
  public static final String expiredate = "expiredate";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String merchantcode = "merchantcode";
  public static final String ownername = "ownername";
  public static final String partnercode = "partnercode";
  public static final String policyno = "policyno";
  public static final String policytype = "policytype";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String relationtype = "relationtype";
  public static final String status = "status";
  public static final String statusdate = "statusdate";
  public static final String userid = "userid";
}
