package com.thailife.api.policyInfoService.entity;

public class CertEntity {
  public static final String policyno = "policyno";
  public static final String nameid = "nameid";
  public static final String lifesum="lifesum";
  public static final String age="age";
  public static final String mode="mode";
  public static final String effectivedate="effectivedate";
  public static final String maturedate="maturedate";
  public static final String statcer="statcer";
  public static final String statcer2="statcer2";
  public static final String oldstatcert1="oldstatcert1";
  public static final String oldstatcert2="oldstatcert2";
  public static final String payperiod="payperiod";
  public static final String period="period";
}
