package com.thailife.api.policyInfoService.entity;

public class PersonEntity {
  public static final String birthdate = "birthdate";
  public static final String bluecard = "bluecard";
  public static final String customerid = "customerid";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String personid = "personid";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String referenceid = "referenceid";
  public static final String referencetype = "referencetype";
  public static final String reserve = "reserve";
  public static final String sex = "sex";
}
