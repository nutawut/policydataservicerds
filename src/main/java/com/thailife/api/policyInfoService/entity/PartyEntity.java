package com.thailife.api.policyInfoService.entity;

public class PartyEntity {

  public static final String party_id = "party_id";        
  public static final String party_type = "party_type";      
  public static final String full_name_th = "full_name_th";    
  public static final String full_name_en = "full_name_en";    
  public static final String issue_country = "issue_country";   
  public static final String govt_id = "govt_id";         
  public static final String govt_id_type = "govt_id_type";    
  public static final String is_sms = "is_sms";          
  public static final String create_user_code = "create_user_code";
  public static final String create_time = "create_time";     
  public static final String update_user_code = "update_user_code";
  public static final String last_update = "last_update";     
  public static final String system_id = "system_id";       
  public static final String system_key = "system_key";      
}