package com.thailife.api.policyInfoService.entity;

import java.sql.Timestamp;

public class BrafileEntity {
    public static final String branch = "branch";
    public static final String branchName = "branchname";
    public static final String provinceCode = "provincecode";
    public static final String address1 = "address1";
    public static final String address2 = "address2";
    public static final String phoneNo = "phoneno";
    public static final String groupColumn = "groupcolumn";
    public static final String opened = "opened";
    public static final String openDate = "opendate";
    public static final String closedDate = "closeddate";
    public static final String mainBranch = "mainbranch";
    public static final String recordTimestamp = "recordtimestamp";
    public static final String journalTimestamp = "journaltimestamp";
}
