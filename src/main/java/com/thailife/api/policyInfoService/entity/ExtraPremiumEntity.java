package com.thailife.api.policyInfoService.entity;

public class ExtraPremiumEntity extends ExtraPremiumUKEntity {
  public static final String emrate = "emrate";
  public static final String epfromem = "epfromem";
  public static final String epunderwrt = "epunderwrt";
  public static final String extrapremium = "extrapremium";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String mode = "mode";
  public static final String noofyear = "noofyear";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String startdate = "startdate";
}
