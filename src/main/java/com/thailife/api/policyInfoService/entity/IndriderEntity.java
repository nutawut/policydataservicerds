package com.thailife.api.policyInfoService.entity;

public class IndriderEntity {
  public static final String effectiveDate = "effectivedate";
  public static final String journalTimestamp = "journaltimestamp";
  public static final String recordTimestamp = "recordtimestamp";
  public static final String riderPremium = "riderpremium";
  public static final String riderStatus = "riderstatus";
  public static final String riderStatusDate = "riderstatusdate";
  public static final String riderSum = "ridersum";
  public static final String policyno = "policyno";
  public static final String ridertype = "ridertype";
}
