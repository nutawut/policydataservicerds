package com.thailife.api.policyInfoService.entity;

public class MortbranchEntity {
  public static final String address1 = "address1";
  public static final String address2 = "address2";
  public static final String code = "code";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String name = "name";
  public static final String parent = "parent";
  public static final String part = "part";
  public static final String realcode = "realcode";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String remark = "remark";
  public static final String reserve = "reserve";
}
