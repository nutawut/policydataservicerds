package com.thailife.api.policyInfoService.entity;

public class CertMappingEntity {
  public static final String policyNo = "policyno";
  public static final String certNo = "certno";
  public static final String rPolicyNo = "rpolicyno";
  public static final String rCertNo = "rcertno";
  public static final String recordTimestamp = "recordtimestamp";
  public static final String journalTimestamp = "joutnaltimestamp";
}
