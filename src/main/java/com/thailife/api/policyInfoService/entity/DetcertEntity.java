package com.thailife.api.policyInfoService.entity;

public class DetcertEntity {
  public static final String address1 = "address1";
  public static final String address2 = "address2";
  public static final String analist = "analist";
  public static final String certno = "certno";
  public static final String code = "code";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String loantype = "loantype";
  public static final String mariagestatus = "mariagestatus";
  public static final String med = "med";
  public static final String percent1 = "percent1";
  public static final String percent2 = "percent2";
  public static final String percent3 = "percent3";
  public static final String policyno = "policyno";
  public static final String prmiseno = "prmiseno";
  public static final String prmiseno2 = "prmiseno2";
  public static final String recname1 = "recname1";
  public static final String recname2 = "recname2";
  public static final String recname3 = "recname3";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String relationshipcode1 = "relationshipcode1";
  public static final String relationshipcode2 = "relationshipcode2";
  public static final String relationshipcode3 = "relationshipcode3";
  public static final String reserve = "reserve";
  public static final String telephoneno = "telephoneno";
}
