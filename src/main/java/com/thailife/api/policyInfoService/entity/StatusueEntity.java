package com.thailife.api.policyInfoService.entity;

public class StatusueEntity {
  public static final String policyno = "policyno";
  public static final String cashreturn = "cashreturn";
  public static final String cashreturnint = "cashreturnint";
  public static final String closedate = "closedate";
  public static final String closeyear = "closeyear";
  public static final String coverdate = "coverdate";
  public static final String extend = "extend";
  public static final String extendreturn = "extendreturn";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String maturedate = "maturedate";
  public static final String paidup = "paidup";
  public static final String paycashbranch = "paycashbranch";
  public static final String payreturndate = "payreturndate";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String reserve = "reserve";
  public static final String startdate = "startdate";
  public static final String status = "status";
  public static final String submitbranch = "submitbranch";
}
