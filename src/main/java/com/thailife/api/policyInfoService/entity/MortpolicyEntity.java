package com.thailife.api.policyInfoService.entity;

public class MortpolicyEntity {
  public static final String effectivedate = "effectivedate";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String name = "name";
  public static final String planname = "planname";
  public static final String policyno = "policyno";
  public static final String rate = "rate";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String riderplan = "riderplan";
  public static final String tlbranch = "tlbranch";
  public static final String type = "type";
}
