package com.thailife.api.policyInfoService.entity;


public class SubunitlinkEntity {
  public static final String policyno = "policyno";
  public static final String premiumtype = "premiumtype";
  public static final String duedate = "duedate";
  public static final String effectivedate = "effectivedate";
  public static final String journaltimestamp = "journaltimestamp";
  public static final String oldpolicystatus1 = "oldpolicystatus1";
  public static final String oldpolicystatus2 = "oldpolicystatus2";
  public static final String oldpolicystatusdate1 = "oldpolicystatusdate1";
  public static final String oldpolicystatusdate2 = "oldpolicystatusdate2";
  public static final String paydate = "paydate";
  public static final String payperiod = "payperiod";
  public static final String policystatus1 = "policystatus1";
  public static final String policystatus2 = "policystatus2";
  public static final String policystatusdate1 = "policystatusdate1";
  public static final String policystatusdate2 = "policystatusdate2";
  public static final String premium = "premium";
  public static final String recordtimestamp = "recordtimestamp";
  public static final String rpno = "rpno";
  public static final String sum = "sum";
}
