package com.thailife.api.policyInfoService.entity;

public class PolicyParticipantEntity {
    public static final String  policyNo = "policy_no";
    public static final String certNo = "cert_no";
    public static final String partyId = "party_id";
    public static final String participantRole = "participant_role";
    public static final String issueDate = "issue_date";
    public static final String createUserCode = "create_user_code";
    public static final String createTime = "create_time";
    public static final String updateUserCode = "update_user_code";
    public static final String lastUpdate = "last_update";
    public static final String systemId = "system_id";
    public static final String systemKey = "system_key";
    public static final String personId = "personid";
}
