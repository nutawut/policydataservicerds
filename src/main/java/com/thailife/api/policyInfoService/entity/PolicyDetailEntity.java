package com.thailife.api.policyInfoService.entity;

public class PolicyDetailEntity {
  public static final String policystatus1 = "policystatus1";
  public static final String policystatus2 = "policystatus2";
  public static final String policystatusdate1 = "policystatusdate1";
  public static final String policystatusdate2 = "policystatusdate2";
  public static final String oldpolicystatus1 = "oldpolicystatus1";
  public static final String oldpolicystatus2 = "oldpolicystatus2";
  public static final String oldpolicystatusdate1 = "oldpolicystatusdate1";
  public static final String oldpolicystatusdate2 = "oldpolicystatusdate2";
  public static final String payperiod = "payperiod";
  public static final String duedate = "duedate";
  public static final String tpdpremium = "tpdpremium";
  public static final String addpremium = "addpremium";
  public static final String lifesum = "lifesum";
  public static final String extratpdpremium = "extratpdpremium";
  public static final String effectivedate = "effectivedate";
  public static final String accidentsum = "accidentsum";
  public static final String planCode = "plancode";
  public static final String mode = "mode";
}
