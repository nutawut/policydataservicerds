package com.thailife.api.policyInfoService.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class DvmastordoEntity {
    public static String policyNo = "policyno";
    public static String payYear = "payyear";
    public static String payDate = "paydate";
    public static String payFlag = "payflag";
    public static String amount = "amount";
    public static String interest = "interest";
    public static String branch = "branch";
    public static String reserve = "reserve";
    public static String recordTimestamp = "recordtimestamp";
    public static String journalTimestamp = "journaltimestamp";
}
