package com.thailife.api.policyInfoService.entity;


public class BenefitEntity {
  public static final String payyear = "payyear";
  public static final String paydate = "paydate";
  public static final String payflag = "payflag";
  public static final String amount = "amount";
  public static final String interest = "interest";
  public static final String branch = "branch";
  public static final String typediv = "typediv";
}
