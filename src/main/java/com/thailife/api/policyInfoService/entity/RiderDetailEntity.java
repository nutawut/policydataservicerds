package com.thailife.api.policyInfoService.entity;

public class RiderDetailEntity {
  public static final String ridertype = "ridertype";
  public static final String ridersum = "ridersum";
  public static final String riderpremium = "riderpremium";
  public static final String riderstatus = "riderstatus";
  public static final String riderstatusdate = "riderstatusdate";
  public static final String effectivedate = "effectivedate";
  public static final String extrapremium = "extrapremium";
}
