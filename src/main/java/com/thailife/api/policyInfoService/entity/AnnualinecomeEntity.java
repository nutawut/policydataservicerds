package com.thailife.api.policyInfoService.entity;

public class AnnualinecomeEntity {
  public static final String policyNo = "policyno";
  public static final String amount = "amount";
  public static final String annualType = "annualtype";
  public static final String annuityDate = "annuitydate";
  public static final String cv = "cv";
  public static final String journalTimestamp = "journaltimestamp";
  public static final String netCv = "netcv";
  public static final String netLoan = "netloan";
  public static final String netPenSion = "netpension";
  public static final String netPrem = "netprem";
  public static final String pension = "pension";
  public static final String recordTimestamp = "recordtimestamp";
}
