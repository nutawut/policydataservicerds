package com.thailife.api.policyInfoService.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ExtraPremiumUKEntity {
  public static final String emrate = "emrate";
  public static final String eprate = "eprate";
  public static final String epurate = "epurate";
  public static final String nextpdextrapremium = "nextpdextrapremium";
}
